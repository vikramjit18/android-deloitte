/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.masterData

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Height(
    @Json(name = "maximumHeightValue")
    val maximumHeightValue: String,
    @Json(name = "minimumHeightValue")
    val minimumHeightValue: String
)
