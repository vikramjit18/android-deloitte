/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.etag


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Etag(
    @Json(name = "etagLicensesList")
    val etagLicensesList: List<EtagLicenses>?,
    @Json(name = "licenseYear")
    val licenseYear: String?
)
