/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.masterData

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Weight(
    @Json(name = "minimumWeightValue")
    val minimumWeightValue: String,
    @Json(name = "maximumWeightValue")
    val maximumWeightValue: String
)
