/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fwp.deloittedctcustomerapp.data.db.Params
import com.squareup.moshi.JsonClass

@Entity(tableName = Params.DOWNLOADED_TAGS_TABLE_NAME)
@JsonClass(generateAdapter = true)
data class DownloadedTags(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,
    @ColumnInfo(name = "downloadedSpiId")
    val downloadedSpiId: String?
)

