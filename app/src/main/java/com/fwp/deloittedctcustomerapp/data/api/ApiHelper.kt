/*
 * Montana Fish, Wildlife & Parks
 */
package com.fwp.deloittedctcustomerapp.data.api

import com.fwp.deloittedctcustomerapp.data.model.requests.*
import com.fwp.deloittedctcustomerapp.data.model.responses.agreement.AgreemnetResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.changeFwpEmail.ChangeFwpEmailResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ChangePasswordResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse

import com.fwp.deloittedctcustomerapp.data.model.responses.masterData.MasterData
import com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.UserStatusResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.validateEtag.ValidateEtagResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.ALS_LINKING_ENDPOINT
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.ALS_LOOKUP_ENDPOINT
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.CHANGE_FWP_EMAIL_ENDPOINTS
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.CHANGE_PASSWORD_ENDPOINTS
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.CHECK_USER_STATUS_ENDPOINTS
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.DOWNLOAD_TAGS_ENDPOINTS
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.EXISTING_EMAIL_ENDPOINT
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.EXISTING_USERNAME_ENDPOINT
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.FORGET_PASSWORD_ENDPOINT
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.GET_ETAGS_ENDPOINT
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.GET_ITEMS_ENDPOINTS
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.GET_MASTER_DATA
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.LOGIN_ENDPOINT
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.NEW_USER_SIGNUP
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.REQUEST_EMAIL_COPY_ENDPOINTS_PRIMARY
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.REQUEST_EMAIL_COPY_ENDPOINTS_SECONDARY
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.RESET_PASSWORD_ENDPOINT
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.SHOW_USER_AGGREMENT_ENDPOINTS
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.UPDATE_USER_PROFILE_V2
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.VALIDATE_TAGS_ENDPOINTS
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.VIEW_USER_PROFILE_ENDPOINT_V2
import retrofit2.Response
import retrofit2.http.*
import retrofit2.http.GET
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints.ZIP_CODE_FROM_CITY


interface ApiHelper {

    //post request for new user
    @POST(NEW_USER_SIGNUP)
    suspend fun createNewUser(@Body signupRequest: SignupRequest): Response<CheckCredsResponse>

    //post request for existing email
    @POST(EXISTING_EMAIL_ENDPOINT)
    suspend fun checkExistingEmail(@Body checkEmailRequest: CheckEmailRequest): Response<CheckCredsResponse>

    //post request for existing username
    @POST(EXISTING_USERNAME_ENDPOINT)
    suspend fun checkExistingUsername(@Body checkUsernameRequest: CheckUsernameRequest): Response<CheckCredsResponse>

    //post request for existing reset password
    @POST(RESET_PASSWORD_ENDPOINT)
    suspend fun resetPassword(@Body resetPasswordRequest: ResetPasswordRequest): Response<ForgotResponse>

    //post request for existing forget password
    @POST(FORGET_PASSWORD_ENDPOINT)
    suspend fun forgetPassword(@Body forgotPasswordRequest: ForgotPasswordRequest): Response<ForgotResponse>

    //post request for existing login
    @POST(LOGIN_ENDPOINT)
    suspend fun login(@Body loginRequest: LoginRequest): Response<LoginResponse>

    //get request for existing als linking
    @POST(ALS_LINKING_ENDPOINT)
    suspend fun postAlsLinking(
        @Header("jwt") token: String,
        @Body alsLinkingRequest: AlsLinkingRequest
    ): Response<AlsResponse>

    //get request for existing als lookup
    @POST(ALS_LOOKUP_ENDPOINT)
    suspend fun postAlsLookUp(
        @Header("jwt") token: String,
        @Body alsLookupRequest: AlsLookupRequest
    ): Response<AlsResponse>

    //post request for existing changefwpemail
    @POST(CHANGE_FWP_EMAIL_ENDPOINTS)
    suspend fun changeFWPEmail(
        @Header("jwt") token: String,
        @Body changeFwpEmailRequest: ChangeFwpEmailRequest
    ): Response<ChangeFwpEmailResponse>

    //Post request for existing update user profile
    @POST(UPDATE_USER_PROFILE_V2)
    suspend fun updateUserProfile(
        @Header("jwt") token: String,
        @Body updateUserProfileRequest: UpdateUserProfileRequest
    ): Response<UserProfileResponse>

    //get request for existing view user profile
    @POST(VIEW_USER_PROFILE_ENDPOINT_V2)
    suspend fun getUserProfile(
        @Header("jwt") token: String
    ): Response<UserProfileResponse>

    // Get zipcode from city name
    @GET(ZIP_CODE_FROM_CITY)
   suspend fun getZipCode(
        @Query("zipcd") param1: Int,
        @Query("isresident") param2: String
    ): Response<ZipcodeResponse>

    //get request for get items
    @POST(GET_ITEMS_ENDPOINTS)
    suspend fun getItemsHeld(
        @Header("jwt") token: String
    ): Response<ItemsHeldResponse>

    @POST(GET_ETAGS_ENDPOINT)
    suspend fun getEtags(
        @Header("jwt") token: String,
        @Body etagRequest: EtagRequest
    ): Response<EtagResponse>

    @GET(GET_MASTER_DATA)
    suspend fun getMasterData(): Response<MasterData>

    //get request for download tag
    @POST(DOWNLOAD_TAGS_ENDPOINTS)
    suspend fun getDownloadOffLoadTags(
        @Body downloadOffloadRequest: DownloadOffloadRequest
    ): Response<DownloadOffloadResponse>

    //post request for validate tags
    @POST(VALIDATE_TAGS_ENDPOINTS)
    suspend fun validateEtags(
        @Header("jwt") token: String,
        @Body validateEtagRequest: ValidateEtagRequest
    ): Response<ValidateEtagResponse>

    //post primary request for email copy
    @POST(REQUEST_EMAIL_COPY_ENDPOINTS_PRIMARY)
    suspend fun requestPrimaryEmailCopies(
        @Header("jwt") token: String,
        @Body requestCopyPrimaryRequest: RequestCopyPrimaryRequest
    ): Response<RequestEmailResponse>

    //post secondary request for email copy
    @POST(REQUEST_EMAIL_COPY_ENDPOINTS_SECONDARY)
    suspend fun requestSecondaryEmailCopies(
        @Header("jwt") token: String,
        @Body requestCopySecondaryRequest: RequestCopySecondaryRequest
    ): Response<RequestEmailResponse>
    //post request for change password
    @POST(CHANGE_PASSWORD_ENDPOINTS)
    suspend fun changePassword(
        @Header("jwt") token: String,
        @Body changePasswordRequest: ChangePasswordRequest
    ): Response<ChangePasswordResponse>

    //get request for show user agreement
    @POST(SHOW_USER_AGGREMENT_ENDPOINTS)
    suspend fun getUserAgreement(
        @Header("jwt") token: String,
    ): Response<AgreemnetResponse>

    //post request for check user status
    @POST(CHECK_USER_STATUS_ENDPOINTS)
    suspend fun checkUserStatus(
        @Header("jwt") token: String
    ): Response<UserStatusResponse>
}

