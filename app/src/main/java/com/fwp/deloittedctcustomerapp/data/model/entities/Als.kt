/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.entities

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Als(
    @Json(name = "alsDob")
    val alsDob: String?,
    @Json(name = "alsNo")
    val alsNo: String?,
    @Json(name = "alsZipCode")
    val alsZipCode: String?,
    @Json(name = "alsResidencyStatus")
    val alsResidencyStatus: String?,
)
