/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Account(
    @Json(name = "accountType")
    var accountType: String?,
    @Json(name = "itemsHeld")
    var itemsHeld: List<ItemsHeld>?,
    @Json(name = "owner")
    var owner: String?
)
