/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.requests

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SignupRequest(
    @Json(name = "firstName")
    val firstName: String?,
    @Json(name = "lastName")
    val lastName: String?,
    @Json(name = "middleName")
    val middleName: String?,
    @Json(name = "password")
    val password: String?,
    @Json(name = "username")
    val username: String?,
    @Json(name = "emailId")
    val emailId: String?
)


// /users/user

