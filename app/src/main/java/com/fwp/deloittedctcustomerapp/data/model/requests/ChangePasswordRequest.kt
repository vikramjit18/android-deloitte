/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.requests
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChangePasswordRequest(
    @Json(name = "confirmNewPassword")
    val confirmNewPassword: String,
    @Json(name = "currentPassword")
    val currentPassword: String,
    @Json(name = "newPassword")
    val newPassword: String
)

// /passwordchange
