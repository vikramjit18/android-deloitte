/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.login


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LoginResponse(
    @Json(name = "resObject")
    val resObject: List<ResObject>?,
    @Json(name = "responseCode")
    val responseCode: String?,
    @Json(name = "responseMessage")
    val responseMessage: String?
)
