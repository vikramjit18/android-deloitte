/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResObject(
    @Json(name = "emailid ")
    var emailid: String?,
    @Json(name = "message ")
    var message: List<String>?
)
