/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.etag


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EtagAccounts(
    @Json(name = "accountType")
    val accountType: String?,
    @Json(name = "etags")
    val etags: List<Etag>?,
    @Json(name = "owner")
    val owner: String?
)
