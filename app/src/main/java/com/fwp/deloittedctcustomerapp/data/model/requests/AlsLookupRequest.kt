/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.requests
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AlsLookupRequest(
    @Json(name = "dob")
    val dob: String,
    @Json(name = "phoneNo")
    val phoneNo: String,
    @Json(name = "residentStatus")
    val residentStatus: String,
    @Json(name = "zipCode")
    val zipCode: String
)

//  /alslookup
