/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.db


import androidx.lifecycle.LiveData
import androidx.room.*
import com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
import com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest

@Dao
interface AppDao {

    @Query("SELECT * FROM validate_request")
     fun getValidateRequest(): LiveData<List<ValidateEtagRequest>>

    @Query("SELECT * FROM validate_request where sync=0")
    fun getUnSyncedValidateRequest(): LiveData<List<ValidateEtagRequest>>

    //it will be use to check and offload before logout
    @Query("SELECT * FROM downloaded_tags")
    fun getDownloadedTags(): LiveData<List<DownloadedTags>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun  insertValidateRequest(validateEtagRequest: ValidateEtagRequest)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun  insertDownloadTags(downloadedTags: DownloadedTags)

    @Query("DELETE FROM validate_request")
    suspend fun deleteValidateRequest()

    @Query("DELETE FROM downloaded_tags")
     fun deleteDownloadedTagsTable()

    @Query("DELETE FROM downloaded_tags where downloadedSpiId = :onlineSpiId")
    suspend fun deleteDownloadedTagsById(onlineSpiId:String)


}

