/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.requests


import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fwp.deloittedctcustomerapp.data.db.Params
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity(tableName = Params.VALIREQUESTTABLENAME)
@JsonClass(generateAdapter = true)
data class ValidateEtagRequest(

    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Int = 0,

    @ColumnInfo(name = "sync")
    val sync: Int = 0,
    @ColumnInfo(name = "device_id")
    @Json(name = "device_id")
    val deviceId: String?,
    @ColumnInfo(name = "etag_upload_date")
    @Json(name = "etag_upload_date")
    val etagUploadDate: String?,
    @ColumnInfo(name = "harvestConfNum")
    @Json(name = "harvestConfNum")
    val harvestConfNum: String?,
    @ColumnInfo(name = "harvested_date")
    @Json(name = "harvested_date")
    val harvestedDate: String?,
    @ColumnInfo(name = "lat")
    @Json(name = "lat")
    val lat: Double?,
    @ColumnInfo(name = "longt")
    @Json(name = "longt")
    val longt: Double?,
    @ColumnInfo(name = "speciesCd")
    @Json(name = "speciesCd")
    val speciesCd: String?,

    @ColumnInfo(name = "spi_id")
    @Json(name = "spi_id")
    val spiId: String?
)
