/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.requests
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResetPasswordRequest(
    @Json(name = "emailid")
    val emailid: String,
    @Json(name = "password")
    val password: String,
    @Json(name = "uid")
    val uid: String
)
// /passwordreset
