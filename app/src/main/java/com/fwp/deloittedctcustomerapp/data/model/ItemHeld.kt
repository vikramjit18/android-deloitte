/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model

data class ItemHeld(
    val year: String,
    val list: List<ItemDetail>
)

data class ItemDetail(
    val year: String,
    val permitName: String,
    val accountType: String,
    val multiAccountOwner: String = ""
)




