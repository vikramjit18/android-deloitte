/*
 * Montana Fish, Wildlife & Parks
 */
package com.fwp.deloittedctcustomerapp.data.hilt

import android.content.Context
import com.fwp.deloittedctcustomerapp.data.api.ApiHelper
import com.fwp.deloittedctcustomerapp.data.db.AppDao
import com.fwp.deloittedctcustomerapp.data.db.AppDatabase
import com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return AppDatabase.getAppDbInstance(context)
    }

    @Provides
    @Singleton
    fun provideAppDao(appDatabase: AppDatabase): AppDao {
        return appDatabase.getAppDao()
    }

//    login inspector
    @Provides
    @Singleton
    fun provideHttpLoginInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

//   http intercepter
    @Provides
    @Singleton
    fun provideCallFactory(httpLoggingInterceptor: HttpLoggingInterceptor): Call.Factory {
        return OkHttpClient.Builder()
            .connectTimeout(5000, TimeUnit.SECONDS)
            .readTimeout(5000, TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }

//    moshi
    @Provides
    @Singleton
    fun provideMoshi(): Moshi {
        return Moshi.Builder().build()
    }

    //moshi converter
    @Provides
    @Singleton
    fun provideMoshiConvertorFactory(): MoshiConverterFactory {
        return MoshiConverterFactory.create()
    }

    //    retrofit
    @Provides
    @Singleton
    fun provideRetrofitApi(
        httpLoggingInterceptor: Call.Factory,
        moshiConverterFactory: MoshiConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(NetworkEndpoints.TEST_BASE_URL)
            .callFactory(httpLoggingInterceptor)
            .addConverterFactory(moshiConverterFactory)
            .build()
    }

    @Provides
    @Singleton
    fun provideApiServices(retrofit: Retrofit): ApiHelper{
        return retrofit.create(ApiHelper::class.java)
    }
}
