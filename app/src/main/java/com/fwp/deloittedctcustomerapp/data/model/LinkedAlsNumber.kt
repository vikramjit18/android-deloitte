/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model


data class LinkedAlsNumber(val alsNumber: String, val id: Int, var isLinked: Boolean = false)
