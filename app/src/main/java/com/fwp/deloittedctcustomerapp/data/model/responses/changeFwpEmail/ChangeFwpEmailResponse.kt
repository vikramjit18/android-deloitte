/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.changeFwpEmail


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChangeFwpEmailResponse(
    @Json(name = "resObject")
    var resObject: List<ResObject>?,
    @Json(name = "responseCode")
    var responseCode: String?,
    @Json(name = "responseMessage")
    var responseMessage: String?
)
