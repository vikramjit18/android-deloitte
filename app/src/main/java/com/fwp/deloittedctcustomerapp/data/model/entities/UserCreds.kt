/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.entities


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserCreds(
    @Json(name = "emailids")
    val emailids: String? = null,
    @Json(name = "firstName")
    val firstName: String? = null,
    @Json(name = "lastName")
    val lastName: String? = null,
    @Json(name = "middleName")
    val middleName: String? = null,
    @Json(name = "password")
    val password: String? = null,
    @Json(name = "username")
    val username: String? = null,
)
