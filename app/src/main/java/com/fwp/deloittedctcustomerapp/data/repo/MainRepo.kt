/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.repo

import androidx.lifecycle.LiveData
import com.fwp.deloittedctcustomerapp.data.api.ApiHelper
import com.fwp.deloittedctcustomerapp.data.db.AppDao
import com.fwp.deloittedctcustomerapp.data.model.requests.*
import com.fwp.deloittedctcustomerapp.data.model.responses.agreement.AgreemnetResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.changeFwpEmail.ChangeFwpEmailResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ChangePasswordResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse


import com.fwp.deloittedctcustomerapp.data.model.responses.masterData.MasterData
import com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.UserStatusResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.validateEtag.ValidateEtagResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRepo @Inject constructor(private val apiHelper: ApiHelper, private val appDao: AppDao) {


      fun getValiRecords(): LiveData<List<ValidateEtagRequest>> {
        return appDao.getValidateRequest()
    }

    suspend  fun insertValiTagRecord(validateEtagRequest: ValidateEtagRequest) {
        appDao.insertValidateRequest(validateEtagRequest)
    }

    suspend fun createNewUser(signupRequest: SignupRequest): Response<CheckCredsResponse> {
        return apiHelper.createNewUser(signupRequest)
    }

    suspend fun login(loginRequest: LoginRequest): Response<LoginResponse> {
        return apiHelper.login(loginRequest)
    }

    suspend fun acceptTOU(token: String): Response<AgreemnetResponse> {
        return apiHelper.getUserAgreement(token)
    }

    suspend fun forgotPassword(forgotPasswordRequest: ForgotPasswordRequest): Response<ForgotResponse> {
        return apiHelper.forgetPassword(forgotPasswordRequest)
    }

    suspend fun resetPassword(resetPasswordRequest: ResetPasswordRequest): Response<ForgotResponse> {
        return apiHelper.resetPassword(resetPasswordRequest)
    }

    suspend fun checkExistingUsername(checkUsernameRequest: CheckUsernameRequest): Response<CheckCredsResponse> {
        return apiHelper.checkExistingUsername(checkUsernameRequest)
    }

    suspend fun checkExistingEmail(checkEmailRequest: CheckEmailRequest): Response<CheckCredsResponse> {
        return apiHelper.checkExistingEmail(checkEmailRequest)
    }

    suspend fun changePassword(
        jwtToken: String,
        changePasswordRequest: ChangePasswordRequest
    ): Response<ChangePasswordResponse> {
        return apiHelper.changePassword(jwtToken, changePasswordRequest)
    }

    suspend fun changeFwpEmail(
        jwtToken: String,
        changeFwpEmailRequest: ChangeFwpEmailRequest
    ): Response<ChangeFwpEmailResponse> {
        return apiHelper.changeFWPEmail(jwtToken, changeFwpEmailRequest)
    }

    suspend fun requestEmailCopyPrimary(
        jwtToken: String,
        requestCopyPrimaryRequest: RequestCopyPrimaryRequest
    ): Response<RequestEmailResponse> {
        return apiHelper.requestPrimaryEmailCopies(jwtToken, requestCopyPrimaryRequest)
    }
    suspend fun requestEmailCopySecondary(
        jwtToken: String,
        requestCopySecondaryRequest: RequestCopySecondaryRequest
    ): Response<RequestEmailResponse> {
        return apiHelper.requestSecondaryEmailCopies(jwtToken, requestCopySecondaryRequest)
    }


    suspend fun getViewUserProfileDetails(jwtToken: String): Response<UserProfileResponse> {
        return apiHelper.getUserProfile(jwtToken)
    }

    suspend fun checkUserStatus(
        jwtToken: String
    ): Response<UserStatusResponse> {
        return apiHelper.checkUserStatus(jwtToken)
    }

    suspend fun getMasterData(): Response<MasterData> {
        return apiHelper.getMasterData()
    }

    suspend fun updateUserProfile(
        token: String,
        updateUserProfileRequest: UpdateUserProfileRequest
    ): Response<UserProfileResponse> {
        return apiHelper.updateUserProfile(token, updateUserProfileRequest)
    }

    suspend fun linkAls(
        jwtToken: String,
        alsLinkingRequest: AlsLinkingRequest
    ): Response<AlsResponse> {
        return apiHelper.postAlsLinking(token = jwtToken, alsLinkingRequest = alsLinkingRequest)
    }

    suspend fun lookUpAls(
        jwtToken: String,
        alsLookupRequest: AlsLookupRequest
    ): Response<AlsResponse> {
        return apiHelper.postAlsLookUp(token = jwtToken, alsLookupRequest = alsLookupRequest)

    }

    suspend fun getCityNameFromZipcode(
        zipcode: Int,
        isResident: String
    ): Response<ZipcodeResponse> {
        return apiHelper.getZipCode(zipcode, isResident)
    }

    suspend fun getEtags(etagRequest: EtagRequest): Response<EtagResponse> {
        val jwt = SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString()
        return apiHelper.getEtags(token = jwt, etagRequest = etagRequest)
    }

    suspend fun validateEtag(validateEtagRequest: ValidateEtagRequest): Response<ValidateEtagResponse> {
        val jwt = SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString()
        return apiHelper.validateEtags(token = jwt, validateEtagRequest)
    }

    suspend fun downloadOffLoadEtag(downloadOffloadRequest: DownloadOffloadRequest): Response<DownloadOffloadResponse> {
        return apiHelper.getDownloadOffLoadTags(downloadOffloadRequest)
    }

    suspend fun getItemHeld(jwtToken: String): Response<ItemsHeldResponse> {
        return apiHelper.getItemsHeld(jwtToken)

    }
}
