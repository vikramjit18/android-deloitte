/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses

data class Etag(val year: String, val list: EtagListDetail)

data class EtagListDetail(
    val year: String,
    val animalName: String,
    val region: String,
    val validateStatus: String,
    val downloadStatus: String,
    val imageUrl: String,
    val owner: String,
    val accountType: String,
    val spiid :String?,
    val isEtagActive :String?,
    val tagStatus: List<String>?
)
