/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.requests
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AlsLinkingRequest(
    @Json(name = "alsDob")
    val alsDob: String,
    @Json(name = "alsNo")
    val alsNo: Int,
    @Json(name = "alsZipCode")
    val alsZipCode: String
)

// /alslinking
