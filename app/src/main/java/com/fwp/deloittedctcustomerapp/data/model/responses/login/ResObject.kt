/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.login


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResObject(
    @Json(name = "alsLinked")
    val alsLinked: Boolean?,
    @Json(name = "fwpEmail")
    val fwpEmail: String?,
    @Json(name = "jwt")
    val jwt: String?,
    @Json(name = "logoutPeriod")
    val logoutPeriod: Int?,
    @Json(name = "name")
    val name: Name?,
    @Json(name = "touAccepted")
    val touAccepted: Boolean?,
    @Json(name = "userName")
    val userName: String?
)
