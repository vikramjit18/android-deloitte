/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NonCarcassTagLicensesX(
    @Json(name = "issuedDate")
    var issuedDate: String?,
    @Json(name = "licenseName")
    var licenseName: String?,
    @Json(name = "oppCode")
    var oppCode: String?
)
