/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.profile


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class P(
    @Json(name = "alsEmail")
    val alsEmail: String?,
    @Json(name = "alsNo")
    val alsNo: Int?,
    @Json(name = "clubMember500")
    val clubMember500: String?,
    @Json(name = "dob")
    val dob: String?,
    @Json(name = "eyeColorCd")
    val eyeColorCd: String?,
    @Json(name = "firstNm")
    val firstNm: String?,
    @Json(name = "fwpEmailId")
    val fwpEmailId: String?,
    @Json(name = "hairColorCd")
    val hairColorCd: String?,
    @Json(name = "heightFeet")
    val heightFeet: Int?,
    @Json(name = "heightInches")
    val heightInches: Int?,
    @Json(name = "homePhone")
    val homePhone: String?,
    @Json(name = "lastNm")
    val lastNm: String?,
    @Json(name = "mailingAddrLine")
    val mailingAddrLine: String?,
    @Json(name = "mailingCity")
    val mailingCity: String?,
    @Json(name = "mailingCountry")
    val mailingCountry: String?,
    @Json(name = "mailingState")
    val mailingState: String?,
    @Json(name = "mailingZipCd")
    val mailingZipCd: String?,
    @Json(name = "midInit")
    val midInit: String?,
    @Json(name = "residencyStatus")
    var residencyStatus: String?,
    @Json(name = "residentialAddrLine")
    val residentialAddrLine: String?,
    @Json(name = "residentialCity")
    val residentialCity: String?,
    @Json(name = "residentialCountry")
    val residentialCountry: String?,
    @Json(name = "residentialState")
    val residentialState: String?,
    @Json(name = "residentialZipCd")
    val residentialZipCd: String?,
    @Json(name = "sexCd")
    val sexCd: String?,
    @Json(name = "suffix")
    val suffix: String?,
    @Json(name = "userName")
    val userName: String?,
    @Json(name = "weight")
    val weight: Int?,
    @Json(name = "workPhone")
    val workPhone: String?
)
