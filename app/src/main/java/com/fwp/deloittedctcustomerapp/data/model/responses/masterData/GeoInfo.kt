/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.masterData


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GeoInfo(
    @Json(name = "countries")
    val countries: Map<String, String>,
    @Json(name = "states")
    val states: Map<String, Map<String, String>>
)
