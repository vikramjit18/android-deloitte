/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.requests


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RequestCopySecondaryRequest(
    @Json(name = "alsNo")
    val alsNo: Int?,
    @Json(name = "dob")
    val dob: String?,
    @Json(name = "requestEmailIdList")
    val requestEmailIdList: List<String>?
)
