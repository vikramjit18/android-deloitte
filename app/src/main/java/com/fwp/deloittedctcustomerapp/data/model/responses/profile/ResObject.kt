/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.profile


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResObject(
    @Json(name = "P")
    val p: P?,
    @Json(name = "S")
    val s: List<P>?,
    @Json(name = "message")
    val message: List<String>?
)
