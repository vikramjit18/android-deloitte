/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PermitData(
    @Json(name = "alsNo")
    var alsNo: String?,
    @Json(name = "designatedArea")
    var designatedArea: String?,
    @Json(name = "issuedDate")
    var issuedDate: String?,
    @Json(name = "owner")
    var owner: String?,
    @Json(name = "permit")
    var permit: String?,
    @Json(name = "regulationsSection1")
    var regulationsSection1: List<String>?,
    @Json(name = "regulationsSection2")
    var regulationsSection2: List<String>?,
    @Json(name = "speciesDescription")
    var speciesDescription: String?,
    @Json(name = "speciesName")
    var speciesName: String?
)
