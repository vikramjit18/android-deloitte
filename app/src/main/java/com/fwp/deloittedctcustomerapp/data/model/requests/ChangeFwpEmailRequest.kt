/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.requests
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChangeFwpEmailRequest(
    @Json(name = "myFwpEmail")
    val myFwpEmail: String,
    @Json(name = "password")
    val password: String,
    @Json(name = "userName")
    val userName: String
)

//  /users/changepassword
