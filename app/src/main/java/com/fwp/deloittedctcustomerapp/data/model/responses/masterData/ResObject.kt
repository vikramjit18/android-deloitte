/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.masterData


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResObject(
    @Json(name = "EYE")
    val eYE: Map<Int,String>,
    @Json(name = "GeoInfo")
    val geoInfo: GeoInfo,
    @Json(name = "HAIR")
    val hAIR: Map<Int,String>,
    @Json(name = "sexCd")
    val sexCd: Map<String,String>,
    @Json(name = "suffix")
    val suffix: List<String>,
    @Json(name = "heightData")
    val height : Height,
    @Json(name = "weightData")
    val weight : Weight
)
