/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.etag


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EtagsubDetails(
    @Json(name = "alsNumber")
    val alsNumber: String?,
    @Json(name = "backgroundImageUrl")
    val backgroundImageUrl: String?,
    @Json(name = "description")
    val description: String?,
    @Json(name = "downloads")
    val downloads: String?,
    @Json(name = "eTagMandatoryDetails")
    val eTagMandatoryDetails: List<ETagMandatoryDetail>?,
    @Json(name = "etagConfirmationNumber")
    val etagConfirmationNumber: String?,
    @Json(name = "harvestTimeStamp")
    val harvestTimeStamp: String?,
    @Json(name = "imageUrlIcon")
    val imageUrlIcon: String?,
    @Json(name = "issuedDate")
    val issuedDate: String?,
    @Json(name = "licenseName")
    val licenseName: String?,
    @Json(name = "oppurtunity")
    val oppurtunity: String?,
    @Json(name = "owner")
    val owner: String?,
    @Json(name = "regulationsText")
    val regulationsText: List<String>?,
    @Json(name = "regulationsUrl")
    val regulationsUrl: String?,
    @Json(name = "sponsorDetails")
    val sponsorDetails: List<Any>?,
    @Json(name = "status")
    val status: List<String>?,
    @Json(name = "subtitle")
    val subtitle: String?
)
