/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.als


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AlsResponse(
    @Json(name = "resObject")
    val resObject: List<ResObject>,
    @Json(name = "responseCode")
    val responseCode: String,
    @Json(name = "responseMessage")
    val responseMessage: String
)
