/*
 * Montana Fish, Wildlife & Parks
 */
package com.fwp.deloittedctcustomerapp.data.model.entities
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

// FWP
// Modals
// for
// Entities
@JsonClass(generateAdapter = true)
data class User(
    @Json(name = "emailId")
    val emailId: String?,
    @Json(name = "firstName")
    val firstName: String?,
    @Json(name = "lastName")
    val lastName: String?,
    @Json(name = "middleName")
    val middleName: String?,
    @Json(name = "password")
    val password: String?,
    @Json(name = "username")
    val username: String?,
    @Json(name = "uid")
    val uid: String?,
    @Json(name = "confirmNewFwpEmail")
    val confirmNewFwpEmail: String?,
    @Json(name = "newFwpEmail")
    val newFwpEmail: String?,
    @Json(name = "confirmNewPassword")
    val confirmNewPassword: String?,
    @Json(name = "currentPassword")
    val currentPassword: String?,
    @Json(name = "newPassword")
    val newPassword: String?,

// this is user profile entities
    @Json(name = "addrLine1")
    val addrLine1: String?,
    @Json(name = "addrLine2")
    val addrLine2: String?,
    @Json(name = "addrType")
    val addrType: String?,
    @Json(name = "alsNo")
    val alsNo: String?,
    @Json(name = "city")
    val city: String?,
    @Json(name = "clubMember500")
    val clubMember500: String?,
    @Json(name = "contactViaEmailId")
    val contactViaEmailId: String?,
    @Json(name = "country")
    val country: String?,
    @Json(name = "dob")
    val dob: String?,
    @Json(name = "email")
    val email: String?,
    @Json(name = "eyeColorCd")
    val eyeColorCd: String?,
    @Json(name = "firstNm")
    val firstNm: String?,
    @Json(name = "hairColorCd")
    val hairColorCd: String?,
    @Json(name = "heightFeet")
    val heightFeet: String?,
    @Json(name = "heightInches")
    val heightInches: String?,
    @Json(name = "homePhone")
    val homePhone: String?,
    @Json(name = "lastNm")
    val lastNm: String?,
    @Json(name = "midInit")
    val midInit: String?,
    @Json(name = "poBoxInd")
    val poBoxInd: String?,
    @Json(name = "residencyStatus")
    val residencyStatus: String?,
    @Json(name = "sexCd")
    val sexCd: String?,
    @Json(name = "state")
    val state: String?,
    @Json(name = "suffix")
    val suffix: String?,
    @Json(name = "weight")
    val weight: String?,
    @Json(name = "workPhone")
    val workPhone: String?,
    @Json(name = "zipCd")
    val zipCd: String?
)
