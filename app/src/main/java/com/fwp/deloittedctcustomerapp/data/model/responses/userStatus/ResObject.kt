/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.userStatus

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResObject(
    @Json(name = "accountActivated")
    val accountActivated: Boolean,
    @Json(name = "alsActivated")
    val alsActivated: Boolean,
    @Json(name = "alsLinked")
    val alsLinked: Boolean,
    @Json(name = "touAccepted")
    val touAccepted: Boolean,
    @Json(name = "message")
    val message: List<String>?
)
