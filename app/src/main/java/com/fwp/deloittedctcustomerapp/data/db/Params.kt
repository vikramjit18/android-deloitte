/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.db

object Params {
    const val DBNAME ="dct_db"
    const val DBVERSION = 1
    const val VALIREQUESTTABLENAME ="validate_request"
    const val DOWNLOADED_TAGS_TABLE_NAME ="downloaded_tags"
}
