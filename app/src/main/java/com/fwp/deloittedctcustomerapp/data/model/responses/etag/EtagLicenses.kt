/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.etag


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EtagLicenses(
    @Json(name = "etagLicenseDetails")
    val etagLicenseDetails: EtagLicenseDetails?,
    @Json(name = "licenseName")
    val licenseName: String?
)
