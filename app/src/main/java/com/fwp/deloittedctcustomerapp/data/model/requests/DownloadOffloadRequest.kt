/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.requests


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DownloadOffloadRequest(
    @Json(name = "device_uid")
    val deviceUid: String?,
    @Json(name = "service")
    val service: String?,
    @Json(name = "spi_id")
    val spiId: List<String>?
)

/**
if
service is
download then D
or
Offload then O
 **/
