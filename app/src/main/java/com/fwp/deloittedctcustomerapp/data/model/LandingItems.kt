/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model

data class LandingItems(val itemName: String, val id: Int)
