/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.etag


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResObject(
    @Json(name = "etagAccountsList")
    val etagAccountsList: List<EtagAccounts>?,
    @Json(name = "message")
    val message: List<String>?
)
