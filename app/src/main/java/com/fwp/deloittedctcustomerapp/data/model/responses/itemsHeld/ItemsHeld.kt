/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ItemsHeld(
    @Json(name = "nonCarcassTagLicenses")
    var nonCarcassTagLicenses: NonCarcassTagLicenses?,
    @Json(name = "permits")
    var permits: Permits?,
    @Json(name = "year")
    var year: String?
)
