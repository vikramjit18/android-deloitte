/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses


// FWP
// Modal
// class for
// Etags

// Parents
// modal
class EtagModel(val year: String, val list: MutableList<EtagDetail>)

// Child
// modal

class EtagDetail(
    specie: String?,
    region: String?,
    year: String?,
    isValidated: Boolean?,
    etagDownload: EtagDownload
) {

    // variables
    private var specie: String
    private var region: String
    private var year: String
    private var isValidated: Boolean


    fun getValidation(): Boolean? {
        return isValidated
    }

    fun setValidation(value: Boolean?) {
        this.isValidated = value!!
    }

    // Initiate constructor
    init {
        this.specie = specie!!
        this.region = region!!
        this.year = year!!
        this.isValidated = isValidated!!
    }

    fun getSpecie(): String? {
        return specie
    }

    fun setSpecie(specie: String?) {
        this.specie = specie!!
    }

    fun getYear(): String? {
        return year
    }

    fun setYear(year: String?) {
        this.year = year!!
    }

    fun getRegion(): String? {
        return region
    }

    fun setRegion(region: String?) {
        this.region = region!!
    }

}

// Modal
// class for
// etags downloads
class EtagDownload(
    isProgress: Boolean?,
    isError: Boolean?,
    isSuccess: Boolean?,
) {
    // variables
    private var isProgress: Boolean
    private var isError: Boolean
    private var isSuccess: Boolean

    // Initiate constructor
    init {
        this.isProgress = isProgress!!
        this.isError = isError!!
        this.isSuccess = isSuccess!!
    }

    // Getter and setter
    fun getProgress(): Boolean? {
        return isProgress
    }

    fun setProgress(value: Boolean?) {
        this.isProgress = value!!
    }

    fun getError(): Boolean? {
        return isError
    }

    fun setError(value: Boolean?) {
        this.isError = value!!
    }

    fun getSuccess(): Boolean? {
        return isSuccess
    }

    fun setSuccess(value: Boolean?) {
        this.isSuccess = value!!
    }

    // End of
    // etags modal
}




