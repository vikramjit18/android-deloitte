/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.etag


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EtagLicenseDetails(
    @Json(name = "districtCode")
    val districtCode: String?,
    @Json(name = "districtSpeciesCode")
    val districtSpeciesCode: String?,
    @Json(name = "downloadStatus")
    val downloadStatus: String?,
    @Json(name = "etagsubDetails")
    val etagsubDetails: EtagsubDetails?,
    @Json(name = "imageUrl")
    val imageUrl: String?,
    @Json(name = "isEtagActive")
    val isEtagActive: String?,
    @Json(name = "spiId")
    val spiId: String?,
    @Json(name = "validatedStatus")
    val validatedStatus: String?
)
