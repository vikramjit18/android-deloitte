/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NonCarcassTagLicenses(
    @Json(name = "alsNo")
    var alsNo: String?,
    @Json(name = "licenseYear")
    var licenseYear: String?,
    @Json(name = "nonCarcassTagLicensesList")
    var nonCarcassTagLicensesList: List<NonCarcassTagLicensesX>?,
    @Json(name = "owner")
    var owner: String?,
    @Json(name = "session")
    var session: String?,
    @Json(name = "usageDates")
    var usageDates: String?
)
