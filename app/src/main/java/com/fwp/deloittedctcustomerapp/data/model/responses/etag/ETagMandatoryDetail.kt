/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.data.model.responses.etag


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ETagMandatoryDetail(
    @Json(name = "reReportingHTMLText")
    val reReportingHTMLText: String?,
    @Json(name = "rsReportingIconUrl")
    val rsReportingIconUrl: String?,
    @Json(name = "rsReportingStep")
    val rsReportingStep: String?
)
