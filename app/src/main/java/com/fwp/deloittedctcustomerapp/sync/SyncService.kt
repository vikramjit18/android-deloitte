/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.sync

import android.content.Context
import android.content.Intent
import androidx.core.app.JobIntentService

class SyncService : JobIntentService() {

    override fun onHandleWork(intent: Intent) {
        TODO("Not yet implemented")


    }


    companion object {
        var syncService: SyncService? = null
        fun enqueWork(context: Context) {
            val intent = Intent(context, SyncService::class.java)
            enqueueWork(context, SyncService::class.java, 123, intent)
        }

        @JvmStatic
        val instance: SyncService?
            get() {
                if (syncService == null) syncService = SyncService()
                return syncService
            }
    }

    override fun onDestroy() {
        super.onDestroy()
    }


}
