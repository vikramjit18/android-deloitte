/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.signup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.SignupRequest
import com.fwp.deloittedctcustomerapp.databinding.FragmentSignUpAgreementBinding
import com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException

@AndroidEntryPoint
class SignUpAgreement : Fragment() {

    //init binding
    lateinit var binding: FragmentSignUpAgreementBinding

    //init fragment view model
    private val model: SignUpViewModel by viewModels()

    //init communicator to pass data from one fragment to another
    private lateinit var viewModelCommunicator: Communicator

    //loadin init
    private lateinit var loadingDialog: LoadingDialog

    //on create view lifecyle
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up_agreement, container, false)
//init viewmodel
        viewModelCommunicator = ViewModelProvider(requireActivity())[Communicator::class.java]
//init loader
        loadingDialog = LoadingDialog(requireActivity())

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //init fun
        initButtonProperties()
        //init fun
        buttonHandler()
        //returning view
    }

    // xml button handler
    private fun buttonHandler() {
        var username = ""
        var password = ""
        var email = ""
        var firstName = ""
        var midName = ""
        var lastName = ""


        viewModelCommunicator.email.observe(requireActivity(),
            { o -> email = o!!.toString() })

        viewModelCommunicator.password.observe(requireActivity(),
            { o -> password = o!!.toString() })
        viewModelCommunicator.username.observe(requireActivity(),
            { o -> username = o!!.toString() })

        viewModelCommunicator.fn.observe(requireActivity(),
            { o -> firstName = o!!.toString() })
        viewModelCommunicator.mn.observe(requireActivity(),
            { o -> midName = o!!.toString() })

        viewModelCommunicator.ln.observe(requireActivity(),
            { o -> lastName = o!!.toString() })

/*        UtilsMethods.toastMaker(requireContext(), "        firstName = $firstName ,\n" +
            "                        lastName = $lastName,\n" +
            "                        middleName = $midName,\n" +
            "                        password = $password,\n" +
            "                        username = $username,\n" +
            "                        emailId = $email")*/

        //agree button
        binding.view.agree.btn.setOnClickListener {

            //set the message to share to another fragment
            if (UtilsMethods.isInternetConnected(requireContext())) {
                //init fun

                loadingDialog.startLoading()

                /*      viewModelCommunicator.userCreds.observe(
                          requireActivity(), {
                              if (it != null) {
                                  val signupRequest = SignupRequest(
                                      firstName = it.firstName!!,
                                      lastName = it.lastName!!,
                                      middleName = it.middleName!!,
                                      password = it.password!!,
                                      username = it.username!!,
                                      emailId = it.emailId!!
                                  )
                                  UtilsMethods.toastMaker(requireContext(), "$signupRequest")
                                  try {
                                      model.sendNewUserData(
                                          signupRequest
                                      )
                                  } catch (e: Exception) {
                                      e.printStackTrace()
                                  } catch (e2: SocketTimeoutException) {
                                      e2.printStackTrace()
                                  }

                              }

                          }
                      )*/


                try {
                    model.sendNewUserData(
                        SignupRequest(
                            firstName = firstName,
                            lastName = lastName,
                            middleName = midName,
                            password = password,
                            username = username,
                            emailId = email
                        )
                    )
                } catch (e: Exception) {
                    loadingDialog.isDismiss()
                    e.printStackTrace()
                } catch (e2: SocketTimeoutException) {
                    loadingDialog.isDismiss()
                    e2.printStackTrace()
                }catch (e3: SSLException){
                    loadingDialog.isDismiss()
                    e3.printStackTrace()
                }
                viewModelObser()

            } else {
                UtilsMethods.showSimpleAlert(
                    mContext = requireContext(),
                    tvPrimary = getString(R.string.no_connection),
                    tvSecondary = getString(R.string.check_and_try_again),
                    primaryBtn = resources.getString(R.string.ok)

                )
            }

        }


        //back button
        binding.view.ivBack.setOnClickListener {
            requireActivity().finish()
            requireActivity().overridePendingTransition(R.anim.slide_bottom_to_top, R.anim.slide_top_to_bottom)

        }
    }


    //view model response observer to handle fragment state
    private fun viewModelObser() {
        model.newUserSignupResponse.observe(
            viewLifecycleOwner
        ) {
            loadingDialog.isDismiss()
            if (it.responseMessage.contains("invalid")) {
//                UtilsMethods.toastMaker(requireContext(), "${it.resObject[0].message[0]}")
            } else {
                UtilsMethods.navigateToFragment(binding.root, R.id.action_signUpAgreement_to_signUpActivate)
            }
        }
    }

    // init buton property
    private fun initButtonProperties() {
        binding.view.agree.btn.text = resources.getString(R.string.agree_login_continue)
        binding.view.agree.btn.setTextColor(resources.getColor(R.color.black, null))
        binding.view.agree.btn.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
    }

}


