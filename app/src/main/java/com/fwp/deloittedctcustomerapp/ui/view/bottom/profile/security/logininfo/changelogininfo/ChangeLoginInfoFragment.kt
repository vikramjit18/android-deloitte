/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo

import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.ChangeFwpEmailRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest
import com.fwp.deloittedctcustomerapp.databinding.ChangeLoginInfoFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginActivity
import com.fwp.deloittedctcustomerapp.utils.*
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException


/**
 * FWP
 * Hilt entry point
 */
@AndroidEntryPoint
class ChangeLoginInfoFragment : Fragment() {

    companion object {
        fun newInstance() = ChangeLoginInfoFragment()
    }

    // Global Variables
    private var _binding: ChangeLoginInfoFragmentBinding? = null
    private val binding get() = _binding!!
    private val changeLoginInfoViewModel: ChangeLoginInfoViewModel by viewModels()
    private val listOffLoad = mutableListOf<String>()
    private lateinit var loadingDialog: LoadingDialog

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_bottom_to_top)
        } else {
            AnimationUtils.loadAnimation(context, R.anim.slide_top_to_bottom)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        // Binding Initiate
        _binding = ChangeLoginInfoFragmentBinding.inflate(inflater, container, false)

        // laoding

        if (!UtilsMethods.isInternetConnected(requireContext())) {
            UtilsMethods.showSimpleAlert(
                mContext = requireContext(),
                tvPrimary = resources.getString(R.string.device_currently_offline),
                tvSecondary = "You must have a network connection to update your email.",
                primaryBtn = resources.getString(R.string.ok)
            )
        }

        checkLiveNetworkConnection()
        loadingDialog = LoadingDialog(requireActivity())

        // Returning root binding
        return binding.root
    }

    private fun checkLiveNetworkConnection() {
        val connectivityManager =
            requireActivity().getSystemService(ConnectivityManager::class.java)
        val liveConnection = ConnectionLiveData(connectivityManager)

        liveConnection.observe(viewLifecycleOwner) { isConnected ->
            if (!isConnected) {
                UtilsMethods.showSimpleAlert(
                    mContext = requireContext(),
                    tvPrimary = getString(R.string.device_currently_offline),
                    tvSecondary = "You must have a network connection to update your email.",
                    primaryBtn = resources.getString(R.string.ok)
                )
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Init Process
        init()
        /**
         * Observing
         * changePassword Response
         */
        listOffLoad.clear()

        changeLoginInfoViewModel.getDownloadedTagSid().observe(requireActivity()) {
            it.forEach { data ->
                data.downloadedSpiId?.let { it1 -> listOffLoad.add(it1) }
            }
        }
        viewModelObser()
    }

    private fun viewModelObser() {
        changeLoginInfoViewModel.changeFwpEmailResponse.observe(
            requireActivity()
        ) { changeFwpResponse ->
            //dissmis loading
            loadingDialog.isDismiss()
            when (changeFwpResponse.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
//                    UtilsMethods.toastMaker(
//                        requireContext(),
//                        changeFwpResponse.resObject?.get(0)?.message.toString().substring(
//                            1,
//                            changeFwpResponse.resObject?.get(0)?.message.toString().length - 1
//                        )
//                    )
                    if (listOffLoad.isNotEmpty()) {
                        loadingDialog.startLoading()
                        val offloadRequest = DownloadOffloadRequest(
                            deviceUid = UtilConstant.DEVICE_ID,
                            service = UtilConstant.OFFLOAD,
                            spiId = listOffLoad.toSet().toList()
                        )
                        offLoadApi(offloadRequest)
                        offloadObsr()

                    } else {
                        UtilConstant.itemLastIndex = 0
                        UtilConstant.etagLastIndex = 0
                        UtilConstant.ITEMS_LAST_VIEW_INDEX = 0
                        UtilConstant.ACCOUNT_VIEWPAGER = 0
                        SharedPrefs.clearSharedPref()
                        changeLoginInfoViewModel.cleardb()
                        val i = Intent(activity, LoginActivity::class.java)
                        requireActivity().startActivity(i)
                        requireActivity().finish()
                    }
//                        UtilsMethods.backStack(findNavController())

                }

                UtilConstant.FAIL_CODE -> {
//                    UtilsMethods.toastMaker(
//                        requireContext(),
//                        changeFwpResponse.resObject?.get(0)?.message.toString().substring(
//                            1,
//                            changeFwpResponse.resObject?.get(0)?.message.toString().length - 1
//                        )
//                    )
                }
            }


        }
    }

    private fun offLoadApi(offloadRequest: DownloadOffloadRequest) {
        try {
            changeLoginInfoViewModel.offLoadRequest(offloadRequest)

        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (e2: SocketTimeoutException) {
            loadingDialog.isDismiss()
            e2.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        }
    }

    private fun init() {

        // Setting
        // primary button
        val btnUpdate = binding.updateLogin.btn
        btnUpdate.text = resources.getString(R.string.updateLoginButtonText)
        btnUpdate.setTextColor(resources.getColor(R.color.black, null))
        btnUpdate.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
        binding.tvUserName.text = SharedPrefs.read(UtilConstant.USERNAME_LOGIN, "")
        // Email Edittext Input Type
        binding.fwpEmail.editText.inputType =
            InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS or InputType.TYPE_CLASS_TEXT

        // Confirm Edittext Input Type
        binding.confirmEmail.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS or InputType.TYPE_CLASS_TEXT

        // TextWatch listener
        // for any changes
        binding.fwpEmail.editText.addTextChangedListener(mTextWatcher)
        binding.confirmEmail.addTextChangedListener(mTextWatcher)
        binding.pass.addTextChangedListener(mTextWatcher)

        // Global Functions
        buttonActivation()
        buttonHandler()
    }

    /**
     * All click
     * listener function
     */
    private fun buttonHandler() {

        // Back to previous page
        binding.close.setOnClickListener {
            UtilsMethods.backStack(findNavController())
        }

        // custom dialog box
        binding.ibAlertUserName.setOnClickListener {
            UtilsMethods.showSimpleAlert(
                requireContext(),
                resources.getString(R.string.username_cant_change),
                resources.getString(R.string.call_assistance),
                resources.getString(R.string.ok)
            )
        }
    }

    /**
     * function for
     * activate primary button
     */
    private fun buttonActivation() {
        val email = binding.fwpEmail.editText.text.toString()
        val emailCon = binding.confirmEmail.text.toString()
        val emailLCon = binding.confirmFwpEmail
        when (UtilConstant.EMAIL_ADDRESS_PATTERN.matcher(binding.fwpEmail.editText.text.toString())
            .matches() && UtilConstant.EMAIL_ADDRESS_PATTERN.matcher(emailCon)
            .matches() && binding.fwpEmail.editText.text.toString() == emailCon && binding.pass.text.toString()
            .isNotEmpty()) {
            true -> {
                enablePrimaryBtn()
                updateEmailBtnHandler()
            }

            false -> {
                disablePrimaryBtn()

            }
        }


        when (email.trim() == emailCon.trim()) {
            true -> {
                emailLCon.isErrorEnabled = false
            }
            false -> {
                emailLCon.isErrorEnabled = true
                emailLCon.error = "Email addresses must match"

            }
        }
    }

    private fun updateEmailBtnHandler() {
        binding.updateLogin.btn.setOnClickListener {
            if (UtilsMethods.isInternetConnected(requireContext())) {

                // show loader
                //dissmis loading
                loadingDialog.startLoading()
                val token = SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString()

                try {
                    changeLoginInfoViewModel.changeFwpEmail(
                        token,
                        ChangeFwpEmailRequest(
                            myFwpEmail = binding.fwpEmail.editText.text.toString(),
                            password = binding.pass.text.toString(),
                            userName = SharedPrefs.read(UtilConstant.USERNAME_LOGIN, "")!!
                        )
                    )

                } catch (e: Exception) {
                    loadingDialog.isDismiss()
                    e.printStackTrace()
                } catch (e2: SocketTimeoutException) {
                    loadingDialog.isDismiss()
                    e2.printStackTrace()
                } catch (e3: SSLException) {
                    loadingDialog.isDismiss()
                    e3.printStackTrace()
                }


            } else {
                // showing No internet dialog
                UtilsMethods.showSimpleAlert(
                    mContext = requireContext(),
                    tvPrimary = getString(R.string.no_connection),
                    tvSecondary = getString(R.string.check_and_try_again),
                    primaryBtn = resources.getString(R.string.ok)
                )
            }
        }
    }

    /**
     *  create a textWatcher member
     *  */
    private val mTextWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun afterTextChanged(editable: Editable) {
            //needed for runtime validation
            buttonActivation()
        }
    }

    /**
     * function for the
     * disable primary btn
     */
    private fun disablePrimaryBtn() {
        val btnUpdate = binding.updateLogin.btn
        btnUpdate.isEnabled = false
        btnUpdate.isClickable = false
        btnUpdate.setTextColor(resources.getColor(R.color.white, null))
        btnUpdate.setBackgroundColor(resources.getColor(R.color.disableButtonColor, null))
    }

    /**
     * function for the
     * enabling primary btn
     */
    private fun enablePrimaryBtn() {
        val btnUpdate = binding.updateLogin.btn
        btnUpdate.isEnabled = true
        btnUpdate.isClickable = true
        btnUpdate.setTextColor(resources.getColor(R.color.black, null))
        btnUpdate.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
    }

    private fun offloadObsr() {
        changeLoginInfoViewModel.offloadResponse.observe(requireActivity()) {
            loadingDialog.isDismiss()
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    UtilConstant.itemLastIndex = 0
                    UtilConstant.etagLastIndex = 0
                    UtilConstant.ITEMS_LAST_VIEW_INDEX = 0
                    UtilConstant.ACCOUNT_VIEWPAGER = 0
                    SharedPrefs.clearSharedPref()
                    changeLoginInfoViewModel.cleardb()
                    changeLoginInfoViewModel.deleteDownloadedTagSid()
                    val i = Intent(activity, LoginActivity::class.java)
                    requireActivity().startActivity(i)
                    requireActivity().finish()

                }
                UtilConstant.FAIL_CODE -> {
                    it.resObject?.get(UtilConstant.CURRENT_INDEX)
                        ?.let { _ ->
//                            it1.message?.get(CURRENT_INDEX)?.let { it2 ->
//                                UtilsMethods.toastMaker(
//                                    requireContext(),
//                                    it2
//                                )
//                            }
                        }
                }
            }
        }
    }
}
