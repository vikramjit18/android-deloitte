/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.location


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.databinding.LocationFragmentBinding
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import android.content.Intent
import android.provider.Settings
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.viewModels
import com.fwp.deloittedctcustomerapp.R


class LocationFragment : Fragment() {

    companion object {
        fun newInstance() = LocationFragment()
    }

    private var _binding: LocationFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LocationFragmentBinding.inflate(inflater, container, false)
        val root = binding.root
        buttonHandler()
        return root
    }

    private fun buttonHandler() {

        binding.back.backLayout.setOnClickListener() {
            UtilsMethods.backStack(findNavController())
        }
        binding.gotoDeviceSetting.setOnClickListener() {
            val callGPSSettingIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            requireActivity().startActivity(callGPSSettingIntent)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
