/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.onboard

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.FragmentOnBoardBinding
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginActivity
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpActivity
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class OnBoard : Fragment() {
    lateinit var binding: FragmentOnBoardBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_on_board, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonProperty()
    }

    private fun buttonProperty() {

        binding.login.btn.text = resources.getString(R.string.loginText2)
        binding.login.btn.setTextColor(resources.getColor(R.color.black, null))
        binding.login.btn.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))

        binding.signup.btn.text = resources.getString(R.string.dontHaveAccSignUp)
        binding.signup.btn.setTextColor(resources.getColor(R.color.prim_yellow, null))
        binding.signup.btn.setBackgroundColor(resources.getColor(R.color.barColor, null))
        binding.signup.btn.typeface =
            ResourcesCompat.getFont(requireContext(), R.font.font_mont_semi_bold)

        binding.signup.btn.stateListAnimator = null
        binding.signup.btn.elevation = 0F

        buttonHandler()
    }

    private fun buttonHandler() {
        binding.login.btn.setOnClickListener {
            val i = Intent(requireActivity(), LoginActivity::class.java)
            requireActivity().startActivity(i)
            requireActivity().overridePendingTransition(
                R.anim.slide_bottom_to_top,
                R.anim.slide_top_to_bottom
            )
            requireActivity().finish()
        }

//        val crashButton =  binding.login.btn
//        crashButton.text = "Test Crash"
//        crashButton.setOnClickListener {
//            throw RuntimeException("Test Crash") // Force a crash
//        }

        binding.signup.btn.setOnClickListener {
            val i = Intent(requireActivity(), SignUpActivity::class.java)
            requireActivity().startActivity(i)
            requireActivity().overridePendingTransition(
                R.anim.slide_bottom_to_top,
                R.anim.slide_top_to_bottom
            )
        }

    }

}
