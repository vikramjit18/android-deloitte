/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.als.lookup

import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.RadioButton
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.AlsLookupRequest
import com.fwp.deloittedctcustomerapp.databinding.LookUpAlsFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.als.viewModel.AlsViewModel
import com.fwp.deloittedctcustomerapp.utils.*
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException
import android.widget.TextView
import android.widget.TextView.OnEditorActionListener
import androidx.compose.ui.text.android.InternalPlatformTextApi
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment


// FWP
// Hilt entry point
@AndroidEntryPoint
class LookUpAlsFragment : Fragment() {

    private var radioText: String = "R"

    // Global variables
    private var _binding: LookUpAlsFragmentBinding? = null
    private val binding get() = _binding!!
    private val pattern = Regex(UtilConstant.DATE_PATTERN)
    private lateinit var alsViewModel: AlsViewModel
    private lateinit var loadingDialog: LoadingDialog
    private var alsFailedCount = 0
    private var firstTime = true

    //    private val navHostFragment = requireActivity().supportFragmentManager.findFragmentById(R.id.als_linking_nav) as NavHostFragment
//    private val navALSController = navHostFragment.navController
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        // Data binding
        _binding = LookUpAlsFragmentBinding.inflate(inflater, container, false)

        loadingDialog = LoadingDialog(requireActivity())
        alsViewModel = ViewModelProvider(requireActivity())[AlsViewModel::class.java]
        val root: View = binding.root

        // Init Process
        init()



        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Obser
        if (firstTime)
            viewModelObser()
    }

    private fun viewModelObser() {

        alsViewModel.alsLookupResponse.observe(
            viewLifecycleOwner
        ) {
            loadingDialog.isDismiss()
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE ->
                    successObser()

                else ->
                    failedObser()

            }
        }
    }

    private fun failedObser() {
        alsFailedCount++

        if (alsFailedCount > 3) {
            UtilsMethods.showDialerAlsAlert(
                requireActivity(),
                requireContext(),
                getString(R.string.having_trouble_als),
                getString(R.string.als_finding_als_support),
                resources.getString(R.string.callFwpText),
                resources.getString(R.string.close)
            )
        } else {
            UtilsMethods.showSimpleAlert(
                requireContext(),
                getString(R.string.no_als_found),
                getString(R.string.no_als_found_support),
                resources.getString(R.string.ok)
            )
        }
    }

    private fun successObser() {
        UtilsMethods.navigateToFragment(
            binding.root,
            R.id.action_lookUpAlsFragment_to_confirmAlsFragment
        )
//                        navALSController.navigate( R.id.action_lookUpAlsFragment_to_confirmAlsFragment)
        /*     UtilsMethods.snackBarCustom(
                 binding.root,
                 "${it.resObject[0].message[0]}",
                 ContextCompat.getColor(binding.root.context, R.color.black),
                 ContextCompat.getColor(binding.root.context, R.color.white)
             )*/
        alsFailedCount = 0
        UtilConstant.snackbarShown = true
        firstTime = false
        /*             UtilsMethods.toastMaker(
                         requireActivity(),
                         " ${it.resObject[0].message[0]}"
                     )*/
    }

    private fun init() {
        // Setting
        // primary Button
        binding.linkAlsB.btn.text = getString(R.string.search)

        // Setting
        // disable btn
        disableButton()

        // Setting
        // Zip code (Edittext)
        binding.etZipcode.editText.filters += InputFilter.LengthFilter(5)
        binding.etZipcode.editText.inputType = InputType.TYPE_CLASS_NUMBER
        binding.etDob.inputType = InputType.TYPE_CLASS_NUMBER
        binding.etNumber.inputType = InputType.TYPE_CLASS_NUMBER

        // phone no edit text hide
        /*     binding.etNumber.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
                 var handled = false
                 if (actionId == EditorInfo.IME_ACTION_DONE) {
                     *//* Write your logic here that will be executed when user taps next button *//*
                handled = true
            }
            handled
        })*/


        binding.etNumber.setOnEditorActionListener { _, actionId, event ->
            if (event != null && event.keyCode === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                UtilsMethods.hideKeyBoard(binding.root, requireContext())
            }
            false
        }

        // Setting
        // clearing als edittext
//        binding.etNumber.text?.clear()
        binding.radioGroup.setOnCheckedChangeListener { group, checkedId ->
            val radioButton = group.findViewById<RadioButton>(checkedId)
            radioText = radioButton.text.toString()
            radioText = if (radioText.contains(resources.getString(R.string.resident))) {
                "R"
            } else {
                "N"
            }
        }

        // Setting
        // click listener
        buttonHandler()

        DateInputMethod(binding.etDob).listen()
        binding.etDob.addTextChangedListener(mTextWatcher)
        binding.etZipcode.editText.addTextChangedListener(mTextWatcher)
        binding.etNumber.addTextChangedListener(mTextWatcher)
    }

    private fun buttonHandler() {

        // Back to previous page
        binding.ivClose.backLayout.setOnClickListener {
            UtilsMethods.backStack(findNavController())
        }

        binding.tvHavingTrouble.setOnClickListener {
            showAlert()
        }
    }

    @OptIn(InternalPlatformTextApi::class)
    private fun showAlert() {

        val builder = AlertDialog.Builder(requireContext()).create()
        val view = layoutInflater.inflate(R.layout.custom_dialog, null)
        val leftButton = view.findViewById<TextView>(R.id.btn_left)
        val rightButton = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = "Having trouble finding your ALS number? "
        des.text =
            "For more support, call FWP support at 406-444-2950 during business hours (Monday – Friday, 8 a.m. – 5 p.m.). "
        leftButton.text = "Close"
        rightButton.text = getString(R.string.callFwpText)
        builder.setView(view)
        leftButton.setOnClickListener {
            builder.dismiss()
        }
        rightButton.setOnClickListener {

            UtilsMethods.dialerOpener(getString(R.string.helpPhoneNoUri), requireActivity())
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    // Activate primary button
    private fun buttonActivation() {
        val linkBtn = binding.linkAlsB.btn
        val dob: String = binding.etDob.text.toString()
        when (pattern.containsMatchIn(dob) && binding.etZipcode.editText.length() == 5 && !binding.etNumber.rawText.isNullOrEmpty() && binding.etNumber.rawText!!.length == 10) {
            true -> {
                enableButton()
                linkBtn.setOnClickListener {
                    initAlsLookup()
                }
            }
            false -> disableButton()

        }
    }

    private fun initAlsLookup() {
        if (UtilsMethods.isInternetConnected(requireContext())) {
            apiHits()
        } else {
            UtilsMethods.showSimpleAlert(
                mContext = requireContext(),
                tvPrimary = getString(R.string.no_connection),
                tvSecondary = getString(R.string.check_and_try_again),
                primaryBtn = resources.getString(R.string.ok)
            )
        }
    }

    private fun apiHits() {

        if (SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString().isNotEmpty()) {
            loadingDialog.startAlsSearchLoading()
            try {
                firstTime = true
                alsViewModel.alsLookUp(
                    SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString(), AlsLookupRequest(
                        dob = binding.etDob.text.toString().trim(),
                        zipCode = binding.etZipcode.editText.text.toString(),
                        phoneNo = binding.etNumber.rawText!!,
                        residentStatus = radioText
                    )
                )
            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (e2: SocketTimeoutException) {
                loadingDialog.isDismiss()
                e2.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }
//
        } else {
//            UtilsMethods.toastMaker(requireActivity(), "Jwt not found")
        }
    }

    private val mTextWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun afterTextChanged(editable: Editable) {
            buttonActivation()
        }
    }

    // disabling primary btn
    private fun disableButton() {
        binding.linkAlsB.btn.isClickable = false
        binding.linkAlsB.btn.setTextColor(resources.getColor(R.color.white, null))
        binding.linkAlsB.btn.setBackgroundColor(
            resources.getColor(
                R.color.disableButtonColor,
                null
            )
        )
    }

    // enabling primary btn
    private fun enableButton() {
        binding.linkAlsB.btn.isClickable = true
        binding.linkAlsB.btn.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
        binding.linkAlsB.btn.setTextColor(resources.getColor(R.color.black, null))
    }

}
