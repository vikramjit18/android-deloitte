/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter


import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.responses.Etag
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.ValidateTagsActivity
import com.fwp.deloittedctcustomerapp.utils.*
import com.google.android.material.button.MaterialButton


class EtagCurrentYearAdapter(
    private val list: List<Etag>,
    private val oflineSpiidList: List<String>
) :
    RecyclerView.Adapter<EtagCurrentYearAdapter.MyCurrentYearViewHolder>() {

    private lateinit var mListener: OnItemClickListener

    interface OnItemClickListener {
        fun onItemClick(position: Int, holder: MyCurrentYearViewHolder)
    }

    fun setOnItemClick(listener: OnItemClickListener) {
        mListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyCurrentYearViewHolder {

        val binding = LayoutInflater.from(parent.context)
            .inflate(R.layout.etags_available_child_row, parent, false)

        return MyCurrentYearViewHolder(binding, mListener)
    }


    class MyCurrentYearViewHolder(viewHolder: View, listener: OnItemClickListener) :
        RecyclerView.ViewHolder(viewHolder) {

        val itemContainer: ConstraintLayout = viewHolder.findViewById(R.id.relativeLayout)
        val tagImage: ImageView = viewHolder.findViewById(R.id.tagImage)
        val yearAnimalType: TextView = viewHolder.findViewById(R.id.year_animal_type)
        val region: TextView = viewHolder.findViewById(R.id.region)
        val etagContainer: FrameLayout = viewHolder.findViewById(R.id.etagStateContainer)
        val downloadBtnContainer: LinearLayout =
            viewHolder.findViewById(R.id.downloadButtonContainer)
        val etagValidate: View = viewHolder.findViewById(R.id.etagValidated)
        val etagVoid: View = viewHolder.findViewById(R.id.etag_void)
        val etagRefund: View = viewHolder.findViewById(R.id.etag_refunded)
        val etagExpire: View = viewHolder.findViewById(R.id.etag_expire)
        val iconDownloadBtn: MaterialButton = viewHolder.findViewById(R.id.iconDownloadButton)
        val iconProgressButton: CardView = viewHolder.findViewById(R.id.iconProgressButton)
        val validateBtn: Button = viewHolder.findViewById(R.id.validateButton)

        init {
            iconDownloadBtn.setOnClickListener {
                when (UtilsMethods.isInternetConnected(viewHolder.context)) {
                    true -> {
                        listener.onItemClick(adapterPosition, holder = MyCurrentYearViewHolder(viewHolder, listener))

                    }
                    false -> {
                        UtilsMethods.showSimpleAlert(
                            mContext = viewHolder.context,
                            tvPrimary = "No connection",
                            tvSecondary = "Check your network connection and try again.",
                            primaryBtn = "Ok"

                        )
                    }
                }
            }
        }
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyCurrentYearViewHolder, position: Int) {
        holder.validateBtn.setText(R.string.validate)
        val currentItem = list[position]
        holder.yearAnimalType.text =  "${currentItem.list.year} ${currentItem.list.animalName}"
        holder.region.text = currentItem.list.region


            // show download container when etageactive is Y
            when (currentItem.list.isEtagActive) {
                EtagActiveStatus.YES -> {
                    holder.downloadBtnContainer.visibility = View.VISIBLE
//                    holder.etagContainer.visibility = View.GONE
                    obserOfflineValidation(currentItem, holder)
                }
                EtagActiveStatus.NO -> {
                    holder.downloadBtnContainer.visibility = View.GONE
                    obserOfflineValidation(currentItem, holder)                }
            }




        //todo in new update   tag status
        if (currentItem.list.tagStatus!!.isNotEmpty()){
            currentItem.list.tagStatus!!.forEach{
                holder.etagContainer.visibility = View.VISIBLE
                when (it) {
                    TagStatus.VALIDATED -> {
                       holder.etagValidate.visibility = View.VISIBLE
                        holder.etagRefund.visibility = View.GONE
                    }
                    /*    TagStatus.ISSUE -> holder.viewDataBinding.etagIssue.stateContainer.visibility = View.VISIBLE
                        TagStatus.EXPIRED -> holder.viewDataBinding.etagExpire.stateContainer.visibility = View.VISIBLE*/
                    TagStatus.REFUNDED -> {
                        holder.etagRefund.visibility = View.VISIBLE
                        holder.etagValidate.visibility = View.GONE
                    }
//                    TagStatus.VOID -> holder.viewDataBinding.etagVoid.stateContainer.visibility = View.VISIBLE
                }
            }
        }

        // Download Status
        when (currentItem.list.downloadStatus) {
            DownloadStatus.SAME_DEVICE -> {
                holder.iconDownloadBtn.isClickable = false
                holder.iconDownloadBtn.setIconResource(R.drawable.ic_download_done)
                holder.iconDownloadBtn.setIconTintResource(R.color.white)
                holder.iconDownloadBtn.setStrokeColorResource(R.color.black)
                holder.iconDownloadBtn.setBackgroundColor(
                    holder.itemView.context.getColor(
                        R.color.black
                    )
                )
                enableValiBtn(holder, position)

            }

            DownloadStatus.OTHER_DEVICE ->{
                holder.iconDownloadBtn.isClickable = true
                holder.iconDownloadBtn.visibility = View.VISIBLE
                holder.iconDownloadBtn.setIconResource(R.drawable.ic_warning_yellow)
                holder.iconDownloadBtn.setIconTintResource(R.color.prim_yellow)
                /*        binding.downloaded.setBackgroundColor(
                            requireContext().getColor(
                                R.color.black
                            )
                        )*/
                disableValiBtn(holder, position)
            }



            DownloadStatus.NO_DOWNLOAD -> {
                holder.iconDownloadBtn.setIconResource(R.drawable.ic_dowload_yellow)
                holder.iconDownloadBtn.setIconTintResource(R.color.prim_yellow)
                holder.iconDownloadBtn.isCheckable = true

                disableValiBtn(holder, position)
            }

        }

        // animal image
        TagImageInflater().showListTagIcon(
            context = holder.itemView.context,
            imageView = holder.tagImage,
            heroName = currentItem.list.imageUrl
        )

        viewDetails(holder, position, currentItem.list.owner,currentItem.list.animalName,currentItem.list.region)

    }

    private fun obserOfflineValidation(
        currentItem: Etag,
        holder: MyCurrentYearViewHolder
    ) {
        when (oflineSpiidList.contains(currentItem.list.spiid)) {
            true -> {
                holder.downloadBtnContainer.visibility = View.GONE
                holder.etagContainer.visibility = View.VISIBLE
                holder.etagValidate.visibility = View.VISIBLE
            }
            false -> {
    //                holder.downloadBtnContainer.visibility = View.VISIBLE
                holder.etagContainer.visibility = View.GONE

            }
        }
    }

    private fun enableValiBtn(holder: MyCurrentYearViewHolder, position: Int) {
        val validatorButtonId = holder.validateBtn
        validatorButtonId.setText(R.string.validate)
        validatorButtonId.isEnabled = true
        validatorButtonId.isClickable = true
        validatorButtonId.setTextColor(holder.itemView.context.getColor(R.color.black))
        validatorButtonId.setBackgroundColor(
            holder.itemView.context.getColor(
                R.color.prim_yellow
            )
        )
    }

    private fun disableValiBtn(holder: MyCurrentYearViewHolder, position: Int) {
        val validatorButtonId = holder.validateBtn

        validatorButtonId.setText(R.string.validate)

        validatorButtonId.isEnabled = false
        validatorButtonId.isClickable = false
        validatorButtonId.setTextColor(holder.itemView.context.getColor(R.color.white))
        validatorButtonId.setBackgroundColor(holder.itemView.context.getColor(R.color.buttonTrans))
    }

    private fun viewDetails(holder: MyCurrentYearViewHolder, position: Int, owner: String, animalName:String, animalRegaion:String) {

        holder.itemContainer.setOnClickListener {
//            model.setEtagModel(itemSelected)
            val i = Intent(holder.itemView.context, ValidateTagsActivity::class.java)
            i.putExtra(UtilConstant.GET_ETAG_INDEX_KEY, position)
            i.putExtra(UtilConstant.GET_ETAG_ACCOUNT_OWNER, owner)
            i.putExtra(UtilConstant.GET_ETAG_YEAR_KEY, 0)
            i.putExtra(UtilConstant.GET_ANIMAL_NAME, animalName)
            i.putExtra(UtilConstant.GET_ANIMAL_REGION, animalRegaion)
            i.putExtra(UtilConstant.GET_ANIMAL_REGION, animalRegaion)
            holder.itemView.context.startActivity(i)
        }


        holder.validateBtn.setOnClickListener {
//            model.setEtagModel(itemSelected)
            val i = Intent(holder.itemView.context, ValidateTagsActivity::class.java)
            i.putExtra(UtilConstant.GET_ETAG_INDEX_KEY, position)
            i.putExtra(UtilConstant.GET_ETAG_ACCOUNT_OWNER, owner)
            i.putExtra(UtilConstant.GET_ETAG_YEAR_KEY, 0)
            i.putExtra(UtilConstant.GET_ANIMAL_NAME, animalName)
            i.putExtra(UtilConstant.GET_ANIMAL_REGION, animalRegaion)
            holder.itemView.context.startActivity(i)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}
