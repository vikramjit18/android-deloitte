/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.items

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.LinkedAlsNumber
import com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.UserStatusResponse
import com.fwp.deloittedctcustomerapp.databinding.FragmentItemsBinding
import com.fwp.deloittedctcustomerapp.ui.view.als.AlsLinkingActivity
import com.fwp.deloittedctcustomerapp.ui.view.bottom.common.AlsNotLinked
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.multiple.MultiUserItemsFragmnet
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.ItemsHeldAvailableFragment
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.PREVIOUS_INDEX
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import com.google.gson.Gson
import java.lang.Exception

class ItemsFragment : Fragment() {

    private var _binding: FragmentItemsBinding? = null
    private val binding get() = _binding!!
    private lateinit var landingScreenActivityViewModel: LandingScreenActivityViewModel
    private val gson = Gson()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentItemsBinding.inflate(inflater, container, false)
        landingScreenActivityViewModel =
            ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]

        buttonHandler()
        SharedPrefs.init(requireActivity())
        try {
            landingScreenActivityViewModel.getItemHeldResponse()
        } catch (e1: Exception) {
            throw e1
        } catch (f: RuntimeException) {
            throw f
        } catch (e: NullPointerException) {
            throw e
        }

        landingScreenActivityViewModel.getItemHeldResponse.observe(viewLifecycleOwner) {
            val json = gson.toJson(it)
            SharedPrefs.write(UtilConstant.ITEM_HELD, json)
//            when (it.responseCode) {
//                UtilConstant.SUCCESS_CODE -> {
//                    val json = gson.toJson(it)
//                    SharedPrefs.write(UtilConstant.ITEM_HELD, json)
//                }
//            }
        }

        return binding.root
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
        }
    }

    override fun onResume() {
        super.onResume()
        if (UtilConstant.snackbarShown) {
            UtilsMethods.snackBarCustom(
                binding.root,
                resources.getString(R.string.als_linked),
                ContextCompat.getColor(binding.root.context, R.color.black),
                ContextCompat.getColor(binding.root.context, R.color.white)
            )
            UtilConstant.snackbarShown = false
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            replaceItemFragment()
        } catch (f: RuntimeException) {
            throw f
        } catch (e: NullPointerException) {
            throw e
        }
    }

    private fun buttonHandler() {
        val buttonLinkAls = binding.itemsAlsNotLinked.helpGetStartButton.btn
        buttonLinkAls.text = resources.getString(R.string.linkALSText)
        buttonLinkAls.setTextColor(resources.getColor(R.color.black, null))
        buttonLinkAls.setBackgroundColor(
            resources.getColor(
                R.color.prim_yellow,
                null
            )
        )

        // Intent to
        buttonLinkAls.setOnClickListener {
            val i = Intent(activity, AlsLinkingActivity::class.java)
            startActivity(i)
            (activity as Activity?)!!.overridePendingTransition(0, 0)
        }
    }

    private fun replaceItemFragment() {
        val userJson = SharedPrefs.read(UtilConstant.CHECK_USER, "")
        if (!userJson.isNullOrEmpty()) {
            val userObj = gson.fromJson(userJson, UserStatusResponse::class.java)
            if (userObj.responseCode == UtilConstant.SUCCESS_CODE && userObj.responseMessage == UtilConstant.SUCCESS_MSG) {
                when (userObj.resObject[0].alsLinked && userObj.resObject[0].alsActivated) {
                    false -> {
                        binding.itemsAlsNotLinked.clAlsNotLinked.visibility = View.VISIBLE
                    }
                    true -> {
                        binding.itemsAlsNotLinked.clAlsNotLinked.visibility = View.GONE
                        replaceMultiItemFragment()
                    }
                }
            } else {
//                UtilsMethods.toastMaker(requireActivity(), "Internal server error")
            }
        }
    }


    private fun replaceMultiItemFragment() {
        val json = SharedPrefs.read(UtilConstant.USER_PROFILE, "")
        if (!json.isNullOrEmpty()) {
            val obj = gson.fromJson(json, UserProfileResponse::class.java)
            if (obj.resObject?.get(0)?.message == null) {
                when (obj.resObject!![PREVIOUS_INDEX].s?.isNotEmpty()) {
                    true -> navigateToFrag(MultiUserItemsFragmnet.newInstance())
                    false -> navigateToFrag(ItemsHeldAvailableFragment.newInstance())
                }
            }
        } else {
            Log.e("Null item frag", "replaceMultiItemFragment: It is nullll")
        }
    }

    private fun navigateToFrag(fragment: Fragment) {
        childFragmentManager
            .beginTransaction()
            .replace(
                R.id.items_fragment_container,
                fragment
            ).commitNowAllowingStateLoss()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
