/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.LoginInfoFragmentBinding
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import android.content.Intent
import android.os.Build
import android.provider.Settings
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_WEAK
import androidx.lifecycle.ViewModelProvider
import com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.google.gson.Gson


class LoginInfoFragment : Fragment() {

    companion object {
        fun newInstance() = LoginInfoFragment()
    }

    private var _binding: LoginInfoFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var landingScreenActivityViewModel: LandingScreenActivityViewModel

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LoginInfoFragmentBinding.inflate(inflater, container, false)
        landingScreenActivityViewModel =
            ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]
        SharedPrefs.init(requireActivity())
        binding.myFwpEmail.text = SharedPrefs.read(UtilConstant.FWP_EMAIL_LOGIN, "")
        binding.myFwpUsername.text = SharedPrefs.read(UtilConstant.USERNAME_LOGIN, "")

        val root = binding.root
        buttonHandler()
        binding.bioLayer.visibility = View.GONE
        biometricSwitch()
        return root
    }

    override fun onResume() {
        super.onResume()
//        biometricAuthenticate()
    }

    private fun biometricAuthenticate() {

        val biometricManager = BiometricManager.from(requireActivity())
        if (Build.VERSION.SDK_INT >= 29) {
            binding.bioLayer.visibility = View.VISIBLE
            if (biometricManager.canAuthenticate(BIOMETRIC_STRONG) == BiometricManager.BIOMETRIC_SUCCESS) {
                binding.youNeedFirst.visibility = View.INVISIBLE
                binding.gotoDeviceSetting.visibility = View.INVISIBLE
                return
            } else {
                binding.enableBiometric.isEnabled = false
                binding.youNeedFirst.visibility = View.VISIBLE
                binding.gotoDeviceSetting.visibility = View.VISIBLE

//                if (biometricManager.canAuthenticate() != BiometricManager.BIOMETRIC_SUCCESS) {
//                    return
//                }
            }
//        }
//        if (biometricManager.canAuthenticate() != BiometricManager.BIOMETRIC_SUCCESS) {
//            return
//        }
        } else {
            binding.bioLayer.visibility = View.GONE
        }
    }

    private fun buttonHandler() {
        binding.back.backLayout.setOnClickListener() {
            UtilsMethods.backStack(findNavController())
        }

        binding.gotoDeviceSetting.setOnClickListener() {
            val intent = Intent(Settings.ACTION_BIOMETRIC_ENROLL)
            requireActivity().startActivity(intent)

        }
        binding.changeLoginInfo.setOnClickListener() {
            UtilsMethods.navigateToFragment(binding.root, R.id.changeLoginInfoFragment)
        }


    }

    private fun biometricSwitch() {
        val biometricBool = SharedPrefs.read(UtilConstant.BIOMETRIC_ENABLE, "")
        binding.enableBiometric.isChecked = !biometricBool.isNullOrEmpty()
        binding.enableBiometric.setOnCheckedChangeListener { _, isChecked ->
            when (isChecked) {
                true -> SharedPrefs.write(UtilConstant.BIOMETRIC_ENABLE, "Pressed")

                false -> SharedPrefs.removeData(UtilConstant.BIOMETRIC_ENABLE)
            }
        }
    }

}
