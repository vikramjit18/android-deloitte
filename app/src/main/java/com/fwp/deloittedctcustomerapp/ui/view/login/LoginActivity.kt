/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.login

import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.ui.view.bottom.LandingScreenActivity
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.Executor

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private var biometricPrompt: BiometricPrompt? = null
    private var promptInfo: BiometricPrompt.PromptInfo? = null
    private lateinit var executor: Executor
    private var keyguard: KeyguardManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        SharedPrefs.init(this)

        /**
         * Initiate
         * Biometric process
         */
        executor = ContextCompat.getMainExecutor(applicationContext)
        keyguard = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        biometricPrompt =
            BiometricPrompt(
                this@LoginActivity,
                executor,
                object : BiometricPrompt.AuthenticationCallback() {
                    override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                        super.onAuthenticationError(errorCode, errString)

                    }

                    /**
                     * Function for success
                     * biometric scan
                     */
                    override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                        super.onAuthenticationSucceeded(result)
//                        UtilsMethods.toastMaker(this@LoginActivity, "${keyguard!!.isDeviceSecure}")
//                        UtilConstant.biometricAuthDone = true

                        /**
                         * Navigation to
                         * login agreement
                         */
                        val intent: Intent =
                            Intent(this@LoginActivity, LandingScreenActivity::class.java)
                        startActivity(intent)
                        finish()

                    }

                    /* override fun onAuthenticationFailed() {
                         super.onAuthenticationFailed()
                     }*/

                })

        /**
         * Biometric
         * Scan Builder
         */
        // Biometric Scan Builder
        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Authenticate ${getString(R.string.app_name)}")
            .setNegativeButtonText("CANCEL")
            .setConfirmationRequired(false)
            .build()


    }

    override fun onBackPressed() {

    }

    override fun onResume() {
        super.onResume()
        val biometricBool = SharedPrefs.read(UtilConstant.BIOMETRIC_ENABLE, "")
        if (!biometricBool.isNullOrEmpty()) {
//            biometricAuthenticate()
        }
    }

    private fun biometricAuthenticate() {
        val biometricManager = BiometricManager.from(this)
        if (Build.VERSION.SDK_INT >= 29 && biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG) == BiometricManager.BIOMETRIC_SUCCESS) {
//            val intent: Intent = Intent(this@LoginActivity, LandingScreenActivity::class.java)
//            startActivity(intent)
//            finish()
            biometricPrompt!!.authenticate(promptInfo!!)
        }

    }
}
