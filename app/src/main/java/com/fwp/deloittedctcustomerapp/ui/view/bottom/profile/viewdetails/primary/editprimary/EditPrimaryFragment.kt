/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.editprimary


import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.*
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.UpdateUserProfileRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse
import com.fwp.deloittedctcustomerapp.databinding.EditPrimaryFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.PrimaryAccountViewModel
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint
import kotlin.collections.ArrayList
import java.lang.Exception
import java.net.SocketTimeoutException
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import android.widget.TextView.OnEditorActionListener
import androidx.core.content.ContextCompat
import com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.COUNTRY
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.EYE
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.FEMALE
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.GENDER
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.HAIR
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.HEIGHT
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.LBS
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.MAIL_STATE
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.STATE
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.SUFFIX
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.WEIGHT
import javax.net.ssl.SSLException


private const val TAG = "EditPrimaryFragment"

/**
 * FWP
 * Hilt entry point
 */
@AndroidEntryPoint
class EditPrimaryFragment : Fragment() {

    companion object {
        fun newInstance() = EditPrimaryFragment()
    }

    // GLobal variables for this file.

    private var zipcodeCity: String = ""
    private var _binding: EditPrimaryFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var loadingDialog: LoadingDialog

    private lateinit var viewModel: PrimaryAccountViewModel
    private lateinit var landingScreenActivityViewModel: LandingScreenActivityViewModel

    // Array List
    private val eyeItemsList = arrayListOf<String>()
    private val countryCodeList = arrayListOf<String>()
    private val suffixList = arrayListOf<String>()
    private val hairItemsList = arrayListOf<String>()
    private val countriesItemsList = arrayListOf<String>()
    private val genderItemsList = arrayListOf<String>()
    private val genderKeyList = arrayListOf<String>()
    private val stateResCodeItemsList = arrayListOf<String>()
    private val stateMailCodeItemsList = arrayListOf<String>()
    private val stateResValueItemsList = arrayListOf<String>()
    private val stateMailValueItemsList = arrayListOf<String>()
    private val heightItemList = arrayListOf<String>()
    private val weightItemList = arrayListOf<String>()
    private val eyeColorCodeList = arrayListOf<String>()
    private val hairColorCodeList = arrayListOf<String>()
    private var userName = ""
    private val timeStateSet: Long = 100

    private var firstName: String = ""
    private var lastName: String = ""
    private var middleName: String = ""
    private var suffix: Int = 0
    private var resStreet: String = ""
    private var resCity: String = ""
    private var resState: Int = 0
    private var resZipcode: String = ""
    private var resCountry: Int = 0
    private var mailStreet: String = ""
    private var mailCity: String = ""
    private var mailState: Int = 0
    private var mailZipcode: String = ""
    private var mailCountry: Int = 0
    private var workNum: String = ""
    private var homeNum: String = ""
    private var alsEmail: String = ""
    private var gender: Int = 0
    private var eyeColor: Int = 0
    private var hairColor: Int = 0
    private var height: Int = 0
    private var weight: Int = 0

    // Empty variables
    private var dob = ""
    private var alsNumber: Int = 0
    private var clubMember500 = ""
    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_bottom_to_top)
        } else {
            AnimationUtils.loadAnimation(context, R.anim.slide_top_to_bottom)
        }
    }

    override fun onPause() {
        super.onPause()
        UtilsMethods.hideKeyBoard(binding.root, requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Binding xml to file
        _binding = EditPrimaryFragmentBinding.inflate(inflater, container, false)

        // Initiate view model by default way
        viewModel = ViewModelProvider(this)[PrimaryAccountViewModel::class.java]
        landingScreenActivityViewModel =
            ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]
        // Starting View user profile api
        viewModel.getUserProfile()

        // Initiate loading instance
        loadingDialog = LoadingDialog(mActivity = requireActivity())

        loadingDialog.startLoading()

        // Returning binding
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        // Globals Functions
        editTextInputType()
        buttonHandler()

        landingScreenActivityViewModel.toastMasterUSer.observe(viewLifecycleOwner, {
            it.getContentIfNotHandledOrReturnNull()?.let {
                loadingDialog.isDismiss()
//                UtilsMethods.toastMaker(requireContext(), it)
            }
        })

        landingScreenActivityViewModel.toastViewUser.observe(viewLifecycleOwner, {
            it.getContentIfNotHandledOrReturnNull()?.let {
                loadingDialog.isDismiss()
//                UtilsMethods.toastMaker(requireContext(), it)
            }
        })

        binding.workNo.inputType = InputType.TYPE_CLASS_NUMBER
        binding.homeNo.inputType = InputType.TYPE_CLASS_NUMBER
        binding.errMailZip.error.visibility = View.INVISIBLE
        binding.errResZip.error.visibility = View.INVISIBLE
        // Observing master data
        masterDataViewModelObsr()
        // Observing Zio code response
        zipcodeViewModelObsr()


    }

    private fun masterDataViewModelObsr() {
        landingScreenActivityViewModel.masterDataResponse.observe(viewLifecycleOwner) {

            if (it.responseMessage == UtilConstant.SUCCESS_MSG) {

                // Clearing array
                clearArrayList()

                // setting up eyes
//                eyeColorCodeList.add("")
//                eyeItemsList.add("")
                it.resObject[0].eYE.entries.forEach { color ->
                    eyeColorCodeList.add(color.key.toString())
                    eyeItemsList.add(color.value)
                    eyeDropdown(eyeItemsList)
                }
                // setting up hair
//                hairColorCodeList.add("")
//                hairItemsList.add("")
                it.resObject[0].hAIR.entries.forEach { color ->
                    hairColorCodeList.add(color.key.toString())
                    hairItemsList.add(color.value)
                    hairDropdown(hairItemsList)
                }

                // Setting suffix list item
                suffixList.add("")
                it.resObject[0].suffix.forEach {
                    suffixList.add(it)
                    suffixDropdown(suffixList)
                }

                // Setting gender list item
//                genderItemsList.add("")
//                genderKeyList.add("")
                it.resObject[0].sexCd.entries.forEach { gender ->
                    genderItemsList.add(gender.value)
                    genderKeyList.add(gender.key)
                    genderDropdown(genderItemsList)
                }

                val heightMinCount = it.resObject[0].height.minimumHeightValue.toDouble()
                val heightMaxCount = it.resObject[0].height.maximumHeightValue.toDouble() + 1.0
                var value = heightMinCount.toInt()
                var tempValue = 0

                for (i in heightMinCount.toInt() until (11 * heightMaxCount.toInt()) + 3) {

                    if (value >= heightMaxCount.toInt()) {
                        heightItemList.add(("$value'0\""))
                        break
                    }
                    if (tempValue < 12) {
                        heightItemList.add(("$value'$tempValue\""))
                        tempValue += 1
                    } else {
                        tempValue = 0
                        value = (value + 1)
                    }
                }

                //        heightItemList.add("")
//                while (heightCount <= it.resObject[0].height.maximumHeightValue.toDouble()) {
//                    val heightString = heightCount.toFloat().toString().split(".")
//                    heightItemList.add("${heightString[0]}'${heightString[1]}\"")
//                        heightCount += 0.1
//                }
                heightDropdown(heightItemList)


                var weightCount = it.resObject[0].weight.minimumWeightValue.toInt()
//                weightItemList.add("")
                while (weightCount < it.resObject[0].weight.maximumWeightValue.toInt()) {
                    weightItemList.add("$weightCount $LBS")
                    weightCount += 1
                }
                weightDropdown(weightItemList)
                // Setting countries list item
                countriesItemsList.add("")
                countryCodeList.add("")
                it.resObject[0].geoInfo.countries.entries.forEach { country ->
                    countriesItemsList.add(country.value)
                    countryCodeList.add(country.key)
                    countryDropdown(countriesItemsList)
                }

                viewModel.countryResCode.observe(viewLifecycleOwner) { cc ->
//                    UtilsMethods.toastMaker(requireContext(), cc)
                    // Setting state list item
                    // viewmodel country key should be selected country code
                    if (it.resObject[0].geoInfo.states.containsKey(cc)) {
                        stateResCodeItemsList.clear()
                        stateResValueItemsList.clear()
                        it.resObject[0].geoInfo.states[cc]!!.entries.forEach { stateEntryKey ->
                            stateResCodeItemsList.add(stateEntryKey.key)
                            stateResValueItemsList.add("${stateEntryKey.key} - ${stateEntryKey.value}")
                            stateResidentialDropdown(stateResValueItemsList)
                        }
                    } else {
                        stateResCodeItemsList.clear()
                        stateResValueItemsList.clear()
                        stateResValueItemsList.add(" ")
                        stateResCodeItemsList.add(" ")
                        stateResidentialDropdown(stateResValueItemsList)
                    }
                }

                viewModel.countryMailCode.observe(viewLifecycleOwner) { cc ->
//                    UtilsMethods.toastMaker(requireContext(), cc)
                    // Setting state list item
                    // viewmodel country key should be selected country code
                    if (it.resObject[0].geoInfo.states.containsKey(cc)) {
                        stateMailCodeItemsList.clear()
                        stateMailValueItemsList.clear()
                        it.resObject[0].geoInfo.states[cc]!!.entries.forEach { stateEntryKey ->
                            stateMailCodeItemsList.add(stateEntryKey.key)
                            stateMailValueItemsList.add("${stateEntryKey.key} - ${stateEntryKey.value}")
                            stateMailDropdown(stateMailValueItemsList)
                        }
                    } else {
                        stateMailCodeItemsList.clear()
                        stateMailValueItemsList.clear()
                        stateMailCodeItemsList.add(" ")
                        stateMailValueItemsList.add(" ")
                        stateMailDropdown(stateMailValueItemsList)
                    }
                }

//                // Observing User Profile Response
//                userProfileViewModelObsr()
            } else {
//                UtilsMethods.toastMaker(requireContext(), it.resObject[0].toString())
            }
        }
        // Observing User Profile Response
        userProfileViewModelObsr()
    }


    private fun userProfileViewModelObsr() {
        viewModel.viewUserProfileResponse.observe(
            viewLifecycleOwner,
            {
                if (it.responseMessage == UtilConstant.SUCCESS_MSG)
                    setUpView(it)
//                    loadingDialog.isDismiss()
                else {
                    it.resObject?.get(0)?.let { it1 ->
//                        UtilsMethods.toastMaker(
//                            requireContext(),
//                            it1.toString()
//                        )
                    }
                }
            })
    }

    private fun setUpView(response: UserProfileResponse) {

        // Saving Value for update user
        dob = response.resObject!![0].p?.dob.toString()
        alsNumber = response.resObject[0].p?.alsNo!!
        userName = response.resObject[0].p?.userName!!
        // checking on email matches
        when (response.resObject[0].p?.alsEmail == response.resObject[0].p?.fwpEmailId) {
            true -> {
                binding.emailMatch.text = resources.getText(R.string.emailMatchingText)
                binding.learnMoreFwpEmail.visibility = View.GONE
            }
            false -> {
                binding.emailMatch.text = resources.getText(R.string.emailNotMatchingText)
                binding.learnMoreFwpEmail.visibility = View.VISIBLE

            }
        }
        binding.learnMoreFwpEmail.setOnClickListener {
            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.action_editPrimaryFragment_to_learnMoreAlsEmailFragment
            )
        }

        // First name
        binding.firstName.editText.setText(response.resObject[0].p?.firstNm)
        firstName = response.resObject[0].p?.firstNm.toString()

        // Middle name
        binding.midName.editText.setText(response.resObject[0].p?.midInit)
        middleName = response.resObject[0].p?.midInit.toString()

        // last name
        binding.lastName.editText.setText(response.resObject[0].p?.lastNm)
        lastName = response.resObject[0].p?.lastNm.toString()

        // als Number
        binding.tvAlsNumber.text =
            "ALS#: ${response.resObject[0].p?.dob} - ${response.resObject[0].p?.alsNo}"

        // Residency status
        if (response.resObject[0].p?.residencyStatus == "R") {

            binding.tvResidencyStatus.text = resources.getText(R.string.resident)
        } else {
            binding.tvResidencyStatus.text = resources.getText(R.string.nonresident)
        }
        //res Address
        binding.street.editText.setText(response.resObject[0].p?.residentialAddrLine)
        resStreet = response.resObject[0].p?.residentialAddrLine.toString()

        if (zipcodeCity == "") {
            binding.city.editText.setText(response.resObject[0].p?.residentialCity)
            resCity = response.resObject[0].p?.residentialCity.toString()
        }
        binding.city.editText.setText(response.resObject[0].p?.residentialCity)

        binding.etZipcode.editText.setText(response.resObject[0].p?.residentialZipCd)
        resZipcode = response.resObject[0].p?.residentialZipCd.toString()

        //mail Address
        binding.streetMail.editText.setText(response.resObject[0].p?.mailingAddrLine)
        mailStreet = response.resObject[0].p?.mailingAddrLine.toString()

        if (zipcodeCity == "") {
            binding.cityMail.editText.setText(response.resObject[0].p?.mailingCity)
            mailCity = response.resObject[0].p?.mailingCity.toString()
        }
        binding.cityMail.editText.setText(response.resObject[0].p?.mailingCity)
        binding.etZipcodeMail.editText.setText(response.resObject[0].p?.mailingZipCd)
        mailZipcode = response.resObject[0].p?.mailingZipCd.toString()

        // Work number
        workNum = if (response.resObject[0].p?.workPhone!!.isEmpty()) {
            binding.workNo.setText("")
            ""
        } else {
            binding.workNo.setText(response.resObject[0].p?.workPhone!!.replace("-", ""))
            response.resObject[0].p?.workPhone!!.replace("-", "")
        }

        // home number
        homeNum = if (response.resObject[0].p?.homePhone!!.isEmpty()) {
            binding.homeNo.setText("")
            ""
        } else {
            binding.homeNo.setText(response.resObject[0].p?.homePhone!!.replace("-", ""))
            response.resObject[0].p?.homePhone!!.replace("-", "")
        }

        // Fwp email
        binding.tvFwpEmail.text = response.resObject[0].p?.fwpEmailId

        // Als Email
        binding.alsEmail.editText.setText(response.resObject[0].p?.alsEmail)
        alsEmail = response.resObject[0].p?.alsEmail.toString()

        binding.countryDropdown.prompt = response.resObject[0].p?.residentialCountry
        // set mail state dropdown
        Handler(Looper.myLooper()!!).postDelayed({
            response.resObject?.get(0)?.p?.mailingState?.let {
                getPosition(it, MAIL_STATE)
            }?.let {
                binding.spinnerStateMail.setSelection(
                    it
                )
                mailState = it
            }
        }, timeStateSet)
        // set res state dropdown
        Handler(Looper.myLooper()!!).postDelayed({
            response.resObject?.get(0)?.p?.residentialState?.let { getPosition(it, STATE) }?.let {
                binding.spinnerState.setSelection(
                    it
                )
                resState = it
            }
        }, timeStateSet)

        // Hide loader
        loadingDialog.isDismiss()
    }

    private fun updateProfileResponse() {
        viewModel.editProfileResponse.observe(viewLifecycleOwner, { updateUserResponse ->

            loadingDialog.isDismiss()

            if (updateUserResponse.responseCode == UtilConstant.SUCCESS_CODE) {
//                UtilsMethods.toastMaker(requireContext(), "User profile updated")
                UtilConstant.isProfileUpdate = true
                requireActivity().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                )
                UtilsMethods.backStack(findNavController())
            } else {
//                updateUserResponse.responseMessage?.let {
////                    UtilsMethods.toastMaker(
////                        requireContext(),
////                        it
////                    )
//                }

            }
        })

        viewModel.toastEditResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandledOrReturnNull()?.let { message ->
                loadingDialog.isDismiss()
//                UtilsMethods.toastMaker(requireContext(), message)
            }
        })
    }

    private fun zipcodeViewModelObsr() {
        viewModel.userResZipCodeResponse.observe(viewLifecycleOwner, {
            loadingDialog.isDismiss()
            if (it.responseCode == UtilConstant.SUCCESS_CODE && it.responseMessage == UtilConstant.SUCCESS_MSG) {
                setResCityAndCountryUI(it)
            } else {

                it.resObject[0].message?.get(0)?.let { it1 ->
                    UtilsMethods.toastMaker(
                        requireContext(),
                        it1
                    )
                }

            }


        })
        viewModel.userMailZipCodeResponse.observe(viewLifecycleOwner, {
            loadingDialog.isDismiss()
            if (it.responseCode == UtilConstant.SUCCESS_CODE && it.responseMessage == UtilConstant.SUCCESS_MSG) {
                setMailCityAndCountryUI(it)
            } else {

                it.resObject[0].message?.get(0)?.let { it1 ->
                    UtilsMethods.toastMaker(
                        requireContext(),
                        it1
                    )
                }

            }


        })
    }

    private fun setResCityAndCountryUI(response: ZipcodeResponse) {
        response.resObject[0].country?.let { getPosition(it, COUNTRY) }?.let {
            binding.countryDropdown.setSelection(
                it
            )

        }

        zipcodeCity = response.resObject[0].city.toString()
        binding.city.editText.setText(response.resObject[0].city)

        Handler(Looper.myLooper()!!).postDelayed({
            response.resObject[0].state?.let { getPosition(it, STATE) }?.let {
                binding.spinnerState.setSelection(
                    it
                )
            }
        }, timeStateSet)

    }

    private fun setMailCityAndCountryUI(response: ZipcodeResponse) {
        response.resObject[0].country?.let { getPosition(it, COUNTRY) }?.let {
            binding.countryMailDropdown.setSelection(
                it
            )
        }
        binding.cityMail.editText.setText(response.resObject[0].city)
        Handler(Looper.myLooper()!!).postDelayed({
            response.resObject[0].state?.let { getPosition(it, MAIL_STATE) }?.let {
                binding.spinnerStateMail.setSelection(
                    it
                )
            }

        }, timeStateSet)
//        zipcodeCity = response.resObject[0].city.toString()
    }


    private fun setHairAndEyeColor(code: String, of: String): Int {
        var color = 0

        when (of) {
            EYE -> {
                for (i in 0 until eyeItemsList.size) {
                    if (eyeColorCodeList[i] == code) {
                        color = i
                    }
                }
            }

            HAIR -> {
                for (i in 0 until hairItemsList.size) {
                    if (hairColorCodeList[i] == code) {
                        color = i
                    }
                }
            }
        }

        return color
    }


    private fun getPosition(name: String, of: String): Int {
        var position = 0

        when (of) {

            EYE -> {
                position = setEye(position, name)
            }

            HAIR -> {
                position = setHair(position, name)
            }

            SUFFIX -> {
                position = setSuffix(position, name)
            }

            STATE -> {
                position = setResState(position, name)
            }
            MAIL_STATE -> {
                position = setMailState(position, name)
            }

            COUNTRY -> {
                position = setCountry(position, name)
            }

            GENDER -> {
                position = setGender(position, name)
            }

            HEIGHT -> {
                position = setHegiht(position, name)
            }

            WEIGHT -> {
                position = setWeight(position, name)
            }
        }

        return position
    }

    private fun setWeight(position: Int, name: String): Int {
        var position1 = position
        for (i in 0 until weightItemList.size) {
            position1 = setUserWeightData(i, name, position1)
        }
        return position1
    }

    private fun setHegiht(position: Int, name: String): Int {
        var position1 = position
        for (i in 0 until heightItemList.size) {
            position1 = setUserHeightData(i, name, position1)
        }
        return position1
    }

    private fun setGender(position: Int, name: String): Int {
        var position1 = position
        for (i in 0 until genderItemsList.size) {
            position1 = setUserGenderData(i, name, position1)
        }
        return position1
    }

    private fun setCountry(position: Int, name: String): Int {
        var position1 = position
        for (i in 0 until countryCodeList.size) {
            position1 = setUserCountryData(i, name, position1)
        }
        return position1
    }

    private fun setResState(position: Int, name: String): Int {
        var position1 = position
        for (i in 0 until stateResCodeItemsList.size) {
            position1 = setUserStateData(i, name, position1)
        }
        return position1
    }

    private fun setMailState(position: Int, name: String): Int {
        var position1 = position
        for (i in 0 until stateMailCodeItemsList.size) {
            position1 = setUserMailStateData(i, name, position1)
        }
        return position1
    }

    private fun setSuffix(position: Int, name: String): Int {
        var position1 = position
        for (i in 0 until suffixList.size) {
            position1 = setUserSuffixData(i, name, position1)
        }
        return position1
    }

    private fun setHair(position: Int, name: String): Int {
        var position1 = position
        for (i in 0 until hairItemsList.size) {
            position1 = setUserHairData(i, name, position1)
        }
        return position1
    }

    private fun setEye(position: Int, name: String): Int {
        var position1 = position
        for (i in 0 until eyeItemsList.size) {
            position1 = setUserEyeData(i, name, position1)
        }
        return position1
    }

    private fun setUserWeightData(
        i: Int,
        name: String,
        position: Int
    ): Int {
        var position1 = position
        if (weightItemList[i] == name) position1 = i
        return position1
    }

    private fun setUserHeightData(
        i: Int,
        name: String,
        position: Int
    ): Int {
        var position1 = position
        if (heightItemList[i] == name) position1 = i
        return position1
    }

    private fun setUserGenderData(
        i: Int,
        name: String,
        position: Int
    ): Int {
        var position1 = position
        if (genderKeyList[i] == name) position1 = i
        return position1
    }

    private fun setUserCountryData(
        i: Int,
        name: String,
        position: Int
    ): Int {
        var position1 = position
        if (countryCodeList[i] == name) position1 = i
        return position1
    }

    private fun setUserStateData(i: Int, name: String, position: Int): Int {
        var position1 = position
        if (stateResCodeItemsList[i] == name) position1 = i
        return position1
    }

    private fun setUserMailStateData(i: Int, name: String, position: Int): Int {
        var position1 = position
        if (stateMailCodeItemsList[i] == name) position1 = i
        return position1
    }

    private fun setUserSuffixData(
        i: Int,
        name: String,
        position: Int
    ): Int {
        var position1 = position
        if (suffixList[i] == name) position1 = i
        return position1
    }

    private fun setUserHairData(i: Int, name: String, position: Int): Int {
        var position1 = position
        if (hairItemsList[i] == name)
            position1 = hairColorCodeList[i].toInt()
        return position1
    }

    private fun setUserEyeData(i: Int, name: String, position: Int): Int {
        var position1 = position
        if (eyeItemsList[i] == name)
            position1 = eyeColorCodeList[i].toInt()
        return position1
    }


    private fun editTextInputType() {

        // First name
        binding.firstName.editText.inputType =
            InputType.TYPE_TEXT_VARIATION_PERSON_NAME or InputType.TYPE_TEXT_FLAG_CAP_WORDS

        // Middle name
        binding.midName.editText.inputType =
            InputType.TYPE_TEXT_VARIATION_PERSON_NAME or InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
        binding.midName.editText.filters += InputFilter.LengthFilter(1)

        // Last name
        binding.lastName.editText.inputType =
            InputType.TYPE_TEXT_VARIATION_PERSON_NAME or InputType.TYPE_TEXT_FLAG_CAP_WORDS
        binding.workNo.inputType = InputType.TYPE_CLASS_PHONE
        binding.homeNo.inputType = InputType.TYPE_CLASS_PHONE
//address
        binding.street.editText.filters += InputFilter.LengthFilter(30)
        binding.streetMail.editText.filters += InputFilter.LengthFilter(30)
        binding.city.editText.filters += InputFilter.LengthFilter(30)
        binding.cityMail.editText.filters += InputFilter.LengthFilter(30)

        // Zip code  Residentail (Edittext)
        binding.etZipcode.editText.filters += InputFilter.LengthFilter(5)
        binding.etZipcode.editText.inputType = InputType.TYPE_CLASS_NUMBER
        binding.etZipcode.editText.imeOptions = EditorInfo.IME_ACTION_DONE
        // Zip code  mail (Edittext)
        binding.etZipcodeMail.editText.filters += InputFilter.LengthFilter(5)
        binding.etZipcodeMail.editText.inputType = InputType.TYPE_CLASS_NUMBER
        binding.etZipcodeMail.editText.imeOptions = EditorInfo.IME_ACTION_DONE

        // Email
        binding.alsEmail.editText.inputType =
            InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS or InputType.TYPE_CLASS_TEXT


    }

    private fun suffixDropdown(list: ArrayList<String>) {
        val adapter = activity?.let { ArrayAdapter(it, R.layout.spinner_style, list) }
        adapter!!.notifyDataSetChanged()
        binding.spinner1.adapter = adapter
    }

    private fun stateResidentialDropdown(list: ArrayList<String>) {
        val adapter = activity?.let { ArrayAdapter(it, R.layout.spinner_style, list) }
        adapter!!.notifyDataSetChanged()
        binding.spinnerState.adapter = adapter


    }

    private fun stateMailDropdown(list: ArrayList<String>) {
        val adapter = activity?.let { ArrayAdapter(it, R.layout.spinner_style, list) }
        adapter!!.notifyDataSetChanged()
        binding.spinnerStateMail.adapter = adapter
    }

    private fun countryDropdown(list: ArrayList<String>) {
        val adapter = activity?.let { ArrayAdapter(it, R.layout.spinner_style, list) }
        adapter!!.notifyDataSetChanged()
        binding.countryDropdown.adapter = adapter
        binding.countryMailDropdown.adapter = adapter
        landingScreenActivityViewModel.viewUserProfileResponse.observe(
            viewLifecycleOwner,
            { response ->
                if (response.responseMessage == UtilConstant.SUCCESS_MSG) {

                    // setting mail country dropdowm
                    response.resObject?.get(0)?.p?.mailingCountry?.let { getPosition(it, COUNTRY) }
                        ?.let { index ->
                            binding.countryMailDropdown.setSelection(
                                index
                            )
                            mailCountry = index
                        }
                    // setting residential country dropdowm
                    response.resObject?.get(0)?.p?.residentialCountry?.let {
                        getPosition(
                            it,
                            COUNTRY
                        )
                    }
                        ?.let { index ->
                            binding.countryDropdown.setSelection(
                                index
                            )
                            resCountry = index
                        }
                    // set eye color dropdown
                    response.resObject?.get(0)?.p?.eyeColorCd?.let { setHairAndEyeColor(it, EYE) }
                        ?.let {
                            binding.spinnerEye.setSelection(it)
                            eyeColor = it
                        }

                    // set hair color dropdown
                    response.resObject?.get(0)?.p?.hairColorCd?.let { setHairAndEyeColor(it, HAIR) }
                        ?.let {
                            binding.spinnerHair.setSelection(it)
                            hairColor = it
                        }
                    // set mail state dropdown
                    Handler(Looper.myLooper()!!).postDelayed({
                        response.resObject?.get(0)?.p?.mailingState?.let {
                            getPosition(it, MAIL_STATE)
                        }?.let {
                            binding.spinnerStateMail.setSelection(
                                it
                            )
                            mailState = it
                        }
                    }, timeStateSet)
                    // set res state dropdown
                    Handler(Looper.myLooper()!!).postDelayed({
                        response.resObject?.get(0)?.p?.residentialState?.let {
                            getPosition(
                                it,
                                STATE
                            )
                        }?.let {
                            binding.spinnerState.setSelection(
                                it
                            )
                            resState = it
                        }
                    }, timeStateSet)

                    // setting suffix dropdown string value
                    response.resObject?.get(0)?.p?.suffix?.let { getPosition(it, SUFFIX) }?.let {
                        binding.spinner1.setSelection(
                            it
                        )
                        suffix = it
                    }

                    //setting gender dropdown string value
                    response.resObject?.get(0)?.p?.sexCd?.let { getPosition(it, GENDER) }?.let {
                        binding.spinnerGender.setSelection(
                            it
                        )
                        gender = it
                    }


                    // height dropd own
                    binding.spinnerHeight.setSelection(
                        getPosition(
                            "${response.resObject?.get(0)?.p?.heightFeet}'${
                                response.resObject?.get(
                                    0
                                )?.p?.heightInches
                            }\"",
                            HEIGHT
                        )
                    )
                    height = getPosition(
                        "${response.resObject?.get(0)?.p?.heightFeet}'${response.resObject?.get(0)?.p?.heightInches}\"",
                        HEIGHT
                    )
                    binding.spinnerWeight.setSelection(
                        getPosition(
                            "${response.resObject?.get(0)?.p?.weight} $LBS",
                            WEIGHT
                        )
                    )
                    weight = getPosition(
                        "${response.resObject?.get(0)?.p?.weight} $LBS",
                        WEIGHT
                    )
                    // same as residential address
                    binding.isAddressSame.setOnCheckedChangeListener { _, isChecked ->
                        when (isChecked) {
                            true -> {
//                                binding.mailContainer.visibility = View.GONE
                                //mail Address is same as residential address
//                                binding.streetMail.editText.isEnabled = false
                                binding.countryDropdown.isEnabled = false
                                binding.spinnerState.isEnabled = false
                                binding.street.editText.isClickable = false
                                binding.street.editText.inputType = InputType.TYPE_NULL
                                binding.city.editText.isClickable = false
                                binding.city.editText.inputType = InputType.TYPE_NULL
                                binding.etZipcode.editText.isClickable = false
                                binding.etZipcode.editText.inputType = InputType.TYPE_NULL
                                binding.countryDropdown.isClickable = false
                                binding.spinnerState.isClickable = false
                                binding.street.editText.setText(binding.streetMail.editText.text)

                                if (zipcodeCity == "") {
                                    binding.city.editText.setText(binding.cityMail.editText.text)
                                }
                                binding.city.editText.setText(binding.cityMail.editText.text)

                                binding.etZipcode.editText.setText(binding.etZipcodeMail.editText.text)

                                binding.countryDropdown.setSelection(binding.countryMailDropdown.selectedItemPosition)
                                Handler(Looper.myLooper()!!).postDelayed({
                                    binding.spinnerState.setSelection(binding.spinnerStateMail.selectedItemPosition)

                                }, timeStateSet)

                            }
                            false -> {
//                                binding.mailContainer.visibility = View.VISIBLE
                                //mail Address
                                binding.street.editText.isEnabled = true

                                binding.city.editText.isEnabled = true
                                binding.etZipcode.editText.isEnabled = true
                                binding.etZipcode.editText.inputType = InputType.TYPE_CLASS_TEXT
                                binding.city.editText.inputType = InputType.TYPE_CLASS_TEXT
                                binding.street.editText.inputType = InputType.TYPE_CLASS_TEXT
                                binding.countryDropdown.isEnabled = true
                                binding.spinnerState.isEnabled = true
                                binding.street.editText.isClickable = true
                                binding.city.editText.isClickable = true
                                binding.etZipcode.editText.isClickable = true
                                binding.countryDropdown.isClickable = true
                                binding.spinnerState.isClickable = true

                                binding.street.editText.setText(response.resObject?.get(0)?.p?.residentialAddrLine)

                                if (zipcodeCity == "") {
                                    binding.city.editText.setText(response.resObject?.get(0)?.p?.residentialCity)
                                }
                                binding.city.editText.setText(response.resObject?.get(0)?.p?.residentialCity)
                                binding.etZipcode.editText.setText(response.resObject?.get(0)?.p?.residentialZipCd)

                                response.resObject?.get(0)?.p?.residentialCountry?.let { no ->
                                    getPosition(
                                        no,
                                        COUNTRY
                                    )
                                }
                                    ?.let { index ->
                                        binding.countryDropdown.setSelection(
                                            index
                                        )
                                    }
                                Handler(Looper.myLooper()!!).postDelayed({
                                    // set mail state dropdown
                                    response.resObject?.get(0)?.p?.residentialState?.let { no ->
                                        getPosition(
                                            no,
                                            STATE
                                        )
                                    }?.let { index ->
                                        binding.spinnerState.setSelection(
                                            index
                                        )
                                    }
                                }, timeStateSet)
                            }
                        }
                    }



                    binding.countryDropdown.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View?,
                                position: Int,
                                id: Long
                            ) {
                                viewModel.setResCountryCode(countryCodeList[binding.countryDropdown.selectedItemPosition])

                            }

                            override fun onNothingSelected(p0: AdapterView<*>?) {

                            }
                        }
                    binding.countryMailDropdown.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View?,
                                position: Int,
                                id: Long
                            ) {
                                viewModel.setMailCountryCode(countryCodeList[binding.countryMailDropdown.selectedItemPosition])

                            }

                            override fun onNothingSelected(p0: AdapterView<*>?) {

                            }
                        }
                    autoApiHitZipcode(response)
                } else {
                    response.resObject?.get(0)?.let { it1 ->
//                        UtilsMethods.toastMaker(
//                            requireContext(),
//                            it1.toString()
//                        )
                    }
                }
            })
    }

    private fun genderDropdown(list: ArrayList<String>) {
        val adapter = activity?.let { ArrayAdapter(it, R.layout.spinner_style, list) }
        adapter!!.notifyDataSetChanged()
        binding.spinnerGender.adapter = adapter
    }

    private fun eyeDropdown(eyeColors: ArrayList<String>) {
        val adapter = activity?.let { ArrayAdapter(it, R.layout.spinner_style, eyeColors) }
        adapter!!.notifyDataSetChanged()
        binding.spinnerEye.adapter = adapter
    }

    private fun hairDropdown(hairColours: ArrayList<String>) {
        val adapter = activity?.let { ArrayAdapter(it, R.layout.spinner_style, hairColours) }
        adapter!!.notifyDataSetChanged()
        binding.spinnerHair.adapter = adapter
    }

    private fun heightDropdown(heightList: ArrayList<String>) {
        val adapter = activity?.let { ArrayAdapter(it, R.layout.spinner_style, heightList) }
        adapter!!.notifyDataSetChanged()
        binding.spinnerHeight.adapter = adapter
    }

    private fun weightDropdown(weightList: ArrayList<String>) {
        val adapter = activity?.let { ArrayAdapter(it, R.layout.spinner_style, weightList) }
        adapter!!.notifyDataSetChanged()
        binding.spinnerWeight.adapter = adapter
    }

    private fun buttonHandler() {

        binding.back.backLayout.setImageDrawable(
            ResourcesCompat.getDrawable(resources, R.drawable.ic_close, null)
        )


        binding.back.backLayout.setOnClickListener {
            if (isEverythingSaved()) {
                UtilsMethods.backStack(findNavController())
            } else {
                showUnsavedAlert()
            }
        }

        binding.saveContainer.setOnClickListener {
            saveProfileDetails()
        }

        binding.ivAlsAlert.setOnClickListener {
            UtilsMethods.showDialerAlert(
                requireActivity(),
                requireContext(),
                resources.getString(R.string.about_als),
                resources.getString(R.string.alert_als_desc),
                resources.getString(R.string.ok),
                resources.getString(R.string.callFwpText)
            )
        }

        binding.ivResidencyAlert.setOnClickListener {
            UtilsMethods.showDialerAlert(
                requireActivity(),
                requireContext(),
                resources.getString(R.string.residencyStatusText),
                resources.getString(R.string.alert_residency_status),
                resources.getString(R.string.ok),
                resources.getString(R.string.callFwpText)

            )
        }

        binding.ivFwpEmailAlert.setOnClickListener {
            showFragmentNavigationAlert(
                requireContext(),
                binding.root,
                R.id.action_editPrimaryFragment_to_changeLoginInfoFragment
            )
        }

    }

    private fun showUnsavedAlert() {
        val builder = AlertDialog.Builder(requireContext()).create()
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.custom_dialog, null)
        val leftBtn = view.findViewById<TextView>(R.id.btn_left)
        val rightBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = "Unsaved changes"
        des.text =
            "You’ve made changes to your profile. If you exit without saving, all your changes will be lost. Do you want to save your changes?"
        leftBtn.text = "Discard changes"
        rightBtn.text = "Save"
        builder.setView(view)
        rightBtn.setOnClickListener {
            saveProfileDetails()
            builder.dismiss()
        }
        leftBtn.setOnClickListener {
            UtilsMethods.backStack(findNavController())
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    private fun isEverythingSaved(): Boolean {

        if (firstName != binding.firstName.editText.text.toString()) {
            return false
        }

        if (lastName != binding.lastName.editText.text.toString()) {
            return false
        }

        if (middleName != binding.midName.editText.text.toString()) {
            return false
        }

        if (suffix != binding.spinner1.selectedItemPosition) {
            return false
        }

        if (resStreet != binding.street.editText.text.toString()) {
            return false
        }

        if (resCity != binding.city.editText.text.toString()) {
            return false
        }

        if (resState != binding.spinnerState.selectedItemPosition) {
            return false
        }

        if (resZipcode != binding.etZipcode.editText.text.toString()) {
            return false
        }

        if (resCountry != binding.countryDropdown.selectedItemPosition) {
            return false
        }
        if (mailStreet != binding.streetMail.editText.text.toString()) {
            return false
        }

        if (mailCity != binding.cityMail.editText.text.toString()) {
            return false
        }

        if (mailState != binding.spinnerStateMail.selectedItemPosition) {
            return false
        }

        if (mailZipcode != binding.etZipcodeMail.editText.text.toString()) {
            return false
        }

        if (mailCountry != binding.countryMailDropdown.selectedItemPosition) {
            return false
        }

        if (workNum != binding.workNo.unMaskedText.toString()) {
            return false
        }

        if (homeNum != binding.homeNo.unMaskedText.toString()) {
            return false
        }

        if (alsEmail != binding.alsEmail.editText.text.toString()) {
            return false
        }

        if (gender != binding.spinnerGender.selectedItemPosition) {
            return false
        }

        if (eyeColor != binding.spinnerEye.selectedItemPosition) {
            return false
        }

        if (hairColor != binding.spinnerHair.selectedItemPosition) {
            return false
        }

        if (height != binding.spinnerHeight.selectedItemPosition) {
            return false
        }

        if (weight != binding.spinnerWeight.selectedItemPosition) {
            return false
        }

        return true
    }

    private fun saveProfileDetails() {
        if (UtilsMethods.isInternetConnected(requireContext())) {

            if (isFieldAreNotEmpty()) {
                loadingDialog.startLoading()
                updatingUserProfile()
            } else {
                requireActivity().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                )
                requireActivity().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                )

                UtilsMethods.showSimpleAlert(
                    requireContext(),
                    "Profile Edits",
                    "You need resolve the errors before you can save edits to your profile. If you exit without saving, all your changes will be lost.",
                    "Okay"
                )
            }

        } else {
            UtilsMethods.showSimpleAlert(
                mContext = requireContext(),
                tvPrimary = "Your device is currently offline",
                tvSecondary = "To make edits to your profile, you need a network connection. You can still validate any downloaded E-tags while offline.",
                primaryBtn = resources.getString(R.string.ok)

            )
        }
    }

    private fun showFragmentNavigationAlert(
        mContext: Context,
        mView: View,
        viewId: Int
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.update_my_email_modal_layout, null)
        val lBtn = view.findViewById<TextView>(R.id.btn_left)
        val rBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des1 = view.findViewById<TextView>(R.id.textView5)
        title.text = "MyFWP email"
        des1.text =
            "You can change your MyFWP account email address under Profile > Security > Login info. "
        lBtn.text = "Close"
        rBtn.text = "Go to Login info"
        builder.setView(view)
        rBtn.setOnClickListener {
            builder.dismiss()
            UtilsMethods.navigateToFragment(mView, viewId)
        }
        lBtn.setOnClickListener {

            builder.dismiss()
        }

        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    private fun autoApiHitZipcode(userProfileResponse: UserProfileResponse) {
        val residentStatus =
            when (userProfileResponse.resObject?.get(0)?.p?.residencyStatus!! == "R") {
                true -> {
                    "Y"
                }
                false -> {
                    "N"
                }
            }
        binding.etZipcode.editText.setOnEditorActionListener(OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                loadingDialog.startLoading()
                try {
                    viewModel.getResCityFromZipcode(
                        binding.etZipcode.editText.text.toString().toInt(),
                        residentStatus
                    )
                } catch (e: Exception) {
                    loadingDialog.isDismiss()
                    e.printStackTrace()
                } catch (e2: SocketTimeoutException) {
                    loadingDialog.isDismiss()
                    e2.printStackTrace()
                } catch (e3: SSLException) {
                    loadingDialog.isDismiss()
                    e3.printStackTrace()
                }



                return@OnEditorActionListener true
            }
            false
        })
        binding.etZipcodeMail.editText.setOnEditorActionListener(OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {

                loadingDialog.startLoading()

                try {
                    viewModel.getMailCityFromZipcode(
                        binding.etZipcodeMail.editText.text.toString().toInt(),
                        residentStatus
                    )
                } catch (e: Exception) {
                    loadingDialog.isDismiss()
                    e.printStackTrace()
                } catch (e2: SocketTimeoutException) {
                    loadingDialog.isDismiss()
                    e2.printStackTrace()
                } catch (e3: SSLException) {
                    loadingDialog.isDismiss()
                    e3.printStackTrace()
                }
                return@OnEditorActionListener true
            }
            false
        })
    }

    private fun updatingUserProfile() {

        val token = SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString()
        val height = binding.spinnerHeight.selectedItem.toString().split("'")
//        binding.spinner1.selectedItem.toString()
        val updateUserProfileRequest = UpdateUserProfileRequest(
            alsEmail = binding.alsEmail.editText.text.toString(),
            alsNo = alsNumber,
            clubMember500 = clubMember500,
            dob = dob,
            eyeColorCd = getPosition(binding.spinnerEye.selectedItem.toString(), EYE).toString(),
            firstNm = binding.firstName.editText.text.toString(),
            fwpEmailId = binding.tvFwpEmail.text.toString(),
            hairColorCd = getPosition(
                binding.spinnerHair.selectedItem.toString(),
                HAIR
            ).toString(),
            heightFeet = height[0].toInt(),
            heightInches = height[1].replace("\"", "").trim().toInt(),
            homePhone = binding.homeNo.unMaskedText.toString(),
            lastNm = binding.lastName.editText.text.toString(),
            midInit = binding.midName.editText.text.toString(),
            residencyStatus = getResidentCode(binding.tvResidencyStatus.text.toString()),
            sexCd = getGenderCode(binding.spinnerGender.selectedItem.toString()),
            suffix = binding.spinner1.selectedItem.toString(),
            weight = binding.spinnerWeight.selectedItem.toString().replace(LBS, "").trim()
                .toInt(),
            workPhone = binding.workNo.unMaskedText.toString(),
            residentialAddrLine = binding.street.editText.text.toString(),
            residentialCity = binding.city.editText.text.toString(),
            residentialCountry = getCountryCode(binding.countryDropdown.selectedItem.toString()),
            residentialState = stateResCodeItemsList[binding.spinnerState.selectedItemPosition],
            residentialZipCd = binding.etZipcode.editText.text.toString(),
            mailingAddrLine = binding.streetMail.editText.text.toString(),
            mailingCity = binding.cityMail.editText.text.toString(),
            mailingCountry = getCountryCode(binding.countryMailDropdown.selectedItem.toString()),
            mailingState = stateMailCodeItemsList[binding.spinnerStateMail.selectedItemPosition],
            mailingZipCd = binding.etZipcodeMail.editText.text.toString(),
            userName = userName
        )

        Log.e(TAG, "updatingUserProfile: $updateUserProfileRequest")

        try {
            viewModel.updateUserProfile(token, updateUserProfileRequest)
        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (e2: SocketTimeoutException) {
            loadingDialog.isDismiss()
            e2.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        }

        // Observing update profile response
        updateProfileResponse()

    }

    private fun getCountryCode(country: String): String {
        var countryCode = ""
        for (i in 0 until countriesItemsList.size) {
            if (countriesItemsList[i] == country) {
                countryCode = countryCodeList[i]
            }
        }
        return countryCode
    }

    private fun getGenderCode(gender: String): String {
        var genderCode = "M"
        if (gender == FEMALE) {
            genderCode = "F"
        }
        return genderCode
    }

    private fun getResidentCode(resident: String): String {
        var residencyStatus = "N"
        if (resident == resources.getString(R.string.resident)) {
            residencyStatus = "R"
        }
        return residencyStatus
    }

    private fun clearArrayList() {
        eyeItemsList.clear()
        countryCodeList.clear()
        suffixList.clear()
        hairItemsList.clear()
        countriesItemsList.clear()
        genderItemsList.clear()
        stateResCodeItemsList.clear()
        stateResValueItemsList.clear()
        heightItemList.clear()
        weightItemList.clear()
    }

    private fun isFieldAreNotEmpty(): Boolean {

        var state = true
        if (binding.firstName.editText.text.isNullOrEmpty()) {
            binding.firstName.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errFirst.error.visibility = View.VISIBLE
            binding.errFirst.tvNoError.text = "First name is required"

            state = false
        } else {
            binding.firstName.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errFirst.error.visibility = View.GONE

        }

        if (binding.lastName.editText.text.isNullOrEmpty()) {
            binding.lastName.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errLast.error.visibility = View.VISIBLE
            binding.errLast.tvNoError.text = "Last name is required"
            state = false
        } else {
            binding.lastName.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errLast.error.visibility = View.GONE
        }

        if (binding.street.editText.text.isNullOrEmpty()) {
            binding.street.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errResSt.error.visibility = View.VISIBLE
            binding.errResSt.tvNoError.text = "Street is required"
            state = false
        } else {
            binding.street.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errResSt.error.visibility = View.GONE
        }

        if (binding.streetMail.editText.text.isNullOrEmpty()) {

            binding.streetMail.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errMailSt.error.visibility = View.VISIBLE
            binding.errMailSt.tvNoError.text = "Street is required"
            state = false
        } else {
            binding.streetMail.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errMailSt.error.visibility = View.GONE
        }

        if (binding.city.editText.text.isNullOrEmpty()) {
            binding.city.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errResCt.error.visibility = View.VISIBLE
            binding.errResCt.tvNoError.text = "City is required"
            state = false
        } else {
            binding.city.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errResCt.error.visibility = View.GONE
        }

        if (binding.cityMail.editText.text.isNullOrEmpty()) {
            binding.cityMail.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errMailCt.error.visibility = View.VISIBLE
            binding.errMailCt.tvNoError.text = "City is required"
            state = false
        } else {
            binding.streetMail.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errMailCt.error.visibility = View.GONE
        }


        if (binding.etZipcodeMail.editText.text.isNullOrEmpty() || binding.etZipcodeMail.editText.text!!.length < 5) {
            binding.etZipcodeMail.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errMailZip.error.visibility = View.VISIBLE
            binding.errMailZip.tvNoError.text = "Zipcode is required"
            state = false
        } else {
            binding.etZipcodeMail.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errMailZip.error.visibility = View.INVISIBLE
        }

        if (binding.etZipcode.editText.text.isNullOrEmpty() || binding.etZipcode.editText.text!!.length < 5) {
            binding.etZipcode.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errResZip.error.visibility = View.VISIBLE
            binding.errResZip.tvNoError.text = "Zipcode is required"
            state = false
        } else {
            binding.etZipcode.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errResZip.error.visibility = View.INVISIBLE
        }

        if (binding.countryMailDropdown.selectedItem == "") {
            binding.countryMailLayout.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errMailCountry.error.visibility = View.VISIBLE
            binding.errMailCountry.tvNoError.text = "Country is required"
            state = false
        } else {
            binding.countryMailLayout.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errMailCountry.error.visibility = View.GONE
        }
        if (binding.countryDropdown.selectedItem == "") {
            binding.countryLayout.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errResCountry.error.visibility = View.VISIBLE
            binding.errResCountry.tvNoError.text = "Country is required"
            state = false
        } else {
            binding.countryLayout.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errResCountry.error.visibility = View.GONE
        }
        if (binding.homeNo.unMaskedText.isNullOrEmpty() || binding.homeNo.unMaskedText!!.length < 10) {
            binding.homeNo.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errHomeNo.error.visibility = View.VISIBLE
            binding.errHomeNo.tvNoError.text = "Enter a valid 10-digit phone number."
            state = false
        } else {
            binding.homeNo.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errHomeNo.error.visibility = View.GONE
        }
        if (!binding.workNo.unMaskedText.isNullOrEmpty() && binding.workNo.unMaskedText!!.length < 10) {
            binding.workNo.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errWorkNo.error.visibility = View.VISIBLE
            binding.errWorkNo.tvNoError.text = "Enter a valid 10-digit phone number."
            state = false
        } else {
            binding.workNo.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errWorkNo.error.visibility = View.GONE
        }

        if (binding.alsEmail.editText.text.isNullOrEmpty() || !UtilConstant.EMAIL_ADDRESS_PATTERN.matcher(
                binding.alsEmail.editText.text.toString().trim()
            ).matches()
        ) {
            binding.alsEmail.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errAlsEmail.error.visibility = View.VISIBLE
            binding.errAlsEmail.tvNoError.text = "Enter a valid email address"
            state = false
        } else {
            binding.alsEmail.editText.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errAlsEmail.error.visibility = View.GONE
        }
        //gender
        if (binding.spinnerGender.selectedItem == "") {
            binding.dropdownGenderLayout.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errGender.error.visibility = View.VISIBLE
            binding.errGender.tvNoError.text = "Gender is required"
            state = false
        } else {
            binding.dropdownGenderLayout.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errGender.error.visibility = View.INVISIBLE
        }
        // eye
        if (binding.spinnerEye.selectedItem == "") {
            binding.dropdownEye.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errEye.error.visibility = View.VISIBLE
            binding.errEye.tvNoError.text = "Eye color is required"
            state = false
        } else {
            binding.dropdownEye.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errEye.error.visibility = View.INVISIBLE
        }
        // Hair
        if (binding.spinnerHair.selectedItem == "") {
            binding.dropdownHair.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errHair.error.visibility = View.VISIBLE
            binding.errHair.tvNoError.text = "Hair color is required"
            state = false
        } else {
            binding.dropdownHair.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errHair.error.visibility = View.INVISIBLE
        }
        // Height
        if (binding.spinnerHeight.selectedItem == "") {
            binding.dropdownHeight.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errHeight.error.visibility = View.VISIBLE
            binding.errHeight.tvNoError.text = "Height is required"
            state = false
        } else {
            binding.dropdownHeight.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errHeight.error.visibility = View.INVISIBLE
        }

        // weight
        if (binding.spinnerWeight.selectedItem == "") {
            binding.dropdownWeight.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
            binding.errWeight.error.visibility = View.VISIBLE
            binding.errWeight.tvNoError.text = "Weight is required"
            state = false
        } else {
            binding.dropdownWeight.background =
                ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
            binding.errWeight.error.visibility = View.INVISIBLE
        }

        return state
    }

}
