/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.UserStatusResponse
import com.fwp.deloittedctcustomerapp.databinding.ActivityLandingScreenBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.MultiUserEtagAvailableFragment
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.EtagsAvailableFragment
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginActivity
import com.fwp.deloittedctcustomerapp.utils.*
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.DEVICE_ID
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.PREVIOUS_INDEX
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException
import java.lang.Exception
import java.net.SocketTimeoutException
import java.util.*
import javax.net.ssl.SSLException
import javax.net.ssl.SSLHandshakeException

// FWP
// Hilt entry point
@AndroidEntryPoint
class LandingScreenActivity : AppCompatActivity() {

    private var isoflineValidated: Boolean = false

    // Global Variables
    private lateinit var binding: ActivityLandingScreenBinding
    private lateinit var validateTagActivityViewModel: ValidateTagActivityViewModel
    private lateinit var landingScreenActivityViewModel: LandingScreenActivityViewModel
    private lateinit var loadingDialog: LoadingDialog
    private val listOffLoad = mutableListOf<String>()
    private val gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLandingScreenBinding.inflate(layoutInflater)
        DEVICE_ID = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)
        validateTagActivityViewModel =
            ViewModelProvider(this)[ValidateTagActivityViewModel::class.java]
        landingScreenActivityViewModel =
            ViewModelProvider(this)[LandingScreenActivityViewModel::class.java]
        loadingDialog = LoadingDialog(this)
        setContentView(binding.root)
        SharedPrefs.init(this)
        listOffLoad.clear()

        landingScreenActivityViewModel.getDownloadedTagSid().observe(this) {
            it.forEach { data ->
                data.downloadedSpiId?.let { it1 -> listOffLoad.add(it1) }
            }
        }

        // laoding
        // Setting Bottom navigation
        val navView: BottomNavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_activity_landing_screen)

        checkLiveNetworkConnection()
        // Checking destination for hiding bottom nav
        navController.addOnDestinationChangedListener { _, destination, _ ->
            val destiForgot =
                (destination.id == R.id.forgot1) || (destination.id == R.id.frogot2) || (destination.id == R.id.forgot3)
            val destiProfile =
                (destination.id == R.id.editPrimaryFragment) || (destination.id == R.id.changeLoginInfoFragment) || (destination.id == R.id.resetPasswordFragment)
            val bottomSheetHideFrag =
                destiForgot || destiProfile || (destination.id == R.id.forgot4)
            if (bottomSheetHideFrag) {

                navView.visibility = View.GONE
            } else {

                navView.visibility = View.VISIBLE
            }
            val destiMenu =
                destination.id == R.id.securityFragment || destination.id == R.id.primaryAccountFragment || destination.id == R.id.multipleAccountFragment
            val destiMenu2 =
                destination.id == R.id.notificationFragment || destination.id == R.id.loginInfoFragment || destination.id == R.id.locationFragment
            val destiMenu3 =
                destination.id == R.id.getHelpFragment || destination.id == R.id.termPrivacyFragment
            val profileFrag = (destiMenu || destiMenu2 || destiMenu3)
            if (profileFrag) {
                navView.menu.findItem(R.id.navigation_profile).isChecked = true
            }
        }


        // Attaching bottom nav with nav host
        navView.setupWithNavController(navController)

        // Disable reselected
        binding.navView.setOnNavigationItemReselectedListener {
            return@setOnNavigationItemReselectedListener
        }

        landingScreenActivityViewModel.checkUserStatusResponse.observe(this) {
            it?.let {
                val json = gson.toJson(it)
                SharedPrefs.write(UtilConstant.CHECK_USER, json)
                val userJson = SharedPrefs.read(UtilConstant.CHECK_USER, "")
                val userObj = gson.fromJson(userJson, UserStatusResponse::class.java)
                UtilConstant.isAlsLinked = userObj.resObject[0].alsLinked
                UtilConstant.isAlsActivated = userObj.resObject[0].alsActivated
//                    UtilConstant.snackbarShown = it.resObject[0].alsActivated
                // Saving gson data
                // Other response
                if (it.responseCode == UtilConstant.SUCCESS_CODE) {
                    userProfileObserver()
                }

            }
        }
    }

    private fun checkLiveNetworkConnection() {
        val connectivityManager = getSystemService(ConnectivityManager::class.java)
        val liveConnection = ConnectionLiveData(connectivityManager)

        liveConnection.observe(this) { isConnected ->
            when (isConnected) {
                true -> {
                    try {
                        validateOfflineEtag()
                        if (!UtilConstant.multiAccount) {
//                            UtilsMethods.toastMaker(this, "hit to obs")
                            checkUserapiHit()
                            apiHits()
                        }
                    } catch (e: Exception) {
                        loadingDialog.isDismiss()
                        e.printStackTrace()
                    } catch (e2: SocketTimeoutException) {
                        loadingDialog.isDismiss()
                        e2.printStackTrace()
                    } catch (e3: SSLException) {
                        loadingDialog.isDismiss()
                        e3.printStackTrace()
                    } catch (e0: SSLHandshakeException) {
                        loadingDialog.isDismiss()
                        e0.printStackTrace()
                    }
                }
            }
        }

        when (UtilsMethods.isInternetConnected(this)) {
            true -> {
                liveConnectionObsr()
            }
            false -> {
                val userJson = SharedPrefs.read(UtilConstant.CHECK_USER, "")
                val userStatusObj = gson.fromJson(userJson, UserStatusResponse::class.java)
                val objson = SharedPrefs.read(UtilConstant.USER_PROFILE, "")
                val userProfileObj = gson.fromJson(objson, UserProfileResponse::class.java)
                if (userProfileObj.resObject?.get(0)?.message?.get(0)?.lowercase()
                        ?.contains("activated") == true
                ) {
                    UtilsMethods.showAccountExpireAlert(
                        this,
                        "Alert",
                        userProfileObj.resObject?.get(0)?.message!![0],
                        "Ok"
                    )
                }

            }
        }
    }


    private fun liveConnectionObsr() {
        loadingDialog.startLoading()
        try {
            validateOfflineEtag()
            checkUserapiHit()
            apiHits()

        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (e2: SocketTimeoutException) {
            loadingDialog.isDismiss()
            e2.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        } catch (e0: SSLHandshakeException) {
            loadingDialog.isDismiss()
            e0.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        UtilConstant.alsExpiredDialog = true
        val sessionDate = SharedPrefs.read(UtilConstant.LOGIN_DATE, "")
        val logoutPeriod = SharedPrefs.read(UtilConstant.LOGOUT_PERIOD, "360")
        //todo session
//        val dayDiff = UtilsMethods.getDaysBetweenDates(currentDate, sessionDate!!)
        if (!sessionDate.isNullOrEmpty()) {
            val dayDiff = UtilsMethods.dateDifference(
                UtilsMethods.stringToDateConversion(sessionDate!!)!!,
                UtilsMethods.stringToDateConversion(UtilsMethods.getCurrentDateMMMddYY())!!
            )
            Log.e(
                TAG,
                "onStart: diff b/w dates $dayDiff  saved date  ${
                    UtilsMethods.stringToDateConversion(
                        sessionDate!!
                    )
                }, parsed current date ${UtilsMethods.stringToDateConversion(UtilsMethods.getCurrentDateMMMddYY())},current date ${UtilsMethods.getCurrentDateMMMddYY()}"
            )
//        val dayDiff = UtilsMethods.dateDifference(,UtilsMethods.getCurrentDate() )
//        UtilsMethods.toastMaker(this, "${sessionDate} $dayDiff")
            if (dayDiff.toInt() == logoutPeriod.toString().toInt()) {
                showSessionLogOutDialog(dayDiff)
            }

            when (dayDiff.toInt() >= 100) {
                true -> {
                    showSessionLogOutDialog(dayDiff)
                }
                false -> {
                    if (dayDiff.toInt() >= 30) {
                        showSessionNotifyDialog(dayDiff)
                    }
                }

            }
        }

    }

    override fun onResume() {
        super.onResume()
        if (UtilConstant.emailSent) {
            UtilsMethods.snackBarCustom(
                binding.root,
                getString(R.string.request_email),
                ContextCompat.getColor(binding.root.context, R.color.black),
                ContextCompat.getColor(binding.root.context, R.color.white)
            )
            UtilConstant.emailSent = false

        }

    }

    private fun userProfileObserver() {

        // Master data
        landingScreenActivityViewModel.masterDataResponse.observe(this) {
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    val json = gson.toJson(it)
                    SharedPrefs.write(UtilConstant.MASTER_DATA, json)
                }
            }
        }

        // User profile
        landingScreenActivityViewModel.viewUserProfileResponse.observe(this) {


            val json = gson.toJson(it)
            SharedPrefs.write(UtilConstant.USER_PROFILE, json)
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    landingScreenActivityViewModel.setMultiAccountBool(
                        it.resObject?.get(
                            PREVIOUS_INDEX
                        )?.s?.isNotEmpty() == true
                    )
                    UtilConstant.multiAccount =
                        it.resObject?.get(PREVIOUS_INDEX)?.s?.isNotEmpty() == true

                }
                UtilConstant.FAIL_CODE -> {
                    val userJson = SharedPrefs.read(UtilConstant.CHECK_USER, "")
                    val userObj = gson.fromJson(userJson, UserStatusResponse::class.java)
                    if (it.resObject?.get(0)?.message?.get(0)?.toLowerCase()
                            ?.contains("activated") == true
                    ) {
                        UtilsMethods.showAccountExpireAlert(
                            this,
                            "Alert",
                            it.resObject?.get(0)?.message!![0],
                            "Ok"
                        )
                    }
                }
            }
        }

        // Etag response
        landingScreenActivityViewModel.etagResponse.observe(this) { etagResponse ->

            etagResponse?.let { etag ->
                when (etag.responseCode) {
                    UtilConstant.SUCCESS_CODE -> {
                        successEtagResponseObsr(etag)
                    }
                    UtilConstant.FAIL_CODE -> {
                        expiredEtagResponseObsr(etag)
                    }
                }
            }
        }

        // Item held response
        landingScreenActivityViewModel.getItemHeldResponse.observe(this) {
            val json = gson.toJson(it)
            SharedPrefs.write(UtilConstant.ITEM_HELD, json)
//            when (it.responseCode) {
//                UtilConstant.SUCCESS_CODE -> {
//                    val json = gson.toJson(it)
//                    SharedPrefs.write(UtilConstant.ITEM_HELD, json)
//                }
//            }
        }
    }

    private fun expiredEtagResponseObsr(etag: EtagResponse) {
        if (UtilConstant.alsExpiredDialog && etag.resObject!![0].message!!.contains("ALS activation Email expired") || etag.resObject!![0].message!!.contains(
                "server"
            ) || UtilConstant.alsExpiredDialog && etag.resObject[0].message!!.contains("Not Found")
        ) {
            UtilsMethods.showAccountExpireAlert(
                this,
                "Alert",
                etag.resObject[0].message!![0],
                "Ok"
            )
            UtilConstant.alsExpiredDialog = false
        }
    }

    private fun successEtagResponseObsr(etag: EtagResponse) {
        val eJson = gson.toJson(etag)
        SharedPrefs.write(UtilConstant.USER_ETAG, eJson)
    }

    @SuppressLint("HardwareIds")
    private fun checkUserapiHit() {
        if (SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString().isNotEmpty()) {
            try {

                // Hitting user status
                landingScreenActivityViewModel.checkUserStatus(
                    SharedPrefs.read(
                        UtilConstant.JWT_TOKEN,
                        ""
                    ).toString()
                )

            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (e2: SocketTimeoutException) {
                loadingDialog.isDismiss()
                e2.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }

        }
    }


    @SuppressLint("HardwareIds")
    private fun apiHits() {
        DEVICE_ID =
            Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)

        if (SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString().isNotEmpty()) {
//            checkuser
//            loadingDialog.startLoading()
            try {
                landingScreenActivityViewModel.getMasterData()
                landingScreenActivityViewModel.getUserProfile()
                landingScreenActivityViewModel.getItemHeldResponse()
                landingScreenActivityViewModel.getEtag(EtagRequest(DEVICE_ID))
                checkingResDeletingDB()
            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (e2: SocketTimeoutException) {
                loadingDialog.isDismiss()
                e2.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }
//
        }
    }


    private fun showSessionNotifyDialog(
        daysDiff: String
    ) {
        val builder = AlertDialog.Builder(this).create()
        val view = LayoutInflater.from(this).inflate(R.layout.custom_dialog, null)
        val close = view.findViewById<TextView>(R.id.btn_left)
        val reLogin = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = "Your session is about to expire"
        des.text = "Your session will become inactive on [date].\n\n" +
            "Log in now to continue your session for another $daysDiff days and use the app even if you’re offline."
        reLogin.text = "Log in"
        close.text = "Cancel"
        builder.setView(view)
        reLogin.setOnClickListener {
            builder.dismiss()
            initReLoginProcess()
        }
        close.setOnClickListener {
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    private fun initReLoginProcess() {
        if (listOffLoad.isNotEmpty()) {
            loadingDialog.startLoading()
            val offloadRequest = DownloadOffloadRequest(
                deviceUid = DEVICE_ID,
                service = UtilConstant.OFFLOAD,
                spiId = listOffLoad.toSet().toList()
            )
            try {
                landingScreenActivityViewModel.offLoadRequest(offloadRequest)

            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (e2: SocketTimeoutException) {
                loadingDialog.isDismiss()
                e2.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }
            offloadObsr()

        } else {
            UtilConstant.itemLastIndex = 0
            UtilConstant.etagLastIndex = 0
            UtilConstant.ITEMS_LAST_VIEW_INDEX = 0
            UtilConstant.ACCOUNT_VIEWPAGER = 0
            SharedPrefs.clearSharedPref()
            landingScreenActivityViewModel.cleardb()
            val i = Intent(this, LoginActivity::class.java)
            startActivity(i)
            finish()
        }
    }

    private fun showSessionLogOutDialog(
        daysDiff: String
    ) {
        val builder = AlertDialog.Builder(this).create()
        val view = LayoutInflater.from(this).inflate(R.layout.custom_dialog, null)
        val close = view.findViewById<TextView>(R.id.btn_left)
        val reLogin = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = "Your session is about to expire"
        des.text = "Your session will become inactive on [date].\n\n" +
            "Log in now to continue your session for another $daysDiff days and use the app even if you’re offline."
        reLogin.text = "Log in"
        close.text = "Cancel"
        close.visibility = View.GONE
        builder.setView(view)
        reLogin.setOnClickListener {
            builder.dismiss()
            initReLoginProcess()
        }
        close.setOnClickListener {
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }


    private fun validateOfflineEtag() {
        validateTagActivityViewModel.getValidateTagData().observe(this) {
            if (UtilsMethods.isInternetConnected(this) && !it.isNullOrEmpty()) {
                successOfflineEtagsObsr(it)

                validateTagActivityViewModel.validateEtagResponse.observe(this) { res ->
                    when (res.responseCode) {
                        UtilConstant.SUCCESS_CODE -> {
                            successValidateEtagsObsr()
                        }
                    }
                }
            }

        }
    }

    private fun successValidateEtagsObsr() {
//        successValidateEtagsObsr()
        loadingDialog.isDismiss()
        isoflineValidated = true
        checkingResDeletingDB()
//                                loadingDialog.startLoading()
        reLoadingEtag()
    }

    private fun successOfflineEtagsObsr(it: List<ValidateEtagRequest>) {
        it.forEach {
            val request = ValidateEtagRequest(
                id = it.id,
                deviceId = it.deviceId,
                // etaguploaddate
                etagUploadDate = UtilsMethods.getCurrentDateTime(),
                harvestConfNum = it.harvestConfNum,
                //current date
                harvestedDate = it.harvestedDate,
                lat = it.lat,
                longt = it.longt,
                speciesCd = it.speciesCd,
                spiId = it.spiId,
            )
            autoHitValidateApi(request)
        }
    }

    private fun autoHitValidateApi(request: ValidateEtagRequest) {
        try {
            validateTagActivityViewModel.getEtagValidated(request)

        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (e2: SocketTimeoutException) {
            loadingDialog.isDismiss()
            e2.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        } catch (e4: IOException) {
            loadingDialog.isDismiss()
            e4.printStackTrace()
        }
    }

    private fun reLoadingEtag() {
        try {
            landingScreenActivityViewModel.getEtag(EtagRequest(DEVICE_ID))
        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (e2: SocketTimeoutException) {
            loadingDialog.isDismiss()
            e2.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        } catch (e4: IOException) {
            loadingDialog.isDismiss()
            e4.printStackTrace()
        } finally {
            // optional finally block
        }
    }

    private fun checkingResDeletingDB() {
        landingScreenActivityViewModel.etagResponse.observe(this) {
            if (it.responseMessage == UtilConstant.SUCCESS_MSG) {
                loadingDialog.isDismiss()
                if (it.responseCode == UtilConstant.SUCCESS_CODE && isoflineValidated) {
//                    loadingDialog.isDismiss()
                    //not deleting db for safe check. deleting on validate etag fragment
//                    validateTagActivityViewModel.deleteValiData()
                    isoflineValidated = false
                }
            }
        }
    }

    private fun offloadObsr() {
        landingScreenActivityViewModel.offloadResponse.observe(this) {
            loadingDialog.isDismiss()
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    successOffloadObsr()
                }
                UtilConstant.FAIL_CODE -> {
                    it.resObject?.get(UtilConstant.CURRENT_INDEX)
                        ?.let {
//                            it1.message?.get(CURRENT_INDEX)?.let { it2 ->
//                                UtilsMethods.toastMaker(
//                                    requireContext(),
//                                    it2
//                                )
//                            }
                        }
                }
            }
        }
    }

    private fun successOffloadObsr() {
        UtilConstant.itemLastIndex = 0
        UtilConstant.etagLastIndex = 0
        UtilConstant.ITEMS_LAST_VIEW_INDEX = 0
        UtilConstant.ACCOUNT_VIEWPAGER = 0
        SharedPrefs.clearSharedPref()
        landingScreenActivityViewModel.cleardb()
        landingScreenActivityViewModel.deleteDownloadedTagSid()
        val i = Intent(this, LoginActivity::class.java)
        startActivity(i)
        finish()
    }
}


