/*
 * Montana Fish, Wildlife & Parks
 */



package com.fwp.deloittedctcustomerapp.ui.view.signup


import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import android.text.Editable
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.fwp.deloittedctcustomerapp.data.model.requests.CheckUsernameRequest
import com.fwp.deloittedctcustomerapp.databinding.SignUpInitialsFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.MainActivity
import com.fwp.deloittedctcustomerapp.ui.view.login.Login
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginActivity
import com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.google.android.material.textfield.TextInputEditText
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException

/**
 * FWP
 * Hilt entry point
 */
@AndroidEntryPoint
class SignUpInitial : Fragment() {

    /**
     * Global variables
     */
    lateinit var binding: SignUpInitialsFragmentBinding
    private val signupViewModel: SignUpViewModel by viewModels()
    private lateinit var viewModelCommunicator: Communicator
    private lateinit var loadingDialog: LoadingDialog
    private var firstTime = true


    override fun onResume() {
        super.onResume()
        /**
         * initiate
         * communicator view modal
         */
        viewModelCommunicator = ViewModelProvider(requireActivity())[Communicator::class.java]

        // init loading
        loadingDialog = LoadingDialog(requireActivity())

        firstTime = true
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        /**
         * Binding Initiate
         */
        binding =
            DataBindingUtil.inflate(inflater, R.layout.sign_up_initials_fragment, container, false)


        /**
         * Returning
         * root binding
         */
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * Globals functions
         */
        validator()
        buttonActivation()
        buttonHandler()

        /**
         * TextWatch listener
         * for any changes
         */
        binding.user.addTextChangedListener(mTextWatcher)
        binding.pass.addTextChangedListener(mTextWatcher)

        /**
         * Observing
         * existing user Api response
         */
        viewModelObserver()

    }

    /**
     * function for
     * validation username and password
     */
    private fun validator() {
        passValidator()
        userNameValidator()
    }

    /**
     * Function for
     * validation username
     */
    private fun isUserNameValidate(): Boolean {
        if (!binding.user.editableText.isNullOrEmpty()) {
            return if (UtilConstant.USERNAME_PATTERN.matcher(binding.user.editableText.toString())
                    .matches()
            ) {
                binding.usernameLayout.boxStrokeColor =
                    requireActivity().getColor(R.color.onboardTextColor)
                true
            } else {
                binding.usernameLayout.boxStrokeColor =
                    requireActivity().getColor(R.color.errorColor)
                false
            }
        }
        return false
    }

    /**
     * Initiate
     * button activation function
     */
    private fun buttonActivation() {

        val nextButtonId = binding.next.btn
        nextButtonId.text = resources.getString(R.string.nextButtonText)
        val checkingUserAndPass = isUserNameValidate() && binding.pass.length() >= 6
        when (checkingUserAndPass && binding.pass.text.toString()
            .indexOfAny(
                charArrayOf(
                    '\'',
                    '(',
                    ')',
                    '|',
                    '\\'
                )
            ) <= 0) {
            true -> {
                nextButtonId.isEnabled = true
                nextButtonId.isClickable = true
                nextButtonId.setTextColor(resources.getColor(R.color.black, null))
                nextButtonId.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
                nextButtonId.setOnClickListener {
                    /**
                     * Checking Internet connection
                     */
                    apiHitUserCheck()
                }
            }
            false -> {
                nextButtonId.isEnabled = false
                nextButtonId.isClickable = false
                nextButtonId.setTextColor(resources.getColor(R.color.white, null))
                nextButtonId.setBackgroundColor(
                    resources.getColor(
                        R.color.disableButtonColor,
                        null
                    )
                )
            }
        }
    }

    private fun apiHitUserCheck() {
        if (UtilsMethods.isInternetConnected(requireContext())) {
            /**
             * showing
             * progress loader
             */
            loadingDialog.startLoading()

            /**
             * Initiate
             * existing user Api
             */
            try {
                signupViewModel.checkExistingUsername(
                    CheckUsernameRequest(
                        username = binding.user.text.toString()
                    )
                )
            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (e2: SocketTimeoutException) {
                loadingDialog.isDismiss()
                e2.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }


        } else {
            /**
             * showing
             * No internet dialog
             */
            UtilsMethods.showSimpleAlert(
                mContext = requireContext(),
                tvPrimary = getString(R.string.no_connection),
                tvSecondary = getString(R.string.check_and_try_again),
                primaryBtn = resources.getString(R.string.ok)

            )
        }
    }

    private fun viewModelObserver() {

        signupViewModel.existingUserResponse.observe(requireActivity()) {

            if (firstTime) {
                loadingDialog.isDismiss()

                if (it.responseCode == UtilConstant.FAIL_CODE) {
                    viewModelCommunicator.setUserNameCommunicator(
                        binding.user.text.toString().trim(),

                        )
                    viewModelCommunicator.setPassCommunicator(
                        password = binding.pass.text.toString().trim(),
                    )
                    UtilsMethods.navigateToFragment(
                        binding.root,
                        R.id.action_signUpInitial_to_signUpDetails
                    )

                    binding.user.text?.clear()
                    binding.pass.text?.clear()

                    firstTime = false

                } else {
                    showActivityNavigationAlert(
                        requireContext(),
                        "Username already in use",
                        "Username ${binding.user.text} is taken. Try again with a different username.",
                        "Log in",
                        "Try again",
                    )
                    /*      UtilsMethods.toastMaker(
                              requireContext(),
                              resources.getString(R.string.username_already_use)
                          )*/
                }
            }
        }
    }


    private fun showActivityNavigationAlert(
        mContext: Context,
        tvPrimary: String,
        tvSecondary: String,
        leftBtn: String,
        rightBtn: String
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val lBtn = view.findViewById<TextView>(R.id.btn_left)
        val rBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = tvPrimary
        des.text = tvSecondary
        lBtn.text = leftBtn
        rBtn.text = rightBtn
        builder.setView(view)
        lBtn.setOnClickListener {

            builder.dismiss()
            requireActivity().finish()
            requireActivity().overridePendingTransition(R.anim.slide_bottom_to_top, R.anim.slide_top_to_bottom)

        }
        rBtn.setOnClickListener {

            builder.dismiss()
        }

        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    /**
     * create a
     * textWatcher member
     */
    private val mTextWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun afterTextChanged(editable: Editable) {
            buttonActivation()
            isUserNameValidate()
        }
    }


    /**
     * Button handler function
     */
    private fun buttonHandler() {
        binding.back.setOnClickListener {
            requireActivity().finish()
            requireActivity().overridePendingTransition(R.anim.slide_bottom_to_top,R.anim.slide_top_to_bottom)
        }
    }

    /**
     * function for
     * validating password
     */
    private fun passValidator() {
        val password = binding.pass
        val validateImg = R.drawable.ic_check
        val unvalidateImg = R.drawable.ic_unchecked_checkbox
        password.doAfterTextChanged {
            passwordValidator(password, validateImg, unvalidateImg)
        }
    }

    private fun passwordValidator(
        password: TextInputEditText,
        validateImg: Int,
        unValidateImg: Int
    ) {
        when (password.length() >= 6) {
            true -> binding.checkboxLeastCharacter.setImageResource(validateImg)
            false -> binding.checkboxLeastCharacter.setImageResource(unValidateImg)
        }

        when (password.text.toString()
            .indexOfAny(
                charArrayOf(
                    '\'',
                    '(',
                    ')',
                    '|',
                    '\\'
                )
            ) >= 0 || password.editableText.isEmpty()) {
            true -> binding.checkboxNumberCharacter.setImageResource(unValidateImg)
            false -> binding.checkboxNumberCharacter.setImageResource(validateImg)
        }
    }


    /**
     * function for
     * validating username
     */
    private fun userNameValidator() {
        val user = binding.user

        user.doAfterTextChanged {
            //not needed
        }
    }
}
