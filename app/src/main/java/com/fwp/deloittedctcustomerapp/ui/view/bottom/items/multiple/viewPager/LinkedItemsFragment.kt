/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.multiple.viewPager

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.PopupMenu
import androidx.compose.ui.text.android.InternalPlatformTextApi
import androidx.compose.ui.text.android.style.TypefaceSpan
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.ItemDetail
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.PermitData
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Permits
import com.fwp.deloittedctcustomerapp.databinding.LinkedItemsFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView.TagLicenceActivity
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.adapter.ItemHeldPermitCurrentYearAdapter
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.adapter.ItemHeldPermitPreviousYearAdapter
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailActivity
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

private const val TAG = "LinkedItemsFragment"

@AndroidEntryPoint
class LinkedItemsFragment : Fragment() {

    companion object {
        fun newInstance() = LinkedItemsFragment()
    }

    private lateinit var binding: LinkedItemsFragmentBinding
    private lateinit var loadingDialog: LoadingDialog
    private lateinit var viewModel: LandingScreenActivityViewModel
    private val listCurrentYearDetail = mutableListOf<ItemDetail>()
    private val listPreviousYearDetail = mutableListOf<ItemDetail>()
    private val ownerItemList = mutableListOf<String>()
    private var currentYear = ""
    private var previousYear = ""
    private var selectedName: String = ""
    private var lastSpinnerIndex = 0
    private val gson = Gson()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.linked_items_fragment, container, false)

        viewModel = ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loadingDialog = LoadingDialog(
            requireActivity()
        )
        binding.more.visibility = View.VISIBLE
        buttonHandler()
        binding.currentYearRecyclerView.setHasFixedSize(true)
        binding.previousYearRecyclerView.setHasFixedSize(true)

        viewModelObsr("")

        // Owner Selection
        binding.spinner1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (ownerItemList[position] == resources.getString(R.string.select_account)) {
                    binding.includeNoAccountView.itemContainer.visibility = View.VISIBLE
                    binding.rlMain.visibility = View.GONE
                    binding.includeNoAccountView.textView43.text =
                        "Select a user to display their items held."
                } else {
                    binding.includeNoAccountView.itemContainer.visibility = View.GONE
                    binding.rlMain.visibility = View.VISIBLE
                    viewModelObsr(ownerItemList[position])
                }

                UtilConstant.itemLastIndex = binding.spinner1.selectedItemPosition

                selectedName = binding.spinner1.selectedItem.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Nothing
            }
        }
        ownerSpinner()
    }

    @SuppressLint("SetTextI18n")
    private fun viewModelObsr(name: String) {

        val json = SharedPrefs.read(UtilConstant.ITEM_HELD, "")
        if (!json.isNullOrEmpty()) {
            setUpUI(gson.fromJson(json, ItemsHeldResponse::class.java), name)
        }
//        if (UtilsMethods.isInternetConnected(requireContext())) {
//            viewModel.getItemHeldResponse.observe(requireActivity()) { response ->
//                setUpUI(response, name)
//            }
//
//        } else {
//            val gson = Gson()
//            val json = SharedPrefs.read(UtilConstant.ITEM_HELD, "")
//            if (!json.isNullOrEmpty()) {
//                setUpUI(gson.fromJson(json, ItemsHeldResponse::class.java), name)
//            }
//        }
    }

    private fun setUpUI(response: ItemsHeldResponse?, name: String) {
        response?.let {
            if (it.responseCode == UtilConstant.SUCCESS_CODE && it.responseMessage == UtilConstant.SUCCESS_MSG) {

                listCurrentYearDetail.clear()
                listPreviousYearDetail.clear()
                ownerItemList.clear()
                ownerItemList.add(resources.getString(R.string.select_account))

                currentYear = "${
                    it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts?.get(UtilConstant.CURRENT_INDEX)?.itemsHeld?.get(
                        UtilConstant.CURRENT_INDEX
                    )?.year
                }"

                previousYear = "${
                    it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts?.get(UtilConstant.CURRENT_INDEX)?.itemsHeld?.get(
                        UtilConstant.PREVIOUS_INDEX
                    )?.year
                }"

                // YEAR Heading
                binding.yearHeader.text =
                    "$currentYear ${resources.getString(R.string.currentYearText)}"
                binding.previousYearHeading.text =
                    "$previousYear ${resources.getString(R.string.previousYearText)}"

                findAccount(it, name)
                recyclerContent(
                    previousList = listPreviousYearDetail,
                    currentList = listCurrentYearDetail
                )
            }
            setSpannableString()
        }
    }

    private fun findAccount(
        it: ItemsHeldResponse,
        name: String
    ) {
        for (item in it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts?.indices!!) {
            when (it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts!![item].accountType) {
                UtilConstant.MULTIPLE_ACCOUNT_KEY -> {
                    it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts!![item].owner.let { owner ->
                        getOwner(owner)
                    }
                    val responseList =
                        it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts!![item]

                    // current licence year
                    setOwnerItemDetailsCurrent(responseList, name)
                }
                else -> Log.e(TAG, "viewModelObsr: Primary account")
            }
        }
    }

    private fun setOwnerItemDetailsCurrent(
        responseList: Account,
        name: String
    ) {
        if (responseList.owner.equals(name)) {

            when (responseList.itemsHeld?.get(UtilConstant.CURRENT_INDEX)?.nonCarcassTagLicenses?.alsNo == null && responseList.itemsHeld?.get(
                UtilConstant.CURRENT_INDEX
            )?.permits?.permitData.isNullOrEmpty()) {
                true -> {
                    binding.noItemAvailable.noEtagPur.visibility =
                        View.VISIBLE
                }
                false -> {
                    binding.noItemAvailable.noEtagPur.visibility = View.GONE
                    binding.buyAndApply.buyApplyCard.visibility = View.VISIBLE
                }
            }
            if (responseList.itemsHeld?.get(UtilConstant.CURRENT_INDEX)?.nonCarcassTagLicenses?.alsNo == null) {
                binding.licenseCurrentYear.itemContainer.visibility = View.GONE
                binding.more.setOnClickListener {
                    responseList.itemsHeld?.get(UtilConstant.CURRENT_INDEX)?.year?.let { it1 ->
                        showPopupMenu(
                            binding.root.context, binding.more, "", "",
                            it1,
                        )
                    }
                }
            } else {
                val dob =
                    responseList.itemsHeld?.get(UtilConstant.CURRENT_INDEX)?.nonCarcassTagLicenses?.alsNo!!.substringBefore(
                        "-"
                    )
                val alsNo =
                    responseList.itemsHeld?.get(UtilConstant.CURRENT_INDEX)?.nonCarcassTagLicenses?.alsNo!!.substringAfter(
                        "-"
                    )
                val year = responseList.itemsHeld?.get(UtilConstant.CURRENT_INDEX)?.year
                val permitData = responseList.itemsHeld?.get(UtilConstant.CURRENT_INDEX)?.permits
                binding.more.setOnClickListener {
                    showPopupMenu(binding.root.context, binding.more, alsNo, dob, year!!)
                }
                binding.licenseCurrentYear.itemContainer.visibility = View.VISIBLE

                binding.licenseCurrentYear.year.text = "${
                    responseList.itemsHeld?.get(UtilConstant.CURRENT_INDEX)?.nonCarcassTagLicenses?.licenseYear
                }"
                binding.licenseCurrentYear.tvTagLicence.text = UtilConstant.LICENSE_LISTING
                binding.licenseCurrentYear.itemContainer.setOnClickListener {
                    val i = Intent(context, TagLicenceActivity::class.java)
                    i.putExtra("account", UtilConstant.MULTIPLE_ACCOUNT_KEY)
                    i.putExtra("ownerName", name)
                    requireActivity().startActivity(i)
                }

            }

            //  current permit year
            currentPermit(responseList, name)
            shownHidePreviousYearState(responseList)
            // previous licence year
            previousLicenceYear(responseList, name)


            //  previous permit year
            previousPermit(responseList, name)
        }
    }

    private fun previousPermit(
        responseList: Account,
        name: String
    ) {
        if (responseList.itemsHeld?.get(UtilConstant.PREVIOUS_INDEX)?.permits != null) {
            val permit =
                responseList.itemsHeld?.get(UtilConstant.PREVIOUS_INDEX)?.permits!!.permitData

            when (permit != null) {
                true -> {
                    binding.previousYearRecyclerView.visibility = View.VISIBLE
                    perviousPermitList(permit, responseList, name)
                }
                false -> {
                    binding.previousYearRecyclerView.visibility = View.GONE

                }
            }
        }
    }

    private fun perviousPermitList(
        permit: List<PermitData>,
        responseList: Account,
        name: String
    ) {
        for (i in permit.indices) {
            listPreviousYearDetail.add(
                ItemDetail(
                    "${
                        responseList.itemsHeld?.get(
                            UtilConstant.PREVIOUS_INDEX
                        )?.year
                    }",
                    permitName = permit[i].permit.toString(),
                    accountType = UtilConstant.MULTIPLE_ACCOUNT_KEY,
                    multiAccountOwner = name
                )
            )
        }
    }


    private fun shownHidePreviousYearState(responseList: Account) {

        when (responseList.itemsHeld?.get(UtilConstant.PREVIOUS_INDEX)?.nonCarcassTagLicenses?.alsNo == null && responseList.itemsHeld?.get(
            UtilConstant.PREVIOUS_INDEX
        )?.permits?.permitData.isNullOrEmpty()) {
            true -> {
                binding.noItemPrevYear.noEtagYear.visibility =
                    View.VISIBLE
            }
            false -> {
                binding.noItemPrevYear.noEtagYear.visibility = View.GONE
            }
        }

    }

    private fun previousLicenceYear(
        responseList: Account,
        name: String
    ) {
        if (responseList.itemsHeld?.get(UtilConstant.PREVIOUS_INDEX)?.nonCarcassTagLicenses?.alsNo == null && responseList.itemsHeld?.get(
                UtilConstant.PREVIOUS_INDEX
            )?.permits?.permitData.isNullOrEmpty()
        ) {
            binding.licensePrevYear.itemContainer.visibility = View.GONE
        } else {
            binding.licensePrevYear.itemContainer.visibility =
                View.VISIBLE

            binding.licensePrevYear.year.text =
                responseList.itemsHeld?.get(UtilConstant.PREVIOUS_INDEX)?.nonCarcassTagLicenses?.licenseYear
            binding.licensePrevYear.tvTagLicence.text = UtilConstant.LICENSE_LISTING
            binding.licensePrevYear.itemContainer.setOnClickListener {
                val i = Intent(context, TagLicenceActivity::class.java)
                i.putExtra("account", UtilConstant.MULTIPLE_ACCOUNT_KEY)
                i.putExtra(UtilConstant.ACCOUNT_TYPE, UtilConstant.PREVIOUS_YEAR)
                i.putExtra("ownerName", name)
                requireActivity().startActivity(i)
            }

//            listPreviousYearDetail.add(
//                ItemDetail(
//                    "${
//                        responseList.itemsHeld?.get(UtilConstant.PREVIOUS_INDEX)?.nonCarcassTagLicenses?.licenseYear
//                    }",
//                    permitName = UtilConstant.LICENSE_LISTING,
//                    accountType = UtilConstant.MULTIPLE_ACCOUNT_KEY,
//                    multiAccountOwner = name
//                )
//            )
        }
    }

    private fun currentPermit(
        responseList: Account,
        name: String
    ) {
        if (responseList.itemsHeld?.get(UtilConstant.CURRENT_INDEX)?.permits != null) {
            val permit =
                responseList.itemsHeld?.get(UtilConstant.CURRENT_INDEX)?.permits!!.permitData

            when (permit != null) {
                true -> {
                    binding.currentYearRecyclerView.visibility = View.VISIBLE

                    currentPermitList(permit, responseList, name)
                }
                false -> {
                    binding.currentYearRecyclerView.visibility = View.GONE

                }
            }
        }
    }

    private fun currentPermitList(
        permit: List<PermitData>,
        responseList: Account,
        name: String
    ) {
        for (i in permit.indices) {
            listCurrentYearDetail.add(
                ItemDetail(
                    "${
                        responseList.itemsHeld?.get(
                            UtilConstant.CURRENT_INDEX
                        )?.year
                    }",
                    permitName = permit[i].permit.toString(),
                    accountType = UtilConstant.MULTIPLE_ACCOUNT_KEY,
                    multiAccountOwner = name
                )
            )
        }
    }

    private fun getOwner(owner: String?) = if (owner != null && owner != "") {
        ownerItemList.add(owner)
    } else {
        Log.e(TAG, "viewModelObsr: Owner not fouund")
    }

    @OptIn(InternalPlatformTextApi::class)
    private fun setSpannableString() {
        val spannable =
            SpannableString(resources.getString(R.string.buy_string))

        val spanText =
            SpannableStringBuilder().append(resources.getString(R.string.noItemsPrevYearText1))
                .append(" $previousYear ")
                .append(resources.getString(R.string.noEtagsPrevYearText2))

        // Setting
        // font family
        val myTypeface =
            Typeface.create(
                ResourcesCompat.getFont(requireContext(), R.font.font_open_sens_bold),
                Typeface.BOLD
            )
        spannable.setSpan(
            TypefaceSpan(myTypeface),
            59,
            spannable.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )

        // Setting string
        // to textview
        binding.noItemAvailable.textView11.text = spannable
        binding.buyAndApply.textView11.text = spannable
        binding.noItemPrevYear.noEtagPurchasePre.text = spanText
        binding.noItemAvailable.noEtagPurchase.text =
            resources.getText(R.string.no_purchase_item)
    }

    private fun ownerSpinner() {
        val adapter = activity?.let { ArrayAdapter(it, R.layout.spinner_style, ownerItemList) }
        binding.spinner1.adapter = adapter
        binding.spinner1.setSelection(UtilConstant.itemLastIndex)
        /*   binding.spinner1.setSelection(0, false)
           binding.spinner1.getItemAtPosition(0)*/
    }


    private fun recyclerContent(previousList: List<ItemDetail>, currentList: List<ItemDetail>) {
        binding.currentYearRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity?.applicationContext)
            val itemHeldChildAdapter =
                ItemHeldPermitCurrentYearAdapter(currentList)
            adapter = itemHeldChildAdapter
        }
        binding.previousYearRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity?.applicationContext)
            val itemHelpPreviousYearAdapter = ItemHeldPermitPreviousYearAdapter(previousList)
            adapter = itemHelpPreviousYearAdapter
        }

    }

    private fun buttonHandler() {
        // hiding more  not in r1
        binding.more.visibility =View.VISIBLE

        binding.noItemAvailable.buyApplyButton.setOnClickListener {
            UtilsMethods.websiteOpener("https://fwp.mt.gov/buyandapply", requireActivity())
        }
        binding.buyAndApply.buyApplyButton.setOnClickListener {
            UtilsMethods.websiteOpener("https://fwp.mt.gov/buyandapply", requireActivity())
        }
        binding.buyAndApply.notSeeingYourPar.setOnClickListener {
            UtilsMethods.showDialerAlert(
                requireActivity(),
                requireContext(),
                "Not seeing your purchase?",
                "If you feel like an item you purchased is missing, call FWP support at 406-444-2950 during business hours (Monday – Friday, 8 a.m. – 5 p.m.)",
                "Close",
                resources.getString(R.string.callFwpText)
            )
        }
        binding.noItemAvailable.notSeeingYourPar.setOnClickListener {
            UtilsMethods.showDialerAlert(
                requireActivity(),
                requireContext(),
                "Not seeing your purchase?",
                "If you feel like an item you purchased is missing, call FWP support at 406-444-2950 during business hours (Monday – Friday, 8 a.m. – 5 p.m.)",
                "Close",
                resources.getString(R.string.callFwpText)
            )
        }
    }

    //   * waiting for approval
    private fun showPopupMenu(
        context: Context,
        view: View,
        als: String,
        dob: String,
        year: String
    ) {
        val wrapper: Context = ContextThemeWrapper(context, R.style.CustomPopUpStyle)
        val popup = PopupMenu(wrapper, view)
        popup.inflate(R.menu.request_copy_menu)
        popup.setOnMenuItemClickListener { item: MenuItem? ->
            when (item!!.itemId) {

                R.id.request_copy -> if (als.trim().isNullOrEmpty()) {
                    UtilsMethods.showSimpleAlert(
                        requireContext(),
                        "No items held",
                        "You do not currently hold any FWP Items for the ${year} license year.",
                        "Ok"
                    )
                } else {
                    val i = Intent(requireActivity(), RequestEmailActivity::class.java)
                    i.putExtra(UtilConstant.GET_ITEM_ACCOUNT, "2")
                    i.putExtra(UtilConstant.GET_ALS_NO, als)
                    i.putExtra(UtilConstant.GET_DOB, dob)
                    requireActivity().startActivity(i)
                    requireActivity().overridePendingTransition(
                        R.anim.slide_bottom_to_top,
                        R.anim.slide_top_to_bottom
                    )
                }
            }


            true
        }

        popup.show()
    }


    override fun onPause() {
        super.onPause()
        UtilConstant.ITEMS_LAST_VIEW_INDEX = 1
    }

}
