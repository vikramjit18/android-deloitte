/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fwp.deloittedctcustomerapp.data.db.AppDao
import com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
import com.fwp.deloittedctcustomerapp.data.model.requests.ChangePasswordRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ChangePasswordResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse
import com.fwp.deloittedctcustomerapp.data.repo.MainRepo
import com.fwp.deloittedctcustomerapp.utils.Event
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ResetPasswordViewModel @Inject constructor(private val mainRepo: MainRepo ,
                                                 private val appDao: AppDao
) : ViewModel() {

    private val _changePasswordResponse = MutableLiveData<ChangePasswordResponse>()
    val changePasswordResponse: LiveData<ChangePasswordResponse>
        get() = _changePasswordResponse


    private val _toast = MutableLiveData<Event<String>>()
    val toast: LiveData<Event<String>>
        get() = _toast

    private val _offloadResponse = MutableLiveData<DownloadOffloadResponse>()
    val offloadResponse: LiveData<DownloadOffloadResponse>
        get() = _offloadResponse
    val coroutineExceptionHandler = CoroutineExceptionHandler{_, t -> t.printStackTrace() }
    fun getDownloadedTagSid(): LiveData<List<DownloadedTags>> {
        return appDao.getDownloadedTags()
    }

    fun deleteDownloadedTagSid() {
        appDao.deleteDownloadedTagsTable()
    }

    fun cleardb() {
        viewModelScope.launch {
            appDao.deleteValidateRequest()
        }

    }


    fun changePassword(jwtToken: String, changePasswordRequest: ChangePasswordRequest) {
        viewModelScope.launch {
            val response = mainRepo.changePassword(jwtToken, changePasswordRequest)

            if (response.isSuccessful) {
                when (response.body()!!.responseMessage) {
                    UtilConstant.SUCCESS_MSG -> {
                        changePassRes(response)
                    }

                    else -> _toast.value =
                        response.body()!!.resObject?.get(0)?.message?.get(0)?.let { Event(it) }
                }
            } else {
                _toast.value = Event(response.message())
            }
        }
    }

    private fun changePassRes(response: Response<ChangePasswordResponse>) {
        when (response.body()!!.responseCode) {
            UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE ->
                _changePasswordResponse.value = response.body()

            UtilConstant.ERROR_CODE -> _toast.value =
                response.body()!!.resObject?.get(0)?.message?.get(0)
                    ?.let { Event(it) }
        }
    }

    @DelicateCoroutinesApi
    fun offLoadRequest(downloadOffloadRequest: DownloadOffloadRequest) {
        GlobalScope.launch (Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.downloadOffLoadEtag(downloadOffloadRequest)

            withContext(Dispatchers.Main)   {
                if (response.isSuccessful) {
                    when (response.body()!!.responseCode) {

                        UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE
                        -> _offloadResponse.value = response.body()

//                        UtilConstant.ERROR_CODE -> _toastViewUser.value =
//                            response.body()!!.resObject?.get(0)?.let { Event(it.toString())
//                            }
                    }

                } else {
//                    _toastDownloadOffload.value = Event(response.message())
                }
            }
        }
    }
}
