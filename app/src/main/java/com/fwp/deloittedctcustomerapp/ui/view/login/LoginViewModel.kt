/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fwp.deloittedctcustomerapp.data.model.requests.LoginRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.agreement.AgreemnetResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse
import com.fwp.deloittedctcustomerapp.data.repo.MainRepo
import com.fwp.deloittedctcustomerapp.utils.Event
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import retrofit2.Response
import javax.inject.Inject


@HiltViewModel
class LoginViewModel @Inject constructor(private val mainRepo: MainRepo) : ViewModel() {

    private val _userLoginResponse = MutableLiveData<LoginResponse>()
    val userLoginResponse: LiveData<LoginResponse>
        get() = _userLoginResponse

    private val _acceptTouResponse = MutableLiveData<Any>()
    val acceptTouResponse: LiveData<Any>
        get() = _acceptTouResponse

    private val _toastLoginResponse = MutableLiveData<Event<String>>()
    val toastLoginResponse: LiveData<Event<String>>
        get() = _toastLoginResponse

    private val _toastTouResponse = MutableLiveData<Event<String>>()
    val toastTouResponse: LiveData<Event<String>>
        get() = _toastTouResponse
    val coroutineExceptionHandler = CoroutineExceptionHandler { _, t -> t.printStackTrace() }

    fun loginUserRequest(loginRequest: LoginRequest) {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.login(loginRequest)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful && response.body() != null) {
                    when (response.body()!!.responseMessage) {
                        UtilConstant.SUCCESS_MSG -> {
                            loginResponse(response)
                        }
                    }

                } else {
                    _toastLoginResponse.value = Event(response.message())
                }
            }
        }
    }

    private fun loginResponse(response: Response<LoginResponse>) {
        when (response.body()!!.responseCode) {
            UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE ->
                _userLoginResponse.value = response.body()

            UtilConstant.ERROR_CODE ->
                _toastLoginResponse.value = Event(response.message())
        }
    }

    fun acceptTou(token: String) {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.acceptTOU(token)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful && response.body() != null) {
                    when (response.body()!!.responseMessage) {
                        UtilConstant.SUCCESS_MSG -> {
                            touResponse(response)
                        }
                    }
                } else {
                    _toastTouResponse.value = Event(response.message())
                }
            }
        }
    }

    private fun touResponse(response: Response<AgreemnetResponse>) {
        when (response.body()!!.responseCode) {
            UtilConstant.SUCCESS_CODE -> _acceptTouResponse.value =
                response.body()

            UtilConstant.FAIL_CODE -> _toastTouResponse.value =
                Event(
                    response.body()!!.resObject[0].message[0]
                )

            UtilConstant.ERROR_CODE -> _toastTouResponse.value =
                Event(response.message())
        }
    }
}
