/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.signup

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.compose.ui.text.android.InternalPlatformTextApi
import androidx.compose.ui.text.android.style.TypefaceSpan
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.SignUpActivateFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint

/**
 * FWP
 * Hilt entry point
 */
@AndroidEntryPoint
class SignUpActivate : Fragment() {

    /**
     * Global variables
     */
    lateinit var binding: SignUpActivateFragmentBinding
    private lateinit var viewModelCommunicator: Communicator

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.sign_up_activate_fragment, container, false)
        viewModelCommunicator = ViewModelProvider(requireActivity())[Communicator::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonProperty()
        setEmail()
        buttonHandler()
    }

    private fun buttonHandler() {
        binding.close.setOnClickListener {
            requireActivity().finish()
            requireActivity().overridePendingTransition(R.anim.slide_bottom_to_top, R.anim.slide_top_to_bottom)
        }

        binding.emailApp.btn.setOnClickListener {
            UtilsMethods.openEmailApp(requireActivity())
        }

        binding.notReceive.btn.setOnClickListener {
            showAlert()
        }
    }

    @OptIn(InternalPlatformTextApi::class)
    private fun showAlert() {

        val builder = AlertDialog.Builder(requireContext()).create()
        val view = layoutInflater.inflate(R.layout.custom_dialog, null)
        val leftButton = view.findViewById<TextView>(R.id.btn_left)
        val rightButton = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = getString(R.string.notReceiveEmailTitle)
        des.text =
            "Please check your spam folder or try searching your inbox for ‘MyFWP New User Confirmation’.\n\n If you still don’t see an email, call FWP support at 406-444-2950 during business hours (Monday – Friday, 8 a.m. – 5 p.m.)."
        leftButton.text = getString(R.string.callFwpText)
        rightButton.text = getString(R.string.close)
        builder.setView(view)
        leftButton.setOnClickListener {
            UtilsMethods.dialerOpener(getString(R.string.helpPhoneNoUri), requireActivity())
            builder.dismiss()
        }
        rightButton.setOnClickListener {
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    @OptIn(InternalPlatformTextApi::class)
    private fun setEmail() {
        var email: String = ""
        viewModelCommunicator.email.observe(requireActivity(),
            { o -> email = o!!.toString() })
        val myTypeface =
            Typeface.create(
                ResourcesCompat.getFont(requireContext(), R.font.opensreg),
                Typeface.BOLD
            )
        val spannable =
            SpannableString("To activate your MyFWP account, you need to select the confirmation link we sent to $email\n\nThe activation link will expire in 24 hours.")
        spannable.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.white, null)),
            84,
            84 + email.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE,
        )
        spannable.setSpan(
            TypefaceSpan(myTypeface),
            84,
            84 + email.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )

        binding.contentSign.text = spannable
    }

    private fun buttonProperty() {
        binding.emailApp.btn.text = resources.getString(R.string.openEmailAppText)
        binding.emailApp.btn.setTextColor(resources.getColor(R.color.black, null))
        binding.emailApp.btn.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))

        binding.notReceive.btn.text = resources.getString(R.string.notReceiveEmail)
        binding.notReceive.btn.setTextColor(resources.getColor(R.color.prim_yellow, null))
        binding.notReceive.btn.setBackgroundColor(resources.getColor(R.color.bgColor, null))
        binding.notReceive.btn.typeface =
            ResourcesCompat.getFont(requireContext(), R.font.font_mont_semi_bold)
        binding.notReceive.btn.stateListAnimator = null
        binding.notReceive.btn.elevation = 0F
    }
}
