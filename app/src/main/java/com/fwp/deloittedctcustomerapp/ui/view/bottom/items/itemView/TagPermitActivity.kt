/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.ActivityTagPermitBinding
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.PermitData
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.CURRENT_INDEX
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.ITEM_POSITION
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.PREVIOUS_INDEX
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

private const val TAG = "TagPermitActivity"

@AndroidEntryPoint
class TagPermitActivity : AppCompatActivity() {

    lateinit var binding: ActivityTagPermitBinding
    private lateinit var viewModel: LandingScreenActivityViewModel
    private lateinit var loadingDialog: LoadingDialog
    private var itemPosition: Int = CURRENT_INDEX
    private var year: String = ""
    private var name = ""
    private var accountType = "P"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tag_permit)

        viewModel = ViewModelProvider(this)[LandingScreenActivityViewModel::class.java]

        loadingDialog = LoadingDialog(this)
        SharedPrefs.init(this)
        itemPosition = intent.getIntExtra(ITEM_POSITION, CURRENT_INDEX)
        year = intent.getStringExtra(UtilConstant.ACCOUNT_TYPE).toString()
        accountType = intent.getStringExtra("account").toString()
        name = intent.getStringExtra("ownerName").toString()
        binding.tv22.text =
            "This permit and your valid license must be in your possession to hunt, take, or kill as stated under the limitations hereon.\n" +
                "\n" +
                "As required by law, your carcass must be tagged with your valid carcass tag and not this special permit."

        apiHit()
        buttonHandler()
    }

    private fun apiHit() {

        loadingDialog.startLoading()
        val gson = Gson()
        val json = SharedPrefs.read(UtilConstant.ITEM_HELD, "")
        if (!json.isNullOrEmpty()) {
            val permitObj = gson.fromJson(json, ItemsHeldResponse::class.java)
            setupView(permitObj)
        }

    }


    @SuppressLint("SetTextI18n")
    private fun setupView(response: ItemsHeldResponse?) {

        response?.let {
            if (accountType == UtilConstant.MULTIPLE_ACCOUNT_KEY) {

                multiAccountUIBind(it)
            } else {

                primaryAccountUIBind(it)
            }
        }
        loadingDialog.isDismiss()
    }

    private fun primaryAccountUIBind(it: ItemsHeldResponse) {
        // current
        if (year != UtilConstant.PREVIOUS_YEAR) {
            val permitResponse =
                it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                    CURRENT_INDEX
                )?.permits?.permitData?.get(itemPosition)
            binding.head.text = "${
                it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                    CURRENT_INDEX
                )?.year
            } ${permitResponse?.permit}"

            setPermitGuidline(permitResponse)

            binding.tvOwnerName.text = permitResponse?.owner

            dateTimeIssueInFormatter(permitResponse)

            binding.tvAlsNumber.text = permitResponse?.alsNo

            binding.tvDescription.text = permitResponse?.speciesDescription

            binding.tvOpportunity.text = permitResponse?.designatedArea

            binding.regulationLink.text = permitResponse?.regulationsSection2?.get(0)

            binding.regulation1.text = permitResponse?.regulationsSection1?.get(0)

            regulationLinkSetup()

            binding.regulation1.text = "Hunting must be done in accordance with the ${
                it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                    CURRENT_INDEX
                )?.year
            } season hunting regulations."
        }
        // previous
        else {
            val permitResponse =
                it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                    PREVIOUS_INDEX
                )?.permits?.permitData?.get(
                    itemPosition
                )

            binding.head.text =
                "${
                    it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                        PREVIOUS_INDEX
                    )?.year
                } ${permitResponse?.permit}"

            /*       // Spannable
                           setSpannableString(
                               "${permitResponse?.speciesName}", "${
                                   it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                                       PREVIOUS_INDEX
                                   )?.year
                               }"
                           )*/

            setPermitGuidline(permitResponse)

            binding.tvOwnerName.text = permitResponse?.owner

            dateTimeIssueInFormatter(permitResponse)

            binding.tvAlsNumber.text = permitResponse?.alsNo

            binding.tvDescription.text = permitResponse?.speciesDescription

            binding.tvOpportunity.text = permitResponse?.designatedArea

            binding.regulationLink.text = permitResponse?.regulationsSection2?.get(0)

            /*       if (!permitResponse?.regulationsSection1?.get(0).isNullOrEmpty()) {
                        binding.regulation1.text =
                            permitResponse?.regulationsSection1?.get(0).toString()
                        binding.regulation1.visibility = View.VISIBLE
                    }*/
            binding.regulation1.text =
                "Hunting must be done in accordance with the ${
                    it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                        PREVIOUS_INDEX
                    )?.year
                } season hunting regulations."

            regulationLinkSetup()
        }
    }

    private fun multiAccountUIBind(it: ItemsHeldResponse) {
        // current
        if (year != UtilConstant.PREVIOUS_YEAR) {
            multiCurrentYearUIBind(it)
        }
        // previous
        else {
            multiPreviousYearUIBind(it)
        }
    }

    private fun multiPreviousYearUIBind(it: ItemsHeldResponse) {
        for (item in it.resObject?.get(CURRENT_INDEX)?.accounts?.indices!!) {
            when (it.resObject?.get(CURRENT_INDEX)?.accounts!![item].accountType) {
                UtilConstant.MULTIPLE_ACCOUNT_KEY -> {
                    val responseList =
                        it.resObject?.get(CURRENT_INDEX)?.accounts!![item]

                    ownerMultiPreviousSetup(responseList, it, item)
                }
            }
        }
    }

    private fun ownerMultiPreviousSetup(
        responseList: Account,
        it: ItemsHeldResponse,
        item: Int
    ) {
        if (responseList.owner.equals(name)) {

            val permitResponse =
                it.resObject?.get(CURRENT_INDEX)?.accounts?.get(
                    item
                )?.itemsHeld?.get(
                    PREVIOUS_INDEX
                )?.permits?.permitData?.get(itemPosition)

            // Heading
            binding.head.text =
                "${
                    it.resObject?.get(CURRENT_INDEX)?.accounts?.get(item)?.itemsHeld?.get(
                        PREVIOUS_INDEX
                    )?.year
                } ${permitResponse?.permit}"


            // Spannable

            setPermitGuidline(permitResponse)
            /*    setSpannableString(
                                            "${permitResponse?.speciesName}", "${
                                                it.resObject?.get(CURRENT_INDEX)?.accounts?.get(
                                                    item
                                                )?.itemsHeld?.get(
                                                    PREVIOUS_INDEX
                                                )?.year
                                            }"
                                        )*/

            binding.tvOwnerName.text = permitResponse?.owner

            dateTimeIssueInFormatter(permitResponse)

            binding.tvAlsNumber.text = permitResponse?.alsNo

            binding.tvDescription.text = permitResponse?.speciesDescription

            binding.tvOpportunity.text = permitResponse?.designatedArea

            binding.regulationLink.text =
                permitResponse?.regulationsSection2?.get(0)

            /*              if (!permitResponse?.regulationsSection1?.get(0)
                                            .isNullOrEmpty()
                                    ) {
                                        binding.regulation1.text =
                                            permitResponse?.regulationsSection1?.get(0).toString()
                                        binding.regulation1.visibility = View.VISIBLE
                                    }*/
            binding.regulation1.text =
                "Hunting must be done in accordance with the ${
                    it.resObject?.get(CURRENT_INDEX)?.accounts?.get(item)?.itemsHeld?.get(
                        PREVIOUS_INDEX
                    )?.year
                } season hunting regulations."

            regulationLinkSetup()
        }
    }

    private fun multiCurrentYearUIBind(it: ItemsHeldResponse) {
        for (item in it.resObject?.get(CURRENT_INDEX)?.accounts?.indices!!) {
            when (it.resObject?.get(CURRENT_INDEX)?.accounts!![item].accountType) {
                UtilConstant.MULTIPLE_ACCOUNT_KEY -> {
                    val responseList =
                        it.resObject?.get(CURRENT_INDEX)?.accounts!![item]

                    ownerCurrentYearMutilSetup(responseList, it, item)
                }
            }
        }
    }

    private fun ownerCurrentYearMutilSetup(
        responseList: Account,
        it: ItemsHeldResponse,
        item: Int
    ) {
        if (responseList.owner.equals(name)) {

            val permitResponse =
                it.resObject?.get(CURRENT_INDEX)?.accounts?.get(
                    item
                )?.itemsHeld?.get(
                    CURRENT_INDEX
                )?.permits?.permitData?.get(itemPosition)

            // Heading
            binding.head.text =
                "${
                    it.resObject?.get(CURRENT_INDEX)?.accounts?.get(item)?.itemsHeld?.get(
                        CURRENT_INDEX
                    )?.year
                } ${permitResponse?.permit}"


            setPermitGuidline(permitResponse)
            /*    // Spannable
                                        setSpannableString(
                                            "${permitResponse?.speciesName}", "${
                                                it.resObject?.get(CURRENT_INDEX)?.accounts?.get(item)?.itemsHeld?.get(
                                                    CURRENT_INDEX
                                                )?.year
                                            }"
                                        )*/

            binding.tvOwnerName.text = permitResponse?.owner

            dateTimeIssueInFormatter(permitResponse)


            binding.tvAlsNumber.text = permitResponse?.alsNo

            binding.tvDescription.text = permitResponse?.speciesDescription

            binding.tvOpportunity.text = permitResponse?.designatedArea

            binding.regulationLink.text =
                permitResponse?.regulationsSection2?.get(0)

            /* if (!permitResponse?.regulationsSection1?.get(0)
                                             .isNullOrEmpty()
                                     ) {
                                         binding.regulation1.text =
                                             permitResponse?.regulationsSection1?.get(0).toString()
                                         binding.regulation1.visibility = View.VISIBLE
                                     }*/

            binding.regulation1.text =
                "Hunting must be done in accordance with the ${
                    it.resObject?.get(CURRENT_INDEX)?.accounts?.get(item)?.itemsHeld?.get(
                        CURRENT_INDEX
                    )?.year
                } season hunting regulations."

            regulationLinkSetup()
        }
    }

    private fun regulationLinkSetup() {
        if (!binding.regulationLink.text.isNullOrEmpty()) {
            binding.regulationLink.text = UtilsMethods.stringSpannable(
                this,
                binding.regulationLink.text as String,
                R.drawable.ic_buy_yellow,
                true
            )
        }
    }

    private fun dateTimeIssueInFormatter(permitResponse: PermitData?) {
        when (!permitResponse?.issuedDate.isNullOrEmpty()) {
            true -> {
                val issueDT = permitResponse?.issuedDate!!.split(" ")

                if (issueDT.contains("PM"))
                    binding.tvIssuedDt.text =
                        "${issueDT[0]} - ${issueDT[1]} p.m."
                else
                    binding.tvIssuedDt.text =
                        "${issueDT[0]} - ${issueDT[1]} a.m."
            }

            false -> binding.tvIssuedDt.text =
                permitResponse?.issuedDate
        }
    }

    private fun setPermitGuidline(permitResponse: PermitData?) {
        when (permitResponse?.speciesName!!.lowercase()) {
            "deer" -> {
                binding.tv22.visibility = View.VISIBLE
            }
            "elk" -> {
                binding.tv22.visibility = View.VISIBLE
            }
            else -> {
                binding.tv22.visibility = View.GONE
            }

        }
    }

    private fun buttonHandler() {
        binding.close.setOnClickListener {
            UtilsMethods.activityBack(this)
        }

        binding.regulationLink.setOnClickListener {
            UtilsMethods.websiteOpener(binding.regulationLink.text.toString().trim(), this)
        }
    }

/*    @OptIn(InternalPlatformTextApi::class)
    private fun setSpannableString(name: String, year: String) {
        val spannable =
            SpannableString("The $name permit and your valid $year $name License must be in your possession to hunt, take, or kill $name as stated under the limitations hereon.\n\nAs required by law, the E-tag for your $year eld carcass must be validated and not this special $name permit")

        // setting custom string to TV
        binding.tv22.text = spannable
    }*/
}
