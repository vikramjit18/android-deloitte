/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.linkedAccountAdapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagAccounts
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.ValidateTagsActivity
import com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator
import com.fwp.deloittedctcustomerapp.utils.*
import com.google.android.material.button.MaterialButton


// FWP
/*var validButtonState = ArrayList<Boolean>()
var isValidated = ArrayList<Boolean>()*/
private var model: Communicator? = null

class MultipleEtagPreviousYearAdapter(
    private val list: EtagAccounts,
    private val oflineSpiidList: List<String>
) :
    RecyclerView.Adapter<MultipleEtagPreviousYearAdapter.MyMultiPreviousYearViewHolder>() {


    private lateinit var mListener: OnMultiPreviousClickListener

    interface OnMultiPreviousClickListener {
        fun onMultiPreviousClick(position: Int, holder: MyMultiPreviousYearViewHolder)
    }

    fun setOnMultiPreviousDownloadClick(listenerMulti: OnMultiPreviousClickListener) {
        mListener = listenerMulti
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyMultiPreviousYearViewHolder {
        val binding = LayoutInflater.from(parent.context)
            .inflate(R.layout.etags_available_child_row, parent, false)

        return MyMultiPreviousYearViewHolder(binding, mListener)
    }

    class MyMultiPreviousYearViewHolder(
        viewHolder: View,
        listener: MultipleEtagPreviousYearAdapter.OnMultiPreviousClickListener
    ) :
        RecyclerView.ViewHolder(viewHolder) {

        val itemContainer: ConstraintLayout = viewHolder.findViewById(R.id.relativeLayout)
        val tagImage: ImageView = viewHolder.findViewById(R.id.tagImage)
        val yearAnimalType: TextView = viewHolder.findViewById(R.id.year_animal_type)
        val region: TextView = viewHolder.findViewById(R.id.region)
        val etagContainer: FrameLayout = viewHolder.findViewById(R.id.etagStateContainer)
        val downloadBtnContainer: LinearLayout =
            viewHolder.findViewById(R.id.downloadButtonContainer)
        val etagValidate: View = viewHolder.findViewById(R.id.etagValidated)
        val etagVoid: View = viewHolder.findViewById(R.id.etag_void)
        val etagRefund: View = viewHolder.findViewById(R.id.etag_refunded)
        val etagExpire: View = viewHolder.findViewById(R.id.etag_expire)
        val iconDownloadBtn: MaterialButton = viewHolder.findViewById(R.id.iconDownloadButton)
        val iconProgressBtn: CardView = viewHolder.findViewById(R.id.iconProgressButton)
        val validateBtn: Button = viewHolder.findViewById(R.id.validateButton)

        init {
            iconDownloadBtn.setOnClickListener {

                when (UtilsMethods.isInternetConnected(viewHolder.context)) {
                    true -> {
                        listener.onMultiPreviousClick(adapterPosition, MyMultiPreviousYearViewHolder(viewHolder, listener))

                    }

                    false -> {
                        UtilsMethods.showSimpleAlert(
                            mContext = viewHolder.context,
                            tvPrimary = "No connection",
                            tvSecondary = "Check your network connection and try again.",
                            primaryBtn = "Ok"

                        )
                    }
                }

            }
        }
    }

    override fun onBindViewHolder(
        holder: MyMultiPreviousYearViewHolder,
        position: Int
    ) {
        holder.validateBtn.setText(R.string.validate)
        val multiAccountList =
            list.etags!![UtilConstant.PREVIOUS_INDEX].etagLicensesList?.get(position)
        val tagImageId = holder.tagImage
        holder.yearAnimalType.text =
            "${list.etags?.get(UtilConstant.PREVIOUS_INDEX)?.licenseYear} ${
                list.etags!![UtilConstant.PREVIOUS_INDEX].etagLicensesList?.get(
                    position
                )?.etagLicenseDetails?.etagsubDetails?.licenseName
            }"
        holder.region.text =
            list.etags!![1].etagLicensesList?.get(position)?.etagLicenseDetails?.districtCode
// list tag icon
        list.etags!![1].etagLicensesList?.get(position)?.etagLicenseDetails?.imageUrl!!.let {
            TagImageInflater().showListTagIcon(
                context = holder.itemView.context,
                imageView = tagImageId,
                heroName = it
            )
        }


// is download buttn hide and show
            when (multiAccountList?.etagLicenseDetails!!.isEtagActive) {
                EtagActiveStatus.YES -> {
                    holder.downloadBtnContainer.visibility = View.VISIBLE

                    checkingValidationOffline(multiAccountList, holder)
                }
                EtagActiveStatus.NO -> {
                    holder.downloadBtnContainer.visibility = View.GONE
//                    holder.etagContainer.visibility = View.VISIBLE
                }
            }


        // Download Status
        when (multiAccountList!!.etagLicenseDetails?.downloadStatus!!) {
            DownloadStatus.SAME_DEVICE -> {
                holder.iconDownloadBtn.isClickable = false
                holder.iconDownloadBtn.setIconResource(R.drawable.ic_download_done)
                holder.iconDownloadBtn.setIconTintResource(R.color.white)
                holder.iconDownloadBtn.setStrokeColorResource(R.color.black)
                holder.iconDownloadBtn.setBackgroundColor(
                    holder.itemView.context.getColor(
                        R.color.black
                    )
                )
                enableValiBtn(holder)

            }

            DownloadStatus.OTHER_DEVICE -> {
                holder.iconDownloadBtn.isClickable = true
                holder.iconDownloadBtn.visibility = View.VISIBLE
                holder.iconDownloadBtn.setIconResource(R.drawable.ic_warning_yellow)
                holder.iconDownloadBtn.setIconTintResource(R.color.prim_yellow)
                /*        binding.downloaded.setBackgroundColor(
                            requireContext().getColor(
                                R.color.black
                            )
                        )*/
                disableValiBtn(holder)
            }


            DownloadStatus.NO_DOWNLOAD -> {
                holder.iconDownloadBtn.setIconResource(R.drawable.ic_dowload_yellow)
                holder.iconDownloadBtn.setIconTintResource(R.color.prim_yellow)
                holder.iconDownloadBtn.isCheckable = true

                disableValiBtn(holder)
            }

        }


        // show download container when etageactive is Y
        when (list.etags!![UtilConstant.PREVIOUS_INDEX].etagLicensesList?.get(position)?.etagLicenseDetails?.isEtagActive) {
            EtagActiveStatus.YES -> {
                holder.downloadBtnContainer.visibility = View.VISIBLE
                holder.etagContainer.visibility = View.GONE
            }
            EtagActiveStatus.NO -> {
                holder.downloadBtnContainer.visibility = View.GONE
                holder.etagContainer.visibility = View.VISIBLE
            }
        }

        //  in new update for tag status
        //todo in new update   tag status
        if (list.etags!![UtilConstant.PREVIOUS_INDEX].etagLicensesList?.get(position)?.etagLicenseDetails?.etagsubDetails?.status?.isNotEmpty() == true) {
            list.etags!![UtilConstant.PREVIOUS_INDEX].etagLicensesList?.get(position)?.etagLicenseDetails?.etagsubDetails?.status?.forEach {
                when (it) {
                    TagStatus.VALIDATED -> holder.etagValidate.visibility =
                        View.VISIBLE
//                    TagStatus.ISSUE -> holder.viewDataBinding.etagIssue.stateContainer.visibility = View.VISIBLE

//                TagStatus.EXPIRED -> holder.viewDataBinding.etagExpire.stateContainer.visibility = View.VISIBLE
                    TagStatus.REFUNDED -> holder.etagRefund.visibility = View.VISIBLE
//                TagStatus.VOID -> holder.viewDataBinding.etagVoid.stateContainer.visibility = View.VISIBLE
                }
            }
        }


        //todo in new update   tag status
        if (multiAccountList!!.etagLicenseDetails?.etagsubDetails?.status!!.isNotEmpty()) {
            multiAccountList!!.etagLicenseDetails?.etagsubDetails?.status!!.forEach {
                when (it) {
                    TagStatus.VALIDATED -> {
                        holder.etagValidate.visibility = View.VISIBLE
                        holder.etagRefund.visibility = View.GONE
                    }
                    /*    TagStatus.ISSUE -> holder.viewDataBinding.etagIssue.stateContainer.visibility = View.VISIBLE
                        TagStatus.EXPIRED -> holder.viewDataBinding.etagExpire.stateContainer.visibility = View.VISIBLE*/
                    TagStatus.REFUNDED -> {
                        holder.etagRefund.visibility = View.VISIBLE
                        holder.etagValidate.visibility = View.GONE
                    }
//                    TagStatus.VOID -> holder.viewDataBinding.etagVoid.stateContainer.visibility = View.VISIBLE
                }
            }
        }
        viewDetails(holder, position, list.owner.toString())
    }

    private fun checkingValidationOffline(
        multiAccountList: EtagLicenses,
        holder: MyMultiPreviousYearViewHolder
    ) {
        when ((oflineSpiidList.contains(
            multiAccountList.etagLicenseDetails!!.spiId
        ))) {
            true -> {
                holder.downloadBtnContainer.visibility = View.GONE
                holder.etagContainer.visibility = View.VISIBLE
                holder.etagValidate.visibility = View.VISIBLE
            }
            false -> {
    //                holder.downloadBtnContainer.visibility = View.VISIBLE
                holder.etagContainer.visibility = View.GONE

            }
        }
    }


    /* private fun enableBtn(holder: MyMultiPreviousYearViewHolder, position: Int) {
         val validatorButtonId = holder.viewDataBinding.validateButton
         val downloadIconButtonLayout = holder.viewDataBinding.iconDownloadButton
         validatorButtonId.setText(R.string.validate)
         validatorButtonId.isEnabled = true
         validatorButtonId.isClickable = true
         validatorButtonId.setTextColor(holder.itemView.context.getColor(R.color.black))
         validatorButtonId.setBackgroundColor(
             holder.itemView.context.getColor(
                 R.color.prim_yellow
             )
         )
         downloadIconButtonLayout.setBackgroundColor(
             holder.itemView.context.getColor(
                 R.color.black
             )
         )
         downloadIconButtonLayout.setStrokeColorResource(R.color.black)
         downloadIconButtonLayout.setIconResource(R.drawable.ic_download_done)
 //        validatorButtonId.setOnClickListener {
 //            UtilsMethods.snackBarCustom(
 //                holder.itemView,
 //                "$position",
 //                holder.itemView.context.getColor(R.color.white),
 //                holder.itemView.context.getColor(R.color.black)
 //            )
 //        }
     }

     private fun disableBtn(holder: MultipleEtagPreviousYearAdapter.MyMultiPreviousYearViewHolder, position: Int) {
         val validatorButtonId = holder.viewDataBinding.validateButton
         val downloadIconButtonLayout = holder.viewDataBinding.iconDownloadButton
         validatorButtonId.setText(R.string.validate)
         validatorButtonId.isEnabled = false
         validatorButtonId.isClickable = false
         validatorButtonId.setTextColor(holder.itemView.context.getColor(R.color.white))
         validatorButtonId.setBackgroundColor(holder.itemView.context.getColor(R.color.buttonTrans))
 //        downloadIconButtonLayout.setOnClickListener {
 //
 //            //todo on click db
 //            UtilsMethods.snackBarCustom(
 //                holder.itemView,
 //                " $position",
 //                holder.itemView.context.getColor(R.color.white),
 //                holder.itemView.context.getColor(R.color.black)
 //            )
 //        }
     }
 */

    override fun getItemCount(): Int {
        return list.etags!![1].etagLicensesList!!.size
    }

    private fun viewDetails(
        holder: MyMultiPreviousYearViewHolder,
        position: Int,
        owner: String
    ) {
        holder.itemContainer.setOnClickListener {
            val i = Intent(holder.itemView.context, ValidateTagsActivity::class.java)
            i.putExtra(UtilConstant.GET_ETAG_INDEX_KEY, position)
            i.putExtra(UtilConstant.GET_ETAG_ACCOUNT_OWNER, owner)
            i.putExtra(UtilConstant.GET_ETAG_YEAR_KEY, 1)
            holder.itemView.context.startActivity(i)
        }
        holder.validateBtn.setOnClickListener {
            val i = Intent(holder.itemView.context, ValidateTagsActivity::class.java)
            i.putExtra(UtilConstant.GET_ETAG_INDEX_KEY, position)
            i.putExtra(UtilConstant.GET_ETAG_ACCOUNT_OWNER, owner)
            i.putExtra(UtilConstant.GET_ETAG_YEAR_KEY, 1)
            holder.itemView.context.startActivity(i)
        }
    }


    private fun enableValiBtn(holder: MyMultiPreviousYearViewHolder) {
        val validatorButtonId = holder.validateBtn
        validatorButtonId.setText(R.string.validate)
        validatorButtonId.isEnabled = true
        validatorButtonId.isClickable = true
        validatorButtonId.setTextColor(holder.itemView.context.getColor(R.color.black))
        validatorButtonId.setBackgroundColor(
            holder.itemView.context.getColor(
                R.color.prim_yellow
            )
        )
    }

    private fun disableValiBtn(holder: MyMultiPreviousYearViewHolder) {
        val validatorButtonId = holder.validateBtn

        validatorButtonId.setText(R.string.validate)

        validatorButtonId.isEnabled = false
        validatorButtonId.isClickable = false
        validatorButtonId.setTextColor(holder.itemView.context.getColor(R.color.white))
        validatorButtonId.setBackgroundColor(holder.itemView.context.getColor(R.color.buttonTrans))
    }
}
