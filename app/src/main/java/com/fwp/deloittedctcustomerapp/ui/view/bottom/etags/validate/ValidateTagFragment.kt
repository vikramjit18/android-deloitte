/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import com.fwp.deloittedctcustomerapp.databinding.ValidateTagFragmentBinding
import android.widget.PopupMenu
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.fwp.deloittedctcustomerapp.R
import androidx.lifecycle.ViewModelProvider
import com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
import com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.*
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel
import com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator
import com.fwp.deloittedctcustomerapp.utils.*
import com.google.gson.Gson
import java.net.SocketTimeoutException
import java.util.regex.Matcher
import javax.net.ssl.SSLException

private const val TAG = "ValidateTagFragment"

class ValidateTagFragment : Fragment() {

    companion object {
        fun newInstance() = ValidateTagFragment()
    }

    //    private var currentDownloadStatus: String? = nulldesRegulation
//    private var downloadNo: String? = null
    private var issueDateAndTime: List<String>? = null
    private var dateAndTime: List<String>? = null
    private var eTagYearIndex: Any? = null
    private var accountOwner: Any? = null
    private var position: Any? = null
    private var animalName: Any? = null
    private var animalregion: Any? = null
    private var extras: Bundle? = null
    private var _binding: ValidateTagFragmentBinding? = null
    private lateinit var communicator: Communicator
    private lateinit var loadingDialog: LoadingDialog
    private val binding get() = _binding!!
    private val spiIDLIst = mutableListOf<String>()
    private val offlineSpiidList = mutableListOf<String>()
    private val etagValiRequest = mutableListOf<ValidateEtagRequest>()
    private val tagStatus = mutableListOf<String>()
    private val downloadStatus = mutableListOf<String>()
    private lateinit var validateTagActivityViewModel: ValidateTagActivityViewModel
    private val gson = Gson()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ValidateTagFragmentBinding.inflate(inflater, container, false)
        val root = binding.root
        extras = requireActivity().intent.extras
        validateTagActivityViewModel =
            ViewModelProvider(requireActivity())[ValidateTagActivityViewModel::class.java]
        communicator = ViewModelProvider(requireActivity())[Communicator::class.java]

        loadingDialog = LoadingDialog(requireActivity())


        if (UtilsMethods.isInternetConnected(requireContext())) {
            loadingDialog.startLoading()
            validateTagActivityViewModel.getEtag(EtagRequest(UtilConstant.DEVICE_ID))
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonHandler()

        // Getting Intent data

        position = extras?.get(UtilConstant.GET_ETAG_INDEX_KEY)
        accountOwner = extras?.get(UtilConstant.GET_ETAG_ACCOUNT_OWNER)
        eTagYearIndex = extras?.get(UtilConstant.GET_ETAG_YEAR_KEY)
        animalName = extras?.get(UtilConstant.GET_ANIMAL_NAME)
        animalregion = extras?.get(UtilConstant.GET_ANIMAL_REGION)
        binding.validateButton.btn.setText(R.string.validate)

        validateTagActivityViewModel.getValidateTagData().observe(requireActivity()) {
            etagValiRequest.clear()
            offlineSpiidList.clear()
            it.forEach { itRes ->
                etagValiRequest.add(itRes)
                itRes.spiId?.let { it1 -> offlineSpiidList.add(it1) }
                Log.e("validate ", "onViewCreated: from db spid $offlineSpiidList")
            }
            if (UtilsMethods.isInternetConnected(requireContext())) {
                validateTagActivityViewModel.etagResponse.observe(requireActivity()) {
                    when (it.responseCode) {
                        UtilConstant.SUCCESS_CODE -> {
                            validateTagActivityViewModel.deleteValiData()
                            loadingDialog.isDismiss()
//                            GetLocation(requireActivity())
                            validateUISetup(it)

                        }
                    }
                }
            } else {
                val json = SharedPrefs.read(UtilConstant.USER_ETAG, "")
                if (!json.isNullOrEmpty()) {
                    validateUISetup(gson.fromJson(json, EtagResponse::class.java))
                }
            }
        }
    }

    private fun initImage(imagename: String) {
        // initialization of constraint
        //when -> step1 complete -> change ic to success ic,

        val heroImageId = binding.bgImagem

        TagImageInflater().showDetailedTagImage(
            context = requireContext(),
            imageView = heroImageId,
            heroName = imagename
        )

    }

    private fun initTagicon(iconName: String) {
        val heroIconId = binding.heroIc
        TagImageInflater().showDetailedTagIcon(
            context = requireContext(),
            imageView = heroIconId,
            heroName = iconName
        )
    }

    private fun buttonHandler() {
        binding.back.setOnClickListener {
            requireActivity().finish()
            UtilConstant.backStatus = true
        }

        binding.tvViewAllSponsors.setOnClickListener {

            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.action_validateTagFragment_to_viewAllSponsorsFrag2
            )
        }

        binding.customPopup1.close.setOnClickListener {
            binding.clToolTip.visibility = View.GONE
        }
        binding.ibAlertHarvestTime.setOnClickListener {
            UtilsMethods.showSimpleAlert(
                requireContext(),
                resources.getString(R.string.timestamp),
                resources.getString(R.string.harvest_alert_des),
                resources.getString(R.string.ok)
            )
        }

        binding.ibAlertDownload.setOnClickListener {
            UtilsMethods.showSimpleAlert(
                requireContext(),
                resources.getString(R.string.alert_download_title),
                resources.getString(R.string.alert_download_des),
                resources.getString(R.string.ok),
            )
        }


    }


    private fun apiResponseMandatory(eTagMandatoryDetails: List<ETagMandatoryDetail>?) {
        if (eTagMandatoryDetails != null && eTagMandatoryDetails[0].reReportingHTMLText?.isNotEmpty() == true) {
            binding.mandatoryContainer.clMandatory.visibility = View.VISIBLE
            eTagMandatoryDetails.forEach {
                when (it.rsReportingStep) {
                    "1" -> {
                        val step1Heading = "Step 1 – ${
                            eTagMandatoryDetails[0].reReportingHTMLText!!.substringBefore("|")
                        }"
                        binding.mandatoryContainer.step1Heading.text = step1Heading
                        binding.mandatoryContainer.step1Desc.visibility = View.GONE
                    }
                    "2" -> {
                        var result = ""
                        val step2Heading = "Step 2 – ${
                            eTagMandatoryDetails[1].reReportingHTMLText!!.substringBefore("|")
                        }"
                        binding.mandatoryContainer.step2Heading.text = step2Heading
                        binding.mandatoryContainer.step2Desc.text =
                            eTagMandatoryDetails[1].reReportingHTMLText!!.substringAfter("|")
                                .substringBefore("\n\nBy phone:")
                        binding.mandatoryContainer.step2Desc.visibility = View.VISIBLE
//                  Phone no set
                        val no =   eTagMandatoryDetails[1].reReportingHTMLText!!.substringAfter("\n\nBy phone:\n").substringBefore("\n\nOnline:")
                        if (!no.isNullOrEmpty() && UtilConstant.PHONE_PATTERN.matcher(no).matches()) {
                            binding.mandatoryContainer.step2DescCall1.visibility = View.VISIBLE
                            binding.mandatoryContainer.step2DescCall2.visibility = View.VISIBLE
                            val contentUnderLine = SpannableString(no)
                            contentUnderLine.setSpan(
                                UnderlineSpan(),
                                0,
                                no.length,
                                0
                            )
                            binding.mandatoryContainer.step2DescCall2.text = contentUnderLine
                            binding.mandatoryContainer.step2DescCall2.setOnClickListener {
                                UtilsMethods.dialerOpener(
                                    "1$no",
                                    requireActivity()
                                )
                            }
                        }

                        if (eTagMandatoryDetails[1].reReportingHTMLText!!.contains("fwp.mt.gov")) {
                            binding.mandatoryContainer.step2DescWeb.visibility = View.VISIBLE
                            binding.mandatoryContainer.step2DescWeb.setOnClickListener {
                                UtilsMethods.websiteOpener("https://fwp.mt.gov", requireActivity())
                            }
                        } else {
                            binding.mandatoryContainer.step2DescWeb.visibility = View.GONE
                        }

                    }
                    "3" -> {
                        val step3Heading = "Step 3 – ${
                            eTagMandatoryDetails[2].reReportingHTMLText!!.substringBefore("|")
                        }"
                        binding.mandatoryContainer.step3Heading.text = step3Heading

                        binding.mandatoryContainer.step3Desc.text =
                            eTagMandatoryDetails[2].reReportingHTMLText!!.substringAfter("|")
                    }
                }
            }
        }


        binding.more.setOnClickListener {
            showPopupMenu(binding.root.context, binding.more)
        }

    }

    private fun showPopupMenu(context: Context, view: View) {
        val wrapper: Context = ContextThemeWrapper(context, R.style.CustomPopUpStyle)
        val popup = PopupMenu(wrapper, view)
        popup.inflate(R.menu.validate_etag_more_menu)
        popup.setOnMenuItemClickListener { item: MenuItem? ->
            when (item!!.itemId) {
                R.id.remove_download_menu -> {

                    if (UtilsMethods.isInternetConnected(requireContext())) {

                        if (downloadStatus.contains(DownloadStatus.SAME_DEVICE)) {
                            showRemoveTagStateDialog(
                                context,
                                "Remove E-tag?",
                                "You won’t be able to validate your E-tag unless it’s downloaded to your device.",
                                "Cancel",
                                "Remove download"
                            )
                        } else {

                            UtilsMethods.showSimpleAlert(
                                requireContext(),
                                "There are no E-tag to remove.",
                                "You don’t have any E-tag downloaded currently.",
                                "Ok"
                            )
                        }


                    } else {
                        UtilsMethods.showNoNetwokDialog(requireContext())

                    }
                }
                R.id.get_help_menu -> {
                    UtilsMethods.navigateToFragment(
                        binding.root,
                        R.id.action_validateTagFragment_to_getHelpFragment2
                    )
                }
            }

            true
        }

        popup.show()
    }


    private fun showRemoveTagStateDialog(
        mContext: Context,
        tvPrimary: String,
        tvSecondary: String,
        cancelText: String,
        secText: String,
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val cancelBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = tvPrimary
        des.text = tvSecondary
        cancelBtn.text = cancelText
        sBtn.text = secText
        builder.setView(view)
        cancelBtn.setOnClickListener {
            builder.dismiss()
        }
        sBtn.setOnClickListener {

            try {

                loadingDialog.startLoading()

                val offloadRequest = DownloadOffloadRequest(
                    deviceUid = UtilConstant.DEVICE_ID,
                    service = UtilConstant.OFFLOAD,
                    spiId = spiIDLIst
                )

                validateTagActivityViewModel.offLoadRequest(offloadRequest)

                // Observing Response
                offloadObsr()

            } catch (e: Exception) {
                e.printStackTrace()
            } catch (s: SocketTimeoutException) {
                s.printStackTrace()
            } catch (e3: SSLException) {
                e3.printStackTrace()
            }

            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }


    private fun offloadObsr() {
        validateTagActivityViewModel.offloadResponse.observe(viewLifecycleOwner) {
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    it.resObject?.get(UtilConstant.CURRENT_INDEX)
                        ?.let { it1 ->

                            loadingDialog.isDismiss()
                            loadingDialog.startLoading()
                            validateTagActivityViewModel.getEtag(
                                EtagRequest(
                                    UtilConstant.DEVICE_ID
                                )
                            )
                            validateTagActivityViewModel.removeDetailsDownloadTagsById(spiIDLIst[0])
                            it1.message?.get(UtilConstant.CURRENT_INDEX)?.let { _ ->
                                /*            UtilsMethods.toastMaker(
                                                requireContext(),
                                                it2
                                            )*/
                            }
                        }
                }
                UtilConstant.FAIL_CODE -> {
                    loadingDialog.isDismiss()
                    it.resObject?.get(UtilConstant.CURRENT_INDEX)
                        ?.let { it1 ->
                            it1.message?.get(UtilConstant.CURRENT_INDEX)?.let { _ ->
                                /*      UtilsMethods.toastMaker(
                                          requireContext(),
                                          it2
                                      )*/
                            }
                        }
                }
            }
        }
    }


    private fun validateUISetup(it: EtagResponse) {
        binding.clToolTip.visibility = View.GONE
        if (!position.toString().isNullOrEmpty() && !accountOwner.toString()
                .isNullOrEmpty() && !eTagYearIndex.toString().isNullOrEmpty()
        ) {
            for (item in it.resObject?.get(0)?.etagAccountsList!!) {
                ownerDataFinder(
                    item
                )
            }

        }
    }

    private fun ownerDataFinder(
        item: EtagAccounts
    ) {
        when (item.owner!!.contains(accountOwner.toString())) {
            true -> {
                item.let { etagAccounts ->
                    val etagList = etagAccounts.etags?.get(
                        eTagYearIndex.toString().toInt()
                    )?.etagLicensesList?.get(position.toString().toInt())!!
                    val etagsubDetails = etagList.etagLicenseDetails!!.etagsubDetails

                    settingDownloadOnEtagActive(etagList.etagLicenseDetails)

                    // in new update   tag status
                    settingTagStatus(etagList)
                    settingHarvestDetails(
                        etagList.etagLicenseDetails
                    )

                    val heroTitle = "${
                        etagAccounts.etags[eTagYearIndex.toString()
                            .toInt()].licenseYear
                    } ${etagList.licenseName}"
                    binding.heroTitle.text = heroTitle

                    apiResponseMandatory(etagsubDetails!!.eTagMandatoryDetails)

                    // bind images
                    etagsubDetails.backgroundImageUrl?.let { it1 ->
                        initImage(
                            it1
                        )
                    }
                    etagsubDetails.imageUrlIcon?.let { it1 ->
                        initTagicon(
                            it1
                        )
                    }



                    settingSponserDetails(etagsubDetails)

                    spiIDLIst.clear()
                    etagAccounts.etags[eTagYearIndex.toString()
                        .toInt()].etagLicensesList?.get(
                        position.toString().toInt()
                    )?.etagLicenseDetails?.spiId?.let { it1 ->
                        spiIDLIst.add(
                            it1
                        )
                    }

                    downloadStatus.clear()
                    etagAccounts.etags[eTagYearIndex.toString()
                        .toInt()].etagLicensesList?.get(
                        position.toString().toInt()
                    )?.etagLicenseDetails?.downloadStatus?.let { it1 ->
                        downloadStatus.add(it1)
                    }


                    tagStatus.clear()
                    etagAccounts.etags[eTagYearIndex.toString()
                        .toInt()].etagLicensesList?.get(
                        position.toString().toInt()
                    )?.etagLicenseDetails?.etagsubDetails?.status?.forEach { state ->
                        tagStatus.add(state)
                    }


                    settingDownload(etagList.etagLicenseDetails)

                    settingClipDownloadData(etagList.etagLicenseDetails)

                    binding.ownerName.text = etagAccounts.owner

                    binding.alsLink.text =
                        etagsubDetails.alsNumber

                    // date manuplation
                    settingIssueDate(etagsubDetails, etagList)

                    binding.downloadNo.text =
                        etagsubDetails.downloads

                    //                            binding.downloadNo.text = "$downloadNo E-tag download"


                    binding.opData.text =
                        etagsubDetails.oppurtunity
                    binding.heroDes.text =
                        etagsubDetails.oppurtunity

                    binding.tvDescription.text =
                        etagsubDetails.description

                    binding.desRegulation.text =
                        "See regulations for specific harvest information, requirements, and dates"

                    binding.linkRegulation.text =
                        UtilsMethods.stringSpannable(
                            requireContext(),
                            etagsubDetails.regulationsUrl.toString(),
                            R.drawable.ic_buy_yellow,
                            true
                        )
                    binding.linkRegulation.setOnClickListener {
                        UtilsMethods.websiteOpener(
                            etagsubDetails.regulationsUrl.toString(),
                            requireActivity()
                        )
                    }


                    //setting state of download and validate btn state
                    buttonProperties(etagList)


                }
            }
        }
    }

    private fun settingIssueDate(
        etagsubDetails: EtagsubDetails,
        etagList: EtagLicenses
    ) {
        when (!etagsubDetails.issuedDate.isNullOrEmpty()) {
            true -> {
                issueDateAndTime =
                    etagsubDetails.issuedDate.split(
                        " "
                    )

                if (issueDateAndTime!!.contains("PM"))
                    binding.issuedtext.text =
                        "${issueDateAndTime!![0]} - ${issueDateAndTime!![1]} p.m."
                else
                    binding.issuedtext.text =
                        "${issueDateAndTime!![0]} - ${issueDateAndTime!![1]} a.m."
            }

            false -> binding.issuedtext.text =
                etagList.etagLicenseDetails!!.etagsubDetails!!.issuedDate
        }
    }

    private fun settingClipDownloadData(etagLicenseDetails: EtagLicenseDetails) {
        when (etagLicenseDetails.downloadStatus) {
            DownloadStatus.OTHER_DEVICE -> {
                binding.clToolTip.visibility = View.VISIBLE
                binding.customPopup1.tvToottip.text =
                    "Your E-tag is currently downloaded on another device."
            }

            DownloadStatus.NO_DOWNLOAD -> {
                binding.clToolTip.visibility = View.VISIBLE
                binding.customPopup1.tvToottip.text =
                    "Make sure to download your E-tag for offline use and check your mobile battery life."
            }

            DownloadStatus.SAME_DEVICE -> {
                binding.clToolTip.visibility = View.GONE
            }
        }
    }

    private fun settingDownload(etagLicenseDetails: EtagLicenseDetails) {
        val currentDownloadStatus = etagLicenseDetails.downloadStatus

        binding.downloaded.setOnClickListener {
            if (currentDownloadStatus == DownloadStatus.OTHER_DEVICE) {
                showTagStateDialog(spiIDLIst)
            } else {
                startDownloadProcess(spiIDLIst)
            }

        }
    }

    private fun settingSponserDetails(etagsubDetails: EtagsubDetails) {
        when (etagsubDetails.sponsorDetails?.isNullOrEmpty()) {
            true -> {
                binding.tvViewAllSponsors.visibility = View.GONE
                binding.tvSponsorSubtitle.text =
                    "No Sponsors Available"
            }

            else -> {
                binding.tvViewAllSponsors.visibility =
                    View.VISIBLE

                binding.tvSponsorSubtitle.text =
                    "${
                        etagsubDetails.sponsorDetails?.get(
                            0
                        )
                    }"

                // Saving list
                etagsubDetails.sponsorDetails.let { it1 ->
                    communicator.setSponsorList(
                        it1
                    )
                }
            }
        }
    }

    private fun settingHarvestDetails(
        etagLicenseDetails: EtagLicenseDetails,
    ) {
        if (!etagLicenseDetails.etagsubDetails!!.etagConfirmationNumber.isNullOrEmpty()) {

            binding.tvConfirmationNo.text = etagLicenseDetails.etagsubDetails.etagConfirmationNumber
            when (etagLicenseDetails.etagsubDetails.harvestTimeStamp.isNullOrEmpty()) {
                false -> {
                    dateAndTime =
                        etagLicenseDetails.etagsubDetails.harvestTimeStamp.split(
                            " "
                        )

                    val alterTime = dateTimeFormatter()
                    binding.tvHarvestTime.text = alterTime

                }

                true -> binding.tvHarvestTime.text =
                    etagLicenseDetails.etagsubDetails.harvestTimeStamp
            }

        } else {
            offlineValidationHarvestInfo(etagLicenseDetails)
        }
    }

    private fun offlineValidationHarvestInfo(etagLicenseDetails: EtagLicenseDetails) {
        for (i in etagValiRequest.indices) {
            if ((etagValiRequest[i].spiId!!.contains(etagLicenseDetails.spiId!!))) {


                settingOfflineHarvestDate(i)

                binding.tvConfirmationNo.text =
                    etagValiRequest[i].harvestConfNum

            }

        }
    }

    private fun settingOfflineHarvestDate(i: Int) {
        if (etagValiRequest[i].harvestedDate!!.isNotEmpty()) {
            val dateTimeSplit =
                etagValiRequest[i].harvestedDate!!.split(" ")
            //                                            UtilsMethods.toastMaker(requireContext(),"${etagValiRequest[i].harvestedDate}, $dateTimeSplit ")
            val alterTime: String =
                if (dateTimeSplit.contains("PM")) {
                    "${dateTimeSplit[0]} - ${
                        dateTimeSplit[2].substring(
                            0,
                            5
                        )
                    } p.m."
                } else {
                    "${dateTimeSplit[0]} - ${
                        dateTimeSplit[2].substring(
                            0,
                            5
                        )
                    } a.m."
                }
            binding.tvHarvestTime.text = alterTime
        }
    }

    private fun settingTagStatus(etagList: EtagLicenses) {
        if (etagList.etagLicenseDetails!!.etagsubDetails!!.status!!.isNotEmpty()) {
            etagList.etagLicenseDetails.etagsubDetails!!.status!!.forEach {
                when (it) {
                    TagStatus.VALIDATED -> {
                        binding.mandatoryContainer.step1IcCheck.visibility = View.VISIBLE
                        binding.mandatoryContainer.step1IcEtag.visibility = View.GONE
                        binding.constraintLayout3.visibility = View.GONE
                        binding.harvestContainer.visibility = View.VISIBLE
                        binding.etagStatevalidated.stateContainer.visibility =
                            View.VISIBLE
                        binding.etagStateRefund.stateContainer.visibility =
                            View.GONE
                    }
                    TagStatus.REFUNDED -> {
                        binding.etagStateRefund.stateContainer.visibility =
                            View.VISIBLE
                        binding.etagStatevalidated.stateContainer.visibility =
                            View.GONE
                    }
                    else -> {
                        binding.etagStatevalidated.stateContainer.visibility =
                            View.GONE
                        binding.etagStateRefund.stateContainer.visibility =
                            View.GONE
                    }
                }
            }
        }
    }

    private fun settingDownloadOnEtagActive(etagLicenseDetails: EtagLicenseDetails) {
        when (etagLicenseDetails.isEtagActive) {
            EtagActiveStatus.YES -> {
                binding.constraintLayout3.visibility = View.VISIBLE
                validateInOffline(etagLicenseDetails)
            }
            EtagActiveStatus.NO -> {
                binding.constraintLayout3.visibility = View.GONE
                validateInOffline(etagLicenseDetails)
            }

        }
    }

    private fun dateTimeFormatter(): String {
        var alterTime = " "
        if (dateAndTime != null) {

            alterTime = if (dateAndTime!!.contains("PM")) {
                "${dateAndTime!![0]} - ${dateAndTime!![1]} p.m."
            } else {
                "${dateAndTime!![0]} - ${dateAndTime!![1]} a.m."
            }
        }
        return alterTime
    }

    private fun validateInOffline(etagLicenseDetails: EtagLicenseDetails) {
        when (offlineSpiidList.contains(
            etagLicenseDetails.spiId
        )) {
            true -> {
                binding.mandatoryContainer.step1IcEtag.visibility =
                    View.GONE
                // bind buttons
                binding.mandatoryContainer.step1IcCheck.visibility =
                    View.VISIBLE
                binding.mandatoryContainer.step1IcEtag.visibility = View.GONE
                binding.harvestContainer.visibility = View.VISIBLE
                binding.constraintLayout3.visibility = View.GONE
                binding.etagStatevalidated.stateContainer.visibility =
                    View.VISIBLE
            }

            false -> {
                binding.mandatoryContainer.step1IcEtag.visibility =
                    View.VISIBLE
                binding.harvestContainer.visibility = View.GONE
                binding.etagStatevalidated.stateContainer.visibility =
                    View.GONE
                binding.constraintLayout3.visibility = View.VISIBLE

            }

        }
    }


    private fun showTagStateDialog(spiIDLIst: List<String>) {
        val builder = AlertDialog.Builder(requireContext()).create()
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.custom_dialog, null)
        val cancelBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = getString(R.string.other_download_primary)
        des.text = getString(R.string.other_download_secondary)
        cancelBtn.text = resources.getText(R.string.cancelText)
        sBtn.text = resources.getText(R.string.download)
        builder.setView(view)
        cancelBtn.setOnClickListener {
            builder.dismiss()
        }
        sBtn.setOnClickListener {
            startDownloadProcess(spiIDLIst)
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    private fun startDownloadProcess(spiIDLIst: List<String>) {
        try {

            val downloadRequest = DownloadOffloadRequest(
                deviceUid = UtilConstant.DEVICE_ID,
                service = UtilConstant.DOWNLOAD,
                spiId = spiIDLIst
            )

            if (UtilsMethods.isInternetConnected(requireContext())) {
                loadingDialog.startLoading()
                validateTagActivityViewModel.downloadRequest(downloadRequest)
                // Observing Response
                downloadObsr()
            } else {
                UtilsMethods.showSimpleAlert(
                    mContext = requireContext(),
                    tvPrimary = getString(R.string.no_connection),
                    tvSecondary = getString(R.string.check_and_try_again),
                    primaryBtn = resources.getString(R.string.ok)

                )
            }

        } catch (e: Exception) {
            e.printStackTrace()
        } catch (s: SocketTimeoutException) {
            s.printStackTrace()
        } catch (e3: SSLException) {
            e3.printStackTrace()
        }
    }

    private fun buttonProperties(etagList: EtagLicenses) {


        when (etagList.etagLicenseDetails?.downloadStatus) {

            DownloadStatus.NO_DOWNLOAD -> {
                binding.downloaded.setIconResource(R.drawable.ic_dowload_yellow)
                binding.downloaded.setStrokeColorResource(R.color.prim_yellow)
                binding.downloaded.setIconTintResource(R.color.prim_yellow)
                binding.downloaded.setBackgroundColor(
                    requireContext().getColor(
                        R.color.trans
                    )
                )
                binding.downloaded.text = "Download"
                binding.downloaded.visibility = View.VISIBLE

                binding.downloaded.isCheckable = true

                disableValidateBtn()
            }
            DownloadStatus.SAME_DEVICE -> {
                binding.downloaded.isClickable = false
                binding.downloaded.text = "Downloaded"
                binding.downloaded.visibility = View.VISIBLE
                binding.downloaded.setIconResource(R.drawable.ic_download_done)
                binding.downloaded.setStrokeColorResource(R.color.black)
                binding.downloaded.setBackgroundColor(
                    requireContext().getColor(
                        R.color.black
                    )
                )
                enableValidateBtn()
            }

            DownloadStatus.OTHER_DEVICE -> {
                binding.downloaded.isClickable = true
                binding.downloaded.text = "Download"
                binding.downloaded.visibility = View.VISIBLE
                binding.downloaded.setStrokeColorResource(R.color.prim_yellow)
                binding.downloaded.setIconResource(R.drawable.ic_warning_yellow)
                binding.downloaded.setIconTintResource(R.color.prim_yellow)
                binding.downloaded.setBackgroundColor(
                    requireContext().getColor(
                        R.color.trans
                    )
                )
                binding.validateButton.btn.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.white
                    )
                )
                /*        binding.downloaded.setBackgroundColor(
                            requireContext().getColor(
                                R.color.black
                            )
                        )*/
            }


            "E" -> {
                binding.downloaded.isClickable = false
                binding.downloaded.setIconResource(R.drawable.ic_downloading)
                binding.downloaded.setStrokeColorResource(R.color.black)
                binding.validateButton.btn.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        R.color.white
                    )
                )
                binding.downloaded.setBackgroundColor(
                    binding.downloaded.context.getColor(
                        R.color.black
                    )
                )

            }
        }
    }


    private fun enableValidateBtn() {
        val validatorButtonId = binding.validateButton.btn
        validatorButtonId.setText(R.string.validate)
        validatorButtonId.isEnabled = true
        validatorButtonId.isClickable = true
        validatorButtonId.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
        validatorButtonId.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
        /*       downloadIconButtonLayout.setBackgroundColor(
                   ContextCompat.getColor(
                       requireContext(),
                       R.color.black
                   )
               )*/
        validatorButtonId.setOnClickListener {
            val bundle = Bundle()
            bundle.putString(UtilConstant.GET_ETAG_ACCOUNT_OWNER, accountOwner.toString())
            bundle.putString(UtilConstant.GET_ETAG_INDEX_KEY, position.toString())
            bundle.putString(UtilConstant.GET_ETAG_YEAR_KEY, eTagYearIndex.toString())
            UtilsMethods.navigateToFragmentWithValues(
                binding.root,
                R.id.reviewHarvestFragment,
                bundle
            )
        }
    }

    private fun disableValidateBtn() {
        val validatorButtonId = binding.validateButton.btn
        validatorButtonId.setText(R.string.validate)

        validatorButtonId.isEnabled = false
        validatorButtonId.isClickable = false

        validatorButtonId.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        validatorButtonId.setBackgroundColor(resources.getColor(R.color.buttonTrans, null))

    }

    private fun downloadObsr() {
        validateTagActivityViewModel.downloadResponse.observe(viewLifecycleOwner) {
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    it.resObject?.get(UtilConstant.CURRENT_INDEX)
                        ?.let { it1 ->
                            loadingDialog.isDismiss()
                            loadingDialog.startLoading()
                            validateTagActivityViewModel.getEtag(
                                EtagRequest(
                                    UtilConstant.DEVICE_ID
                                )
                            )
                            validateTagActivityViewModel.insertDownloadTag(
                                DownloadedTags(
                                    downloadedSpiId = spiIDLIst[0]
                                )
                            )
                            it1.message?.get(UtilConstant.CURRENT_INDEX)?.let { _ ->
                                /*    UtilsMethods.toastMaker(
                                        requireContext(),
                                        it2
                                    )*/
                            }
                        }
                }
                UtilConstant.FAIL_CODE -> {
                    loadingDialog.isDismiss()
                    it.resObject?.get(UtilConstant.CURRENT_INDEX)
                        ?.let { it1 ->
                            it1.message?.get(UtilConstant.CURRENT_INDEX)?.let { _ ->
//                                UtilsMethods.toastMaker(
//                                    requireContext(),
//                                    it2
//                                )
                            }
                        }
                }
            }
        }
    }


}
