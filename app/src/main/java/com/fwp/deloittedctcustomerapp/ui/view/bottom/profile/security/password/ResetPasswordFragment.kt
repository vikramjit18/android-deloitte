/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password

import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.ChangePasswordRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest
import com.fwp.deloittedctcustomerapp.databinding.ResetPasswordFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotActivity
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginActivity
import com.fwp.deloittedctcustomerapp.utils.*
import com.google.android.material.textfield.TextInputEditText
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException

/**
 * FWP
 * Hilt entry point
 */
@AndroidEntryPoint
class ResetPasswordFragment : Fragment() {

    companion object {
        fun newInstance() = ResetPasswordFragment()
    }

    /**
     * Global variables
     */
    private var _binding: ResetPasswordFragmentBinding? = null
    private val binding get() = _binding!!
    private val resetPasswordViewModel: ResetPasswordViewModel by viewModels()
    private lateinit var loadingDialog: LoadingDialog
    private val listOffLoad = mutableListOf<String>()


    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_bottom_to_top)
        } else {
            AnimationUtils.loadAnimation(context, R.anim.slide_top_to_bottom)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        /**
         * Binding Initiate
         */
        _binding = ResetPasswordFragmentBinding.inflate(inflater, container, false)

        if (!UtilsMethods.isInternetConnected(requireContext())) {
            UtilsMethods.showSimpleAlert(
                mContext = requireContext(),
                tvPrimary = resources.getString(R.string.device_currently_offline),
                tvSecondary = "You must have a network connection to update your password.",
                primaryBtn = resources.getString(R.string.ok)
            )
        }

        checkLiveNetworkConnection()
        /**
         * Returning
         * root binding
         */
        return binding.root
    }

    /**
     * Checking live
     * internet connection
     * function in run time
     */
    private fun checkLiveNetworkConnection() {
        val connectivityManager =
            requireActivity().getSystemService(ConnectivityManager::class.java)
        val liveConnection = ConnectionLiveData(connectivityManager)

        liveConnection.observe(viewLifecycleOwner) { isConnected ->
            if (isConnected) {
                binding.network.networkNotifyContainer.visibility = View.GONE
            } else {
                binding.network.networkNotifyContainer.visibility = View.VISIBLE
                UtilsMethods.showSimpleAlert(
                    mContext = requireContext(),
                    tvPrimary = getString(R.string.device_currently_offline),
                    tvSecondary = "You must have a network connection to update your password.",
                    primaryBtn = resources.getString(R.string.ok)
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()

        // laoding
        loadingDialog = LoadingDialog(requireActivity())

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listOffLoad.clear()

        resetPasswordViewModel.getDownloadedTagSid().observe(requireActivity()) {
            it.forEach { data ->
                data.downloadedSpiId?.let { it1 -> listOffLoad.add(it1) }
            }
        }
        // Setting back icon
        binding.back.backLayout.setImageDrawable(
            ResourcesCompat.getDrawable(
                resources,
                R.drawable.ic_close,
                null
            )
        )

        /**
         * Globals functions
         */

        buttonHandler()
        passValidator()
        buttonActivation()

        /**
         * TextWatch listener
         * for any changes
         */
        binding.passCurrent.addTextChangedListener(mTextWatcher)
        binding.passNew.addTextChangedListener(mTextWatcher)
        binding.passNewConfirm.addTextChangedListener(mTextWatcher)


        /**
         * Observing
         * changePassword Response
         */
        resetPasswordViewModel.changePasswordResponse.observe(
            viewLifecycleOwner
        ) { changePasswordResponse ->

            //dissmis loading
            loadingDialog.isDismiss()

            if (changePasswordResponse.responseCode == UtilConstant.SUCCESS_CODE) {
                changePasswordResponse.resObject?.get(0)?.message
                    ?.let {
//                        UtilsMethods.toastMaker(
//                            requireContext(),
//                            "Your password has been reset successfully"
//                        )
                    }

                if (listOffLoad.isNotEmpty()) {
                    loadingDialog.startLoading()
                    val offloadRequest = DownloadOffloadRequest(
                        deviceUid = UtilConstant.DEVICE_ID,
                        service = UtilConstant.OFFLOAD,
                        spiId = listOffLoad.toSet().toList()
                    )
                    offloadApi(offloadRequest)
                    offloadObsr()

                } else {
                    UtilConstant.itemLastIndex = 0
                    UtilConstant.etagLastIndex = 0
                    UtilConstant.ITEMS_LAST_VIEW_INDEX = 0
                    UtilConstant.ACCOUNT_VIEWPAGER = 0
                    SharedPrefs.clearSharedPref()
                    resetPasswordViewModel.cleardb()
                    val i = Intent(activity, LoginActivity::class.java)
                    requireActivity().startActivity(i)
                    requireActivity().finish()
                }

            } else {
                changePasswordResponse.resObject?.get(0)?.message
                    ?.let {
//                            UtilsMethods.toastMaker(requireContext(), it.toString())
                    }
            }
        }

        resetPasswordViewModel.toast.observe(viewLifecycleOwner) {
            it.getContentIfNotHandledOrReturnNull()?.let { _ ->
                loadingDialog.isDismiss()
//                UtilsMethods.toastMaker(requireContext(), message)
            }
        }
    }

    private fun offloadApi(offloadRequest: DownloadOffloadRequest) {
        try {
            resetPasswordViewModel.offLoadRequest(offloadRequest)

        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (e2: SocketTimeoutException) {
            loadingDialog.isDismiss()
            e2.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        }
    }


    private fun offloadObsr() {
        resetPasswordViewModel.offloadResponse.observe(requireActivity()) {
            loadingDialog.isDismiss()
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    UtilConstant.itemLastIndex = 0
                    UtilConstant.etagLastIndex = 0
                    UtilConstant.ITEMS_LAST_VIEW_INDEX = 0
                    UtilConstant.ACCOUNT_VIEWPAGER = 0
                    SharedPrefs.clearSharedPref()
                    resetPasswordViewModel.cleardb()
                    resetPasswordViewModel.deleteDownloadedTagSid()
                    val i = Intent(activity, LoginActivity::class.java)
                    requireActivity().startActivity(i)
                    requireActivity().finish()

                }
                UtilConstant.FAIL_CODE -> {
                    it.resObject?.get(UtilConstant.CURRENT_INDEX)
                        ?.let { _ ->
//                            it1.message?.get(CURRENT_INDEX)?.let { it2 ->
//                                UtilsMethods.toastMaker(
//                                    requireContext(),
//                                    it2
//                                )
//                            }
                        }
                }
            }
        }
    }


    /**
     * All click
     * listener function
     */
    private fun buttonHandler() {

        // Back to previous function
        binding.back.backLayout.setOnClickListener {
            UtilsMethods.backStack(findNavController())
        }

        // Closing no internet layout
        binding.network.closeNetNotify.setOnClickListener {
            binding.network.networkNotifyContainer.visibility = View.GONE
        }

        // Navigate to forget password fragment
        binding.forgotPass.setOnClickListener {
            /* UtilsMethods.navigateToFragment(
                 binding.root,
                 R.id.action_resetPasswordFragment_to_forgot12
             )*/
            val i = Intent(requireActivity(), ForgotActivity::class.java)
            requireActivity().startActivity(i)
        }
    }

    /**
     * Validating password
     */
    private fun passValidator() {
        val password = binding.passNew
        val validateImg = R.drawable.ic_check
        val unValidateImg = R.drawable.ic_unchecked_checkbox
        password.doAfterTextChanged {

            /*when (UtilConstant.PASSWORD_PATTERN1.matcher(password.editableText).matches()) {

                true -> binding.checkboxLeastCharacter.setImageResource(validateImg)
                false -> binding.checkboxLeastCharacter.setImageResource(unValidateImg)
            }
            when (UtilConstant.PASSWORD_PATTERN2.matcher(password.editableText).matches()) {

                true -> binding.checkboxNumberCharacter.setImageResource(validateImg)
                false -> binding.checkboxNumberCharacter.setImageResource(unValidateImg)
            }*/


            passwordValidator(password, validateImg, unValidateImg)


        }
    }

    private fun passwordValidator(
        password: TextInputEditText,
        validateImg: Int,
        unValidateImg: Int
    ) {
        when (password.length() >= 6) {
            true -> binding.checkboxLeastCharacter.setImageResource(validateImg)
            false -> binding.checkboxLeastCharacter.setImageResource(unValidateImg)
        }

        when (password.text.toString()
            .indexOfAny(
                charArrayOf(
                    '\'',
                    '(',
                    ')',
                    '|',
                    '\\'
                )
            ) >= 0 || password.editableText.isEmpty()) {
            true -> binding.checkboxNumberCharacter.setImageResource(unValidateImg)
            false -> binding.checkboxNumberCharacter.setImageResource(validateImg)
        }
    }

    /**
     * function for
     * activate primary button
     */
    private fun buttonActivation() {
        when (binding.passCurrent.editableText.isNotEmpty() && binding.passNew.length() >= 6 && binding.passNew.text.toString()
            .indexOfAny(
                charArrayOf(
                    '\'',
                    '(',
                    ')',
                    '|',
                    '\\'
                )
            ) <= 0 && binding.passNew.text.toString() == binding.passNewConfirm.text.toString()) {
            true -> {
                enablePrimaryBtn()
                saveNewPasswordBtnHandler()
            }
            false ->
                disablePrimaryBtn()
        }
        passwordAlert()
    }

    private fun passwordAlert() {
        val newPass = binding.passNew.text.toString()
        val newPassCon = binding.passNewConfirm.text.toString()
        val newPassConLayout = binding.passwordNewConfirmTextLayout
        when (newPassCon.isNotEmpty()) {
            true -> when (newPass.trim() == newPassCon.trim()) {
                true -> {
                    newPassConLayout.isErrorEnabled = false
                }
                false -> {
                    newPassConLayout.isErrorEnabled = true
                    newPassConLayout.error = "Passwords must match"

                }
            }
            false -> newPassConLayout.isErrorEnabled = false
        }
    }

    /**
     * btn click listener
     * after the btn activation
     */
    private fun saveNewPasswordBtnHandler() {
        binding.saveNewPass.btn.setOnClickListener {
            if (UtilsMethods.isInternetConnected(requireContext())) {
                val token = SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString()
                // Showing loader
                loadingDialog.startLoading()

                // Initiate User login Api
                try {
                    // change password api
                    resetPasswordViewModel.changePassword(
                        token,
                        ChangePasswordRequest(
                            binding.passNewConfirm.text.toString(),
                            binding.passCurrent.text.toString(),
                            binding.passNew.text.toString()
                        )
                    )
                } catch (e: Exception) {
                    loadingDialog.isDismiss()
                    e.printStackTrace()
                } catch (e2: SocketTimeoutException) {
                    loadingDialog.isDismiss()
                    e2.printStackTrace()
                } catch (e3: SSLException) {
                    loadingDialog.isDismiss()
                    e3.printStackTrace()
                }

            } else {

                // showing No internet dialog
                UtilsMethods.showSimpleAlert(
                    mContext = requireContext(),
                    tvPrimary = getString(R.string.no_connection),
                    tvSecondary = getString(R.string.check_and_try_again),
                    primaryBtn = resources.getString(R.string.ok)
                )
            }
        }
    }


    /**
     *  create a textWatcher member
     *  */
    private val mTextWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun afterTextChanged(editable: Editable) {
            //needed for runtime validation
            buttonActivation()
        }
    }

    /**
     * function for the
     * disable primary btn
     */
    private fun disablePrimaryBtn() {
        val saveNewPassButtonId = binding.saveNewPass.btn
        saveNewPassButtonId.text = resources.getString(R.string.saveNewPass)

        saveNewPassButtonId.isEnabled = false
        saveNewPassButtonId.isClickable = false
        saveNewPassButtonId.setTextColor(resources.getColor(R.color.white, null))
        saveNewPassButtonId.setBackgroundColor(
            resources.getColor(
                R.color.disableButtonColor,
                null
            )
        )
    }

    /**
     * function for the
     * enabling primary btn
     */
    private fun enablePrimaryBtn() {
        val saveNewPassButtonId = binding.saveNewPass.btn
        saveNewPassButtonId.text = resources.getString(R.string.saveNewPass)

        saveNewPassButtonId.isEnabled = true
        saveNewPassButtonId.isClickable = true

        saveNewPassButtonId.setTextColor(resources.getColor(R.color.black, null))
        saveNewPassButtonId.setBackgroundColor(
            resources.getColor(
                R.color.prim_yellow,
                null
            )
        )
    }
}
