/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fwp.deloittedctcustomerapp.data.model.requests.SignupRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.EtagDetail
import dagger.hilt.android.lifecycle.HiltViewModel

class Communicator : ViewModel() {

    private val _message = MutableLiveData<Any>()
    val message: LiveData<Any>
        get() = _message


    private val _sponsorDetails = MutableLiveData<List<Any>>()
    val sponsorDetails: LiveData<List<Any>>
        get() = _sponsorDetails

    private val _userCreds = MutableLiveData<SignupRequest>()
    val userCreds: LiveData<SignupRequest>
        get() = _userCreds
    private val _username = MutableLiveData<Any>()
    val username: LiveData<Any>
        get() = _username
    private val _email = MutableLiveData<Any>()
    val email: LiveData<Any>
        get() = _email
    private val _password = MutableLiveData<Any>()
    val password: LiveData<Any>
        get() = _password
    private val _fn = MutableLiveData<Any>()
    val fn: LiveData<Any>
        get() = _fn
    private val _mn = MutableLiveData<Any>()
    val mn: LiveData<Any>
        get() = _mn
    private val _ln = MutableLiveData<Any>()
    val ln: LiveData<Any>
        get() = _ln

    fun setMsgCommunicator(msg: String) {
        _message.value = msg
    }

    fun setUserNameCommunicator(user: String) {
        _username.value = user
    }

    fun setEmailCommunicator(email: String) {
        _email.value = email
    }

    fun setPassCommunicator(password: String) {
        _password.value = password
    }

    fun setFNCommunicator(fn: String) {
        _fn.value = fn
    }

    fun setMNCommunicator(mn: String) {
        _mn.value = mn
    }

    fun setLNCommunicator(ln: String) {
        _ln.value = ln
    }


    fun setSponsorList(list: List<Any>) {
        _sponsorDetails.value = list
    }

    fun setUserCredModel(userCreds: SignupRequest) {
        _userCreds.value = userCreds
    }

    fun setEtagModel(etagDetail: EtagDetail) {
        _message.value = etagDetail
    }
}
