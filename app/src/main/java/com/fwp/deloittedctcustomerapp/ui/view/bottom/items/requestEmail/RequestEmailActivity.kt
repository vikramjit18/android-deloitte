/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RequestEmailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_email)
        SharedPrefs.init(this)
    }
    override fun onBackPressed() {

    }
}
