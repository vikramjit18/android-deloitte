/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fwp.deloittedctcustomerapp.data.db.AppDao
import com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
import com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.validateEtag.ValidateEtagResponse
import com.fwp.deloittedctcustomerapp.data.repo.MainRepo
import com.fwp.deloittedctcustomerapp.utils.Event
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ValidateTagActivityViewModel @Inject constructor(
    private val mainRepo: MainRepo,
    private val appDao: AppDao
) :
    ViewModel() {

    private val _validateEtagResponse = MutableLiveData<ValidateEtagResponse>()
    val validateEtagResponse: LiveData<ValidateEtagResponse>
        get() = _validateEtagResponse


    private val _toast = MutableLiveData<Event<String>>()
    val toast: LiveData<Event<String>>
        get() = _toast
    private val _offloadResponse = MutableLiveData<DownloadOffloadResponse>()
    val offloadResponse: LiveData<DownloadOffloadResponse>
        get() = _offloadResponse

    private val _downloadResponse = MutableLiveData<DownloadOffloadResponse>()
    val downloadResponse: LiveData<DownloadOffloadResponse>
        get() = _downloadResponse
    private val _etagResponse = MutableLiveData<EtagResponse>()
    val etagResponse: LiveData<EtagResponse>
        get() = _etagResponse

    private val _toastGetEtag = MutableLiveData<Event<String>>()
    val toastGetEtag: LiveData<Event<String>>
        get() = _toastGetEtag
    private val _toastDownloadOffload = MutableLiveData<Event<String>>()
    val toastDownloadOffload: LiveData<Event<String>>
        get() = _toastDownloadOffload

    val coroutineExceptionHandler = CoroutineExceptionHandler { _, t -> t.printStackTrace() }
    fun getEtagValidated(validateEtagRequest: ValidateEtagRequest) {
        viewModelScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.validateEtag(validateEtagRequest)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful && response.body() != null) {

                    when (response.body()!!.responseMessage) {
                        UtilConstant.SUCCESS_MSG -> {
                            etagData(response)
                        }
                        else -> _toast.value =
                            response.body()!!.resObject?.get(0)?.message?.get(0)?.let {
                                Event(it)
                            }
                    }
                } else {
                    _toast.value = Event(response.message())
                }
            }
        }
    }

    private fun etagData(response: Response<ValidateEtagResponse>) {
        when (response.body()?.responseCode) {
            UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> {
                _validateEtagResponse.value = response.body()
            }
            UtilConstant.ERROR_CODE -> {
                _toast.value =
                    response.body()!!.resObject?.get(0)
                        ?.let { Event(it.toString()) }
            }
        }
    }

    fun insertValidateTagData(validateEtagRequest: ValidateEtagRequest) {
        viewModelScope.launch {
            mainRepo.insertValiTagRecord(validateEtagRequest)
        }

    }



    fun deleteValiData() {
        viewModelScope.launch {
            appDao.deleteValidateRequest()
        }

    }
    fun insertDownloadTag(downloadedTags: DownloadedTags){
        viewModelScope.launch {
            appDao.insertDownloadTags(downloadedTags)
        }
    }
    fun removeDetailsDownloadTagsById(onlineSpiId:String){
        viewModelScope.launch{
            appDao.deleteDownloadedTagsById(onlineSpiId = onlineSpiId)
        }
    }

    fun getValidateTagData(): LiveData<List<ValidateEtagRequest>> {
        return mainRepo.getValiRecords()
    }
    fun getEtag(etagRequest: EtagRequest) {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.getEtags(etagRequest)
            withContext(Dispatchers.Main)  {
                if (response.isSuccessful) {
                    when (response.body()!!.responseCode) {

                        UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE
                        -> _etagResponse.value = response.body()

                        UtilConstant.ERROR_CODE -> _toastGetEtag.value =
                            response.body()!!.resObject?.get(0)?.let { Event(it.toString()) }
                    }
                } else {
                    _toastGetEtag.value = Event(response.message())
                }
            }
        }
    }
    fun offLoadRequest(downloadOffloadRequest: DownloadOffloadRequest) {
        GlobalScope.launch (Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.downloadOffLoadEtag(downloadOffloadRequest)
            withContext(Dispatchers.Main)   {
                if (response.isSuccessful) {
                    when (response.body()!!.responseCode) {

                        UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE
                        -> _offloadResponse.value = response.body()

                        UtilConstant.ERROR_CODE -> _toastDownloadOffload.value =
                            response.body()!!.resObject?.get(0)?.let { Event(it.toString()) }
                    }

                } else {
                    _toastDownloadOffload.value = Event(response.message())
                }
            }}
    }

    fun downloadRequest(downloadOffloadRequest: DownloadOffloadRequest) {
        GlobalScope.launch (Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.downloadOffLoadEtag(downloadOffloadRequest)
            withContext(Dispatchers.Main)   {
                if (response.isSuccessful) {

                    when (response.body()!!.responseCode) {

                        UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE
                        -> _downloadResponse.value = response.body()

                        UtilConstant.ERROR_CODE -> _toastDownloadOffload.value =
                            response.body()!!.resObject?.get(0)?.let { Event(it.toString()) }
                    }

                } else {
                    _toastDownloadOffload.value = Event(response.message())
                }
            }
        }
    }
}
