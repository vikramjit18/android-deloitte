/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.als.confirmAls

import android.app.Activity
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.compose.ui.text.android.InternalPlatformTextApi
import androidx.compose.ui.text.android.style.TypefaceSpan
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.ConfirmAlsFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint

// FWP
// Hilt entry point
@AndroidEntryPoint
class ConfirmAlsFragment : Fragment() {

    companion object {
        fun newInstance() = ConfirmAlsFragment()
    }

    // Global variables
    private lateinit var binding: ConfirmAlsFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Data binding
        binding = DataBindingUtil.inflate(inflater, R.layout.confirm_als_fragment, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initiate execution
        init()


    }

    private fun init() {

        binding.textView2.text = resources.getString(R.string.confirmation_als_string)

        // Setting
        // Button Primary
        binding.openEmailBtn.btn.text = resources.getString(R.string.openEmailAppText)
        binding.openEmailBtn.btn.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
        binding.openEmailBtn.btn.setTextColor(resources.getColor(R.color.black, null))

        // Setting
        // Button Secondary
        binding.didNotReceiveBtn.btn.text = resources.getString(R.string.havingTroubleText)
        binding.didNotReceiveBtn.btn.setBackgroundColor(resources.getColor(R.color.bgColor, null))
        binding.didNotReceiveBtn.btn.setTextColor(resources.getColor(R.color.prim_yellow, null))
        binding.didNotReceiveBtn.btn.typeface =
            ResourcesCompat.getFont(requireContext(), R.font.font_mont_semi_bold)
        binding.didNotReceiveBtn.btn.elevation = 0F
        binding.didNotReceiveBtn.btn.stateListAnimator = null

        // button click listener
        buttonHandler()

    }

    private fun buttonHandler() {

        // Back to previous
        binding.close.backLayout.setOnClickListener {
            requireActivity().finish()
        }

        // open email
        binding.openEmailBtn.btn.setOnClickListener {
            UtilsMethods.openEmailApp(requireActivity())
        }

        binding.didNotReceiveBtn.btn.setOnClickListener {
           UtilsMethods.showDialerAlsAlert(
                mActivity = requireActivity(),
                mContext = requireContext(),
                tvPrimary = getString(R.string.notReceiveEmailTitle),
                tvSecondary = "For more support, call FWP support at 406-444-2950 during business hours (Monday – Friday, 8 a.m. – 5 p.m.).",
                secBtn = getString(R.string.close),
                primaryBtn = getString(R.string.callFwpText)
            )
        }
    }
}



/*    private fun sendMailToContact() {
*//*        interactionType = Constants.EMAIL
        message = ""
        happenedAt = Helpers.localToUTC(
            Helpers.getCurrentDate(Constants.DATE_FORMAT4)!!,
            Constants.DATE_FORMAT4
        ).toString()*//*
        val i = Intent(Intent.ACTION_SENDTO)
        i.type = Constants.MAiL_TYPE
        i.putExtra(Intent.EXTRA_EMAIL, arrayOf(data!!.contacts?.email))
        i.data = Uri.parse("mailto:");
        val shareAction = "com.email.share.SHARE_ACTION"
        val Ireceiver = Intent(shareAction)
        Ireceiver.putExtra("test", "test")
        val pendingIntent: PendingIntent =
            PendingIntent.getBroadcast(context, 0, Ireceiver, PendingIntent.FLAG_UPDATE_CURRENT)
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                context?.registerReceiver(receiver, IntentFilter(shareAction))
                startActivity(Intent.createChooser(i, "Send mail...", pendingIntent.intentSender))
            }
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(
                activity!!,
                getString(R.string.no_email_client_error),
                Toast.LENGTH_SHORT
            ).show()
        }
    }*/


