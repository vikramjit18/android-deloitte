/*
* Montana Fish, Wildlife & Parks
*/



package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.viewPager

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.util.Log
import android.view.*
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import android.widget.ArrayAdapter
import android.widget.PopupMenu
import android.widget.TextView
import androidx.compose.ui.text.android.InternalPlatformTextApi
import androidx.compose.ui.text.android.style.TypefaceSpan
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
import com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenseDetails
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.ResObject
import com.fwp.deloittedctcustomerapp.databinding.LinkedEtagsFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.linkedAccountAdapter.MultipleEtagCurrentYearAdapter
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.linkedAccountAdapter.MultipleEtagPreviousYearAdapter
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator
import com.fwp.deloittedctcustomerapp.utils.*
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.CURRENT_INDEX
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.MULTIPLE_ACCOUNT_KEY
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.PREVIOUS_INDEX
import com.google.gson.Gson
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException

private const val TAG = "LinkedAccountEtagsFragm"

class LinkedAccountEtagsFragment : Fragment() {

    companion object {
        fun newInstance() = LinkedAccountEtagsFragment()
    }

    private lateinit var binding: LinkedEtagsFragmentBinding
    private val model: Communicator by viewModels()
    private lateinit var validateTagActivityViewModel: ValidateTagActivityViewModel

    private var firstName = "Linked minor"

    private lateinit var viewModel: LandingScreenActivityViewModel
    private val userList = arrayListOf<String>()

    //    current year download
    private val downloadCurrentStatus = mutableListOf<String>()
    private val allDownLoadCurrentSpiIDLIst = mutableListOf<String>()

    //    previous year downlaod
    private val downloadPreStatus = mutableListOf<String>()

    //    private val downLoadPreSpiIDLIst = mutableListOf<String>()
    private val allPreviousSpiIdList = mutableListOf<String>()

    //only etagactive == yes
    //daownload all
    private val downloadAllStatus = mutableListOf<String>()
    private val downloadAllSpiIdList = mutableListOf<String>()

    //remove all
    private val offLoadAllStatus = mutableListOf<String>()
    private val offLoadAllSpiIDLIst = mutableListOf<String>()

    // db spid
    private val offlineSpiidList = mutableListOf<String>()

    private lateinit var loadingDialog: LoadingDialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.linked_etags_fragment, container, false)
        loadingDialog = LoadingDialog(requireActivity())
        viewModel = ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]
        validateTagActivityViewModel =
            ViewModelProvider(requireActivity())[ValidateTagActivityViewModel::class.java]
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        viewModel.setOwnerName(firstName)

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.currentYearRecyclerView.setHasFixedSize(true)
        binding.previousYearRecyclerView.setHasFixedSize(true)


//        loadingDialog.startLoading()
        validateTagActivityViewModel.getValidateTagData().observe(requireActivity()) {
            offlineSpiidList.clear()
            it.forEach {
                /*           var request = ValidateEtagRequest(
                               id = it.id,
                               deviceId = it.deviceId,
                               // etaguploaddate
                               etagUploadDate = it.etagUploadDate,
                               harvestConfNum = it.harvestConfNum,
                               //current date
                               harvestedDate = it.harvestedDate,
                               lat = it.lat,
                               longt = it.longt,
                               speciesCd = it.speciesCd,
                               spiId = it.spiId,
                           )*/
                it.spiId?.let { it1 -> offlineSpiidList.add(it1) }
//                Log.e("Linked ", "onViewCreated: from db spid $offlineSpiidList")
            }

            viewModelObsr("")
            binding.spinner1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {

                    if (userList[position] == resources.getString(R.string.select_account)) {
                        /*              val name = "back"
                                      val spannable = SpannableString("Welcome $name")
                                      spannable.setSpan(
                                          ForegroundColorSpan(resources.getColor(R.color.prim_yellow, null)),
                                          8,
                                          8 + name.length,
                                          Spannable.SPAN_INCLUSIVE_EXCLUSIVE,
                                      )
                                      UtilConstant.MINOR_NAME = spannable.toString()*/
                        viewModel.setOwnerName("Linked minor")
                        binding.includeNoAccountView.itemContainer.visibility = View.VISIBLE
                        binding.includeNoAccountView.textView43.text =
                            "Select a user from the dropdown to display their E-tags."
                        binding.rlInfoContainer.visibility = View.GONE
                    } else {
                        firstName = userList[position].substringBefore(" ")
                        viewModel.setOwnerName(firstName)
                        binding.includeNoAccountView.itemContainer.visibility = View.GONE
                        binding.rlInfoContainer.visibility = View.VISIBLE
                        viewModelObsr(userList[position])
                    }

                    // Getting last spinner index
                    UtilConstant.etagLastIndex = binding.spinner1.selectedItemPosition
                }


                override fun onNothingSelected(parent: AdapterView<*>?) {
                    // Nothing
                }
            }
            userSpinner()

        }


        buttonHandler()
        setSpannableString()


    }

    private fun buttonHandler() {
        binding.more.setOnClickListener {
            showPopupMenu(binding.root.context, binding.more)
        }

        binding.noEtagAvailable.buyApplyButton.setOnClickListener {
            UtilsMethods.websiteOpener("https://fwp.mt.gov/buyandapply", requireActivity())
        }
        binding.buyAndApply.notSeeingYourPar.setOnClickListener {
            UtilsMethods.showDialerAlert(
                requireActivity(),
                requireContext(),
                "Not seeing your purchase?",
                "If you feel like an item you purchased is missing, call FWP support at 406-444-2950 during business hours (Monday – Friday, 8 a.m. – 5 p.m.)",
                "Close",
                resources.getString(R.string.callFwpText)
            )
        }
        binding.buyAndApply.buyApplyButton.setOnClickListener {
            UtilsMethods.websiteOpener("https://fwp.mt.gov/buyandapply", requireActivity())
        }
        binding.noEtagAvailable.notSeeingYourPar.setOnClickListener {
            UtilsMethods.showDialerAlert(
                requireActivity(),
                requireContext(),
                "Not seeing your purchase?",
                "If you feel like an item you purchased is missing, call FWP support at 406-444-2950 during business hours (Monday – Friday, 8 a.m. – 5 p.m.)",
                "Close",
                resources.getString(R.string.callFwpText)
            )
        }

        binding.downloadAll.setOnClickListener {
            if (UtilsMethods.isInternetConnected(requireContext())) {
                if (downloadAllStatus.isNotEmpty()) {
                    if (downloadAllStatus.contains(DownloadStatus.NO_DOWNLOAD)) {
                        showDownloadAllTagStateDialog(
                            requireContext(),
                            "Do you want to download all tags to this device?",
                            "You should only download E-tags to the device you plan to bring in the field. \n" +
                                "\n" +
                                "By downloading all your E-tags, you are agreeing to make this your primary device.\n",
                            "No, cancel",
                            "Yes, download all"
                        )
                    } else {
                        UtilsMethods.showSimpleAlert(
                            requireContext(),
                            "All E-tags currently downloaded",
                            "You have already downloaded all your E-tags to this device.",
                            "Ok"
                        )
                    }
                } else {
                    UtilsMethods.showDialerAlert(
                        requireActivity(),
                        requireContext(),
                        getString(R.string.you_have_no),
                        getString(R.string.you_have_no_des),
                        getString(R.string.cancelText),
                        getString(R.string.callFwpText)
                    )
                }
            } else {
                UtilsMethods.showNoNetwokDownloadDialog(requireContext())

            }

        }
    }


    private fun userSpinner() {
        val adapterS = activity?.let { ArrayAdapter(it, R.layout.spinner_style, userList) }
        binding.spinner1.adapter = adapterS
        binding.spinner1.setSelection(UtilConstant.etagLastIndex)
    }

//secondry account

    private fun recyclerContent(name: String, etagResponse: EtagResponse) {

        downloadAllSpiIdList.clear()
        downloadAllStatus.clear()
        offLoadAllStatus.clear()
        // get all download  id whose etag active == Y
        itrationForAll(etagResponse, etagResponse.resObject!!, name)

        binding.currentYearRecyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext().applicationContext)
            itrationForCurrentYear(etagResponse.resObject, name, etagResponse)

        }
        binding.previousYearRecyclerView.apply {
            downloadPreStatus.clear()
            for (item in etagResponse.resObject[0].etagAccountsList!!.indices) {
                if (etagResponse.resObject[0].etagAccountsList!![item].owner == name) {

                    //null for previous year
                    previousMinorListNull(etagResponse, item)
                    Log.e(TAG, "recyclerContent: previous download status ---> $downloadPreStatus")
                    for (i in etagResponse.resObject[0].etagAccountsList?.get(item)?.etags?.get(
                        PREVIOUS_INDEX
                    )?.etagLicensesList?.indices!!) {
                        etagResponse.resObject[0].etagAccountsList?.get(item)?.etags?.get(
                            PREVIOUS_INDEX
                        )?.etagLicensesList?.get(i)?.etagLicenseDetails?.downloadStatus?.let { it1 ->
                            downloadPreStatus.add(it1)

                        }
                    }

                    layoutManager = LinearLayoutManager(activity?.applicationContext)
                    val preAdapter =
                        etagResponse.resObject[0].etagAccountsList?.get(item)?.let {
                            MultipleEtagPreviousYearAdapter(
                                etagResponse.resObject[0].etagAccountsList!![item],
                                offlineSpiidList
                            )
                        }
                    adapter = preAdapter
                    preAdapter!!.setOnMultiPreviousDownloadClick(object :
                        MultipleEtagPreviousYearAdapter.OnMultiPreviousClickListener {
                        override fun onMultiPreviousClick(
                            position: Int,
                            holder: MultipleEtagPreviousYearAdapter.MyMultiPreviousYearViewHolder
                        ) {
                            if (downloadPreStatus[position] == DownloadStatus.OTHER_DEVICE) {
                                showPreviousTagStateDialog(position = position, holder)
                            } else {
                                startPreDownloadProcess(position, holder)

                            }

                        }
                    })
                }
            }
        }


    }

    private fun RecyclerView.itrationForCurrentYear(
        resObject: List<ResObject>,
        name: String,
        etagResponse: EtagResponse
    ) {
        for (item in resObject[0].etagAccountsList!!.indices) {
            if (resObject[0].etagAccountsList!![item].owner?.contains(name) == true) {

                // null for Current Year
                currentMinorListNull(etagResponse, item)


                for (i in resObject[0].etagAccountsList?.get(item)?.etags?.get(0)?.etagLicensesList?.indices!!) {
                    resObject[0].etagAccountsList?.get(item)?.etags?.get(0)?.etagLicensesList?.get(
                        i
                    )?.etagLicenseDetails?.downloadStatus?.let { it1 ->
                        downloadCurrentStatus.add(it1)
                    }
                }

                for (i in resObject[0].etagAccountsList?.get(item)?.etags?.get(
                    0
                )?.etagLicensesList?.indices!!) {
                    val response =
                        resObject[0].etagAccountsList?.get(item)?.etags?.get(0)?.etagLicensesList?.get(
                            i
                        )?.etagLicenseDetails


                    response!!.spiId?.let { it1 ->
                        allDownLoadCurrentSpiIDLIst.add(it1)
                    }

                }
                Log.e(
                    TAG,
                    "Linked current download only one recyclerContent: $allDownLoadCurrentSpiIDLIst",
                )

                val currentAdapter = MultipleEtagCurrentYearAdapter(
                    resObject[0].etagAccountsList!![item],
                    offlineSpiidList
                )

                adapter = currentAdapter
                currentAdapter.notifyDataSetChanged()
                currentAdapter.setOnItemClick(object :
                    MultipleEtagCurrentYearAdapter.OnItemClickListener {
                    override fun onItemClick(
                        position: Int,
                        holder: MultipleEtagCurrentYearAdapter.MyViewHolder
                    ) {
                        if (downloadCurrentStatus[position] == DownloadStatus.OTHER_DEVICE) {
                            showCurrentTagStateDialog(position = position, holder)
                        } else {
                            startCurrentDownloadProcess(position, holder)
                        }


                    }
                })
            }
        }
    }

//    private fun hideProgressBtn(holder: MultipleEtagCurrentYearAdapter.MyViewHolder) {
//        holder.iconProgressBtn.visibility = View.GONE
//        holder.iconDownloadBtn.visibility = View.VISIBLE
//    }

    private fun showProgressBtn(holder: MultipleEtagCurrentYearAdapter.MyViewHolder) {
        holder.iconProgressBtn.visibility = View.VISIBLE
        holder.iconDownloadBtn.visibility = View.GONE
    }

    private fun itrationForAll(
        etagResponse: EtagResponse,
        resObject: List<ResObject>,
        name: String
    ) {
        for (item in etagResponse.resObject?.get(0)?.etagAccountsList!!.indices) {

            loopEtagAccountList(resObject, item, name)
        }
    }

    private fun loopEtagAccountList(
        resObject: List<ResObject>,
        item: Int,
        name: String
    ) {
        for (etagsI in resObject[0].etagAccountsList?.get(item)?.etags?.indices!!) {
            loopEtagLicensesList(resObject, item, etagsI, name)

        }
    }

    private fun loopEtagLicensesList(
        resObject: List<ResObject>,
        item: Int,
        etagsI: Int,
        name: String
    ) {
        for (i in resObject[0].etagAccountsList?.get(item)?.etags?.get(etagsI)?.etagLicensesList?.indices!!) {

            if (resObject[0].etagAccountsList!![item].owner?.contains(name) == true && resObject[0].etagAccountsList!![item].owner!!.lowercase() == name.lowercase()) {
                val response =
                    resObject[0].etagAccountsList?.get(item)?.etags?.get(etagsI)?.etagLicensesList?.get(
                        i
                    )?.etagLicenseDetails

                Log.e(
                    TAG,
                    "multi account ${resObject[0].etagAccountsList!![item].owner} recyclerContent:  ${response!!.isEtagActive} all  download spid -> ${downloadAllStatus}, ${downloadAllSpiIdList} ",
                )

                // downlaod
                statusSpiidDownload(response, response.downloadStatus!!)


                // offload
                statusSpiidOffload(response, response.downloadStatus!!)
            }
        }
    }

    private fun statusSpiidDownload(
        response: EtagLicenseDetails,
        downloadStatus: String
    ) {
        if (response.isEtagActive == EtagActiveStatus.YES) {
            downloadStatus.let { it1 -> downloadAllStatus.add(it1) }
            response.spiId?.let { it1 -> downloadAllSpiIdList.add(it1) }

        }
    }

    private fun statusSpiidOffload(
        response: EtagLicenseDetails,
        downloadStatus: String
    ) {
        if (response.downloadStatus == DownloadStatus.SAME_DEVICE && response.isEtagActive == EtagActiveStatus.YES) {
            response.spiId?.let { it1 ->
                offLoadAllSpiIDLIst.add(it1)
            }
            downloadStatus.let {

                offLoadAllStatus.add(it)
            }

        }
    }

    private fun currentMinorListNull(
        etagResponse: EtagResponse,
        item: Int
    ) {
        when (etagResponse.resObject?.get(0)?.etagAccountsList?.get(item)?.etags?.get(0)?.etagLicensesList.isNullOrEmpty()) {
            true -> {
                binding.noEtagAvailable.noEtagPur.visibility = View.VISIBLE
                binding.currentYearRecyclerView.visibility = View.GONE
                binding.buyAndApply.buyApplyCard.visibility = View.GONE
            }
            false -> {
                binding.noEtagAvailable.noEtagPur.visibility = View.GONE
                binding.currentYearRecyclerView.visibility = View.VISIBLE
                binding.buyAndApply.buyApplyCard.visibility = View.VISIBLE
            }
        }
    }

    private fun previousMinorListNull(
        etagResponse: EtagResponse,
        item: Int
    ) {
        when (etagResponse.resObject?.get(0)?.etagAccountsList?.get(item)?.etags?.get(1)?.etagLicensesList.isNullOrEmpty()) {
            true -> {
                binding.noEtagPrevYear.noEtagYear.visibility = View.VISIBLE
                binding.previousYearRecyclerView.visibility = View.GONE
            }
            false -> {
                binding.previousYearRecyclerView.visibility = View.VISIBLE
                binding.noEtagPrevYear.noEtagYear.visibility = View.GONE
            }
        }
    }

    private fun getMinorNames(etagResponse: EtagResponse) {
        userList.clear()
        userList.add(resources.getString(R.string.select_account))
        for (item in etagResponse.resObject?.get(0)?.etagAccountsList!!.indices) {
            when (etagResponse.resObject[0].etagAccountsList!![item].accountType!!) {
                MULTIPLE_ACCOUNT_KEY ->
                    etagResponse.resObject[0].etagAccountsList!![item].owner?.let { names ->
                        if (names != null && names != "") {
                            userList.add(
                                names
                            )
                        } else {
                            //                            Log.e("TAG", "viewModelObsr: Owner not fouund")
                        }
                    }
            }
        }
    }

    private fun startPreDownloadProcess(
        position: Int,
        holder: MultipleEtagPreviousYearAdapter.MyMultiPreviousYearViewHolder
    ) {
        try {
            val downloadRequest = DownloadOffloadRequest(
                deviceUid = UtilConstant.DEVICE_ID,
                service = UtilConstant.DOWNLOAD,
                spiId = listOf(allPreviousSpiIdList[position])
            )

            if (UtilsMethods.isInternetConnected(requireContext())) {
//                loadingDialog.startLoading()
                showPreviousProgressBtn(holder)
                viewModel.downloadRequest(downloadRequest)
                // Observing Response
                downloadObsr(allPreviousSpiIdList[position])
            } else {
//                UtilsMethods.toastMaker(
//                    requireContext(),
//                    "Check your network connection and try again."
//                )
            }


        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (s: SocketTimeoutException) {
            loadingDialog.isDismiss()
            s.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        }
    }

    private fun showPreviousProgressBtn(holder: MultipleEtagPreviousYearAdapter.MyMultiPreviousYearViewHolder) {
        holder.iconProgressBtn.visibility = View.VISIBLE
        holder.iconDownloadBtn.visibility = View.GONE
    }

    fun startCurrentDownloadProcess(
        position: Int,
        holder: MultipleEtagCurrentYearAdapter.MyViewHolder
    ) {
        try {
//            loadingDialog.startLoading()
            val downloadRequest = DownloadOffloadRequest(
                deviceUid = UtilConstant.DEVICE_ID,
                service = UtilConstant.DOWNLOAD,
                spiId = listOf(allDownLoadCurrentSpiIDLIst[position])
            )

            if (UtilsMethods.isInternetConnected(requireContext())) {
                showProgressBtn(holder)
                viewModel.downloadRequest(downloadRequest)
                // Observing Response
                downloadObsr(allDownLoadCurrentSpiIDLIst[position])
            } else {
//                UtilsMethods.toastMaker(requireContext(), "You are currently offline")
            }

        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (s: SocketTimeoutException) {
            loadingDialog.isDismiss()
            s.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        }
    }

    private fun showCurrentTagStateDialog(
        position: Int,
        holder: MultipleEtagCurrentYearAdapter.MyViewHolder
    ) {
        val builder = AlertDialog.Builder(requireContext()).create()
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.custom_dialog, null)
        val cancelBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = getString(R.string.other_download_primary)
        des.text = getString(R.string.other_download_secondary)
        cancelBtn.text = resources.getText(R.string.cancelText)
        sBtn.text = resources.getText(R.string.download)
        builder.setView(view)
        cancelBtn.setOnClickListener {
            builder.dismiss()
        }
        sBtn.setOnClickListener {
            startCurrentDownloadProcess(position, holder)
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    private fun showPreviousTagStateDialog(
        position: Int,
        holder: MultipleEtagPreviousYearAdapter.MyMultiPreviousYearViewHolder
    ) {
        val builder = AlertDialog.Builder(requireContext()).create()
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.custom_dialog, null)
        val cancelBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = getString(R.string.other_download_primary)
        des.text = getString(R.string.other_download_secondary)
        cancelBtn.text = resources.getText(R.string.cancelText)
        sBtn.text = resources.getText(R.string.download)
        builder.setView(view)
        cancelBtn.setOnClickListener {
            builder.dismiss()
        }
        sBtn.setOnClickListener {
            startPreDownloadProcess(position, holder)
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    @OptIn(InternalPlatformTextApi::class)
    private fun setSpannableString() {
        val spannable = SpannableString(resources.getString(R.string.buy_string))
// Setting
// font family
        val myTypeface =
            Typeface.create(
                ResourcesCompat.getFont(requireContext(), R.font.font_open_sens_bold),
                Typeface.BOLD
            )
        spannable.setSpan(
            TypefaceSpan(myTypeface),
            59,
            spannable.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )

// Setting string
// to textview
        binding.noEtagAvailable.textView11.text = spannable
        binding.buyAndApply.textView11.text = spannable
        binding.noEtagAvailable.noEtagPurchase.text =
            resources.getText(R.string.no_purchase_etag)
    }


    private fun showPopupMenu(context: Context, view: View) {
        val wrapper: Context = ContextThemeWrapper(context, R.style.CustomPopUpStyle)
        val popup = PopupMenu(wrapper, view)
        popup.inflate(R.menu.popup_menu)
        popup.setOnMenuItemClickListener { item: MenuItem? ->
            when (item!!.itemId) {
                R.id.remove -> {
                    if (UtilsMethods.isInternetConnected(requireContext())) {

                        if (offLoadAllStatus.contains(DownloadStatus.SAME_DEVICE)) {
                            showRemoveAllTagStateDialog(
                                context,
                                "Remove all downloaded E-tags?",
                                "You won’t be able to validate your E-tags unless they are downloaded to your device. \n" +
                                    "Do you want to remove all E-tags from this device?",
                                "Cancel",
                                "Remove all"
                            )
                        } else {

                            UtilsMethods.showSimpleAlert(
                                requireContext(),
                                "There are no E-tags to remove.",
                                "You don’t have any E-tags downloaded currently.",
                                "Ok"
                            )
                        }

                    } else {
                        UtilsMethods.showNoNetwokDialog(requireContext())

                    }
                }
            }

            true
        }

        popup.show()
    }

    private fun viewModelObsr(name: String) {

        if (UtilsMethods.isInternetConnected(requireContext())) {
//            loadingDialog.isDismiss()
            viewModel.etagResponse.observe(viewLifecycleOwner) {
                it?.let {
                    when (it.responseCode) {
                        UtilConstant.SUCCESS_CODE -> {
                            setupUI(it, name)
                        }
                    }
                }
            }
        } else {
            val gson = Gson()
            val json = SharedPrefs.read(UtilConstant.USER_ETAG, "")
            if (!json.isNullOrEmpty()) {
                setupUI(gson.fromJson(json, EtagResponse::class.java), name)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupUI(it: EtagResponse, name: String) {
        clearList()
        loadingDialog.isDismiss()
        binding.noEtagPrevYear.noEtagPurchasePre.text = " You held no E-tags for the ${
            it.resObject?.get(0)?.etagAccountsList?.get(0)?.etags?.get(1)?.licenseYear
        } license year."

        binding.previousYearHeading.text =
            "${it.resObject?.get(0)?.etagAccountsList?.get(0)?.etags?.get(1)?.licenseYear} ${
                resources.getString(
                    R.string.previousYearText
                )
            }"
        binding.yearHeader.text =
            "${it.resObject?.get(0)?.etagAccountsList?.get(0)?.etags?.get(0)?.licenseYear} ${
                resources.getString(
                    R.string.currentYearText
                )
            }"

        // linked owner list
        getMinorNames(it)
        for (item in it.resObject?.get(0)?.etagAccountsList!!.indices) {
            if (it.resObject[0].etagAccountsList?.get(item)?.owner == name) {
                /*  for (i in it.resObject?.get(0).etagAccountsList?.get(item)?.etags!!.indices){

                  }*/
                for (i in it.resObject[0].etagAccountsList?.get(item)?.etags?.get(PREVIOUS_INDEX)?.etagLicensesList?.indices!!) {

                    val response =
                        it.resObject[0].etagAccountsList?.get(item)?.etags?.get(PREVIOUS_INDEX)?.etagLicensesList?.get(
                            i
                        )?.etagLicenseDetails

                    response?.spiId?.let { it1 -> allPreviousSpiIdList.add(it1) }

                }
            }
        }

        loadingDialog.isDismiss()
        recyclerContent(name, it)
    }

    private fun clearList() {
        allPreviousSpiIdList.clear()
//        downLoadPreSpiIDLIst.clear()
        allDownLoadCurrentSpiIDLIst.clear()
        downloadCurrentStatus.clear()
    }

    private fun showRemoveAllTagStateDialog(
        mContext: Context,
        tvPrimary: String,
        tvSecondary: String,
        cancelText: String,
        secText: String,
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val cancelBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = tvPrimary
        des.text = tvSecondary
        cancelBtn.text = cancelText
        sBtn.text = secText
        builder.setView(view)
        cancelBtn.setOnClickListener {
            builder.dismiss()
        }
        sBtn.setOnClickListener {

            try {
                loadingDialog.startLoading()

                val offloadRequest = DownloadOffloadRequest(
                    deviceUid = UtilConstant.DEVICE_ID,
                    service = UtilConstant.OFFLOAD,
                    spiId = offLoadAllSpiIDLIst
                )

                viewModel.offLoadRequest(offloadRequest)

                // Observing Response
                offLoadAllSpiIDLIst.forEach {
                    offloadObsr(it)
                }


            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (s: SocketTimeoutException) {
                loadingDialog.isDismiss()
                s.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }

            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }


    private fun downloadObsr(spiIDLIst: String) {
        viewModel.downloadResponse.observe(viewLifecycleOwner) {
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    viewModel.getEtag(
                        EtagRequest(
                            UtilConstant.DEVICE_ID
                        )
                    )
                    viewModel.insertDownloadTag(
                        DownloadedTags(
                            downloadedSpiId = spiIDLIst
                        )
                    )
                    it.resObject?.get(CURRENT_INDEX)
                        ?.let {
//                            it1.message?.get(CURRENT_INDEX)?.let { it2 ->
//                                UtilsMethods.toastMaker(
//                                    requireContext(),
//                                    it2
//                                )
//                            }
                        }
                }
                UtilConstant.FAIL_CODE -> {
                    it.resObject?.get(CURRENT_INDEX)
                        ?.let { it1 ->
                            it1.message?.get(CURRENT_INDEX)?.let {
                                /*        UtilsMethods.toastMaker(
                                            requireContext(),
                                            it2
                                        )*/
                            }
                        }
                }
            }
        }
    }

    private fun offloadObsr(idString: String) {
        viewModel.offloadResponse.observe(viewLifecycleOwner) {

            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    viewModel.getEtag(
                        EtagRequest(
                            UtilConstant.DEVICE_ID
                        )
                    )
                    viewModel.removeDownloadTagsById(idString)
                    it.resObject?.get(CURRENT_INDEX)
                        ?.let {
//                            it1.message?.get(CURRENT_INDEX)?.let { it2 ->
//                                UtilsMethods.toastMaker(
//                                    requireContext(),
//                                    it2
//                                )
//                            }
                        }
                }
                UtilConstant.FAIL_CODE -> {
                    it.resObject?.get(CURRENT_INDEX)
                        ?.let { it1 ->
                            it1.message?.get(CURRENT_INDEX)?.let {
                                /*  UtilsMethods.toastMaker(
                                      requireContext(),
                                      it2
                                  )*/
                            }
                        }
                }
            }
        }
    }

    private fun showDownloadAllTagStateDialog(
        mContext: Context,
        tvPrimary: String,
        tvSecondary: String,
        cancelText: String,
        secText: String,
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val cancelBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = tvPrimary
        des.text = tvSecondary
        cancelBtn.text = cancelText
        sBtn.text = secText
        builder.setView(view)
        cancelBtn.setOnClickListener {
            builder.dismiss()
        }
        sBtn.setOnClickListener {


            try {
                loadingDialog.startLoading()

                val downloadRequest = DownloadOffloadRequest(
                    deviceUid = UtilConstant.DEVICE_ID,
                    service = UtilConstant.DOWNLOAD,
                    spiId = downloadAllSpiIdList
                )

                viewModel.downloadRequest(downloadRequest)

                // Observing Response
                downloadAllSpiIdList.forEach {

                    downloadObsr(it)
                }


            } catch (e: Exception) {
                e.printStackTrace()
            } catch (s: SocketTimeoutException) {
                s.printStackTrace()
            } catch (e3: SSLException) {
                e3.printStackTrace()
            }

            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

/*    override fun onPause() {
        UtilConstant.ETAG_LAST_VIEW_INDEX = 1
        Log.e("Linked", "onPause: setting Index : ${UtilConstant.ETAG_LAST_VIEW_INDEX}")
        super.onPause()
    }*/
}
