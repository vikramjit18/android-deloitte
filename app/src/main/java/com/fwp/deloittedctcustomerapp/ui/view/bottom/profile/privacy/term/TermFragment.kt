/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.privacy.term

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.TermFragmentBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class TermFragment : BottomSheetDialogFragment() {

    companion object {
        fun newInstance() = TermFragment()
    }

    private val viewModel: TermViewModel by viewModels()
    private var _binding: TermFragmentBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = TermFragmentBinding.inflate(inflater, container, false)
        val root = binding.root
        binding.agrement.mainCurvLayout.setBackgroundColor(resources.getColor(R.color.bgColor))
        binding.agrement.constraintLayout3.visibility = View.GONE
        binding.agrement.ivBack.visibility =View.GONE
        binding.agrement.linearProgress.visibility = View.GONE
        buttonHandler()
        return root
    }

    private fun buttonHandler() {
        binding.close.setOnClickListener() {
//            UtilsMethods().backStack(findNavController())
            dismiss()
        }
    }


}
