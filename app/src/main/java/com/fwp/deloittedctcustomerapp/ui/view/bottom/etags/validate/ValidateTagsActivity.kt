/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException


// FWP
// Hilt entry point
@AndroidEntryPoint
class ValidateTagsActivity : AppCompatActivity() {


    private lateinit var validateTagActivityViewModel: ValidateTagActivityViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_validate_tags)
        validateTagActivityViewModel =
            ViewModelProvider(this)[ValidateTagActivityViewModel::class.java]
        // api hits
        SharedPrefs.init(this)
        if (UtilsMethods.isInternetConnected(this)) {

            validateTagActivityViewModel.getValidateTagData().observe(this, {
                autoValidatingEtag(it)


            })
        }
    }

    private fun autoValidatingEtag(it: List<ValidateEtagRequest>) {
        if (!it.isNullOrEmpty()) {
            it.forEach {
                val request = ValidateEtagRequest(
                    id = it.id,
                    deviceId = it.deviceId,
                    // etaguploaddate
                    etagUploadDate = UtilsMethods.getCurrentDateTime(),
                    harvestConfNum = it.harvestConfNum,
                    //current date
                    harvestedDate = it.harvestedDate,
                    lat = it.lat,
                    longt = it.longt,
                    speciesCd = it.speciesCd,
                    spiId = it.spiId,
                )
                Log.e("api db before api hit", "validate activity: \"from db $request\"")
                try {
                    validateTagActivityViewModel.getEtagValidated(request)


                } catch (e: Exception) {

                    e.printStackTrace()
                } catch (e2: SocketTimeoutException) {
                    e2.printStackTrace()
                } catch (e3: SSLException) {
                    e3.printStackTrace()
                } catch (e4: IOException) {
                    e4.printStackTrace()
                }
            }

        }
    }
// disabling back button
    override fun onBackPressed() {
        // removed super function to disable phone's back button
    }
}





