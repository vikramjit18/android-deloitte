/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.*
import com.fwp.deloittedctcustomerapp.databinding.ValidateSuccessFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import com.google.gson.Gson

class ValidateSuccessFragment : Fragment() {

    companion object {
        fun newInstance() = ValidateSuccessFragment()
    }

    private lateinit var viewModel: LandingScreenActivityViewModel
    private var _binding: ValidateSuccessFragmentBinding? = null
    private val binding get() = _binding!!
    private var confirmationNumber: Any? = null
    private var owner: Any? = null
    private var opportunity: Any? = null
    private var description: Any? = null
    private var harvestTime: Any? = null
    private var eTagYearIndex: Any? = null
    private var accountOwner: Any? = null
    private var position: Any? = null
    private var extras: Bundle? = null
    private val gson = Gson()

    //    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
//        return if (enter) {
//            AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
//        } else {
//            AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
//        }
//    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ValidateSuccessFragmentBinding.inflate(inflater, container, false)
        val root = binding.root
        //Showing loader
        viewModel = ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]
        buttonProperty()
        initState()
        buttonHandler()
        confirmationNumber = arguments?.getString(UtilConstant.ETAG_CON)
        owner = arguments?.getString(UtilConstant.ETAG_OWNER)
        opportunity = arguments?.getString(UtilConstant.ETAG_OP)
        description = arguments?.getString(UtilConstant.ETAG_DES)
        harvestTime = arguments?.getString(UtilConstant.HARVEST_TIME)
        position = arguments?.getString(UtilConstant.GET_ETAG_INDEX_KEY)
        accountOwner = arguments?.getString(UtilConstant.GET_ETAG_ACCOUNT_OWNER)
        eTagYearIndex = arguments?.getString(UtilConstant.GET_ETAG_YEAR_KEY)



        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        buttonHandler()
        viewModelSetup()
    }


    private fun initState() {


        val heading: String = getString(R.string.successHeading)
        val spannable = SpannableString(heading)
        spannable.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.prim_yellow, null)),
            0,
            8,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE,
        )
        binding.head.text = spannable

        val pngImage = ContextCompat.getDrawable(requireContext(), R.drawable.ic_etags)
        binding.icConfirmNumber.setImageDrawable(pngImage)

        binding.mandatoryContainer.step1IcCheck.visibility = View.VISIBLE


    }

    private fun buttonHandler() {
        binding.back.setOnClickListener() {
            UtilsMethods.activityBack(requireActivity())
            UtilConstant.backStatus = true
        }
        binding.viewEtag.btn.setOnClickListener() {
            val bundle = Bundle()
            bundle.putString(UtilConstant.ETAG_CON, confirmationNumber.toString())
            bundle.putString(UtilConstant.HARVEST_TIME, harvestTime.toString())
            bundle.putString(UtilConstant.GET_ETAG_ACCOUNT_OWNER, accountOwner.toString())
            bundle.putString(UtilConstant.GET_ETAG_INDEX_KEY, position.toString())
            bundle.putString(UtilConstant.GET_ETAG_YEAR_KEY, eTagYearIndex.toString())
            UtilsMethods.navigateToFragmentWithValues(
                binding.root,
                R.id.validatedEtagFragment,
                bundle
            )

        }
        binding.mandatoryContainer.step2DescWeb.setOnClickListener {
            UtilsMethods.websiteOpener("https://fwp.mt.gov", requireActivity())
        }

        binding.ibAlertHarvestTime.setOnClickListener {
            UtilsMethods.showSimpleAlert(
                requireContext(),
                resources.getString(R.string.timestamp),
                resources.getString(R.string.harvest_alert_des),
                resources.getString(R.string.ok)

            )
        }
    }


    private fun buttonProperty() {
        binding.viewEtag.btn.text = resources.getString(R.string.view_e_tag_details)
        binding.viewEtag.btn.setTextColor(resources.getColor(R.color.black, null))
        binding.viewEtag.btn.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
    }


    @SuppressLint("SetTextI18n")
    private fun viewModelSetup() {

        val json = SharedPrefs.read(UtilConstant.USER_ETAG, "")
        if(!json.isNullOrEmpty()){
        setupUI(gson.fromJson(json, EtagResponse::class.java))
    }}

    private fun setupUI(
        it: EtagResponse
    ) {
        when (it.responseCode) {
            UtilConstant.SUCCESS_CODE -> {

                if (!position.toString().isNullOrEmpty() && !accountOwner.toString()
                        .isNullOrEmpty() && !eTagYearIndex.toString().isNullOrEmpty()
                ) {
                    ownerAccountData(
                        it
                    )
                }


            }

            UtilConstant.ERROR_CODE -> {
                /*     it.resObject?.get(0)
                        ?.let { it1 -> UtilsMethods.toastMaker(requireContext(), it1.toString()) }
              */
            }


        }

    }

    private fun ownerAccountData(
        it: EtagResponse
    ) {
        for (item in it.resObject?.get(0)?.etagAccountsList!!.indices) {
            ownerSetup(
                it.resObject,
                item
            )
        }
    }

    private fun ownerSetup(
        resObject: List<ResObject>,
        item: Int
    ) {
        when (resObject[0].etagAccountsList!![item].owner?.contains(
            accountOwner.toString()
        )) {
            true -> resObject[0].etagAccountsList?.get(item)
                ?.let { etagAccounts ->
                    val etagList = etagAccounts.etags?.get(
                        eTagYearIndex.toString().toInt()
                    )?.etagLicensesList?.get(position.toString().toInt())!!

                    apiResponseMandatory(etagList.etagLicenseDetails?.etagsubDetails?.eTagMandatoryDetails)

                    // binding title
                    val title = "${
                        etagAccounts.etags[eTagYearIndex.toString().toInt()].licenseYear
                    } ${etagList.licenseName}"
                    binding.title.text = title
                    // binding opportunity
                    binding.tvOpportunity.text =
                        etagList.etagLicenseDetails?.etagsubDetails!!.oppurtunity
                    //binding desc
                    binding.tvDescription.text =
                        etagList.etagLicenseDetails.etagsubDetails.description
                    //binding owner
                    binding.tvOwner.text = etagAccounts.owner
                    //binding con#
                    binding.tvCon.text = confirmationNumber.toString()

                    // binding Harvest#

                    dateFormatter()

                }


            false -> {
                resObject.get(0)
                    .let { _ ->
                    }
            }
        }
    }

    private fun dateFormatter() {
        if (harvestTime.toString().isNotEmpty()) {
            val dateTimeSplit = harvestTime.toString().split(" ")
            val alterTime: String =
                if (dateTimeSplit.contains("PM")) {
                    "${dateTimeSplit[0]} ${
                        dateTimeSplit[2].substring(
                            0,
                            5
                        )
                    } p.m."
                } else {
                    "${dateTimeSplit[0]} ${
                        dateTimeSplit[2].substring(
                            0,
                            5
                        )
                    } a.m."
                }
            binding.tvHarvest.text = alterTime
        }
    }


    private fun apiResponseMandatory(eTagMandatoryDetails: List<ETagMandatoryDetail>?) {
        if (eTagMandatoryDetails != null && eTagMandatoryDetails[0].reReportingHTMLText?.isNotEmpty() == true) {
            binding.subHead.visibility = View.VISIBLE
            binding.mandatoryContainer.clMandatory.visibility = View.VISIBLE
            eTagMandatoryDetails.forEach {
                when (it.rsReportingStep) {

                    "1" -> {
                        val step1Heading = "Step 1 – ${
                            eTagMandatoryDetails[0].reReportingHTMLText!!.substringBefore("|")
                        }"
                        binding.mandatoryContainer.step1Heading.text = step1Heading

                        binding.mandatoryContainer.step1Desc.visibility = View.GONE
                    }
                    "2" -> {
                        var result: String = ""
                        val step2Heading = "Step 2 – ${
                            eTagMandatoryDetails[1].reReportingHTMLText!!.substringBefore("|")
                        }"
                        binding.mandatoryContainer.step2Heading.text = step2Heading
                        binding.mandatoryContainer.step2Desc.text =
                            eTagMandatoryDetails[1].reReportingHTMLText!!.substringAfter("|")
                                .substringBefore("\n\nBy phone:")
                        binding.mandatoryContainer.step2Desc.visibility = View.VISIBLE
//                  Phone no set
                        val no =   eTagMandatoryDetails[1].reReportingHTMLText!!.substringAfter("\n\nBy phone:\n").substringBefore("\n\nOnline:")
                        if (!no.isNullOrEmpty() && UtilConstant.PHONE_PATTERN.matcher(no).matches()) {
                            binding.mandatoryContainer.step2DescCall1.visibility = View.VISIBLE
                            binding.mandatoryContainer.step2DescCall2.visibility = View.VISIBLE
                            val contentUnderLine = SpannableString(no)
                            contentUnderLine.setSpan(
                                UnderlineSpan(),
                                0,
                                no.length,
                                0
                            )
                            binding.mandatoryContainer.step2DescCall2.text = contentUnderLine
                            binding.mandatoryContainer.step2DescCall2.setOnClickListener {
                                UtilsMethods.dialerOpener(
                                    "1$no",
                                    requireActivity()
                                )
                            }
                        }

                        if (eTagMandatoryDetails[1].reReportingHTMLText!!.contains("fwp.mt.gov")){
                            binding.mandatoryContainer.step2DescWeb.visibility = View.VISIBLE
                            binding.mandatoryContainer.step2DescWeb.setOnClickListener {
                                UtilsMethods.websiteOpener("https://fwp.mt.gov", requireActivity())
                            }
                        }else{
                            binding.mandatoryContainer.step2DescWeb.visibility = View.GONE
                        }
                    }
                    "3" -> {
                        val step3Heading = "Step 3 – ${
                            eTagMandatoryDetails[2].reReportingHTMLText!!.substringBefore("|")
                        }"
                        binding.mandatoryContainer.step3Heading.text = step3Heading

                        binding.mandatoryContainer.step3Desc.text =
                            eTagMandatoryDetails[2].reReportingHTMLText!!.substringAfter("|")
                    }
                }
            }
        } else {
            binding.mandatoryContainer.clMandatory.visibility = View.INVISIBLE
        }



        binding.mandatoryContainer.step2DescCall1.setOnClickListener {
            UtilsMethods.dialerOpener(getString(R.string.personally_call_1_no), requireActivity())

        }
        binding.mandatoryContainer.step2DescCall2.setOnClickListener {
            UtilsMethods.dialerOpener(getString(R.string.personally_call_2_no), requireActivity())
        }

    }

    private fun extractingPhoneNo(
        eTagMandatoryDetails: List<ETagMandatoryDetail>
    ): String {
        var result1 = ""
        when {
            eTagMandatoryDetails[1].reReportingHTMLText!!.contains("12 hours,") -> {
                result1 =
                    eTagMandatoryDetails[1].reReportingHTMLText?.substringAfter(
                        "12 hours,"
                    )!!
            }
            eTagMandatoryDetails[1].reReportingHTMLText!!.contains("24 hours,") -> {
                result1 =
                    eTagMandatoryDetails[1].reReportingHTMLText?.substringAfter(
                        "24 hours,"
                    )!!
            }
            eTagMandatoryDetails[1].reReportingHTMLText!!.contains("48 hours,") -> {
                result1 =
                    eTagMandatoryDetails[1].reReportingHTMLText?.substringAfter(
                        "48 hours,"
                    )!!
            }
            else ->{
                result1 = ""
            }
        }
        return result1
    }
}
