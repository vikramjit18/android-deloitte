/*
 * Montana Fish, Wildlife & Parks
 */



package com.fwp.deloittedctcustomerapp.ui.view.login

import android.annotation.SuppressLint
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.*
import android.text.style.ForegroundColorSpan
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricPrompt
import androidx.compose.ui.text.android.InternalPlatformTextApi
import androidx.compose.ui.text.android.style.TypefaceSpan
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.LoginRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse
import com.fwp.deloittedctcustomerapp.databinding.FragmentLoginBinding
import com.fwp.deloittedctcustomerapp.ui.view.MainActivity
import com.fwp.deloittedctcustomerapp.ui.view.bottom.LandingScreenActivity
import com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotActivity
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpActivity
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import java.util.concurrent.Executor
import javax.net.ssl.SSLException

/**
 * FWP
 * Hilt android entry
 */
@AndroidEntryPoint
class Login : Fragment() {
    /**
     * Global variables
     */
    lateinit var binding: FragmentLoginBinding
    private var biometricPrompt: BiometricPrompt? = null
    private var promptInfo: BiometricPrompt.PromptInfo? = null
    private val loginViewModel: LoginViewModel by viewModels()
    private lateinit var loadingDialog: LoadingDialog
    private lateinit var executor: Executor
    private var keyguard: KeyguardManager? = null

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_bottom_to_top)
        } else {
            AnimationUtils.loadAnimation(context, R.anim.slide_top_to_bottom)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        /**
         * Binding Initiate
         */
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        binding.textView8.visibility = View.INVISIBLE
        binding.enableBiometric.visibility = View.INVISIBLE
        // init loader
        loadingDialog = LoadingDialog(mActivity = requireActivity())
        // init shared prefs
        SharedPrefs.init(requireActivity())
        // Biometric Initiate
        /**
         * Initiate
         * Biometric process
         */
        executor = ContextCompat.getMainExecutor(requireActivity())
        keyguard = requireActivity().getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
//        biometricPrompt =
//            BiometricPrompt(
//                requireActivity(),
//                executor,
//                object : BiometricPrompt.AuthenticationCallback() {
//
//                    /*     override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
//                             super.onAuthenticationError(errorCode, errString)
//
//                         }*/
//
//                    /**
//                     * Function for success
//                     * biometric scan
//                     */
//                    override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
//                        super.onAuthenticationSucceeded(result)
////                        UtilsMethods.toastMaker(requireContext(), "${keyguard!!.isDeviceSecure}")
//                        UtilConstant.biometricAuthDone = true
//
//                        /**
//                         * Navigation to
//                         * login agreement
//                         */
//                        UtilsMethods.navigateToFragment(
//                            binding.root,
//                            R.id.action_login2_to_loginAgreement
//                        )
//                    }
//
//                    /* override fun onAuthenticationFailed() {
//                         super.onAuthenticationFailed()
//                     }*/
//
//                })

/*        */
        /**
         * Biometric
         * Scan Builder
         *//*
        // Biometric Scan Builder
        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Authenticate ${getString(R.string.app_name)}")
            .setNegativeButtonText("Cancel")
            .setConfirmationRequired(false)
            .build()*/


        /**
         * Returning
         * root binding
         */
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Init Execution
        init()

        viewModelObser()

        enablePrimaryBtn()

        loginBtnHandler()

    }

    override fun onResume() {
        super.onResume()
//        biometricAuthenticate()
    }

    /**
     * Initiate
     * init Function
     */
    private fun init() {

        /**
         * Setting edittext
         * temporary base
         */

/*        binding.email.setText("saisriram")

        binding.pass.setText("Sai#123")*/

        /**
         * Global Functions
         */
        backButton()
        biometricSwitch()
        forgotButton()
        signUpButton()
        /*    buttonActivation()
            emailValidator()
            passValidator()*/

        /**
         * TextWatch listener
         * for any changes
         */
/*        binding.email.addTextChangedListener(mTextWatcher)
        binding.pass.addTextChangedListener(mTextWatcher)*/

    }

    /**
     * Biometric
     * switch button
     */
    private fun biometricSwitch() {
        val biometricBool = SharedPrefs.read(UtilConstant.BIOMETRIC_ENABLE, "")
        binding.enableBiometric.isChecked = !biometricBool.isNullOrEmpty()
        binding.enableBiometric.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                showBiometricAlert()
                /*       when (UtilConstant.biometricAuthDone) {
       //                    true -> biometricAuthenticate()

                           false -> showBiometricAlert()
                       }*/
            } else {
                UtilConstant.biometric = false
//                UtilConstant.biometricAuthDone = false
                UtilConstant.itemLastIndex = 0
                UtilConstant.etagLastIndex = 0
                UtilConstant.ITEMS_LAST_VIEW_INDEX = 0
                UtilConstant.ACCOUNT_VIEWPAGER = 0
                UtilConstant.alsExpiredDialog = true
                SharedPrefs.clearSharedPref()
            }
        }
    }


    /**
     * Initiate
     *Biometric function
     */
    private fun biometricAuthenticate() {
        val biometricManager = BiometricManager.from(requireActivity())
        if (Build.VERSION.SDK_INT >= 29) {
            binding.textView8.visibility = View.VISIBLE
            binding.enableBiometric.visibility = View.VISIBLE
            if (biometricManager.canAuthenticate(BIOMETRIC_STRONG) != BiometricManager.BIOMETRIC_SUCCESS) {
                binding.textView8.visibility = View.INVISIBLE
                binding.enableBiometric.visibility = View.INVISIBLE
            } else {
                binding.textView8.visibility = View.VISIBLE
                binding.enableBiometric.visibility = View.VISIBLE
            }
        } else {
            binding.enableBiometric.visibility = View.INVISIBLE
            binding.textView8.visibility = View.INVISIBLE
        }
    }

    /**
     * Showing Biometric
     * Alert Dialog
     */
    @SuppressLint("SetTextI18n")
    @OptIn(InternalPlatformTextApi::class)
    private fun showBiometricAlert() {
        val builder = AlertDialog.Builder(requireContext())
            .create()
        val view = layoutInflater.inflate(R.layout.custom_dialog, null)
        val leftButton = view.findViewById<TextView>(R.id.btn_left)
        val rightButton = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = getString(R.string.biometricDialogTitleText)
        des.text = "Turning this on allows you to log in using your fingerprint or facial ID."
        title.setTextColor(resources.getColor(R.color.onboardTextColor, null))
        title.typeface = Typeface.create(
            ResourcesCompat.getFont(requireContext(), R.font.opensreg),
            Typeface.NORMAL
        )
        leftButton.text = "Not now"
        rightButton.text = "Turn on"
        builder.setView(view)
        leftButton.setOnClickListener {
            builder.dismiss()
            binding.enableBiometric.isChecked = false
        }
        rightButton.setOnClickListener() {
            builder.dismiss()
            UtilConstant.biometric = true
//            shared prefs save
//            biometricAuthenticate()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    /**
     * Showing reset password
     * Alert Dialog
     */
    @SuppressLint("SetTextI18n")
    @OptIn(InternalPlatformTextApi::class)
    private fun showResetPasswordAlert() {

        val builder = AlertDialog.Builder(requireContext())
            .create()
        val view = layoutInflater.inflate(R.layout.custom_dialog, null)
        val leftButton = view.findViewById<TextView>(R.id.btn_left)
        val rightButton = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text =
            "Login error"
        des.text = "We couldn’t find a MyFWP account matching the login information provided."
//        title.setTextColor(resources.getColor(R.color.onboardTextColor, null))
//        title.typeface = Typeface.create(
//            ResourcesCompat.getFont(requireContext(), R.font.opensreg),
//            Typeface.NORMAL
//        )
        rightButton.text = resources.getText(R.string.resetPassText)
        leftButton.text = resources.getText(R.string.tryAgain)
        builder.setView(view)
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
        rightButton.setOnClickListener {
            builder.dismiss()
            val i = Intent(requireActivity(), ForgotActivity::class.java)
            requireActivity().startActivity(i)
        }
        leftButton.setOnClickListener {

            builder.dismiss()
        }


    }

    /**
     * Initiate
     * forget button function
     */
    private fun forgotButton() {
        binding.forgot.setOnClickListener {
            /**
             * Navigate to
             * Forget Fragments
             */
//            UtilsMethods.navigateToFragment(binding.root, R.id.navigateToForgot1)
            val i = Intent(requireActivity(), ForgotActivity::class.java)
            requireActivity().startActivity(i)
            requireActivity().overridePendingTransition(
                R.anim.slide_bottom_to_top, R.anim.slide_top_to_bottom
            )
        }

    }

    /**
     * Initiate
     * signup button function
     */
    private fun signUpButton() {
        signUpSpan()
        binding.signup.setOnClickListener {
            /**
             * Navigate to
             * Sign up Fragments
             */
            val i = Intent(requireActivity(), SignUpActivity::class.java)
            requireActivity().startActivity(i)
            requireActivity().overridePendingTransition(
                R.anim.slide_bottom_to_top,
                R.anim.slide_top_to_bottom
            )
        }
    }

    /**
     * Initiate
     * close button function
     */
    private fun backButton() {
        binding.back.setOnClickListener {
            /**
             * Navigate to
             * previous Fragment
             */

            val i = Intent(requireActivity(), MainActivity::class.java)
            requireActivity().startActivity(i)
            requireActivity().overridePendingTransition(
                R.anim.slide_bottom_to_top,
                R.anim.slide_top_to_bottom
            )
            requireActivity().finish()

        }

    }

    /**
     * Setting
     * Signup text
     */
    @OptIn(InternalPlatformTextApi::class)
    private fun signUpSpan() {
        /**
         * Changing
         * font style of text
         */
        val myTypeface = Typeface.create(
            ResourcesCompat.getFont(requireContext(), R.font.montsemibold),
            Typeface.NORMAL
        )
        val spannable = SpannableString(resources.getString(R.string.dontHaveFwpSignUp))
        spannable.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.prim_yellow, null)),
            28,
            spannable.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE,
        )
        spannable.setSpan(
            TypefaceSpan(myTypeface),
            28,
            spannable.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )

        /**
         * Setting
         * custom text to Textview
         */
        binding.signup.text = spannable
    }

    /*  */
    /**
     * Initiate
     * email validator function
     *//*
    fun emailValidator(): Boolean {
        val emailL = binding.emailLayout
        val email: String = binding.email.text.toString()
        val errorDrawable = ContextCompat.getDrawable(requireContext(), R.drawable.ic_error)
        errorDrawable!!.setBounds(0, 5, 40, 40);

        if (email.contains("@")) {
            when (!UtilConstant.EMAIL_ADDRESS_PATTERN.matcher(email).matches()) {
                true -> {
                    emailL.isErrorEnabled = true
                    emailL.error =
                        SpannableString("    Enter a valid email address").apply {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                setSpan(
                                    ImageSpan(errorDrawable, ImageSpan.ALIGN_CENTER),
                                    0,
                                    1,
                                    Spanned.SPAN_INCLUSIVE_EXCLUSIVE
                                )
                            }
                        }
                }
                false -> {
                    emailL.isErrorEnabled = false
                }
            }
            return false
        } else {

            return true
        }
    }

    */
    /**
     * Initiate
     * password validator function
     *//*
    fun passValidator(): Boolean {
        val password: String = binding.pass.text.toString()
        val errorDrawable = ContextCompat.getDrawable(requireContext(), R.drawable.ic_error)
        errorDrawable!!.setBounds(0, 5, 40, 40);
        return if (password.isEmpty()) {
            false
        } else UtilConstant.PASSWORD_PATTERN.matcher(password).matches()
    }

    */
    /**
     * Initiate
     * button activation function
     *//*
    private fun buttonActivation() {
        binding.login.btn.text = resources.getText(R.string.loginText)
        when ((binding.email.text.toString().trim()
            .isNotEmpty() || UtilConstant.EMAIL_ADDRESS_PATTERN.matcher(
            binding.email.text.toString().trim()
        )
            .matches()) && binding.pass.text.toString().trim().isNotEmpty()
        ) {
            true -> {
                //
                enablePrimaryBtn()
                loginBtnHandler()
            }
            false -> {
                enablePrimaryBtn()

            }
        }
    }*/

    private fun loginBtnHandler() {
        binding.login.btn.setOnClickListener {

            // Checking Internet connection
            if (isFieldNotEmpty()) {
                if (UtilsMethods.isInternetConnected(requireContext())) {

                    apiHitLogin()

                    //Observing user login response
//                viewModelObser()
                } else {

                    // Showing
                    // No internet dialog
                    UtilsMethods.showSimpleAlert(
                        mContext = requireContext(),
                        tvPrimary = getString(R.string.no_connection),
                        tvSecondary = getString(R.string.check_and_try_again),
                        primaryBtn = resources.getString(R.string.ok)
                    )
                }
            }
        }
    }

    private fun apiHitLogin() {
        //Showing loader
        loadingDialog.startLoading()

        // Initiate User login Api
        try {
            loginViewModel.loginUserRequest(
                LoginRequest(
                    password = binding.pass.text.toString().trim(),
                    username = binding.email.text.toString().trim()
                )
            )
        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (e2: SocketTimeoutException) {
            loadingDialog.isDismiss()
            e2.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        }
    }

    private fun viewModelObser() {

        loginViewModel.userLoginResponse.observe(requireActivity()) { loginResponse ->
            //  Hiding progress dialog
            loadingDialog.isDismiss()
            when (loginResponse.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    initiateLoginProcess(loginResponse)
                }
                UtilConstant.FAIL_CODE -> {
                    showResetPasswordAlert()
                }
            }
        }

        // Observing for any error message from server
        loginViewModel.toastLoginResponse.observe(viewLifecycleOwner) {
            it.getContentIfNotHandledOrReturnNull()?.let { message ->
                loadingDialog.isDismiss()
//                UtilsMethods.toastMaker(requireContext(), message)
            }
        }
    }

    private fun initiateLoginProcess(loginResponse: LoginResponse) {
        loadingDialog.isDismiss()
        /*     UtilsMethods.toastMaker(
                 requireContext(),
                 resources.getString(R.string.login_successful)
             )*/

        loginResponse.resObject!![0].name?.let {
//            SharedPrefs.write(UtilConstant.NAME, it.firstName.toString())
        }


        // Saving jwt token
        // in shared prefs
        loginResponse.resObject[0].jwt?.let {
            SharedPrefs.write(
                UtilConstant.JWT_TOKEN,
                it
            )
        }
        loginResponse.resObject[0].fwpEmail?.let {
            SharedPrefs.write(UtilConstant.FWP_EMAIL_LOGIN, it)
        }
        loginResponse.resObject[0].userName?.let {
            SharedPrefs.write(UtilConstant.USERNAME_LOGIN, it)
        }

        loginResponse.resObject[0].logoutPeriod.let {
            SharedPrefs.write(UtilConstant.LOGOUT_PERIOD, it.toString())
        }
        SharedPrefs.write(UtilConstant.LOGIN_DATE, UtilsMethods.getCurrentDateMMMddYY())
        if (UtilConstant.biometric) {
            SharedPrefs.write(UtilConstant.BIOMETRIC_ENABLE, "Pressed")
        }
        if (loginResponse.resObject[0].touAccepted == true) {
            // Navigate
            // to main screen
            // if tou accepted is true
            val i = Intent(activity, LandingScreenActivity::class.java)
            startActivity(i)
            requireActivity().finish()


        } else {
            // Navigate
            // to login screen
            //if tou accepted is false
            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.action_login2_to_loginAgreement
            )
        }
    }

/*    */
    /**
     *  create
     *  a textWatcher member
     *//*
    private val mTextWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
            emailValidator()
        }

        override fun afterTextChanged(editable: Editable) {
            buttonActivation()

            passValidator()
        }
    }*/

    private fun enablePrimaryBtn() {
        val logInButtonId = binding.login.btn
        logInButtonId.text = resources.getText(R.string.loginText)
        logInButtonId.isEnabled = true
        logInButtonId.isClickable = true
        binding.emailLayout.isErrorEnabled = false
        logInButtonId.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
        logInButtonId.setTextColor(resources.getColor(R.color.black, null))
    }

    private fun isFieldNotEmpty(): Boolean {

        if (binding.email.text.isNullOrEmpty()) {
            UtilsMethods.toastMaker(requireContext(), "Username or Email can not be null")
            return false
        }

        if (binding.pass.text.isNullOrEmpty()) {
            UtilsMethods.toastMaker(requireContext(), "Password can not be null")
            return false
        }
        return true
    }

    /* private fun disablePrimaryBtn() {
         val logInButtonId = binding.login.btn
         logInButtonId.isEnabled = false
         logInButtonId.isClickable = false
         logInButtonId.setTextColor(resources.getColor(R.color.white, null))
         logInButtonId.setBackgroundColor(
             resources.getColor(
                 R.color.disableButtonColor,
                 null
             )
         )
     }*/
}
