/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.common

import android.app.Activity
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.AlsNotLinkedFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.als.AlsLinkingActivity
import dagger.hilt.android.AndroidEntryPoint

// FWP
// Hilt entry point
@AndroidEntryPoint
class AlsNotLinked : Fragment() {

    companion object {
        fun newInstance() = AlsNotLinked()
    }

    // Global Variables
    private lateinit var viewModel: AlsNotLinkedViewModel
    private var _binding: AlsNotLinkedFragmentBinding? = null
    private val binding get() = _binding!!
    private var extras: Bundle? = null
    private var alsLinkedState = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        // Setting
        // view modal
        viewModel =
            ViewModelProvider(this)[AlsNotLinkedViewModel::class.java]

        _binding = AlsNotLinkedFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        alsLinkedState = extras?.getString("alsLinkedState").toString()

        // Init Process
        init()

        return root
    }

    private fun init() {

/*        if (alsLinkedState == resources.getString(R.string.titleETag)) {
            binding.textView9.text =
                "To access your E-tags, you need to connect your ALS customer number to your MyFWP account."
        } else {
            binding.textView9.text =
                "To access your items held, you need to connect your ALS customer number to your MyFWP account."
        }*/

        binding.textView9.text =
            "To access your E-tags, you need to connect your ALS customer number to your MyFWP account."

        // Setting
        // Primary Btn
        val buttonLinkAls = binding.helpGetStartButton.btn
        buttonLinkAls.text = resources.getString(R.string.linkALSText)
        buttonLinkAls.setTextColor(resources.getColor(R.color.black, null))
        buttonLinkAls.setBackgroundColor(
            resources.getColor(
                R.color.prim_yellow,
                null
            )
        )

        // Setting
        // click listener
        buttonHandler()
    }


    private fun buttonHandler() {

        // Intent to
        binding.helpGetStartButton.btn.setOnClickListener {

            val i = Intent(activity, AlsLinkingActivity::class.java)
            startActivity(i)
            (activity as Activity?)!!.overridePendingTransition(0, 0)
        }

    }

    // Remove binding
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
