/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fwp.deloittedctcustomerapp.data.api.ApiHelper
import com.fwp.deloittedctcustomerapp.data.model.entities.User
import com.fwp.deloittedctcustomerapp.data.model.requests.CheckEmailRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.CheckUsernameRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.SignupRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse
import com.fwp.deloittedctcustomerapp.data.repo.MainRepo
import com.fwp.deloittedctcustomerapp.utils.Event
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val mainRepo: MainRepo
) : ViewModel() {

    private val _newUserSignupResponse = MutableLiveData<CheckCredsResponse>()
    val newUserSignupResponse: LiveData<CheckCredsResponse>
        get() = _newUserSignupResponse

    private val _existingUserResponse = MutableLiveData<CheckCredsResponse>()
    val existingUserResponse: LiveData<CheckCredsResponse>
        get() = _existingUserResponse

    private val _existingEmailResponse = MutableLiveData<CheckCredsResponse>()
    val existingEmailResponse: LiveData<CheckCredsResponse>
        get() = _existingEmailResponse

    private val _toastExistingEmail = MutableLiveData<Event<String>>()
    val toastExistingEmail: LiveData<Event<String>>
        get() = _toastExistingEmail

    private val _toastNewUser = MutableLiveData<Event<String>>()
    val toastNewUser: LiveData<Event<String>>
        get() = _toastNewUser

    private val _toastExistingUser = MutableLiveData<Event<String>>()
    val toastExistingUser: LiveData<Event<String>>
        get() = _toastExistingUser

    fun checkExistingUsername(checkUsernameRequest: CheckUsernameRequest) {
        viewModelScope.launch {
            val response = mainRepo.checkExistingUsername(checkUsernameRequest)

            if (response.isSuccessful && response.body() != null) {
                when (response.body()!!.responseCode) {
                    UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> _existingUserResponse.value =
                        response.body()

                    UtilConstant.ERROR_CODE -> _toastExistingUser.value = Event(response.message())
                }

            } else {
                _toastExistingUser.value = Event(response.message())
            }
        }
    }

    fun checkExistingEmail(checkEmailRequest: CheckEmailRequest) {
        viewModelScope.launch {
            val response = mainRepo.checkExistingEmail(checkEmailRequest)

            if (response.isSuccessful && response.body() != null) {
                when (response.body()!!.responseCode) {
                    UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> _existingEmailResponse.value =
                        response.body()

                    UtilConstant.ERROR_CODE -> _toastExistingEmail.value = Event(response.message())
                }
            } else {
                _toastExistingEmail.value = Event(response.message())
            }

        }
    }

    fun sendNewUserData(signupRequest: SignupRequest) {
        viewModelScope.launch {
            val response = mainRepo.createNewUser(signupRequest)
            if (response.isSuccessful && response.body() != null) {
                when (response.body()!!.responseCode) {

                    UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> _newUserSignupResponse.value =
                        response.body()

                    UtilConstant.ERROR_CODE -> _toastNewUser.value = Event(response.message())
                }

            } else {
                _toastNewUser.value = Event(response.message())
            }

        }
    }
}
