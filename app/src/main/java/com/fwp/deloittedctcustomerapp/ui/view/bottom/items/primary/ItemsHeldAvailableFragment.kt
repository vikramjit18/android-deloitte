/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.view.*
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.compose.ui.text.android.InternalPlatformTextApi
import androidx.compose.ui.text.android.style.TypefaceSpan
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.*
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeld
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse
import com.fwp.deloittedctcustomerapp.databinding.FragmentItemsHeldAvailableBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView.TagLicenceActivity
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.adapter.ItemHeldPermitCurrentYearAdapter
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.adapter.ItemHeldPermitPreviousYearAdapter
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailActivity
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.*
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.CURRENT_INDEX
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.PREVIOUS_INDEX
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint

private const val TAG = "ItemsHeldAvailableFragm"

@AndroidEntryPoint
class ItemsHeldAvailableFragment : Fragment() {

    companion object {
        fun newInstance() = ItemsHeldAvailableFragment()
    }

    // Global Variables
    private var _binding: FragmentItemsHeldAvailableBinding? = null
    private val binding get() = _binding!!

    private lateinit var viewModel: LandingScreenActivityViewModel
    private lateinit var loadingDialog: LoadingDialog
    private val listCurrentYearPermitDetail = mutableListOf<ItemDetail>()
    private val listPreviousPermitYearDetail = mutableListOf<ItemDetail>()
    private var currentYear = UtilsMethods.getCurrentYear().toString()
    private var previousYear = UtilsMethods.getPreviousYear().toString()

    override fun onResume() {
        super.onResume()
        // Checking for multiple accouunt
        if (UtilConstant.multiAccount) {
            binding.itemHeldMain.background = ResourcesCompat.getDrawable(resources, R.drawable.bg_layout, null)
        }
//        if (UtilsMethods.isInternetConnected(requireContext())) {
//            loadingDialog.startLoading()
//            viewModel.getItemHeldResponse()
//            viewModel.getItemHeldResponse.observe(viewLifecycleOwner) {
//                loadingDialog.isDismiss()
//                when (it.responseCode) {
//                    UtilConstant.SUCCESS_CODE -> setupItemHeld(it)
//                }
//            }
//        } else {
//            val gson = Gson()
//            val json = SharedPrefs.read(UtilConstant.ITEM_HELD, "")
//            if (!json.isNullOrEmpty()) {
//                setupItemHeld(gson.fromJson(json, ItemsHeldResponse::class.java))
//            }
//        }
    }

    override fun onStart() {
        super.onStart()
        val gson = Gson()
        val json = SharedPrefs.read(UtilConstant.ITEM_HELD, "")
        if (!json.isNullOrEmpty()) {
            setupItemHeld(gson.fromJson(json, ItemsHeldResponse::class.java))
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentItemsHeldAvailableBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]
        loadingDialog = LoadingDialog(requireActivity())
        binding.currentYearPermitRecyclerView.setHasFixedSize(true)
        binding.previousYearPermitRecyclerView.setHasFixedSize(true)
        binding.more.visibility = View.GONE
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkLiveNetworkConnection()
        buttonHandler()
    }


    private fun clearList() {

        listCurrentYearPermitDetail.clear()
        listPreviousPermitYearDetail.clear()
    }

    private fun checkLiveNetworkConnection() {
        if (UtilsMethods.isInternetConnected(requireContext())) {
            binding.network.networkNotifyContainer.visibility = View.GONE
        } else {
            binding.network.networkNotifyContainer.visibility = View.VISIBLE
        }
        val connectivityManager =
            requireActivity().getSystemService(ConnectivityManager::class.java)
        val liveConnection = ConnectionLiveData(connectivityManager)
        liveConnection.observe(viewLifecycleOwner) { isConnected ->
            when (isConnected) {
                true -> binding.network.networkNotifyContainer.visibility = View.GONE

                false -> binding.network.networkNotifyContainer.visibility = View.VISIBLE
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupItemHeld(it: ItemsHeldResponse) {
        // Clear list
        clearList()

        currentYear = "${
            it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                CURRENT_INDEX
            )?.year
        }"

        previousYear = "${
            it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                PREVIOUS_INDEX
            )?.year
        }"

        // YEAR Heading
        binding.yearHeader.text =
            "$currentYear ${resources.getString(R.string.currentYearText)}"
        binding.previousYearHeading.text =
            "$previousYear ${resources.getString(R.string.previousYearText)}"



        binding.more.setOnClickListener { view ->
            it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.let { it1 ->
                showPopupMenu(
                    binding.root.context, binding.more,
                    it1
                )
            }
        }




        when (it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
            CURRENT_INDEX
        )?.nonCarcassTagLicenses?.alsNo == null && it.resObject?.get(CURRENT_INDEX)?.accounts?.get(
            CURRENT_INDEX
        )?.itemsHeld?.get(
            CURRENT_INDEX
        )?.permits?.permitData.isNullOrEmpty()) {
            true -> {
                binding.currentLicenseAvailable.itemContainer.visibility = View.GONE
                binding.noItemAvailable.noEtagPur.visibility = View.VISIBLE
                binding.currentYearPermitRecyclerView.visibility = View.GONE
                binding.buyAndApply.buyApplyCard.visibility = View.GONE
            }
            false -> {
                binding.noItemAvailable.noEtagPur.visibility = View.GONE
                binding.currentLicenseAvailable.itemContainer.visibility = View.VISIBLE
                binding.currentYearPermitRecyclerView.visibility = View.VISIBLE
                binding.buyAndApply.buyApplyCard.visibility = View.VISIBLE
            }
        }


        //for current license year
        when (it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
            CURRENT_INDEX
        )?.nonCarcassTagLicenses?.alsNo == null) {
            true -> {
                binding.currentLicenseAvailable.itemContainer.visibility = View.GONE
            }
            false -> {
                binding.currentLicenseAvailable.itemContainer.visibility = View.VISIBLE
                /*            listCurrentYearLicenseDetail.add(
                                ItemDetail(
                                    "${
                                        it.resObject?.get(CURRENT_INDEX)?.accounts?.get(
                                            CURRENT_INDEX
                                        )?.itemsHeld?.get(
                                            CURRENT_INDEX
                                        )?.year
                                    }",
                                    permitName = LICENSE_LISTING,
                                    accountType = UtilConstant.PRIMARY_ACCOUNT_KEY
                                )
                            )*/
                binding.currentLicenseAvailable.year.text = "${
                    it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                        CURRENT_INDEX
                    )?.year
                }"
                binding.currentLicenseAvailable.tvTagLicence.text = UtilConstant.LICENSE_LISTING
                binding.currentLicenseAvailable.itemContainer.setOnClickListener {
                    val i = Intent(context, TagLicenceActivity::class.java)
                    i.putExtra("account", UtilConstant.PRIMARY_ACCOUNT_KEY)
                    i.putExtra("ownerName", "")
                    requireActivity().startActivity(i)
                }
            }
        }

        //for current permit year
        if (it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                CURRENT_INDEX
            )?.permits != null
        ) {
            val permit =
                it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                    CURRENT_INDEX
                )?.permits!!.permitData

            when (permit != null) {
                true -> {
                    binding.currentYearPermitRecyclerView.visibility = View.VISIBLE
                    for (i in permit.indices) {
                        listCurrentYearPermitDetail.add(
                            ItemDetail(
                                "${
                                    it.resObject?.get(CURRENT_INDEX)?.accounts?.get(
                                        CURRENT_INDEX
                                    )?.itemsHeld?.get(
                                        CURRENT_INDEX
                                    )?.year
                                }",
                                permitName = permit[i].permit.toString(),
                                accountType = UtilConstant.PRIMARY_ACCOUNT_KEY
                            )
                        )
                    }
                }
                false -> {
                    binding.currentYearPermitRecyclerView.visibility = View.GONE
                }
            }
        }

        //for previous license year


        when (it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
            PREVIOUS_INDEX
        )?.nonCarcassTagLicenses?.alsNo == null && it.resObject?.get(CURRENT_INDEX)?.accounts?.get(
            CURRENT_INDEX
        )?.itemsHeld?.get(
            PREVIOUS_INDEX
        )?.permits?.permitData.isNullOrEmpty()) {
            true -> {
                binding.previousLicenseAvailable.itemContainer.visibility = View.GONE
                binding.noItemPrevYear.noEtagYear.visibility = View.VISIBLE
                binding.previousYearPermitRecyclerView.visibility = View.GONE
            }
            false -> {
                binding.noItemPrevYear.noEtagYear.visibility = View.GONE
                binding.previousLicenseAvailable.itemContainer.visibility = View.VISIBLE
                binding.previousYearPermitRecyclerView.visibility = View.VISIBLE

            }
        }


        when (it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
            PREVIOUS_INDEX
        )?.nonCarcassTagLicenses?.alsNo == null) {
            true -> {
                binding.previousLicenseAvailable.itemContainer.visibility = View.GONE
            }
            false -> {
                binding.previousLicenseAvailable.itemContainer.visibility = View.VISIBLE
                binding.previousLicenseAvailable.year.text = "${
                    it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                        PREVIOUS_INDEX
                    )?.year
                }"
                binding.previousLicenseAvailable.tvTagLicence.text = UtilConstant.LICENSE_LISTING
                binding.previousLicenseAvailable.itemContainer.setOnClickListener {
                    val i = Intent(context, TagLicenceActivity::class.java)
                    i.putExtra("account", UtilConstant.PRIMARY_ACCOUNT_KEY)
                    i.putExtra(UtilConstant.ACCOUNT_TYPE, UtilConstant.PREVIOUS_YEAR)
                    requireActivity().startActivity(i)
                }
            }
        }

        //for previous permit year
        if (it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                PREVIOUS_INDEX
            )?.permits != null
        ) {
            val permit =
                it.resObject?.get(CURRENT_INDEX)?.accounts?.get(CURRENT_INDEX)?.itemsHeld?.get(
                    PREVIOUS_INDEX
                )?.permits!!.permitData

            when (permit != null) {
                true -> {
                    binding.previousYearPermitRecyclerView.visibility = View.VISIBLE
                    for (i in permit.indices) {
                        listPreviousPermitYearDetail.add(
                            ItemDetail(
                                "${
                                    it.resObject?.get(CURRENT_INDEX)?.accounts?.get(
                                        CURRENT_INDEX
                                    )?.itemsHeld?.get(
                                        PREVIOUS_INDEX

                                    )?.year
                                }",
                                permitName = permit[i].permit.toString(),
                                accountType = UtilConstant.PRIMARY_ACCOUNT_KEY
                            )
                        )
                    }
                }
                false -> {
                    binding.previousYearPermitRecyclerView.visibility = View.GONE

                }
            }
        }



        binding.currentYearPermitRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity?.applicationContext)

            adapter = ItemHeldPermitCurrentYearAdapter(listCurrentYearPermitDetail)
        }

        binding.previousYearPermitRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity?.applicationContext)
            adapter = ItemHeldPermitPreviousYearAdapter(listPreviousPermitYearDetail)
        }

        if (UtilsMethods.isInternetConnected(requireContext()))
            loadingDialog.isDismiss()

        setSpannableString()
    }


    private fun buttonHandler() {
        binding.clToolTip.tvToottip.text = resources.getText(R.string.request_email_copy)

        binding.clToolTip.root.visibility = View.VISIBLE
        binding.downArrow.visibility = View.VISIBLE
        binding.more.visibility = View.VISIBLE
//         * not part of R1  Waiting for approval
        val dismissBanner = SharedPrefs.read(UtilConstant.DISMISSIBLE_BANNER_ITEM, "")
        if (dismissBanner!!.isEmpty()) {
            binding.clToolTip.root.visibility = View.VISIBLE
            binding.downArrow.visibility = View.VISIBLE
        } else {
            binding.clToolTip.root.visibility = View.GONE
            binding.downArrow.visibility = View.GONE
        }
        binding.clToolTip.close.setOnClickListener {
            SharedPrefs.write(UtilConstant.DISMISSIBLE_BANNER_ITEM, "Pressed")
            binding.clToolTip.root.visibility = View.GONE
            binding.downArrow.visibility = View.GONE
        }




        binding.network.closeNetNotify.setOnClickListener {
            binding.network.networkNotifyContainer.visibility = View.GONE
        }

        // Intent to buy
        binding.noItemAvailable.buyApplyButton.setOnClickListener {
            UtilsMethods.websiteOpener("https://fwp.mt.gov/buyandapply", requireActivity())
        }
        binding.buyAndApply.buyApplyButton.setOnClickListener {
            UtilsMethods.websiteOpener("https://fwp.mt.gov/buyandapply", requireActivity())
        }
        binding.buyAndApply.notSeeingYourPar.setOnClickListener {
            UtilsMethods.showDialerAlert(
                requireActivity(),
                requireContext(),
                "Not seeing your purchase?",
                "If you feel like an item you purchased is missing, call FWP support at 406-444-2950 during business hours (Monday – Friday, 8 a.m. – 5 p.m.)",
                "Close",
                resources.getString(R.string.callFwpText)
            )
        }
        binding.noItemAvailable.notSeeingYourPar.setOnClickListener {
            UtilsMethods.showDialerAlert(
                requireActivity(),
                requireContext(),
                "Not seeing your purchase?",
                "If you feel like an item you purchased is missing, call FWP support at 406-444-2950 during business hours (Monday – Friday, 8 a.m. – 5 p.m.)",
                "Close",
                resources.getString(R.string.callFwpText)
            )
        }
    }


    @OptIn(InternalPlatformTextApi::class)
    private fun setSpannableString() {
        val spannable =
            SpannableString(resources.getString(R.string.buy_string))

        val spanText =
            SpannableStringBuilder().append(resources.getString(R.string.noItemsPrevYearText1))
                .append(" $previousYear ")
                .append(resources.getString(R.string.noEtagsPrevYearText2))

        // Setting
        // font family
        val myTypeface =
            Typeface.create(
                ResourcesCompat.getFont(requireContext(), R.font.font_open_sens_bold),
                Typeface.BOLD
            )
        spannable.setSpan(
            TypefaceSpan(myTypeface),
            59,
            spannable.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )

        // Setting string
        // to textview
        binding.noItemAvailable.textView11.text = spannable
        binding.buyAndApply.textView11.text = spannable
        binding.noItemPrevYear.noEtagPurchasePre.text = spanText
        binding.noItemAvailable.noEtagPurchase.text =
            resources.getText(R.string.no_purchase_item)
    }

    //    * Waiting for approval R1
    private fun showPopupMenu(context: Context, view: View, itemsHeld: List<ItemsHeld>) {
        val wrapper: Context = ContextThemeWrapper(context, R.style.CustomPopUpStyle)
        val popup = PopupMenu(wrapper, view)
        popup.inflate(R.menu.request_copy_menu)
        popup.setOnMenuItemClickListener { item: MenuItem? ->
            when (item!!.itemId) {
                R.id.request_copy -> {
                    if (itemsHeld[0].nonCarcassTagLicenses?.alsNo == null && itemsHeld[0].permits?.permitData.isNullOrEmpty()) {
                        UtilsMethods.showSimpleAlert(
                            requireContext(),
                            "No items held",
                            "You do not currently hold any FWP Items for the ${itemsHeld[0].year} license year.",
                            "Ok"
                        )
                    } else {
                        val i = Intent(requireActivity(), RequestEmailActivity::class.java)
                        i.putExtra(UtilConstant.GET_ITEM_ACCOUNT, "1")
                        requireActivity().startActivity(i)
                        requireActivity().overridePendingTransition(
                            R.anim.slide_bottom_to_top,
                            R.anim.slide_top_to_bottom
                        )
                    }
                }
            }

            true
        }

        popup.show()
    }

    override fun onPause() {
        super.onPause()
        UtilConstant.ITEMS_LAST_VIEW_INDEX = 0
    }
}
