/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.privacy

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.TermPrivacyFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.privacy.term.TermFragment
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import com.google.android.material.bottomsheet.BottomSheetBehavior

class TermPrivacyFragment : Fragment() {

    companion object {
        fun newInstance() = TermPrivacyFragment()
    }

    private lateinit var viewModel: TermPrivacyViewModel
    private var _binding: TermPrivacyFragmentBinding? = null
    private val binding get() = _binding!!
    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = TermPrivacyFragmentBinding.inflate(inflater, container, false)
        val root = binding.root
        init()
        buttonHandler()

        return root

    }


    private fun init() {
        val termImg = binding.itemTerm.itemImage
        val termText = binding.itemTerm.itemText
        val privacyImg = binding.itemPrivacy.itemImage
        val privacyText = binding.itemPrivacy.itemText
        binding.itemTerm.itemEndImage.visibility = View.INVISIBLE
        termImg.setImageResource(R.drawable.ic_term)
        termText.setText(R.string.termOfService)
        privacyImg.setImageResource(R.drawable.ic_lock)
        privacyText.setText(R.string.privacyPolicy)
    }

    private fun buttonHandler() {
        binding.back.backLayout.setOnClickListener() {
            UtilsMethods.backStack(findNavController())
        }
        binding.itemTermL.setOnClickListener() {

            val termFragment: TermFragment?
            termFragment = TermFragment.newInstance()
            termFragment.show(childFragmentManager, termFragment.tag).apply {
                BottomSheetBehavior.PEEK_HEIGHT_AUTO

            }


        }

        binding.itemPrivacyL.setOnClickListener() {

            /*     val privacyFragment: PrivacyFragment?
                 privacyFragment = PrivacyFragment.newInstance()
                 privacyFragment!!.show(childFragmentManager, privacyFragment.tag).apply {
                     BottomSheetBehavior.PEEK_HEIGHT_AUTO
                 }*/

            UtilsMethods.websiteOpener("https://mt.gov/1240-X06.pdf", requireActivity())

        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
