/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.als.linking


import android.app.AlertDialog
import android.os.Bundle
import android.text.*
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.LinkingFragmentBinding
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.fwp.deloittedctcustomerapp.data.model.requests.AlsLinkingRequest
import com.fwp.deloittedctcustomerapp.ui.view.als.viewModel.AlsViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException
import android.widget.Toast
import com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse
import com.fwp.deloittedctcustomerapp.utils.*


private const val TAG = "LinkingFragment"

// FWP
// Hilt android point
@AndroidEntryPoint
class LinkingFragment : Fragment() {

    private var _binding: LinkingFragmentBinding? = null
    private val pattern = Regex(UtilConstant.DATE_PATTERN)
    private lateinit var alsViewModel: AlsViewModel
    private lateinit var loadingDialog: LoadingDialog
    private val binding get() = _binding!!
    private var alsFailedCount = 0
    private var firstTime = true

    /*  private var cal: Calendar = Calendar.getInstance()*/

    override fun onResume() {
        super.onResume()
        alsFailedCount = 0
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // data binding
        _binding = LinkingFragmentBinding.inflate(inflater, container, false)
        loadingDialog = LoadingDialog(requireActivity())
        alsViewModel = ViewModelProvider(this)[AlsViewModel::class.java]
        binding.etSmall.editText.hint ="###"
        // Obser
        if (firstTime)
            viewModelObsr()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initiate Process
        init()


    }

    private fun init() {

        // Setting
        // Close Icon
        binding.ivClose.backLayout.setImageDrawable(
            ResourcesCompat.getDrawable(
                resources,
                R.drawable.ic_close,
                null
            )
        )

        // Setting
        // Primary Button
        binding.linkAls.btn.text = resources.getString(R.string.linkAlsButtonText)
        disablePrimaryBtn()
        settingButtonProperty()
        // Setting
        // secondary
        // (Edittext)
        binding.etSmall.editText.filters += InputFilter.LengthFilter(3)
        binding.etSmall.editText.inputType = InputType.TYPE_CLASS_NUMBER
        binding.etNumber.inputType = InputType.TYPE_CLASS_NUMBER

        // Setting
        // Zip code
        // (Edittext)
        binding.etZipcode.editText.filters += InputFilter.LengthFilter(5)
        binding.etZipcode.editText.inputType = InputType.TYPE_CLASS_NUMBER

        // click listener
        buttonHandler()
    }

    private fun settingButtonProperty() {
        // Setting
        // Secondary Button
        binding.lookUpAls.btn.text = resources.getString(R.string.lookupAlsButtonText)
        binding.lookUpAls.btn.typeface =
            ResourcesCompat.getFont(requireContext(), R.font.font_mont_semi_bold)
        binding.lookUpAls.btn.setTextColor(resources.getColor(R.color.prim_yellow, null))
        binding.lookUpAls.btn.setBackgroundColor(resources.getColor(R.color.bgColor, null))
        binding.lookUpAls.btn.typeface =
            ResourcesCompat.getFont(requireContext(), R.font.font_mont_semi_bold)
        binding.lookUpAls.btn.stateListAnimator = null
        binding.lookUpAls.btn.elevation = 0F
    }


    private fun buttonHandler() {

        // text watcher
        // on various edittext
        DateInputMethod(binding.etNumber).listen()
        binding.etNumber.addTextChangedListener(mTextWatcher)
        binding.etSmall.editText.addTextChangedListener(mTextWatcher)
        binding.etZipcode.editText.addTextChangedListener(mTextWatcher)

        // Back to previous page
        binding.ivClose.backLayout.setOnClickListener {
            UtilsMethods.activityBack(requireActivity())
        }

        // intent to learn more frag
        binding.learnMore.setOnClickListener {
            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.linkingFragment_to_lookUp_know_more_Fragment
            )
        }

        // intent to lookup frag
        binding.lookUpAls.btn.setOnClickListener {
            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.linkingFragment_to_lookUpAlsFragment
            )

            binding.etNumber.editableText.clear()
            binding.etSmall.editText.setText("")
            binding.etZipcode.editText.setText("")
        }
    }


    // Validating ALS number
    fun alsValidate(): Boolean {
        if (binding.etNumber.text?.let { UtilConstant.ALS_PATTERN.containsMatchIn(it) } == true) {
            return true
        }

        return false
    }

    // Activation of primary button
    private fun buttonActivation() {
        val linkBtn = binding.linkAls.btn
        linkBtn.text = resources.getString(R.string.linkAlsButtonText)
        val dob: String = binding.etNumber.text.toString()

        when (pattern.containsMatchIn(dob) && binding.etSmall.editText.length() > 0 && binding.etZipcode.editText.length() == 5) {
            true -> {
                enablePrimaryBtn()
                linkBtn.setOnClickListener {
                    initAlsLinking()
                }
            }
            false -> disablePrimaryBtn()
        }
    }

    private fun initAlsLinking() {
        val isAlsValidate = alsValidate()
        if (UtilsMethods.isInternetConnected(requireContext())) {
            if (isAlsValidate)
                apiHits()
        } else
            UtilsMethods.showSimpleAlert(
                mContext = requireContext(),
                tvPrimary = getString(R.string.no_connection),
                tvSecondary = getString(R.string.check_and_try_again),
                primaryBtn = resources.getString(R.string.ok)
            )
    }

    private fun apiHits() {
        if (SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString().isNotEmpty()) {
            loadingDialog.startAlsSearchLoading()
            try {
                firstTime = true
                alsViewModel.alsLinking(
                    SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString(), AlsLinkingRequest(
                        alsDob = binding.etNumber.text.toString().trim(),
                        alsNo = binding.etSmall.editText.text.toString().toInt(),
                        alsZipCode = binding.etZipcode.editText.text.toString()
                    )
                )
            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (e2: SocketTimeoutException) {
                loadingDialog.isDismiss()
                e2.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }
//
        } else {
//            UtilsMethods.toastMaker(requireActivity(), "Jwt token not found")
        }
    }


    private fun viewModelObsr() {
        alsViewModel.alsLinkingResponse.observe(
            requireActivity()
        ) {
            loadingDialog.isDismiss()

            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> successObser()

                UtilConstant.FAIL_CODE -> failedObser(it)
            }
            firstTime = false
        }
    }

    private fun successObser() {
        UtilConstant.isAlsLinked = true
        UtilConstant.isAlsActivated = true
        clearFields()
        UtilsMethods.navigateToFragment(
            binding.root,
            R.id.action_linkingFragment_to_confirmAlsFragment
        )
        UtilConstant.snackbarShown = true

        alsFailedCount = 0

    }

    private fun failedObser(it: AlsResponse){
        alsFailedCount++
        if (alsFailedCount > 3) {
            UtilsMethods.showDialerAlsAlert(
                requireActivity(),
                requireContext(),
                getString(R.string.having_trouble_als),
                getString(R.string.als_finding_als_support),
                resources.getString(R.string.callFwpText),
                resources.getString(R.string.close)
            )
        } else {
//            UtilsMethods.toastMaker(
//                requireActivity(),
//                " ${it.resObject[0].message[0]}"
//            )
        }
    }

    // Text watcher on required fields
    private val mTextWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun afterTextChanged(editable: Editable) {
            buttonActivation()
            alsValidate()
        }
    }

    // Clear Field
    private fun clearFields() {
        binding.etNumber.text?.clear()
        binding.etSmall.editText.text?.clear()
        binding.etZipcode.editText.text?.clear()
    }

    // Disabling primary btn
    private fun disablePrimaryBtn() {
        binding.linkAls.btn.isClickable = false
        binding.linkAls.btn.setTextColor(resources.getColor(R.color.white, null))
        binding.linkAls.btn.setBackgroundColor(
            resources.getColor(
                R.color.disableButtonColor,
                null
            )
        )
    }

    // Enabling primary btn
    private fun enablePrimaryBtn() {
        binding.linkAls.btn.isClickable = true
        binding.linkAls.btn.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
        binding.linkAls.btn.setTextColor(resources.getColor(R.color.black, null))
    }
}

