/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.telephony.PhoneNumberUtils
import android.text.Html
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.compose.ui.text.android.InternalPlatformTextApi
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.responses.masterData.MasterData
import com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
import com.fwp.deloittedctcustomerapp.databinding.PrimaryAccountFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.EYE
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.HAIR
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.LBS
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

private const val TAG = "PrimaryAccountFragment"

@AndroidEntryPoint
class PrimaryAccountFragment : Fragment() {

    companion object {
        fun newInstance() = PrimaryAccountFragment()
    }

    // Global Variables
    private var _binding: PrimaryAccountFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var loadingDialog: LoadingDialog
    private lateinit var landingScreenActivityViewModel: LandingScreenActivityViewModel
    private val eyeItemsList = arrayListOf<String>()
    private val eyeCodeList = arrayListOf<String>()
    private val hairItemsList = arrayListOf<String>()
    private val hairCodeList = arrayListOf<String>()
    private val genderItemsList = arrayListOf<String>()
    private val genderCodeList = arrayListOf<String>()
    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right)
        }
    }

    override fun onResume() {
        super.onResume()

        loadingDialog = LoadingDialog(mActivity = requireActivity())

        landingScreenActivityViewModel =
            ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]

        loadingDialog.startLoading()

        // Check for internet
        if (UtilsMethods.isInternetConnected(requireContext())) {
            landingScreenActivityViewModel.getMasterData()
            landingScreenActivityViewModel.getUserProfile()
            viewModelObsr()
        } else {
            val gson = Gson()

            val masterJson = SharedPrefs.read(UtilConstant.MASTER_DATA, "")
            val userJson = SharedPrefs.read(UtilConstant.USER_PROFILE, "")

            if (!masterJson.isNullOrEmpty()) {
                val masterObj = gson.fromJson(masterJson, MasterData::class.java)
                maserDataList(masterObj)
            }
            if (!userJson.isNullOrEmpty()) {
                val obj = gson.fromJson(userJson, UserProfileResponse::class.java)
                setUpView(obj)
            }
        }
        if (UtilConstant.isProfileUpdate) {
            UtilsMethods.snackBarCustom(
                binding.root, "Profile updates saved",
                ContextCompat.getColor(binding.root.context, R.color.black),
                ContextCompat.getColor(binding.root.context, R.color.white)
            )
            UtilConstant.isProfileUpdate = false
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = PrimaryAccountFragmentBinding.inflate(inflater, container, false)
        binding.editContainer.visibility = View.VISIBLE
        return binding.root
    }


    private fun viewModelObsr() {

        landingScreenActivityViewModel.masterDataResponse.observe(viewLifecycleOwner) {
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    maserDataList(it)
                }
            }
        }

        landingScreenActivityViewModel.viewUserProfileResponse.observe(
            viewLifecycleOwner
        ) { userProfileResponse ->

            if (userProfileResponse.responseCode == UtilConstant.SUCCESS_CODE)
                setUpView(userProfileResponse)

        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonHandler()
    }

    @SuppressLint("SetTextI18n")
    private fun setUpView(profileResponse: UserProfileResponse) {

        // Name
        binding.priName.text =
            "${profileResponse.resObject!![0].p?.firstNm} ${profileResponse.resObject[0].p?.midInit} ${profileResponse.resObject[0].p?.lastNm} ${profileResponse.resObject[0].p?.suffix}".trim()

        // Als number
        binding.priALsNo.text =
            "ALS#: ${profileResponse.resObject[0].p?.dob} - ${profileResponse.resObject[0].p?.alsNo}"
        binding.resident.visibility = View.VISIBLE
        // resident status
        if (profileResponse.resObject[0].p?.residencyStatus == "R") {
            binding.resident.text = resources.getText(R.string.resident)
        } else {
            binding.resident.text = resources.getText(R.string.nonresident)
        }

        // User res  Address
        if (profileResponse.resObject[0].p?.residentialAddrLine!!.isNullOrEmpty()) {
            binding.residentialAddress.text = UtilConstant.NA
            binding.residentialAddress2.visibility = View.GONE
            binding.residentialAddressCountry.visibility = View.GONE
        } else {
            binding.residentialAddress2.visibility = View.VISIBLE
            binding.residentialAddressCountry.visibility = View.VISIBLE

            binding.residentialAddress.text = profileResponse.resObject[0].p?.residentialAddrLine
            binding.residentialAddress2.text =
                "${profileResponse.resObject[0].p?.residentialCity}, ${profileResponse.resObject[0].p?.residentialState} ${profileResponse.resObject[0].p?.residentialZipCd}"
            binding.residentialAddressCountry.text =
                profileResponse.resObject[0].p?.residentialCountry
        }

        // User mail  Address
        if (profileResponse.resObject[0].p?.mailingAddrLine!!.isNullOrEmpty()) {
            binding.mailAddress.text = UtilConstant.NA
            binding.mailAddress2.visibility = View.GONE
            binding.mailAddressCountry.visibility = View.GONE
        } else {
            binding.mailAddress2.visibility = View.VISIBLE
            binding.mailAddressCountry.visibility = View.VISIBLE

            binding.mailAddress.text = profileResponse.resObject[0].p?.mailingAddrLine
            binding.mailAddress2.text =
                "${profileResponse.resObject[0].p?.mailingCity}, ${profileResponse.resObject[0].p?.mailingState} ${profileResponse.resObject[0].p?.mailingZipCd}"
            binding.mailAddressCountry.text = profileResponse.resObject[0].p?.mailingCountry
        }

        // work number
        if (profileResponse.resObject[0].p?.workPhone!!.isNullOrEmpty()) {
            binding.contactNumber.text = UtilConstant.NA
        } else {
            binding.contactNumber.text = profileResponse.resObject[0].p?.workPhone?.let {
                getMaskedNumber(
                    it
                )
            }
        }

        // home number
        if (profileResponse.resObject[0].p?.homePhone!!.isNullOrEmpty()) {
            binding.homeNumber.text = UtilConstant.NA
        } else {
            binding.homeNumber.text = profileResponse.resObject[0].p?.homePhone?.let {
                getMaskedNumber(
                    it
                )
            }
        }

        // als email
        if (profileResponse.resObject[0].p?.alsEmail!!.isNullOrEmpty()) {
            binding.alsEmail.text = UtilConstant.NA
        } else {
            binding.alsEmail.text = profileResponse.resObject[0].p?.alsEmail
        }

        // fwp email
        if (profileResponse.resObject[0].p?.fwpEmailId!!.isNullOrEmpty()) {
            binding.myFwpEmail.text = UtilConstant.NA
        } else {
            binding.myFwpEmail.text = profileResponse.resObject[0].p?.fwpEmailId
        }

        // checking on email matches
        when (profileResponse.resObject[0].p?.alsEmail == profileResponse.resObject[0].p?.fwpEmailId) {
            true -> {
                binding.emailMatch.text = resources.getText(R.string.emailMatchingText)
                binding.learnMoreFwpEmail.visibility = View.GONE
            }
            false -> {
                binding.emailMatch.text = resources.getText(R.string.emailNotMatchingText)
                binding.learnMoreFwpEmail.visibility = View.VISIBLE

            }
        }


        //Gender
        binding.personGenderValue.text =
            profileResponse.resObject[0].p?.sexCd?.let { setupGender(it) }

        // Eye color
        binding.personEyeColorValue.text =
            profileResponse.resObject[0].p?.eyeColorCd?.let { setupEyeAndHairColor(it, EYE) }


        // Hair color
        binding.personHairColorValue.text = profileResponse.resObject[0].p?.hairColorCd?.let {
            setupEyeAndHairColor(it, HAIR)
        }

        // height color
        if (profileResponse.resObject[0].p?.eyeColorCd!!.isNullOrEmpty()) {
            binding.personHeightValue.text = UtilConstant.NA
        } else {
            binding.personHeightValue.text =
                "${profileResponse.resObject[0].p?.heightFeet}'${profileResponse.resObject[0].p?.heightInches}\""
        }

        // weight
        binding.personWeightValue.text = "${profileResponse.resObject[0].p?.weight} $LBS"
        loadingDialog.isDismiss()
    }

    private fun setupGender(code: String): String {
        var gender = ""
        for (i in genderItemsList.indices) {
            if (genderItemsList[i] == code) {
                gender = genderCodeList[i]
            }
        }
        return gender
    }

    private fun maserDataList(masterResponse: MasterData) {

        masterResponse.resObject[0].eYE.entries.forEach { color ->
            eyeItemsList.add(color.value)
            eyeCodeList.add(color.key.toString())
        }
        // setting up hair
        masterResponse.resObject[0].hAIR.entries.forEach { color ->
            hairItemsList.add(color.value)
            hairCodeList.add(color.key.toString())
        }

        masterResponse.resObject[0].sexCd.entries.forEach { color ->
            genderItemsList.add(color.key)
            genderCodeList.add(color.value)
        }

    }

    private fun setupEyeAndHairColor(position: String, of: String): String {
        var color = ""

        when (of) {
            EYE -> {
                for (i in eyeCodeList.indices) {
                    if (eyeCodeList[i] == position) {
                        color = eyeItemsList[i]
                    }
                }
            }

            HAIR -> {
                for (i in hairCodeList.indices) {
                    if (hairCodeList[i] == position) {
                        color = hairItemsList[i]
                    }
                }
            }
        }

        return color
    }

    @OptIn(InternalPlatformTextApi::class)
    private fun buttonHandler() {
        binding.back.backLayout.setOnClickListener {
            UtilsMethods.backStack(findNavController())
        }

        binding.editContainer.setOnClickListener {
            if (UtilsMethods.isInternetConnected(requireContext())) {
                UtilsMethods.navigateToFragment(
                    binding.root,
                    R.id.primaryAccountFragment_to_editPrimaryFragment
                )
            } else {
                UtilsMethods.showSimpleAlert(
                    mContext = requireContext(),
                    tvPrimary = "Your device is currently offline",
                    tvSecondary = "To make edits to your profile, you need a network connection. You can still validate any downloaded E-tags while offline.",
                    primaryBtn = resources.getString(R.string.ok)

                )
            }
        }

        binding.learnMoreFwpEmail.setOnClickListener {
            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.primaryAccountFragment_to_learnMoreAlsEmailFragment
            )
        }

        binding.ibFwpAlert.setOnClickListener {
            /*           val spannable =
                           SpannableString(resources.getString(R.string.alert_fwp_des))

                       val myTypeface =
                           Typeface.create(
                               ResourcesCompat.getFont(requireContext(), R.font.font_open_sens_bold),
                               Typeface.BOLD
                           )
                       spannable.setSpan(
                           TypefaceSpan(myTypeface),
                           76,
                           spannable.length,
                           Spannable.SPAN_INCLUSIVE_EXCLUSIVE
                       )*/


            showFragmentNavigationAlert(
                requireContext(),
                binding.root,
                R.id.action_primaryAccountFragment_to_changeLoginInfoFragment
            )
        }
    }

    private fun getMaskedNumber(number: String): String {
        return PhoneNumberUtils.formatNumber(number, Locale.getDefault().country)
    }

    private fun clearListDropdownList() {
        hairItemsList.clear()
        eyeItemsList.clear()
        genderCodeList.clear()
        genderItemsList.clear()
    }

    private fun showFragmentNavigationAlert(
        mContext: Context,
        mView: View,
        viewId: Int
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.update_my_email_modal_layout, null)
        val lBtn = view.findViewById<TextView>(R.id.btn_left)
        val rBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des1 = view.findViewById<TextView>(R.id.textView5)
        title.text = "Updating your MyFWP email"
        des1.text =
            "You can update your MyFWP account email under your Profile settings. Select Profile > Security > Login info or select Change email below."
        lBtn.text = "Ok"
        rBtn.text = "Update email"
        builder.setView(view)
        rBtn.setOnClickListener {
            builder.dismiss()
            UtilsMethods.navigateToFragment(mView, viewId)
        }
        lBtn.setOnClickListener {

            builder.dismiss()
        }

        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }
}
