/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.ReviewHarvestFragmentBinding
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import android.widget.Button
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.google.android.gms.location.*
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import java.util.*
import javax.net.ssl.SSLException

private const val TAG = "ReviewHarvestFragment"

// FWP
// Hilt entry point
@AndroidEntryPoint
class ReviewHarvestFragment : Fragment() {

    companion object {
        fun newInstance() = ReviewHarvestFragment()
    }

    // Global Variables
    private var _binding: ReviewHarvestFragmentBinding? = null
    private val binding get() = _binding!!
    private val validateTagActivityViewModel: ValidateTagActivityViewModel by viewModels()
    private lateinit var loadingDialog: LoadingDialog
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var harvestDate: String? = null
    private var confNo: String? = null
    private var spiId: String? = null
    private var speciceCode: String? = null
    private var eTagYearIndex: Any? = null
    private var accountOwner: Any? = null
    private var position: Any? = null
    private var owner: Any? = null
    private var opportunity: Any? = null
    private var description: Any? = null
    private var longitude: Double? = null
    private var latitude: Double? = null
    private val gson = Gson()

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult?) {
            longitude = result?.lastLocation?.longitude
            latitude = result?.lastLocation?.latitude

            super.onLocationResult(result)
        }
    }
//    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
//        return if (enter) {
//            AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
//        } else {
//            AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
//        }
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // data
        // binding
        _binding = ReviewHarvestFragmentBinding.inflate(inflater, container, false)
        // init loader
        loadingDialog = LoadingDialog(mActivity = requireActivity())

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        val root = binding.root
        // Checking location enable
        fetchLocation()



        try {
            if (UtilsMethods.isInternetConnected(requireContext())) {
                loadingDialog.startLoading()
                validateTagActivityViewModel.getEtag(EtagRequest(UtilConstant.DEVICE_ID))
                // Observing Response
            }

        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (s: SocketTimeoutException) {
            loadingDialog.isDismiss()
            s.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        }


        // Init
// process
        init()
        position = arguments?.getString(UtilConstant.GET_ETAG_INDEX_KEY)
        accountOwner = arguments?.getString(UtilConstant.GET_ETAG_ACCOUNT_OWNER)
        eTagYearIndex = arguments?.getString(UtilConstant.GET_ETAG_YEAR_KEY)

//genrating harvest conf no
        confNo = "${lastFourChar(accountOwner.toString())}${sixRandomNo()}"
        harvestDate = UtilsMethods.getCurrentDateTime()
// click
// listener
// handle
        buttonHandler()

//        onGPS()


        return root
    }


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (UtilsMethods.isInternetConnected(requireContext())) {
            // auto validating
//etag response observ
            validateTagActivityViewModel.etagResponse.observe(requireActivity(), {
                setupUI(it)
                loadingDialog.isDismiss()
            })
        } else {
            val json = SharedPrefs.read(UtilConstant.USER_ETAG, "")
          if(!json.isNullOrEmpty()) {
              setupUI(gson.fromJson(json, EtagResponse::class.java))
          }
        }


    }

    private fun setupUI(
        it: EtagResponse
    ) {
        when (it.responseCode) {
            UtilConstant.SUCCESS_CODE -> {
                if (!position.toString().isNullOrEmpty() && !accountOwner.toString()
                        .isNullOrEmpty() && !eTagYearIndex.toString().isNullOrEmpty()
                ) {
                    for (item in it.resObject?.get(0)?.etagAccountsList!!.indices) {
                        ownerMatching(it, item)
                    }
                }
            }

            UtilConstant.ERROR_CODE -> {
                it.resObject?.get(0)
                    ?.let { _ ->
                        //                            UtilsMethods.toastMaker(requireContext(), it1.toString())
                    }
            }
        }
    }

    private fun ownerMatching(
        it: EtagResponse,
        item: Int
    ) {
        when (it.resObject?.get(0)?.etagAccountsList!![item].owner?.contains(accountOwner.toString())) {
            true -> it.resObject?.get(0)?.etagAccountsList?.get(item)
                ?.let { etagAccounts ->
                    val etagList = etagAccounts.etags?.get(
                        eTagYearIndex.toString().toInt()
                    )?.etagLicensesList?.get(position.toString().toInt())!!
                    speciceCode =
                        etagList.etagLicenseDetails?.districtSpeciesCode
                    etagList.etagLicenseDetails?.etagsubDetails?.harvestTimeStamp

                    spiId = etagList.etagLicenseDetails?.spiId
                    owner = etagAccounts.owner
                    opportunity =
                        etagList.etagLicenseDetails?.etagsubDetails?.oppurtunity
                    description =
                        etagList.etagLicenseDetails?.etagsubDetails!!.description
                    val title = "${
                        etagAccounts.etags?.get(
                            eTagYearIndex.toString().toInt()
                        ).licenseYear
                    } ${etagList.licenseName}"
                    binding.title.text = title

                    if (UtilsMethods.getCurrentDateTime().isNotEmpty()) {

                        val dateTimeSplit = UtilsMethods.getCurrentDateTime().split(" ")
                        val alterTime: String =
                            if (dateTimeSplit!!.contains("PM")) {
                                "${dateTimeSplit!![0]} • ${dateTimeSplit!![2].substring(0, 5)} p.m."
                            } else {
                                "${dateTimeSplit!![0]} • ${dateTimeSplit!![2].substring(0, 5)} a.m."
                            }
                        binding.tvHarvestTime.text = alterTime
                    }
                    binding.tvOwner.text = etagAccounts.owner
                    binding.tvDes.text =
                        etagList.etagLicenseDetails?.etagsubDetails!!.description
                    binding.tvOp.text =
                        etagList.etagLicenseDetails?.etagsubDetails!!.oppurtunity
                }


            false -> {
                it.resObject?.get(0)
                    .let { _ ->
                    }
            }
        }
    }

    private fun lastFourChar(owner: String?): String {
        val lastNameIndex = owner!!.split(" ").lastIndex
        val lastName = owner!!.split(" ").get(lastNameIndex)
        return if (lastName.length <= 3) {
            "0$lastName"
        } else {
            lastName.substring(0, 4)
        }
    }

    private fun sixRandomNo(): String {
        val rnd = Random()
        val number: Int = rnd.nextInt(999999)
        return number.toString()
    }




    private fun onGPS() {
        if (!isLocationEnabled()) {
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            fetchLocation()
        } else {
            fetchLocation()
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager =
            requireActivity().applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.GPS_PROVIDER
        )
    }

    private fun fetchLocation() {
        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                requireActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                101
            )
            return
        } else {
            when (ActivityCompat.checkSelfPermission(
                requireActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION
            )) {
                PackageManager.PERMISSION_DENIED -> {
                    // not in use
                }
                PackageManager.PERMISSION_GRANTED -> {
                    requestLocation()
                }

            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestLocation() {
        val request = LocationRequest.create()
        request.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        fusedLocationProviderClient.requestLocationUpdates(
            request,
            locationCallback,
            Looper.myLooper()
        )
    }


    private fun init() {
        // changing
        // button
        // view
        buttonProperty()

        // button
        // handler
        val btn: Button = binding.validateButton.btn

        // Checkbox
        // handler
        val checkBox = binding.checkBox
        checkBox.setOnCheckedChangeListener { _, isChecked ->
            when (isChecked) {
                true -> {
                    btn.isEnabled = true
                    btn.isClickable = true
                    btn.setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
                    btn.setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.prim_yellow
                        )
                    )
                    navigteToSuccessPage()
                }
                false -> {
                    btn.isEnabled = false
                    btn.isClickable = false
                    btn.setTextColor(resources.getColor(R.color.white, null))
                    btn.setBackgroundColor(resources.getColor(R.color.disableButtonColor, null))
                }
            }
        }
    }

    private fun navigteToSuccessPage() {
        var conNo = confNo
        val request = ValidateEtagRequest(
            deviceId = UtilConstant.DEVICE_ID,
            // etaguploaddate
            etagUploadDate = UtilsMethods.getCurrentDateTime(),
            harvestConfNum = conNo,
            //current date
            harvestedDate = harvestDate,
            lat = latitude,
            longt = longitude,
            speciesCd = speciceCode,
            spiId = spiId,
        )
        binding.validateButton.btn.setOnClickListener {

            // Checking Internet connection
            if (UtilsMethods.isInternetConnected(requireContext())) {
                //Showing loader
                loadingDialog.startLoading()
                // Initiate User login Api
                try {
                    validateTagActivityViewModel.getEtagValidated(request)
//                    validateTagActivityViewModel.insertValidateTagData(request)
                    Log.e(TAG, "navigteToSuccessPage: $request")

                } catch (e: Exception) {
                    loadingDialog.isDismiss()
                    e.printStackTrace()
                } catch (e2: SocketTimeoutException) {
                    loadingDialog.isDismiss()
                    e2.printStackTrace()
                } catch (e3: SSLException) {
                    loadingDialog.isDismiss()
                    e3.printStackTrace()
                }
                //Observing user login response
                viewModelObser()
            } else {

                validateTagActivityViewModel.insertValidateTagData(request)

                val bundle = Bundle()
                bundle.putString(UtilConstant.ETAG_CON, confNo.toString())
                bundle.putString(UtilConstant.HARVEST_TIME, harvestDate.toString())
                bundle.putString(UtilConstant.ETAG_OWNER, owner.toString())
                bundle.putString(UtilConstant.ETAG_OP, opportunity.toString())
                bundle.putString(UtilConstant.ETAG_DES, description.toString())
                bundle.putString(
                    UtilConstant.GET_ETAG_ACCOUNT_OWNER,
                    accountOwner.toString()
                )
                bundle.putString(
                    UtilConstant.GET_ETAG_INDEX_KEY,
                    position.toString()
                )
                bundle.putString(
                    UtilConstant.GET_ETAG_YEAR_KEY,
                    eTagYearIndex.toString()
                )

//                    UtilsMethods.toastMaker(requireContext(), confirmationNumber.toString())
                UtilsMethods.navigateToFragmentWithValues(
                    binding.root,
                    R.id.validateSuccessFragment,
                    bundle
                )

                // Showing
                /*           // No internet dialog
                           UtilsMethods.showSimpleAlert(
                               mContext = requireContext(),
                               tvPrimary = getString(R.string.no_connection),
                               tvSecondary = getString(R.string.check_and_try_again),
                               primaryBtn = resources.getString(R.string.ok)

                           )*/
            }
        }
    }

    // btn
// click
// listener
// function
    private fun buttonHandler() {

        // back to
        // previous
        // fragment
        binding.back.setOnClickListener {
            UtilsMethods.backStack(findNavController())
        }

        // Custom
        // Dialog
        // Alert
        binding.ibAlertHarvestTime.setOnClickListener {
            UtilsMethods.showSimpleAlert(
                requireContext(),
                resources.getString(R.string.timestamp),
                resources.getString(R.string.harvest_alert_des),
                resources.getString(R.string.ok)

            )
        }
    }


    private fun viewModelObser() {
        validateTagActivityViewModel.validateEtagResponse.observe(
            viewLifecycleOwner,
            {
                when (it.responseCode) {
                    UtilConstant.SUCCESS_CODE -> {
                        val bundle = Bundle()
                        bundle.putString(UtilConstant.ETAG_CON, confNo.toString())
                        bundle.putString(
                            UtilConstant.HARVEST_TIME,
                            harvestDate.toString()
                        )
                        bundle.putString(UtilConstant.ETAG_OWNER, owner.toString())
                        bundle.putString(
                            UtilConstant.ETAG_OP,
                            opportunity.toString()
                        )
                        bundle.putString(
                            UtilConstant.ETAG_DES,
                            description.toString()
                        )
                        bundle.putString(
                            UtilConstant.GET_ETAG_ACCOUNT_OWNER,
                            accountOwner.toString()
                        )
                        bundle.putString(
                            UtilConstant.GET_ETAG_INDEX_KEY,
                            position.toString()
                        )
                        bundle.putString(
                            UtilConstant.GET_ETAG_YEAR_KEY,
                            eTagYearIndex.toString()
                        )


//                    UtilsMethods.toastMaker(requireContext(), confirmationNumber.toString())
                        UtilsMethods.navigateToFragmentWithValues(
                            binding.root,
                            R.id.validateSuccessFragment,
                            bundle
                        )
                    }

                }
                loadingDialog.isDismiss()
            })

        validateTagActivityViewModel.toast.observe(viewLifecycleOwner, {

            it.getContentIfNotHandledOrReturnNull()?.let { _ ->
                loadingDialog.isDismiss()
//                UtilsMethods.toastMaker(requireContext(), message)
            }

        })
    }



// Button
// property
// function
private fun buttonProperty() {
    binding.validateButton.btn.text =
        "Validate"
    binding.validateButton.btn.setTextColor(resources.getColor(R.color.white, null))
    binding.validateButton.btn.setBackgroundColor(
        resources.getColor(
            R.color.disableButtonColor,
            null
        )

    )
}


}

