/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.sponsors

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fwp.deloittedctcustomerapp.data.model.Sponsors
import com.fwp.deloittedctcustomerapp.databinding.SponsorRowBinding

class SponsorAdapter(private val list: List<Sponsors>) :
    RecyclerView.Adapter<SponsorAdapter.MyViewHolder>() {

    inner class MyViewHolder(val viewDataBinding: SponsorRowBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding =
            SponsorRowBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        /* val mContext = holder.itemView.resources*/

        holder.viewDataBinding.tvSponsor.text = list[position].text
    }

    override fun getItemCount(): Int {
        return list.size
    }
}
