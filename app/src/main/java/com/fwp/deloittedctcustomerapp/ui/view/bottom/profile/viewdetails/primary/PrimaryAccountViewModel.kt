/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fwp.deloittedctcustomerapp.data.model.requests.UpdateUserProfileRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse
import com.fwp.deloittedctcustomerapp.data.repo.MainRepo
import com.fwp.deloittedctcustomerapp.utils.Event
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import retrofit2.Response
import javax.inject.Inject


@HiltViewModel
class PrimaryAccountViewModel @Inject constructor(private val mainRepo: MainRepo) : ViewModel() {

    val countryResCode = MutableLiveData<String>()
    val countryMailCode = MutableLiveData<String>()
    private val _userResZipCodeResponse = MutableLiveData<ZipcodeResponse>()
    val userResZipCodeResponse: LiveData<ZipcodeResponse>
        get() = _userResZipCodeResponse
    private val _userMailZipCodeResponse = MutableLiveData<ZipcodeResponse>()
    val userMailZipCodeResponse: LiveData<ZipcodeResponse>
        get() = _userMailZipCodeResponse


    private val _editProfileResponse = MutableLiveData<UserProfileResponse>()
    val editProfileResponse: LiveData<UserProfileResponse>
        get() = _editProfileResponse

    private val _toast = MutableLiveData<Event<String>>()
    val toast: LiveData<Event<String>>
        get() = _toast

    private val _toastEditResponse = MutableLiveData<Event<String>>()
    val toastEditResponse: LiveData<Event<String>>
        get() = _toastEditResponse


    private val _viewUserProfileResponse = MutableLiveData<UserProfileResponse>()
    val viewUserProfileResponse: LiveData<UserProfileResponse>
        get() = _viewUserProfileResponse

    val coroutineExceptionHandler = CoroutineExceptionHandler { _, t -> t.printStackTrace() }


    fun getUserProfile() {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val token = SharedPrefs.read(UtilConstant.JWT_TOKEN, "")
            val response = token?.let { mainRepo.getViewUserProfileDetails(it) }
            withContext(Dispatchers.Main) {
                if (response!!.isSuccessful) {
                    when (response.body()!!.responseMessage) {
                        UtilConstant.SUCCESS_MSG -> {
                            getUserSuccessRes(response)
                        }
                    }
                }
            }
        }
    }

    private fun getUserSuccessRes(response: Response<UserProfileResponse>) {
        when (response.body()!!.responseCode) {
            UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> _viewUserProfileResponse.value =
                response.body()
        }
    }
    fun setResCountryCode(countryCodeInput: String) {
        countryResCode.value = countryCodeInput
    }
    fun setMailCountryCode(countryCodeInput: String) {
        countryMailCode.value = countryCodeInput
    }
    fun getResCityFromZipcode(zipcode: Int, isResident: String) {
        viewModelScope.launch {
            val response = mainRepo.getCityNameFromZipcode(zipcode, isResident)
            if (response.isSuccessful) {
                _userResZipCodeResponse.value = response.body()

                /*          when (response.body()!!.responseMessage) {
                              UtilConstant.SUCCESS_MSG -> {
                                  zipcodeRes(response)
                              }
                              else -> {
                                  _toast.value = Event(response.body()!!.resObject[0].message.toString())
                              }
                          }*/

            } else {
                _toast.value = Event(response.message())
            }
        }
    }

    fun getMailCityFromZipcode(zipcode: Int, isResident: String) {
        viewModelScope.launch {
            val response = mainRepo.getCityNameFromZipcode(zipcode, isResident)
            if (response.isSuccessful) {
                _userMailZipCodeResponse.value = response.body()

                /*          when (response.body()!!.responseMessage) {
                              UtilConstant.SUCCESS_MSG -> {
                                  zipcodeRes(response)
                              }
                              else -> {
                                  _toast.value = Event(response.body()!!.resObject[0].message.toString())
                              }
                          }*/

            } else {
                _toast.value = Event(response.message())
            }
        }
    }

   /* private fun zipcodeRes(response: Response<ZipcodeResponse>) {
        when (response.body()!!.responseCode) {
            UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> _userZipCodeResponse.value =
                response.body()

            UtilConstant.ERROR_CODE -> _toast.value = Event(response.message())
        }
    }
*/
    fun updateUserProfile(
        token: String,
        updateUserProfileRequest: UpdateUserProfileRequest
    ) {
        viewModelScope.launch {
            val response = mainRepo.updateUserProfile(token, updateUserProfileRequest)

            if (response.isSuccessful) {
                when (response.body()!!.responseMessage) {
                    UtilConstant.SUCCESS_MSG -> {
                        updateRes(response)
                    }

                    else -> _toastEditResponse.value = response.body()!!.resObject?.get(0)
                        ?.let {
                            Event(
                                it.toString()
                            )
                        }
                }


            } else {
                _toastEditResponse.value = Event(response.message())
            }
        }
    }

    private fun updateRes(response: Response<UserProfileResponse>) {
        when (response.body()!!.responseCode) {
            UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> _editProfileResponse.value =
                response.body()

            UtilConstant.ERROR_CODE -> _toastEditResponse.value =
                response.body()!!.resObject?.get(0)
                    ?.let {
                        Event(
                            it.toString()
                        )
                    }
        }
    }
}
