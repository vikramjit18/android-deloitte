/*
 * Montana Fish, Wildlife & Parks
 */

/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.adapter

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.ItemMultiAccountBinding
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods

class ProfileAdapter(private val list: List<Profile>) :
    RecyclerView.Adapter<ProfileAdapter.MyViewHolder>() {

    inner class MyViewHolder(val viewDataBinding: ItemMultiAccountBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding =
            ItemMultiAccountBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = list[position]

        holder.viewDataBinding.multiName.text = currentItem.name

        holder.viewDataBinding.multiALsNo.text = currentItem.alsNo


        // Navigate to next page
        holder.viewDataBinding.itemContainer.setOnClickListener {

            val bundle = Bundle()
            bundle.putString(UtilConstant.MULTI_NAME, currentItem.name)
            bundle.putString(UtilConstant.MULTI_ALS_NO, currentItem.alsNo)
            bundle.putString(UtilConstant.MULTI_Resident, currentItem.residentStatus)
            bundle.putString(UtilConstant.MULTI_LINKED, currentItem.linkedStatus)

            UtilsMethods.navigateToFragmentWithValues(
                holder.itemView,
                R.id.multipleAccountFragment,
                bundle
            )
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}
