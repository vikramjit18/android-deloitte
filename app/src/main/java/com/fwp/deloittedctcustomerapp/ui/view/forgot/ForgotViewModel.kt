/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.forgot

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fwp.deloittedctcustomerapp.data.model.requests.ForgotPasswordRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.ResetPasswordRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse
import com.fwp.deloittedctcustomerapp.data.repo.MainRepo
import com.fwp.deloittedctcustomerapp.utils.Event
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.broadcast
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ForgotViewModel @Inject constructor(private val mainRepo: MainRepo) : ViewModel() {

    private val _forgotPassResponse = MutableLiveData<ForgotResponse>()
    val forgotPassResponse: LiveData<ForgotResponse>
        get() = _forgotPassResponse

    private val _resetPassResponse = MutableLiveData<ForgotResponse>()
    val resetPassResponse: LiveData<ForgotResponse>
        get() = _resetPassResponse

    private val _toastForgotPass = MutableLiveData<Event<String>>()
    val toastForgotPass: LiveData<Event<String>>
        get() = _toastForgotPass

    private val _toastResetPass = MutableLiveData<Event<String>>()
    val toastResetPass: LiveData<Event<String>>
        get() = _toastResetPass

    val coroutineExceptionHandler = CoroutineExceptionHandler { _, t -> t.printStackTrace() }

    fun forgotPassRequest(forgotPasswordRequest: ForgotPasswordRequest) {
        viewModelScope.launch{
            val response = mainRepo.forgotPassword(forgotPasswordRequest)
                if (response.isSuccessful && response.body() != null) {
                    when (response.body()!!.responseMessage) {
                        UtilConstant.SUCCESS_MSG -> {
                            forgotPAssResponse(response)
                        }
                        else -> {
                            _toastForgotPass.value =
                                Event(response.body()!!.resObject[0].message[0])
                        }
                    }
                } else {
                    _toastForgotPass.value = Event(response.message())
                }

        }
    }

    private fun forgotPAssResponse(response: Response<ForgotResponse>) {
        when (response.body()!!.responseCode) {
            "s" -> _forgotPassResponse.value = response.body()
            "f" -> _forgotPassResponse.value = response.body()
            UtilConstant.ERROR_CODE -> _toastForgotPass.value =
                Event(response.body()!!.resObject[0].message[0])
        }
    }

    fun resetPassRequest(resetPasswordRequest: ResetPasswordRequest) {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.resetPassword(resetPasswordRequest)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful && response.body() != null) {
                    when (response.body()!!.responseCode) {
                        UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> _resetPassResponse.value =
                            response.body()
                        UtilConstant.ERROR_CODE -> _toastResetPass.value = Event(response.message())
                    }
                } else {
                    _toastResetPass.value = Event(response.errorBody().toString())
                }
            }
        }

    }

}
