/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.learnmore

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.text.Spanned

import android.graphics.Color
import android.os.Build

import android.text.style.BulletSpan

import android.text.SpannableString
import androidx.annotation.RequiresApi


class LearnMoreAlsEmailViewModel : ViewModel() {


    @RequiresApi(Build.VERSION_CODES.P)
    private val _content1Text = MutableLiveData<String>().apply {
        spannableContentSet("emergency closures and important opportunities (e.g. fires, disease outbreaks, natural disasters)").also { abc ->
            value = abc.toString()
        }
    }


    @RequiresApi(Build.VERSION_CODES.P)
    val content1Text: LiveData<String> = _content1Text


}

@RequiresApi(Build.VERSION_CODES.P)
fun spannableContentSet(content: String): SpannableString {
    val string = SpannableString(content)
    string.setSpan(
        BulletSpan(16, Color.BLUE, 20),
        0,
        content.length,
        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    return string
}
