/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags

import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.compose.ui.text.toUpperCase
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.add
import androidx.lifecycle.ViewModelProvider
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.UserStatusResponse
import com.fwp.deloittedctcustomerapp.databinding.FragmentEtagsBinding
import com.fwp.deloittedctcustomerapp.ui.view.als.AlsLinkingActivity
import com.fwp.deloittedctcustomerapp.ui.view.bottom.common.AlsNotLinked
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.MultiUserEtagAvailableFragment
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.EtagsAvailableFragment
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.*
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import java.lang.NullPointerException

// FWP
// Hilt entry point
@AndroidEntryPoint
class EtagsFragment : Fragment() {

    // Global variables
    private var _binding: FragmentEtagsBinding? = null
    private lateinit var landingScreenActivityViewModel: LandingScreenActivityViewModel
    private lateinit var loadingDialog: LoadingDialog
    private val binding get() = _binding!!
    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        // data binding
        _binding = FragmentEtagsBinding.inflate(inflater, container, false)
        // laoding
        loadingDialog = LoadingDialog(requireActivity())
        SharedPrefs.init(requireActivity())
        landingScreenActivityViewModel = ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]

        buttonHandler()

        return binding.root
    }

    private fun buttonHandler() {
        val buttonLinkAls = binding.etagsAlsNotLinked.helpGetStartButton.btn
        buttonLinkAls.text = resources.getString(R.string.linkALSText)
        buttonLinkAls.setTextColor(resources.getColor(R.color.black, null))
        buttonLinkAls.setBackgroundColor(
            resources.getColor(
                R.color.prim_yellow,
                null
            )
        )

        // Intent to
        buttonLinkAls.setOnClickListener {

            val i = Intent(activity, AlsLinkingActivity::class.java)
            startActivity(i)
            (activity as Activity?)!!.overridePendingTransition(0, 0)
        }
    }

    override fun onResume() {
        super.onResume()
        if (UtilConstant.snackbarShown) {
            UtilsMethods.snackBarCustom(
                binding.root,
                resources.getString(R.string.als_linked),
                ContextCompat.getColor(binding.root.context, R.color.black),
                ContextCompat.getColor(binding.root.context, R.color.white)
            )

            UtilConstant.snackbarShown = false
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (UtilsMethods.isInternetConnected(requireContext())) {
            startOnlineObservation()
        } else {
            startOfflineObservation()
        }
    }

    private fun startOnlineObservation() {
        loadingDialog.startLoading()
//        landingScreenActivityViewModel.getEtag(EtagRequest(UtilConstant.DEVICE_ID))
        viewModelObsr()
    }

    private fun startOfflineObservation() {
        val gson = Gson()
        val userJson = SharedPrefs.read(UtilConstant.USER_PROFILE, "")
        val checkUserJson = SharedPrefs.read(UtilConstant.CHECK_USER, "")
        try {
            if (!checkUserJson.isNullOrEmpty()) {
                val checkUserObj = gson.fromJson(checkUserJson, UserStatusResponse::class.java)
                if (checkUserObj.responseCode == UtilConstant.SUCCESS_CODE && checkUserObj.responseMessage == UtilConstant.SUCCESS_MSG) {
                    replaceEtagsFragment(
                        checkUserObj.resObject[0].alsLinked,
                        checkUserObj.resObject[0].alsActivated
                    )
                    replaceBgImgGreetText(isAlsLinked = checkUserObj.resObject[0].alsLinked)
                }
            }
        } catch (e: NumberFormatException) {
            throw e
        }catch (e1: NullPointerException) {
            throw e1
        }

        if (!userJson.isNullOrEmpty()) {
            val userObj = gson.fromJson(userJson, UserProfileResponse::class.java)
            if (userObj.resObject?.get(0)?.message == null) {
                landingScreenActivityViewModel.setMultiAccountBool(
                    userObj.resObject?.get(UtilConstant.PREVIOUS_INDEX)?.s?.isNotEmpty() == true
                )
                UtilConstant.multiAccount =
                    userObj.resObject?.get(UtilConstant.PREVIOUS_INDEX)?.s?.isNotEmpty() == true
            }  }

    }

    private fun viewModelObsr() {

        // User profile response
        landingScreenActivityViewModel.viewUserProfileResponse.observe(requireActivity()) {
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    UtilConstant.multiAccount =
                        it.resObject?.get(UtilConstant.PREVIOUS_INDEX)?.s?.isNotEmpty() == true
                    /*  name = it.resObject?.get(0)?.p?.firstNm.toString()*/
                    SharedPrefs.write("name", it.resObject?.get(0)?.p?.firstNm.toString())
                }
            }
        }

        // User status response
        landingScreenActivityViewModel.checkUserStatusResponse.observe(
            requireActivity()
        ) {
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    replaceEtagsFragment(
                        isAlsLinked = it.resObject[0].alsLinked,
                        alsActivated = it.resObject[0].alsActivated
                    )
                    replaceBgImgGreetText(isAlsLinked = it.resObject[0].alsLinked)
                }

                UtilConstant.FAIL_CODE -> {
                    it.resObject[0].let { it1 ->
//                        UtilsMethods.toastMaker(
//                            requireContext(),
//                            it1.toString()
//                        )
                    }
                }
            }
        }

        landingScreenActivityViewModel.etagResponse.observe(viewLifecycleOwner) {
            loadingDialog.isDismiss()
            val gson = Gson()
            val eJson = gson.toJson(it)
            SharedPrefs.write(UtilConstant.USER_ETAG, eJson)
//            when (it.responseCode) {
//                UtilConstant.SUCCESS_CODE -> {
//                    val gson = Gson()
//                    val eJson = gson.toJson(it)
//                    SharedPrefs.write(UtilConstant.USER_ETAG, eJson)
//                }
//            }
        }

        if (UtilsMethods.isInternetConnected(requireContext()))
            loadingDialog.isDismiss()
    }

    // Replace function start
    private fun replaceEtagsFragment(isAlsLinked: Boolean, alsActivated: Boolean) {

        when (isAlsLinked && alsActivated) {
            false -> {
                /*val bundle = Bundle()
                bundle.putString("alsLinkedState", resources.getString(R.string.titleETag))
                UtilsMethods.navigateToFragmentWithValues(binding.root, R.id.emptyState, bundle)*/
                binding.etagsAlsNotLinked.clAlsNotLinked.visibility = View.VISIBLE
                binding.etagsFragmentContainer.visibility = View.GONE
            }

            true -> {
                binding.etagsAlsNotLinked.clAlsNotLinked.visibility = View.GONE
                binding.etagsFragmentContainer.visibility = View.VISIBLE
                showEtagAccounts()
            }
        }
    }

    // Showing multiple account function
    private fun showEtagAccounts() {
        landingScreenActivityViewModel.isMultiAccount.observe(requireActivity()) {
            when (it) {
                true -> {
                    // multiple account header
                    showAccountHeader(UtilConstant.MULTI_ACCOUNT)

                    // Change to multiple user
                    navigateToFrag(MultiUserEtagAvailableFragment.newInstance())
                }
                false -> {
                    navigateToFrag(EtagsAvailableFragment.newInstance())
                }
            }
        }

/*        when (UtilConstant.multiAccount) {
            true -> {
                // multiple account header
                showAccountHeader(UtilConstant.MULTI_ACCOUNT)

                // Change to multiple user
                navigateToFrag(MultiUserEtagAvailableFragment.newInstance())
            }
            false -> {
                navigateToFrag(EtagsAvailableFragment.newInstance())
            }
        }*/
    }

    // Replace background image function
    private fun replaceBgImgGreetText(isAlsLinked: Boolean) {
        when (isAlsLinked) {
            false -> showAccountHeader(UtilConstant.ALS_NOT_LINKED)

            true -> {
                // als liked account header
                showAccountHeader(UtilConstant.ALS_LINKED)
            }
        }
    }

    // showing account header function
    private fun showAccountHeader(state: String) {
        when (state) {
            //            UtilConstant.ALS_LINKED -> showAlsLinkedHeader()

            UtilConstant.MULTI_ACCOUNT -> showMultiAccountHeader()

            //            else -> binding.textHome.text = "Hello"
            else -> showAlsLinkedHeader()

        }
    }

    // Custom Als Linked header string
    private fun showAlsLinkedHeader() {
        landingScreenActivityViewModel.ownerName.observe(requireActivity()) {
            binding.textHome.textSize = 34F
            val name = it.substring(0, 1).toUpperCase() + it.substring(1, it.length).toLowerCase()
            val primaryOwnerName = SharedPrefs.read(UtilConstant.NAME, "User")
            val spannable = SpannableString("Hello $name")
            spannable.setSpan(
                ForegroundColorSpan(resources.getColor(R.color.prim_yellow, null)),
                6,
                6 + name.length,
                Spannable.SPAN_INCLUSIVE_EXCLUSIVE,
            )
            when (primaryOwnerName.toString().lowercase() == name.lowercase()) {
                true -> binding.textHome.text = spannable
                false -> binding.textHome.text = "${name}\'s E-tags"
            }

        }
    }

    // Custom multi header string
    private fun showMultiAccountHeader() {
        val name = "back"
        val spannable = SpannableString("Welcome $name")
        spannable.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.prim_yellow, null)),
            8,
            8 + name.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE,
        )

        // setting custom string to TV
        binding.textHome.text = spannable
    }

    // Common navigate function to destination
    private fun navigateToFrag(fragment: Fragment) {
        childFragmentManager
            .beginTransaction()
            .replace(
                R.id.etags_fragment_container,
                fragment
            )
            .commitNowAllowingStateLoss()
    }
}

