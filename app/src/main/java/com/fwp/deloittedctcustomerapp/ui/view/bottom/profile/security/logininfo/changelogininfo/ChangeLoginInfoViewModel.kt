/*
 * Montana Fish, Wildlife & Parks
 */

/**
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
>>>>>>> ab6320130e1bd2e0472e3fb7fb065ba49599d866
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fwp.deloittedctcustomerapp.data.db.AppDao
import com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
import com.fwp.deloittedctcustomerapp.data.model.requests.ChangeFwpEmailRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.changeFwpEmail.ChangeFwpEmailResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse
import com.fwp.deloittedctcustomerapp.data.repo.MainRepo
import com.fwp.deloittedctcustomerapp.utils.Event
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ChangeLoginInfoViewModel @Inject constructor(private val mainRepo: MainRepo,private val appDao: AppDao) :
    ViewModel() {

    private val _changeFwpEmailResponse = MutableLiveData<ChangeFwpEmailResponse>()
    val changeFwpEmailResponse: LiveData<ChangeFwpEmailResponse>
        get() = _changeFwpEmailResponse

    private val _toast = MutableLiveData<Event<String>>()
    val toast: LiveData<Event<String>>
        get() = _toast

    private val _offloadResponse = MutableLiveData<DownloadOffloadResponse>()
    val offloadResponse: LiveData<DownloadOffloadResponse>
        get() = _offloadResponse

    fun getDownloadedTagSid(): LiveData<List<DownloadedTags>> {
        return appDao.getDownloadedTags()
    }

    fun deleteDownloadedTagSid() {
        appDao.deleteDownloadedTagsTable()
    }
    fun cleardb() {
        viewModelScope.launch{
            appDao.deleteValidateRequest()
        }

    }
    val coroutineExceptionHandler = CoroutineExceptionHandler{_, t -> t.printStackTrace() }
    fun changeFwpEmail(jwtToken: String, changeFwpEmailRequest: ChangeFwpEmailRequest) {
        viewModelScope.launch {
            val response = mainRepo.changeFwpEmail(jwtToken, changeFwpEmailRequest)

            if (response.isSuccessful && response.body() != null) {
                when (response.body()!!.responseMessage) {
                    UtilConstant.SUCCESS_MSG -> {
                        changeFwpEmailResponse(response)
                    }
                    else -> _toast.value =
                        response.body()!!.resObject?.get(0)?.let { Event(it.toString()) }
                }
            } else {
                _toast.value = Event(response.message())
            }

        }
    }

    private fun changeFwpEmailResponse(response: Response<ChangeFwpEmailResponse>) {
        when (response.body()!!.responseCode) {
            UtilConstant.SUCCESS_CODE -> {
                _changeFwpEmailResponse.value = response.body()
            }
            else -> _changeFwpEmailResponse.value = response.body()
        }
    }

    @DelicateCoroutinesApi
    fun offLoadRequest(downloadOffloadRequest: DownloadOffloadRequest) {
        GlobalScope.launch (Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.downloadOffLoadEtag(downloadOffloadRequest)

            withContext(Dispatchers.Main)   {
                if (response.isSuccessful) {
                    when (response.body()!!.responseCode) {

                        UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE
                        -> _offloadResponse.value = response.body()

//                        UtilConstant.ERROR_CODE -> _toastViewUser.value =
//                            response.body()!!.resObject?.get(0)?.let { Event(it.toString())
//                            }
                    }

                } else {
//                    _toastDownloadOffload.value = Event(response.message())
                }
            }
        }
    }
}
