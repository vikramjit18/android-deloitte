/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicensesX
import com.fwp.deloittedctcustomerapp.databinding.LicenseItemBinding

class LicenseAdapter(
    private val list: List<NonCarcassTagLicensesX>,
) : RecyclerView.Adapter<LicenseAdapter.MyViewHolder>() {

    inner class MyViewHolder(val viewDataBinding: LicenseItemBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding =
            LicenseItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = list[position]
        holder.viewDataBinding.tvLicenseName.text = currentItem.licenseName
        holder.viewDataBinding.tvLicenseDate.text = currentItem.issuedDate
    }

    override fun getItemCount(): Int {
        return list.size
    }
}
