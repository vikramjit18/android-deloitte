/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.UserStatusResponse
import com.fwp.deloittedctcustomerapp.databinding.FragmentProfileBinding
import com.fwp.deloittedctcustomerapp.ui.view.als.AlsLinkingActivity
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.adapter.Profile
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.adapter.ProfileAdapter
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginActivity
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException

//private const val TAG = "ProfileFragment"

@AndroidEntryPoint
class ProfileFragment : Fragment() {

    private lateinit var profileViewModel: ProfileViewModel
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    private lateinit var landingScreenActivityViewModel: LandingScreenActivityViewModel
    private val list = mutableListOf<Profile>()
    private val gson = Gson()
    private lateinit var loadingDialog: LoadingDialog
    private val listOffLoad = mutableListOf<String>()

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
        }
    }

    override fun onResume() {
        super.onResume()
        if (UtilConstant.snackbarShown) {
            UtilsMethods.snackBarCustom(
                binding.root,
                resources.getString(R.string.als_linked),
                ContextCompat.getColor(binding.root.context, R.color.black),
                ContextCompat.getColor(binding.root.context, R.color.white)
            )
            UtilConstant.snackbarShown = false
        }
        if (UtilsMethods.isInternetConnected(requireContext())){
            landingScreenActivityViewModel.viewUserProfileResponse.observe(viewLifecycleOwner) {
                val json = gson.toJson(it)
                SharedPrefs.write(UtilConstant.USER_PROFILE, json)
            }
        }

        val userJson = SharedPrefs.read(UtilConstant.CHECK_USER, "")
        if (!userJson.isNullOrEmpty()) {
            val userObj = gson.fromJson(userJson, UserStatusResponse::class.java)
            if (userObj.resObject[0].alsLinked && userObj.resObject[0].alsActivated) {
                hideAlsView()
            } else {
                showAlsView()
            }

            val json = SharedPrefs.read(UtilConstant.USER_PROFILE, "")
            if (!json.isNullOrEmpty()) {
                val obj = gson.fromJson(json, UserProfileResponse::class.java)
                if (obj.resObject?.get(0)?.message == null) {
                    setupUI(it = obj)
                }
            }
//            // Internet Check
//            if (UtilsMethods.isInternetConnected(requireContext())) {
//                if (userObj.resObject[0].alsLinked && userObj.resObject[0].alsActivated) {
////                    landingScreenActivityViewModel.getUserProfile()
//                    viewModelObser()
//                }
//            } else {
//                val json = SharedPrefs.read(UtilConstant.USER_PROFILE, "")
//                if (!json.isNullOrEmpty()) {
//                    val obj = gson.fromJson(json, UserProfileResponse::class.java)
//                    if (obj.resObject?.get(0)?.message == null) {
//                        setupUI(it = obj)
//                    }
//                }
//            }
        }


    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        profileViewModel = ViewModelProvider(requireActivity())[ProfileViewModel::class.java]
        landingScreenActivityViewModel = ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]
        _binding = FragmentProfileBinding.inflate(inflater, container, false)

        val root: View = binding.root
        SharedPrefs.init(requireActivity())
        loadingDialog = LoadingDialog(mActivity = requireActivity())
        // Init Process
        init()

//        isAlsLinked()

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listOffLoad.clear()
        landingScreenActivityViewModel.getDownloadedTagSid().observe(requireActivity()) {
            it.forEach { data ->
                data.downloadedSpiId?.let { it1 -> listOffLoad.add(it1) }
            }
        }
    }

//    @SuppressLint("SetTextI18n")
//    private fun viewModelObser() {
//        landingScreenActivityViewModel.viewUserProfileResponse.observe(viewLifecycleOwner) {
//            when (it.responseCode) {
//                UtilConstant.SUCCESS_CODE -> {
//                    // clear List
//                    binding.primaryProfileCard.visibility = View.VISIBLE
//                    setupUI(it)
//                }
//            }
//
//        }
//    }

    @SuppressLint("SetTextI18n")
    private fun setupUI(it: UserProfileResponse) {
        binding.primaryProfileCardIn.visibility = View.VISIBLE
        // Primary User Binding
        binding.priName.text =
            "${it.resObject!![UtilConstant.CURRENT_INDEX].p?.firstNm} ${it.resObject[UtilConstant.CURRENT_INDEX].p?.midInit} ${it.resObject[UtilConstant.CURRENT_INDEX].p?.lastNm} ${it.resObject[UtilConstant.CURRENT_INDEX].p?.suffix}"
        binding.priALsNo.text =
            "ALS#: ${it.resObject[UtilConstant.CURRENT_INDEX].p?.dob} - ${it.resObject[UtilConstant.CURRENT_INDEX].p?.alsNo}"
        UtilConstant.userName = it.resObject[UtilConstant.CURRENT_INDEX].p?.userName
        // Linked User

        if (UtilConstant.multiAccount) {
            list.clear()
            binding.rlMultiAccounts.visibility = View.VISIBLE
            for (i in it.resObject[UtilConstant.PREVIOUS_INDEX].s?.indices!!) {
                // Adding items in list
                list.add(
                    Profile(
                        name = "${it.resObject[UtilConstant.PREVIOUS_INDEX].s?.get(i)?.firstNm} ${
                            it.resObject[UtilConstant.PREVIOUS_INDEX].s?.get(
                                i
                            )?.midInit
                        } ${it.resObject[UtilConstant.PREVIOUS_INDEX].s?.get(i)?.lastNm} ${
                            it.resObject[UtilConstant.PREVIOUS_INDEX].s?.get(
                                i
                            )?.suffix
                        }",
                        alsNo = "ALS#: ${
                            it.resObject[UtilConstant.PREVIOUS_INDEX].s?.get(
                                i
                            )?.dob
                        } - ${
                            it.resObject[UtilConstant.PREVIOUS_INDEX].s?.get(
                                i
                            )?.alsNo
                        }",
                        residentStatus = "${
                            it.resObject[UtilConstant.PREVIOUS_INDEX].s?.get(
                                i
                            )?.residencyStatus
                        }",
                        linkedStatus = "Linked minor"
                    )
                )
            }
            recyclerContent(list)
        }

//        if (UtilsMethods.isInternetConnected(requireContext()))
//            loadingDialog.isDismiss()
    }

    private fun recyclerContent(list: List<Profile>) {
        binding.multiRecycler.apply {
            layoutManager = LinearLayoutManager(activity?.applicationContext)
            adapter = ProfileAdapter(list)
        }
    }


    private fun init() {

        binding.version.text = profileViewModel.version.value
        val btnStart = binding.helpGetStartButton.btn
        val notifyImg = binding.itemNotify.itemImage
        val notifyText = binding.itemNotify.itemText
        val verifyImg = binding.itemVerify.itemImage
        val verifyText = binding.itemVerify.itemText
        val helpImg = binding.itemHelp.itemImage
        val helpText = binding.itemHelp.itemText
        val legalImg = binding.itemLegal.itemImage
        val legalText = binding.itemLegal.itemText
        binding.itemNotify.itemEndImage.visibility = View.INVISIBLE
        binding.itemVerify.itemEndImage.visibility = View.INVISIBLE
        binding.itemHelp.itemEndImage.visibility = View.INVISIBLE
        binding.itemLegal.itemEndImage.visibility = View.INVISIBLE
        btnStart.text = resources.getString(R.string.linkALSText)
        btnStart.setTextColor(resources.getColor(R.color.black, null))
        btnStart.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
        notifyImg.setImageResource(R.drawable.ic_bell)
        notifyText.setText(R.string.notificationText)
        verifyImg.setImageResource(R.drawable.ic_verified)
        verifyText.setText(R.string.securityText)
        helpImg.setImageResource(R.drawable.ic_help)
        helpText.setText(R.string.helpText)
        legalImg.setImageResource(R.drawable.ic_legal)
        legalText.text = getString(R.string.legal)
        buttonHandler()
    }


    fun buttonHandler() {
        binding.itemNotifyL.setOnClickListener {
            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.navigation_profile_to_notificationFragment
            )
        }

        binding.primaryProfileCardIn.setOnClickListener {
            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.navigation_profile_to_primaryAccountFragment
            )
        }


        binding.itemVerifyL.setOnClickListener {
            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.navigation_profile_to_securityFragment
            )
        }
        binding.helpGetStartButton.btn.setOnClickListener {

            val i = Intent(activity, AlsLinkingActivity::class.java)
            startActivity(i)
            (activity as Activity?)!!.overridePendingTransition(0, 0)
        }

        binding.itemHelpL.setOnClickListener {
            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.navigation_profile_to_getHelpFragment
            )
        }

        binding.itemLegalL.setOnClickListener {
            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.navigation_profile_to_termPrivacyFragment
            )
        }

        binding.logoutButton.setOnClickListener {

            if (UtilsMethods.isInternetConnected(requireContext())) {

                logoutDialog()
            } else {
                UtilsMethods.showNoNetwokLogoutDialog(requireContext())
            }

        }
    }

    private fun logoutDialog() {
        val builder = AlertDialog.Builder(requireContext()).create()
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.custom_dialog, null)
        val lBtn = view.findViewById<TextView>(R.id.btn_left)
        val rBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)

        title.text = getString(R.string.logout_title)
        des.text = getString(R.string.logout_subtitle)

        rBtn.text = resources.getString(R.string.logOutText).uppercase()
        lBtn.text = resources.getString(R.string.cancelText).uppercase()
        builder.setView(view)

        rBtn.setOnClickListener {

//            UtilsMethods.toastMaker(requireContext(), resources.getString(R.string.logout))
            if (UtilsMethods.isInternetConnected(requireContext())) {

                Log.e(TAG, "logoutDialog: ofline logout spiid  $it $listOffLoad")
                if (listOffLoad.isNotEmpty()) {
                    loadingDialog.startLoading()
                    val offloadRequest = DownloadOffloadRequest(
                        deviceUid = UtilConstant.DEVICE_ID,
                        service = UtilConstant.OFFLOAD,
                        spiId = listOffLoad.toSet().toList()
                    )
                    apioffloadTag(offloadRequest)

                } else {

                    SharedPrefs.clearSharedPref()
                    landingScreenActivityViewModel.cleardb()
                    UtilConstant.itemLastIndex = 0
                    UtilConstant.etagLastIndex = 0
                    UtilConstant.ITEMS_LAST_VIEW_INDEX = 0
                    UtilConstant.ACCOUNT_VIEWPAGER = 0
                    UtilConstant.alsExpiredDialog = true
                    UtilConstant.multiAccount = false
//                        landingScreenActivityViewModel.deleteDownloadedTagSid()
                    val i = Intent(activity, LoginActivity::class.java)
                    requireActivity().startActivity(i)
                    requireActivity().finish()
                }


//                if (count == 0) {
//                    offloadObsr()
//                    count += 1
//                }
                offloadObsr()
                builder.dismiss()
            } else {
                UtilsMethods.showNoNetwokDialog(requireContext())
            }

        }

        lBtn.setOnClickListener {
            builder.dismiss()
        }

        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder?.window?.setBackgroundDrawableResource(android.R.color.background_dark)
        builder.show()
    }

    private fun apioffloadTag(offloadRequest: DownloadOffloadRequest) {
        try {
            profileViewModel.offLoadRequest(offloadRequest)

        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (e2: SocketTimeoutException) {
            loadingDialog.isDismiss()
            e2.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        }
    }

    private fun hideAlsView() {
        binding.version.visibility = View.VISIBLE
        binding.helpCard.visibility = View.GONE
        binding.primaryProfileCard.visibility = View.VISIBLE
    }

    private fun showAlsView() {
        binding.version.visibility = View.INVISIBLE
        binding.helpCard.visibility = View.VISIBLE
        binding.primaryProfileCard.visibility = View.GONE
    }

    private fun offloadObsr() {
        profileViewModel.offloadResponse.observe(requireActivity()) {
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    loadingDialog.isDismiss()
                    UtilConstant.itemLastIndex = 0
                    UtilConstant.etagLastIndex = 0
                    UtilConstant.ITEMS_LAST_VIEW_INDEX = 0
                    UtilConstant.ACCOUNT_VIEWPAGER = 0
                    UtilConstant.multiAccount = false
                    SharedPrefs.clearSharedPref()
                    landingScreenActivityViewModel.cleardb()
                    landingScreenActivityViewModel.deleteDownloadedTagSid()
                    val i = Intent(activity, LoginActivity::class.java)
                    requireActivity().startActivity(i)
                    requireActivity().finish()

                }
                UtilConstant.FAIL_CODE -> {
                    it.resObject?.get(UtilConstant.CURRENT_INDEX)
                        ?.let {
//                            it1.message?.get(CURRENT_INDEX)?.let { it2 ->
//                                UtilsMethods.toastMaker(
//                                    requireContext(),
//                                    it2
//                                )
//                            }
                        }
                }
            }
        }
    }

}
