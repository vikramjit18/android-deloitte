/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.learnmore

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.FragmentLearnMoreAlsEmailBinding
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods


class LearnMoreAlsEmailFragment : Fragment() {

    private var _binding: FragmentLearnMoreAlsEmailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_bottom_to_top)
        } else {
            AnimationUtils.loadAnimation(context, R.anim.slide_top_to_bottom)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentLearnMoreAlsEmailBinding.inflate(inflater, container, false)
        val root: View = binding.root
        buttonHandler()

        return root

    }

    private fun buttonHandler() {
        binding.close.setOnClickListener {
            UtilsMethods.backStack(findNavController())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
