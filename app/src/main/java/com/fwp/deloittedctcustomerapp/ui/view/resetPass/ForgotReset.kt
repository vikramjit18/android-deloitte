/*
 * Montana Fish, Wildlife & Parks
 */

/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.resetPass

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.ResetPasswordRequest
import com.fwp.deloittedctcustomerapp.databinding.ForgotResetFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotViewModel
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import com.google.android.material.textfield.TextInputEditText
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException

@AndroidEntryPoint
class ForgotReset : Fragment() {
    //init binding
    lateinit var binding: ForgotResetFragmentBinding
    private val email = SharedPrefs.read(UtilConstant.FORGOT_PASS_EMAIL_KEY, "").toString()
    private lateinit var loadingDialog: LoadingDialog

    //init fragment view model
    private val forgotViewModel: ForgotViewModel by viewModels()

    override fun onResume() {
        super.onResume()

        loadingDialog = LoadingDialog(
            requireActivity()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.forgot_reset_fragment, container, false)

        // laoding
        SharedPrefs.init(requireActivity())
        //init fun
        buttonProperty()
        //init fun
        buttonHandler()
        //init fun
        passValidator()
        //receive intent for Applinks/deep links
        getActivity()?.let { handleIntent(it.getIntent()) }
        // init textwatcher
        binding.pass.addTextChangedListener(mTextWatcher)

        return binding.root
    }

    // handling app link/ deep link intent
    private fun handleIntent(intent: Intent) {
        val appLinkData: Uri? = intent.data
        if (appLinkData != null) {

            appLinkData.getQueryParameter("uid")!!.substringAfter("?uid=").let { uid ->
                UtilConstant.uid = uid
                /* UtilsMethods.toastMaker(
                     requireContext(),
                     "${uid.toString()}, email is $email , ${UtilConstant.uid}"
                 )*/

            }
        }
    }

    // handling button
    private fun buttonHandler() {

        binding.ivClose.setOnClickListener() {
            requireActivity().finish()
        }
        binding.next.btn.setOnClickListener() {
            Navigation.findNavController(binding.root).navigate(R.id.forgot3_to_forgot4)
        }
    }

    // password validator
    private fun passValidator() {
        val password = binding.pass
        val validateImg = R.drawable.ic_check
        val unvalidateImg = R.drawable.ic_unchecked_checkbox
        password.doAfterTextChanged {
            passwordValidator(password, validateImg, unvalidateImg)

        }
    }

    private fun passwordValidator(
        password: TextInputEditText,
        validateImg: Int,
        unValidateImg: Int
    ) {
        when (password.length() >= 6) {
            true -> binding.checkboxLeastCharacter.setImageResource(validateImg)
            false -> binding.checkboxLeastCharacter.setImageResource(unValidateImg)
        }

        when (password.text.toString()
            .indexOfAny(
                charArrayOf(
                    '\'',
                    '(',
                    ')',
                    '|',
                    '\\'
                )
            ) >= 0 || password.editableText.isEmpty()) {
            true -> binding.checkboxNumberCharacter.setImageResource(unValidateImg)
            false -> binding.checkboxNumberCharacter.setImageResource(validateImg)
        }
    }


    //    button activator after validation
    private fun buttonActivation() {
        val logInButtonId = binding.next.btn
        val s2: String = binding.pass.text.toString()
        when (binding.pass.length() >= 6 && binding.pass.text.toString()
            .indexOfAny(
                charArrayOf(
                    '\'',
                    '(',
                    ')',
                    '|',
                    '\\'
                )
            ) <= 0) {
            true -> {
                logInButtonId.isEnabled = true
                logInButtonId.isClickable = true
                logInButtonId.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
                logInButtonId.setTextColor(resources.getColor(R.color.black, null))
                logInButtonId.setOnClickListener {


                    //set the message to share to another fragment
                    apiHitPassReset(s2)
                }
            }
            false -> {
                logInButtonId.isEnabled = false
                logInButtonId.isClickable = false
                logInButtonId.setTextColor(resources.getColor(R.color.white, null))
                logInButtonId.setBackgroundColor(
                    resources.getColor(
                        R.color.disableButtonColor,
                        null
                    )
                )
            }
        }
    }

    private fun apiHitPassReset(s2: String) {
        if (UtilsMethods.isInternetConnected(requireContext())) {
            //init fun
            viewModelObser()
            //starat loader
            loadingDialog.startLoading()
            try {
                forgotViewModel.resetPassRequest(
                    ResetPasswordRequest(
                        emailid = email,
                        password = s2,
                        uid = UtilConstant.uid
                    )
                )
            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (e2: SocketTimeoutException) {
                loadingDialog.isDismiss()
                e2.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }

        } else {
            UtilsMethods.showSimpleAlert(
                mContext = requireContext(),
                tvPrimary = getString(R.string.no_connection),
                tvSecondary = getString(R.string.check_and_try_again),
                primaryBtn = resources.getString(R.string.ok)

            )
        }
    }


    //view model response observer to handle fragment state
    private fun viewModelObser() {
        forgotViewModel.resetPassResponse.observe(
            viewLifecycleOwner, {
                // didmis loader
                loadingDialog.isDismiss()
                if (it.responseMessage.contains("invalid")) {
//                    UtilsMethods.toastMaker(requireContext(), "${it.resObject[0].message[0]}")
                } else {
                    UtilsMethods.navigateToFragment(binding.root, R.id.action_forgotReset_to_forgotSuccess)
                }
            }
        )
    }

    //setting button property after validation
    private fun buttonProperty() {
        binding.next.btn.text = resources.getString(R.string.nextButtonText)
        binding.next.btn.setTextColor(resources.getColor(R.color.black, null))
        binding.next.btn.setBackgroundColor(resources.getColor(R.color.buttonTrans, null))
    }

    //  create a textWatcher member
    private val mTextWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            passValidator()
            buttonActivation()
        }

        override fun afterTextChanged(editable: Editable) {
            //not needed
        }
    }
}

