/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.validated

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.PopupMenu
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.*
import com.fwp.deloittedctcustomerapp.databinding.ValidatedEtagFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.*
import com.google.gson.Gson
import android.text.style.UnderlineSpan

import android.text.SpannableString




class ValidatedEtagFragment : Fragment() {

    companion object {
        fun newInstance() = ValidatedEtagFragment()
    }

    private lateinit var viewModel: LandingScreenActivityViewModel
    private var _binding: ValidatedEtagFragmentBinding? = null
    private val binding get() = _binding!!
    private var eTagYearIndex: Any? = null
    private var accountOwner: Any? = null
    private var position: Any? = null
    private var harvestTime: Any? = null
    private var confirmationNumber: Any? = null
    private val gson = Gson()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = ValidatedEtagFragmentBinding.inflate(inflater, container, false)
        position = arguments?.getString(UtilConstant.GET_ETAG_INDEX_KEY)
        accountOwner = arguments?.getString(UtilConstant.GET_ETAG_ACCOUNT_OWNER)
        eTagYearIndex = arguments?.getString(UtilConstant.GET_ETAG_YEAR_KEY)
        confirmationNumber = arguments?.getString(UtilConstant.ETAG_CON)
        harvestTime = arguments?.getString(UtilConstant.HARVEST_TIME)

        val root = binding.root
        //Showing loader
        viewModel = ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]


        if (UtilsMethods.isInternetConnected(requireContext())) {
            viewModel.getEtag(EtagRequest(UtilConstant.DEVICE_ID))
        }
        buttonHandler()
        return root

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModelSetup()
    }

    private fun buttonHandler() {
        binding.back.setOnClickListener {
            requireActivity().finish()
            UtilConstant.backStatus = true
        }
        binding.mandatoryContainer.step2DescWeb.setOnClickListener {
            UtilsMethods.websiteOpener("https://fwp.mt.gov", requireActivity())
        }


        binding.more.setOnClickListener {
            showPopupMenu(binding.root.context, binding.more)
        }

        binding.ibAlertHarvestTime.setOnClickListener {
            UtilsMethods.showSimpleAlert(
                requireContext(),
                resources.getString(R.string.timestamp),
                resources.getString(R.string.harvest_alert_des),
                resources.getString(R.string.ok)

            )
        }

        binding.ibAlertDownload.setOnClickListener {
            UtilsMethods.showSimpleAlert(
                requireContext(),
                resources.getString(R.string.alert_download_title),
                resources.getString(R.string.alert_download_des),
                resources.getString(R.string.ok),
            )
        }
    }

    private fun showPopupMenu(context: Context, view: View) {
        val wrapper: Context = ContextThemeWrapper(context, R.style.CustomPopUpStyle)
        val popup = PopupMenu(wrapper, view)
        popup.inflate(R.menu.validate_etag_more_menu)
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->
            when (item!!.itemId) {
                R.id.remove_download_menu -> {
                    Toast.makeText(context, item.title, Toast.LENGTH_SHORT).show()
                }
                R.id.get_help_menu -> {
                    UtilsMethods.navigateToFragment(
                        binding.root,
                        R.id.action_validatedEtagFragment_to_getHelpFragment2
                    )
                }
            }

            true
        })

        popup.show()
    }


    @SuppressLint("SetTextI18n")
    private fun viewModelSetup() {
        val json = SharedPrefs.read(UtilConstant.USER_ETAG, "")
        if(!json.isNullOrEmpty()){
        setupUI(gson.fromJson(json, EtagResponse::class.java))
    }}


    private fun setupUI(
        it: EtagResponse
    ) {
        when (it.responseCode) {

            UtilConstant.SUCCESS_CODE -> {
                if (!position.toString().isNullOrEmpty() && !accountOwner.toString()
                        .isNullOrEmpty() && !eTagYearIndex.toString().isNullOrEmpty()
                ) {

                    ownerAccount(
                        it
                    )
                }
            }

            UtilConstant.ERROR_CODE -> {
                it.resObject?.get(0)
                    ?.let {
                    }
            }
        }
    }

    private fun ownerAccount(
        it: EtagResponse
    ) {
        for (item in it.resObject?.get(0)?.etagAccountsList!!.indices) {
            when (it.resObject[0].etagAccountsList!![item].owner?.contains(
                accountOwner.toString()
            )) {
                true -> it.resObject[0].etagAccountsList?.get(item)
                    ?.let { etagAccounts ->
                        val etagList = etagAccounts.etags?.get(
                            eTagYearIndex.toString().toInt()
                        )?.etagLicensesList?.get(position.toString().toInt())!!
                        val title = "${
                            etagAccounts.etags[eTagYearIndex.toString().toInt()].licenseYear
                        } ${etagList.licenseName}"

                        binding.heroTitle.text = title
                        apiResponseMandatory(etagList.etagLicenseDetails?.etagsubDetails?.eTagMandatoryDetails)

                        // bind images
                        etagList.etagLicenseDetails?.etagsubDetails?.backgroundImageUrl?.let { it1 ->
                            initImage(
                                it1
                            )
                        }
                        etagList.etagLicenseDetails?.etagsubDetails?.imageUrlIcon?.let { it1 ->
                            initTagicon(
                                it1
                            )
                        }
                        // bind buttons

                        binding.etagStatevalidated.stateContainer.visibility =
                            View.VISIBLE
                        binding.ownerName.text = etagAccounts.owner


                        settingHarvestDate()


                        binding.conNoTxt.text = confirmationNumber.toString()
                        etagList.etagLicenseDetails?.etagsubDetails?.etagConfirmationNumber

                        binding.alsLink.text =
                            etagList.etagLicenseDetails?.etagsubDetails!!.alsNumber

                        binding.issuedtext.text =
                            etagList.etagLicenseDetails.etagsubDetails.issuedDate

                        binding.downloadNo.text =
                            "${etagList.etagLicenseDetails.etagsubDetails.downloads}"

                        binding.opData.text =
                            etagList.etagLicenseDetails.etagsubDetails.oppurtunity
                        binding.heroDes.text =
                            etagList.etagLicenseDetails.etagsubDetails.oppurtunity

                        binding.tvDescription.text =
                            etagList.etagLicenseDetails.etagsubDetails.description

                        binding.desRegulation.text = "See regulations for specific harvest information, requirements, and dates"
                        binding.linkRegulation.text = UtilsMethods.stringSpannable(
                            requireContext(),
                            etagList.etagLicenseDetails.etagsubDetails.regulationsUrl.toString(),
                            R.drawable.ic_buy_yellow,
                            true
                        )
                        binding.linkRegulation.setOnClickListener {
                            UtilsMethods.websiteOpener(
                                etagList.etagLicenseDetails.etagsubDetails.regulationsUrl.toString(),
                                requireActivity()
                            )
                        }
                    }


                false -> {
                    it.resObject[0]
                        .let {
                        }
                }
            }
        }
    }

    private fun settingHarvestDate() {
        if (harvestTime.toString().isNotEmpty()) {
            val dateTimeSplit = harvestTime.toString().split(" ")
            //                                        UtilsMethods.toastMaker(requireContext(),"${harvestTime.toString()}, $dateTimeSplit ")
            val alterTime: String =
                if (dateTimeSplit.contains("PM")) {
                    "${dateTimeSplit[0]} - ${
                        dateTimeSplit[2].substring(
                            0,
                            5
                        )
                    } p.m."
                } else {
                    "${dateTimeSplit[0]} - ${
                        dateTimeSplit[2].substring(
                            0,
                            5
                        )
                    } a.m."
                }
            binding.timeHarText.text = alterTime
        }
    }


    private fun initImage(imagename: String) {
        // initialization of constraint
        //when -> step1 complete -> change ic to success ic,

        val heroImageId = binding.bgImagem

        TagImageInflater().showDetailedTagImage(
            context = requireContext(),
            imageView = heroImageId,
            heroName = imagename
        )

        binding.mandatoryContainer.step1IcCheck.visibility = View.VISIBLE

    }

    private fun initTagicon(iconName: String) {
        val heroIconId = binding.heroIc
        TagImageInflater().showDetailedTagIcon(
            context = requireContext(),
            imageView = heroIconId,
            heroName = iconName
        )
    }

    private fun apiResponseMandatory(eTagMandatoryDetails: List<ETagMandatoryDetail>?) {

        if (eTagMandatoryDetails != null && eTagMandatoryDetails[0].reReportingHTMLText?.isNotEmpty() == true) {
            binding.mandatoryContainer.clMandatory.visibility = View.VISIBLE
            eTagMandatoryDetails.forEach {
                when (it.rsReportingStep) {

                    "1" -> {
                        val step1Heading = "Step 1 – ${
                            eTagMandatoryDetails[0].reReportingHTMLText!!.substringBefore("|")
                        }"
                        binding.mandatoryContainer.step1Heading.text = step1Heading

                        binding.mandatoryContainer.step1Desc.visibility = View.GONE
                    }
                    "2" -> {
                        var result: String = ""
                        val step2Heading = "Step 2 – ${
                            eTagMandatoryDetails[1].reReportingHTMLText!!.substringBefore("|")
                        }"
                        binding.mandatoryContainer.step2Heading.text = step2Heading
                        binding.mandatoryContainer.step2Desc.text =
                            eTagMandatoryDetails[1].reReportingHTMLText!!.substringAfter("|")
                                .substringBefore("\n\nBy phone:")
                        binding.mandatoryContainer.step2Desc.visibility = View.VISIBLE
                        //                  Phone no set
                        val no =   eTagMandatoryDetails[1].reReportingHTMLText!!.substringAfter("\n\nBy phone:\n").substringBefore("\n\nOnline:")
                        if (!no.isNullOrEmpty() && UtilConstant.PHONE_PATTERN.matcher(no).matches()) {
                            binding.mandatoryContainer.step2DescCall1.visibility = View.VISIBLE
                            binding.mandatoryContainer.step2DescCall2.visibility = View.VISIBLE
                            val contentUnderLine = SpannableString(no)
                            contentUnderLine.setSpan(
                                UnderlineSpan(),
                                0,
                                no.length,
                                0
                            )
                            binding.mandatoryContainer.step2DescCall2.text = contentUnderLine
                            binding.mandatoryContainer.step2DescCall2.setOnClickListener {
                                UtilsMethods.dialerOpener(
                                    "1$no",
                                    requireActivity()
                                )
                            }
                        }

                        if (eTagMandatoryDetails[1].reReportingHTMLText!!.contains("fwp.mt.gov")){
                            binding.mandatoryContainer.step2DescWeb.visibility = View.VISIBLE
                            binding.mandatoryContainer.step2DescWeb.setOnClickListener {
                                UtilsMethods.websiteOpener("https://fwp.mt.gov", requireActivity())
                            }
                        }else{
                            binding.mandatoryContainer.step2DescWeb.visibility = View.GONE
                        }
                    }
                    "3" -> {
                        val step3Heading = "Step 3 – ${
                            eTagMandatoryDetails[2].reReportingHTMLText!!.substringBefore("|")
                        }"
                        binding.mandatoryContainer.step3Heading.text = step3Heading

                        binding.mandatoryContainer.step3Desc.text =
                            eTagMandatoryDetails[2].reReportingHTMLText!!.substringAfter("|")
                    }
                }
            }
        } else {
            binding.mandatoryContainer.clMandatory.visibility = View.GONE
        }



        binding.mandatoryContainer.step2DescCall1.setOnClickListener {
            UtilsMethods.dialerOpener(getString(R.string.personally_call_1_no), requireActivity())

        }
        binding.mandatoryContainer.step2DescCall2.setOnClickListener {
            UtilsMethods.dialerOpener(getString(R.string.personally_call_2_no), requireActivity())
        }
    }

    private fun extractingPhoneNo(
        eTagMandatoryDetails: List<ETagMandatoryDetail>,
    ): String {
        var result1 = ""
        when {
            eTagMandatoryDetails[1].reReportingHTMLText!!.contains("12 hours,") -> {
                result1 =
                    eTagMandatoryDetails[1].reReportingHTMLText?.substringAfter(
                        "12 hours,"
                    )!!
            }
            eTagMandatoryDetails[1].reReportingHTMLText!!.contains("24 hours,") -> {
                result1 =
                    eTagMandatoryDetails[1].reReportingHTMLText?.substringAfter(
                        "24 hours,"
                    )!!
            }
            eTagMandatoryDetails[1].reReportingHTMLText!!.contains("48 hours,") -> {
                result1 =
                    eTagMandatoryDetails[1].reReportingHTMLText?.substringAfter(
                        "48 hours,"
                    )!!
            }
            else ->{
                result1 = ""
            }
        }
        return result1
    }

}
