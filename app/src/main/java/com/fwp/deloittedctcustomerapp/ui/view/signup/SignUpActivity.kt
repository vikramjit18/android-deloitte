/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.signup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        SharedPrefs.init(this)
    }

    override fun onBackPressed() {

    }
}

