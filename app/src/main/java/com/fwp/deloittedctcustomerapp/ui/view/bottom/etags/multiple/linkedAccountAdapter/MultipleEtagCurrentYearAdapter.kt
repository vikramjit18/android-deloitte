/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.linkedAccountAdapter


import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagAccounts
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.ValidateTagsActivity
import com.fwp.deloittedctcustomerapp.utils.*
import com.google.android.material.button.MaterialButton


class MultipleEtagCurrentYearAdapter(
    private val list: EtagAccounts,
    private val oflineSpiidList: List<String>
) :
    RecyclerView.Adapter<MultipleEtagCurrentYearAdapter.MyViewHolder>() {

    private lateinit var mListener: OnItemClickListener

    interface OnItemClickListener {
        fun onItemClick(position: Int, holder: MyViewHolder)
    }

    fun setOnItemClick(listener: OnItemClickListener) {
        mListener = listener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {

        val binding = LayoutInflater.from(parent.context)
            .inflate(R.layout.etags_available_child_row, parent, false)

        return MyViewHolder(binding, mListener)
    }


    class MyViewHolder(viewHolder: View, listener: OnItemClickListener) :
        RecyclerView.ViewHolder(viewHolder) {

        val itemContainer: ConstraintLayout = viewHolder.findViewById(R.id.relativeLayout)
        val tagImage: ImageView = viewHolder.findViewById(R.id.tagImage)
        val yearAnimalType: TextView = viewHolder.findViewById(R.id.year_animal_type)
        val region: TextView = viewHolder.findViewById(R.id.region)
        val etagContainer: FrameLayout = viewHolder.findViewById(R.id.etagStateContainer)
        val downloadBtnContainer: LinearLayout =
            viewHolder.findViewById(R.id.downloadButtonContainer)
        val etagValidate: View = viewHolder.findViewById(R.id.etagValidated)
        val iconDownloadBtn: MaterialButton = viewHolder.findViewById(R.id.iconDownloadButton)
        val iconProgressBtn: CardView = viewHolder.findViewById(R.id.iconProgressButton)
        val validateBtn: Button = viewHolder.findViewById(R.id.validateButton)
        val etagVoid: View = viewHolder.findViewById(R.id.etag_void)
        val etagRefund: View = viewHolder.findViewById(R.id.etag_refunded)
        val etagExpire: View = viewHolder.findViewById(R.id.etag_expire)


        init {
            iconDownloadBtn.setOnClickListener {
                when (UtilsMethods.isInternetConnected(viewHolder.context)) {
                    true -> {
                        listener.onItemClick(adapterPosition, MyViewHolder(viewHolder, listener))
                    }
                    false -> {
                        UtilsMethods.showSimpleAlert(
                            mContext = viewHolder.context,
                            tvPrimary = "No connection",
                            tvSecondary = "Check your network connection and try again.",
                            primaryBtn = "Ok"
                        )
                    }
                }
            }
        }
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val tagStatus = mutableListOf<String>()
        holder.validateBtn.setText(R.string.validate)
        holder.yearAnimalType.text =
            "${list.etags?.get(UtilConstant.CURRENT_INDEX)?.licenseYear} ${
                list.etags!![UtilConstant.CURRENT_INDEX].etagLicensesList?.get(
                    position
                )?.etagLicenseDetails?.etagsubDetails?.licenseName
            }"
        holder.region.text =
            list.etags!![UtilConstant.CURRENT_INDEX].etagLicensesList?.get(position)?.etagLicenseDetails?.districtCode

        tagStatus.clear()
        list.etags!![UtilConstant.CURRENT_INDEX].etagLicensesList?.get(position)?.etagLicenseDetails?.etagsubDetails?.status?.forEach {
            tagStatus.add(it)
        }

//setings list icon
        list.etags!![UtilConstant.CURRENT_INDEX].etagLicensesList?.get(position)?.etagLicenseDetails?.imageUrl!!.let {
            TagImageInflater().showListTagIcon(
                context = holder.itemView.context,
                imageView = holder.tagImage,
                heroName = it
            )
        }

//etagactive
        when (list.etags!![UtilConstant.CURRENT_INDEX].etagLicensesList?.get(position)?.etagLicenseDetails?.isEtagActive) {
            EtagActiveStatus.YES -> {
                holder.downloadBtnContainer.visibility = View.VISIBLE
//                    holder.etagContainer.visibility = View.GONE

                checkingValidationOffline(position, holder)
            }
            EtagActiveStatus.NO -> {
                holder.downloadBtnContainer.visibility = View.GONE
//                    holder.etagContainer.visibility = View.VISIBLE
            }
        }



        if (tagStatus.isNotEmpty()) {
            tagStatus.forEach {
                holder.etagContainer.visibility = View.VISIBLE
                when (it) {

                    TagStatus.VALIDATED -> holder.etagValidate.visibility = View.VISIBLE
//                    TagStatus.EXPIRED ->     holder.etagExpire.visibility = View.VISIBLE
                    TagStatus.REFUNDED -> holder.etagRefund.visibility = View.VISIBLE
//                    TagStatus.VOID ->     holder.etagVoid.visibility = View.VISIBLE

                }
            }
        }


        when ((list.etags!![UtilConstant.CURRENT_INDEX].etagLicensesList?.get(position)?.etagLicenseDetails?.downloadStatus)) {

            DownloadStatus.NO_DOWNLOAD -> {
                holder.iconDownloadBtn.setIconResource(R.drawable.ic_dowload_yellow)
                holder.iconDownloadBtn.setIconTintResource(R.color.prim_yellow)
                holder.iconDownloadBtn.isCheckable = true

                disableValiBtn(holder)
            }
            DownloadStatus.SAME_DEVICE -> {
                holder.iconDownloadBtn.isClickable = false
                holder.iconDownloadBtn.setIconResource(R.drawable.ic_download_done)
                holder.iconDownloadBtn.setIconTintResource(R.color.white)
                holder.iconDownloadBtn.setStrokeColorResource(R.color.black)
                holder.iconDownloadBtn.setBackgroundColor(
                    holder.itemView.context.getColor(
                        R.color.black
                    )
                )
//                disableValiBtn(holder,position)
                enableValiBtn(holder)
            }

            DownloadStatus.OTHER_DEVICE -> {
                holder.iconDownloadBtn.isClickable = true
                holder.iconDownloadBtn.visibility = View.VISIBLE
                holder.iconDownloadBtn.setIconResource(R.drawable.ic_warning_yellow)
                holder.iconDownloadBtn.setIconTintResource(R.color.prim_yellow)
                /*        binding.downloaded.setBackgroundColor(
                            requireContext().getColor(
                                R.color.black
                            )
                        )*/
            }
            /*       "E" -> {
                       holder.iconDownloadBtn.isClickable = false
                       holder.iconDownloadBtn.setIconResource(R.drawable.ic_downloading)
                       holder.iconDownloadBtn.setStrokeColorResource(R.color.black)
                       holder.iconDownloadBtn.setBackgroundColor(
                           holder.itemView.context.getColor(
                               R.color.black
                           )
                       )

                   }*/
        }
        list.owner?.let { viewDetails(holder, position, it) }
    }

    private fun checkingValidationOffline(
        position: Int,
        holder: MyViewHolder
    ) {
        when ((oflineSpiidList.contains(
            list.etags!![UtilConstant.CURRENT_INDEX].etagLicensesList?.get(
                position
            )?.etagLicenseDetails?.spiId
        ))) {
            true -> {
                holder.downloadBtnContainer.visibility = View.GONE
                holder.etagContainer.visibility = View.VISIBLE
                holder.etagValidate.visibility = View.VISIBLE
            }
            false -> {
                //                holder.downloadBtnContainer.visibility = View.VISIBLE
                holder.etagContainer.visibility = View.GONE

            }
        }
    }

    private fun enableValiBtn(holder: MyViewHolder) {
        val validatorButtonId = holder.validateBtn

        validatorButtonId.isEnabled = true
        validatorButtonId.isClickable = true
        validatorButtonId.setTextColor(holder.itemView.context.getColor(R.color.black))
        validatorButtonId.setBackgroundColor(
            holder.itemView.context.getColor(
                R.color.prim_yellow
            )
        )
    }

    private fun disableValiBtn(holder: MyViewHolder) {
        val validatorButtonId = holder.validateBtn

        validatorButtonId.isEnabled = false
        validatorButtonId.isClickable = false
        validatorButtonId.setTextColor(holder.itemView.context.getColor(R.color.white))
        validatorButtonId.setBackgroundColor(holder.itemView.context.getColor(R.color.buttonTrans))
    }

    private fun viewDetails(holder: MyViewHolder, position: Int, owner: String) {

        holder.itemContainer.setOnClickListener {
            val i = Intent(holder.itemView.context, ValidateTagsActivity::class.java)
            i.putExtra(UtilConstant.GET_ETAG_INDEX_KEY, position)
            i.putExtra(UtilConstant.GET_ETAG_ACCOUNT_OWNER, owner)
            i.putExtra(UtilConstant.GET_ETAG_YEAR_KEY, 0)
            holder.itemView.context.startActivity(i)
        }

        holder.validateBtn.setOnClickListener {
            val i = Intent(holder.itemView.context, ValidateTagsActivity::class.java)
            i.putExtra(UtilConstant.GET_ETAG_INDEX_KEY, position)
            i.putExtra(UtilConstant.GET_ETAG_ACCOUNT_OWNER, owner)
            i.putExtra(UtilConstant.GET_ETAG_YEAR_KEY, 0)
            holder.itemView.context.startActivity(i)
        }

    }

    override fun getItemCount(): Int {
        return list.etags?.get(0)?.etagLicensesList!!.size
    }
}
