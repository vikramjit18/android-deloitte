/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.ui.view.MainActivity
import com.fwp.deloittedctcustomerapp.ui.view.bottom.LandingScreenActivity
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods

class SplashScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        val time: Long = 2000
        // init shared prefs
        SharedPrefs.init(context = applicationContext)
        val token = SharedPrefs.read(UtilConstant.JWT_TOKEN, "")
        val biometricBool = SharedPrefs.read(UtilConstant.BIOMETRIC_ENABLE, "")

        /**to retain session*/
        val oldLogoutDate = SharedPrefs.read("login_date", "")
        if (!oldLogoutDate.isNullOrEmpty()) {
            SharedPrefs.write(UtilConstant.LOGIN_DATE, UtilsMethods.getCurrentDateMMMddYY())
            SharedPrefs.removeData("login_date")
        }
        /** end retain session*/
        Handler(Looper.myLooper()!!).postDelayed({
            if (token.isNullOrEmpty()) {
                val intent: Intent = Intent(this@SplashScreen, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                if (!biometricBool.isNullOrEmpty()) {
                    /**
                     * change activity to Login Activity after biometric
                     * */
                    val intent: Intent =
                        Intent(this@SplashScreen, LandingScreenActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    val intent: Intent =
                        Intent(this@SplashScreen, LandingScreenActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
//            startActivity(intent)
//            finish()


        }, time)
    }


}
