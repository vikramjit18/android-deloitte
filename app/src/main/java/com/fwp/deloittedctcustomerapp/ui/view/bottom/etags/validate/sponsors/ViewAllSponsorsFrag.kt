/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.sponsors

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.Sponsors
import com.fwp.deloittedctcustomerapp.databinding.ViewAllSponsorsFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ViewAllSponsorsFrag : Fragment() {

    companion object {
        fun newInstance() = ViewAllSponsorsFrag()
    }

    private lateinit var binding: ViewAllSponsorsFragmentBinding
    private val listSponsors = mutableListOf<Sponsors>()
    private lateinit var communicator: Communicator
    private lateinit var viewModel: ViewAllSponsorsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.view_all_sponsors_fragment, container, false)
        communicator = ViewModelProvider(requireActivity())[Communicator::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initSponsorList()
        buttonHandler()


        binding.rvSponsor.apply {
            layoutManager = LinearLayoutManager(activity?.applicationContext)
            adapter = SponsorAdapter(listSponsors)
        }

    }

    private fun buttonHandler() {
        binding.ivClose.setOnClickListener {
            UtilsMethods.backStack(findNavController())
        }
    }

    private fun initSponsorList() {

        communicator.sponsorDetails.observe(requireActivity(), {
            it.forEach { list ->
                listSponsors.add(Sponsors(list.toString()))
            }
        })

    }

}
