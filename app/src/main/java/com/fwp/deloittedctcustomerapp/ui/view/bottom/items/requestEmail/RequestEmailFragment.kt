/*
 * Montana Fish, Wildlife & Parks
 */



package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopyPrimaryRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopySecondaryRequest
import com.fwp.deloittedctcustomerapp.databinding.FragmentRequestEmailBinding
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException

/**
 * FWP
 * Hilt entry point
 */
@AndroidEntryPoint
class RequestEmailFragment : Fragment() {

    companion object {
        fun newInstance() = RequestEmailFragment()
    }

    // Global Variables
    private var _binding: FragmentRequestEmailBinding? = null
    private val binding get() = _binding!!
    private val requestEmailViewModel: RequestEmailViewModel by viewModels()
    private lateinit var loadingDialog: LoadingDialog
    private var itemAccount: Any? = null
    private var alsNo: Any? = null
    private var dob: Any? = null
    private var extras: Bundle? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentRequestEmailBinding.inflate(inflater, container, false)
        extras = requireActivity().intent.extras
        // init
        loadingDialog = LoadingDialog(requireActivity())
        SharedPrefs.init(requireActivity())
        binding.etEmail.editText.setText(SharedPrefs.read(UtilConstant.FWP_EMAIL_LOGIN, ""))
        // Returning root binding
        return binding.root
    }
//    override fun getTheme() = R.style.CustomBottomSheetDialogTheme

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Email Edittext Input Type
        binding.etEmail.editText.inputType =
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
        itemAccount = extras?.get(UtilConstant.GET_ITEM_ACCOUNT)
        alsNo = extras?.get(UtilConstant.GET_ALS_NO)
        dob = extras?.get(UtilConstant.GET_DOB)
//        UtilsMethods.toastMaker(requireContext(),"item account $itemAccount, Alsno $alsNo, dob $dob")
        // Global functions
        buttonHandler()
        disableButton()
        binding.next.isEnabled= false
        binding.next.isClickable= false

// validating emails
        binding.etEmail.editText.addTextChangedListener(mTextWatcher)
        validatorEmails()
        // Observing request copy email
        viewModelObser()
    }

    private fun validatorEmails() {
        val emailList = binding.etEmail.editText.text.toString().split(",")
        val emailError = binding.textViewErr

        if (emailList.isNotEmpty() && emailList[0].trim().contains("@")) {
            for (i in emailList) {
                when (UtilConstant.EMAIL_ADDRESS_PATTERN.matcher(i.trim())
                    .matches() || i.trim().isEmpty()) {
                    true -> {
                        binding.etEmail.editText.background = ContextCompat.getDrawable(requireContext(), R.drawable.et_selector)
                        emailError.visibility =View.GONE
                        binding.next.isEnabled= true
                        binding.next.isClickable= true
                        enableButton()
                    }
                    false -> {
                        disableButton()
                        binding.etEmail.editText.background = ContextCompat.getDrawable(requireContext(), R.drawable.et_error_selector)
                        emailError.visibility =View.VISIBLE

                    }
                }
            }
        }
    }
    private fun disableButton(){
        binding.next.isEnabled= false
        binding.next.isClickable = false
        binding.next.setTextColor(resources.getColor(R.color.white, null))
        binding.next.setBackgroundColor(
            resources.getColor(
                R.color.disableButtonColor,
                null
            )
        )
}    private fun enableButton(){
        binding.next.isEnabled= true
        binding.next.isClickable= true
        binding.next.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
        binding.next.setTextColor(resources.getColor(R.color.black, null))

    }
    private fun buttonHandler() {
        binding.close.backLayout.setOnClickListener {
            showDialerAlert(requireContext())
        }
        binding.next.setOnClickListener {
            if (UtilsMethods.isInternetConnected(requireContext())) {
                // show loader
                requestApiHit()
            } else {
                // showing No internet dialog
                UtilsMethods.showSimpleAlert(
                    mContext = requireContext(),
                    tvPrimary = getString(R.string.no_connection),
                    tvSecondary = getString(R.string.check_and_try_again),
                    primaryBtn = resources.getString(R.string.ok)
                )
            }
        }
    }

    private fun showDialerAlert(
        mContext: Context
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val leftBtn = view.findViewById<TextView>(R.id.btn_left)
        val rightBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = "Exit and cancel request?"
        des.text = "If you exit now, you will not receive a copy of your license listing. "
        leftBtn.text = "No, go back"
        rightBtn.text = "Yes, exit"
        builder.setView(view)
        rightBtn.setOnClickListener {
            builder.dismiss()
            requireActivity().finish()
        }
        leftBtn.setOnClickListener {
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    private fun requestApiHit() {

        val token = SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString()

        val emailList = binding.etEmail.editText.text.toString().split(",")
        Log.e("TAG email list", "buttonHandler: $emailList")
        try {
            when (itemAccount.toString().toInt()) {
                1 -> {
                    loadingDialog.startLoading()
                    requestEmailViewModel.requestEmailPrimary(
                        token,
                        RequestCopyPrimaryRequest(emailList)
                    )
                }
                2 -> {
                    loadingDialog.startLoading()
                    requestEmailViewModel.requestEmailSecondary(
                        token,
                        RequestCopySecondaryRequest(alsNo = alsNo.toString().trim().toInt(),
                            dob = dob.toString().trim(),
                        emailList)
                    )
                }
            }
//            requestEmailViewModel.requestEmailPrimary(
//                token,
//                RequestCopyPrimaryRequest(emailList)
//            )
        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (e2: SocketTimeoutException) {
            loadingDialog.isDismiss()
            e2.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        }
    }

    private fun viewModelObser() {
        requestEmailViewModel.requestCopyEmailResponse.observe(viewLifecycleOwner, {
            loadingDialog.isDismiss()
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    UtilConstant.emailSent = true
                    requireActivity().finish()
                }
            }
        })

        requestEmailViewModel.toast.observe(viewLifecycleOwner, {
            it.getContentIfNotHandledOrReturnNull()?.let { message ->
//                UtilsMethods.toastMaker(requireContext(), message)
            }
        })
    }

//    private fun checkForEmptyField(): Boolean {
//
//        if (binding.etEmail.text.isNullOrEmpty() || binding.etEmail.editableText.equals(
//                ""
//            )
//        ) {
////            UtilsMethods.toastMaker(requireContext(), "Field cannot be empty")
//            return false
//        }
//
//        return true
//    }


    /*    */
    /**
     *  create
     *  a textWatcher member
     */
    private val mTextWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
            buttonEnable()

        }

        override fun afterTextChanged(editable: Editable) {
            validatorEmails()
        }
    }

    private fun buttonEnable() {

    }
//    private fun setupBackPressListener() {
//        this.view?.isFocusableInTouchMode = true
//        this.view?.requestFocus()
//        this.view?.setOnKeyListener { _, keyCode, _ ->
//            keyCode == KeyEvent.KEYCODE_BACK
//        }
//    }

    /**
     * Destroy
     * binding on app close
     */
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
