/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.util.Log
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.compose.ui.text.android.InternalPlatformTextApi
import androidx.compose.ui.text.android.style.TypefaceSpan
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
import com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.Etag
import com.fwp.deloittedctcustomerapp.data.model.responses.EtagListDetail
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse
import com.fwp.deloittedctcustomerapp.databinding.EtagsAvailableFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagCurrentYearAdapter
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.*
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.CURRENT_INDEX
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.DEVICE_ID
import com.fwp.deloittedctcustomerapp.utils.UtilConstant.PREVIOUS_INDEX
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException


private const val TAG = "EtagsAvailableFragment"

@AndroidEntryPoint
class EtagsAvailableFragment : Fragment() {

    companion object {
        fun newInstance() = EtagsAvailableFragment()
    }

    private var watchDbToDelete: Boolean = false
    private lateinit var viewModel: LandingScreenActivityViewModel
    private var _binding: EtagsAvailableFragmentBinding? = null
    private val binding get() = _binding!!


    private val downLoadAllSpiIDLIst = mutableListOf<String>()
    private val offLoadAllSpiIDLIst = mutableListOf<String>()
    private val offLoadPreSpiIDLIst = mutableListOf<String>()
    private val allCurrentSpiIdList = mutableListOf<String>()
    private val allPreviousSpiIdList = mutableListOf<String>()
    private val currentDownloadStatus = mutableListOf<String>()
    private val previousDownloadStatus = mutableListOf<String>()
    private val allDownloadStatus = mutableListOf<String>()
    private val allDownloadStatusForOffLoad = mutableListOf<String>()
    private val offlineSpiid = mutableListOf<String>()
    private val listCurrentYearDetail = mutableListOf<Etag>()
    private val listPreviousYearDetail = mutableListOf<Etag>()
    private lateinit var validateTagActivityViewModel: ValidateTagActivityViewModel
    private lateinit var loadingDialog: LoadingDialog
    private val gson = Gson()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = EtagsAvailableFragmentBinding.inflate(inflater, container, false)
        loadingDialog = LoadingDialog(requireActivity())
        viewModel = ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]
        SharedPrefs.init(requireActivity())
        validateTagActivityViewModel =
            ViewModelProvider(requireActivity())[ValidateTagActivityViewModel::class.java]



        if (UtilConstant.multiAccount) {
            val layout = binding.rLEtagMain
            val card = binding.mainEtagCard
            val layoutParams = layout.layoutParams as FrameLayout.LayoutParams
            val cardParams = card.layoutParams as FrameLayout.LayoutParams
            layoutParams.topMargin = 0
            cardParams.topMargin = 0
            layout.layoutParams = layoutParams
            layout.layoutParams = cardParams
        }


        validateTagActivityViewModel.getValidateTagData().observe(viewLifecycleOwner) {
            offlineSpiid.clear()
            it.forEach {
                it.spiId?.let { it1 -> offlineSpiid.add(it1) }
            }

            if (UtilsMethods.isInternetConnected(requireContext())) {
                binding.customPopup1.tvToottip.text = resources.getText(R.string.etag_tooltip_text)
//                viewModel.getEtag(EtagRequest(DEVICE_ID))
//todo
            } else {
                binding.customPopup1.tvToottip.text = getString(R.string.etag_offline_text)
                val json = SharedPrefs.read(UtilConstant.USER_ETAG, "")
                if (!json.isNullOrEmpty()) {
                    setupUI(gson.fromJson(json, EtagResponse::class.java))
                }
            }
        }


        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.loader.clProgress.visibility = View.VISIBLE
        buttonHandler()
        checkLiveNetworkConnection()
        setSpannableString()

    }


    private fun checkLiveNetworkConnection() {
        val connectivityManager =
            requireActivity().getSystemService(ConnectivityManager::class.java)
        val liveConnection = ConnectionLiveData(connectivityManager)
        liveConnection.observe(viewLifecycleOwner) { isConnected ->
            if (isConnected) {
                viewModel.getEtag(EtagRequest(DEVICE_ID))
                binding.customPopup1.tvToottip.text = resources.getText(R.string.etag_tooltip_text)
            } else {
                binding.customPopup1.tvToottip.text = getString(R.string.etag_offline_text)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        SharedPrefs.read(UtilConstant.NAME, "User")?.let { viewModel.setOwnerName(it) }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModelObsr()
    }

    private fun viewModelObsr() {
        viewModel.etagResponse.observe(viewLifecycleOwner) {
            loadingDialog.isDismiss()
            if (!it.resObject!!.isNullOrEmpty()) {
                when (it.responseCode) {
                    UtilConstant.SUCCESS_CODE -> {
//                        UtilsMethods.toastMaker(requireContext(),"location hit ${UtilConstant.getLocCount}")
                        if (UtilConstant.getLocCount == 0) {
                            GetLocation(requireActivity())
                            UtilConstant.getLocCount++
                        }
//                        ab.observe(this) {
//                            if (it != null) {
//                                val lats = it.lastLocation.latitude
//                                val long = it.lastLocation.longitude
//                                Log.e(TAG, "viewModelObsr:   $lats , $long ")
//                            }
//                        }
//                        // Saving data
                        val eJson = gson.toJson(it)
                        SharedPrefs.write(UtilConstant.USER_ETAG, eJson)
                        setupUI(it)
                    }
                }
            }

        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupUI(it: EtagResponse) {


        /*       val gson = Gson()
               val eJson = gson.toJson(it)
               SharedPrefs.write(UtilConstant.USER_ETAG, eJson)*/

        UtilConstant.PRIMARY_ACCOUNT_OWNER = it.resObject?.get(0)?.etagAccountsList?.get(0)?.owner!!

        clearList()

        binding.noEtagPrevYear.noEtagPurchasePre.text = " You held no E-tags for the ${
            it.resObject[0].etagAccountsList?.get(0)?.etags?.get(1)?.licenseYear
        } license year."

        binding.previousYearHeading.text =
            "${it.resObject[0].etagAccountsList?.get(0)?.etags?.get(1)?.licenseYear} ${
                resources.getString(
                    R.string.previousYearText
                )
            }"

        binding.yearHeader.text =
            "${it.resObject[0].etagAccountsList?.get(0)?.etags?.get(0)?.licenseYear} ${
                resources.getString(
                    R.string.currentYearText
                )
            }"

        // Hide loading
//        loadingDialog.isDismiss()

        binding.more.visibility = View.VISIBLE
        binding.downloadAll.visibility = View.VISIBLE
        // primary user download all spid and status and offload spid and status'

        for (etagIndex in it.resObject[0].etagAccountsList!![0].etags!!.indices) {
            val userEtags = it.resObject[0].etagAccountsList!![0].etags!![etagIndex]
            loopEtagLicenseList(userEtags, userEtags.etagLicensesList!!)
        }

        // Getting previous spi id and download status
        for (i in it.resObject[0].etagAccountsList?.get(0)?.etags?.get(PREVIOUS_INDEX)?.etagLicensesList?.indices!!) {


            val response =
                it.resObject[0].etagAccountsList?.get(0)?.etags?.get(PREVIOUS_INDEX)?.etagLicensesList?.get(
                    i
                )?.etagLicenseDetails

            response?.spiId?.let { it1 -> allPreviousSpiIdList.add(it1) }


            // previous download status
            response?.downloadStatus?.let { it1 ->
                previousDownloadStatus.add(it1)
            }
        }


        //for Current Year
        when (it.resObject[0].etagAccountsList?.get(0)?.etags?.get(0)?.etagLicensesList.isNullOrEmpty()) {
            true -> {
                binding.noEtagAvailable.noEtagPur.visibility = View.VISIBLE
                binding.currentYearRecyclerView.visibility = View.GONE
                binding.buyAndApply.buyApplyCard.visibility = View.GONE

            }
            false -> {
                binding.noEtagAvailable.noEtagPur.visibility = View.GONE
                binding.currentYearRecyclerView.visibility = View.VISIBLE
                binding.buyAndApply.buyApplyCard.visibility = View.VISIBLE
                for (i in it.resObject[0].etagAccountsList?.get(0)?.etags?.get(0)?.etagLicensesList!!.indices) {

                    listCurrentYearDetail.add(
                        Etag(
                            "${it.resObject[0].etagAccountsList?.get(0)?.etags?.get(0)?.licenseYear}",
                            list = EtagListDetail(
                                year = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        0
                                    )?.licenseYear
                                }",
                                animalName = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        0
                                    )?.etagLicensesList!![i].etagLicenseDetails?.etagsubDetails?.licenseName
                                }",
                                region = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        0
                                    )?.etagLicensesList!![i].etagLicenseDetails?.districtCode
                                }",
                                validateStatus = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        0
                                    )?.etagLicensesList!![i].etagLicenseDetails?.validatedStatus
                                }",
                                downloadStatus = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        0
                                    )?.etagLicensesList!![i].etagLicenseDetails?.downloadStatus
                                }",
                                imageUrl = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        0
                                    )?.etagLicensesList!![i].etagLicenseDetails?.imageUrl
                                }",
                                owner = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.owner
                                }",
                                accountType = "${it.resObject[0].etagAccountsList?.get(0)?.accountType}",
                                spiid = "${it.resObject[0].etagAccountsList?.get(0)?.etags!![0].etagLicensesList!![i].etagLicenseDetails?.spiId}",
                                tagStatus = it.resObject[0].etagAccountsList?.get(
                                    CURRENT_INDEX
                                )?.etags!![0].etagLicensesList?.get(i)?.etagLicenseDetails?.etagsubDetails?.status,
                                isEtagActive = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        0
                                    )?.etagLicensesList!![i].etagLicenseDetails?.isEtagActive
                                }",
                            )
                        )
                    )
                }

                // Getting spi id and download status
                for (i in it.resObject[0].etagAccountsList?.get(0)?.etags?.get(0)?.etagLicensesList?.indices!!) {


                    val response =
                        it.resObject[0].etagAccountsList?.get(0)?.etags?.get(0)?.etagLicensesList?.get(
                            i
                        )?.etagLicenseDetails

                    response?.spiId?.let { it1 -> allCurrentSpiIdList.add(it1) }

                    // downlaod

                    response?.downloadStatus?.let { it1 ->
                        currentDownloadStatus.add(it1)
                    }
                }
            }
        }

        //for previous year
        when (it.resObject[0].etagAccountsList?.get(0)?.etags?.get(1)?.etagLicensesList.isNullOrEmpty()) {
            true -> {
                binding.noEtagPrevYear.noEtagYear.visibility = View.VISIBLE
                binding.previousYearRecyclerView.visibility = View.GONE
                it.resObject[0].etagAccountsList?.get(0)?.etags?.get(0)?.etagLicensesList
            }
            false -> {
                binding.previousYearRecyclerView.visibility = View.VISIBLE
                binding.noEtagPrevYear.noEtagYear.visibility = View.GONE

                for (i in it.resObject[0].etagAccountsList?.get(0)?.etags?.get(PREVIOUS_INDEX)?.etagLicensesList!!.indices) {
                    listPreviousYearDetail.add(
                        Etag(
                            "${
                                it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                    PREVIOUS_INDEX
                                )?.licenseYear
                            }",
                            list = EtagListDetail(
                                year = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        PREVIOUS_INDEX
                                    )?.licenseYear
                                }",
                                animalName = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        PREVIOUS_INDEX
                                    )?.etagLicensesList!![i].etagLicenseDetails?.etagsubDetails?.licenseName
                                }",
                                region = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        PREVIOUS_INDEX
                                    )?.etagLicensesList!![i].etagLicenseDetails?.districtCode
                                }",
                                validateStatus = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        PREVIOUS_INDEX
                                    )?.etagLicensesList!![i].etagLicenseDetails?.validatedStatus
                                }",
                                downloadStatus = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        PREVIOUS_INDEX
                                    )?.etagLicensesList!![i].etagLicenseDetails?.downloadStatus
                                }",
                                imageUrl = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        PREVIOUS_INDEX
                                    )?.etagLicensesList!![i].etagLicenseDetails?.imageUrl
                                }",
                                owner = "${
                                    it.resObject[0].etagAccountsList?.get(0)?.etags?.get(
                                        PREVIOUS_INDEX
                                    )?.etagLicensesList!![i].etagLicenseDetails?.etagsubDetails?.owner
                                }",
                                accountType = "${it.resObject[0].etagAccountsList?.get(CURRENT_INDEX)?.accountType}",
                                spiid = it.resObject[0].etagAccountsList?.get(CURRENT_INDEX)?.etags!![PREVIOUS_INDEX].etagLicensesList?.get(
                                    i
                                )?.etagLicenseDetails?.spiId,
                                tagStatus = it.resObject[0].etagAccountsList?.get(CURRENT_INDEX)?.etags!![PREVIOUS_INDEX].etagLicensesList?.get(
                                    i
                                )?.etagLicenseDetails?.etagsubDetails?.status,
                                isEtagActive = "${
                                    it.resObject[0].etagAccountsList?.get(CURRENT_INDEX)?.etags?.get(
                                        PREVIOUS_INDEX
                                    )?.etagLicensesList!![i].etagLicenseDetails?.isEtagActive
                                }",
                            )

                        )
                    )
                }


            }
        }

        recyclerContent(
            previousList = listPreviousYearDetail,
            currentList = listCurrentYearDetail
        )
    }

    private fun loopEtagLicenseList(
        userEtags: com.fwp.deloittedctcustomerapp.data.model.responses.etag.Etag,
        etagLicensesList: List<EtagLicenses>
    ) {
        for (etagLicensesIndex in userEtags.etagLicensesList!!.indices) {

            if (etagLicensesList[etagLicensesIndex].etagLicenseDetails!!.isEtagActive == EtagActiveStatus.YES) {
                etagLicensesList[etagLicensesIndex].etagLicenseDetails!!.downloadStatus!!.let {
                    allDownloadStatus.add(it)
                }
            }


            if (etagLicensesList[etagLicensesIndex].etagLicenseDetails!!.downloadStatus == DownloadStatus.SAME_DEVICE && etagLicensesList[etagLicensesIndex].etagLicenseDetails!!.validatedStatus == ValidateStatus.NOT_VALIDATED && etagLicensesList[etagLicensesIndex].etagLicenseDetails!!.isEtagActive == EtagActiveStatus.YES) {
                etagLicensesList[etagLicensesIndex].etagLicenseDetails!!.downloadStatus!!.let {
                    allDownloadStatusForOffLoad.add(it)
                }
                Log.e(TAG, "loopEtagLicenseList: offload  $allDownloadStatusForOffLoad ")
            }
            //for  offload

            if (etagLicensesList[etagLicensesIndex].etagLicenseDetails!!.downloadStatus == DownloadStatus.SAME_DEVICE && etagLicensesList[etagLicensesIndex].etagLicenseDetails!!.isEtagActive == EtagActiveStatus.YES) {
                etagLicensesList[etagLicensesIndex].etagLicenseDetails!!.spiId!!.let {
                    offLoadAllSpiIDLIst.add(it)
                }

            }

            //for all  download

            if (etagLicensesList[etagLicensesIndex].etagLicenseDetails!!.downloadStatus == DownloadStatus.NO_DOWNLOAD && etagLicensesList[etagLicensesIndex].etagLicenseDetails!!.isEtagActive == EtagActiveStatus.YES) {
                etagLicensesList[etagLicensesIndex].etagLicenseDetails!!.spiId!!.let {
                    downLoadAllSpiIDLIst.add(it)
                }
            }
        }
    }


    private fun clearList() {
        listCurrentYearDetail.clear()
        listPreviousYearDetail.clear()
        allCurrentSpiIdList.clear()
        allPreviousSpiIdList.clear()
        downLoadAllSpiIDLIst.clear()
        currentDownloadStatus.clear()
        previousDownloadStatus.clear()
        allDownloadStatus.clear()
        allDownloadStatusForOffLoad.clear()
        offLoadAllSpiIDLIst.clear()
        offLoadPreSpiIDLIst.clear()
    }

    // Setting recycler view
    private fun recyclerContent(
        previousList: List<Etag>,
        currentList: List<Etag>

    ) {

        binding.loader.clProgress.visibility = View.GONE

        binding.currentYearRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity?.applicationContext)
            val currentAdapter = EtagCurrentYearAdapter(currentList, offlineSpiid)
            adapter = currentAdapter

            currentAdapter.setOnItemClick(object :
                EtagCurrentYearAdapter.OnItemClickListener {
                override fun onItemClick(
                    position: Int,
                    holder: EtagCurrentYearAdapter.MyCurrentYearViewHolder
                ) {
                    if (currentDownloadStatus[position] == DownloadStatus.OTHER_DEVICE) {
                        showTagStateDialog(position = position, holder)
                    } else {
                        startCurrentDownloadProcess(position, holder)
                    }

                }
            })
        }

        binding.previousYearRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity?.applicationContext)
            val preAdapter = EtagPreviousYearAdapter(previousList, offlineSpiid)
            adapter = preAdapter

            preAdapter.setOnPreviousDownloadClick(object :
                EtagPreviousYearAdapter.OnPreviousClickListener {
                override fun onPreviousClick(
                    position: Int,
                    holder: EtagPreviousYearAdapter.MyPreviousYearViewHolder
                ) {

                    if (previousDownloadStatus[position] == DownloadStatus.OTHER_DEVICE) {
                        showPreTagStateDialog(position = position, holder)
                    } else {
                        startPreDownloadProcess(position, holder)
                    }

                }
            })
        }
    }


    fun startCurrentDownloadProcess(
        position: Int,
        holder: EtagCurrentYearAdapter.MyCurrentYearViewHolder
    ) {
        try {
            val downloadRequest = DownloadOffloadRequest(
                deviceUid = DEVICE_ID,
                service = UtilConstant.DOWNLOAD,
                spiId = listOf(allCurrentSpiIdList[position])
            )

            if (UtilsMethods.isInternetConnected(requireContext())) {
//                loadingDialog.startLoading()
                showCurrentProgressBtn(holder)
                viewModel.downloadRequest(downloadRequest)
                // Observing Response
                downloadObsr(allCurrentSpiIdList[position])
            } else {
//                UtilsMethods.toastMaker(requireContext(), "You are currently offline")
            }


        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (s: SocketTimeoutException) {
            loadingDialog.isDismiss()
            s.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        }
    }

    private fun startPreDownloadProcess(
        position: Int,
        holder: EtagPreviousYearAdapter.MyPreviousYearViewHolder
    ) {
        try {
            val downloadRequest = DownloadOffloadRequest(
                deviceUid = DEVICE_ID,
                service = UtilConstant.DOWNLOAD,
                spiId = listOf(allPreviousSpiIdList[position])
            )

            if (UtilsMethods.isInternetConnected(requireContext())) {
//                loadingDialog.startLoading()
                showPreviousProgressBtn(holder)
                viewModel.downloadRequest(downloadRequest)
                // Observing Response
                downloadObsr(allPreviousSpiIdList[position])
            } else {
//                UtilsMethods.toastMaker(requireContext(), "You are currently offline")
            }


        } catch (e: Exception) {
            loadingDialog.isDismiss()
            e.printStackTrace()
        } catch (s: SocketTimeoutException) {
            loadingDialog.isDismiss()
            s.printStackTrace()
        } catch (e3: SSLException) {
            loadingDialog.isDismiss()
            e3.printStackTrace()
        }
    }

    private fun showTagStateDialog(
        position: Int,
        holder: EtagCurrentYearAdapter.MyCurrentYearViewHolder
    ) {
        val builder = AlertDialog.Builder(requireContext()).create()
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.custom_dialog, null)
        val cancelBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = getString(R.string.other_download_primary)
        des.text = getString(R.string.other_download_secondary)
        cancelBtn.text = resources.getText(R.string.cancelText)
        sBtn.text = resources.getText(R.string.download)
        builder.setView(view)
        cancelBtn.setOnClickListener {
            builder.dismiss()
        }
        sBtn.setOnClickListener {
            startCurrentDownloadProcess(position, holder)
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    private fun showPreTagStateDialog(
        position: Int,
        holder: EtagPreviousYearAdapter.MyPreviousYearViewHolder
    ) {
        val builder = AlertDialog.Builder(requireContext()).create()
        val view = LayoutInflater.from(requireContext()).inflate(R.layout.custom_dialog, null)
        val cancelBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = getString(R.string.other_download_primary)
        des.text = getString(R.string.other_download_secondary)
        cancelBtn.text = resources.getText(R.string.cancelText)
        sBtn.text = resources.getText(R.string.download)
        builder.setView(view)
        cancelBtn.setOnClickListener {
            builder.dismiss()
        }
        sBtn.setOnClickListener {
            startPreDownloadProcess(position, holder)
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    private fun showCurrentProgressBtn(holder: EtagCurrentYearAdapter.MyCurrentYearViewHolder) {
        holder.iconProgressButton.visibility = View.VISIBLE
        holder.iconDownloadBtn.visibility = View.GONE
    }

    private fun showPreviousProgressBtn(holder: EtagPreviousYearAdapter.MyPreviousYearViewHolder) {
        holder.iconProgressButton.visibility = View.VISIBLE
        holder.iconDownloadBtn.visibility = View.GONE
    }

    @OptIn(InternalPlatformTextApi::class)
    private fun setSpannableString() {
        val spannable =
            SpannableString(resources.getString(R.string.buy_string))

        // Setting
        // font family
        val myTypeface =
            Typeface.create(
                ResourcesCompat.getFont(requireContext(), R.font.font_open_sens_bold),
                Typeface.BOLD
            )
        spannable.setSpan(
            TypefaceSpan(myTypeface),
            59,
            spannable.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )

        // Setting string
        // to textview
        binding.noEtagAvailable.textView11.text = spannable
        binding.buyAndApply.textView11.text = spannable
        binding.noEtagAvailable.noEtagPurchase.text = resources.getText(R.string.no_purchase_etag)
    }

    private fun buttonHandler() {
        val dismissBanner = SharedPrefs.read(UtilConstant.DISMISSIBLE_BANNER_ETAG, "")
        if (dismissBanner!!.isEmpty()) {
            binding.clToolTip.visibility = View.VISIBLE
            binding.downArrow.visibility = View.VISIBLE
        } else {
            binding.clToolTip.visibility = View.GONE
            binding.downArrow.visibility = View.GONE
        }
        binding.customPopup1.close.setOnClickListener {
            SharedPrefs.write(UtilConstant.DISMISSIBLE_BANNER_ETAG, "Pressed")
            binding.clToolTip.visibility = View.GONE
            binding.downArrow.visibility = View.GONE
        }
        // Intent to buy
        binding.noEtagAvailable.buyApplyButton.setOnClickListener {
            UtilsMethods.websiteOpener("https://fwp.mt.gov/buyandapply", requireActivity())
        }
        binding.buyAndApply.buyApplyButton.setOnClickListener {
            UtilsMethods.websiteOpener("https://fwp.mt.gov/buyandapply", requireActivity())
        }
        binding.buyAndApply.notSeeingYourPar.setOnClickListener {
            UtilsMethods.showDialerAlert(
                requireActivity(),
                requireContext(),
                "Not seeing your purchase?",
                "If you feel like an item you purchased is missing, call FWP support at 406-444-2950 during business hours (Monday – Friday, 8 a.m. – 5 p.m.)",
                "Close",
                resources.getString(R.string.callFwpText)
            )
        }
        binding.noEtagAvailable.notSeeingYourPar.setOnClickListener {
            UtilsMethods.showDialerAlert(
                requireActivity(),
                requireContext(),
                "Not seeing your purchase?",
                "If you feel like an item you purchased is missing, call FWP support at 406-444-2950 during business hours (Monday – Friday, 8 a.m. – 5 p.m.)",
                "Close",
                resources.getString(R.string.callFwpText)
            )
        }
        binding.more.setOnClickListener {
            showPopupMenu(binding.root.context, binding.more)
        }


        // download all
        binding.downloadAll.setOnClickListener {

            if (UtilsMethods.isInternetConnected(requireContext())) {
                if (allDownloadStatus.isNotEmpty()) {
                    if (allDownloadStatus.contains(DownloadStatus.NO_DOWNLOAD)) {
                        showDownloadTagStateDialog(
                            requireContext(),
                            "Do you want to download all tags to this device?",
                            "You should only download E-tags to the device you plan to bring in the field. \n" +
                                "\n" +
                                "By downloading all your E-tags, you are agreeing to make this your primary device.\n",
                            "No, cancel",
                            "Yes, download all",
                            downLoadAllSpiIDLIst
                        )
                    } else {
                        UtilsMethods.showSimpleAlert(
                            requireContext(),
                            "All E-tags currently downloaded",
                            "You have already downloaded all your E-tags to this device.",
                            "Ok"
                        )
                    }
                } else {
                    UtilsMethods.showDialerAlert(
                        requireActivity(),
                        requireContext(),
                        getString(R.string.you_have_no),
                        getString(R.string.you_have_no_des),
                        getString(R.string.cancelText),
                        getString(R.string.callFwpText)
                    )
                }


            } else {
                UtilsMethods.showNoNetwokDownloadDialog(requireContext())

            }


        }
    }

    private fun downloadObsr(spiIDLIst: String) {
        viewModel.downloadResponse.observe(viewLifecycleOwner) {
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    viewModel.getEtag(
                        EtagRequest(
                            DEVICE_ID
                        )
                    )
                    viewModel.insertDownloadTag(
                        DownloadedTags(
                            downloadedSpiId = spiIDLIst
                        )
                    )
                    it.resObject?.get(CURRENT_INDEX)
                        ?.let {
//                            it1.message?.get(CURRENT_INDEX)?.let { it2 ->
//                                UtilsMethods.toastMaker(
//                                    requireContext(),
//                                    it2
//                                )
//                            }
                        }
                }
                UtilConstant.FAIL_CODE -> {
                    it.resObject?.get(CURRENT_INDEX)
                        ?.let {
//                            it1.message?.get(CURRENT_INDEX)?.let { it2 ->
//                                UtilsMethods.toastMaker(
//                                    requireContext(),
//                                    it2
//                                )
//                            }
                        }
                }
            }
        }
    }

    private fun showPopupMenu(context: Context, view: View) {
        val wrapper: Context = ContextThemeWrapper(context, R.style.CustomPopUpStyle)
        val popup = PopupMenu(wrapper, view)
        popup.inflate(R.menu.popup_menu)
        popup.setOnMenuItemClickListener { item: MenuItem? ->
            when (item!!.itemId) {
                R.id.remove -> {

                    if (UtilsMethods.isInternetConnected(requireContext())) {

                        if (allDownloadStatusForOffLoad.contains(DownloadStatus.SAME_DEVICE)) {
                            showRemoveTagStateDialog(
                                context,
                                "Remove all downloaded E-tags?",
                                "You won’t be able to validate your E-tags unless they are downloaded to your device. \n" +
                                    "Do you want to remove all E-tags from this device?",
                                "Cancel",
                                "Remove all"
                            )
                        } else {

                            UtilsMethods.showSimpleAlert(
                                requireContext(),
                                "There are no E-tags to remove.",
                                "You don’t have any E-tags downloaded currently.",
                                "Ok"
                            )
                        }
                    } else {
                        UtilsMethods.showNoNetwokDialog(requireContext())

                    }

                }
            }

            true
        }

        popup.show()
    }

    private fun offloadObsr(idString: String) {
        viewModel.offloadResponse.observe(viewLifecycleOwner) {
            when (it.responseCode) {
                UtilConstant.SUCCESS_CODE -> {
                    viewModel.getEtag(
                        EtagRequest(
                            DEVICE_ID
                        )
                    )
                    viewModel.removeDownloadTagsById(idString)

                    it.resObject?.get(CURRENT_INDEX)
                        ?.let { it1 ->
                            viewModel.getEtag(
                                EtagRequest(
                                    DEVICE_ID
                                )
                            )
                            it1.message?.get(CURRENT_INDEX)?.let {
                                /*        UtilsMethods.toastMaker(
                                            requireContext(),
                                            it2
                                        )*/
                            }
                        }

                }
                UtilConstant.FAIL_CODE -> {
                    it.resObject?.get(CURRENT_INDEX)
                        ?.let {
//                            it1.message?.get(CURRENT_INDEX)?.let { it2 ->
//                                UtilsMethods.toastMaker(
//                                    requireContext(),
//                                    it2
//                                )
//                            }
                        }
                }
            }
        }
    }

    private fun showRemoveTagStateDialog(
        mContext: Context,
        tvPrimary: String,
        tvSecondary: String,
        cancelText: String,
        secText: String,
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val cancelBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = tvPrimary
        des.text = tvSecondary
        cancelBtn.text = cancelText
        sBtn.text = secText
        builder.setView(view)
        cancelBtn.setOnClickListener {
            builder.dismiss()
        }
        sBtn.setOnClickListener {

            try {

                loadingDialog.startLoading()

                val offloadRequest = DownloadOffloadRequest(
                    deviceUid = DEVICE_ID,
                    service = UtilConstant.OFFLOAD,
                    spiId = offLoadAllSpiIDLIst
                )

                viewModel.offLoadRequest(offloadRequest)

                // Observing Response
                offLoadAllSpiIDLIst.forEach {
                    offloadObsr(it)
                }


            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (s: SocketTimeoutException) {
                loadingDialog.isDismiss()
                s.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }

            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    private fun showDownloadTagStateDialog(
        mContext: Context,
        tvPrimary: String,
        tvSecondary: String,
        cancelText: String,
        secText: String,
        downLoadSpiIDLIst: MutableList<String>
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val cancelBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = tvPrimary
        des.text = tvSecondary
        cancelBtn.text = cancelText
        sBtn.text = secText
        builder.setView(view)
        cancelBtn.setOnClickListener {
            builder.dismiss()
        }
        sBtn.setOnClickListener {

            try {

                loadingDialog.startLoading()

                val downloadRequest = DownloadOffloadRequest(
                    deviceUid = DEVICE_ID,
                    service = UtilConstant.DOWNLOAD,
                    spiId = downLoadSpiIDLIst
                )

                viewModel.downloadRequest(downloadRequest)
                // Observing Response
                downLoadSpiIDLIst.forEach {
                    downloadObsr(it)
                }


            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (s: SocketTimeoutException) {
                loadingDialog.isDismiss()
                s.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }

            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    override fun onPause() {
        super.onPause()
        UtilConstant.backStatus = false
    }
}
