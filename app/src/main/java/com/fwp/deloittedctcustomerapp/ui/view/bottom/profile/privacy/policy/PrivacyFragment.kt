/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.privacy.policy


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.PrivacyFragmentBinding
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


class PrivacyFragment : BottomSheetDialogFragment() {

    companion object {
        fun newInstance() = PrivacyFragment()
    }

    private var _binding: PrivacyFragmentBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = PrivacyFragmentBinding.inflate(inflater, container, false)
        val root = binding.root

        // Init Process
        init()

        return root
    }

    private fun init() {

        val btnDownload = binding.download.btn
        btnDownload.text = resources.getString(R.string.download_privacy_policy_pdf)
        btnDownload.setTextColor(resources.getColor(R.color.black, null))
        btnDownload.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))

        buttonHandler()
    }

    private fun buttonHandler() {
        binding.back.setOnClickListener() {
            dismiss()
        }

        binding.download.btn.setOnClickListener() {

            UtilsMethods.downloadFile(
                requireActivity(),
                "https://mt.gov/1240-X06.pdf",
                "Montana Operations Manual Policy"
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
