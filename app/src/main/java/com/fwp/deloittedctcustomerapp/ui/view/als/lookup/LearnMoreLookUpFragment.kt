/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.als.lookup

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.LearnMoreLookUpFragmentBinding

import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint

// FWP
// Hilt Entry point
@AndroidEntryPoint
class LearnMoreLookUpFragment : Fragment() {

    // Global Variables
    private var _binding: LearnMoreLookUpFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = LearnMoreLookUpFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        // click listener
        buttonHandler()

        return root
    }
    override fun onPause() {
        UtilsMethods.hideKeyBoard(binding.root, requireContext())
        super.onPause()
    }

    private fun buttonHandler() {

        // Back to previous page
        binding.ivClose.setOnClickListener {
            UtilsMethods.backStack(findNavController())
        }

        // intent to look up als frag
        binding.tvLookAls.setOnClickListener {
            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.lookUpKnowMoreFragment_to_lookUpAlsFragment
            )

        }
    }
}
