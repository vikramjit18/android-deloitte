/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fwp.deloittedctcustomerapp.data.db.AppDao
import com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
import com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.masterData.MasterData
import com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.UserStatusResponse
import com.fwp.deloittedctcustomerapp.data.repo.MainRepo
import com.fwp.deloittedctcustomerapp.utils.Event
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class LandingScreenActivityViewModel @Inject constructor(
    private val mainRepo: MainRepo,
    private val appDao: AppDao
) :
    ViewModel() {

    val ownerName = MutableLiveData<String>()
    var isMultiAccount = MutableLiveData<Boolean>()

    private val _checkUserStatusResponse = MutableLiveData<UserStatusResponse>()
    val checkUserStatusResponse: LiveData<UserStatusResponse>
        get() = _checkUserStatusResponse

    private val _viewUserProfileResponse = MutableLiveData<UserProfileResponse>()
    val viewUserProfileResponse: LiveData<UserProfileResponse>
        get() = _viewUserProfileResponse

    private val _etagResponse = MutableLiveData<EtagResponse>()
    val etagResponse: LiveData<EtagResponse>
        get() = _etagResponse

    private val _masterDataResponse = MutableLiveData<MasterData>()
    val masterDataResponse: LiveData<MasterData>
        get() = _masterDataResponse

    private val _offloadResponse = MutableLiveData<DownloadOffloadResponse>()
    val offloadResponse: LiveData<DownloadOffloadResponse>
        get() = _offloadResponse

    private val _downloadResponse = MutableLiveData<DownloadOffloadResponse>()
    val downloadResponse: LiveData<DownloadOffloadResponse>
        get() = _downloadResponse


    private val _toastViewUser = MutableLiveData<Event<String>>()
    val toastViewUser: LiveData<Event<String>>
        get() = _toastViewUser

    private val _toastMasterUSer = MutableLiveData<Event<String>>()
    val toastMasterUSer: LiveData<Event<String>>
        get() = _toastMasterUSer

    private val _toastCheckUser = MutableLiveData<Event<String>>()
    val toastCheckUser: LiveData<Event<String>>
        get() = _toastCheckUser

    private val _getItemHeldResponse = MutableLiveData<ItemsHeldResponse>()
    val getItemHeldResponse: LiveData<ItemsHeldResponse>
        get() = _getItemHeldResponse

    private val _toastGetEtag = MutableLiveData<Event<String>>()
    val toastGetEtag: LiveData<Event<String>>
        get() = _toastGetEtag

    private val _toastItemHeld = MutableLiveData<Event<String>>()
    val toastItemHeld: LiveData<Event<String>>
        get() = _toastItemHeld

    private val _toastDownloadOffload = MutableLiveData<Event<String>>()
    val toastDownloadOffload: LiveData<Event<String>>
        get() = _toastDownloadOffload
    private val _dbDownloadOffload = MutableLiveData<String>()
    val dbDownloadOffloadRes: LiveData<String>
        get() = _dbDownloadOffload

    val coroutineExceptionHandler = CoroutineExceptionHandler { _, t -> t.printStackTrace() }

    fun cleardb() {
        viewModelScope.launch {
            appDao.deleteValidateRequest()
        }

    }

    fun getDownloadedTagSid(): LiveData<List<DownloadedTags>> {
        return appDao.getDownloadedTags()
    }

    fun deleteDownloadedTagSid() {
        appDao.deleteDownloadedTagsTable()
    }

    fun insertDownloadTag(downloadedTags: DownloadedTags) {
        viewModelScope.launch {
            appDao.insertDownloadTags(downloadedTags)
        }
    }

    fun removeDownloadTagsById(onlineSpiId: String) {
        viewModelScope.launch {
            appDao.deleteDownloadedTagsById(onlineSpiId = onlineSpiId)
        }
    }

    fun setOwnerName(name: String) {
        ownerName.value = name
    }

    fun setMultiAccountBool(bool : Boolean){
        isMultiAccount.value = bool
    }

    fun getItemHeldResponse() {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.getItemHeld(SharedPrefs.read(UtilConstant.JWT_TOKEN, "")!!)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful && response.body() != null) {
                    when (response.body()!!.responseMessage) {
                        UtilConstant.SUCCESS_MSG -> {
                            itemSuccesRes(response)
                        }
                    }
                } else {
                    _toastItemHeld.value = Event(response.message())
                }
            }
        }
    }

    private fun itemSuccesRes(response: Response<ItemsHeldResponse>) {
        when (response.body()!!.responseCode) {
            UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> _getItemHeldResponse.value =
                response.body()

            UtilConstant.ERROR_CODE -> _toastItemHeld.value =
                response.body()!!.resObject?.get(0)?.let { Event(it.toString()) }
        }
    }

    fun checkUserStatus(token: String) {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.checkUserStatus(token)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful && response.body() != null) {
                    checkUserSuccessRes(response)
//                    when (response.body()!!.responseMessage) {
//
//                        UtilConstant.SUCCESS_MSG -> {
//
//                        }
//
//                        else -> _toastCheckUser.value =
//                            Event(response.body()!!.resObject[0].toString())
//                    }
                } else {
                    _toastCheckUser.value = Event(response.message())
                }
            }
        }
    }

    private fun checkUserSuccessRes(response: Response<UserStatusResponse>) {
        _checkUserStatusResponse.value =
            response.body()

//        when (response.body()!!.responseCode) {
//
//            UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> _checkUserStatusResponse.value =
//                response.body()
//
//            UtilConstant.ERROR_CODE -> _toastCheckUser.value =
//                Event(response.body()!!.resObject[0].toString())
//        }
    }


    fun getMasterData() {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.getMasterData()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful && response.body() != null) {
                    when (response.body()!!.responseMessage) {
                        UtilConstant.SUCCESS_MSG -> {
                            getMasterSuccessRes(response)
                        }
                        else -> _toastMasterUSer.value =
                            Event(response.body()!!.resObject[0].toString())
                    }

                } else {
                    _toastMasterUSer.value = Event(response.message())
                }
            }
        }

    }

    private fun getMasterSuccessRes(response: Response<MasterData>) {
        when (response.body()!!.responseCode) {
            UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> _masterDataResponse.value =
                response.body()

            UtilConstant.ERROR_CODE -> _toastMasterUSer.value =
                Event(response.body()!!.resObject[0].toString())
        }
    }

    fun getUserProfile() {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val token = SharedPrefs.read(UtilConstant.JWT_TOKEN, "")
            val response = token?.let { mainRepo.getViewUserProfileDetails(it) }
            withContext(Dispatchers.Main) {
                if (response!!.isSuccessful) {
                    when (response.body()!!.responseMessage) {
                        UtilConstant.SUCCESS_MSG -> {
                            getUserSuccessRes(response)
                        }
                    }
                } else {
                    _toastViewUser.value = Event(response.message())
                }
            }
        }
    }

    private fun getUserSuccessRes(response: Response<UserProfileResponse>) {
        when (response.body()!!.responseCode) {

            UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> _viewUserProfileResponse.value =
                response.body()

            UtilConstant.ERROR_CODE -> _toastViewUser.value =
                response.body()!!.resObject?.get(0)?.let { Event(it.toString()) }
        }
    }

    fun getEtag(etagRequest: EtagRequest) {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.getEtags(etagRequest)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {


                    when (response.body()!!.responseCode) {

                        UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE
                        -> _etagResponse.value = response.body()

                        UtilConstant.ERROR_CODE -> _toastViewUser.value =
                            response.body()!!.resObject?.get(0)?.let { Event(it.toString()) }
                    }


                } else {
                    _toastGetEtag.value = Event(response.message())
                }
            }
        }
    }

    fun offLoadRequest(downloadOffloadRequest: DownloadOffloadRequest) {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.downloadOffLoadEtag(downloadOffloadRequest)

            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    when (response.body()!!.responseCode) {

                        UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE
                        -> _offloadResponse.value = response.body()

                        UtilConstant.ERROR_CODE -> _toastViewUser.value =
                            response.body()!!.resObject?.get(0)?.let { Event(it.toString()) }
                    }

                } else {
                    _toastDownloadOffload.value = Event(response.message())
                }
            }
        }
    }

    fun downloadRequest(downloadOffloadRequest: DownloadOffloadRequest) {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.downloadOffLoadEtag(downloadOffloadRequest)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {

                    when (response.body()!!.responseCode) {

                        UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE
                        -> _downloadResponse.value = response.body()

                        UtilConstant.ERROR_CODE -> _toastViewUser.value =
                            response.body()!!.resObject?.get(0)?.let { Event(it.toString()) }
                    }

                } else {
                    _toastDownloadOffload.value = Event(response.message())
                }
            }
        }
    }

}
