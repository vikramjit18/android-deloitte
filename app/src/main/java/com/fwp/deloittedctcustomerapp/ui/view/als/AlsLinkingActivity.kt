/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.als

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import dagger.hilt.android.AndroidEntryPoint


// FWP
// Hilt entry point
@AndroidEntryPoint
class AlsLinkingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setting
        // XMl layout
        setContentView(R.layout.activity_als_linking)
        SharedPrefs.init(this)
    }

    override fun onBackPressed() {

    }
}
// End of Kt.
