/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.SecurityFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password.ResetPasswordFragment
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods

class SecurityFragment : Fragment() {

    companion object {
        fun newInstance() = SecurityFragment()
    }

    private var resetPasswordFragment: ResetPasswordFragment? = null
    private var _binding: SecurityFragmentBinding? = null
    private val binding get() = _binding!!
    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SecurityFragmentBinding.inflate(inflater, container, false)
        val root = binding.root

        init()
        buttonHandler()
        return root
    }


    private fun init() {
        val passImg = binding.itemPass.itemImage
        val passText = binding.itemPass.itemText
        val infoImg = binding.itemInfo.itemImage
        val infoText = binding.itemInfo.itemText
        val locImg = binding.itemLocation.itemImage
        val locText = binding.itemLocation.itemText
        binding.itemPass.itemEndImage.visibility = View.INVISIBLE
        binding.itemInfo.itemEndImage.visibility = View.INVISIBLE
        binding.itemLocation.itemEndImage.visibility = View.INVISIBLE
        passImg.setImageResource(R.drawable.ic_key)
        passText.setText(R.string.passwordText)
        infoImg.setImageResource(R.drawable.ic_login_info)
        infoText.setText(R.string.LoginInfoText)
        locImg.setImageResource(R.drawable.ic_location)
        locText.setText(R.string.locationText)
    }

    private fun buttonHandler() {
        binding.back.backLayout.setOnClickListener {
            UtilsMethods.backStack(findNavController())
        }
        binding.itemPassL.setOnClickListener {
            UtilsMethods.navigateToFragment(binding.root, R.id.resetPasswordFragment)
/*
            resetPasswordFragment = ResetPasswordFragment.newInstance()
            resetPasswordFragment?.show(childFragmentManager, resetPasswordFragment?.tag)*/
        }
        binding.itemInfoL.setOnClickListener {
            UtilsMethods.navigateToFragment(
                binding.root,
                R.id.securityFragment_to_loginInfoFragment
            )
        }
        binding.itemLocationL.setOnClickListener {
            UtilsMethods.navigateToFragment(binding.root, R.id.securityFragment_to_locationFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
