/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.notify


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.NotificationFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.als.AlsLinkingActivity
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods

class NotificationFragment : Fragment() {
    companion object {
        fun newInstance() = NotificationFragment()
    }

    private var _binding: NotificationFragmentBinding? = null
    private val binding get() = _binding!!
    private var isPauseAllEnable = false

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = NotificationFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root

        buttonHandler()
        return root

    }

    override fun onResume() {
        super.onResume()
        isAlsLinked()
    }

    private fun isAlsLinked() {
        when (UtilConstant.isAlsLinked) {

            true -> binding.alsNotLinkedNotifyContainer.visibility = View.GONE
            false -> binding.alsNotLinkedNotifyContainer.visibility = View.VISIBLE
        }
    }

    private fun buttonHandler() {
        binding.back.backLayout.setOnClickListener() {
            UtilsMethods.backStack(findNavController())
        }

        binding.enablePauseAll.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                disableAllNotification()
            }
        }

        binding.enableEtagPush.setOnCheckedChangeListener { _, isChecked ->

            disablePauseAllSwitch(isChecked)

        }

        binding.enableEtagsValidation.setOnCheckedChangeListener { _, isChecked ->
            disablePauseAllSwitch(isChecked)

        }

        binding.enableReporting.setOnCheckedChangeListener { _, isChecked ->
            disablePauseAllSwitch(isChecked)
        }

        binding.getStared.setOnClickListener() {
            val i = Intent(activity, AlsLinkingActivity::class.java)
            startActivity(i)
            (activity as Activity?)!!.overridePendingTransition(0, 0)
        }
    }

    private fun disablePauseAllSwitch(state: Boolean) {

        if (state)
            binding.enablePauseAll.isChecked = false
    }

    private fun disableAllNotification() {
        binding.enableEtagPush.isChecked = false
        binding.enableEtagsValidation.isChecked = false
        binding.enableReporting.isChecked = false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
