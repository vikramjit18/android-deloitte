/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.signup


import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.*
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R

import com.fwp.deloittedctcustomerapp.data.model.requests.CheckEmailRequest
import com.fwp.deloittedctcustomerapp.databinding.SignUpDetailsFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException

/**
 * FWP
 * Hilt entry point
 */
@AndroidEntryPoint
class SignUpDetails : Fragment() {

    /**
     * Global variables
     */
    lateinit var binding: SignUpDetailsFragmentBinding
    private val model: SignUpViewModel by viewModels()
    private lateinit var viewModelCommunicator: Communicator
    private lateinit var loadingDialog: LoadingDialog

    override fun onResume() {
        super.onResume()

        loadingDialog = LoadingDialog(requireActivity())
        /**
         * initiate
         * communicator view modal
         */
        viewModelCommunicator = ViewModelProvider(requireActivity())[Communicator::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        /**
         * Binding Initiate
         */
        binding =
            DataBindingUtil.inflate(inflater, R.layout.sign_up_details_fragment, container, false)


        /**
         * Returning
         * root binding
         */
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * Globals functions
         */
        init()
        buttonHandler()
        buttonActivation()

        /**
         * Observing
         * New User Response
         */
        viewModelObser()


    }

    private fun init() {
        /*
         * First name
         */
        binding.etFirstName.editText.inputType =
            InputType.TYPE_TEXT_VARIATION_PERSON_NAME or InputType.TYPE_TEXT_FLAG_CAP_WORDS

        /*
         *  Middle name
         */
        binding.etMiddleName.editText.inputType =
            InputType.TYPE_TEXT_VARIATION_PERSON_NAME or InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
        binding.etMiddleName.editText.filters += InputFilter.LengthFilter(1)

        /**
         *  Last name
         */
        binding.etLastName.editText.inputType =
            InputType.TYPE_TEXT_VARIATION_PERSON_NAME or InputType.TYPE_TEXT_FLAG_CAP_WORDS

        /*
         * TextWatch listener
         * for any changes
         */
        binding.email.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.email.addTextChangedListener(mTextWatcher)
            } else {
                binding.emailLayout.isErrorEnabled = false
            }
        }
        binding.confirmEmail.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                binding.confirmEmail.addTextChangedListener(mTextWatcher)
            } else {
                binding.aonEmailLayout.isErrorEnabled = false
            }
        }


        binding.etFirstName.editText.addTextChangedListener(mTextWatcher)
        binding.etLastName.editText.addTextChangedListener(mTextWatcher)
    }

    /**
     * Button handler function
     */
    private fun buttonHandler() {
        binding.back.setOnClickListener {
            UtilsMethods.backStack(findNavController())
        }
    }

    /**
     * Initiate
     * button activation function
     */
    private fun buttonActivation() {
        binding.next.btn.text = resources.getString(R.string.nextButtonText)
        val signUpButtonId = binding.next.btn
        signUpButtonId.text = resources.getString(R.string.nextButtonText)
        val email: String = binding.email.text.toString()
        val confirmEmail: String = binding.confirmEmail.text.toString()
        val firstName: String = binding.etFirstName.editText.text.toString()
        val lastName: String = binding.etLastName.editText.text.toString()

        Log.e("passkey and username ", "${email}, $confirmEmail $firstName $lastName")
        when ((firstName.isNotEmpty() && lastName.isNotEmpty()) && UtilConstant.EMAIL_ADDRESS_PATTERN.matcher(
            email.trim()
        ).matches() && email.trim() == confirmEmail.trim()) {
            true -> {
                if (binding.email.editableText.toString()
                        .trim() == binding.confirmEmail.editableText.toString().trim()
                ) {
                    buttonEnable()
                    binding.next.btn.setOnClickListener {
                        apiHitSignUpEmailCheck()


                    }
                } else {
                    /**
                     * Disabling primary btn
                     */
                    buttonDisable()
                }
            }

            false -> buttonDisable()
        }
    }

    private fun apiHitSignUpEmailCheck() {
        if (UtilsMethods.isInternetConnected(requireContext())) {
            /**
             * showing
             * progress loader
             */
            loadingDialog.startLoading()

            /**
             * Initiate
             * new user signup Api
             */
            try {
                model.checkExistingEmail(
                    checkEmailRequest = CheckEmailRequest(
                        emailids = binding.confirmEmail.editableText.toString()
                            .trim(),
                    )
                )
            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (e2: SocketTimeoutException) {
                loadingDialog.isDismiss()
                e2.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }

        } else {

            UtilsMethods.showSimpleAlert(
                mContext = requireContext(),
                tvPrimary = getString(R.string.no_connection),
                tvSecondary = getString(R.string.check_and_try_again),
                primaryBtn = resources.getString(R.string.ok)

            )
        }
    }

    private fun viewModelObser() {
        model.existingEmailResponse.observe(viewLifecycleOwner) {
            /**
             * hiding
             * progress loader
             */
            loadingDialog.isDismiss()


            if (it.responseCode == UtilConstant.SUCCESS_CODE) {
                /**
                 * Alert dialog
                 * for existing email
                 */
                showActivityNavigationAlert(
                    requireContext(),
                    "Email already in use",
                    "The email you entered is already connected with a MyFWP account. Do you want to try to log in?",
                    "Log in",
                    "Try again"
                )

            } else {
                // Navigate to signup agreement fragment


                viewModelCommunicator.setEmailCommunicator(
                    binding.confirmEmail.editableText.toString().trim()
                )
                viewModelCommunicator.setFNCommunicator(
                    binding.etFirstName.editText.editableText.toString()
                        .trim()
                )
                viewModelCommunicator.setMNCommunicator(
                    binding.etMiddleName.editText.editableText.toString()
                        .trim()
                )
                viewModelCommunicator.setLNCommunicator(
                    binding.etLastName.editText.editableText.toString()
                        .trim()
                )

                UtilsMethods.navigateToFragment(
                    binding.root,
                    R.id.action_signUpDetails_to_signUpAgreement2
                )
            }
        }

        model.toastExistingEmail.observe(viewLifecycleOwner, { it ->
            it.getContentIfNotHandledOrReturnNull()?.let { message ->
//                UtilsMethods.toastMaker(requireContext(), message)
//                Log.e("Error in existing email", "viewModelObser: ")
            }
        })
    }


    private fun showActivityNavigationAlert(
        mContext: Context,
        tvPrimary: String,
        tvSecondary: String,
        leftBtn: String,
        rightBtn: String
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val lBtn = view.findViewById<TextView>(R.id.btn_left)
        val rBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = tvPrimary
        des.text = tvSecondary
        lBtn.text = leftBtn
        rBtn.text = rightBtn
        builder.setView(view)
        lBtn.setOnClickListener {
            builder.dismiss()
            requireActivity().finish()
            requireActivity().overridePendingTransition(
                R.anim.slide_bottom_to_top,
                R.anim.slide_top_to_bottom
            )
        }
        rBtn.setOnClickListener {

            builder.dismiss()
        }

        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    /**
     * Button
     * disable function
     */
    private fun buttonDisable() {
        binding.next.btn.isEnabled = false
        binding.next.btn.isClickable = false
        binding.next.btn.setTextColor(resources.getColor(R.color.white, null))
        binding.next.btn.setBackgroundColor(
            resources.getColor(
                R.color.disableButtonColor,
                null
            )
        )
    }

    /**
     * Button
     * enable function
     */
    private fun buttonEnable() {
        binding.next.btn.isEnabled = true
        binding.next.btn.isClickable = true
        binding.aonEmailLayout.isErrorEnabled = false
        binding.next.btn.setTextColor(resources.getColor(R.color.black, null))
        binding.next.btn.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
    }

    /**
     * create a
     * textWatcher member
     */
    private val mTextWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i2: Int, i3: Int) {
            //not needed
            emailMatchValidator()
            emailValidator()

        }

        override fun afterTextChanged(editable: Editable) {
            buttonActivation()
        }


    }


    private fun emailMatchValidator() {
        val emailL = binding.emailLayout
        val emailLcon = binding.aonEmailLayout
        val email: String = binding.email.text.toString()
        val emailCon: String = binding.confirmEmail.text.toString()
        val errorDrawable = ContextCompat.getDrawable(requireContext(), R.drawable.ic_error)
        errorDrawable!!.setBounds(0, 5, 40, 40)

//when(!email.isNullOrEmpty()){
//    true->      when(email.contains("@")){
//
//        true ->    when( !UtilConstant.EMAIL_ADDRESS_PATTERN.matcher(email).matches()) {
//            true -> {
//                emailL.isErrorEnabled = true
//                emailL.error = "Enter a valid email address"
//            }
//            false -> {
//                emailL.isErrorEnabled = false
//            }
//
//        }
//        false ->   emailLcon.isErrorEnabled = false
//    }
//    false ->  emailL.isErrorEnabled = false
//}
        when (email.trim() == emailCon.trim()) {
            true -> {
                emailLcon.isErrorEnabled = false
            }
            false -> {
                emailLcon.isErrorEnabled = true
                emailLcon.error = "Email addresses must match"

            }
        }
    }

    private fun emailValidator() {
        val emailL = binding.emailLayout
        val emailLcon = binding.aonEmailLayout
        val email: String = binding.email.text.toString()
        val emailCon: String = binding.confirmEmail.text.toString()
        when (email.isNotEmpty() && email.contains("@") && !UtilConstant.EMAIL_ADDRESS_PATTERN.matcher(
            email.trim()
        ).matches()) {
            true -> {
                emailL.isErrorEnabled = true
                emailL.error = "Enter a valid email address"

            }
            false -> {
                emailL.isErrorEnabled = false
            }

        }
    }
}

