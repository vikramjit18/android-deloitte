/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.multiple

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.MultipleAccountFragmentBinding
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods

private const val TAG = "MultipleAccountFragment"

class MultipleAccountFragment : Fragment() {

    companion object {
        fun newInstance() = MultipleAccountFragment()
    }

    private var _binding: MultipleAccountFragmentBinding? = null
    private val binding get() = _binding!!
    private var name: String? = null
    private var alsNumber: String? = null
    private var linked: String? = null
    private var resident: String? = null
    private var extras: Bundle? = null

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = MultipleAccountFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        buttonHandler()

        // Getting bundle value
        name = arguments?.getString(UtilConstant.MULTI_NAME)
        alsNumber = arguments?.getString(UtilConstant.MULTI_ALS_NO)
        linked = arguments?.getString(UtilConstant.MULTI_LINKED)
        resident = arguments?.getString(UtilConstant.MULTI_Resident)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setting UI
        binding.multiName.text = name
        binding.multiALsNo.text = alsNumber
        binding.linked.text = linked

        when (resident) {
            "R" -> binding.resident.text = resources.getString(R.string.resident)
            "N" -> binding.resident.text = resources.getString(R.string.nonresident)
        }
    }

    private fun buttonHandler() {
        binding.back.backLayout.setOnClickListener {
            UtilsMethods.backStack(findNavController())
        }

        binding.closeMy.setOnClickListener {
            binding.unlinkContainer.visibility = View.GONE
        }
        binding.myFwpGoto.setOnClickListener {
            UtilsMethods.websiteOpener("https://fwp.mt.gov/", requireActivity())
        }

    }
}
