/*
 * Montana Fish, Wildlife & Parks
 */

/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopyPrimaryRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopySecondaryRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse
import com.fwp.deloittedctcustomerapp.data.repo.MainRepo
import com.fwp.deloittedctcustomerapp.utils.Event
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class RequestEmailViewModel @Inject constructor(private val mainRepo: MainRepo) : ViewModel() {

    private val _requestCopyEmailResponse = MutableLiveData<RequestEmailResponse>()
    val requestCopyEmailResponse: LiveData<RequestEmailResponse>
        get() = _requestCopyEmailResponse


    private val _toast = MutableLiveData<Event<String>>()
    val toast: LiveData<Event<String>>
        get() = _toast

    fun requestEmailPrimary(jwtToken: String, requestCopyPrimaryRequest: RequestCopyPrimaryRequest) {
        viewModelScope.launch {
            val response = mainRepo.requestEmailCopyPrimary(jwtToken, requestCopyPrimaryRequest)
            if (response.isSuccessful) {
                when (response.body()!!.responseMessage) {
                    UtilConstant.SUCCESS_MSG -> {
                        requestResponse(response)
                    }
                }

            } else {
                _toast.value = Event(response.errorBody().toString())
            }

        }
    }


    fun requestEmailSecondary(jwtToken: String, requestCopySecondaryRequest: RequestCopySecondaryRequest) {
        viewModelScope.launch {
            val response = mainRepo.requestEmailCopySecondary(jwtToken, requestCopySecondaryRequest)
            if (response.isSuccessful) {
                when (response.body()!!.responseMessage) {
                    UtilConstant.SUCCESS_MSG -> {
                        requestResponse(response)
                    }
                }

            } else {
                _toast.value = Event(response.errorBody().toString())
            }

        }
    }

    private fun requestResponse(response: Response<RequestEmailResponse>) {
        when (response.body()!!.responseCode) {
            UtilConstant.SUCCESS_CODE -> _requestCopyEmailResponse.value =
                response.body()

            UtilConstant.FAIL_CODE -> _toast.value =
                response.body()!!.resObject?.get(0)?.let { Event(it.message.toString()) }
        }
    }
}
