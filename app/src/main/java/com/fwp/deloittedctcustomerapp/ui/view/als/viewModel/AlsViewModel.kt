/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.als.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fwp.deloittedctcustomerapp.data.model.requests.AlsLinkingRequest
import com.fwp.deloittedctcustomerapp.data.model.requests.AlsLookupRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse
import com.fwp.deloittedctcustomerapp.data.repo.MainRepo
import com.fwp.deloittedctcustomerapp.utils.Event
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class AlsViewModel @Inject constructor(private val mainRepo: MainRepo) : ViewModel() {

    private val _alsLinkingResponse = MutableLiveData<AlsResponse>()
    val alsLinkingResponse: LiveData<AlsResponse>
        get() {
            return _alsLinkingResponse
        }

    private val _alsLookupResponse = MutableLiveData<AlsResponse>()
    val alsLookupResponse: LiveData<AlsResponse>
        get() {
            return _alsLookupResponse
        }

    private val _toast = MutableLiveData<Event<String>>()
    val toast: LiveData<Event<String>>
        get() = _toast

    val coroutineExceptionHandler = CoroutineExceptionHandler { _, t -> t.printStackTrace() }

    // als link
    fun alsLookUp(token: String, alsLookupRequest: AlsLookupRequest) {
        GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.lookUpAls(token, alsLookupRequest)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful && response.body() != null) {
                    when (response.body()!!.responseMessage) {
                        UtilConstant.SUCCESS_MSG -> {
                            successLookUpRes(response)
                        }

                        else -> {
                            _toast.value =
                                Event(response.body()!!.resObject[0].toString())
                        }
                    }
                } else {
                    _toast.value = Event(response.message())
                }

            }
        }

    }

    //als lookup
    fun alsLinking(token: String, alsLinkingRequest: AlsLinkingRequest) {
        GlobalScope.launch (Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.linkAls(token, alsLinkingRequest)

            withContext(Dispatchers.Main) {
            if (response.isSuccessful && response.body() != null) {
                successLinkIngRes(response)
            } else {
                _toast.value = Event(response.errorBody().toString())
            }

        }

    }}

    private fun successLookUpRes(response: Response<AlsResponse>) {
        when (response.body()!!.responseCode) {

            UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> {
                _alsLookupResponse.value = response.body()
            }

            UtilConstant.ERROR_CODE -> _toast.value =
                Event(response.body()!!.resObject[0].toString())

        }
    }

    private fun successLinkIngRes(response: Response<AlsResponse>) {
        when (response.body()!!.responseCode) {

            UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE -> {
                _alsLinkingResponse.value = response.body()
            }

            UtilConstant.ERROR_CODE -> _toast.value =
                Event(response.body()!!.resObject[0].toString())

        }
    }

}
