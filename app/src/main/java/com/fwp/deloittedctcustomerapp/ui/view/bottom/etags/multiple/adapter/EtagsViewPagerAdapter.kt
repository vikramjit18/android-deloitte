/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.viewPager.LinkedAccountEtagsFragment
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.EtagsAvailableFragment
import com.fwp.deloittedctcustomerapp.utils.UtilConstant

class EtagsViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
//        UtilConstant.ACCOUNT_VIEWPAGER = position
        return when (position) {
            0 -> {
               // UtilConstant.ACCOUNT_VIEWPAGER=0
                EtagsAvailableFragment()

            }
            1 -> {
               // UtilConstant.ACCOUNT_VIEWPAGER=1
                LinkedAccountEtagsFragment()
            }
            else -> Fragment()
        }
    }
}
