/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.multiple

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.MultiUserItemsFragmnetFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.multiple.adapter.ItemsViewPagerAdapter
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.google.android.material.tabs.TabLayoutMediator

class MultiUserItemsFragmnet : Fragment() {

    companion object {
        fun newInstance() = MultiUserItemsFragmnet()
    }

    private lateinit var binding: MultiUserItemsFragmnetFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.multi_user_items_fragmnet_fragment,
            container,
            false
        )
        settingViewPagerView()
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        Handler(Looper.getMainLooper()).post {
            binding.viewPager.setCurrentItem(
                UtilConstant.ITEMS_LAST_VIEW_INDEX,
                true
            )
        }
    }

    private fun settingViewPagerView() {
        binding.viewPager.adapter =
            ItemsViewPagerAdapter(this.childFragmentManager, lifecycle)


        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->

            when (position) {
                0 ->
                    tab.text = "My Items"
                1 ->
                    tab.text = "Linked Items"
            }
        }.attach()
    }
}
