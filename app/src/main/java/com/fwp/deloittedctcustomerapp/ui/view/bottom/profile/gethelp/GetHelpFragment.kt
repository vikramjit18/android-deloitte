/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.gethelp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.GetHelpFragmentBinding
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods


class GetHelpFragment : Fragment() {

    companion object {
        fun newInstance() = GetHelpFragment()
    }

    private val viewModel: GetHelpViewModel by viewModels()
    private var _binding: GetHelpFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return if (enter) {
            AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
        } else {
            AnimationUtils.loadAnimation(context, android.R.anim.slide_out_right)
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = GetHelpFragmentBinding.inflate(inflater, container, false)
        val root = binding.root
        buttonHandler()
        return root
    }

    private fun buttonHandler() {
        binding.back.backLayout.setOnClickListener() {
            UtilsMethods.backStack(findNavController())
        }
        linkHandle()
        phoneHandle()
    }

    private fun phoneHandle() {
        binding.contactNumber.setOnClickListener {
            UtilsMethods.dialerOpener(getString(R.string.officeNo), requireActivity())
        }
        binding.contactNumberReport.setOnClickListener {
            UtilsMethods.dialerOpener("800-TIP-MONT", requireActivity())
        }
    }

    private fun linkHandle() {
        binding.website.setOnClickListener {
            UtilsMethods.websiteOpener("https://fwp.mt.gov/aboutfwp/contact-us", requireActivity())
        }
        binding.websiteReport.setOnClickListener {
            UtilsMethods.websiteOpener("https://myfwp.mt.gov/fwpPub/tipmont", requireActivity())
        }
        binding.region1.setOnClickListener {
            UtilsMethods.websiteOpener(getString(R.string.region_1_link), requireActivity())
        }
        binding.region2.setOnClickListener {
            UtilsMethods.websiteOpener(getString(R.string.region_2_link), requireActivity())
        }
        binding.region3.setOnClickListener {
            UtilsMethods.websiteOpener(getString(R.string.region_3_link), requireActivity())
        }
        binding.region4.setOnClickListener {
            UtilsMethods.websiteOpener(getString(R.string.region_4_link), requireActivity())
        }
        binding.region5.setOnClickListener {
            UtilsMethods.websiteOpener(getString(R.string.region_5_link), requireActivity())
        }
        binding.region6.setOnClickListener {
            UtilsMethods.websiteOpener(getString(R.string.region_6_link), requireActivity())
        }
        binding.region7.setOnClickListener {
            UtilsMethods.websiteOpener(getString(R.string.region_7_link), requireActivity())
        }
    }


}
