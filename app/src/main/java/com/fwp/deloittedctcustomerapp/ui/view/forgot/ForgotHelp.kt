/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.forgot

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.compose.ui.text.android.InternalPlatformTextApi
import androidx.compose.ui.text.android.style.TypefaceSpan
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.ForgotHelpFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods

class ForgotHelp : Fragment() {


    private lateinit var binding: ForgotHelpFragmentBinding
    private lateinit var viewModelCommunicator: Communicator


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.forgot_help_fragment, container, false)
        viewModelCommunicator = ViewModelProvider(requireActivity())[Communicator::class.java]
        buttonProperty()
        buttonHandler()
        setEmail()
        return binding.root
    }

    private fun buttonHandler() {
        binding.tvHavingTrouble.setOnClickListener {
            showAlert()
        }
        binding.ivClose.setOnClickListener {
//            UtilsMethods.backStack(findNavController())
            requireActivity().finish()
            requireActivity().overridePendingTransition(
                R.anim.slide_bottom_to_top, R.anim.slide_top_to_bottom
            )
        }

        binding.tvNextScreen.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.frogot2_to_forgot3)
        }
        binding.emailApp.btn.setOnClickListener {
            UtilsMethods.openEmailApp(requireActivity())
        }
    }

    @OptIn(InternalPlatformTextApi::class)
    private fun showAlert() {

        val builder = AlertDialog.Builder(requireContext()).create()
        val view = layoutInflater.inflate(R.layout.custom_dialog, null)
        val leftButton = view.findViewById<TextView>(R.id.btn_left)
        val rightButton = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = getString(R.string.notReceiveEmailTitle)
        des.text = getString(R.string.notReceiveEmailDes)
        leftButton.text = getString(R.string.cancelText)
        rightButton.text = getString(R.string.callFwpText)
        builder.setView(view)
        leftButton.setOnClickListener {
            builder.dismiss()
        }
        rightButton.setOnClickListener() {

            UtilsMethods.dialerOpener(getString(R.string.helpPhoneNoUri), requireActivity())
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    @OptIn(InternalPlatformTextApi::class)
    private fun setEmail() {
        var email: String
        viewModelCommunicator.message.observe(requireActivity(),
            { o -> email = o!!.toString() })

        if (viewModelCommunicator.message != null) {
            email = viewModelCommunicator.message.value.toString()
        } else {
            email = "abc@abc.com"
        }
        val myTypeface =
            Typeface.create(
                ResourcesCompat.getFont(requireContext(), R.font.opensreg),
                Typeface.BOLD
            )
        val spannable =
            SpannableString("A link to reset your password has been sent to $email.\n\nThis link will expire in 24 hours.")
        spannable.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.white, null)),
            47,
            47 + email.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE,
        )
        spannable.setSpan(
            TypefaceSpan(myTypeface),
            47,
            47 + email.length,
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )

        binding.contentSign.text = spannable

    }



    private fun buttonProperty() {
        binding.emailApp.btn.text = resources.getString(R.string.openEmailAppText)
        binding.emailApp.btn.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
        binding.emailApp.btn.setTextColor(resources.getColor(R.color.black, null))
    }
}
