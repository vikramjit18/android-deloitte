/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fwp.deloittedctcustomerapp.BuildConfig
import com.fwp.deloittedctcustomerapp.data.db.AppDao
import com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest
import com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse
import com.fwp.deloittedctcustomerapp.data.repo.MainRepo
import com.fwp.deloittedctcustomerapp.utils.Event
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val mainRepo: MainRepo
) : ViewModel() {
    private val _offloadResponse = MutableLiveData<DownloadOffloadResponse>()
    val offloadResponse: LiveData<DownloadOffloadResponse>
        get() = _offloadResponse

    private val _toastViewUser = MutableLiveData<Event<String>>()
    val toastViewUser: LiveData<Event<String>>
        get() = _toastViewUser

    private val _toastDownloadOffload = MutableLiveData<Event<String>>()
    val toastDownloadOffload: LiveData<Event<String>>
        get() = _toastDownloadOffload

    var versionCode: Int = BuildConfig.VERSION_CODE
    private var versionName: String = BuildConfig.VERSION_NAME
    private val _text = MutableLiveData<String>().apply {
        value = "Version $versionName (${versionCode})"
    }
    val version: LiveData<String> = _text

    val coroutineExceptionHandler = CoroutineExceptionHandler{_, t -> t.printStackTrace() }

    fun offLoadRequest(downloadOffloadRequest: DownloadOffloadRequest) {
        GlobalScope.launch (Dispatchers.IO + coroutineExceptionHandler) {
            val response = mainRepo.downloadOffLoadEtag(downloadOffloadRequest)

            withContext(Dispatchers.Main)   {
                if (response.isSuccessful) {
                    when (response.body()!!.responseCode) {

                        UtilConstant.SUCCESS_CODE, UtilConstant.FAIL_CODE
                        -> _offloadResponse.value = response.body()

                        UtilConstant.ERROR_CODE -> _toastViewUser.value =
                            response.body()!!.resObject?.get(0)?.let { Event(it.toString()) }
                    }

                } else {
                    _toastDownloadOffload.value = Event(response.message())
                }
            }
        }
    }


}
