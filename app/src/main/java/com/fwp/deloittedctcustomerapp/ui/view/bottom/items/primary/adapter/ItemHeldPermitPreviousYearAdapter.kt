/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.adapter

import android.content.Intent
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.ItemDetail
import com.fwp.deloittedctcustomerapp.databinding.ItemHeldChildRowBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView.TagLicenceActivity
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView.TagPermitActivity
import com.fwp.deloittedctcustomerapp.utils.UtilConstant

class ItemHeldPermitPreviousYearAdapter(
    private val previousList: List<ItemDetail>
) :
    RecyclerView.Adapter<ItemHeldPermitPreviousYearAdapter.MyViewHolder>() {
    inner class MyViewHolder(val viewDataBinding: ItemHeldChildRowBinding) :
        RecyclerView.ViewHolder(viewDataBinding.root)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemHeldPermitPreviousYearAdapter.MyViewHolder {


        val binding =
            ItemHeldChildRowBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemHeldPermitPreviousYearAdapter.MyViewHolder, position: Int) {

        val currentItem = previousList[position]
        holder.viewDataBinding.year.text = currentItem.year
        holder.viewDataBinding.tvTagLicence.text = currentItem.permitName

        val drawable: Drawable? =     ResourcesCompat.getDrawable(
            holder.itemView.resources,
            R.drawable.ic_permit,
            null
        )

        holder.viewDataBinding.ivListIcon.setImageDrawable(drawable)

        navigateHandler(holder, position)
    }

    private fun navigateHandler(holder: MyViewHolder, position: Int) {
        val itemSelected = previousList[position]
        val context = holder.itemView.context

        holder.viewDataBinding.itemContainer.setOnClickListener {
            if (itemSelected.permitName.lowercase().contains(UtilConstant.LICENSE)) {
                val i = Intent(context, TagPermitActivity::class.java)
                i.putExtra(UtilConstant.ITEM_POSITION, position)
                i.putExtra("account", itemSelected.accountType)
                i.putExtra(UtilConstant.ACCOUNT_TYPE, UtilConstant.PREVIOUS_YEAR)
                i.putExtra("ownerName", itemSelected.multiAccountOwner)
                holder.itemView.context.startActivity(i)
            }}
    }

    override fun getItemCount(): Int {
        return previousList.size
    }
}
