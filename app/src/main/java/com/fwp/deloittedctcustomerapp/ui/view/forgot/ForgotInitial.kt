/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.forgot


import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.compose.ui.text.android.InternalPlatformTextApi
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.ForgotPasswordRequest
import com.fwp.deloittedctcustomerapp.databinding.ForgotInitialFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException

@AndroidEntryPoint
class ForgotInitial : Fragment() {

    //init communicator to pass data from one fragment to another
    private lateinit var model: Communicator

    //init binding

    private var _binding: ForgotInitialFragmentBinding? = null
    private val binding get() = _binding!!

    // init loader
    private lateinit var loadingDialog: LoadingDialog

    //init fragment view model
    private lateinit var forgotViewModel: ForgotViewModel

    //on create view lifecyle


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = ForgotInitialFragmentBinding.inflate(inflater, container, false)
        model = ViewModelProvider(requireActivity())[Communicator::class.java]
        forgotViewModel = ViewModelProvider(requireActivity())[ForgotViewModel::class.java]
        // loader
        loadingDialog = LoadingDialog(mActivity = requireActivity())
        viewModelObser()
//        returning view
        return binding.root

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //init fun
        buttonProperty()
        //init fun
        buttonHandler()
        //init fun
        emailValidation()


    }

    // xml button handler
    private fun buttonHandler() {

        binding.etEmail.editText.inputType =
            InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS or InputType.TYPE_CLASS_TEXT

        //back button
        binding.ivClose.setOnClickListener {
//            UtilsMethods.backStack(findNavController())
            requireActivity().finish()
            requireActivity().overridePendingTransition(
                R.anim.slide_bottom_to_top, R.anim.slide_top_to_bottom
            )
        }

        //tvHaving
        binding.tvHavingTrouble.setOnClickListener {
            showAlert()
        }
    }

    // email validation and button activation
    private fun emailValidation() {
        binding.etEmail.editText.doAfterTextChanged {
            val email: String = binding.etEmail.editText.text.toString().trim()
            val nextButtonId = binding.next.btn
            when (UtilConstant.EMAIL_ADDRESS_PATTERN.matcher(email).matches()) {

                true -> {
                    nextButtonId.isEnabled = true
                    nextButtonId.isClickable = true
                    nextButtonId.setTextColor(resources.getColor(R.color.black, null))
                    nextButtonId.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
                    nextButtonId.setOnClickListener {

                        //set the message to share to another fragment
                        apiHitForgot(email)
                    }
                }

                false -> {
                    nextButtonId.isEnabled = false
                    nextButtonId.isClickable = false
                    nextButtonId.setTextColor(resources.getColor(R.color.white, null))
                    nextButtonId.setBackgroundColor(
                        resources.getColor(
                            R.color.disableButtonColor,
                            null
                        )
                    )
                }
            }
        }
    }

    private fun apiHitForgot(email: String) {
        if (UtilsMethods.isInternetConnected(requireContext())) {
            //init fun
            SharedPrefs.write(UtilConstant.FORGOT_PASS_EMAIL_KEY, email)
            model.setMsgCommunicator(email)

    //                            showLoader()

            try {
                loadingDialog.startLoading()

                forgotViewModel.forgotPassRequest(
                    ForgotPasswordRequest(
                        emailid = email
                    )
                )


            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (e2: SocketTimeoutException) {
                loadingDialog.isDismiss()
                e2.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }

        } else {
            UtilsMethods.showSimpleAlert(
                mContext = requireContext(),
                tvPrimary = getString(R.string.no_connection),
                tvSecondary = getString(R.string.check_and_try_again),
                primaryBtn = resources.getString(R.string.ok)

            )
        }
    }


    //view model response observer to handle fragment state
    private fun viewModelObser() {

        forgotViewModel.forgotPassResponse.observe(
            viewLifecycleOwner
        ) {
//                hideLoader()
            loadingDialog.isDismiss()
            if (it.responseMessage == UtilConstant.SUCCESS_MSG) {
                if (it.responseCode == UtilConstant.SUCCESS_CODE) {
                    clearFields()
                    if (it.responseCode == UtilConstant.SUCCESS_CODE) {
                        UtilsMethods.navigateToFragment(
                            binding.root,
                            R.id.action_forgotInitial_to_forgotHelp
                        )
                    }

                }
                if (it.responseCode == UtilConstant.FAIL_CODE) {
                    UtilsMethods.showDialerAlert(
                        mActivity = requireActivity(),
                        mContext = requireContext(),
                        tvPrimary = "Email not found",
                        tvSecondary = resources.getString(R.string.could_not_found_fwp),
                        lBtn = resources.getString(R.string.callFwpText),
                        rBtn = resources.getString(R.string.tryAgain),
                    )
                }


            }

        }

        forgotViewModel.toastForgotPass.observe(requireActivity()) {
            loadingDialog.isDismiss()
            it.getContentIfNotHandledOrReturnNull()?.let { message ->
//                UtilsMethods.toastMaker(requireContext(), message)
            }
        }
    }


    // show dialog alert for help/contact details
    @OptIn(InternalPlatformTextApi::class)
    private fun showAlert() {

        val builder = AlertDialog.Builder(requireContext()).create()
        val view = layoutInflater.inflate(R.layout.custom_dialog, null)
        val leftButton = view.findViewById<TextView>(R.id.btn_left)
        val rightButton = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = getString(R.string.need_support)
        des.text = getString(R.string.notReceiveEmailDes)
        leftButton.text = getString(R.string.tryAgain)
        rightButton.text = getString(R.string.callFwpText)
        builder.setView(view)
        leftButton.setOnClickListener {
            builder.dismiss()
        }
        rightButton.setOnClickListener {

            UtilsMethods.dialerOpener(getString(R.string.helpPhoneNoUri), requireActivity())
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }


    // button property use for init fragment button style in inherited button
    private fun buttonProperty() {
        binding.next.btn.text = resources.getString(R.string.nextButtonText)
        binding.next.btn.setTextColor(resources.getColor(R.color.white, null))
        binding.next.btn.setBackgroundColor(
            resources.getColor(
                R.color.disableButtonColor,
                null
            )
        )

        binding.etEmail.editText.imeOptions = EditorInfo.IME_ACTION_DONE
    }

    // Clear Field
    private fun clearFields() {
        binding.etEmail.editText.text?.clear()
    }


}
