/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.resetPass

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.ForgotSuccessFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.LandingScreenActivity
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import android.content.pm.PackageManager
import android.content.pm.PackageManager.NameNotFoundException
import javax.net.ssl.SSLException


class ForgotSuccess : Fragment() {

    //init binding
    lateinit var binding: ForgotSuccessFragmentBinding


    //on create view lifecyle
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding =
            DataBindingUtil.inflate(inflater, R.layout.forgot_success_fragment, container, false)
        //init fun
        buttonProperty()
        //init fun
        buttonHandler()
        return binding.root

    }

    // xml button handler
    private fun buttonHandler() {
        //back button
        binding.close.setOnClickListener() {
            UtilsMethods.backStack(findNavController())
        }

        //lauch app
        binding.btnLogin.btn.setOnClickListener {
            val i: Intent
            val manager: PackageManager = requireActivity().getPackageManager()
            try {
                i = manager.getLaunchIntentForPackage("com.fwp.deloittedctcustomerapp")!!
                if (i == null) throw NameNotFoundException()
                i.addCategory(Intent.CATEGORY_LAUNCHER)
                startActivity(i)
            } catch (e: NameNotFoundException) {
//                UtilsMethods.toastMaker(context = requireContext(), "$e")
            }catch (e3: SSLException){
                e3.printStackTrace()
            }


        }
    }

    // button property use for init fragment button style in inherited button

    private fun buttonProperty() {
        val btnEmailConfirm = binding.btnLogin.btn
        btnEmailConfirm.text = resources.getString(R.string.loginText)
        btnEmailConfirm.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
        btnEmailConfirm.setTextColor(resources.getColor(R.color.black, null))
    }

}
