/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.util.Util
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.databinding.MultiUserEtagAvailableFragmentBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.EtagsFragment
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.adapter.EtagsViewPagerAdapter
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

// FWP
// Hilt entry point

private const val TAG = "MultiUserEtagAvailableF"

@AndroidEntryPoint
class MultiUserEtagAvailableFragment : Fragment() {

    companion object {
        fun newInstance() = MultiUserEtagAvailableFragment()
    }

    // Global variables
    private lateinit var binding: MultiUserEtagAvailableFragmentBinding
    private lateinit var viewModel: LandingScreenActivityViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // data binding
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.multi_user_etag_available_fragment,
            container,
            false
        )

        viewModel = ViewModelProvider(requireActivity())[LandingScreenActivityViewModel::class.java]
        settingViewPagerView()
        return binding.root
    }

    override fun onResume() {
        super.onResume()

        Handler(Looper.getMainLooper()).post {
            binding.viewPager.setCurrentItem(
                UtilConstant.ACCOUNT_VIEWPAGER,
                true
            )
        }
    }

    override fun onPause() {
        super.onPause()
//        if (!UtilConstant.backStatus)
            UtilConstant.ACCOUNT_VIEWPAGER = binding.viewPager.currentItem
    }

    // view pager function
    private fun settingViewPagerView() {
        val activity = getActivity()
        if (activity != null) {
            // setting
            // adapter
            binding.viewPager.adapter =
                EtagsViewPagerAdapter(activity.supportFragmentManager, activity.lifecycle)


            // binding.viewPager.setCurrentItem(UtilConstant.ACCOUNT_VIEWPAGER)
            // attaching view pager with tab layout
            TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
                when (position) {
                    0 -> tab.text = "My E-tags"
                    1 -> tab.text = "Linked E-tags"
                }
            }.attach()

        }
    }

    // End of code

}
