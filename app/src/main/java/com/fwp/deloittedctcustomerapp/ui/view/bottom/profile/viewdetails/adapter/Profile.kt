/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.adapter

data class Profile(
    val name: String?,
    val alsNo: String?,
    val residentStatus: String?,
    val linkedStatus: String?
)
