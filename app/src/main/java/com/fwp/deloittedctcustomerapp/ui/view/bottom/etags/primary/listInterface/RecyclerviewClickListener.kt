/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.listInterface

interface RecyclerviewClickListener {

    fun recyclerviewClickListener(position: Int)
}
