/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicensesX
import com.fwp.deloittedctcustomerapp.databinding.ActivityTagLicenceBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint
import androidx.recyclerview.widget.DividerItemDecoration
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account
import com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicenses
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.google.gson.Gson


private const val TAG = "TagPermitActivity"

@AndroidEntryPoint
class TagLicenceActivity : AppCompatActivity() {

    lateinit var binding: ActivityTagLicenceBinding
    private lateinit var viewModel: LandingScreenActivityViewModel
    private lateinit var loadingDialog: LoadingDialog
    private var licenseList = mutableListOf<NonCarcassTagLicensesX>()
    private var year: String = ""
    private var accountType = "P"
    private var name = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tag_licence)

        year = intent.getStringExtra(UtilConstant.ACCOUNT_TYPE).toString()
        accountType = intent.getStringExtra("account").toString()
        name = intent.getStringExtra("ownerName").toString()

        viewModel = ViewModelProvider(this)[LandingScreenActivityViewModel::class.java]
        loadingDialog = LoadingDialog(this)
        SharedPrefs.init(this)
/*        apiHit()

        viewModelObsr()
        if (UtilsMethods.isInternetConnected(this)) {
            viewModelObsr()
        } else {

        }*/

        loadingDialog.startLoading()
        val gson = Gson()
        val json = SharedPrefs.read(UtilConstant.ITEM_HELD, "")
        if (!json.isNullOrEmpty()) {
            val licenceObj = gson.fromJson(json, ItemsHeldResponse::class.java)
            setupView(licenceObj)
        }

        buttonHandler()
    }


    @SuppressLint("SetTextI18n")
    private fun setupView(response: ItemsHeldResponse?) {

        response?.let {
            if (accountType == UtilConstant.MULTIPLE_ACCOUNT_KEY) {
                multiAccountUiBind(it)
            } else {
                primaryUiBind(it)
            }

        }
        loadingDialog.isDismiss()
        recyclerContent()
    }

    private fun primaryUiBind(it: ItemsHeldResponse) {
        val licenseResponse: NonCarcassTagLicenses?
        if (year != UtilConstant.PREVIOUS_YEAR) {
            licenseResponse =
                it.resObject?.get(0)?.accounts?.get(0)?.itemsHeld?.get(0)?.nonCarcassTagLicenses

            /*        if (licenseResponse != null) {
                        itrateDataList(licenseResponse)

                        binding.head.text =
                            "${licenseResponse.licenseYear} License listing"

                        binding.tvOwnerName.text = licenseResponse.owner

                        binding.tvLicenseYr.text = licenseResponse.licenseYear

                        binding.tvAlsNumber.text = licenseResponse.alsNo

                        binding.tvUsageDt.text = licenseResponse.usageDates

            //                        binding.tvLastSessionDt.text = "${licenseResponse.session}"
                        sessionDateTimeFormatter(licenseResponse)

        //                recyclerContent()
                    }*/
        } else {
            licenseResponse =
                it.resObject?.get(0)?.accounts?.get(0)?.itemsHeld?.get(UtilConstant.PREVIOUS_INDEX)?.nonCarcassTagLicenses
            //
        }

        settingUI(licenseResponse)
    }

    private fun multiAccountUiBind(it: ItemsHeldResponse) {
        if (year != UtilConstant.PREVIOUS_YEAR) {
            // Current
            currentYearMultiUIBind(it)
            // Previous
        } else {
            previousYearMultiUiBind(it)
        }
    }

    private fun previousYearMultiUiBind(it: ItemsHeldResponse) {
        for (item in it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts?.indices!!) {
            when (it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts!![item].owner) {
                name -> {

                    val licenseResponse =
                        it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts?.get(
                            item
                        )?.itemsHeld?.get(UtilConstant.PREVIOUS_INDEX)?.nonCarcassTagLicenses
                    //
                    settingUI(licenseResponse)

                }
            }
        }
    }

    private fun currentYearMultiUIBind(it: ItemsHeldResponse) {
        for (item in it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts?.indices!!) {
            when (it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts!![item].accountType) {
                UtilConstant.MULTIPLE_ACCOUNT_KEY -> {
                    val responseList =
                        it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts!![item]

                    mutliCurrentOwnerUiBind(responseList, it, item)
                }
            }
        }
    }

    private fun mutliCurrentOwnerUiBind(
        responseList: Account,
        it: ItemsHeldResponse,
        item: Int
    ) {
        if (responseList.owner.equals(name)) {
            val licenseResponse =
                it.resObject?.get(UtilConstant.CURRENT_INDEX)?.accounts?.get(
                    item
                )?.itemsHeld?.get(
                    UtilConstant.CURRENT_INDEX
                )?.nonCarcassTagLicenses

            settingUI(licenseResponse)
        }
    }

    private fun settingUI(licenseResponse: NonCarcassTagLicenses?) {
        if (licenseResponse != null) {
            itrateDataList(licenseResponse)

            binding.head.text =
                "${licenseResponse.licenseYear} License listing"

            binding.tvOwnerName.text = licenseResponse.owner

            binding.tvLicenseYr.text = licenseResponse.licenseYear

            binding.tvAlsNumber.text = licenseResponse.alsNo

            binding.tvUsageDt.text = licenseResponse.usageDates

            //                                    binding.tvLastSessionDt.text = "${licenseResponse.session}"


            sessionDateTimeFormatter(licenseResponse)

        }
    }

    private fun itrateDataList(licenseResponse: NonCarcassTagLicenses) {
        for (i in licenseResponse.nonCarcassTagLicensesList?.indices!!) {
            licenseList.add(
                NonCarcassTagLicensesX(
                    licenseResponse.nonCarcassTagLicensesList!![i].issuedDate,
                    licenseResponse.nonCarcassTagLicensesList!![i].licenseName,
                    licenseResponse.nonCarcassTagLicensesList!![i].oppCode
                )
            )
        }
    }

    private fun sessionDateTimeFormatter(licenseResponse: NonCarcassTagLicenses) {
        when (!licenseResponse.session!!.isNullOrEmpty()) {
            true -> {
                val sessionTime = licenseResponse.session!!.split(" ")

                if (sessionTime!!.contains("PM"))
                    binding.tvLastSessionDt.text =
                        "${sessionTime!![0]} - ${sessionTime!![1]} p.m."
                else
                    binding.tvLastSessionDt.text =
                        "${sessionTime!![0]} - ${sessionTime!![1]} a.m."
            }

            false -> binding.tvLastSessionDt.text =
                licenseResponse.session
        }
    }

    private fun recyclerContent() {

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@TagLicenceActivity)
            addItemDecoration(
                DividerItemDecoration(
                    this@TagLicenceActivity,
                    LinearLayoutManager.VERTICAL
                )
            )
            adapter = LicenseAdapter(licenseList)
        }
    }

    private fun buttonHandler() {
        binding.close.setOnClickListener {
            UtilsMethods.activityBack(this)
        }

    }
}
