/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.forgot

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgotActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot)
        SharedPrefs.init(this)
    }
    override fun onBackPressed() {

    }
}
