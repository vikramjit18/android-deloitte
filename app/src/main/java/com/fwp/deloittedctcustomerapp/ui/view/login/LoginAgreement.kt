/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.ui.view.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.fwp.deloittedctcustomerapp.R
import com.fwp.deloittedctcustomerapp.data.model.requests.LoginRequest
import com.fwp.deloittedctcustomerapp.databinding.FragmentLoginAgreementBinding
import com.fwp.deloittedctcustomerapp.ui.view.bottom.LandingScreenActivity
import com.fwp.deloittedctcustomerapp.utils.LoadingDialog
import com.fwp.deloittedctcustomerapp.utils.SharedPrefs
import com.fwp.deloittedctcustomerapp.utils.UtilConstant
import com.fwp.deloittedctcustomerapp.utils.UtilsMethods
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import java.net.SocketTimeoutException
import javax.net.ssl.SSLException

/**
 * FWP
 * Hilt android entry
 */
@AndroidEntryPoint
class LoginAgreement : Fragment() {
    /**
     * Global variables
     */
    lateinit var binding: FragmentLoginAgreementBinding
    private val loginViewModel: LoginViewModel by viewModels()
    private lateinit var loadingDialog: LoadingDialog
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        /**
         * Binding Initiate
         */
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_login_agreement, container, false)
        SharedPrefs.init(requireActivity())
        loadingDialog = LoadingDialog(requireActivity())
        /**
         * Returning
         * root binding
         */
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /**
         * Init Execution
         */
        init()
        buttonHandler()

        /**
         * Observing
         * for any error message
         * from server
         */
        /*    loginViewModel.toast.observe(viewLifecycleOwner, {
               it.getContentIfNotHandledOrReturnNull()?.let { message ->

               }
           })*/

        // Observing for any error message from server
        loginViewModel.toastTouResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandledOrReturnNull()?.let { message ->
                loadingDialog.isDismiss()

//                UtilsMethods.toastMaker(requireContext(), message)

                Log.e("Login response", "onViewCreated: $message")
            }
        })
    }

    /**
     * Button handler function
     */
    private fun buttonHandler() {
        binding.view.agree.btn.setOnClickListener {
            /**
             * Checking Internet connection
             */
            if (UtilsMethods.isInternetConnected(requireContext())) {

                apiHitTou()
            } else {
                /**
                 * showing
                 * No internet dialog
                 */
                UtilsMethods.showSimpleAlert(
                    mContext = requireContext(),
                    tvPrimary = getString(R.string.no_connection),
                    tvSecondary = getString(R.string.check_and_try_again),
                    primaryBtn = resources.getString(R.string.ok)
                )
            }

            /**
             * Observing
             * agreement Api Response
             */
            viewModelObser()
        }

        /**
         * Navigate to previous fragment
         */
        binding.view.ivBack.setOnClickListener {
            UtilsMethods.backStack(findNavController())
        }
    }

    private fun apiHitTou() {
        /**
         * Getting shared prefs value
         */
        val token = SharedPrefs.read(UtilConstant.JWT_TOKEN, "").toString()
        if (token.isNotEmpty()) {
            /**
             * showing
             * progress loader
             */
            loadingDialog.startLoading()

            /**
             * Initiate
             * agreement Api
             */
            try {
                loginViewModel.acceptTou(token)
            } catch (e: Exception) {
                loadingDialog.isDismiss()
                e.printStackTrace()
            } catch (e2: SocketTimeoutException) {
                loadingDialog.isDismiss()
                e2.printStackTrace()
            } catch (e3: SSLException) {
                loadingDialog.isDismiss()
                e3.printStackTrace()
            }


        } else {
    //                    UtilsMethods.toastMaker(requireContext(), "Jwt not found")
        }
    }

    private fun viewModelObser() {
        loginViewModel.acceptTouResponse.observe(viewLifecycleOwner, {
            // Hide loader
            loadingDialog.isDismiss()

            // Navigate to main screen
//            UtilsMethods.toastMaker(requireContext(), it.toString())
            val i = Intent(activity, LandingScreenActivity::class.java)
            startActivity(i)
            requireActivity().finish()
        })

        // Fetching if there are any unwanted msg comes up
        loginViewModel.toastTouResponse.observe(viewLifecycleOwner, { message ->
            message.getContentIfNotHandledOrReturnNull()?.let {
//                UtilsMethods.toastMaker(requireContext(), it)
                Log.e("Accept agreement", "viewModelObser: $message")
            }
        })
    }

    /**
     * Initiate
     * init Function
     */
    private fun init() {
        binding.view.agree.btn.text = resources.getString(R.string.agree_login_continue)
        binding.view.linearProgress.visibility = View.GONE
        binding.view.agree.btn.setTextColor(resources.getColor(R.color.black, null))
        binding.view.agree.btn.setBackgroundColor(resources.getColor(R.color.prim_yellow, null))
    }

}
