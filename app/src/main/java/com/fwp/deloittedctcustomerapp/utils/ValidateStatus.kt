/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.utils

object ValidateStatus {
    const val VALIDATED = "Y"
    const val NOT_VALIDATED = "N"
}
