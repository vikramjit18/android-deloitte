/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.utils

import android.content.Context
import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.fwp.deloittedctcustomerapp.R
import java.util.*

class TagImageInflater {

    companion object {

        // show detail icon names
        const val icon_tagdetail_ic_Antelope = "icon_tagdetail_ic_Antelope@3x.png"
        const val icon_tagdetail_ic_Antelope_Antlerless = "icon_tagdetail_ic_Antelope_Antlerless@3x.png"
        const val icon_tagdetail_ic_BighornSheep = "icon_tagdetail_ic_BighornSheep@3x.png"
        const val icon_tagdetail_ic_BigHornSheep_AdultEwe =
            "icon_tagdetail_ic_BigHornSheep_AdultEwe@3x.png"
        const val icon_tagdetail_ic_Bison = "icon_tagdetail_ic_Bison@3x.png"
        const val icon_tagdetail_ic_Blackbear = "icon_tagdetail_ic_Blackbear@3x.png"
        const val icon_tagdetail_ic_Deer = "icon_tagdetail_ic_Deer@3x.png"
        const val icon_tagdetail_ic_Deer_Antlerless = "icon_tagdetail_ic_Deer_Antlerless@3x.png"
        const val icon_tagdetail_ic_Elk = "icon_tagdetail_ic_Elk@3x.png"
        const val icon_tagdetail_ic_Elk_Antlerless = "icon_tagdetail_ic_Elk_Antlerless@3x.png"
        const val icon_tagdetail_ic_ElkSpikeBull = "icon_tagdetail_ic_ElkSpikeBull@3x.png"
        const val icon_tagdetail_ic_MigratoryBirds = "icon_tagdetail_ic_MigratoryBirds@3x.png"
        const val icon_tagdetail_ic_Moose = "icon_tagdetail_ic_Moose@3x.png"
        const val icon_tagdetail_ic_MooseAntlerless = "icon_tagdetail_ic_MooseAntlerless@3x.png"
        const val icon_tagdetail_ic_MountainGoat = "icon_tagdetail_ic_MountainGoat@3x.png"
        const val icon_tagdetail_ic_MountainLion = "icon_tagdetail_ic_MountainLion@3x.png"
        const val icon_tagdetail_ic_Paddlefish = "icon_tagdetail_ic_Paddlefish@3x.png"
        const val icon_tagdetail_ic_Sandhillcrane = "icon_tagdetail_ic_Sandhillcrane@3x.png"
        const val icon_tagdetail_ic_Swan = "icon_tagdetail_ic_Swan@3x.png"
        const val icon_tagdetail_ic_Turkey = "icon_tagdetail_ic_Turkey@3x.png"
        const val icon_tagdetail_ic_Turkey_Beardless = "icon_tagdetail_ic_Turkey_Beardless@3x.png"
        const val icon_tagdetail_ic_UplandBirds = "icon_tagdetail_ic_UplandBirds@3x.png"
        const val icon_tagdetail_ic_Wolf = "icon_tagdetail_ic_Wolf@3x.png"

        //show unvalidted tags names

        const val icon_tags_ic_species_Antelope = "icon_tags_ic_species_Antelope@3x.png"
        const val icon_tags_ic_species_AntelopeAntlerless =
            "icon_tags_ic_species_AntelopeAntlerless@3x.png"
        const val icon_tags_ic_species_BighornSheep = "icon_tags_ic_species_BighornSheep@3x.png"
        const val icon_tags_ic_species_Bison = "icon_tags_ic_species_Bison@3x.png"
        const val icon_tags_ic_species_Blackbear = "icon_tags_ic_species_Blackbear@3x.png"
        const val icon_tags_ic_species_Deer = "icon_tags_ic_species_Deer@3x.png"
        const val icon_tags_ic_species_Deer_Antlerless = "icon_tags_ic_species_Deer_Antlerless@3x.png"
        const val icon_tags_ic_species_Elk = "icon_tags_ic_species_Elk@3x.png"
        const val icon_tags_ic_species_Elk_Antlerless = "iicon_tags_ic_species_Elk_Antlerless@3x.png"
        const val icon_tags_ic_species_MigratoryBirds = "icon_tags_ic_species_MigratoryBirds@3x.png"
        const val icon_tags_ic_species_Moose = "icon_tags_ic_species_Moose@3x.png"
        const val icon_tags_ic_species_MountainGoat = "icon_tags_ic_species_MountainGoat@3x.png"
        const val icon_tags_ic_species_MountainLion = "icon_tags_ic_species_MountainLion@3x.png"
        const val icon_tags_ic_species_Paddlefish = "icon_tags_ic_species_Paddlefish@3x.png"
        const val icon_tags_ic_species_Sandhillcrane = "icon_tags_ic_species_Sandhillcrane@3x.png"
        const val icon_tags_ic_species_Swan = "icon_tags_ic_species_Swan@3x.png"
        const val icon_tags_ic_species_turkey = "icon_tags_ic_species_turkey@3x.png"
        const val icon_tags_ic_species_UplandBirds = "icon_tags_ic_species_UplandBirds@3x.png"
        const val icon_tags_ic_species_Wolf = "icon_tags_ic_species_Wolf@3x.png"

        // validate icons

        const val icon_tags_ic_vali_species_Antelope = "icon_tags_ic_vali_species_Antelope@3x.png"
        const val icon_tags_ic_vali_species_AntelopeAntlerless =
            "icon_tags_ic_vali_species_AntelopeAntlerless@3x.png"
        const val icon_tags_ic_vali_species_BighornSheep =
            "icon_tags_ic_vali_species_BighornSheep@3x.png"
        const val icon_tags_ic_vali_species_Bison = "icon_tags_ic_vali_species_Bison@3x.png"
        const val icon_tags_ic_vali_species_Blackbear = "icon_tags_ic_vali_species_Blackbear@3x.png"
        const val icon_tags_ic_vali_species_Deer = "icon_tags_ic_vali_species_Deer@3x.png"
        const val icon_tags_ic_vali_species_Deer_Antlerless =
            "icon_tags_ic_vali_species_Deer_Antlerless@3x.png"
        const val icon_tags_ic_vali_species_Elk = "icon_tags_ic_vali_species_Elk@3x.png"
        const val icon_tags_ic_vali_species_Elk_Antlerless =
            "icon_tags_ic_vali_species_Elk_Antlerless@3x.png"
        const val icon_tags_ic_vali_species_MigratoryBirds =
            "icon_tags_ic_vali_species_MigratoryBirds@3x.png"
        const val icon_tags_ic_vali_species_Moose = "icon_tags_ic_vali_species_Moose@3x.png"
        const val icon_tags_ic_vali_species_MountainGoat =
            "icon_tags_ic_vali_species_MountainGoat@3x.png"
        const val icon_tags_ic_vali_species_MountainLion =
            "icon_tags_ic_vali_species_MountainLion@3x.png"
        const val icon_tags_ic_vali_species_Paddlefish = "icon_tags_ic_vali_species_Paddlefish@3x.png"
        const val icon_tags_ic_vali_species_Sandhillcrane =
            "icon_tags_ic_vali_species_Sandhillcrane@3x.png"
        const val icon_tags_ic_vali_species_Swan = "icon_tags_ic_vali_species_Swan.png"
        const val icon_tags_ic_vali_species_turkey = "icon_tags_ic_vali_species_turkey@3x.png"
        const val icon_tags_ic_vali_species_UplandBirds = "icon_tags_ic_vali_species_UplandBirds@3x.png"
        const val icon_tags_ic_vali_species_Wolf = "icon_tags_ic_vali_species_Wolf@3x.png"


        //hero image
        const val IMG_tagdetail_IMG_Antelope = "IMG_tagdetail_IMG_Antelope.png"
        const val IMG_tagdetail_IMG_Antelope_Antlerless = "IMG_tagdetail_IMG_Antelope_Antlerless.png"
        const val IMG_tagdetail_IMG_BighornSheep = "IMG_tagdetail_IMG_BighornSheep.png"
        const val IMG_tagdetail_IMG_Bison = "IMG_tagdetail_IMG_Bison.png"
        const val IMG_tagdetail_IMG_Blackbear = "IMG_tagdetail_IMG_Blackbear.png"
        const val IMG_tagdetail_IMG_Elk = "IMG_tagdetail_IMG_Elk.png"
        const val IMG_tagdetail_IMG_Elk_Antlerless = "IMG_tagdetail_IMG_Elk_Antlerless.png"
        const val IMG_tagdetail_IMG_MigratoryBirds = "IMG_tagdetail_IMG_MigratoryBirds.png"
        const val IMG_tagdetail_IMG_Moose = "IMG_tagdetail_IMG_Moose.png"
        const val IMG_tagdetail_IMG_MountainGoat = "IMG_tagdetail_IMG_MountainGoat.png"
        const val IMG_tagdetail_IMG_MountainLion = "IMG_tagdetail_IMG_MountainLion.png"
        const val IMG_tagdetail_IMG_MuleDeer = "IMG_tagdetail_IMG_MuleDeer.png"
        const val IMG_tagdetail_IMG_MuleDeer_Antlerless = "IMG_tagdetail_IMG_MuleDeer_Antlerless.png"
        const val IMG_tagdetail_IMG_Paddlefish = "IMG_tagdetail_IMG_Paddlefish.png"
        const val IMG_tagdetail_IMG_Sandhillcrane = "IMG_tagdetail_IMG_Sandhillcrane.png"
        const val IMG_tagdetail_IMG_Swan = "IMG_tagdetail_IMG_Swan.png"
        const val IMG_tagdetail_IMG_Turkey = "IMG_tagdetail_IMG_Turkey.png"
        const val IMG_tagdetail_IMG_UplandBirds = "IMG_tagdetail_IMG_UplandBirds.png"
        const val IMG_tagdetail_IMG_WhitetailDeer = "IMG_tagdetail_IMG_WhitetailDeer.png"
        const val IMG_tagdetail_IMG_WhitetailDeer_Antlerless =
            "IMG_tagdetail_IMG_WhitetailDeer_Antlerless.png"
        const val IMG_tagdetail_IMG_Wolf = "IMG_tagdetail_IMG_Wolf.png"


    }


    fun showDetailedTagIcon(context: Context, imageView: ImageView, heroName: String) {
//        Log.e("heroName", "showDetailedTagIcon: $heroName")

        when {

            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_ElkSpikeBull.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_elkspikebull)
                    .into(imageView)
            }

            heroName.lowercase(Locale.getDefault())
                .contains(icon_tagdetail_ic_Turkey_Beardless.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_turkey_beardless)
                    .into(imageView)
            }

            heroName.lowercase(Locale.getDefault())
                .contains(icon_tagdetail_ic_Antelope_Antlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_antelope_antlerless)
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Deer_Antlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_deer_antlerless)
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Elk_Antlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_elk_antlerless)
                    .into(imageView)
            }

            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_MooseAntlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_mooseantlerless)
                    .into(imageView)
            }

            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Elk.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_elk).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Antelope.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_antelope).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Deer.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_deer).circleCrop()
                    .into(imageView)
            }

            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_MountainLion.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_mountainlion).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_BighornSheep.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_bighornsheep).into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tagdetail_ic_BigHornSheep_AdultEwe.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_bighornsheep_adultewe)
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Bison.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_bison).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Blackbear.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_blackbear).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_MigratoryBirds.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_migratorybirds)
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Moose.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_moose).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Paddlefish.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_paddlefish).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Sandhillcrane.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_sandhillcrane).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Swan.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_swan).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Turkey.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_turkey).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_UplandBirds.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_uplandbirds).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_Wolf.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_wolf).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tagdetail_ic_MountainGoat.lowercase()) -> {
                Glide.with(context).load(R.drawable.icon_tagdetail_ic_mountaingoat).into(imageView)
            }

        }
    }

    fun showListTagIcon(context: Context, imageView: ImageView, heroName: String) {
//        Log.e("heroName", "showListTagIcon: $heroName")
        when {


            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_vali_species_AntelopeAntlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_antelopeantlerless)
                    .circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_vali_species_Deer_Antlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_deer_antlerless)
                    .circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_vali_species_Elk_Antlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_elk_antlerless)
                    .circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_vali_species_Elk.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_elk).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_vali_species_Antelope.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_antelope).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_vali_species_Deer.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_deer).circleCrop().into(imageView)
            }

            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_vali_species_MountainLion.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_mountainlion).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_vali_species_BighornSheep.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_bighornsheep).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_vali_species_Bison.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_bison).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_vali_species_Blackbear.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_blackbear).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_vali_species_MigratoryBirds.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_migratorybirds).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_vali_species_Moose.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_moose).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_vali_species_Paddlefish.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_paddlefish).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_vali_species_Sandhillcrane.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_sandhillcrane).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_vali_species_Swan.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_swan).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_vali_species_turkey.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_turkey).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_vali_species_UplandBirds.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_uplandbirds).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_vali_species_Wolf.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_wolf).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_vali_species_MountainGoat.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_vali_mountaingoat).circleCrop()
                    .into(imageView)
            }


            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_species_AntelopeAntlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_antelopeantlerless)
                    .circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_species_Deer_Antlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_deer_antlerless).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_species_Elk_Antlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_elk_antlerless).circleCrop()
                    .into(imageView)
            }

            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_Elk.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_elk).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_Antelope.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_antelope).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_Deer.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_deer).circleCrop().into(imageView)
            }

            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_MountainLion.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_mountainlion)
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_BighornSheep.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_bighornsheep).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_Bison.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_bison).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_Blackbear.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_blackbear).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_species_MigratoryBirds.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_migratorybirds).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_Moose.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_moose).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_Paddlefish.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_paddlefish).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(icon_tags_ic_species_Sandhillcrane.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_sandhillcrane).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_Swan.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_swan).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_turkey.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_turkey).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_UplandBirds.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_uplandbirds).circleCrop()
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_Wolf.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_wolf).circleCrop().into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(icon_tags_ic_species_MountainGoat.lowercase()) -> {
                Glide.with(context).load(R.drawable.tags_ic_mountaingoat).circleCrop()
                    .into(imageView)
            }

        }
    }


    fun showDetailedTagImage(context: Context, imageView: ImageView, heroName: String) {
//        Log.e("heroName", "showDetailedTagImage: $heroName")
        when {

            heroName.lowercase(Locale.getDefault())
                .contains(IMG_tagdetail_IMG_Antelope_Antlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_antelope_antlerless)
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(IMG_tagdetail_IMG_MuleDeer_Antlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_muledeer_antlerless)
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault())
                .contains(IMG_tagdetail_IMG_WhitetailDeer_Antlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_whitetaildeer_antlerless)
                    .into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_Elk_Antlerless.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_elk_antlerless).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_Elk.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_elk).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_WhitetailDeer.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_whitetaildeer).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_Antelope.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_antelope).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_MuleDeer.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_muledeer).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_MountainLion.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_mountainlion).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_BighornSheep.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_bighornsheep).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_Bison.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_bison).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_Blackbear.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_blackbear).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_MigratoryBirds.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_migratorybirds).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_Moose.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_moose).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_Paddlefish.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_paddlefish).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_Sandhillcrane.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_sandhillcrane).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_Swan.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_swan).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_Turkey.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_turkey).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_UplandBirds.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_uplandbirds).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_Wolf.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_wolf).into(imageView)
            }
            heroName.lowercase(Locale.getDefault()).contains(IMG_tagdetail_IMG_MountainGoat.lowercase()) -> {
                Glide.with(context).load(R.drawable.hero_mountaingoat).into(imageView)
            }

        }
    }
}
