/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.utils

import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.SSLException

class DownloadTask(private val context: Context, downloadUrl: String) {
    private var downloadUrl = ""
    private var downloadFileName = ""
    private var progressBar: ProgressBar? = null

    private inner class DownloadingTask :
        AsyncTask<Void?, Void?, Void?>() {
        var apkStorage: File? = null
        var outputFile: File? = null
/*        override fun onPreExecute() {
            super.onPreExecute()
            progressBar = ProgressBar(context)
            progressBar!!.setMessage("Downloading...")
            progressBar!!.setCancelable(false)
            progressBar!!.show()
        }*/

        override fun onPostExecute(result: Void?) {
            try {
                if (outputFile != null) {
                    progressBar!!.visibility = View.GONE
//                    val ctw = ContextThemeWrapper(context, android.R.style.Theme_AlertDialog)
                    val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(context)
                    alertDialogBuilder.setTitle("Document  ")
                    alertDialogBuilder.setMessage("Document Downloaded Successfully ")
                    alertDialogBuilder.setCancelable(false)
                    alertDialogBuilder.setPositiveButton("ok",
                        DialogInterface.OnClickListener { _, _ -> })
                    alertDialogBuilder.setNegativeButton("Open report",
                        DialogInterface.OnClickListener { _, _ ->
                            val pdfFile = File(
                                context.getExternalFilesDir(null)!!.absolutePath.toString() + "/fwp/" + downloadFileName
                            ) // -> filename = fwp.pdf
                            val path: Uri = Uri.fromFile(pdfFile)
                            val pdfIntent = Intent(Intent.ACTION_VIEW)
                            pdfIntent.setDataAndType(path, "application/pdf")
                            pdfIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            try {
                                context.startActivity(pdfIntent)
                            } catch (e: ActivityNotFoundException) {
                                Toast.makeText(
                                    context,
                                    "No Application available to view PDF",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        })
                    alertDialogBuilder.show()
                    //                    Toast.makeText(context, "Document Downloaded Successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Handler(Looper.getMainLooper()).postDelayed({ }, 3000)
                    Log.e(TAG, "Download Failed")
                }
            } catch (e: Exception) {
                e.printStackTrace()

                //Change button text if exception occurs
                Handler(Looper.getMainLooper()).postDelayed({ }, 3000)
                Log.e(TAG, "Download Failed with Exception - " + e.localizedMessage)
            }catch (e3: SSLException){
                e3.printStackTrace()
            }
            super.onPostExecute(result)
        }

        override fun doInBackground(vararg p0: Void?): Void? {
            try {
                val url = URL(downloadUrl) //Create Download URl
                val c = url.openConnection() as HttpURLConnection //Open Url Connection
                c.requestMethod = "GET" //Set Request Method to "GET" since we are grtting data
                c.connect() //connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.responseCode != HttpURLConnection.HTTP_OK) {
                    Log.e(
                        TAG, "Server returned HTTP " + c.responseCode
                            + " " + c.responseMessage
                    )
                }


                //Get File if SD card is present
                if (CheckForSDCard().isSDCardPresent) {
                    apkStorage = File(
                        context.getExternalFilesDir(null)!!.absolutePath.toString() + "/fwp/" + downloadFileName
                    )
                } else Toast.makeText(context, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT)
                    .show()

                //If File is not present create directory
                if (!apkStorage!!.exists()) {
                    apkStorage!!.mkdir()
                    Log.e(TAG, "Directory Created.")
                }
                outputFile = File(apkStorage, downloadFileName) //Create Output file in Main File

                //Create New File if not present
                if (!outputFile!!.exists()) {
                    outputFile!!.createNewFile()
                    Log.e(TAG, "File Created")
                }
                val fos = FileOutputStream(outputFile) //Get OutputStream for NewFile Location
                val `is` = c.inputStream //Get InputStream for connection
                val buffer = ByteArray(1024) //Set buffer type
                var len1 = 0 //init length
                while (`is`.read(buffer).also { len1 = it } != -1) {
                    fos.write(buffer, 0, len1) //Write new file
                }

                //Close all connection after doing task
                fos.close()
                `is`.close()
            } catch (e: Exception) {

                //Read exception if something went wrong
                e.printStackTrace()
                outputFile = null
                Log.e(TAG, "Download Error Exception " + e.message)
            }catch (e3: SSLException){
                e3.printStackTrace()
            }
            return null
        }
    }


    companion object {
        private const val TAG = "Download Task"
    }

    init {
        this.downloadUrl = downloadUrl
        downloadFileName = downloadUrl.substring(
            downloadUrl.lastIndexOf('/'),
            downloadUrl.length,
        ) //Create file name by picking download file name from URL
        Log.e(TAG, downloadFileName)

        //Start Downloading Task
        DownloadingTask().execute()
    }
}
