/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Looper
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.lifecycle.LiveData
import com.google.android.gms.location.*

class GetLocation(private val activity: Activity) : LiveData<LocationResult>() {
    private var fusedLocationProviderClient: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(activity)
    private val locationManager: LocationManager =
        activity.applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager


    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult?) {
//            longitude = result?.lastLocation?.longitude
//            latitude = result?.lastLocation?.latitude

            super.onLocationResult(result)
        }
    }

    init {
        onGPS(activity)
    }
//    private fun isLocationEnabled(): Boolean {
//        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || locationManager.isProviderEnabled(
//            LocationManager.GPS_PROVIDER
//        )
//    }


    private fun onGPS(activity: Activity) {
        fetchLocation(activity)
//        if (!isLocationEnabled()) {
//            activity.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
//            fetchLocation(activity)
//        } else {
//            fetchLocation(activity)
//        }
    }

    private fun fetchLocation(activity: Activity) {
        if (ActivityCompat.checkSelfPermission(
                activity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                activity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                101
            )
            return
        } else {
            when (ActivityCompat.checkSelfPermission(
                activity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            )) {
                PackageManager.PERMISSION_DENIED -> {
                    // not in use
                }
                PackageManager.PERMISSION_GRANTED -> {
                    requestLocation()
                }

            }

        }
    }

    @SuppressLint("MissingPermission")
    private fun requestLocation() {
        val request = LocationRequest.create()
        request.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        fusedLocationProviderClient.requestLocationUpdates(
            request,
            locationCallback,
            Looper.myLooper()
        )
    }

}
