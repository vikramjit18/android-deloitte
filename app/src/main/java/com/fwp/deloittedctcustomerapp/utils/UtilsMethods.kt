/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar
import android.os.Environment
import android.app.DownloadManager
import android.content.ActivityNotFoundException
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.style.ImageSpan
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.fwp.deloittedctcustomerapp.R
import java.text.SimpleDateFormat
import java.util.*

import android.content.pm.PackageManager
import android.util.Log
import java.text.ParseException
import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter


object UtilsMethods {


    fun activityBack(activity: Activity) {
        activity.finish()
    }

    fun snackBarCustom(contextView: View, message: String, textColor: Int, bgColor: Int) {
        val id = R.id.nav_view
        val snackBar = Snackbar.make(contextView, message, Snackbar.LENGTH_SHORT)
        snackBar.setAnchorView(id)
        snackBar.setBackgroundTint(bgColor)
        snackBar.setTextColor(textColor)
        snackBar.show()
    }

    fun stringSpannable(
        context: Context,
        string: String,
        drawable: Int?,
        isUnderLine: Boolean
    ): SpannableStringBuilder {
        val s = SpannableStringBuilder(string)
        s.append(" ")
        s.setSpan(
            drawable?.let { ImageSpan(context, it) },
            s.length - 1,
            s.length,
            0
        )

        if (isUnderLine)
            s.setSpan(UnderlineSpan(), 0, s.length - 1, 0)

        return s
    }

    fun backStack(navController: NavController) {
        navController.popBackStack()
    }

    fun navigateToFragment(view: View, navIdInt: Int) {
        Navigation.findNavController(view).navigate(navIdInt)
    }

    fun navigateToFragmentWithValues(view: View, destinationId: Int, parameters: Bundle) {
        Navigation.findNavController(view).navigate(destinationId, parameters)
    }

    fun websiteOpener(url: String, activity: Activity) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        activity.startActivity(browserIntent)
    }

    fun dialerOpener(phoneNo: String, activity: Activity) {
        val callIntent = Intent(Intent.ACTION_DIAL)
        callIntent.data = Uri.parse("tel:+${phoneNo.trim()}")
        activity.startActivity(callIntent)
    }

    fun navigateFragmentToActivity(
        fragmentActivity: FragmentActivity,
        activityRequired: Activity, toActivity: Activity
    ) {
        val i = Intent(fragmentActivity, toActivity::class.java)
        activityRequired.startActivity(i)
        (fragmentActivity as Activity?)!!.overridePendingTransition(0, 0)

    }

    // Show email app list.
    fun openEmailApp(activity: Activity) {
        try {
            // Email app list.
            val emailAppLauncherIntents: MutableList<Intent?> = ArrayList()

            // Create intent which can handle only by email apps.
            val emailAppIntent = Intent(Intent.ACTION_SENDTO)
            emailAppIntent.data = Uri.parse("mailto:")

            // Find from all installed apps that can handle email intent and check version.
            val emailApps = activity.packageManager.queryIntentActivities(
                emailAppIntent,
                PackageManager.MATCH_ALL
            )

            // Collect email apps and put in intent list.
            for (resolveInfo in emailApps) {
                val packageName = resolveInfo.activityInfo.packageName
                val launchIntent = activity.packageManager.getLaunchIntentForPackage(packageName)
                emailAppLauncherIntents.add(launchIntent)
            }

            // Create chooser with created intent list to show email apps of device.
            val chooserIntent = Intent.createChooser(Intent(), "OPEN EMAIL APP")
            chooserIntent.putExtra(
                Intent.EXTRA_INITIAL_INTENTS,
                emailAppLauncherIntents.toTypedArray()
            )
            activity.startActivity(chooserIntent)
        } catch (e: ActivityNotFoundException) {
            // Only browser apps are available, or a browser is the default app for this intent
            toastMaker(activity, "EMAIL APP IS NOT AVAILABLE")
        }

    }

    fun downloadFile(activity: Activity, url: String, setTitle: String) {
        val downloadmanager =
            activity.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager?
        val uri =
            Uri.parse(url)

        val request = DownloadManager.Request(uri)
        request.setTitle(setTitle)
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        var downloadFileName = url.substring(
            url.lastIndexOf('/'),
            url.length,
        )
        request.setDestinationInExternalPublicDir(
            Environment.DIRECTORY_DOWNLOADS,
            downloadFileName
        )
        downloadmanager!!.enqueue(request)
    }

    fun toastMaker(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun loader(
        mContext: Context,
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.progress_layout, null)
        builder.setView(view)
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }


    fun showSimpleAlert(
        mContext: Context,
        tvPrimary: String,
        tvSecondary: String,
        primaryBtn: String
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val pBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)

        title.text = tvPrimary
        des.text = tvSecondary
        pBtn.visibility = View.GONE
        sBtn.text = primaryBtn
        builder.setView(view)

        sBtn.setOnClickListener {
            builder.dismiss()
        }

        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder?.window?.setBackgroundDrawableResource(android.R.color.background_dark)

        builder.show()
    }

    fun showAccountExpireAlert(
        mContext: Context,
        tvPrimary: String,
        tvSecondary: String,
        primaryBtn: String
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val pBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)

        title.text = tvPrimary
        des.text = tvSecondary
        pBtn.visibility = View.GONE
        sBtn.text = primaryBtn
        builder.setView(view)

        sBtn.setOnClickListener {
            builder.dismiss()
        }

        builder.setCanceledOnTouchOutside(true)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder?.window?.setBackgroundDrawableResource(android.R.color.background_dark)

        builder.show()
    }

    fun showNoNetwokDownloadDialog(
        mContext: Context,
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val pBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)

        title.text = "No network available"
        des.text =
            "You are currently offline. You must have a network connection to download E-tags."
        pBtn.visibility = View.GONE
        sBtn.text = "Ok"

        builder.setView(view)

        sBtn.setOnClickListener {
            builder.dismiss()
        }

        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder?.window?.setBackgroundDrawableResource(android.R.color.background_dark)

        builder.show()
    }

    fun showNoNetwokDialog(
        mContext: Context,
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val pBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)

        title.text = "No network available"
        des.text = "You are currently offline."
        pBtn.visibility = View.GONE
        sBtn.text = "Ok"

        builder.setView(view)

        sBtn.setOnClickListener {
            builder.dismiss()
        }

        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder?.window?.setBackgroundDrawableResource(android.R.color.background_dark)

        builder.show()
    }

    fun showNoNetwokLogoutDialog(
        mContext: Context,
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val pBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)

        title.text = "You cannot log out while in offline mode"
        des.text = "To protect your data, logout isn’t " +
            "supported while in offline mode."
        pBtn.visibility = View.GONE
        sBtn.text = "Ok"

        builder.setView(view)

        sBtn.setOnClickListener {
            builder.dismiss()
        }

        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder?.window?.setBackgroundDrawableResource(android.R.color.background_dark)

        builder.show()
    }


    fun showDialerAlert(
        mActivity: Activity,
        mContext: Context,
        tvPrimary: String,
        tvSecondary: String,
        rBtn: String,
        lBtn: String
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val leftBtn = view.findViewById<TextView>(R.id.btn_left)
        val rightBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = tvPrimary
        des.text = tvSecondary
        leftBtn.text = rBtn
        rightBtn.text = lBtn
        builder.setView(view)
        rightBtn.setOnClickListener {
            dialerOpener(mContext.getString(R.string.helpPhoneNoUri), mActivity)
            builder.dismiss()
        }
        leftBtn.setOnClickListener {
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    fun showDialerAlsAlert(
        mActivity: Activity,
        mContext: Context,
        tvPrimary: String,
        tvSecondary: String,
        primaryBtn: String,
        secBtn: String
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val pBtn = view.findViewById<TextView>(R.id.btn_left)
        val sBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = tvPrimary
        des.text = tvSecondary
        pBtn.text = primaryBtn
        sBtn.text = secBtn
        builder.setView(view)
        pBtn.setOnClickListener {
            UtilsMethods.dialerOpener(mContext.getString(R.string.helpPhoneNoUri), mActivity)
            builder.dismiss()
        }
        sBtn.setOnClickListener {
            builder.dismiss()
        }
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    fun showFragmentNavigationAlert(
        mContext: Context,
        tvPrimary: String,
        tvSecondary: String,
        leftBtn: String,
        rightBtn: String,
        mView: View,
        viewId: Int
    ) {
        val builder = AlertDialog.Builder(mContext).create()
        val view = LayoutInflater.from(mContext).inflate(R.layout.custom_dialog, null)
        val lBtn = view.findViewById<TextView>(R.id.btn_left)
        val rBtn = view.findViewById<TextView>(R.id.btn_right)
        val title = view.findViewById<TextView>(R.id.textView4)
        val des = view.findViewById<TextView>(R.id.textView5)
        title.text = tvPrimary
        des.text = tvSecondary
        lBtn.text = leftBtn
        rBtn.text = rightBtn
        builder.setView(view)
        lBtn.setOnClickListener {
            builder.dismiss()
            navigateToFragment(mView, viewId)
        }
        rBtn.setOnClickListener {

            builder.dismiss()
        }

        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        builder.show()
    }

    fun hideKeyBoard(view: View, context: Context) {
        val imm =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun isInternetConnected(context: Context) =
        (context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager).run {
            getNetworkCapabilities(activeNetwork)?.run {
                hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                    || hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    || hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
            } ?: false
        }


    // Showing custom popup showSearchAlsDialog
    fun showSearchAlsDialog(
        context: Context,
        activity: Activity
    ): androidx.appcompat.app.AlertDialog {
        val builder = androidx.appcompat.app.AlertDialog.Builder(context).create()
        val view = activity.layoutInflater.inflate(R.layout.als_link_dialog, null)
        builder.setView(view)
        builder.setCanceledOnTouchOutside(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return builder
    }

    fun getPreviousYear(): Int {
        val prevYear = Calendar.getInstance()
        prevYear.add(Calendar.YEAR, -1)
        return prevYear[Calendar.YEAR]
    }

    fun getCurrentYear(): Int {
        return Calendar.getInstance().get(Calendar.YEAR)
    }


    /**
     Monatna time standerd GMT-7
    daylight saving GMT-6
     **/
//    fun getCurrentDate(): String {
//        val date = Calendar.getInstance().time
//        val df = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
//        df.timeZone = TimeZone.getTimeZone("GMT-6")
//        val strDate = df.format(date)
//        return strDate
//    }

    fun getCurrentDateTime(): String {
        val date = Calendar.getInstance().time
        val df = SimpleDateFormat("MM/dd/yyyy  hh:mm:ss a", Locale.ENGLISH)
        df.timeZone = TimeZone.getDefault()
        val strDate = df.format(date)
        return strDate
    }

/*   fun getLocalCurrentDateTime(): String{

    }*/

//    fun getDaysBetweenDates(firstDateValue: String, secondDateValue: String): String {
//        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
//        val firstDate = sdf.parse(firstDateValue)
//        val secondDate = sdf.parse(secondDateValue)
//        if (firstDate == null || secondDate == null)
//            return 0.toString()
//
//        return (((secondDate.time - firstDate.time) / (1000 * 60 * 60 * 24)) + 1).toString()
//    }

//    fun daysBetweenDates(fromDate: String): Int {
//        // parse the date with a suitable formatter
//        val from = LocalDate.parse(fromDate)
//        // get today's date
//        val today = LocalDate.now()
//        // calculate the period between those twoe
//        val period = Period.between(from, today)
//        // and print it in a human-readable way
//        println(
//            "The difference between " + from.format(DateTimeFormatter.ISO_LOCAL_DATE)
//                + " and " + today.format(DateTimeFormatter.ISO_LOCAL_DATE) + " is "
//                + period.getYears() + " years, " + period.getMonths() + " months and "
//                + period.getDays() + " days"
//        )
//        return period.days
//    }

//    fun saveLoginDate(): LocalDate {
//        return LocalDate.now()
//    }


    fun dateDifference(startDate: Date, endDate: Date): String {
        //milliseconds

        var different = endDate.time - startDate.time
        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24
        val elapsedDays = different / daysInMilli
        different %= daysInMilli
        val elapsedHours = different / hoursInMilli
        different %= hoursInMilli
        val elapsedMinutes = different / minutesInMilli
        val elapsedSec = different/secondsInMilli
        Log.e(TAG, "dateDifference:   seconds $elapsedSec", )
        return ("$elapsedDays")
//        return ("$elapsedDays,$elapsedHours,$elapsedMinutes")
    }

//    fun getCurrentDate(): Date {
//        val c = Calendar.getInstance()
//        return c.time
//    }

    fun getCurrentDateMMMddYY(): String {
        val c = Calendar.getInstance()
        val simpleDate = SimpleDateFormat(UtilConstant.DATE_FORMAT_5)
        return simpleDate.format(c.time)
    }
    fun stringToDateConversion(s: String): Date? {
        val format = SimpleDateFormat(UtilConstant.DATE_FORMAT_5)
        try {
            return format.parse(s)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return null
    }


}
