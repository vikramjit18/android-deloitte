/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.utils

import java.util.*
import java.util.regex.Pattern


object UtilConstant {

    var getLocCount: Int =0
    var isProfileUpdate: Boolean = false
    const val GET_ALS_NO= "getAlsNO"
    const val GET_DOB= "getDOB"
    const val DATE_FORMAT_5 = "dd/MM/yyyy hh:mm:ss"
    const val GET_ITEM_ACCOUNT="getItemAccount"
    const  val BIOMETRIC_ENABLE = "biomtric_enabled"
    var biometric : Boolean =false
    var emailSent: Boolean =false
    const val DISMISSIBLE_BANNER_ETAG = "dismissibleBannerEtag"
    const val DISMISSIBLE_BANNER_ITEM = "dismissibleBannerItem"
    const val FWP_EMAIL_LOGIN = "fwpEmailForChange"
    const val USERNAME_LOGIN = "UserName_Login"
    var MINOR_NAME = ""
    var ACCOUNT_VIEWPAGER = 0
    const val NAME = "name"
    const val SNACKBAR_COUNT = "snackbarCount"
    const val snackbarCount = 0
    const val PRIMARYUSER = 0
    const val LOGIN_DATE = "login_date_1"
    const val LOGOUT_PERIOD = "logout_period"
    var PRIMARY_ACCOUNT_OWNER: String = ""
    var snackbarShown: Boolean = false
    var userName: String? = ""
    var checkViewModel: Boolean = true
    const val NA = "N/A"
    const val SUCCESS_MSG = "success"
    const val FAILURE_MSG = "failure"
    const val INVALID_MSG = "invalid"
    const val LICENSE = "license"
    const val LICENSE_LISTING = "License Listing"
    const val EYE = "eye"
    const val HAIR = "hair"
    const val WEIGHT = "weight"
    const val HEIGHT = "height"
    const val COUNTRY = "country"
    const val STATE = "state"
    const val MAIL_STATE = "mailState"
    const val GENDER = "gender"
    const val SUFFIX = "suffix"
    const val FEMALE = "Female"
    const val ITEM_POSITION = "itemPosition"
    const val LBS = "lbs"
    const val FAIL_CODE = "f"
    const val SUCCESS_CODE = "s"
    const val ERROR_CODE = "e"
    const val CURRENT_INDEX = 0
    const val PREVIOUS_INDEX = 1
    const val PREVIOUS_YEAR = "previous"
    const val CURRENT_YEAR = "current"
    const val DOWNLOAD = "D"
    const val OFFLOAD = "O"
    const val PRIMARY_ACCOUNT_KEY = "P"
    const val MULTIPLE_ACCOUNT_KEY = "S"
    const val MULTI_NAME = "multiName"
    const val MULTI_ALS_NO = "multiAlsNo"
    const val MULTI_Resident = "multiResident"
    const val MULTI_LINKED = "multiLinked"
    const val ACCOUNT_TYPE = "account_type"


    val ALS_PATTERN =
        Regex("^(((0[13-9]|1[012])[-/]?(0[1-9]|[12][0-9]|30)|(0[13578]|1[02])[-/]?31|02[-/]?(0[1-9]|1[0-9]|2[0-8]))[-/]?[0-9]{4}|02[-/]?29[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$")

    var isAlsLinked: Boolean = false
    var isAlsActivated: Boolean = false
    var multiAccount: Boolean = false
    var isAccountActivated: Boolean = false
    var DEVICE_ID: String = ""
    var alsExpiredDialog = true
    var backStatus = false

    val PASSWORD_PATTERN1: Pattern = Pattern.compile(
        "^(?=.*[a-zA-Z])(?=\\S+\$).{6,}"
    )
    val PASSWORD_PATTERN2: Pattern = Pattern.compile(
        "^(?=.*[@#\$%!\\-_?&])(?=\\S+\$).{1,}"
    )

    val PASSWORD_PATTERN3: Pattern = Pattern.compile(
        "^(?=.*[0-9])(?=\\S+\$).{1,}"
    )

    //    (?=.*[@#$%!\-_?&[^'()|\-]])
    val PASSWORD_PATTERN: Pattern = Pattern.compile(
        "^(?!.*[0-9])([a-z]*?)([A-Z]*?)(?!.*[@#\$%!\\-_?&])(?!\\S+\$).{6,}"
    )
    val EMAIL_ADDRESS_PATTERN: Pattern = Pattern.compile(
        "[a-zA-Z0-9\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+"
    )

    val USERNAME_PATTERN: Pattern = Pattern.compile("^[a-zA-Z0-9._\\-+]{6,}")
    val PHONE_PATTERN: Pattern = Pattern.compile("^\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}\$")
    val URL_PATTERN: Pattern = Pattern.compile(
        "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|fwp\\.)"
            + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
            + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
        Pattern.CASE_INSENSITIVE or Pattern.MULTILINE or Pattern.DOTALL
    )

    var abc: String = ""
    var uid: String = ""
    var itemLastIndex = 0
    var etagLastIndex = 0
    var etagViewPagerLastIndex = 0
    var itemHeldViewPagerLastIndex = 0
    var ITEMS_MULTIPLE = false
    var ETAGS_MULTIPLE = false
    var ITEMS_LAST_VIEW_INDEX = 0
    var ETAG_LAST_VIEW_INDEX = 0

    var currentYear: Int = Calendar.getInstance().get(Calendar.YEAR)

    const val dateFormat = "MM/DD/YYYY"

    const val DATE_PATTERN =
        "^(((0[13-9]|1[012])[-/]?(0[1-9]|[12][0-9]|30)|(0[13578]|1[02])[-/]?31|02[-/]?(0[1-9]|1[0-9]|2[0-8]))[-/]?[0-9]{4}|02[-/]?29[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$"

    var biometricAuthDone: Boolean = false
    const val EMAIL_TEXT = "com.google.android.gm"
    const val ALS_LINKED = "als_linked"
    const val ALS_NOT_LINKED = "als_not_linked"
    const val MULTI_ACCOUNT = "multi_account"
    const val JWT_TOKEN = "jwt"
    const val FORGOT_PASS_EMAIL_KEY = "forgot_pass_email"
    const val GET_ETAG_INDEX_KEY = "etag_index"
    const val GET_ETAG_MODEL = "etag_MODEL"
    const val GET_ETAG_ACCOUNT_OWNER = "etag_account"
    const val GET_ETAG_YEAR_KEY = "etag_year"
    const val ETAG_CON = "confirmation_number"
    const val HARVEST_TIME = "harvest_time"
    const val ETAG_OWNER = "owner"
    const val ETAG_OP = "opportunity"
    const val ETAG_DES = "description"
    const val MASTER_DATA = "masterData"
    const val USER_ETAG = "eTags"
    const val USER_PROFILE = "userProfile"
    const val CHECK_USER = "checkUser"
    const val ITEM_HELD = "itemHeld"
    const val GET_ANIMAL_NAME = "animalName"
    const val GET_ANIMAL_REGION = "animalRe"
}
