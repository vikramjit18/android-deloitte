/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.utils

object NetworkEndpoints {


//    Other bugsProduction : 443
//    Test: 483
//    Dev: 463



    const val DEV_BASE_URL = "https://mbl.fwp.mt.gov:463/Interface/myfwp/"
    const val TEST_BASE_URL = "https://mbl.fwp.mt.gov:483/Interface/myfwp/"
    const val BUG_PRODUCTION_BASE_URL = "https://mbl.fwp.mt.gov:443/Interface/myfwp/"
    const val NEW_USER_SIGNUP = "users/user"
    const val EXISTING_EMAIL_ENDPOINT = "users/emailids"
    const val EXISTING_USERNAME_ENDPOINT = "users/usernames"
    const val RESET_PASSWORD_ENDPOINT = "passwordreset"
    const val FORGET_PASSWORD_ENDPOINT = "forgotpassword"
    const val LOGIN_ENDPOINT = "login"
    const val ALS_LINKING_ENDPOINT = "alslinking"
    const val ALS_LOOKUP_ENDPOINT = "alslookup"
    const val UPDATE_USER_PROFILE_V1 = "alsdata/updateuser"
    const val UPDATE_USER_PROFILE_V2 = "alsdata/v2/updateuser"
    const val VIEW_USER_PROFILE_ENDPOINT_V1 = "alsdata/fetchuser"
    const val VIEW_USER_PROFILE_ENDPOINT_V2 = "alsdata/v2/fetchuser"
    const val ZIP_CODE_FROM_CITY = "zip"
    const val GET_ITEMS_ENDPOINTS = "itemsheld"
    const val GET_ETAGS_ENDPOINT = "getetags"
    const val GET_MASTER_DATA= "masterdata"
    const val DOWNLOAD_TAGS_ENDPOINTS = "users/tags/download"
    const val VALIDATE_TAGS_ENDPOINTS = "users/tags/validate"
    const val REQUEST_EMAIL_COPY_ENDPOINTS_PRIMARY = "users/emailitems"
    const val REQUEST_EMAIL_COPY_ENDPOINTS_SECONDARY = "users/secondary/emailitems"
    const val CHANGE_PASSWORD_ENDPOINTS = "passwordchange"
    const val CHANGE_FWP_EMAIL_ENDPOINTS = "users/changefwpemail"
    const val SHOW_USER_AGGREMENT_ENDPOINTS = "accepttou"
    const val CHECK_USER_STATUS_ENDPOINTS = "users/usrstatus"
}

