/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.utils

object DownloadStatus {

    const val OTHER_DEVICE = "O"
    const val SAME_DEVICE = "Y"
    const val NO_DOWNLOAD = "N"
}
