/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.utils

object TagStatus {

    const val ISSUED = "ISSUED"
    const val VALIDATED = "VALIDATED"
    const val EXPIRED = "EXPIRED"
    const val ISSUE = "ISSUE"
    const val CANCEL = "CANCEL"
    const val OFF_LINE_ISSUE_PENDING = "OFF-LINE ISSUE/PENDING"
    const val OFF_LINE_ISSUE_PREREQUISITE = "OFF-LINE ISSUE/PREREQUISITE"
    const val OFF_LINE_ISSUE = "OFF-LINE ISSUE"
    const val REFUND_PENDING = "REFUND/PENDING"
    const val REFUNDED = "REFUNDED"
    const val RETURN_PENDING = "RETURN/PENDING"
    const val RETURN = "RETURN"
    const val VOID_PENDING = "VOID/PENDING"
    const val VOID = "VOID"
    const val DONATED = "DONATED"

}
