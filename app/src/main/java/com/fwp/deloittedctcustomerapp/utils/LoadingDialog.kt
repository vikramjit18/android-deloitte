/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.utils

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.fwp.deloittedctcustomerapp.R


class LoadingDialog(val mActivity: Activity) {
    //    init prop
//    private lateinit var isdialog: AlertDialog
    val builder = AlertDialog.Builder(mActivity).create()
    //    start loading fn
    fun startLoading() {
        val view = mActivity.layoutInflater.inflate(R.layout.progress_layout, null)
        builder.setView(view)
        builder.setCanceledOnTouchOutside(false)
        builder.setCancelable(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        isdialog = builder
        builder.show()
    }

    //start als loading
    fun startAlsSearchLoading() {
        val view = mActivity.layoutInflater.inflate(R.layout.als_link_dialog, null)
        builder.setView(view)
        builder.setCanceledOnTouchOutside(false)
        builder.setCancelable(false)
        builder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        isdialog = builder
        builder.show()
    }

    //dismiss loading
    fun isDismiss() {
        builder.dismiss()
    }
}
