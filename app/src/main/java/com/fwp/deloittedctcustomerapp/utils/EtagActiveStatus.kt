/*
 * Montana Fish, Wildlife & Parks
 */

package com.fwp.deloittedctcustomerapp.utils

object EtagActiveStatus {
    const val YES = "Y"
    const val NO = "N"
}
