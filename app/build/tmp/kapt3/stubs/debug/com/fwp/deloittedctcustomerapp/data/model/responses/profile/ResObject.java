package com.fwp.deloittedctcustomerapp.data.model.responses.profile;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B5\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0010\b\u0001\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0005\u0012\u0010\b\u0001\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\bJ\u000b\u0010\u000e\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0011\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0005H\u00c6\u0003J\u0011\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005H\u00c6\u0003J9\u0010\u0011\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0010\b\u0003\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00052\u0010\b\u0003\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0007H\u00d6\u0001R\u0019\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0019\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\n\u00a8\u0006\u0018"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/profile/ResObject;", "", "p", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/profile/P;", "s", "", "message", "", "(Lcom/fwp/deloittedctcustomerapp/data/model/responses/profile/P;Ljava/util/List;Ljava/util/List;)V", "getMessage", "()Ljava/util/List;", "getP", "()Lcom/fwp/deloittedctcustomerapp/data/model/responses/profile/P;", "getS", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ResObject {
    @org.jetbrains.annotations.Nullable()
    private final com.fwp.deloittedctcustomerapp.data.model.responses.profile.P p = null;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.profile.P> s = null;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<java.lang.String> message = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.profile.ResObject copy(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "P")
    com.fwp.deloittedctcustomerapp.data.model.responses.profile.P p, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "S")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.profile.P> s, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "message")
    java.util.List<java.lang.String> message) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public ResObject(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "P")
    com.fwp.deloittedctcustomerapp.data.model.responses.profile.P p, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "S")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.profile.P> s, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "message")
    java.util.List<java.lang.String> message) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.profile.P component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.profile.P getP() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.profile.P> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.profile.P> getS() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getMessage() {
        return null;
    }
}