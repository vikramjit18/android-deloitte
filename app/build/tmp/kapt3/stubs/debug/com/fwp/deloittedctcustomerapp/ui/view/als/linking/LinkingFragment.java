package com.fwp.deloittedctcustomerapp.ui.view.als.linking;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0014\u001a\u00020\rJ\b\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0016H\u0002J\b\u0010\u0018\u001a\u00020\u0016H\u0002J\b\u0010\u0019\u001a\u00020\u0016H\u0002J\b\u0010\u001a\u001a\u00020\u0016H\u0002J\b\u0010\u001b\u001a\u00020\u0016H\u0002J\u0010\u0010\u001c\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\b\u0010\u001f\u001a\u00020\u0016H\u0002J\b\u0010 \u001a\u00020\u0016H\u0002J$\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010&2\b\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\b\u0010)\u001a\u00020\u0016H\u0016J\u001a\u0010*\u001a\u00020\u00162\u0006\u0010+\u001a\u00020\"2\b\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\b\u0010,\u001a\u00020\u0016H\u0002J\b\u0010-\u001a\u00020\u0016H\u0002J\b\u0010.\u001a\u00020\u0016H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/als/linking/LinkingFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/LinkingFragmentBinding;", "alsFailedCount", "", "alsViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/als/viewModel/AlsViewModel;", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/LinkingFragmentBinding;", "firstTime", "", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "mTextWatcher", "Landroid/text/TextWatcher;", "pattern", "Lkotlin/text/Regex;", "alsValidate", "apiHits", "", "buttonActivation", "buttonHandler", "clearFields", "disablePrimaryBtn", "enablePrimaryBtn", "failedObser", "it", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/als/AlsResponse;", "init", "initAlsLinking", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onViewCreated", "view", "settingButtonProperty", "successObser", "viewModelObsr", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class LinkingFragment extends androidx.fragment.app.Fragment {
    private com.fwp.deloittedctcustomerapp.databinding.LinkingFragmentBinding _binding;
    private final kotlin.text.Regex pattern = null;
    private com.fwp.deloittedctcustomerapp.ui.view.als.viewModel.AlsViewModel alsViewModel;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private int alsFailedCount = 0;
    private boolean firstTime = true;
    private final android.text.TextWatcher mTextWatcher = null;
    
    public LinkingFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.LinkingFragmentBinding getBinding() {
        return null;
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void init() {
    }
    
    private final void settingButtonProperty() {
    }
    
    private final void buttonHandler() {
    }
    
    public final boolean alsValidate() {
        return false;
    }
    
    private final void buttonActivation() {
    }
    
    private final void initAlsLinking() {
    }
    
    private final void apiHits() {
    }
    
    private final void viewModelObsr() {
    }
    
    private final void successObser() {
    }
    
    private final void failedObser(com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse it) {
    }
    
    private final void clearFields() {
    }
    
    private final void disablePrimaryBtn() {
    }
    
    private final void enablePrimaryBtn() {
    }
}