package com.fwp.deloittedctcustomerapp.ui.view.forgot;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u001b\u001a\u00020\u001c2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00070\u001eH\u0002J\u000e\u0010\u001f\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020!J\u000e\u0010\"\u001a\u00020\u001c2\u0006\u0010#\u001a\u00020$R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0017\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00070\u00128F\u00a2\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00070\u00128F\u00a2\u0006\u0006\u001a\u0004\b\u0016\u0010\u0014R\u001d\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\u00128F\u00a2\u0006\u0006\u001a\u0004\b\u0018\u0010\u0014R\u001d\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\u00128F\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u0014\u00a8\u0006%"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/forgot/ForgotViewModel;", "Landroidx/lifecycle/ViewModel;", "mainRepo", "Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;", "(Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;)V", "_forgotPassResponse", "Landroidx/lifecycle/MutableLiveData;", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/forgotPass/ForgotResponse;", "_resetPassResponse", "_toastForgotPass", "Lcom/fwp/deloittedctcustomerapp/utils/Event;", "", "_toastResetPass", "coroutineExceptionHandler", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "getCoroutineExceptionHandler", "()Lkotlinx/coroutines/CoroutineExceptionHandler;", "forgotPassResponse", "Landroidx/lifecycle/LiveData;", "getForgotPassResponse", "()Landroidx/lifecycle/LiveData;", "resetPassResponse", "getResetPassResponse", "toastForgotPass", "getToastForgotPass", "toastResetPass", "getToastResetPass", "forgotPAssResponse", "", "response", "Lretrofit2/Response;", "forgotPassRequest", "forgotPasswordRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ForgotPasswordRequest;", "resetPassRequest", "resetPasswordRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ResetPasswordRequest;", "app_debug"})
public final class ForgotViewModel extends androidx.lifecycle.ViewModel {
    private final com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse> _forgotPassResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse> _resetPassResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastForgotPass = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastResetPass = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.CoroutineExceptionHandler coroutineExceptionHandler = null;
    
    @javax.inject.Inject()
    public ForgotViewModel(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse> getForgotPassResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse> getResetPassResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastForgotPass() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastResetPass() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.CoroutineExceptionHandler getCoroutineExceptionHandler() {
        return null;
    }
    
    public final void forgotPassRequest(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.ForgotPasswordRequest forgotPasswordRequest) {
    }
    
    private final void forgotPAssResponse(retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse> response) {
    }
    
    public final void resetPassRequest(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.ResetPasswordRequest resetPasswordRequest) {
    }
}