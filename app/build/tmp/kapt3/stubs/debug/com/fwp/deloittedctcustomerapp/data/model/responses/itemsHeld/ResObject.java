package com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B)\u0012\u0010\b\u0001\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u0012\u0010\b\u0001\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0007J\u0011\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0011\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J-\u0010\u000f\u001a\u00020\u00002\u0010\b\u0003\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\u0010\b\u0003\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0006H\u00d6\u0001R\"\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u0019\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t\u00a8\u0006\u0016"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/ResObject;", "", "accounts", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/Account;", "message", "", "(Ljava/util/List;Ljava/util/List;)V", "getAccounts", "()Ljava/util/List;", "setAccounts", "(Ljava/util/List;)V", "getMessage", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ResObject {
    @org.jetbrains.annotations.Nullable()
    private java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account> accounts;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<java.lang.String> message = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ResObject copy(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "accounts")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account> accounts, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "message")
    java.util.List<java.lang.String> message) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public ResObject(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "accounts")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account> accounts, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "message")
    java.util.List<java.lang.String> message) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account> getAccounts() {
        return null;
    }
    
    public final void setAccounts(@org.jetbrains.annotations.Nullable()
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getMessage() {
        return null;
    }
}