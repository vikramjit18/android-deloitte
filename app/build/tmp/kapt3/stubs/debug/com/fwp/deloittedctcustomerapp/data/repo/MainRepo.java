package com.fwp.deloittedctcustomerapp.data.repo;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u00a0\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ\'\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\b2\u0006\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u0011H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0012J\'\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00140\b2\u0006\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\u0016H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017J\u001f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\b2\u0006\u0010\u001a\u001a\u00020\u001bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001cJ\u001f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00190\b2\u0006\u0010\u001e\u001a\u00020\u001fH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010 J\u001f\u0010!\u001a\b\u0012\u0004\u0012\u00020\"0\b2\u0006\u0010\u000f\u001a\u00020\u000bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ\u001f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00190\b2\u0006\u0010$\u001a\u00020%H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010&J\u001f\u0010\'\u001a\b\u0012\u0004\u0012\u00020(0\b2\u0006\u0010)\u001a\u00020*H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010+J\u001f\u0010,\u001a\b\u0012\u0004\u0012\u00020-0\b2\u0006\u0010.\u001a\u00020/H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00100J\'\u00101\u001a\b\u0012\u0004\u0012\u0002020\b2\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u00020\u000bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00106J\u001f\u00107\u001a\b\u0012\u0004\u0012\u0002080\b2\u0006\u00109\u001a\u00020:H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010;J\u001f\u0010<\u001a\b\u0012\u0004\u0012\u00020=0\b2\u0006\u0010\u000f\u001a\u00020\u000bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ\u0017\u0010>\u001a\b\u0012\u0004\u0012\u00020?0\bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010@J\u0012\u0010A\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020D0C0BJ\u001f\u0010E\u001a\b\u0012\u0004\u0012\u00020F0\b2\u0006\u0010\u000f\u001a\u00020\u000bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ\u0019\u0010G\u001a\u00020H2\u0006\u0010I\u001a\u00020DH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010JJ\'\u0010K\u001a\b\u0012\u0004\u0012\u00020L0\b2\u0006\u0010\u000f\u001a\u00020\u000b2\u0006\u0010M\u001a\u00020NH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010OJ\u001f\u0010P\u001a\b\u0012\u0004\u0012\u00020Q0\b2\u0006\u0010R\u001a\u00020SH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010TJ\'\u0010U\u001a\b\u0012\u0004\u0012\u00020L0\b2\u0006\u0010\u000f\u001a\u00020\u000b2\u0006\u0010V\u001a\u00020WH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010XJ\'\u0010Y\u001a\b\u0012\u0004\u0012\u00020Z0\b2\u0006\u0010\u000f\u001a\u00020\u000b2\u0006\u0010[\u001a\u00020\\H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010]J\'\u0010^\u001a\b\u0012\u0004\u0012\u00020Z0\b2\u0006\u0010\u000f\u001a\u00020\u000b2\u0006\u0010_\u001a\u00020`H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010aJ\u001f\u0010b\u001a\b\u0012\u0004\u0012\u00020-0\b2\u0006\u0010c\u001a\u00020dH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010eJ\'\u0010f\u001a\b\u0012\u0004\u0012\u00020F0\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010g\u001a\u00020hH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010iJ\u001f\u0010j\u001a\b\u0012\u0004\u0012\u00020k0\b2\u0006\u0010I\u001a\u00020DH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010JR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006l"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;", "", "apiHelper", "Lcom/fwp/deloittedctcustomerapp/data/api/ApiHelper;", "appDao", "Lcom/fwp/deloittedctcustomerapp/data/db/AppDao;", "(Lcom/fwp/deloittedctcustomerapp/data/api/ApiHelper;Lcom/fwp/deloittedctcustomerapp/data/db/AppDao;)V", "acceptTOU", "Lretrofit2/Response;", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/agreement/AgreemnetResponse;", "token", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "changeFwpEmail", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/changeFwpEmail/ChangeFwpEmailResponse;", "jwtToken", "changeFwpEmailRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ChangeFwpEmailRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/ChangeFwpEmailRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "changePassword", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/changePasswordResponse/ChangePasswordResponse;", "changePasswordRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ChangePasswordRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/ChangePasswordRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "checkExistingEmail", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/signup/CheckCredsResponse;", "checkEmailRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/CheckEmailRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/CheckEmailRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "checkExistingUsername", "checkUsernameRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/CheckUsernameRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/CheckUsernameRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "checkUserStatus", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/userStatus/UserStatusResponse;", "createNewUser", "signupRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/SignupRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/SignupRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "downloadOffLoadEtag", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/downloadOffload/DownloadOffloadResponse;", "downloadOffloadRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/DownloadOffloadRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/DownloadOffloadRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "forgotPassword", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/forgotPass/ForgotResponse;", "forgotPasswordRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ForgotPasswordRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/ForgotPasswordRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getCityNameFromZipcode", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/zipCode/ZipcodeResponse;", "zipcode", "", "isResident", "(ILjava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getEtags", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagResponse;", "etagRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/EtagRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/EtagRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getItemHeld", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/ItemsHeldResponse;", "getMasterData", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/MasterData;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getValiRecords", "Landroidx/lifecycle/LiveData;", "", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ValidateEtagRequest;", "getViewUserProfileDetails", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/profile/UserProfileResponse;", "insertValiTagRecord", "", "validateEtagRequest", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/ValidateEtagRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "linkAls", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/als/AlsResponse;", "alsLinkingRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/AlsLinkingRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/AlsLinkingRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "login", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/login/LoginResponse;", "loginRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/LoginRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/LoginRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "lookUpAls", "alsLookupRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/AlsLookupRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/AlsLookupRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "requestEmailCopyPrimary", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/requestEmail/RequestEmailResponse;", "requestCopyPrimaryRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/RequestCopyPrimaryRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/RequestCopyPrimaryRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "requestEmailCopySecondary", "requestCopySecondaryRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/RequestCopySecondaryRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/RequestCopySecondaryRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "resetPassword", "resetPasswordRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ResetPasswordRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/ResetPasswordRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateUserProfile", "updateUserProfileRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/UpdateUserProfileRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/UpdateUserProfileRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "validateEtag", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/validateEtag/ValidateEtagResponse;", "app_debug"})
@javax.inject.Singleton()
public final class MainRepo {
    private final com.fwp.deloittedctcustomerapp.data.api.ApiHelper apiHelper = null;
    private final com.fwp.deloittedctcustomerapp.data.db.AppDao appDao = null;
    
    @javax.inject.Inject()
    public MainRepo(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.api.ApiHelper apiHelper, @org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.db.AppDao appDao) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest>> getValiRecords() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object insertValiTagRecord(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest validateEtagRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object createNewUser(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.SignupRequest signupRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object login(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.LoginRequest loginRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object acceptTOU(@org.jetbrains.annotations.NotNull()
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.agreement.AgreemnetResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object forgotPassword(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.ForgotPasswordRequest forgotPasswordRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object resetPassword(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.ResetPasswordRequest resetPasswordRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object checkExistingUsername(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.CheckUsernameRequest checkUsernameRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object checkExistingEmail(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.CheckEmailRequest checkEmailRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object changePassword(@org.jetbrains.annotations.NotNull()
    java.lang.String jwtToken, @org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.ChangePasswordRequest changePasswordRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ChangePasswordResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object changeFwpEmail(@org.jetbrains.annotations.NotNull()
    java.lang.String jwtToken, @org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.ChangeFwpEmailRequest changeFwpEmailRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.changeFwpEmail.ChangeFwpEmailResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object requestEmailCopyPrimary(@org.jetbrains.annotations.NotNull()
    java.lang.String jwtToken, @org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopyPrimaryRequest requestCopyPrimaryRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object requestEmailCopySecondary(@org.jetbrains.annotations.NotNull()
    java.lang.String jwtToken, @org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopySecondaryRequest requestCopySecondaryRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getViewUserProfileDetails(@org.jetbrains.annotations.NotNull()
    java.lang.String jwtToken, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object checkUserStatus(@org.jetbrains.annotations.NotNull()
    java.lang.String jwtToken, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.UserStatusResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getMasterData(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.masterData.MasterData>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object updateUserProfile(@org.jetbrains.annotations.NotNull()
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.UpdateUserProfileRequest updateUserProfileRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object linkAls(@org.jetbrains.annotations.NotNull()
    java.lang.String jwtToken, @org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.AlsLinkingRequest alsLinkingRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object lookUpAls(@org.jetbrains.annotations.NotNull()
    java.lang.String jwtToken, @org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.AlsLookupRequest alsLookupRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getCityNameFromZipcode(int zipcode, @org.jetbrains.annotations.NotNull()
    java.lang.String isResident, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getEtags(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest etagRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object validateEtag(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest validateEtagRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.validateEtag.ValidateEtagResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object downloadOffLoadEtag(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest downloadOffloadRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getItemHeld(@org.jetbrains.annotations.NotNull()
    java.lang.String jwtToken, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse>> continuation) {
        return null;
    }
}