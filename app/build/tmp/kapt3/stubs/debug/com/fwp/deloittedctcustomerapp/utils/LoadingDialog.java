package com.fwp.deloittedctcustomerapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\f\u001a\u00020\rJ\u0006\u0010\u000e\u001a\u00020\rJ\u0006\u0010\u000f\u001a\u00020\rR\u0019\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u0010"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "", "mActivity", "Landroid/app/Activity;", "(Landroid/app/Activity;)V", "builder", "Landroid/app/AlertDialog;", "kotlin.jvm.PlatformType", "getBuilder", "()Landroid/app/AlertDialog;", "getMActivity", "()Landroid/app/Activity;", "isDismiss", "", "startAlsSearchLoading", "startLoading", "app_debug"})
public final class LoadingDialog {
    @org.jetbrains.annotations.NotNull()
    private final android.app.Activity mActivity = null;
    private final android.app.AlertDialog builder = null;
    
    public LoadingDialog(@org.jetbrains.annotations.NotNull()
    android.app.Activity mActivity) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Activity getMActivity() {
        return null;
    }
    
    public final android.app.AlertDialog getBuilder() {
        return null;
    }
    
    public final void startLoading() {
    }
    
    public final void startAlsSearchLoading() {
    }
    
    public final void isDismiss() {
    }
}