package com.fwp.deloittedctcustomerapp.data.model.responses;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/EtagModel;", "", "year", "", "list", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/EtagDetail;", "(Ljava/lang/String;Ljava/util/List;)V", "getList", "()Ljava/util/List;", "getYear", "()Ljava/lang/String;", "app_debug"})
public final class EtagModel {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String year = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.EtagDetail> list = null;
    
    public EtagModel(@org.jetbrains.annotations.NotNull()
    java.lang.String year, @org.jetbrains.annotations.NotNull()
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.EtagDetail> list) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getYear() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.EtagDetail> getList() {
        return null;
    }
}