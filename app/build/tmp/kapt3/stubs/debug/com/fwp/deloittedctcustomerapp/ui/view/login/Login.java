package com.fwp.deloittedctcustomerapp.ui.view.login;

import java.lang.System;

/**
 * FWP
 * Hilt android entry
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0019\u001a\u00020\u001aH\u0002J\b\u0010\u001b\u001a\u00020\u001aH\u0002J\b\u0010\u001c\u001a\u00020\u001aH\u0002J\b\u0010\u001d\u001a\u00020\u001aH\u0002J\b\u0010\u001e\u001a\u00020\u001aH\u0002J\b\u0010\u001f\u001a\u00020\u001aH\u0002J\b\u0010 \u001a\u00020\u001aH\u0002J\u0010\u0010!\u001a\u00020\u001a2\u0006\u0010\"\u001a\u00020#H\u0002J\b\u0010$\u001a\u00020%H\u0002J\b\u0010&\u001a\u00020\u001aH\u0002J\"\u0010\'\u001a\u0004\u0018\u00010(2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020%2\u0006\u0010,\u001a\u00020*H\u0016J$\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u0002002\b\u00101\u001a\u0004\u0018\u0001022\b\u00103\u001a\u0004\u0018\u000104H\u0016J\b\u00105\u001a\u00020\u001aH\u0016J\u001a\u00106\u001a\u00020\u001a2\u0006\u00107\u001a\u00020.2\b\u00103\u001a\u0004\u0018\u000104H\u0016J\b\u00108\u001a\u00020\u001aH\u0003J\b\u00109\u001a\u00020\u001aH\u0003J\b\u0010:\u001a\u00020\u001aH\u0002J\b\u0010;\u001a\u00020\u001aH\u0002J\b\u0010<\u001a\u00020\u001aH\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0011\u001a\u00020\u00128BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0013\u0010\u0014R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006="}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/login/Login;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/FragmentLoginBinding;", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/FragmentLoginBinding;", "setBinding", "(Lcom/fwp/deloittedctcustomerapp/databinding/FragmentLoginBinding;)V", "biometricPrompt", "Landroidx/biometric/BiometricPrompt;", "executor", "Ljava/util/concurrent/Executor;", "keyguard", "Landroid/app/KeyguardManager;", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "loginViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/login/LoginViewModel;", "getLoginViewModel", "()Lcom/fwp/deloittedctcustomerapp/ui/view/login/LoginViewModel;", "loginViewModel$delegate", "Lkotlin/Lazy;", "promptInfo", "Landroidx/biometric/BiometricPrompt$PromptInfo;", "apiHitLogin", "", "backButton", "biometricAuthenticate", "biometricSwitch", "enablePrimaryBtn", "forgotButton", "init", "initiateLoginProcess", "loginResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/login/LoginResponse;", "isFieldNotEmpty", "", "loginBtnHandler", "onCreateAnimation", "Landroid/view/animation/Animation;", "transit", "", "enter", "nextAnim", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onViewCreated", "view", "showBiometricAlert", "showResetPasswordAlert", "signUpButton", "signUpSpan", "viewModelObser", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class Login extends androidx.fragment.app.Fragment {
    
    /**
     * Global variables
     */
    public com.fwp.deloittedctcustomerapp.databinding.FragmentLoginBinding binding;
    private androidx.biometric.BiometricPrompt biometricPrompt;
    private androidx.biometric.BiometricPrompt.PromptInfo promptInfo;
    private final kotlin.Lazy loginViewModel$delegate = null;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private java.util.concurrent.Executor executor;
    private android.app.KeyguardManager keyguard;
    
    public Login() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.databinding.FragmentLoginBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.databinding.FragmentLoginBinding p0) {
    }
    
    private final com.fwp.deloittedctcustomerapp.ui.view.login.LoginViewModel getLoginViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.animation.Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    /**
     * Initiate
     * init Function
     */
    private final void init() {
    }
    
    /**
     * Biometric
     * switch button
     */
    private final void biometricSwitch() {
    }
    
    /**
     * Initiate
     * Biometric function
     */
    private final void biometricAuthenticate() {
    }
    
    /**
     * Showing Biometric
     * Alert Dialog
     */
    @kotlin.OptIn(markerClass = {androidx.compose.ui.text.android.InternalPlatformTextApi.class})
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    private final void showBiometricAlert() {
    }
    
    /**
     * Showing reset password
     * Alert Dialog
     */
    @kotlin.OptIn(markerClass = {androidx.compose.ui.text.android.InternalPlatformTextApi.class})
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    private final void showResetPasswordAlert() {
    }
    
    /**
     * Initiate
     * forget button function
     */
    private final void forgotButton() {
    }
    
    /**
     * Initiate
     * signup button function
     */
    private final void signUpButton() {
    }
    
    /**
     * Initiate
     * close button function
     */
    private final void backButton() {
    }
    
    /**
     * Setting
     * Signup text
     */
    @kotlin.OptIn(markerClass = {androidx.compose.ui.text.android.InternalPlatformTextApi.class})
    private final void signUpSpan() {
    }
    
    /**
     * Initiate
     * button activation function
     */
    private final void loginBtnHandler() {
    }
    
    private final void apiHitLogin() {
    }
    
    private final void viewModelObser() {
    }
    
    private final void initiateLoginProcess(com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse loginResponse) {
    }
    
    /**
     * create
     * a textWatcher member
     */
    private final void enablePrimaryBtn() {
    }
    
    private final boolean isFieldNotEmpty() {
        return false;
    }
}