package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0088\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0006\u0010\u0019\u001a\u00020\u0016J\b\u0010\u001a\u001a\u00020\u0016H\u0002J\b\u0010\u001b\u001a\u00020\u0016H\u0002J\b\u0010\u001c\u001a\u00020\u0016H\u0002J\b\u0010\u001d\u001a\u00020\u0016H\u0002J\"\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020!H\u0016J$\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(2\b\u0010)\u001a\u0004\u0018\u00010*2\b\u0010+\u001a\u0004\u0018\u00010,H\u0017J\b\u0010-\u001a\u00020\u0016H\u0016J\u001a\u0010.\u001a\u00020\u00162\u0006\u0010/\u001a\u00020&2\b\u0010+\u001a\u0004\u0018\u00010,H\u0016J\u0016\u00100\u001a\u00020\u00162\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e01H\u0002J\u0010\u00102\u001a\u00020\u00162\u0006\u00103\u001a\u000204H\u0003J\b\u00105\u001a\u00020\u0016H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00066"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/ProfileFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/FragmentProfileBinding;", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/FragmentProfileBinding;", "gson", "Lcom/google/gson/Gson;", "landingScreenActivityViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "list", "", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/viewdetails/adapter/Profile;", "listOffLoad", "", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "profileViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/ProfileViewModel;", "apioffloadTag", "", "offloadRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/DownloadOffloadRequest;", "buttonHandler", "hideAlsView", "init", "logoutDialog", "offloadObsr", "onCreateAnimation", "Landroid/view/animation/Animation;", "transit", "", "enter", "", "nextAnim", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onViewCreated", "view", "recyclerContent", "", "setupUI", "it", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/profile/UserProfileResponse;", "showAlsView", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class ProfileFragment extends androidx.fragment.app.Fragment {
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.ProfileViewModel profileViewModel;
    private com.fwp.deloittedctcustomerapp.databinding.FragmentProfileBinding _binding;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel landingScreenActivityViewModel;
    private final java.util.List<com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.adapter.Profile> list = null;
    private final com.google.gson.Gson gson = null;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private final java.util.List<java.lang.String> listOffLoad = null;
    
    public ProfileFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.FragmentProfileBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.animation.Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return null;
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    private final void setupUI(com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse it) {
    }
    
    private final void recyclerContent(java.util.List<com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.adapter.Profile> list) {
    }
    
    private final void init() {
    }
    
    public final void buttonHandler() {
    }
    
    private final void logoutDialog() {
    }
    
    private final void apioffloadTag(com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest offloadRequest) {
    }
    
    private final void hideAlsView() {
    }
    
    private final void showAlsView() {
    }
    
    private final void offloadObsr() {
    }
}