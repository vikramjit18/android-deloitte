package com.fwp.deloittedctcustomerapp.data.db;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\bg\u0018\u00002\u00020\u0001J\u0019\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\u0003H\'J\u0011\u0010\b\u001a\u00020\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u0014\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f0\u000bH\'J\u0014\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\f0\u000bH\'J\u0014\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\f0\u000bH\'J\u0019\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\rH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013J\u0019\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u000fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0017"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/db/AppDao;", "", "deleteDownloadedTagsById", "", "onlineSpiId", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "deleteDownloadedTagsTable", "deleteValidateRequest", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getDownloadedTags", "Landroidx/lifecycle/LiveData;", "", "Lcom/fwp/deloittedctcustomerapp/data/model/DownloadedTags;", "getUnSyncedValidateRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ValidateEtagRequest;", "getValidateRequest", "insertDownloadTags", "downloadedTags", "(Lcom/fwp/deloittedctcustomerapp/data/model/DownloadedTags;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "insertValidateRequest", "validateEtagRequest", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/ValidateEtagRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface AppDao {
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM validate_request")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest>> getValidateRequest();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM validate_request where sync=0")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest>> getUnSyncedValidateRequest();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM downloaded_tags")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.fwp.deloittedctcustomerapp.data.model.DownloadedTags>> getDownloadedTags();
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract java.lang.Object insertValidateRequest(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest validateEtagRequest, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract java.lang.Object insertDownloadTags(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.DownloadedTags downloadedTags, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Query(value = "DELETE FROM validate_request")
    public abstract java.lang.Object deleteValidateRequest(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation);
    
    @androidx.room.Query(value = "DELETE FROM downloaded_tags")
    public abstract void deleteDownloadedTagsTable();
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Query(value = "DELETE FROM downloaded_tags where downloadedSpiId = :onlineSpiId")
    public abstract java.lang.Object deleteDownloadedTagsById(@org.jetbrains.annotations.NotNull()
    java.lang.String onlineSpiId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation);
}