package com.fwp.deloittedctcustomerapp.data.model.responses.userStatus;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0011\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B?\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0003\u0012\u0010\b\u0001\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\u0011\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bH\u00c6\u0003JC\u0010\u0017\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00032\b\b\u0003\u0010\u0006\u001a\u00020\u00032\u0010\b\u0003\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00032\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\tH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR\u0019\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\f\u00a8\u0006\u001d"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/userStatus/ResObject;", "", "accountActivated", "", "alsActivated", "alsLinked", "touAccepted", "message", "", "", "(ZZZZLjava/util/List;)V", "getAccountActivated", "()Z", "getAlsActivated", "getAlsLinked", "getMessage", "()Ljava/util/List;", "getTouAccepted", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "", "toString", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ResObject {
    private final boolean accountActivated = false;
    private final boolean alsActivated = false;
    private final boolean alsLinked = false;
    private final boolean touAccepted = false;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<java.lang.String> message = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.ResObject copy(@com.squareup.moshi.Json(name = "accountActivated")
    boolean accountActivated, @com.squareup.moshi.Json(name = "alsActivated")
    boolean alsActivated, @com.squareup.moshi.Json(name = "alsLinked")
    boolean alsLinked, @com.squareup.moshi.Json(name = "touAccepted")
    boolean touAccepted, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "message")
    java.util.List<java.lang.String> message) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public ResObject(@com.squareup.moshi.Json(name = "accountActivated")
    boolean accountActivated, @com.squareup.moshi.Json(name = "alsActivated")
    boolean alsActivated, @com.squareup.moshi.Json(name = "alsLinked")
    boolean alsLinked, @com.squareup.moshi.Json(name = "touAccepted")
    boolean touAccepted, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "message")
    java.util.List<java.lang.String> message) {
        super();
    }
    
    public final boolean component1() {
        return false;
    }
    
    public final boolean getAccountActivated() {
        return false;
    }
    
    public final boolean component2() {
        return false;
    }
    
    public final boolean getAlsActivated() {
        return false;
    }
    
    public final boolean component3() {
        return false;
    }
    
    public final boolean getAlsLinked() {
        return false;
    }
    
    public final boolean component4() {
        return false;
    }
    
    public final boolean getTouAccepted() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getMessage() {
        return null;
    }
}