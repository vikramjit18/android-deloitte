package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u0000 <2\u00020\u0001:\u0001<B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0014\u001a\u00020\u0015H\u0002J\b\u0010\u0016\u001a\u00020\u0015H\u0002J\u0010\u0010\u0017\u001a\u00020\n2\u0006\u0010\u0018\u001a\u00020\nH\u0002J\u0010\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\"\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u001fH\u0016J$\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&2\b\u0010\'\u001a\u0004\u0018\u00010(2\b\u0010)\u001a\u0004\u0018\u00010*H\u0016J\b\u0010+\u001a\u00020\u0015H\u0016J\u001a\u0010,\u001a\u00020\u00152\u0006\u0010-\u001a\u00020$2\b\u0010)\u001a\u0004\u0018\u00010*H\u0016J\u0010\u0010.\u001a\u00020\u00152\u0006\u0010/\u001a\u000200H\u0003J\u0018\u00101\u001a\u00020\n2\u0006\u00102\u001a\u00020\n2\u0006\u00103\u001a\u00020\nH\u0002J\u0010\u00104\u001a\u00020\n2\u0006\u00105\u001a\u00020\nH\u0002J \u00106\u001a\u00020\u00152\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020$2\u0006\u0010:\u001a\u00020\u001fH\u0002J\b\u0010;\u001a\u00020\u0015H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006="}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/viewdetails/primary/PrimaryAccountFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/PrimaryAccountFragmentBinding;", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/PrimaryAccountFragmentBinding;", "eyeCodeList", "Ljava/util/ArrayList;", "", "eyeItemsList", "genderCodeList", "genderItemsList", "hairCodeList", "hairItemsList", "landingScreenActivityViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "buttonHandler", "", "clearListDropdownList", "getMaskedNumber", "number", "maserDataList", "masterResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/MasterData;", "onCreateAnimation", "Landroid/view/animation/Animation;", "transit", "", "enter", "", "nextAnim", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onViewCreated", "view", "setUpView", "profileResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/profile/UserProfileResponse;", "setupEyeAndHairColor", "position", "of", "setupGender", "code", "showFragmentNavigationAlert", "mContext", "Landroid/content/Context;", "mView", "viewId", "viewModelObsr", "Companion", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class PrimaryAccountFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.PrimaryAccountFragment.Companion Companion = null;
    private com.fwp.deloittedctcustomerapp.databinding.PrimaryAccountFragmentBinding _binding;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel landingScreenActivityViewModel;
    private final java.util.ArrayList<java.lang.String> eyeItemsList = null;
    private final java.util.ArrayList<java.lang.String> eyeCodeList = null;
    private final java.util.ArrayList<java.lang.String> hairItemsList = null;
    private final java.util.ArrayList<java.lang.String> hairCodeList = null;
    private final java.util.ArrayList<java.lang.String> genderItemsList = null;
    private final java.util.ArrayList<java.lang.String> genderCodeList = null;
    
    public PrimaryAccountFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.PrimaryAccountFragmentBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.animation.Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return null;
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void viewModelObsr() {
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    private final void setUpView(com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse profileResponse) {
    }
    
    private final java.lang.String setupGender(java.lang.String code) {
        return null;
    }
    
    private final void maserDataList(com.fwp.deloittedctcustomerapp.data.model.responses.masterData.MasterData masterResponse) {
    }
    
    private final java.lang.String setupEyeAndHairColor(java.lang.String position, java.lang.String of) {
        return null;
    }
    
    @kotlin.OptIn(markerClass = {androidx.compose.ui.text.android.InternalPlatformTextApi.class})
    private final void buttonHandler() {
    }
    
    private final java.lang.String getMaskedNumber(java.lang.String number) {
        return null;
    }
    
    private final void clearListDropdownList() {
    }
    
    private final void showFragmentNavigationAlert(android.content.Context mContext, android.view.View mView, int viewId) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/viewdetails/primary/PrimaryAccountFragment$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/viewdetails/primary/PrimaryAccountFragment;", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.PrimaryAccountFragment newInstance() {
            return null;
        }
    }
}