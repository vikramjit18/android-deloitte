package com.fwp.deloittedctcustomerapp.data.model.entities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\bu\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B\u00cd\u0003\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0011\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0013\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0014\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0015\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0016\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0017\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0018\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0019\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u001a\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u001b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u001c\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u001d\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u001e\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u001f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010 \u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010!\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\"\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010#\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010$\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010%\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010&\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\'\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010(\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010)J\u000b\u0010Q\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010R\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010S\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010T\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010U\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010V\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010W\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010X\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010Y\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010Z\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010[\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\\\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010]\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010^\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010_\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010`\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010a\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010c\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010d\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010e\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010g\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010h\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010i\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010j\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010k\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010l\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010m\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010n\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010o\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010p\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010q\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010r\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010s\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010t\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010u\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010v\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u00d1\u0003\u0010w\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\t\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\f\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\r\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u000e\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0010\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0011\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0012\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0013\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0014\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0015\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0016\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0017\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0018\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0019\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u001a\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u001b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u001c\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u001d\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u001e\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u001f\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010 \u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010!\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\"\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010#\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010$\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010%\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010&\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\'\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010(\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010x\u001a\u00020y2\b\u0010z\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010{\u001a\u00020|H\u00d6\u0001J\t\u0010}\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010+R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b,\u0010+R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b-\u0010+R\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010+R\u0013\u0010\u0013\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b/\u0010+R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b0\u0010+R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u0010+R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b2\u0010+R\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b3\u0010+R\u0013\u0010\u0016\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b4\u0010+R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b5\u0010+R\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b6\u0010+R\u0013\u0010\u0018\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b7\u0010+R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b8\u0010+R\u0013\u0010\u0019\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b9\u0010+R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b:\u0010+R\u0013\u0010\u001a\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b;\u0010+R\u0013\u0010\u001b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b<\u0010+R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b=\u0010+R\u0013\u0010\u001d\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b>\u0010+R\u0013\u0010\u001e\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b?\u0010+R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b@\u0010+R\u0013\u0010\u001f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bA\u0010+R\u0013\u0010 \u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bB\u0010+R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bC\u0010+R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bD\u0010+R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bE\u0010+R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bF\u0010+R\u0013\u0010!\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bG\u0010+R\u0013\u0010\"\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bH\u0010+R\u0013\u0010#\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bI\u0010+R\u0013\u0010$\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bJ\u0010+R\u0013\u0010%\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bK\u0010+R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bL\u0010+R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bM\u0010+R\u0013\u0010&\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bN\u0010+R\u0013\u0010\'\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bO\u0010+R\u0013\u0010(\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bP\u0010+\u00a8\u0006~"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/entities/User;", "", "emailId", "", "firstName", "lastName", "middleName", "password", "username", "uid", "confirmNewFwpEmail", "newFwpEmail", "confirmNewPassword", "currentPassword", "newPassword", "addrLine1", "addrLine2", "addrType", "alsNo", "city", "clubMember500", "contactViaEmailId", "country", "dob", "email", "eyeColorCd", "firstNm", "hairColorCd", "heightFeet", "heightInches", "homePhone", "lastNm", "midInit", "poBoxInd", "residencyStatus", "sexCd", "state", "suffix", "weight", "workPhone", "zipCd", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAddrLine1", "()Ljava/lang/String;", "getAddrLine2", "getAddrType", "getAlsNo", "getCity", "getClubMember500", "getConfirmNewFwpEmail", "getConfirmNewPassword", "getContactViaEmailId", "getCountry", "getCurrentPassword", "getDob", "getEmail", "getEmailId", "getEyeColorCd", "getFirstName", "getFirstNm", "getHairColorCd", "getHeightFeet", "getHeightInches", "getHomePhone", "getLastName", "getLastNm", "getMidInit", "getMiddleName", "getNewFwpEmail", "getNewPassword", "getPassword", "getPoBoxInd", "getResidencyStatus", "getSexCd", "getState", "getSuffix", "getUid", "getUsername", "getWeight", "getWorkPhone", "getZipCd", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component24", "component25", "component26", "component27", "component28", "component29", "component3", "component30", "component31", "component32", "component33", "component34", "component35", "component36", "component37", "component38", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class User {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String emailId = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String firstName = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String lastName = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String middleName = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String password = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String username = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String uid = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String confirmNewFwpEmail = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String newFwpEmail = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String confirmNewPassword = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String currentPassword = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String newPassword = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String addrLine1 = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String addrLine2 = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String addrType = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String alsNo = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String city = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String clubMember500 = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String contactViaEmailId = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String country = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String dob = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String email = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String eyeColorCd = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String firstNm = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String hairColorCd = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String heightFeet = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String heightInches = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String homePhone = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String lastNm = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String midInit = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String poBoxInd = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String residencyStatus = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String sexCd = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String state = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String suffix = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String weight = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String workPhone = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String zipCd = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.entities.User copy(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "emailId")
    java.lang.String emailId, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "firstName")
    java.lang.String firstName, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "lastName")
    java.lang.String lastName, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "middleName")
    java.lang.String middleName, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "password")
    java.lang.String password, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "username")
    java.lang.String username, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "uid")
    java.lang.String uid, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "confirmNewFwpEmail")
    java.lang.String confirmNewFwpEmail, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "newFwpEmail")
    java.lang.String newFwpEmail, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "confirmNewPassword")
    java.lang.String confirmNewPassword, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "currentPassword")
    java.lang.String currentPassword, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "newPassword")
    java.lang.String newPassword, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "addrLine1")
    java.lang.String addrLine1, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "addrLine2")
    java.lang.String addrLine2, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "addrType")
    java.lang.String addrType, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "alsNo")
    java.lang.String alsNo, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "city")
    java.lang.String city, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "clubMember500")
    java.lang.String clubMember500, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "contactViaEmailId")
    java.lang.String contactViaEmailId, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "country")
    java.lang.String country, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "dob")
    java.lang.String dob, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "email")
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "eyeColorCd")
    java.lang.String eyeColorCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "firstNm")
    java.lang.String firstNm, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "hairColorCd")
    java.lang.String hairColorCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "heightFeet")
    java.lang.String heightFeet, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "heightInches")
    java.lang.String heightInches, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "homePhone")
    java.lang.String homePhone, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "lastNm")
    java.lang.String lastNm, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "midInit")
    java.lang.String midInit, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "poBoxInd")
    java.lang.String poBoxInd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residencyStatus")
    java.lang.String residencyStatus, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "sexCd")
    java.lang.String sexCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "state")
    java.lang.String state, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "suffix")
    java.lang.String suffix, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "weight")
    java.lang.String weight, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "workPhone")
    java.lang.String workPhone, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "zipCd")
    java.lang.String zipCd) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public User(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "emailId")
    java.lang.String emailId, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "firstName")
    java.lang.String firstName, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "lastName")
    java.lang.String lastName, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "middleName")
    java.lang.String middleName, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "password")
    java.lang.String password, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "username")
    java.lang.String username, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "uid")
    java.lang.String uid, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "confirmNewFwpEmail")
    java.lang.String confirmNewFwpEmail, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "newFwpEmail")
    java.lang.String newFwpEmail, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "confirmNewPassword")
    java.lang.String confirmNewPassword, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "currentPassword")
    java.lang.String currentPassword, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "newPassword")
    java.lang.String newPassword, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "addrLine1")
    java.lang.String addrLine1, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "addrLine2")
    java.lang.String addrLine2, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "addrType")
    java.lang.String addrType, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "alsNo")
    java.lang.String alsNo, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "city")
    java.lang.String city, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "clubMember500")
    java.lang.String clubMember500, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "contactViaEmailId")
    java.lang.String contactViaEmailId, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "country")
    java.lang.String country, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "dob")
    java.lang.String dob, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "email")
    java.lang.String email, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "eyeColorCd")
    java.lang.String eyeColorCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "firstNm")
    java.lang.String firstNm, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "hairColorCd")
    java.lang.String hairColorCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "heightFeet")
    java.lang.String heightFeet, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "heightInches")
    java.lang.String heightInches, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "homePhone")
    java.lang.String homePhone, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "lastNm")
    java.lang.String lastNm, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "midInit")
    java.lang.String midInit, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "poBoxInd")
    java.lang.String poBoxInd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residencyStatus")
    java.lang.String residencyStatus, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "sexCd")
    java.lang.String sexCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "state")
    java.lang.String state, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "suffix")
    java.lang.String suffix, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "weight")
    java.lang.String weight, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "workPhone")
    java.lang.String workPhone, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "zipCd")
    java.lang.String zipCd) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmailId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFirstName() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLastName() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMiddleName() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPassword() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUsername() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUid() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getConfirmNewFwpEmail() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getNewFwpEmail() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component10() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getConfirmNewPassword() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component11() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCurrentPassword() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component12() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getNewPassword() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component13() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAddrLine1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component14() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAddrLine2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component15() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAddrType() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component16() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAlsNo() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component17() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCity() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component18() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getClubMember500() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component19() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getContactViaEmailId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component20() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCountry() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component21() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDob() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component22() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmail() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component23() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEyeColorCd() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component24() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFirstNm() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component25() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getHairColorCd() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component26() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getHeightFeet() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component27() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getHeightInches() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component28() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getHomePhone() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component29() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLastNm() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component30() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMidInit() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component31() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPoBoxInd() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component32() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getResidencyStatus() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component33() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSexCd() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component34() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getState() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component35() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSuffix() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component36() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getWeight() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component37() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getWorkPhone() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component38() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getZipCd() {
        return null;
    }
}