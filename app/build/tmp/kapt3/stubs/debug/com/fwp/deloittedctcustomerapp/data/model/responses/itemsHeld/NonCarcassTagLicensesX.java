package com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B)\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0006J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J-\u0010\u0012\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0003H\u00d6\u0001R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\b\"\u0004\b\f\u0010\nR\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\b\"\u0004\b\u000e\u0010\n\u00a8\u0006\u0019"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/NonCarcassTagLicensesX;", "", "issuedDate", "", "licenseName", "oppCode", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getIssuedDate", "()Ljava/lang/String;", "setIssuedDate", "(Ljava/lang/String;)V", "getLicenseName", "setLicenseName", "getOppCode", "setOppCode", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class NonCarcassTagLicensesX {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String issuedDate;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String licenseName;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String oppCode;
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicensesX copy(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "issuedDate")
    java.lang.String issuedDate, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "licenseName")
    java.lang.String licenseName, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "oppCode")
    java.lang.String oppCode) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public NonCarcassTagLicensesX(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "issuedDate")
    java.lang.String issuedDate, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "licenseName")
    java.lang.String licenseName, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "oppCode")
    java.lang.String oppCode) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getIssuedDate() {
        return null;
    }
    
    public final void setIssuedDate(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLicenseName() {
        return null;
    }
    
    public final void setLicenseName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOppCode() {
        return null;
    }
    
    public final void setOppCode(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
}