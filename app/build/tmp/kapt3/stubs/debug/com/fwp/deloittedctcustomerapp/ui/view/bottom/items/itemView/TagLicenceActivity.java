package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J \u0010\u001d\u001a\u00020\u00152\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010 \u001a\u00020!H\u0002J\u0012\u0010\"\u001a\u00020\u00152\b\u0010#\u001a\u0004\u0018\u00010$H\u0014J\u0010\u0010%\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010&\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\b\u0010\'\u001a\u00020\u0015H\u0002J\u0010\u0010(\u001a\u00020\u00152\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0012\u0010)\u001a\u00020\u00152\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0002J\u0012\u0010*\u001a\u00020\u00152\b\u0010+\u001a\u0004\u0018\u00010\u0018H\u0003R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/itemView/TagLicenceActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "accountType", "", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/ActivityTagLicenceBinding;", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/ActivityTagLicenceBinding;", "setBinding", "(Lcom/fwp/deloittedctcustomerapp/databinding/ActivityTagLicenceBinding;)V", "licenseList", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/NonCarcassTagLicensesX;", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "name", "viewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "year", "buttonHandler", "", "currentYearMultiUIBind", "it", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/ItemsHeldResponse;", "itrateDataList", "licenseResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/NonCarcassTagLicenses;", "multiAccountUiBind", "mutliCurrentOwnerUiBind", "responseList", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/Account;", "item", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "previousYearMultiUiBind", "primaryUiBind", "recyclerContent", "sessionDateTimeFormatter", "settingUI", "setupView", "response", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class TagLicenceActivity extends androidx.appcompat.app.AppCompatActivity {
    public com.fwp.deloittedctcustomerapp.databinding.ActivityTagLicenceBinding binding;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel viewModel;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicensesX> licenseList;
    private java.lang.String year = "";
    private java.lang.String accountType = "P";
    private java.lang.String name = "";
    
    public TagLicenceActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.databinding.ActivityTagLicenceBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.databinding.ActivityTagLicenceBinding p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    private final void setupView(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse response) {
    }
    
    private final void primaryUiBind(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it) {
    }
    
    private final void multiAccountUiBind(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it) {
    }
    
    private final void previousYearMultiUiBind(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it) {
    }
    
    private final void currentYearMultiUIBind(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it) {
    }
    
    private final void mutliCurrentOwnerUiBind(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account responseList, com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it, int item) {
    }
    
    private final void settingUI(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicenses licenseResponse) {
    }
    
    private final void itrateDataList(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicenses licenseResponse) {
    }
    
    private final void sessionDateTimeFormatter(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicenses licenseResponse) {
    }
    
    private final void recyclerContent() {
    }
    
    private final void buttonHandler() {
    }
}