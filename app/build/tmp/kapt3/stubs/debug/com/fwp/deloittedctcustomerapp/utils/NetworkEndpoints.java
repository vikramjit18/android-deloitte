package com.fwp.deloittedctcustomerapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u001b\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/NetworkEndpoints;", "", "()V", "ALS_LINKING_ENDPOINT", "", "ALS_LOOKUP_ENDPOINT", "BUG_PRODUCTION_BASE_URL", "CHANGE_FWP_EMAIL_ENDPOINTS", "CHANGE_PASSWORD_ENDPOINTS", "CHECK_USER_STATUS_ENDPOINTS", "DEV_BASE_URL", "DOWNLOAD_TAGS_ENDPOINTS", "EXISTING_EMAIL_ENDPOINT", "EXISTING_USERNAME_ENDPOINT", "FORGET_PASSWORD_ENDPOINT", "GET_ETAGS_ENDPOINT", "GET_ITEMS_ENDPOINTS", "GET_MASTER_DATA", "LOGIN_ENDPOINT", "NEW_USER_SIGNUP", "REQUEST_EMAIL_COPY_ENDPOINTS_PRIMARY", "REQUEST_EMAIL_COPY_ENDPOINTS_SECONDARY", "RESET_PASSWORD_ENDPOINT", "SHOW_USER_AGGREMENT_ENDPOINTS", "TEST_BASE_URL", "UPDATE_USER_PROFILE_V1", "UPDATE_USER_PROFILE_V2", "VALIDATE_TAGS_ENDPOINTS", "VIEW_USER_PROFILE_ENDPOINT_V1", "VIEW_USER_PROFILE_ENDPOINT_V2", "ZIP_CODE_FROM_CITY", "app_debug"})
public final class NetworkEndpoints {
    @org.jetbrains.annotations.NotNull()
    public static final com.fwp.deloittedctcustomerapp.utils.NetworkEndpoints INSTANCE = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DEV_BASE_URL = "https://mbl.fwp.mt.gov:463/Interface/myfwp/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TEST_BASE_URL = "https://mbl.fwp.mt.gov:483/Interface/myfwp/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BUG_PRODUCTION_BASE_URL = "https://mbl.fwp.mt.gov:443/Interface/myfwp/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NEW_USER_SIGNUP = "users/user";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EXISTING_EMAIL_ENDPOINT = "users/emailids";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EXISTING_USERNAME_ENDPOINT = "users/usernames";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RESET_PASSWORD_ENDPOINT = "passwordreset";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String FORGET_PASSWORD_ENDPOINT = "forgotpassword";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LOGIN_ENDPOINT = "login";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ALS_LINKING_ENDPOINT = "alslinking";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ALS_LOOKUP_ENDPOINT = "alslookup";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UPDATE_USER_PROFILE_V1 = "alsdata/updateuser";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UPDATE_USER_PROFILE_V2 = "alsdata/v2/updateuser";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VIEW_USER_PROFILE_ENDPOINT_V1 = "alsdata/fetchuser";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VIEW_USER_PROFILE_ENDPOINT_V2 = "alsdata/v2/fetchuser";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ZIP_CODE_FROM_CITY = "zip";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_ITEMS_ENDPOINTS = "itemsheld";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_ETAGS_ENDPOINT = "getetags";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_MASTER_DATA = "masterdata";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DOWNLOAD_TAGS_ENDPOINTS = "users/tags/download";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VALIDATE_TAGS_ENDPOINTS = "users/tags/validate";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String REQUEST_EMAIL_COPY_ENDPOINTS_PRIMARY = "users/emailitems";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String REQUEST_EMAIL_COPY_ENDPOINTS_SECONDARY = "users/secondary/emailitems";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CHANGE_PASSWORD_ENDPOINTS = "passwordchange";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CHANGE_FWP_EMAIL_ENDPOINTS = "users/changefwpemail";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SHOW_USER_AGGREMENT_ENDPOINTS = "accepttou";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CHECK_USER_STATUS_ENDPOINTS = "users/usrstatus";
    
    private NetworkEndpoints() {
        super();
    }
}