package com.fwp.deloittedctcustomerapp.data.model.requests;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0087\b\u0018\u00002\u00020\u0001B/\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0010\b\u0001\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bJ\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\nJ\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u0011\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0007H\u00c6\u0003J8\u0010\u0013\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0010\b\u0003\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0007H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0014J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u0005H\u00d6\u0001R\u0015\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u000b\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0019\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u001a"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/requests/RequestCopySecondaryRequest;", "", "alsNo", "", "dob", "", "requestEmailIdList", "", "(Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;)V", "getAlsNo", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getDob", "()Ljava/lang/String;", "getRequestEmailIdList", "()Ljava/util/List;", "component1", "component2", "component3", "copy", "(Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;)Lcom/fwp/deloittedctcustomerapp/data/model/requests/RequestCopySecondaryRequest;", "equals", "", "other", "hashCode", "toString", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class RequestCopySecondaryRequest {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer alsNo = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String dob = null;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<java.lang.String> requestEmailIdList = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopySecondaryRequest copy(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "alsNo")
    java.lang.Integer alsNo, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "dob")
    java.lang.String dob, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "requestEmailIdList")
    java.util.List<java.lang.String> requestEmailIdList) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public RequestCopySecondaryRequest(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "alsNo")
    java.lang.Integer alsNo, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "dob")
    java.lang.String dob, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "requestEmailIdList")
    java.util.List<java.lang.String> requestEmailIdList) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getAlsNo() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDob() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getRequestEmailIdList() {
        return null;
    }
}