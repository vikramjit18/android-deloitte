package com.fwp.deloittedctcustomerapp.data.model.responses.etag;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B/\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0010\b\u0001\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\bJ\u000b\u0010\u000e\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0011\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J3\u0010\u0011\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0010\b\u0003\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00052\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0019\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\n\u00a8\u0006\u0018"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagAccounts;", "", "accountType", "", "etags", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/Etag;", "owner", "(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V", "getAccountType", "()Ljava/lang/String;", "getEtags", "()Ljava/util/List;", "getOwner", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class EtagAccounts {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String accountType = null;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.Etag> etags = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String owner = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagAccounts copy(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "accountType")
    java.lang.String accountType, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "etags")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.Etag> etags, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "owner")
    java.lang.String owner) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public EtagAccounts(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "accountType")
    java.lang.String accountType, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "etags")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.Etag> etags, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "owner")
    java.lang.String owner) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccountType() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.Etag> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.Etag> getEtags() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOwner() {
        return null;
    }
}