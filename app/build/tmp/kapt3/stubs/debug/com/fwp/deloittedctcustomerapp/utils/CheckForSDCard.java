package com.fwp.deloittedctcustomerapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\b\u0003\u0010\u0005\u00a8\u0006\u0006"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/CheckForSDCard;", "", "()V", "isSDCardPresent", "", "()Z", "app_debug"})
public final class CheckForSDCard {
    
    public CheckForSDCard() {
        super();
    }
    
    public final boolean isSDCardPresent() {
        return false;
    }
}