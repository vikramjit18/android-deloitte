package com.fwp.deloittedctcustomerapp.ui.view.resetPass;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\nH\u0002J\b\u0010\u0018\u001a\u00020\u0016H\u0002J\b\u0010\u0019\u001a\u00020\u0016H\u0002J\b\u0010\u001a\u001a\u00020\u0016H\u0002J\u0010\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J$\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010#2\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J\b\u0010&\u001a\u00020\u0016H\u0016J\b\u0010\'\u001a\u00020\u0016H\u0002J \u0010(\u001a\u00020\u00162\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020,H\u0002J\b\u0010.\u001a\u00020\u0016H\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/resetPass/ForgotReset;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/ForgotResetFragmentBinding;", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/ForgotResetFragmentBinding;", "setBinding", "(Lcom/fwp/deloittedctcustomerapp/databinding/ForgotResetFragmentBinding;)V", "email", "", "forgotViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/forgot/ForgotViewModel;", "getForgotViewModel", "()Lcom/fwp/deloittedctcustomerapp/ui/view/forgot/ForgotViewModel;", "forgotViewModel$delegate", "Lkotlin/Lazy;", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "mTextWatcher", "Landroid/text/TextWatcher;", "apiHitPassReset", "", "s2", "buttonActivation", "buttonHandler", "buttonProperty", "handleIntent", "intent", "Landroid/content/Intent;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "passValidator", "passwordValidator", "password", "Lcom/google/android/material/textfield/TextInputEditText;", "validateImg", "", "unValidateImg", "viewModelObser", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class ForgotReset extends androidx.fragment.app.Fragment {
    public com.fwp.deloittedctcustomerapp.databinding.ForgotResetFragmentBinding binding;
    private final java.lang.String email = null;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private final kotlin.Lazy forgotViewModel$delegate = null;
    private final android.text.TextWatcher mTextWatcher = null;
    
    public ForgotReset() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.databinding.ForgotResetFragmentBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.databinding.ForgotResetFragmentBinding p0) {
    }
    
    private final com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotViewModel getForgotViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void handleIntent(android.content.Intent intent) {
    }
    
    private final void buttonHandler() {
    }
    
    private final void passValidator() {
    }
    
    private final void passwordValidator(com.google.android.material.textfield.TextInputEditText password, int validateImg, int unValidateImg) {
    }
    
    private final void buttonActivation() {
    }
    
    private final void apiHitPassReset(java.lang.String s2) {
    }
    
    private final void viewModelObser() {
    }
    
    private final void buttonProperty() {
    }
}