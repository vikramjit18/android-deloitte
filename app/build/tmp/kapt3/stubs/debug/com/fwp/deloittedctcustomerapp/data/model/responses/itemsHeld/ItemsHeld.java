package com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B)\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bJ\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J-\u0010\u0018\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0007H\u00d6\u0001R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006\u001f"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/ItemsHeld;", "", "nonCarcassTagLicenses", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/NonCarcassTagLicenses;", "permits", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/Permits;", "year", "", "(Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/NonCarcassTagLicenses;Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/Permits;Ljava/lang/String;)V", "getNonCarcassTagLicenses", "()Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/NonCarcassTagLicenses;", "setNonCarcassTagLicenses", "(Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/NonCarcassTagLicenses;)V", "getPermits", "()Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/Permits;", "setPermits", "(Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/Permits;)V", "getYear", "()Ljava/lang/String;", "setYear", "(Ljava/lang/String;)V", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ItemsHeld {
    @org.jetbrains.annotations.Nullable()
    private com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicenses nonCarcassTagLicenses;
    @org.jetbrains.annotations.Nullable()
    private com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Permits permits;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String year;
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeld copy(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "nonCarcassTagLicenses")
    com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicenses nonCarcassTagLicenses, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "permits")
    com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Permits permits, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "year")
    java.lang.String year) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public ItemsHeld(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "nonCarcassTagLicenses")
    com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicenses nonCarcassTagLicenses, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "permits")
    com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Permits permits, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "year")
    java.lang.String year) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicenses component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicenses getNonCarcassTagLicenses() {
        return null;
    }
    
    public final void setNonCarcassTagLicenses(@org.jetbrains.annotations.Nullable()
    com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicenses p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Permits component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Permits getPermits() {
        return null;
    }
    
    public final void setPermits(@org.jetbrains.annotations.Nullable()
    com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Permits p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getYear() {
        return null;
    }
    
    public final void setYear(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
}