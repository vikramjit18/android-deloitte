package com.fwp.deloittedctcustomerapp.ui.view.signup;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 J\u000e\u0010!\u001a\u00020\u001e2\u0006\u0010\"\u001a\u00020#J\u000e\u0010$\u001a\u00020\u001e2\u0006\u0010%\u001a\u00020&R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00070\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00070\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u0014\u0010\u0012R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00070\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u0016\u0010\u0012R\u001d\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u0018\u0010\u0012R\u001d\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u0012R\u001d\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u001c\u0010\u0012\u00a8\u0006\'"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/signup/SignUpViewModel;", "Landroidx/lifecycle/ViewModel;", "mainRepo", "Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;", "(Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;)V", "_existingEmailResponse", "Landroidx/lifecycle/MutableLiveData;", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/signup/CheckCredsResponse;", "_existingUserResponse", "_newUserSignupResponse", "_toastExistingEmail", "Lcom/fwp/deloittedctcustomerapp/utils/Event;", "", "_toastExistingUser", "_toastNewUser", "existingEmailResponse", "Landroidx/lifecycle/LiveData;", "getExistingEmailResponse", "()Landroidx/lifecycle/LiveData;", "existingUserResponse", "getExistingUserResponse", "newUserSignupResponse", "getNewUserSignupResponse", "toastExistingEmail", "getToastExistingEmail", "toastExistingUser", "getToastExistingUser", "toastNewUser", "getToastNewUser", "checkExistingEmail", "", "checkEmailRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/CheckEmailRequest;", "checkExistingUsername", "checkUsernameRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/CheckUsernameRequest;", "sendNewUserData", "signupRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/SignupRequest;", "app_debug"})
public final class SignUpViewModel extends androidx.lifecycle.ViewModel {
    private final com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse> _newUserSignupResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse> _existingUserResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse> _existingEmailResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastExistingEmail = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastNewUser = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastExistingUser = null;
    
    @javax.inject.Inject()
    public SignUpViewModel(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse> getNewUserSignupResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse> getExistingUserResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse> getExistingEmailResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastExistingEmail() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastNewUser() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastExistingUser() {
        return null;
    }
    
    public final void checkExistingUsername(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.CheckUsernameRequest checkUsernameRequest) {
    }
    
    public final void checkExistingEmail(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.CheckEmailRequest checkEmailRequest) {
    }
    
    public final void sendNewUserData(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.data.model.requests.SignupRequest signupRequest) {
    }
}