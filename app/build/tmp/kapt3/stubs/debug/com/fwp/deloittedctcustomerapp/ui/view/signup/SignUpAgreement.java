package com.fwp.deloittedctcustomerapp.ui.view.signup;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\u0014H\u0002J$\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J\u001a\u0010\u001e\u001a\u00020\u00142\u0006\u0010\u001f\u001a\u00020\u00172\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J\b\u0010 \u001a\u00020\u0014H\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/signup/SignUpAgreement;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/FragmentSignUpAgreementBinding;", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/FragmentSignUpAgreementBinding;", "setBinding", "(Lcom/fwp/deloittedctcustomerapp/databinding/FragmentSignUpAgreementBinding;)V", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "model", "Lcom/fwp/deloittedctcustomerapp/ui/view/signup/SignUpViewModel;", "getModel", "()Lcom/fwp/deloittedctcustomerapp/ui/view/signup/SignUpViewModel;", "model$delegate", "Lkotlin/Lazy;", "viewModelCommunicator", "Lcom/fwp/deloittedctcustomerapp/ui/viewmodel/Communicator;", "buttonHandler", "", "initButtonProperties", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "viewModelObser", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class SignUpAgreement extends androidx.fragment.app.Fragment {
    public com.fwp.deloittedctcustomerapp.databinding.FragmentSignUpAgreementBinding binding;
    private final kotlin.Lazy model$delegate = null;
    private com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator viewModelCommunicator;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    
    public SignUpAgreement() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.databinding.FragmentSignUpAgreementBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.databinding.FragmentSignUpAgreementBinding p0) {
    }
    
    private final com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpViewModel getModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void buttonHandler() {
    }
    
    private final void viewModelObser() {
    }
    
    private final void initButtonProperties() {
    }
}