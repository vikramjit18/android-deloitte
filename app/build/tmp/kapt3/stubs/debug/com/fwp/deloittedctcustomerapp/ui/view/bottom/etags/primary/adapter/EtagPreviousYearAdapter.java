package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u001b\u001cB!\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u0004\u00a2\u0006\u0002\u0010\bJ\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0002H\u0002J\u0010\u0010\u000e\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0002H\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0011\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0010H\u0016J\u0018\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0010H\u0016J\u000e\u0010\u0017\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\nJ \u0010\u0019\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u0007H\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/adapter/EtagPreviousYearAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/adapter/EtagPreviousYearAdapter$MyPreviousYearViewHolder;", "list", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/Etag;", "oflineSpiidList", "", "(Ljava/util/List;Ljava/util/List;)V", "mListener", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/adapter/EtagPreviousYearAdapter$OnPreviousClickListener;", "disableValiBtn", "", "holder", "enableValiBtn", "getItemCount", "", "onBindViewHolder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setOnPreviousDownloadClick", "listener", "viewDetails", "owner", "MyPreviousYearViewHolder", "OnPreviousClickListener", "app_debug"})
public final class EtagPreviousYearAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.MyPreviousYearViewHolder> {
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.Etag> list = null;
    private final java.util.List<java.lang.String> oflineSpiidList = null;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.OnPreviousClickListener mListener;
    
    public EtagPreviousYearAdapter(@org.jetbrains.annotations.NotNull()
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.Etag> list, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> oflineSpiidList) {
        super();
    }
    
    public final void setOnPreviousDownloadClick(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.OnPreviousClickListener listener) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.MyPreviousYearViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.MyPreviousYearViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    private final void viewDetails(com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.MyPreviousYearViewHolder holder, int position, java.lang.String owner) {
    }
    
    private final void enableValiBtn(com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.MyPreviousYearViewHolder holder) {
    }
    
    private final void disableValiBtn(com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.MyPreviousYearViewHolder holder) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\b"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/adapter/EtagPreviousYearAdapter$OnPreviousClickListener;", "", "onPreviousClick", "", "position", "", "holder", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/adapter/EtagPreviousYearAdapter$MyPreviousYearViewHolder;", "app_debug"})
    public static abstract interface OnPreviousClickListener {
        
        public abstract void onPreviousClick(int position, @org.jetbrains.annotations.NotNull()
        com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.MyPreviousYearViewHolder holder);
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u000f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0011R\u0011\u0010\u0014\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0011R\u0011\u0010\u0016\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0011R\u0011\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u001c\u001a\u00020\u001d\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010 \u001a\u00020!\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u0011\u0010$\u001a\u00020%\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\'R\u0011\u0010(\u001a\u00020)\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010+R\u0011\u0010,\u001a\u00020-\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010/R\u0011\u00100\u001a\u00020%\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u0010\'\u00a8\u00062"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/adapter/EtagPreviousYearAdapter$MyPreviousYearViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "viewHolder", "Landroid/view/View;", "listener", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/adapter/EtagPreviousYearAdapter$OnPreviousClickListener;", "(Landroid/view/View;Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/adapter/EtagPreviousYearAdapter$OnPreviousClickListener;)V", "downloadBtnContainer", "Landroid/widget/LinearLayout;", "getDownloadBtnContainer", "()Landroid/widget/LinearLayout;", "etagContainer", "Landroid/widget/FrameLayout;", "getEtagContainer", "()Landroid/widget/FrameLayout;", "etagExpire", "getEtagExpire", "()Landroid/view/View;", "etagRefund", "getEtagRefund", "etagValidate", "getEtagValidate", "etagVoid", "getEtagVoid", "iconDownloadBtn", "Lcom/google/android/material/button/MaterialButton;", "getIconDownloadBtn", "()Lcom/google/android/material/button/MaterialButton;", "iconProgressButton", "Landroidx/cardview/widget/CardView;", "getIconProgressButton", "()Landroidx/cardview/widget/CardView;", "itemContainer", "Landroidx/constraintlayout/widget/ConstraintLayout;", "getItemContainer", "()Landroidx/constraintlayout/widget/ConstraintLayout;", "region", "Landroid/widget/TextView;", "getRegion", "()Landroid/widget/TextView;", "tagImage", "Landroid/widget/ImageView;", "getTagImage", "()Landroid/widget/ImageView;", "validateBtn", "Landroid/widget/Button;", "getValidateBtn", "()Landroid/widget/Button;", "yearAnimalType", "getYearAnimalType", "app_debug"})
    public static final class MyPreviousYearViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final androidx.constraintlayout.widget.ConstraintLayout itemContainer = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.ImageView tagImage = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView yearAnimalType = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView region = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.FrameLayout etagContainer = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.LinearLayout downloadBtnContainer = null;
        @org.jetbrains.annotations.NotNull()
        private final android.view.View etagValidate = null;
        @org.jetbrains.annotations.NotNull()
        private final android.view.View etagVoid = null;
        @org.jetbrains.annotations.NotNull()
        private final android.view.View etagRefund = null;
        @org.jetbrains.annotations.NotNull()
        private final android.view.View etagExpire = null;
        @org.jetbrains.annotations.NotNull()
        private final com.google.android.material.button.MaterialButton iconDownloadBtn = null;
        @org.jetbrains.annotations.NotNull()
        private final androidx.cardview.widget.CardView iconProgressButton = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.Button validateBtn = null;
        
        public MyPreviousYearViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View viewHolder, @org.jetbrains.annotations.NotNull()
        com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.OnPreviousClickListener listener) {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.constraintlayout.widget.ConstraintLayout getItemContainer() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getTagImage() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getYearAnimalType() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getRegion() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.FrameLayout getEtagContainer() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.LinearLayout getDownloadBtnContainer() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.view.View getEtagValidate() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.view.View getEtagVoid() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.view.View getEtagRefund() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.view.View getEtagExpire() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.android.material.button.MaterialButton getIconDownloadBtn() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.cardview.widget.CardView getIconProgressButton() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.Button getValidateBtn() {
            return null;
        }
    }
}