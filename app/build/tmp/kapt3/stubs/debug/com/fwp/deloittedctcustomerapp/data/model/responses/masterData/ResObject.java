package com.fwp.deloittedctcustomerapp.data.model.responses.masterData;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0087\b\u0018\u00002\u00020\u0001Bu\u0012\u0014\b\u0001\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0014\b\u0001\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0014\b\u0001\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u000e\b\u0001\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u000b\u0012\b\b\u0001\u0010\f\u001a\u00020\r\u0012\b\b\u0001\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0015\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0007H\u00c6\u0003J\u0015\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\u00c6\u0003J\u0015\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0003H\u00c6\u0003J\u000f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00050\u000bH\u00c6\u0003J\t\u0010\"\u001a\u00020\rH\u00c6\u0003J\t\u0010#\u001a\u00020\u000fH\u00c6\u0003Jy\u0010$\u001a\u00020\u00002\u0014\b\u0003\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00032\b\b\u0003\u0010\u0006\u001a\u00020\u00072\u0014\b\u0003\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00032\u0014\b\u0003\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u00032\u000e\b\u0003\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u000b2\b\b\u0003\u0010\f\u001a\u00020\r2\b\b\u0003\u0010\u000e\u001a\u00020\u000fH\u00c6\u0001J\u0013\u0010%\u001a\u00020&2\b\u0010\'\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010(\u001a\u00020\u0004H\u00d6\u0001J\t\u0010)\u001a\u00020\u0005H\u00d6\u0001R\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001d\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0012R\u0011\u0010\f\u001a\u00020\r\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u001d\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0012R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001c\u00a8\u0006*"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/ResObject;", "", "eYE", "", "", "", "geoInfo", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/GeoInfo;", "hAIR", "sexCd", "suffix", "", "height", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/Height;", "weight", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/Weight;", "(Ljava/util/Map;Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/GeoInfo;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/Height;Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/Weight;)V", "getEYE", "()Ljava/util/Map;", "getGeoInfo", "()Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/GeoInfo;", "getHAIR", "getHeight", "()Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/Height;", "getSexCd", "getSuffix", "()Ljava/util/List;", "getWeight", "()Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/Weight;", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "", "other", "hashCode", "toString", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ResObject {
    @org.jetbrains.annotations.NotNull()
    private final java.util.Map<java.lang.Integer, java.lang.String> eYE = null;
    @org.jetbrains.annotations.NotNull()
    private final com.fwp.deloittedctcustomerapp.data.model.responses.masterData.GeoInfo geoInfo = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.Map<java.lang.Integer, java.lang.String> hAIR = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.Map<java.lang.String, java.lang.String> sexCd = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.String> suffix = null;
    @org.jetbrains.annotations.NotNull()
    private final com.fwp.deloittedctcustomerapp.data.model.responses.masterData.Height height = null;
    @org.jetbrains.annotations.NotNull()
    private final com.fwp.deloittedctcustomerapp.data.model.responses.masterData.Weight weight = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.masterData.ResObject copy(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "EYE")
    java.util.Map<java.lang.Integer, java.lang.String> eYE, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "GeoInfo")
    com.fwp.deloittedctcustomerapp.data.model.responses.masterData.GeoInfo geoInfo, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "HAIR")
    java.util.Map<java.lang.Integer, java.lang.String> hAIR, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "sexCd")
    java.util.Map<java.lang.String, java.lang.String> sexCd, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "suffix")
    java.util.List<java.lang.String> suffix, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "heightData")
    com.fwp.deloittedctcustomerapp.data.model.responses.masterData.Height height, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "weightData")
    com.fwp.deloittedctcustomerapp.data.model.responses.masterData.Weight weight) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public ResObject(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "EYE")
    java.util.Map<java.lang.Integer, java.lang.String> eYE, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "GeoInfo")
    com.fwp.deloittedctcustomerapp.data.model.responses.masterData.GeoInfo geoInfo, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "HAIR")
    java.util.Map<java.lang.Integer, java.lang.String> hAIR, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "sexCd")
    java.util.Map<java.lang.String, java.lang.String> sexCd, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "suffix")
    java.util.List<java.lang.String> suffix, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "heightData")
    com.fwp.deloittedctcustomerapp.data.model.responses.masterData.Height height, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "weightData")
    com.fwp.deloittedctcustomerapp.data.model.responses.masterData.Weight weight) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.Integer, java.lang.String> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.Integer, java.lang.String> getEYE() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.masterData.GeoInfo component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.masterData.GeoInfo getGeoInfo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.Integer, java.lang.String> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.Integer, java.lang.String> getHAIR() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.String, java.lang.String> component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.String, java.lang.String> getSexCd() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> getSuffix() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.masterData.Height component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.masterData.Height getHeight() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.masterData.Weight component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.masterData.Weight getWeight() {
        return null;
    }
}