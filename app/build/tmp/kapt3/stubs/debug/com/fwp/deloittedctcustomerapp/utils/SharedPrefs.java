package com.fwp.deloittedctcustomerapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\rJ\u0016\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u000fJ\u0018\u0010\u000e\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0004J\u000e\u0010\u0012\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0004J\u0016\u0010\u0013\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u000fJ\u0016\u0010\u0013\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/SharedPrefs;", "", "()V", "ID_USER", "", "PREFS_NAME", "TOKEN", "prefs", "Landroid/content/SharedPreferences;", "clearSharedPref", "", "init", "context", "Landroid/content/Context;", "read", "", "key", "value", "removeData", "write", "app_debug"})
public final class SharedPrefs {
    @org.jetbrains.annotations.NotNull()
    public static final com.fwp.deloittedctcustomerapp.utils.SharedPrefs INSTANCE = null;
    private static android.content.SharedPreferences prefs;
    private static final java.lang.String PREFS_NAME = "params";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ID_USER = "id_user";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TOKEN = "token";
    
    private SharedPrefs() {
        super();
    }
    
    public final void init(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String read(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.String value) {
        return null;
    }
    
    public final long read(@org.jetbrains.annotations.NotNull()
    java.lang.String key, long value) {
        return 0L;
    }
    
    public final void write(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    public final void write(@org.jetbrains.annotations.NotNull()
    java.lang.String key, long value) {
    }
    
    public final void clearSharedPref() {
    }
    
    public final void removeData(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
    }
}