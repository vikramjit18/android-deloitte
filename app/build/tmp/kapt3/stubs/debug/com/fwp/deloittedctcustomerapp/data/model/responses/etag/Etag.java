package com.fwp.deloittedctcustomerapp.data.model.responses.etag;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B#\u0012\u0010\b\u0001\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J\u0011\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\r\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\u0010\b\u0003\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0006H\u00d6\u0001R\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u0015"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/Etag;", "", "etagLicensesList", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagLicenses;", "licenseYear", "", "(Ljava/util/List;Ljava/lang/String;)V", "getEtagLicensesList", "()Ljava/util/List;", "getLicenseYear", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class Etag {
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses> etagLicensesList = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String licenseYear = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.etag.Etag copy(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "etagLicensesList")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses> etagLicensesList, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "licenseYear")
    java.lang.String licenseYear) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public Etag(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "etagLicensesList")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses> etagLicensesList, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "licenseYear")
    java.lang.String licenseYear) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses> getEtagLicensesList() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLicenseYear() {
        return null;
    }
}