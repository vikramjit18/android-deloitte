package com.fwp.deloittedctcustomerapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u0000 \r2\u00020\u0001:\u0001\rB\u0005\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u001e\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u001e\u0010\f\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000e"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/TagImageInflater;", "", "()V", "showDetailedTagIcon", "", "context", "Landroid/content/Context;", "imageView", "Landroid/widget/ImageView;", "heroName", "", "showDetailedTagImage", "showListTagIcon", "Companion", "app_debug"})
public final class TagImageInflater {
    @org.jetbrains.annotations.NotNull()
    public static final com.fwp.deloittedctcustomerapp.utils.TagImageInflater.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Antelope = "icon_tagdetail_ic_Antelope@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Antelope_Antlerless = "icon_tagdetail_ic_Antelope_Antlerless@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_BighornSheep = "icon_tagdetail_ic_BighornSheep@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_BigHornSheep_AdultEwe = "icon_tagdetail_ic_BigHornSheep_AdultEwe@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Bison = "icon_tagdetail_ic_Bison@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Blackbear = "icon_tagdetail_ic_Blackbear@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Deer = "icon_tagdetail_ic_Deer@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Deer_Antlerless = "icon_tagdetail_ic_Deer_Antlerless@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Elk = "icon_tagdetail_ic_Elk@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Elk_Antlerless = "icon_tagdetail_ic_Elk_Antlerless@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_ElkSpikeBull = "icon_tagdetail_ic_ElkSpikeBull@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_MigratoryBirds = "icon_tagdetail_ic_MigratoryBirds@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Moose = "icon_tagdetail_ic_Moose@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_MooseAntlerless = "icon_tagdetail_ic_MooseAntlerless@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_MountainGoat = "icon_tagdetail_ic_MountainGoat@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_MountainLion = "icon_tagdetail_ic_MountainLion@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Paddlefish = "icon_tagdetail_ic_Paddlefish@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Sandhillcrane = "icon_tagdetail_ic_Sandhillcrane@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Swan = "icon_tagdetail_ic_Swan@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Turkey = "icon_tagdetail_ic_Turkey@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Turkey_Beardless = "icon_tagdetail_ic_Turkey_Beardless@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_UplandBirds = "icon_tagdetail_ic_UplandBirds@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tagdetail_ic_Wolf = "icon_tagdetail_ic_Wolf@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_Antelope = "icon_tags_ic_species_Antelope@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_AntelopeAntlerless = "icon_tags_ic_species_AntelopeAntlerless@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_BighornSheep = "icon_tags_ic_species_BighornSheep@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_Bison = "icon_tags_ic_species_Bison@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_Blackbear = "icon_tags_ic_species_Blackbear@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_Deer = "icon_tags_ic_species_Deer@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_Deer_Antlerless = "icon_tags_ic_species_Deer_Antlerless@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_Elk = "icon_tags_ic_species_Elk@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_Elk_Antlerless = "iicon_tags_ic_species_Elk_Antlerless@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_MigratoryBirds = "icon_tags_ic_species_MigratoryBirds@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_Moose = "icon_tags_ic_species_Moose@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_MountainGoat = "icon_tags_ic_species_MountainGoat@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_MountainLion = "icon_tags_ic_species_MountainLion@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_Paddlefish = "icon_tags_ic_species_Paddlefish@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_Sandhillcrane = "icon_tags_ic_species_Sandhillcrane@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_Swan = "icon_tags_ic_species_Swan@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_turkey = "icon_tags_ic_species_turkey@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_UplandBirds = "icon_tags_ic_species_UplandBirds@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_species_Wolf = "icon_tags_ic_species_Wolf@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_Antelope = "icon_tags_ic_vali_species_Antelope@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_AntelopeAntlerless = "icon_tags_ic_vali_species_AntelopeAntlerless@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_BighornSheep = "icon_tags_ic_vali_species_BighornSheep@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_Bison = "icon_tags_ic_vali_species_Bison@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_Blackbear = "icon_tags_ic_vali_species_Blackbear@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_Deer = "icon_tags_ic_vali_species_Deer@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_Deer_Antlerless = "icon_tags_ic_vali_species_Deer_Antlerless@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_Elk = "icon_tags_ic_vali_species_Elk@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_Elk_Antlerless = "icon_tags_ic_vali_species_Elk_Antlerless@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_MigratoryBirds = "icon_tags_ic_vali_species_MigratoryBirds@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_Moose = "icon_tags_ic_vali_species_Moose@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_MountainGoat = "icon_tags_ic_vali_species_MountainGoat@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_MountainLion = "icon_tags_ic_vali_species_MountainLion@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_Paddlefish = "icon_tags_ic_vali_species_Paddlefish@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_Sandhillcrane = "icon_tags_ic_vali_species_Sandhillcrane@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_Swan = "icon_tags_ic_vali_species_Swan.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_turkey = "icon_tags_ic_vali_species_turkey@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_UplandBirds = "icon_tags_ic_vali_species_UplandBirds@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String icon_tags_ic_vali_species_Wolf = "icon_tags_ic_vali_species_Wolf@3x.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_Antelope = "IMG_tagdetail_IMG_Antelope.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_Antelope_Antlerless = "IMG_tagdetail_IMG_Antelope_Antlerless.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_BighornSheep = "IMG_tagdetail_IMG_BighornSheep.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_Bison = "IMG_tagdetail_IMG_Bison.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_Blackbear = "IMG_tagdetail_IMG_Blackbear.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_Elk = "IMG_tagdetail_IMG_Elk.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_Elk_Antlerless = "IMG_tagdetail_IMG_Elk_Antlerless.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_MigratoryBirds = "IMG_tagdetail_IMG_MigratoryBirds.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_Moose = "IMG_tagdetail_IMG_Moose.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_MountainGoat = "IMG_tagdetail_IMG_MountainGoat.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_MountainLion = "IMG_tagdetail_IMG_MountainLion.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_MuleDeer = "IMG_tagdetail_IMG_MuleDeer.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_MuleDeer_Antlerless = "IMG_tagdetail_IMG_MuleDeer_Antlerless.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_Paddlefish = "IMG_tagdetail_IMG_Paddlefish.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_Sandhillcrane = "IMG_tagdetail_IMG_Sandhillcrane.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_Swan = "IMG_tagdetail_IMG_Swan.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_Turkey = "IMG_tagdetail_IMG_Turkey.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_UplandBirds = "IMG_tagdetail_IMG_UplandBirds.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_WhitetailDeer = "IMG_tagdetail_IMG_WhitetailDeer.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_WhitetailDeer_Antlerless = "IMG_tagdetail_IMG_WhitetailDeer_Antlerless.png";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMG_tagdetail_IMG_Wolf = "IMG_tagdetail_IMG_Wolf.png";
    
    public TagImageInflater() {
        super();
    }
    
    public final void showDetailedTagIcon(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView imageView, @org.jetbrains.annotations.NotNull()
    java.lang.String heroName) {
    }
    
    public final void showListTagIcon(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView imageView, @org.jetbrains.annotations.NotNull()
    java.lang.String heroName) {
    }
    
    public final void showDetailedTagImage(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView imageView, @org.jetbrains.annotations.NotNull()
    java.lang.String heroName) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\bR\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00101\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00102\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00103\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00104\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00105\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00106\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00108\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010:\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010;\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010<\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010=\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010>\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010?\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010@\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010A\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010B\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010C\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010D\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010E\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010F\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010G\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010H\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010I\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010J\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010K\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010L\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010M\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010N\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010O\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010P\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010Q\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010R\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010S\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010T\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010U\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006V"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/TagImageInflater$Companion;", "", "()V", "IMG_tagdetail_IMG_Antelope", "", "IMG_tagdetail_IMG_Antelope_Antlerless", "IMG_tagdetail_IMG_BighornSheep", "IMG_tagdetail_IMG_Bison", "IMG_tagdetail_IMG_Blackbear", "IMG_tagdetail_IMG_Elk", "IMG_tagdetail_IMG_Elk_Antlerless", "IMG_tagdetail_IMG_MigratoryBirds", "IMG_tagdetail_IMG_Moose", "IMG_tagdetail_IMG_MountainGoat", "IMG_tagdetail_IMG_MountainLion", "IMG_tagdetail_IMG_MuleDeer", "IMG_tagdetail_IMG_MuleDeer_Antlerless", "IMG_tagdetail_IMG_Paddlefish", "IMG_tagdetail_IMG_Sandhillcrane", "IMG_tagdetail_IMG_Swan", "IMG_tagdetail_IMG_Turkey", "IMG_tagdetail_IMG_UplandBirds", "IMG_tagdetail_IMG_WhitetailDeer", "IMG_tagdetail_IMG_WhitetailDeer_Antlerless", "IMG_tagdetail_IMG_Wolf", "icon_tagdetail_ic_Antelope", "icon_tagdetail_ic_Antelope_Antlerless", "icon_tagdetail_ic_BigHornSheep_AdultEwe", "icon_tagdetail_ic_BighornSheep", "icon_tagdetail_ic_Bison", "icon_tagdetail_ic_Blackbear", "icon_tagdetail_ic_Deer", "icon_tagdetail_ic_Deer_Antlerless", "icon_tagdetail_ic_Elk", "icon_tagdetail_ic_ElkSpikeBull", "icon_tagdetail_ic_Elk_Antlerless", "icon_tagdetail_ic_MigratoryBirds", "icon_tagdetail_ic_Moose", "icon_tagdetail_ic_MooseAntlerless", "icon_tagdetail_ic_MountainGoat", "icon_tagdetail_ic_MountainLion", "icon_tagdetail_ic_Paddlefish", "icon_tagdetail_ic_Sandhillcrane", "icon_tagdetail_ic_Swan", "icon_tagdetail_ic_Turkey", "icon_tagdetail_ic_Turkey_Beardless", "icon_tagdetail_ic_UplandBirds", "icon_tagdetail_ic_Wolf", "icon_tags_ic_species_Antelope", "icon_tags_ic_species_AntelopeAntlerless", "icon_tags_ic_species_BighornSheep", "icon_tags_ic_species_Bison", "icon_tags_ic_species_Blackbear", "icon_tags_ic_species_Deer", "icon_tags_ic_species_Deer_Antlerless", "icon_tags_ic_species_Elk", "icon_tags_ic_species_Elk_Antlerless", "icon_tags_ic_species_MigratoryBirds", "icon_tags_ic_species_Moose", "icon_tags_ic_species_MountainGoat", "icon_tags_ic_species_MountainLion", "icon_tags_ic_species_Paddlefish", "icon_tags_ic_species_Sandhillcrane", "icon_tags_ic_species_Swan", "icon_tags_ic_species_UplandBirds", "icon_tags_ic_species_Wolf", "icon_tags_ic_species_turkey", "icon_tags_ic_vali_species_Antelope", "icon_tags_ic_vali_species_AntelopeAntlerless", "icon_tags_ic_vali_species_BighornSheep", "icon_tags_ic_vali_species_Bison", "icon_tags_ic_vali_species_Blackbear", "icon_tags_ic_vali_species_Deer", "icon_tags_ic_vali_species_Deer_Antlerless", "icon_tags_ic_vali_species_Elk", "icon_tags_ic_vali_species_Elk_Antlerless", "icon_tags_ic_vali_species_MigratoryBirds", "icon_tags_ic_vali_species_Moose", "icon_tags_ic_vali_species_MountainGoat", "icon_tags_ic_vali_species_MountainLion", "icon_tags_ic_vali_species_Paddlefish", "icon_tags_ic_vali_species_Sandhillcrane", "icon_tags_ic_vali_species_Swan", "icon_tags_ic_vali_species_UplandBirds", "icon_tags_ic_vali_species_Wolf", "icon_tags_ic_vali_species_turkey", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}