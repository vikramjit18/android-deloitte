package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\u0014H\u0002J\u0012\u0010\u0016\u001a\u00020\u00142\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\u001d\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0012\u0010\u001e\u001a\u00020\u00142\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0014J \u0010!\u001a\u00020\u00142\u0006\u0010\"\u001a\u00020#2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010$\u001a\u00020\fH\u0002J \u0010%\u001a\u00020\u00142\u0006\u0010\"\u001a\u00020#2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010$\u001a\u00020\fH\u0002J\u0010\u0010&\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\b\u0010\'\u001a\u00020\u0014H\u0002J\u0012\u0010(\u001a\u00020\u00142\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0002J\u0012\u0010)\u001a\u00020\u00142\b\u0010*\u001a\u0004\u0018\u00010\u001bH\u0003R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/itemView/TagPermitActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "accountType", "", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/ActivityTagPermitBinding;", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/ActivityTagPermitBinding;", "setBinding", "(Lcom/fwp/deloittedctcustomerapp/databinding/ActivityTagPermitBinding;)V", "itemPosition", "", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "name", "viewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "year", "apiHit", "", "buttonHandler", "dateTimeIssueInFormatter", "permitResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/PermitData;", "multiAccountUIBind", "it", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/ItemsHeldResponse;", "multiCurrentYearUIBind", "multiPreviousYearUIBind", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "ownerCurrentYearMutilSetup", "responseList", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/Account;", "item", "ownerMultiPreviousSetup", "primaryAccountUIBind", "regulationLinkSetup", "setPermitGuidline", "setupView", "response", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class TagPermitActivity extends androidx.appcompat.app.AppCompatActivity {
    public com.fwp.deloittedctcustomerapp.databinding.ActivityTagPermitBinding binding;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel viewModel;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private int itemPosition = 0;
    private java.lang.String year = "";
    private java.lang.String name = "";
    private java.lang.String accountType = "P";
    
    public TagPermitActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.databinding.ActivityTagPermitBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.databinding.ActivityTagPermitBinding p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void apiHit() {
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    private final void setupView(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse response) {
    }
    
    private final void primaryAccountUIBind(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it) {
    }
    
    private final void multiAccountUIBind(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it) {
    }
    
    private final void multiPreviousYearUIBind(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it) {
    }
    
    private final void ownerMultiPreviousSetup(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account responseList, com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it, int item) {
    }
    
    private final void multiCurrentYearUIBind(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it) {
    }
    
    private final void ownerCurrentYearMutilSetup(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account responseList, com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it, int item) {
    }
    
    private final void regulationLinkSetup() {
    }
    
    private final void dateTimeIssueInFormatter(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.PermitData permitResponse) {
    }
    
    private final void setPermitGuidline(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.PermitData permitResponse) {
    }
    
    private final void buttonHandler() {
    }
}