package com.fwp.deloittedctcustomerapp.data.model.requests;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\\\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0087\b\u0018\u00002\u00020\u0001B\u00e1\u0002\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0001\u0010\r\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0001\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0011\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0013\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0014\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0015\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0016\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0017\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0018\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0019\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u001a\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u001b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u001c\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u001d\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u001e\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u001f\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0001\u0010 \u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010!J\u000b\u0010B\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0010\u0010C\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003\u00a2\u0006\u0002\u0010%J\u000b\u0010D\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010E\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010F\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010G\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010H\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010I\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010J\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010K\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010L\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0010\u0010M\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003\u00a2\u0006\u0002\u0010%J\u000b\u0010N\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010O\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010P\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010Q\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010R\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010S\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010T\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010U\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0010\u0010V\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003\u00a2\u0006\u0002\u0010%J\u000b\u0010W\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010X\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010Y\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010Z\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010[\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\\\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010]\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0010\u0010^\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003\u00a2\u0006\u0002\u0010%J\u00ea\u0002\u0010_\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\t\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\f\u001a\u0004\u0018\u00010\u00052\n\b\u0003\u0010\r\u001a\u0004\u0018\u00010\u00052\n\b\u0003\u0010\u000e\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0010\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0011\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0012\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0013\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0014\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0015\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0016\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0017\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0018\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0019\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u001a\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u001b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u001c\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u001d\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u001e\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u001f\u001a\u0004\u0018\u00010\u00052\n\b\u0003\u0010 \u001a\u0004\u0018\u00010\u0003H\u00c6\u0001\u00a2\u0006\u0002\u0010`J\u0013\u0010a\u001a\u00020b2\b\u0010c\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010d\u001a\u00020\u0005H\u00d6\u0001J\t\u0010e\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\n\n\u0002\u0010&\u001a\u0004\b$\u0010%R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010#R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010#R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010#R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010#R\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b+\u0010#R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b,\u0010#R\u0015\u0010\f\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\n\n\u0002\u0010&\u001a\u0004\b-\u0010%R\u0015\u0010\r\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\n\n\u0002\u0010&\u001a\u0004\b.\u0010%R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b/\u0010#R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b0\u0010#R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u0010#R\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b2\u0010#R\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b3\u0010#R\u0013\u0010\u0013\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b4\u0010#R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b5\u0010#R\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b6\u0010#R\u0013\u0010\u0016\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b7\u0010#R\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b8\u0010#R\u0013\u0010\u0018\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b9\u0010#R\u0013\u0010\u0019\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b:\u0010#R\u0013\u0010\u001a\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b;\u0010#R\u0013\u0010\u001b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b<\u0010#R\u0013\u0010\u001c\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b=\u0010#R\u0013\u0010\u001d\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b>\u0010#R\u0013\u0010\u001e\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b?\u0010#R\u0015\u0010\u001f\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\n\n\u0002\u0010&\u001a\u0004\b@\u0010%R\u0013\u0010 \u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\bA\u0010#\u00a8\u0006f"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/requests/UpdateUserProfileRequest;", "", "alsEmail", "", "alsNo", "", "clubMember500", "dob", "eyeColorCd", "firstNm", "fwpEmailId", "hairColorCd", "heightFeet", "heightInches", "homePhone", "lastNm", "mailingAddrLine", "mailingCity", "mailingCountry", "mailingState", "mailingZipCd", "midInit", "residencyStatus", "residentialAddrLine", "residentialCity", "residentialCountry", "residentialState", "residentialZipCd", "sexCd", "suffix", "userName", "weight", "workPhone", "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V", "getAlsEmail", "()Ljava/lang/String;", "getAlsNo", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getClubMember500", "getDob", "getEyeColorCd", "getFirstNm", "getFwpEmailId", "getHairColorCd", "getHeightFeet", "getHeightInches", "getHomePhone", "getLastNm", "getMailingAddrLine", "getMailingCity", "getMailingCountry", "getMailingState", "getMailingZipCd", "getMidInit", "getResidencyStatus", "getResidentialAddrLine", "getResidentialCity", "getResidentialCountry", "getResidentialState", "getResidentialZipCd", "getSexCd", "getSuffix", "getUserName", "getWeight", "getWorkPhone", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component24", "component25", "component26", "component27", "component28", "component29", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/fwp/deloittedctcustomerapp/data/model/requests/UpdateUserProfileRequest;", "equals", "", "other", "hashCode", "toString", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class UpdateUserProfileRequest {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String alsEmail = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer alsNo = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String clubMember500 = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String dob = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String eyeColorCd = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String firstNm = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String fwpEmailId = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String hairColorCd = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer heightFeet = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer heightInches = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String homePhone = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String lastNm = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String mailingAddrLine = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String mailingCity = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String mailingCountry = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String mailingState = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String mailingZipCd = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String midInit = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String residencyStatus = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String residentialAddrLine = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String residentialCity = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String residentialCountry = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String residentialState = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String residentialZipCd = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String sexCd = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String suffix = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String userName = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer weight = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String workPhone = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.requests.UpdateUserProfileRequest copy(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "alsEmail")
    java.lang.String alsEmail, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "alsNo")
    java.lang.Integer alsNo, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "clubMember500")
    java.lang.String clubMember500, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "dob")
    java.lang.String dob, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "eyeColorCd")
    java.lang.String eyeColorCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "firstNm")
    java.lang.String firstNm, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "fwpEmailId")
    java.lang.String fwpEmailId, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "hairColorCd")
    java.lang.String hairColorCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "heightFeet")
    java.lang.Integer heightFeet, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "heightInches")
    java.lang.Integer heightInches, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "homePhone")
    java.lang.String homePhone, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "lastNm")
    java.lang.String lastNm, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "mailingAddrLine")
    java.lang.String mailingAddrLine, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "mailingCity")
    java.lang.String mailingCity, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "mailingCountry")
    java.lang.String mailingCountry, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "mailingState")
    java.lang.String mailingState, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "mailingZipCd")
    java.lang.String mailingZipCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "midInit")
    java.lang.String midInit, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residencyStatus")
    java.lang.String residencyStatus, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residentialAddrLine")
    java.lang.String residentialAddrLine, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residentialCity")
    java.lang.String residentialCity, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residentialCountry")
    java.lang.String residentialCountry, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residentialState")
    java.lang.String residentialState, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residentialZipCd")
    java.lang.String residentialZipCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "sexCd")
    java.lang.String sexCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "suffix")
    java.lang.String suffix, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "userName")
    java.lang.String userName, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "weight")
    java.lang.Integer weight, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "workPhone")
    java.lang.String workPhone) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public UpdateUserProfileRequest(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "alsEmail")
    java.lang.String alsEmail, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "alsNo")
    java.lang.Integer alsNo, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "clubMember500")
    java.lang.String clubMember500, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "dob")
    java.lang.String dob, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "eyeColorCd")
    java.lang.String eyeColorCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "firstNm")
    java.lang.String firstNm, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "fwpEmailId")
    java.lang.String fwpEmailId, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "hairColorCd")
    java.lang.String hairColorCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "heightFeet")
    java.lang.Integer heightFeet, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "heightInches")
    java.lang.Integer heightInches, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "homePhone")
    java.lang.String homePhone, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "lastNm")
    java.lang.String lastNm, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "mailingAddrLine")
    java.lang.String mailingAddrLine, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "mailingCity")
    java.lang.String mailingCity, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "mailingCountry")
    java.lang.String mailingCountry, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "mailingState")
    java.lang.String mailingState, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "mailingZipCd")
    java.lang.String mailingZipCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "midInit")
    java.lang.String midInit, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residencyStatus")
    java.lang.String residencyStatus, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residentialAddrLine")
    java.lang.String residentialAddrLine, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residentialCity")
    java.lang.String residentialCity, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residentialCountry")
    java.lang.String residentialCountry, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residentialState")
    java.lang.String residentialState, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "residentialZipCd")
    java.lang.String residentialZipCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "sexCd")
    java.lang.String sexCd, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "suffix")
    java.lang.String suffix, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "userName")
    java.lang.String userName, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "weight")
    java.lang.Integer weight, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "workPhone")
    java.lang.String workPhone) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAlsEmail() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getAlsNo() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getClubMember500() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDob() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEyeColorCd() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFirstNm() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFwpEmailId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getHairColorCd() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getHeightFeet() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component10() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getHeightInches() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component11() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getHomePhone() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component12() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLastNm() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component13() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMailingAddrLine() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component14() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMailingCity() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component15() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMailingCountry() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component16() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMailingState() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component17() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMailingZipCd() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component18() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMidInit() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component19() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getResidencyStatus() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component20() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getResidentialAddrLine() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component21() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getResidentialCity() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component22() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getResidentialCountry() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component23() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getResidentialState() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component24() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getResidentialZipCd() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component25() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSexCd() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component26() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSuffix() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component27() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUserName() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component28() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getWeight() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component29() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getWorkPhone() {
        return null;
    }
}