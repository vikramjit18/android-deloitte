package com.fwp.deloittedctcustomerapp.data.model.responses.als;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B)\u0012\u000e\b\u0001\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\bJ\u000f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0006H\u00c6\u0003J-\u0010\u0011\u001a\u00020\u00002\u000e\b\u0003\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00062\b\b\u0003\u0010\u0007\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0006H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\f\u00a8\u0006\u0018"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/als/AlsResponse;", "", "resObject", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/als/ResObject;", "responseCode", "", "responseMessage", "(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V", "getResObject", "()Ljava/util/List;", "getResponseCode", "()Ljava/lang/String;", "getResponseMessage", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class AlsResponse {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.als.ResObject> resObject = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String responseCode = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String responseMessage = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse copy(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "resObject")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.als.ResObject> resObject, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "responseCode")
    java.lang.String responseCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "responseMessage")
    java.lang.String responseMessage) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public AlsResponse(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "resObject")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.als.ResObject> resObject, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "responseCode")
    java.lang.String responseCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "responseMessage")
    java.lang.String responseMessage) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.als.ResObject> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.als.ResObject> getResObject() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getResponseCode() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getResponseMessage() {
        return null;
    }
}