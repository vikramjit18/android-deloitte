package com.fwp.deloittedctcustomerapp.ui.view.login;

import java.lang.System;

/**
 * FWP
 * Hilt android entry
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0011\u001a\u00020\u0012H\u0002J\b\u0010\u0013\u001a\u00020\u0012H\u0002J\b\u0010\u0014\u001a\u00020\u0012H\u0002J$\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u001a\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u001e\u001a\u00020\u00162\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\b\u0010\u001f\u001a\u00020\u0012H\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000e\u00a8\u0006 "}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/login/LoginAgreement;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/FragmentLoginAgreementBinding;", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/FragmentLoginAgreementBinding;", "setBinding", "(Lcom/fwp/deloittedctcustomerapp/databinding/FragmentLoginAgreementBinding;)V", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "loginViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/login/LoginViewModel;", "getLoginViewModel", "()Lcom/fwp/deloittedctcustomerapp/ui/view/login/LoginViewModel;", "loginViewModel$delegate", "Lkotlin/Lazy;", "apiHitTou", "", "buttonHandler", "init", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "viewModelObser", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class LoginAgreement extends androidx.fragment.app.Fragment {
    
    /**
     * Global variables
     */
    public com.fwp.deloittedctcustomerapp.databinding.FragmentLoginAgreementBinding binding;
    private final kotlin.Lazy loginViewModel$delegate = null;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    
    public LoginAgreement() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.fwp.deloittedctcustomerapp.databinding.FragmentLoginAgreementBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.fwp.deloittedctcustomerapp.databinding.FragmentLoginAgreementBinding p0) {
    }
    
    private final com.fwp.deloittedctcustomerapp.ui.view.login.LoginViewModel getLoginViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    /**
     * Button handler function
     */
    private final void buttonHandler() {
    }
    
    private final void apiHitTou() {
    }
    
    private final void viewModelObser() {
    }
    
    /**
     * Initiate
     * init Function
     */
    private final void init() {
    }
}