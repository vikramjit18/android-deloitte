package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\f\u001a\u00020\rH\u0002J\u0010\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0001H\u0002J\u0012\u0010\u0010\u001a\u00020\r2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016J\"\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0016H\u0016J$\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016J\b\u0010 \u001a\u00020\rH\u0016J\u0010\u0010!\u001a\u00020\r2\u0006\u0010\"\u001a\u00020\u0018H\u0002J\u0018\u0010#\u001a\u00020\r2\u0006\u0010\"\u001a\u00020\u00182\u0006\u0010$\u001a\u00020\u0018H\u0002J\u0010\u0010%\u001a\u00020\r2\u0006\u0010&\u001a\u00020\'H\u0002J\b\u0010(\u001a\u00020\rH\u0002J\b\u0010)\u001a\u00020\rH\u0002J\b\u0010*\u001a\u00020\rH\u0002J\b\u0010+\u001a\u00020\rH\u0002J\b\u0010,\u001a\u00020\rH\u0002J\b\u0010-\u001a\u00020\rH\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006."}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/EtagsFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/FragmentEtagsBinding;", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/FragmentEtagsBinding;", "landingScreenActivityViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "buttonHandler", "", "navigateToFrag", "fragment", "onActivityCreated", "savedInstanceState", "Landroid/os/Bundle;", "onCreateAnimation", "Landroid/view/animation/Animation;", "transit", "", "enter", "", "nextAnim", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onResume", "replaceBgImgGreetText", "isAlsLinked", "replaceEtagsFragment", "alsActivated", "showAccountHeader", "state", "", "showAlsLinkedHeader", "showEtagAccounts", "showMultiAccountHeader", "startOfflineObservation", "startOnlineObservation", "viewModelObsr", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class EtagsFragment extends androidx.fragment.app.Fragment {
    private com.fwp.deloittedctcustomerapp.databinding.FragmentEtagsBinding _binding;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel landingScreenActivityViewModel;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    
    public EtagsFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.FragmentEtagsBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.animation.Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void buttonHandler() {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void onActivityCreated(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void startOnlineObservation() {
    }
    
    private final void startOfflineObservation() {
    }
    
    private final void viewModelObsr() {
    }
    
    private final void replaceEtagsFragment(boolean isAlsLinked, boolean alsActivated) {
    }
    
    private final void showEtagAccounts() {
    }
    
    private final void replaceBgImgGreetText(boolean isAlsLinked) {
    }
    
    private final void showAccountHeader(java.lang.String state) {
    }
    
    private final void showAlsLinkedHeader() {
    }
    
    private final void showMultiAccountHeader() {
    }
    
    private final void navigateToFrag(androidx.fragment.app.Fragment fragment) {
    }
}