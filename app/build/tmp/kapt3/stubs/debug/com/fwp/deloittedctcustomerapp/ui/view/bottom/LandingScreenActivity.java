package com.fwp.deloittedctcustomerapp.ui.view.bottom;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010 \n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0012\u001a\u00020\u0013H\u0003J\u0010\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0013H\u0002J\b\u0010\u0018\u001a\u00020\u0013H\u0003J\b\u0010\u0019\u001a\u00020\u0013H\u0002J\u0010\u0010\u001a\u001a\u00020\u00132\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\b\u0010\u001d\u001a\u00020\u0013H\u0002J\b\u0010\u001e\u001a\u00020\u0013H\u0002J\b\u0010\u001f\u001a\u00020\u0013H\u0002J\u0012\u0010 \u001a\u00020\u00132\b\u0010!\u001a\u0004\u0018\u00010\"H\u0014J\b\u0010#\u001a\u00020\u0013H\u0014J\b\u0010$\u001a\u00020\u0013H\u0014J\b\u0010%\u001a\u00020\u0013H\u0002J\u0010\u0010&\u001a\u00020\u00132\u0006\u0010\'\u001a\u00020\rH\u0002J\u0010\u0010(\u001a\u00020\u00132\u0006\u0010\'\u001a\u00020\rH\u0002J\u0010\u0010)\u001a\u00020\u00132\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0016\u0010*\u001a\u00020\u00132\f\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00160,H\u0002J\b\u0010-\u001a\u00020\u0013H\u0002J\b\u0010.\u001a\u00020\u0013H\u0002J\b\u0010/\u001a\u00020\u0013H\u0002J\b\u00100\u001a\u00020\u0013H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00061"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/LandingScreenActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/ActivityLandingScreenBinding;", "gson", "Lcom/google/gson/Gson;", "isoflineValidated", "", "landingScreenActivityViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "listOffLoad", "", "", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "validateTagActivityViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/activityViewModel/ValidateTagActivityViewModel;", "apiHits", "", "autoHitValidateApi", "request", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ValidateEtagRequest;", "checkLiveNetworkConnection", "checkUserapiHit", "checkingResDeletingDB", "expiredEtagResponseObsr", "etag", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagResponse;", "initReLoginProcess", "liveConnectionObsr", "offloadObsr", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onStart", "reLoadingEtag", "showSessionLogOutDialog", "daysDiff", "showSessionNotifyDialog", "successEtagResponseObsr", "successOfflineEtagsObsr", "it", "", "successOffloadObsr", "successValidateEtagsObsr", "userProfileObserver", "validateOfflineEtag", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class LandingScreenActivity extends androidx.appcompat.app.AppCompatActivity {
    private boolean isoflineValidated = false;
    private com.fwp.deloittedctcustomerapp.databinding.ActivityLandingScreenBinding binding;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel validateTagActivityViewModel;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel landingScreenActivityViewModel;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private final java.util.List<java.lang.String> listOffLoad = null;
    private final com.google.gson.Gson gson = null;
    
    public LandingScreenActivity() {
        super();
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void checkLiveNetworkConnection() {
    }
    
    private final void liveConnectionObsr() {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    private final void userProfileObserver() {
    }
    
    private final void expiredEtagResponseObsr(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse etag) {
    }
    
    private final void successEtagResponseObsr(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse etag) {
    }
    
    @android.annotation.SuppressLint(value = {"HardwareIds"})
    private final void checkUserapiHit() {
    }
    
    @android.annotation.SuppressLint(value = {"HardwareIds"})
    private final void apiHits() {
    }
    
    private final void showSessionNotifyDialog(java.lang.String daysDiff) {
    }
    
    private final void initReLoginProcess() {
    }
    
    private final void showSessionLogOutDialog(java.lang.String daysDiff) {
    }
    
    private final void validateOfflineEtag() {
    }
    
    private final void successValidateEtagsObsr() {
    }
    
    private final void successOfflineEtagsObsr(java.util.List<com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest> it) {
    }
    
    private final void autoHitValidateApi(com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest request) {
    }
    
    private final void reLoadingEtag() {
    }
    
    private final void checkingResDeletingDB() {
    }
    
    private final void offloadObsr() {
    }
    
    private final void successOffloadObsr() {
    }
}