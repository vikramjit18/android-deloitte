package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.validated;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 12\u00020\u0001:\u00011B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0012\u001a\u00020\u00132\u000e\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0015H\u0002J\b\u0010\u0017\u001a\u00020\u0013H\u0002J\u0016\u0010\u0018\u001a\u00020\u00192\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015H\u0002J\u0010\u0010\u001a\u001a\u00020\u00132\u0006\u0010\u001b\u001a\u00020\u0019H\u0002J\u0010\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u0019H\u0002J$\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010#2\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J\u001a\u0010&\u001a\u00020\u00132\u0006\u0010\'\u001a\u00020\u001f2\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J\u0010\u0010(\u001a\u00020\u00132\u0006\u0010)\u001a\u00020*H\u0002J\b\u0010+\u001a\u00020\u0013H\u0002J\u0010\u0010,\u001a\u00020\u00132\u0006\u0010)\u001a\u00020*H\u0002J\u0018\u0010-\u001a\u00020\u00132\u0006\u0010.\u001a\u00020/2\u0006\u0010\'\u001a\u00020\u001fH\u0002J\b\u00100\u001a\u00020\u0013H\u0003R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0010\u0010\n\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00062"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/validated/ValidatedEtagFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/ValidatedEtagFragmentBinding;", "accountOwner", "", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/ValidatedEtagFragmentBinding;", "confirmationNumber", "eTagYearIndex", "gson", "Lcom/google/gson/Gson;", "harvestTime", "position", "viewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "apiResponseMandatory", "", "eTagMandatoryDetails", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/ETagMandatoryDetail;", "buttonHandler", "extractingPhoneNo", "", "initImage", "imagename", "initTagicon", "iconName", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "ownerAccount", "it", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagResponse;", "settingHarvestDate", "setupUI", "showPopupMenu", "context", "Landroid/content/Context;", "viewModelSetup", "Companion", "app_release"})
public final class ValidatedEtagFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.validated.ValidatedEtagFragment.Companion Companion = null;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel viewModel;
    private com.fwp.deloittedctcustomerapp.databinding.ValidatedEtagFragmentBinding _binding;
    private java.lang.Object eTagYearIndex;
    private java.lang.Object accountOwner;
    private java.lang.Object position;
    private java.lang.Object harvestTime;
    private java.lang.Object confirmationNumber;
    private final com.google.gson.Gson gson = null;
    
    public ValidatedEtagFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.ValidatedEtagFragmentBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void buttonHandler() {
    }
    
    private final void showPopupMenu(android.content.Context context, android.view.View view) {
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    private final void viewModelSetup() {
    }
    
    private final void setupUI(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse it) {
    }
    
    private final void ownerAccount(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse it) {
    }
    
    private final void settingHarvestDate() {
    }
    
    private final void initImage(java.lang.String imagename) {
    }
    
    private final void initTagicon(java.lang.String iconName) {
    }
    
    private final void apiResponseMandatory(java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.ETagMandatoryDetail> eTagMandatoryDetails) {
    }
    
    private final java.lang.String extractingPhoneNo(java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.ETagMandatoryDetail> eTagMandatoryDetails) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/validated/ValidatedEtagFragment$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/validated/ValidatedEtagFragment;", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.validated.ValidatedEtagFragment newInstance() {
            return null;
        }
    }
}