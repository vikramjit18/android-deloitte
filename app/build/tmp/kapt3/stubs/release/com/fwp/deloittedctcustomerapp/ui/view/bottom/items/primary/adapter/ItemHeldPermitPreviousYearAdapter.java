package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u0012B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0016J\u001c\u0010\t\u001a\u00020\n2\n\u0010\u000b\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\f\u001a\u00020\bH\u0002J\u001c\u0010\r\u001a\u00020\n2\n\u0010\u000b\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\f\u001a\u00020\bH\u0016J\u001c\u0010\u000e\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\bH\u0016R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/primary/adapter/ItemHeldPermitPreviousYearAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/primary/adapter/ItemHeldPermitPreviousYearAdapter$MyViewHolder;", "previousList", "", "Lcom/fwp/deloittedctcustomerapp/data/model/ItemDetail;", "(Ljava/util/List;)V", "getItemCount", "", "navigateHandler", "", "holder", "position", "onBindViewHolder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "MyViewHolder", "app_release"})
public final class ItemHeldPermitPreviousYearAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.adapter.ItemHeldPermitPreviousYearAdapter.MyViewHolder> {
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.ItemDetail> previousList = null;
    
    public ItemHeldPermitPreviousYearAdapter(@org.jetbrains.annotations.NotNull
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.ItemDetail> previousList) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.adapter.ItemHeldPermitPreviousYearAdapter.MyViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.adapter.ItemHeldPermitPreviousYearAdapter.MyViewHolder holder, int position) {
    }
    
    private final void navigateHandler(com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.adapter.ItemHeldPermitPreviousYearAdapter.MyViewHolder holder, int position) {
    }
    
    @java.lang.Override
    public int getItemCount() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/primary/adapter/ItemHeldPermitPreviousYearAdapter$MyViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "viewDataBinding", "Lcom/fwp/deloittedctcustomerapp/databinding/ItemHeldChildRowBinding;", "(Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/primary/adapter/ItemHeldPermitPreviousYearAdapter;Lcom/fwp/deloittedctcustomerapp/databinding/ItemHeldChildRowBinding;)V", "getViewDataBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/ItemHeldChildRowBinding;", "app_release"})
    public final class MyViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull
        private final com.fwp.deloittedctcustomerapp.databinding.ItemHeldChildRowBinding viewDataBinding = null;
        
        public MyViewHolder(@org.jetbrains.annotations.NotNull
        com.fwp.deloittedctcustomerapp.databinding.ItemHeldChildRowBinding viewDataBinding) {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.databinding.ItemHeldChildRowBinding getViewDataBinding() {
            return null;
        }
    }
}