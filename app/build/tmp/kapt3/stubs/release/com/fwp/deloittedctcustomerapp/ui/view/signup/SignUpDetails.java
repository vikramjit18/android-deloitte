package com.fwp.deloittedctcustomerapp.ui.view.signup;

import java.lang.System;

/**
 * FWP
 * Hilt entry point
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0016H\u0002J\b\u0010\u0018\u001a\u00020\u0016H\u0002J\b\u0010\u0019\u001a\u00020\u0016H\u0002J\b\u0010\u001a\u001a\u00020\u0016H\u0002J\b\u0010\u001b\u001a\u00020\u0016H\u0002J\b\u0010\u001c\u001a\u00020\u0016H\u0002J\b\u0010\u001d\u001a\u00020\u0016H\u0002J$\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010#2\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J\b\u0010&\u001a\u00020\u0016H\u0016J\u001a\u0010\'\u001a\u00020\u00162\u0006\u0010(\u001a\u00020\u001f2\b\u0010$\u001a\u0004\u0018\u00010%H\u0016J0\u0010)\u001a\u00020\u00162\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020-2\u0006\u0010/\u001a\u00020-2\u0006\u00100\u001a\u00020-H\u0002J\b\u00101\u001a\u00020\u0016H\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\r\u001a\u00020\u000e8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00062"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/signup/SignUpDetails;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/SignUpDetailsFragmentBinding;", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/SignUpDetailsFragmentBinding;", "setBinding", "(Lcom/fwp/deloittedctcustomerapp/databinding/SignUpDetailsFragmentBinding;)V", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "mTextWatcher", "Landroid/text/TextWatcher;", "model", "Lcom/fwp/deloittedctcustomerapp/ui/view/signup/SignUpViewModel;", "getModel", "()Lcom/fwp/deloittedctcustomerapp/ui/view/signup/SignUpViewModel;", "model$delegate", "Lkotlin/Lazy;", "viewModelCommunicator", "Lcom/fwp/deloittedctcustomerapp/ui/viewmodel/Communicator;", "apiHitSignUpEmailCheck", "", "buttonActivation", "buttonDisable", "buttonEnable", "buttonHandler", "emailMatchValidator", "emailValidator", "init", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onViewCreated", "view", "showActivityNavigationAlert", "mContext", "Landroid/content/Context;", "tvPrimary", "", "tvSecondary", "leftBtn", "rightBtn", "viewModelObser", "app_release"})
@dagger.hilt.android.AndroidEntryPoint
public final class SignUpDetails extends androidx.fragment.app.Fragment {
    
    /**
     * Global variables
     */
    public com.fwp.deloittedctcustomerapp.databinding.SignUpDetailsFragmentBinding binding;
    private final kotlin.Lazy model$delegate = null;
    private com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator viewModelCommunicator;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    
    /**
     * create a
     * textWatcher member
     */
    private final android.text.TextWatcher mTextWatcher = null;
    
    public SignUpDetails() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final com.fwp.deloittedctcustomerapp.databinding.SignUpDetailsFragmentBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.databinding.SignUpDetailsFragmentBinding p0) {
    }
    
    private final com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpViewModel getModel() {
        return null;
    }
    
    @java.lang.Override
    public void onResume() {
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void init() {
    }
    
    /**
     * Button handler function
     */
    private final void buttonHandler() {
    }
    
    /**
     * Initiate
     * button activation function
     */
    private final void buttonActivation() {
    }
    
    private final void apiHitSignUpEmailCheck() {
    }
    
    private final void viewModelObser() {
    }
    
    private final void showActivityNavigationAlert(android.content.Context mContext, java.lang.String tvPrimary, java.lang.String tvSecondary, java.lang.String leftBtn, java.lang.String rightBtn) {
    }
    
    /**
     * Button
     * disable function
     */
    private final void buttonDisable() {
    }
    
    /**
     * Button
     * enable function
     */
    private final void buttonEnable() {
    }
    
    private final void emailMatchValidator() {
    }
    
    private final void emailValidator() {
    }
}