package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\nJ\u0016\u0010,\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\nJ\u0006\u0010-\u001a\u00020(J\u0016\u0010.\u001a\u00020(2\f\u0010/\u001a\b\u0012\u0004\u0012\u00020\u000700H\u0002J\u000e\u00101\u001a\u00020(2\u0006\u00102\u001a\u00020\nJ\u000e\u00103\u001a\u00020(2\u0006\u00102\u001a\u00020\nJ\u0016\u00104\u001a\u00020(2\f\u0010/\u001a\b\u0012\u0004\u0012\u00020\u000700H\u0002J\u0016\u00105\u001a\u00020(2\u0006\u00106\u001a\u00020\n2\u0006\u00107\u001a\u000208R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\n0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0017\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\n0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0016R\u0017\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00070\u001a8F\u00a2\u0006\u0006\u001a\u0004\b\u001b\u0010\u001cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u001a8F\u00a2\u0006\u0006\u001a\u0004\b\u001e\u0010\u001cR\u001d\u0010\u001f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u001a8F\u00a2\u0006\u0006\u001a\u0004\b \u0010\u001cR\u0017\u0010!\u001a\b\u0012\u0004\u0012\u00020\r0\u001a8F\u00a2\u0006\u0006\u001a\u0004\b\"\u0010\u001cR\u0017\u0010#\u001a\b\u0012\u0004\u0012\u00020\r0\u001a8F\u00a2\u0006\u0006\u001a\u0004\b$\u0010\u001cR\u0017\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00070\u001a8F\u00a2\u0006\u0006\u001a\u0004\b&\u0010\u001c\u00a8\u00069"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/viewdetails/primary/PrimaryAccountViewModel;", "Landroidx/lifecycle/ViewModel;", "mainRepo", "Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;", "(Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;)V", "_editProfileResponse", "Landroidx/lifecycle/MutableLiveData;", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/profile/UserProfileResponse;", "_toast", "Lcom/fwp/deloittedctcustomerapp/utils/Event;", "", "_toastEditResponse", "_userMailZipCodeResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/zipCode/ZipcodeResponse;", "_userResZipCodeResponse", "_viewUserProfileResponse", "coroutineExceptionHandler", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "getCoroutineExceptionHandler", "()Lkotlinx/coroutines/CoroutineExceptionHandler;", "countryMailCode", "getCountryMailCode", "()Landroidx/lifecycle/MutableLiveData;", "countryResCode", "getCountryResCode", "editProfileResponse", "Landroidx/lifecycle/LiveData;", "getEditProfileResponse", "()Landroidx/lifecycle/LiveData;", "toast", "getToast", "toastEditResponse", "getToastEditResponse", "userMailZipCodeResponse", "getUserMailZipCodeResponse", "userResZipCodeResponse", "getUserResZipCodeResponse", "viewUserProfileResponse", "getViewUserProfileResponse", "getMailCityFromZipcode", "", "zipcode", "", "isResident", "getResCityFromZipcode", "getUserProfile", "getUserSuccessRes", "response", "Lretrofit2/Response;", "setMailCountryCode", "countryCodeInput", "setResCountryCode", "updateRes", "updateUserProfile", "token", "updateUserProfileRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/UpdateUserProfileRequest;", "app_release"})
public final class PrimaryAccountViewModel extends androidx.lifecycle.ViewModel {
    private final com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo = null;
    @org.jetbrains.annotations.NotNull
    private final androidx.lifecycle.MutableLiveData<java.lang.String> countryResCode = null;
    @org.jetbrains.annotations.NotNull
    private final androidx.lifecycle.MutableLiveData<java.lang.String> countryMailCode = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse> _userResZipCodeResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse> _userMailZipCodeResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse> _editProfileResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toast = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastEditResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse> _viewUserProfileResponse = null;
    @org.jetbrains.annotations.NotNull
    private final kotlinx.coroutines.CoroutineExceptionHandler coroutineExceptionHandler = null;
    
    @javax.inject.Inject
    public PrimaryAccountViewModel(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getCountryResCode() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getCountryMailCode() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse> getUserResZipCodeResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse> getUserMailZipCodeResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse> getEditProfileResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToast() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastEditResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse> getViewUserProfileResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final kotlinx.coroutines.CoroutineExceptionHandler getCoroutineExceptionHandler() {
        return null;
    }
    
    public final void getUserProfile() {
    }
    
    private final void getUserSuccessRes(retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse> response) {
    }
    
    public final void setResCountryCode(@org.jetbrains.annotations.NotNull
    java.lang.String countryCodeInput) {
    }
    
    public final void setMailCountryCode(@org.jetbrains.annotations.NotNull
    java.lang.String countryCodeInput) {
    }
    
    public final void getResCityFromZipcode(int zipcode, @org.jetbrains.annotations.NotNull
    java.lang.String isResident) {
    }
    
    public final void getMailCityFromZipcode(int zipcode, @org.jetbrains.annotations.NotNull
    java.lang.String isResident) {
    }
    
    public final void updateUserProfile(@org.jetbrains.annotations.NotNull
    java.lang.String token, @org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.UpdateUserProfileRequest updateUserProfileRequest) {
    }
    
    private final void updateRes(retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse> response) {
    }
}