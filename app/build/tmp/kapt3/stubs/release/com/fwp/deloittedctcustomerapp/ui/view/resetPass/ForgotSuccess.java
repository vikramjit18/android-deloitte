package com.fwp.deloittedctcustomerapp.ui.view.resetPass;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u00020\nH\u0002J\b\u0010\u000b\u001a\u00020\nH\u0002J$\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u0014"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/resetPass/ForgotSuccess;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/ForgotSuccessFragmentBinding;", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/ForgotSuccessFragmentBinding;", "setBinding", "(Lcom/fwp/deloittedctcustomerapp/databinding/ForgotSuccessFragmentBinding;)V", "buttonHandler", "", "buttonProperty", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "app_release"})
public final class ForgotSuccess extends androidx.fragment.app.Fragment {
    public com.fwp.deloittedctcustomerapp.databinding.ForgotSuccessFragmentBinding binding;
    
    public ForgotSuccess() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final com.fwp.deloittedctcustomerapp.databinding.ForgotSuccessFragmentBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.databinding.ForgotSuccessFragmentBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void buttonHandler() {
    }
    
    private final void buttonProperty() {
    }
}