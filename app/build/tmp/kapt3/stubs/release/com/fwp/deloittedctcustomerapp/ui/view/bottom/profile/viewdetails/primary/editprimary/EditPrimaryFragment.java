package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.editprimary;

import java.lang.System;

/**
 * FWP
 * Hilt entry point
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0007\u0018\u0000 \u0095\u00012\u00020\u0001:\u0002\u0095\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020@H\u0002J\b\u0010A\u001a\u00020>H\u0002J\b\u0010B\u001a\u00020>H\u0002J \u0010C\u001a\u00020>2\u0016\u0010D\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u000ej\b\u0012\u0004\u0012\u00020\u0006`EH\u0002J\b\u0010F\u001a\u00020>H\u0002J \u0010G\u001a\u00020>2\u0016\u0010H\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u000ej\b\u0012\u0004\u0012\u00020\u0006`EH\u0002J \u0010I\u001a\u00020>2\u0016\u0010D\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u000ej\b\u0012\u0004\u0012\u00020\u0006`EH\u0002J\u0010\u0010J\u001a\u00020\u00062\u0006\u0010K\u001a\u00020\u0006H\u0002J\u0010\u0010L\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u0006H\u0002J\u0018\u0010M\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u00062\u0006\u0010O\u001a\u00020\u0006H\u0002J\u0010\u0010P\u001a\u00020\u00062\u0006\u0010Q\u001a\u00020\u0006H\u0002J \u0010R\u001a\u00020>2\u0016\u0010S\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u000ej\b\u0012\u0004\u0012\u00020\u0006`EH\u0002J \u0010T\u001a\u00020>2\u0016\u0010U\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u000ej\b\u0012\u0004\u0012\u00020\u0006`EH\u0002J\b\u0010V\u001a\u00020WH\u0002J\b\u0010X\u001a\u00020WH\u0002J\b\u0010Y\u001a\u00020>H\u0002J\"\u0010Z\u001a\u0004\u0018\u00010[2\u0006\u0010\\\u001a\u00020\b2\u0006\u0010]\u001a\u00020W2\u0006\u0010^\u001a\u00020\bH\u0016J$\u0010_\u001a\u00020`2\u0006\u0010a\u001a\u00020b2\b\u0010c\u001a\u0004\u0018\u00010d2\b\u0010e\u001a\u0004\u0018\u00010fH\u0016J\b\u0010g\u001a\u00020>H\u0016J\u001a\u0010h\u001a\u00020>2\u0006\u0010i\u001a\u00020`2\b\u0010e\u001a\u0004\u0018\u00010fH\u0016J\b\u0010j\u001a\u00020>H\u0002J\u0018\u0010k\u001a\u00020\b2\u0006\u0010l\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u0006H\u0002J\u0018\u0010m\u001a\u00020\b2\u0006\u0010l\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u0006H\u0002J\u0018\u0010n\u001a\u00020\b2\u0006\u0010l\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u0006H\u0002J\u0018\u0010o\u001a\u00020\b2\u0006\u0010l\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u0006H\u0002J\u0018\u0010p\u001a\u00020\b2\u0006\u0010q\u001a\u00020\u00062\u0006\u0010O\u001a\u00020\u0006H\u0002J\u0018\u0010r\u001a\u00020\b2\u0006\u0010l\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u0006H\u0002J\u0010\u0010s\u001a\u00020>2\u0006\u0010t\u001a\u00020uH\u0002J\u0018\u0010v\u001a\u00020\b2\u0006\u0010l\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u0006H\u0002J\u0010\u0010w\u001a\u00020>2\u0006\u0010t\u001a\u00020uH\u0002J\u0018\u0010x\u001a\u00020\b2\u0006\u0010l\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u0006H\u0002J\u0018\u0010y\u001a\u00020\b2\u0006\u0010l\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u0006H\u0002J\u0010\u0010z\u001a\u00020>2\u0006\u0010t\u001a\u00020@H\u0002J \u0010{\u001a\u00020\b2\u0006\u0010|\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u00062\u0006\u0010l\u001a\u00020\bH\u0002J \u0010}\u001a\u00020\b2\u0006\u0010|\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u00062\u0006\u0010l\u001a\u00020\bH\u0002J \u0010~\u001a\u00020\b2\u0006\u0010|\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u00062\u0006\u0010l\u001a\u00020\bH\u0002J \u0010\u007f\u001a\u00020\b2\u0006\u0010|\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u00062\u0006\u0010l\u001a\u00020\bH\u0002J!\u0010\u0080\u0001\u001a\u00020\b2\u0006\u0010|\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u00062\u0006\u0010l\u001a\u00020\bH\u0002J!\u0010\u0081\u0001\u001a\u00020\b2\u0006\u0010|\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u00062\u0006\u0010l\u001a\u00020\bH\u0002J!\u0010\u0082\u0001\u001a\u00020\b2\u0006\u0010|\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u00062\u0006\u0010l\u001a\u00020\bH\u0002J!\u0010\u0083\u0001\u001a\u00020\b2\u0006\u0010|\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u00062\u0006\u0010l\u001a\u00020\bH\u0002J!\u0010\u0084\u0001\u001a\u00020\b2\u0006\u0010|\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u00062\u0006\u0010l\u001a\u00020\bH\u0002J\u0019\u0010\u0085\u0001\u001a\u00020\b2\u0006\u0010l\u001a\u00020\b2\u0006\u0010N\u001a\u00020\u0006H\u0002J%\u0010\u0086\u0001\u001a\u00020>2\b\u0010\u0087\u0001\u001a\u00030\u0088\u00012\u0007\u0010\u0089\u0001\u001a\u00020`2\u0007\u0010\u008a\u0001\u001a\u00020\bH\u0002J\t\u0010\u008b\u0001\u001a\u00020>H\u0002J!\u0010\u008c\u0001\u001a\u00020>2\u0016\u0010D\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u000ej\b\u0012\u0004\u0012\u00020\u0006`EH\u0002J!\u0010\u008d\u0001\u001a\u00020>2\u0016\u0010D\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u000ej\b\u0012\u0004\u0012\u00020\u0006`EH\u0002J!\u0010\u008e\u0001\u001a\u00020>2\u0016\u0010D\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u000ej\b\u0012\u0004\u0012\u00020\u0006`EH\u0002J\t\u0010\u008f\u0001\u001a\u00020>H\u0002J\t\u0010\u0090\u0001\u001a\u00020>H\u0002J\t\u0010\u0091\u0001\u001a\u00020>H\u0002J\"\u0010\u0092\u0001\u001a\u00020>2\u0017\u0010\u0093\u0001\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u000ej\b\u0012\u0004\u0012\u00020\u0006`EH\u0002J\t\u0010\u0094\u0001\u001a\u00020>H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00100\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00101\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00102\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00103\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00104\u001a\u000205X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u00106\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u000208X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010:\u001a\b\u0012\u0004\u0012\u00020\u00060\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010;\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010<\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0096\u0001"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/viewdetails/primary/editprimary/EditPrimaryFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/EditPrimaryFragmentBinding;", "alsEmail", "", "alsNumber", "", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/EditPrimaryFragmentBinding;", "clubMember500", "countriesItemsList", "Ljava/util/ArrayList;", "countryCodeList", "dob", "eyeColor", "eyeColorCodeList", "eyeItemsList", "firstName", "gender", "genderItemsList", "genderKeyList", "hairColor", "hairColorCodeList", "hairItemsList", "height", "heightItemList", "homeNum", "landingScreenActivityViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "lastName", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "mailCity", "mailCountry", "mailState", "mailStreet", "mailZipcode", "middleName", "resCity", "resCountry", "resState", "resStreet", "resZipcode", "stateMailCodeItemsList", "stateMailValueItemsList", "stateResCodeItemsList", "stateResValueItemsList", "suffix", "suffixList", "timeStateSet", "", "userName", "viewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/viewdetails/primary/PrimaryAccountViewModel;", "weight", "weightItemList", "workNum", "zipcodeCity", "autoApiHitZipcode", "", "userProfileResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/profile/UserProfileResponse;", "buttonHandler", "clearArrayList", "countryDropdown", "list", "Lkotlin/collections/ArrayList;", "editTextInputType", "eyeDropdown", "eyeColors", "genderDropdown", "getCountryCode", "country", "getGenderCode", "getPosition", "name", "of", "getResidentCode", "resident", "hairDropdown", "hairColours", "heightDropdown", "heightList", "isEverythingSaved", "", "isFieldAreNotEmpty", "masterDataViewModelObsr", "onCreateAnimation", "Landroid/view/animation/Animation;", "transit", "enter", "nextAnim", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onPause", "onViewCreated", "view", "saveProfileDetails", "setCountry", "position", "setEye", "setGender", "setHair", "setHairAndEyeColor", "code", "setHegiht", "setMailCityAndCountryUI", "response", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/zipCode/ZipcodeResponse;", "setMailState", "setResCityAndCountryUI", "setResState", "setSuffix", "setUpView", "setUserCountryData", "i", "setUserEyeData", "setUserGenderData", "setUserHairData", "setUserHeightData", "setUserMailStateData", "setUserStateData", "setUserSuffixData", "setUserWeightData", "setWeight", "showFragmentNavigationAlert", "mContext", "Landroid/content/Context;", "mView", "viewId", "showUnsavedAlert", "stateMailDropdown", "stateResidentialDropdown", "suffixDropdown", "updateProfileResponse", "updatingUserProfile", "userProfileViewModelObsr", "weightDropdown", "weightList", "zipcodeViewModelObsr", "Companion", "app_release"})
@dagger.hilt.android.AndroidEntryPoint
public final class EditPrimaryFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.editprimary.EditPrimaryFragment.Companion Companion = null;
    private java.lang.String zipcodeCity = "";
    private com.fwp.deloittedctcustomerapp.databinding.EditPrimaryFragmentBinding _binding;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.PrimaryAccountViewModel viewModel;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel landingScreenActivityViewModel;
    private final java.util.ArrayList<java.lang.String> eyeItemsList = null;
    private final java.util.ArrayList<java.lang.String> countryCodeList = null;
    private final java.util.ArrayList<java.lang.String> suffixList = null;
    private final java.util.ArrayList<java.lang.String> hairItemsList = null;
    private final java.util.ArrayList<java.lang.String> countriesItemsList = null;
    private final java.util.ArrayList<java.lang.String> genderItemsList = null;
    private final java.util.ArrayList<java.lang.String> genderKeyList = null;
    private final java.util.ArrayList<java.lang.String> stateResCodeItemsList = null;
    private final java.util.ArrayList<java.lang.String> stateMailCodeItemsList = null;
    private final java.util.ArrayList<java.lang.String> stateResValueItemsList = null;
    private final java.util.ArrayList<java.lang.String> stateMailValueItemsList = null;
    private final java.util.ArrayList<java.lang.String> heightItemList = null;
    private final java.util.ArrayList<java.lang.String> weightItemList = null;
    private final java.util.ArrayList<java.lang.String> eyeColorCodeList = null;
    private final java.util.ArrayList<java.lang.String> hairColorCodeList = null;
    private java.lang.String userName = "";
    private final long timeStateSet = 100L;
    private java.lang.String firstName = "";
    private java.lang.String lastName = "";
    private java.lang.String middleName = "";
    private int suffix = 0;
    private java.lang.String resStreet = "";
    private java.lang.String resCity = "";
    private int resState = 0;
    private java.lang.String resZipcode = "";
    private int resCountry = 0;
    private java.lang.String mailStreet = "";
    private java.lang.String mailCity = "";
    private int mailState = 0;
    private java.lang.String mailZipcode = "";
    private int mailCountry = 0;
    private java.lang.String workNum = "";
    private java.lang.String homeNum = "";
    private java.lang.String alsEmail = "";
    private int gender = 0;
    private int eyeColor = 0;
    private int hairColor = 0;
    private int height = 0;
    private int weight = 0;
    private java.lang.String dob = "";
    private int alsNumber = 0;
    private java.lang.String clubMember500 = "";
    
    public EditPrimaryFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.EditPrimaryFragmentBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    @java.lang.Override
    public android.view.animation.Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return null;
    }
    
    @java.lang.Override
    public void onPause() {
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void masterDataViewModelObsr() {
    }
    
    private final void userProfileViewModelObsr() {
    }
    
    private final void setUpView(com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse response) {
    }
    
    private final void updateProfileResponse() {
    }
    
    private final void zipcodeViewModelObsr() {
    }
    
    private final void setResCityAndCountryUI(com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse response) {
    }
    
    private final void setMailCityAndCountryUI(com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse response) {
    }
    
    private final int setHairAndEyeColor(java.lang.String code, java.lang.String of) {
        return 0;
    }
    
    private final int getPosition(java.lang.String name, java.lang.String of) {
        return 0;
    }
    
    private final int setWeight(int position, java.lang.String name) {
        return 0;
    }
    
    private final int setHegiht(int position, java.lang.String name) {
        return 0;
    }
    
    private final int setGender(int position, java.lang.String name) {
        return 0;
    }
    
    private final int setCountry(int position, java.lang.String name) {
        return 0;
    }
    
    private final int setResState(int position, java.lang.String name) {
        return 0;
    }
    
    private final int setMailState(int position, java.lang.String name) {
        return 0;
    }
    
    private final int setSuffix(int position, java.lang.String name) {
        return 0;
    }
    
    private final int setHair(int position, java.lang.String name) {
        return 0;
    }
    
    private final int setEye(int position, java.lang.String name) {
        return 0;
    }
    
    private final int setUserWeightData(int i, java.lang.String name, int position) {
        return 0;
    }
    
    private final int setUserHeightData(int i, java.lang.String name, int position) {
        return 0;
    }
    
    private final int setUserGenderData(int i, java.lang.String name, int position) {
        return 0;
    }
    
    private final int setUserCountryData(int i, java.lang.String name, int position) {
        return 0;
    }
    
    private final int setUserStateData(int i, java.lang.String name, int position) {
        return 0;
    }
    
    private final int setUserMailStateData(int i, java.lang.String name, int position) {
        return 0;
    }
    
    private final int setUserSuffixData(int i, java.lang.String name, int position) {
        return 0;
    }
    
    private final int setUserHairData(int i, java.lang.String name, int position) {
        return 0;
    }
    
    private final int setUserEyeData(int i, java.lang.String name, int position) {
        return 0;
    }
    
    private final void editTextInputType() {
    }
    
    private final void suffixDropdown(java.util.ArrayList<java.lang.String> list) {
    }
    
    private final void stateResidentialDropdown(java.util.ArrayList<java.lang.String> list) {
    }
    
    private final void stateMailDropdown(java.util.ArrayList<java.lang.String> list) {
    }
    
    private final void countryDropdown(java.util.ArrayList<java.lang.String> list) {
    }
    
    private final void genderDropdown(java.util.ArrayList<java.lang.String> list) {
    }
    
    private final void eyeDropdown(java.util.ArrayList<java.lang.String> eyeColors) {
    }
    
    private final void hairDropdown(java.util.ArrayList<java.lang.String> hairColours) {
    }
    
    private final void heightDropdown(java.util.ArrayList<java.lang.String> heightList) {
    }
    
    private final void weightDropdown(java.util.ArrayList<java.lang.String> weightList) {
    }
    
    private final void buttonHandler() {
    }
    
    private final void showUnsavedAlert() {
    }
    
    private final boolean isEverythingSaved() {
        return false;
    }
    
    private final void saveProfileDetails() {
    }
    
    private final void showFragmentNavigationAlert(android.content.Context mContext, android.view.View mView, int viewId) {
    }
    
    private final void autoApiHitZipcode(com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse userProfileResponse) {
    }
    
    private final void updatingUserProfile() {
    }
    
    private final java.lang.String getCountryCode(java.lang.String country) {
        return null;
    }
    
    private final java.lang.String getGenderCode(java.lang.String gender) {
        return null;
    }
    
    private final java.lang.String getResidentCode(java.lang.String resident) {
        return null;
    }
    
    private final void clearArrayList() {
    }
    
    private final boolean isFieldAreNotEmpty() {
        return false;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/viewdetails/primary/editprimary/EditPrimaryFragment$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/viewdetails/primary/editprimary/EditPrimaryFragment;", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.editprimary.EditPrimaryFragment newInstance() {
            return null;
        }
    }
}