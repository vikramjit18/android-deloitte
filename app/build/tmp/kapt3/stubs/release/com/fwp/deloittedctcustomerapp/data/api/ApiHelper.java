package com.fwp.deloittedctcustomerapp.data.api;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0080\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J+\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ+\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\f\u001a\u00020\rH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000eJ!\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u00032\b\b\u0001\u0010\u0011\u001a\u00020\u0012H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013J!\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00100\u00032\b\b\u0001\u0010\u0015\u001a\u00020\u0016H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017J!\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001aJ!\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00100\u00032\b\b\u0001\u0010\u001c\u001a\u00020\u001dH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001eJ!\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020 0\u00032\b\b\u0001\u0010!\u001a\u00020\"H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010#J!\u0010$\u001a\b\u0012\u0004\u0012\u00020%0\u00032\b\b\u0001\u0010&\u001a\u00020\'H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010(J+\u0010)\u001a\b\u0012\u0004\u0012\u00020*0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010+\u001a\u00020,H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010-J!\u0010.\u001a\b\u0012\u0004\u0012\u00020/0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001aJ\u0017\u00100\u001a\b\u0012\u0004\u0012\u0002010\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00102J!\u00103\u001a\b\u0012\u0004\u0012\u0002040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001aJ!\u00105\u001a\b\u0012\u0004\u0012\u0002060\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001aJ+\u00107\u001a\b\u0012\u0004\u0012\u0002080\u00032\b\b\u0001\u00109\u001a\u00020:2\b\b\u0001\u0010;\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010<J!\u0010=\u001a\b\u0012\u0004\u0012\u00020>0\u00032\b\b\u0001\u0010?\u001a\u00020@H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010AJ+\u0010B\u001a\b\u0012\u0004\u0012\u00020C0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010D\u001a\u00020EH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010FJ+\u0010G\u001a\b\u0012\u0004\u0012\u00020C0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010H\u001a\u00020IH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010JJ+\u0010K\u001a\b\u0012\u0004\u0012\u00020L0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010M\u001a\u00020NH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010OJ+\u0010P\u001a\b\u0012\u0004\u0012\u00020L0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010Q\u001a\u00020RH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010SJ!\u0010T\u001a\b\u0012\u0004\u0012\u00020 0\u00032\b\b\u0001\u0010U\u001a\u00020VH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010WJ+\u0010X\u001a\b\u0012\u0004\u0012\u0002060\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010Y\u001a\u00020ZH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010[J+\u0010\\\u001a\b\u0012\u0004\u0012\u00020]0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010^\u001a\u00020_H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010`\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006a"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/api/ApiHelper;", "", "changeFWPEmail", "Lretrofit2/Response;", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/changeFwpEmail/ChangeFwpEmailResponse;", "token", "", "changeFwpEmailRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ChangeFwpEmailRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/ChangeFwpEmailRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "changePassword", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/changePasswordResponse/ChangePasswordResponse;", "changePasswordRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ChangePasswordRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/ChangePasswordRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "checkExistingEmail", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/signup/CheckCredsResponse;", "checkEmailRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/CheckEmailRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/CheckEmailRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "checkExistingUsername", "checkUsernameRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/CheckUsernameRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/CheckUsernameRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "checkUserStatus", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/userStatus/UserStatusResponse;", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "createNewUser", "signupRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/SignupRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/SignupRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "forgetPassword", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/forgotPass/ForgotResponse;", "forgotPasswordRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ForgotPasswordRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/ForgotPasswordRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getDownloadOffLoadTags", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/downloadOffload/DownloadOffloadResponse;", "downloadOffloadRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/DownloadOffloadRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/DownloadOffloadRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getEtags", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagResponse;", "etagRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/EtagRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/EtagRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getItemsHeld", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/ItemsHeldResponse;", "getMasterData", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/MasterData;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getUserAgreement", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/agreement/AgreemnetResponse;", "getUserProfile", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/profile/UserProfileResponse;", "getZipCode", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/zipCode/ZipcodeResponse;", "param1", "", "param2", "(ILjava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "login", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/login/LoginResponse;", "loginRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/LoginRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/LoginRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "postAlsLinking", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/als/AlsResponse;", "alsLinkingRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/AlsLinkingRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/AlsLinkingRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "postAlsLookUp", "alsLookupRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/AlsLookupRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/AlsLookupRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "requestPrimaryEmailCopies", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/requestEmail/RequestEmailResponse;", "requestCopyPrimaryRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/RequestCopyPrimaryRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/RequestCopyPrimaryRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "requestSecondaryEmailCopies", "requestCopySecondaryRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/RequestCopySecondaryRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/RequestCopySecondaryRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "resetPassword", "resetPasswordRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ResetPasswordRequest;", "(Lcom/fwp/deloittedctcustomerapp/data/model/requests/ResetPasswordRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateUserProfile", "updateUserProfileRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/UpdateUserProfileRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/UpdateUserProfileRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "validateEtags", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/validateEtag/ValidateEtagResponse;", "validateEtagRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ValidateEtagRequest;", "(Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/requests/ValidateEtagRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_release"})
public abstract interface ApiHelper {
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "users/user")
    public abstract java.lang.Object createNewUser(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.SignupRequest signupRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "users/emailids")
    public abstract java.lang.Object checkExistingEmail(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.CheckEmailRequest checkEmailRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "users/usernames")
    public abstract java.lang.Object checkExistingUsername(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.CheckUsernameRequest checkUsernameRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.signup.CheckCredsResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "passwordreset")
    public abstract java.lang.Object resetPassword(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.ResetPasswordRequest resetPasswordRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "forgotpassword")
    public abstract java.lang.Object forgetPassword(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.ForgotPasswordRequest forgotPasswordRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "login")
    public abstract java.lang.Object login(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.LoginRequest loginRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "alslinking")
    public abstract java.lang.Object postAlsLinking(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.AlsLinkingRequest alsLinkingRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "alslookup")
    public abstract java.lang.Object postAlsLookUp(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.AlsLookupRequest alsLookupRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "users/changefwpemail")
    public abstract java.lang.Object changeFWPEmail(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.ChangeFwpEmailRequest changeFwpEmailRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.changeFwpEmail.ChangeFwpEmailResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "alsdata/v2/updateuser")
    public abstract java.lang.Object updateUserProfile(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.UpdateUserProfileRequest updateUserProfileRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "alsdata/v2/fetchuser")
    public abstract java.lang.Object getUserProfile(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.GET(value = "zip")
    public abstract java.lang.Object getZipCode(@retrofit2.http.Query(value = "zipcd")
    int param1, @org.jetbrains.annotations.NotNull
    @retrofit2.http.Query(value = "isresident")
    java.lang.String param2, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "itemsheld")
    public abstract java.lang.Object getItemsHeld(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "getetags")
    public abstract java.lang.Object getEtags(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest etagRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.GET(value = "masterdata")
    public abstract java.lang.Object getMasterData(@org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.masterData.MasterData>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "users/tags/download")
    public abstract java.lang.Object getDownloadOffLoadTags(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest downloadOffloadRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "users/tags/validate")
    public abstract java.lang.Object validateEtags(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest validateEtagRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.validateEtag.ValidateEtagResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "users/emailitems")
    public abstract java.lang.Object requestPrimaryEmailCopies(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopyPrimaryRequest requestCopyPrimaryRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "users/secondary/emailitems")
    public abstract java.lang.Object requestSecondaryEmailCopies(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopySecondaryRequest requestCopySecondaryRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "passwordchange")
    public abstract java.lang.Object changePassword(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.fwp.deloittedctcustomerapp.data.model.requests.ChangePasswordRequest changePasswordRequest, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ChangePasswordResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "accepttou")
    public abstract java.lang.Object getUserAgreement(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.agreement.AgreemnetResponse>> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "users/usrstatus")
    public abstract java.lang.Object checkUserStatus(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Header(value = "jwt")
    java.lang.String token, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.UserStatusResponse>> continuation);
}