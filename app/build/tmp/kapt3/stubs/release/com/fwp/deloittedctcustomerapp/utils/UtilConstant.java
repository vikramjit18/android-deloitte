package com.fwp.deloittedctcustomerapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00003\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0003\b\u0097\u0001\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0018\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u000e\u0010\u001d\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0011\u0010 \u001a\u00020!\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u000e\u0010$\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001a\u0010&\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u000e\u0010,\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001a\u0010.\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u0010\b\"\u0004\b0\u0010\nR\u000e\u00101\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00102\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00103\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00104\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00105\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00106\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00108\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010:\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010;\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010<\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010=\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010>\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010?\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010@\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010A\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010B\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010C\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010D\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010E\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010F\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001a\u0010G\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bH\u0010\b\"\u0004\bI\u0010\nR\u001a\u0010J\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bK\u0010)\"\u0004\bL\u0010+R\u000e\u0010M\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010N\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010O\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010P\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010Q\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010R\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010S\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010T\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010U\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010V\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001a\u0010W\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bX\u0010\u001a\"\u0004\bY\u0010\u001cR\u000e\u0010Z\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010[\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\\\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010]\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010^\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010_\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010`\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010a\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0011\u0010c\u001a\u00020!\u00a2\u0006\b\n\u0000\u001a\u0004\bd\u0010#R\u0011\u0010e\u001a\u00020!\u00a2\u0006\b\n\u0000\u001a\u0004\bf\u0010#R\u0011\u0010g\u001a\u00020!\u00a2\u0006\b\n\u0000\u001a\u0004\bh\u0010#R\u0011\u0010i\u001a\u00020!\u00a2\u0006\b\n\u0000\u001a\u0004\bj\u0010#R\u0011\u0010k\u001a\u00020!\u00a2\u0006\b\n\u0000\u001a\u0004\bl\u0010#R\u000e\u0010m\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010o\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010p\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001a\u0010q\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\br\u0010\u001a\"\u0004\bs\u0010\u001cR\u000e\u0010t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010u\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010v\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010w\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010x\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0011\u0010y\u001a\u00020!\u00a2\u0006\b\n\u0000\u001a\u0004\bz\u0010#R\u000e\u0010{\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0011\u0010|\u001a\u00020!\u00a2\u0006\b\n\u0000\u001a\u0004\b}\u0010#R\u000e\u0010~\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u007f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000f\u0010\u0080\u0001\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0081\u0001\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0082\u0001\u0010\u001a\"\u0005\b\u0083\u0001\u0010\u001cR\u001d\u0010\u0084\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0085\u0001\u0010)\"\u0005\b\u0086\u0001\u0010+R\u001d\u0010\u0087\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0088\u0001\u0010)\"\u0005\b\u0089\u0001\u0010+R\u001d\u0010\u008a\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u008b\u0001\u0010)\"\u0005\b\u008c\u0001\u0010+R\u001d\u0010\u008d\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u008e\u0001\u0010)\"\u0005\b\u008f\u0001\u0010+R\u001d\u0010\u0090\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0091\u0001\u0010)\"\u0005\b\u0092\u0001\u0010+R\u001d\u0010\u0093\u0001\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0094\u0001\u0010\b\"\u0005\b\u0095\u0001\u0010\nR\u000f\u0010\u0096\u0001\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0097\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0098\u0001\u0010)\"\u0005\b\u0099\u0001\u0010+R\u001d\u0010\u009a\u0001\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u009b\u0001\u0010\b\"\u0005\b\u009c\u0001\u0010\nR\u001d\u0010\u009d\u0001\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u009e\u0001\u0010\b\"\u0005\b\u009f\u0001\u0010\nR\u001d\u0010\u00a0\u0001\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00a1\u0001\u0010\b\"\u0005\b\u00a2\u0001\u0010\nR\u001d\u0010\u00a3\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00a3\u0001\u0010)\"\u0005\b\u00a4\u0001\u0010+R\u001d\u0010\u00a5\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00a5\u0001\u0010)\"\u0005\b\u00a6\u0001\u0010+R\u001d\u0010\u00a7\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00a7\u0001\u0010)\"\u0005\b\u00a8\u0001\u0010+R\u001d\u0010\u00a9\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00a9\u0001\u0010)\"\u0005\b\u00aa\u0001\u0010+R\u001d\u0010\u00ab\u0001\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00ac\u0001\u0010\b\"\u0005\b\u00ad\u0001\u0010\nR\u001d\u0010\u00ae\u0001\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00af\u0001\u0010\b\"\u0005\b\u00b0\u0001\u0010\nR\u001d\u0010\u00b1\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00b2\u0001\u0010)\"\u0005\b\u00b3\u0001\u0010+R\u000f\u0010\u00b4\u0001\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u00b5\u0001\u001a\u00020\'X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00b6\u0001\u0010)\"\u0005\b\u00b7\u0001\u0010+R\u001d\u0010\u00b8\u0001\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00b9\u0001\u0010\u001a\"\u0005\b\u00ba\u0001\u0010\u001cR\u001f\u0010\u00bb\u0001\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u00bc\u0001\u0010\u001a\"\u0005\b\u00bd\u0001\u0010\u001c\u00a8\u0006\u00be\u0001"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/UtilConstant;", "", "()V", "ACCOUNT_TYPE", "", "ACCOUNT_VIEWPAGER", "", "getACCOUNT_VIEWPAGER", "()I", "setACCOUNT_VIEWPAGER", "(I)V", "ALS_LINKED", "ALS_NOT_LINKED", "ALS_PATTERN", "Lkotlin/text/Regex;", "getALS_PATTERN", "()Lkotlin/text/Regex;", "BIOMETRIC_ENABLE", "CHECK_USER", "COUNTRY", "CURRENT_INDEX", "CURRENT_YEAR", "DATE_FORMAT_5", "DATE_PATTERN", "DEVICE_ID", "getDEVICE_ID", "()Ljava/lang/String;", "setDEVICE_ID", "(Ljava/lang/String;)V", "DISMISSIBLE_BANNER_ETAG", "DISMISSIBLE_BANNER_ITEM", "DOWNLOAD", "EMAIL_ADDRESS_PATTERN", "Ljava/util/regex/Pattern;", "getEMAIL_ADDRESS_PATTERN", "()Ljava/util/regex/Pattern;", "EMAIL_TEXT", "ERROR_CODE", "ETAGS_MULTIPLE", "", "getETAGS_MULTIPLE", "()Z", "setETAGS_MULTIPLE", "(Z)V", "ETAG_CON", "ETAG_DES", "ETAG_LAST_VIEW_INDEX", "getETAG_LAST_VIEW_INDEX", "setETAG_LAST_VIEW_INDEX", "ETAG_OP", "ETAG_OWNER", "EYE", "FAILURE_MSG", "FAIL_CODE", "FEMALE", "FORGOT_PASS_EMAIL_KEY", "FWP_EMAIL_LOGIN", "GENDER", "GET_ALS_NO", "GET_ANIMAL_NAME", "GET_ANIMAL_REGION", "GET_DOB", "GET_ETAG_ACCOUNT_OWNER", "GET_ETAG_INDEX_KEY", "GET_ETAG_MODEL", "GET_ETAG_YEAR_KEY", "GET_ITEM_ACCOUNT", "HAIR", "HARVEST_TIME", "HEIGHT", "INVALID_MSG", "ITEMS_LAST_VIEW_INDEX", "getITEMS_LAST_VIEW_INDEX", "setITEMS_LAST_VIEW_INDEX", "ITEMS_MULTIPLE", "getITEMS_MULTIPLE", "setITEMS_MULTIPLE", "ITEM_HELD", "ITEM_POSITION", "JWT_TOKEN", "LBS", "LICENSE", "LICENSE_LISTING", "LOGIN_DATE", "LOGOUT_PERIOD", "MAIL_STATE", "MASTER_DATA", "MINOR_NAME", "getMINOR_NAME", "setMINOR_NAME", "MULTIPLE_ACCOUNT_KEY", "MULTI_ACCOUNT", "MULTI_ALS_NO", "MULTI_LINKED", "MULTI_NAME", "MULTI_Resident", "NA", "NAME", "OFFLOAD", "PASSWORD_PATTERN", "getPASSWORD_PATTERN", "PASSWORD_PATTERN1", "getPASSWORD_PATTERN1", "PASSWORD_PATTERN2", "getPASSWORD_PATTERN2", "PASSWORD_PATTERN3", "getPASSWORD_PATTERN3", "PHONE_PATTERN", "getPHONE_PATTERN", "PREVIOUS_INDEX", "PREVIOUS_YEAR", "PRIMARYUSER", "PRIMARY_ACCOUNT_KEY", "PRIMARY_ACCOUNT_OWNER", "getPRIMARY_ACCOUNT_OWNER", "setPRIMARY_ACCOUNT_OWNER", "SNACKBAR_COUNT", "STATE", "SUCCESS_CODE", "SUCCESS_MSG", "SUFFIX", "URL_PATTERN", "getURL_PATTERN", "USERNAME_LOGIN", "USERNAME_PATTERN", "getUSERNAME_PATTERN", "USER_ETAG", "USER_PROFILE", "WEIGHT", "abc", "getAbc", "setAbc", "alsExpiredDialog", "getAlsExpiredDialog", "setAlsExpiredDialog", "backStatus", "getBackStatus", "setBackStatus", "biometric", "getBiometric", "setBiometric", "biometricAuthDone", "getBiometricAuthDone", "setBiometricAuthDone", "checkViewModel", "getCheckViewModel", "setCheckViewModel", "currentYear", "getCurrentYear", "setCurrentYear", "dateFormat", "emailSent", "getEmailSent", "setEmailSent", "etagLastIndex", "getEtagLastIndex", "setEtagLastIndex", "etagViewPagerLastIndex", "getEtagViewPagerLastIndex", "setEtagViewPagerLastIndex", "getLocCount", "getGetLocCount", "setGetLocCount", "isAccountActivated", "setAccountActivated", "isAlsActivated", "setAlsActivated", "isAlsLinked", "setAlsLinked", "isProfileUpdate", "setProfileUpdate", "itemHeldViewPagerLastIndex", "getItemHeldViewPagerLastIndex", "setItemHeldViewPagerLastIndex", "itemLastIndex", "getItemLastIndex", "setItemLastIndex", "multiAccount", "getMultiAccount", "setMultiAccount", "snackbarCount", "snackbarShown", "getSnackbarShown", "setSnackbarShown", "uid", "getUid", "setUid", "userName", "getUserName", "setUserName", "app_release"})
public final class UtilConstant {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.utils.UtilConstant INSTANCE = null;
    private static int getLocCount = 0;
    private static boolean isProfileUpdate = false;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String GET_ALS_NO = "getAlsNO";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String GET_DOB = "getDOB";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String DATE_FORMAT_5 = "dd/MM/yyyy hh:mm:ss";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String GET_ITEM_ACCOUNT = "getItemAccount";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String BIOMETRIC_ENABLE = "biomtric_enabled";
    private static boolean biometric = false;
    private static boolean emailSent = false;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String DISMISSIBLE_BANNER_ETAG = "dismissibleBannerEtag";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String DISMISSIBLE_BANNER_ITEM = "dismissibleBannerItem";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String FWP_EMAIL_LOGIN = "fwpEmailForChange";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String USERNAME_LOGIN = "UserName_Login";
    @org.jetbrains.annotations.NotNull
    private static java.lang.String MINOR_NAME = "";
    private static int ACCOUNT_VIEWPAGER = 0;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String NAME = "name";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String SNACKBAR_COUNT = "snackbarCount";
    public static final int snackbarCount = 0;
    public static final int PRIMARYUSER = 0;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String LOGIN_DATE = "login_date_1";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String LOGOUT_PERIOD = "logout_period";
    @org.jetbrains.annotations.NotNull
    private static java.lang.String PRIMARY_ACCOUNT_OWNER = "";
    private static boolean snackbarShown = false;
    @org.jetbrains.annotations.Nullable
    private static java.lang.String userName = "";
    private static boolean checkViewModel = true;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String NA = "N/A";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String SUCCESS_MSG = "success";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String FAILURE_MSG = "failure";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String INVALID_MSG = "invalid";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String LICENSE = "license";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String LICENSE_LISTING = "License Listing";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String EYE = "eye";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String HAIR = "hair";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String WEIGHT = "weight";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String HEIGHT = "height";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String COUNTRY = "country";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String STATE = "state";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String MAIL_STATE = "mailState";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String GENDER = "gender";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String SUFFIX = "suffix";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String FEMALE = "Female";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ITEM_POSITION = "itemPosition";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String LBS = "lbs";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String FAIL_CODE = "f";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String SUCCESS_CODE = "s";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ERROR_CODE = "e";
    public static final int CURRENT_INDEX = 0;
    public static final int PREVIOUS_INDEX = 1;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String PREVIOUS_YEAR = "previous";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String CURRENT_YEAR = "current";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String DOWNLOAD = "D";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String OFFLOAD = "O";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String PRIMARY_ACCOUNT_KEY = "P";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String MULTIPLE_ACCOUNT_KEY = "S";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String MULTI_NAME = "multiName";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String MULTI_ALS_NO = "multiAlsNo";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String MULTI_Resident = "multiResident";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String MULTI_LINKED = "multiLinked";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ACCOUNT_TYPE = "account_type";
    @org.jetbrains.annotations.NotNull
    private static final kotlin.text.Regex ALS_PATTERN = null;
    private static boolean isAlsLinked = false;
    private static boolean isAlsActivated = false;
    private static boolean multiAccount = false;
    private static boolean isAccountActivated = false;
    @org.jetbrains.annotations.NotNull
    private static java.lang.String DEVICE_ID = "";
    private static boolean alsExpiredDialog = true;
    private static boolean backStatus = false;
    @org.jetbrains.annotations.NotNull
    private static final java.util.regex.Pattern PASSWORD_PATTERN1 = null;
    @org.jetbrains.annotations.NotNull
    private static final java.util.regex.Pattern PASSWORD_PATTERN2 = null;
    @org.jetbrains.annotations.NotNull
    private static final java.util.regex.Pattern PASSWORD_PATTERN3 = null;
    @org.jetbrains.annotations.NotNull
    private static final java.util.regex.Pattern PASSWORD_PATTERN = null;
    @org.jetbrains.annotations.NotNull
    private static final java.util.regex.Pattern EMAIL_ADDRESS_PATTERN = null;
    @org.jetbrains.annotations.NotNull
    private static final java.util.regex.Pattern USERNAME_PATTERN = null;
    @org.jetbrains.annotations.NotNull
    private static final java.util.regex.Pattern PHONE_PATTERN = null;
    @org.jetbrains.annotations.NotNull
    private static final java.util.regex.Pattern URL_PATTERN = null;
    @org.jetbrains.annotations.NotNull
    private static java.lang.String abc = "";
    @org.jetbrains.annotations.NotNull
    private static java.lang.String uid = "";
    private static int itemLastIndex = 0;
    private static int etagLastIndex = 0;
    private static int etagViewPagerLastIndex = 0;
    private static int itemHeldViewPagerLastIndex = 0;
    private static boolean ITEMS_MULTIPLE = false;
    private static boolean ETAGS_MULTIPLE = false;
    private static int ITEMS_LAST_VIEW_INDEX = 0;
    private static int ETAG_LAST_VIEW_INDEX = 0;
    private static int currentYear;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String dateFormat = "MM/DD/YYYY";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String DATE_PATTERN = "^(((0[13-9]|1[012])[-/]?(0[1-9]|[12][0-9]|30)|(0[13578]|1[02])[-/]?31|02[-/]?(0[1-9]|1[0-9]|2[0-8]))[-/]?[0-9]{4}|02[-/]?29[-/]?([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00))$";
    private static boolean biometricAuthDone = false;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String EMAIL_TEXT = "com.google.android.gm";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ALS_LINKED = "als_linked";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ALS_NOT_LINKED = "als_not_linked";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String MULTI_ACCOUNT = "multi_account";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String JWT_TOKEN = "jwt";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String FORGOT_PASS_EMAIL_KEY = "forgot_pass_email";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String GET_ETAG_INDEX_KEY = "etag_index";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String GET_ETAG_MODEL = "etag_MODEL";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String GET_ETAG_ACCOUNT_OWNER = "etag_account";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String GET_ETAG_YEAR_KEY = "etag_year";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ETAG_CON = "confirmation_number";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String HARVEST_TIME = "harvest_time";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ETAG_OWNER = "owner";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ETAG_OP = "opportunity";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ETAG_DES = "description";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String MASTER_DATA = "masterData";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String USER_ETAG = "eTags";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String USER_PROFILE = "userProfile";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String CHECK_USER = "checkUser";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ITEM_HELD = "itemHeld";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String GET_ANIMAL_NAME = "animalName";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String GET_ANIMAL_REGION = "animalRe";
    
    private UtilConstant() {
        super();
    }
    
    public final int getGetLocCount() {
        return 0;
    }
    
    public final void setGetLocCount(int p0) {
    }
    
    public final boolean isProfileUpdate() {
        return false;
    }
    
    public final void setProfileUpdate(boolean p0) {
    }
    
    public final boolean getBiometric() {
        return false;
    }
    
    public final void setBiometric(boolean p0) {
    }
    
    public final boolean getEmailSent() {
        return false;
    }
    
    public final void setEmailSent(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getMINOR_NAME() {
        return null;
    }
    
    public final void setMINOR_NAME(@org.jetbrains.annotations.NotNull
    java.lang.String p0) {
    }
    
    public final int getACCOUNT_VIEWPAGER() {
        return 0;
    }
    
    public final void setACCOUNT_VIEWPAGER(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getPRIMARY_ACCOUNT_OWNER() {
        return null;
    }
    
    public final void setPRIMARY_ACCOUNT_OWNER(@org.jetbrains.annotations.NotNull
    java.lang.String p0) {
    }
    
    public final boolean getSnackbarShown() {
        return false;
    }
    
    public final void setSnackbarShown(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getUserName() {
        return null;
    }
    
    public final void setUserName(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
    
    public final boolean getCheckViewModel() {
        return false;
    }
    
    public final void setCheckViewModel(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final kotlin.text.Regex getALS_PATTERN() {
        return null;
    }
    
    public final boolean isAlsLinked() {
        return false;
    }
    
    public final void setAlsLinked(boolean p0) {
    }
    
    public final boolean isAlsActivated() {
        return false;
    }
    
    public final void setAlsActivated(boolean p0) {
    }
    
    public final boolean getMultiAccount() {
        return false;
    }
    
    public final void setMultiAccount(boolean p0) {
    }
    
    public final boolean isAccountActivated() {
        return false;
    }
    
    public final void setAccountActivated(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getDEVICE_ID() {
        return null;
    }
    
    public final void setDEVICE_ID(@org.jetbrains.annotations.NotNull
    java.lang.String p0) {
    }
    
    public final boolean getAlsExpiredDialog() {
        return false;
    }
    
    public final void setAlsExpiredDialog(boolean p0) {
    }
    
    public final boolean getBackStatus() {
        return false;
    }
    
    public final void setBackStatus(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.regex.Pattern getPASSWORD_PATTERN1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.regex.Pattern getPASSWORD_PATTERN2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.regex.Pattern getPASSWORD_PATTERN3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.regex.Pattern getPASSWORD_PATTERN() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.regex.Pattern getEMAIL_ADDRESS_PATTERN() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.regex.Pattern getUSERNAME_PATTERN() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.regex.Pattern getPHONE_PATTERN() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.regex.Pattern getURL_PATTERN() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getAbc() {
        return null;
    }
    
    public final void setAbc(@org.jetbrains.annotations.NotNull
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getUid() {
        return null;
    }
    
    public final void setUid(@org.jetbrains.annotations.NotNull
    java.lang.String p0) {
    }
    
    public final int getItemLastIndex() {
        return 0;
    }
    
    public final void setItemLastIndex(int p0) {
    }
    
    public final int getEtagLastIndex() {
        return 0;
    }
    
    public final void setEtagLastIndex(int p0) {
    }
    
    public final int getEtagViewPagerLastIndex() {
        return 0;
    }
    
    public final void setEtagViewPagerLastIndex(int p0) {
    }
    
    public final int getItemHeldViewPagerLastIndex() {
        return 0;
    }
    
    public final void setItemHeldViewPagerLastIndex(int p0) {
    }
    
    public final boolean getITEMS_MULTIPLE() {
        return false;
    }
    
    public final void setITEMS_MULTIPLE(boolean p0) {
    }
    
    public final boolean getETAGS_MULTIPLE() {
        return false;
    }
    
    public final void setETAGS_MULTIPLE(boolean p0) {
    }
    
    public final int getITEMS_LAST_VIEW_INDEX() {
        return 0;
    }
    
    public final void setITEMS_LAST_VIEW_INDEX(int p0) {
    }
    
    public final int getETAG_LAST_VIEW_INDEX() {
        return 0;
    }
    
    public final void setETAG_LAST_VIEW_INDEX(int p0) {
    }
    
    public final int getCurrentYear() {
        return 0;
    }
    
    public final void setCurrentYear(int p0) {
    }
    
    public final boolean getBiometricAuthDone() {
        return false;
    }
    
    public final void setBiometricAuthDone(boolean p0) {
    }
}