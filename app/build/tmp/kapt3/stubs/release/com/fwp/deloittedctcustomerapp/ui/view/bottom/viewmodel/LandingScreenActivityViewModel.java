package com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0018\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010G\u001a\u00020H2\u0006\u0010I\u001a\u00020\u000bJ\u0016\u0010J\u001a\u00020H2\f\u0010K\u001a\b\u0012\u0004\u0012\u00020\t0LH\u0002J\u0006\u0010M\u001a\u00020HJ\u0006\u0010N\u001a\u00020HJ\u000e\u0010O\u001a\u00020H2\u0006\u0010P\u001a\u00020QJ\u0012\u0010R\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020T0S0\u001fJ\u000e\u0010U\u001a\u00020H2\u0006\u0010V\u001a\u00020WJ\u0006\u0010,\u001a\u00020HJ\u0006\u0010X\u001a\u00020HJ\u0016\u0010Y\u001a\u00020H2\f\u0010K\u001a\b\u0012\u0004\u0012\u00020\u00130LH\u0002J\u0006\u0010Z\u001a\u00020HJ\u0016\u0010[\u001a\u00020H2\f\u0010K\u001a\b\u0012\u0004\u0012\u00020\u001d0LH\u0002J\u000e\u0010\\\u001a\u00020H2\u0006\u0010]\u001a\u00020TJ\u0016\u0010^\u001a\u00020H2\f\u0010K\u001a\b\u0012\u0004\u0012\u00020\u00110LH\u0002J\u000e\u0010_\u001a\u00020H2\u0006\u0010P\u001a\u00020QJ\u000e\u0010`\u001a\u00020H2\u0006\u0010a\u001a\u00020\u000bJ\u000e\u0010b\u001a\u00020H2\u0006\u0010c\u001a\u00020/J\u000e\u0010d\u001a\u00020H2\u0006\u0010e\u001a\u00020\u000bR\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\r0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00160\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00160\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00160\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00160\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001a\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00160\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00160\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001d0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\t0\u001f8F\u00a2\u0006\u0006\u001a\u0004\b \u0010!R\u0011\u0010\"\u001a\u00020#\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0017\u0010&\u001a\b\u0012\u0004\u0012\u00020\u000b0\u001f8F\u00a2\u0006\u0006\u001a\u0004\b\'\u0010!R\u0017\u0010(\u001a\b\u0012\u0004\u0012\u00020\r0\u001f8F\u00a2\u0006\u0006\u001a\u0004\b)\u0010!R\u0017\u0010*\u001a\b\u0012\u0004\u0012\u00020\u000f0\u001f8F\u00a2\u0006\u0006\u001a\u0004\b+\u0010!R\u0017\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00110\u001f8F\u00a2\u0006\u0006\u001a\u0004\b-\u0010!R \u0010.\u001a\b\u0012\u0004\u0012\u00020/0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u00100\"\u0004\b1\u00102R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u00103\u001a\b\u0012\u0004\u0012\u00020\u00130\u001f8F\u00a2\u0006\u0006\u001a\u0004\b4\u0010!R\u0017\u00105\u001a\b\u0012\u0004\u0012\u00020\r0\u001f8F\u00a2\u0006\u0006\u001a\u0004\b6\u0010!R\u0017\u00107\u001a\b\u0012\u0004\u0012\u00020\u000b0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b8\u00100R\u001d\u00109\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00160\u001f8F\u00a2\u0006\u0006\u001a\u0004\b:\u0010!R\u001d\u0010;\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00160\u001f8F\u00a2\u0006\u0006\u001a\u0004\b<\u0010!R\u001d\u0010=\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00160\u001f8F\u00a2\u0006\u0006\u001a\u0004\b>\u0010!R\u001d\u0010?\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00160\u001f8F\u00a2\u0006\u0006\u001a\u0004\b@\u0010!R\u001d\u0010A\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00160\u001f8F\u00a2\u0006\u0006\u001a\u0004\bB\u0010!R\u001d\u0010C\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00160\u001f8F\u00a2\u0006\u0006\u001a\u0004\bD\u0010!R\u0017\u0010E\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001f8F\u00a2\u0006\u0006\u001a\u0004\bF\u0010!\u00a8\u0006f"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "Landroidx/lifecycle/ViewModel;", "mainRepo", "Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;", "appDao", "Lcom/fwp/deloittedctcustomerapp/data/db/AppDao;", "(Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;Lcom/fwp/deloittedctcustomerapp/data/db/AppDao;)V", "_checkUserStatusResponse", "Landroidx/lifecycle/MutableLiveData;", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/userStatus/UserStatusResponse;", "_dbDownloadOffload", "", "_downloadResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/downloadOffload/DownloadOffloadResponse;", "_etagResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagResponse;", "_getItemHeldResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/ItemsHeldResponse;", "_masterDataResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/masterData/MasterData;", "_offloadResponse", "_toastCheckUser", "Lcom/fwp/deloittedctcustomerapp/utils/Event;", "_toastDownloadOffload", "_toastGetEtag", "_toastItemHeld", "_toastMasterUSer", "_toastViewUser", "_viewUserProfileResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/profile/UserProfileResponse;", "checkUserStatusResponse", "Landroidx/lifecycle/LiveData;", "getCheckUserStatusResponse", "()Landroidx/lifecycle/LiveData;", "coroutineExceptionHandler", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "getCoroutineExceptionHandler", "()Lkotlinx/coroutines/CoroutineExceptionHandler;", "dbDownloadOffloadRes", "getDbDownloadOffloadRes", "downloadResponse", "getDownloadResponse", "etagResponse", "getEtagResponse", "getItemHeldResponse", "getGetItemHeldResponse", "isMultiAccount", "", "()Landroidx/lifecycle/MutableLiveData;", "setMultiAccount", "(Landroidx/lifecycle/MutableLiveData;)V", "masterDataResponse", "getMasterDataResponse", "offloadResponse", "getOffloadResponse", "ownerName", "getOwnerName", "toastCheckUser", "getToastCheckUser", "toastDownloadOffload", "getToastDownloadOffload", "toastGetEtag", "getToastGetEtag", "toastItemHeld", "getToastItemHeld", "toastMasterUSer", "getToastMasterUSer", "toastViewUser", "getToastViewUser", "viewUserProfileResponse", "getViewUserProfileResponse", "checkUserStatus", "", "token", "checkUserSuccessRes", "response", "Lretrofit2/Response;", "cleardb", "deleteDownloadedTagSid", "downloadRequest", "downloadOffloadRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/DownloadOffloadRequest;", "getDownloadedTagSid", "", "Lcom/fwp/deloittedctcustomerapp/data/model/DownloadedTags;", "getEtag", "etagRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/EtagRequest;", "getMasterData", "getMasterSuccessRes", "getUserProfile", "getUserSuccessRes", "insertDownloadTag", "downloadedTags", "itemSuccesRes", "offLoadRequest", "removeDownloadTagsById", "onlineSpiId", "setMultiAccountBool", "bool", "setOwnerName", "name", "app_release"})
public final class LandingScreenActivityViewModel extends androidx.lifecycle.ViewModel {
    private final com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo = null;
    private final com.fwp.deloittedctcustomerapp.data.db.AppDao appDao = null;
    @org.jetbrains.annotations.NotNull
    private final androidx.lifecycle.MutableLiveData<java.lang.String> ownerName = null;
    @org.jetbrains.annotations.NotNull
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMultiAccount;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.UserStatusResponse> _checkUserStatusResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse> _viewUserProfileResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse> _etagResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.masterData.MasterData> _masterDataResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse> _offloadResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse> _downloadResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastViewUser = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastMasterUSer = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastCheckUser = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse> _getItemHeldResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastGetEtag = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastItemHeld = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastDownloadOffload = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.String> _dbDownloadOffload = null;
    @org.jetbrains.annotations.NotNull
    private final kotlinx.coroutines.CoroutineExceptionHandler coroutineExceptionHandler = null;
    
    @javax.inject.Inject
    public LandingScreenActivityViewModel(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo, @org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.db.AppDao appDao) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getOwnerName() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMultiAccount() {
        return null;
    }
    
    public final void setMultiAccount(@org.jetbrains.annotations.NotNull
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.UserStatusResponse> getCheckUserStatusResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse> getViewUserProfileResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse> getEtagResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.masterData.MasterData> getMasterDataResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse> getOffloadResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse> getDownloadResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastViewUser() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastMasterUSer() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastCheckUser() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse> getGetItemHeldResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastGetEtag() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastItemHeld() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastDownloadOffload() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.lang.String> getDbDownloadOffloadRes() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final kotlinx.coroutines.CoroutineExceptionHandler getCoroutineExceptionHandler() {
        return null;
    }
    
    public final void cleardb() {
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.util.List<com.fwp.deloittedctcustomerapp.data.model.DownloadedTags>> getDownloadedTagSid() {
        return null;
    }
    
    public final void deleteDownloadedTagSid() {
    }
    
    public final void insertDownloadTag(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.DownloadedTags downloadedTags) {
    }
    
    public final void removeDownloadTagsById(@org.jetbrains.annotations.NotNull
    java.lang.String onlineSpiId) {
    }
    
    public final void setOwnerName(@org.jetbrains.annotations.NotNull
    java.lang.String name) {
    }
    
    public final void setMultiAccountBool(boolean bool) {
    }
    
    public final void getItemHeldResponse() {
    }
    
    private final void itemSuccesRes(retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse> response) {
    }
    
    public final void checkUserStatus(@org.jetbrains.annotations.NotNull
    java.lang.String token) {
    }
    
    private final void checkUserSuccessRes(retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.userStatus.UserStatusResponse> response) {
    }
    
    public final void getMasterData() {
    }
    
    private final void getMasterSuccessRes(retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.masterData.MasterData> response) {
    }
    
    public final void getUserProfile() {
    }
    
    private final void getUserSuccessRes(retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse> response) {
    }
    
    public final void getEtag(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest etagRequest) {
    }
    
    public final void offLoadRequest(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest downloadOffloadRequest) {
    }
    
    public final void downloadRequest(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest downloadOffloadRequest) {
    }
}