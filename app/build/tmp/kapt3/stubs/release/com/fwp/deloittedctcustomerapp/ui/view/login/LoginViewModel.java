package com.fwp.deloittedctcustomerapp.ui.view.login;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\nJ\u0016\u0010\u001f\u001a\u00020\u001d2\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\r0!H\u0002J\u000e\u0010\"\u001a\u00020\u001d2\u0006\u0010#\u001a\u00020$J\u0016\u0010%\u001a\u00020\u001d2\f\u0010 \u001a\b\u0012\u0004\u0012\u00020&0!H\u0002R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00070\u000f8F\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u000f8F\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0011R\u001d\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u000f8F\u00a2\u0006\u0006\u001a\u0004\b\u0019\u0010\u0011R\u0017\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\r0\u000f8F\u00a2\u0006\u0006\u001a\u0004\b\u001b\u0010\u0011\u00a8\u0006\'"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/login/LoginViewModel;", "Landroidx/lifecycle/ViewModel;", "mainRepo", "Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;", "(Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;)V", "_acceptTouResponse", "Landroidx/lifecycle/MutableLiveData;", "", "_toastLoginResponse", "Lcom/fwp/deloittedctcustomerapp/utils/Event;", "", "_toastTouResponse", "_userLoginResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/login/LoginResponse;", "acceptTouResponse", "Landroidx/lifecycle/LiveData;", "getAcceptTouResponse", "()Landroidx/lifecycle/LiveData;", "coroutineExceptionHandler", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "getCoroutineExceptionHandler", "()Lkotlinx/coroutines/CoroutineExceptionHandler;", "toastLoginResponse", "getToastLoginResponse", "toastTouResponse", "getToastTouResponse", "userLoginResponse", "getUserLoginResponse", "acceptTou", "", "token", "loginResponse", "response", "Lretrofit2/Response;", "loginUserRequest", "loginRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/LoginRequest;", "touResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/agreement/AgreemnetResponse;", "app_release"})
public final class LoginViewModel extends androidx.lifecycle.ViewModel {
    private final com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse> _userLoginResponse = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Object> _acceptTouResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastLoginResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastTouResponse = null;
    @org.jetbrains.annotations.NotNull
    private final kotlinx.coroutines.CoroutineExceptionHandler coroutineExceptionHandler = null;
    
    @javax.inject.Inject
    public LoginViewModel(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse> getUserLoginResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.lang.Object> getAcceptTouResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastLoginResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastTouResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final kotlinx.coroutines.CoroutineExceptionHandler getCoroutineExceptionHandler() {
        return null;
    }
    
    public final void loginUserRequest(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.LoginRequest loginRequest) {
    }
    
    private final void loginResponse(retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse> response) {
    }
    
    public final void acceptTou(@org.jetbrains.annotations.NotNull
    java.lang.String token) {
    }
    
    private final void touResponse(retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.agreement.AgreemnetResponse> response) {
    }
}