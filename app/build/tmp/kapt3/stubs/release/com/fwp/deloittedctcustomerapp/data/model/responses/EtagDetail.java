package com.fwp.deloittedctcustomerapp.data.model.responses;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B5\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u000b\u001a\u0004\u0018\u00010\u0003J\b\u0010\f\u001a\u0004\u0018\u00010\u0003J\r\u0010\r\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u000eJ\b\u0010\u000f\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\u0010\u001a\u00020\u00112\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003J\u0010\u0010\u0012\u001a\u00020\u00112\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003J\u0015\u0010\u0013\u001a\u00020\u00112\b\u0010\u0014\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u00112\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/EtagDetail;", "", "specie", "", "region", "year", "isValidated", "", "etagDownload", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/EtagDownload;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/fwp/deloittedctcustomerapp/data/model/responses/EtagDownload;)V", "getRegion", "getSpecie", "getValidation", "()Ljava/lang/Boolean;", "getYear", "setRegion", "", "setSpecie", "setValidation", "value", "(Ljava/lang/Boolean;)V", "setYear", "app_release"})
public final class EtagDetail {
    private java.lang.String specie;
    private java.lang.String region;
    private java.lang.String year;
    private boolean isValidated;
    
    public EtagDetail(@org.jetbrains.annotations.Nullable
    java.lang.String specie, @org.jetbrains.annotations.Nullable
    java.lang.String region, @org.jetbrains.annotations.Nullable
    java.lang.String year, @org.jetbrains.annotations.Nullable
    java.lang.Boolean isValidated, @org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.responses.EtagDownload etagDownload) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Boolean getValidation() {
        return null;
    }
    
    public final void setValidation(@org.jetbrains.annotations.Nullable
    java.lang.Boolean value) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getSpecie() {
        return null;
    }
    
    public final void setSpecie(@org.jetbrains.annotations.Nullable
    java.lang.String specie) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getYear() {
        return null;
    }
    
    public final void setYear(@org.jetbrains.annotations.Nullable
    java.lang.String year) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getRegion() {
        return null;
    }
    
    public final void setRegion(@org.jetbrains.annotations.Nullable
    java.lang.String region) {
    }
}