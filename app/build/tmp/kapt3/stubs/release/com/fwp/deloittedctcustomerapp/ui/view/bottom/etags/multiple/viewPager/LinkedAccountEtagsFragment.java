package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.viewPager;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u00a4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 a2\u00020\u0001:\u0001aB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u001f\u001a\u00020 H\u0002J\b\u0010!\u001a\u00020 H\u0002J\u0018\u0010\"\u001a\u00020 2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&H\u0002J\u0010\u0010\'\u001a\u00020 2\u0006\u0010(\u001a\u00020\u0005H\u0002J\u0010\u0010)\u001a\u00020 2\u0006\u0010#\u001a\u00020$H\u0002J&\u0010*\u001a\u00020 2\u0006\u0010#\u001a\u00020$2\f\u0010+\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020\u0005H\u0002J&\u0010/\u001a\u00020 2\f\u0010+\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010%\u001a\u00020&2\u0006\u0010.\u001a\u00020\u0005H\u0002J.\u00100\u001a\u00020 2\f\u0010+\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010%\u001a\u00020&2\u0006\u00101\u001a\u00020&2\u0006\u0010.\u001a\u00020\u0005H\u0002J\u0010\u00102\u001a\u00020 2\u0006\u00103\u001a\u00020\u0005H\u0002J$\u00104\u001a\u0002052\u0006\u00106\u001a\u0002072\b\u00108\u001a\u0004\u0018\u0001092\b\u0010:\u001a\u0004\u0018\u00010;H\u0016J\b\u0010<\u001a\u00020 H\u0016J\u001a\u0010=\u001a\u00020 2\u0006\u0010>\u001a\u0002052\b\u0010:\u001a\u0004\u0018\u00010;H\u0016J\u0018\u0010?\u001a\u00020 2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&H\u0002J\u0018\u0010@\u001a\u00020 2\u0006\u0010.\u001a\u00020\u00052\u0006\u0010#\u001a\u00020$H\u0002J\b\u0010A\u001a\u00020 H\u0002J\u0018\u0010B\u001a\u00020 2\u0006\u0010C\u001a\u00020$2\u0006\u0010.\u001a\u00020\u0005H\u0003J\u0018\u0010D\u001a\u00020 2\u0006\u0010E\u001a\u00020&2\u0006\u0010F\u001a\u00020GH\u0002J0\u0010H\u001a\u00020 2\u0006\u0010I\u001a\u00020J2\u0006\u0010K\u001a\u00020\u00052\u0006\u0010L\u001a\u00020\u00052\u0006\u0010M\u001a\u00020\u00052\u0006\u0010N\u001a\u00020\u0005H\u0002J\u0018\u0010O\u001a\u00020 2\u0006\u0010P\u001a\u00020J2\u0006\u0010>\u001a\u000205H\u0002J\u0010\u0010Q\u001a\u00020 2\u0006\u0010F\u001a\u00020RH\u0002J\u0018\u0010S\u001a\u00020 2\u0006\u0010E\u001a\u00020&2\u0006\u0010F\u001a\u00020RH\u0002J\u0010\u0010T\u001a\u00020 2\u0006\u0010F\u001a\u00020GH\u0002J0\u0010U\u001a\u00020 2\u0006\u0010I\u001a\u00020J2\u0006\u0010K\u001a\u00020\u00052\u0006\u0010L\u001a\u00020\u00052\u0006\u0010M\u001a\u00020\u00052\u0006\u0010N\u001a\u00020\u0005H\u0002J\u0016\u0010V\u001a\u00020 2\u0006\u0010E\u001a\u00020&2\u0006\u0010F\u001a\u00020GJ\u0018\u0010W\u001a\u00020 2\u0006\u0010E\u001a\u00020&2\u0006\u0010F\u001a\u00020RH\u0002J\u0018\u0010X\u001a\u00020 2\u0006\u0010Y\u001a\u00020Z2\u0006\u0010[\u001a\u00020\u0005H\u0002J\u0018\u0010\\\u001a\u00020 2\u0006\u0010Y\u001a\u00020Z2\u0006\u0010[\u001a\u00020\u0005H\u0002J\b\u0010]\u001a\u00020 H\u0002J\u0010\u0010^\u001a\u00020 2\u0006\u0010.\u001a\u00020\u0005H\u0002J*\u0010_\u001a\u00020 *\u00020`2\f\u0010+\u001a\b\u0012\u0004\u0012\u00020-0,2\u0006\u0010.\u001a\u00020\u00052\u0006\u0010#\u001a\u00020$H\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\u00118BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00050\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006b"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/multiple/viewPager/LinkedAccountEtagsFragment;", "Landroidx/fragment/app/Fragment;", "()V", "allDownLoadCurrentSpiIDLIst", "", "", "allPreviousSpiIdList", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/LinkedEtagsFragmentBinding;", "downloadAllSpiIdList", "downloadAllStatus", "downloadCurrentStatus", "downloadPreStatus", "firstName", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "model", "Lcom/fwp/deloittedctcustomerapp/ui/viewmodel/Communicator;", "getModel", "()Lcom/fwp/deloittedctcustomerapp/ui/viewmodel/Communicator;", "model$delegate", "Lkotlin/Lazy;", "offLoadAllSpiIDLIst", "offLoadAllStatus", "offlineSpiidList", "userList", "Ljava/util/ArrayList;", "validateTagActivityViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/activityViewModel/ValidateTagActivityViewModel;", "viewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "buttonHandler", "", "clearList", "currentMinorListNull", "etagResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagResponse;", "item", "", "downloadObsr", "spiIDLIst", "getMinorNames", "itrationForAll", "resObject", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/ResObject;", "name", "loopEtagAccountList", "loopEtagLicensesList", "etagsI", "offloadObsr", "idString", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onViewCreated", "view", "previousMinorListNull", "recyclerContent", "setSpannableString", "setupUI", "it", "showCurrentTagStateDialog", "position", "holder", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/multiple/linkedAccountAdapter/MultipleEtagCurrentYearAdapter$MyViewHolder;", "showDownloadAllTagStateDialog", "mContext", "Landroid/content/Context;", "tvPrimary", "tvSecondary", "cancelText", "secText", "showPopupMenu", "context", "showPreviousProgressBtn", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/multiple/linkedAccountAdapter/MultipleEtagPreviousYearAdapter$MyMultiPreviousYearViewHolder;", "showPreviousTagStateDialog", "showProgressBtn", "showRemoveAllTagStateDialog", "startCurrentDownloadProcess", "startPreDownloadProcess", "statusSpiidDownload", "response", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagLicenseDetails;", "downloadStatus", "statusSpiidOffload", "userSpinner", "viewModelObsr", "itrationForCurrentYear", "Landroidx/recyclerview/widget/RecyclerView;", "Companion", "app_release"})
public final class LinkedAccountEtagsFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.viewPager.LinkedAccountEtagsFragment.Companion Companion = null;
    private com.fwp.deloittedctcustomerapp.databinding.LinkedEtagsFragmentBinding binding;
    private final kotlin.Lazy model$delegate = null;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel validateTagActivityViewModel;
    private java.lang.String firstName = "Linked minor";
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel viewModel;
    private final java.util.ArrayList<java.lang.String> userList = null;
    private final java.util.List<java.lang.String> downloadCurrentStatus = null;
    private final java.util.List<java.lang.String> allDownLoadCurrentSpiIDLIst = null;
    private final java.util.List<java.lang.String> downloadPreStatus = null;
    private final java.util.List<java.lang.String> allPreviousSpiIdList = null;
    private final java.util.List<java.lang.String> downloadAllStatus = null;
    private final java.util.List<java.lang.String> downloadAllSpiIdList = null;
    private final java.util.List<java.lang.String> offLoadAllStatus = null;
    private final java.util.List<java.lang.String> offLoadAllSpiIDLIst = null;
    private final java.util.List<java.lang.String> offlineSpiidList = null;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    
    public LinkedAccountEtagsFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator getModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override
    public void onResume() {
    }
    
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void buttonHandler() {
    }
    
    private final void userSpinner() {
    }
    
    private final void recyclerContent(java.lang.String name, com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse etagResponse) {
    }
    
    private final void itrationForCurrentYear(androidx.recyclerview.widget.RecyclerView $this$itrationForCurrentYear, java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.ResObject> resObject, java.lang.String name, com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse etagResponse) {
    }
    
    private final void showProgressBtn(com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.linkedAccountAdapter.MultipleEtagCurrentYearAdapter.MyViewHolder holder) {
    }
    
    private final void itrationForAll(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse etagResponse, java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.ResObject> resObject, java.lang.String name) {
    }
    
    private final void loopEtagAccountList(java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.ResObject> resObject, int item, java.lang.String name) {
    }
    
    private final void loopEtagLicensesList(java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.ResObject> resObject, int item, int etagsI, java.lang.String name) {
    }
    
    private final void statusSpiidDownload(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenseDetails response, java.lang.String downloadStatus) {
    }
    
    private final void statusSpiidOffload(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenseDetails response, java.lang.String downloadStatus) {
    }
    
    private final void currentMinorListNull(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse etagResponse, int item) {
    }
    
    private final void previousMinorListNull(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse etagResponse, int item) {
    }
    
    private final void getMinorNames(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse etagResponse) {
    }
    
    private final void startPreDownloadProcess(int position, com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.linkedAccountAdapter.MultipleEtagPreviousYearAdapter.MyMultiPreviousYearViewHolder holder) {
    }
    
    private final void showPreviousProgressBtn(com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.linkedAccountAdapter.MultipleEtagPreviousYearAdapter.MyMultiPreviousYearViewHolder holder) {
    }
    
    public final void startCurrentDownloadProcess(int position, @org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.linkedAccountAdapter.MultipleEtagCurrentYearAdapter.MyViewHolder holder) {
    }
    
    private final void showCurrentTagStateDialog(int position, com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.linkedAccountAdapter.MultipleEtagCurrentYearAdapter.MyViewHolder holder) {
    }
    
    private final void showPreviousTagStateDialog(int position, com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.linkedAccountAdapter.MultipleEtagPreviousYearAdapter.MyMultiPreviousYearViewHolder holder) {
    }
    
    @kotlin.OptIn(markerClass = {androidx.compose.ui.text.android.InternalPlatformTextApi.class})
    private final void setSpannableString() {
    }
    
    private final void showPopupMenu(android.content.Context context, android.view.View view) {
    }
    
    private final void viewModelObsr(java.lang.String name) {
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    private final void setupUI(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse it, java.lang.String name) {
    }
    
    private final void clearList() {
    }
    
    private final void showRemoveAllTagStateDialog(android.content.Context mContext, java.lang.String tvPrimary, java.lang.String tvSecondary, java.lang.String cancelText, java.lang.String secText) {
    }
    
    private final void downloadObsr(java.lang.String spiIDLIst) {
    }
    
    private final void offloadObsr(java.lang.String idString) {
    }
    
    private final void showDownloadAllTagStateDialog(android.content.Context mContext, java.lang.String tvPrimary, java.lang.String tvSecondary, java.lang.String cancelText, java.lang.String secText) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/multiple/viewPager/LinkedAccountEtagsFragment$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/multiple/viewPager/LinkedAccountEtagsFragment;", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.viewPager.LinkedAccountEtagsFragment newInstance() {
            return null;
        }
    }
}