package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u0000 .2\u00020\u0001:\u0001.B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\u0014H\u0002J\b\u0010\u0016\u001a\u00020\u0014H\u0002J$\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J\b\u0010\u001f\u001a\u00020\u0014H\u0016J\b\u0010 \u001a\u00020\u0014H\u0016J\b\u0010!\u001a\u00020\u0014H\u0016J\u001a\u0010\"\u001a\u00020\u00142\u0006\u0010#\u001a\u00020\u00182\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0017J\b\u0010$\u001a\u00020\u0014H\u0002J\u0010\u0010%\u001a\u00020\u00142\u0006\u0010&\u001a\u00020\'H\u0003J&\u0010(\u001a\u00020\u00142\u0006\u0010)\u001a\u00020*2\u0006\u0010#\u001a\u00020\u00182\f\u0010+\u001a\b\u0012\u0004\u0012\u00020-0,H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/primary/ItemsHeldAvailableFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/FragmentItemsHeldAvailableBinding;", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/FragmentItemsHeldAvailableBinding;", "currentYear", "", "listCurrentYearPermitDetail", "", "Lcom/fwp/deloittedctcustomerapp/data/model/ItemDetail;", "listPreviousPermitYearDetail", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "previousYear", "viewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "buttonHandler", "", "checkLiveNetworkConnection", "clearList", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onPause", "onResume", "onStart", "onViewCreated", "view", "setSpannableString", "setupItemHeld", "it", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/ItemsHeldResponse;", "showPopupMenu", "context", "Landroid/content/Context;", "itemsHeld", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/ItemsHeld;", "Companion", "app_release"})
@dagger.hilt.android.AndroidEntryPoint
public final class ItemsHeldAvailableFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.ItemsHeldAvailableFragment.Companion Companion = null;
    private com.fwp.deloittedctcustomerapp.databinding.FragmentItemsHeldAvailableBinding _binding;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel viewModel;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.ItemDetail> listCurrentYearPermitDetail = null;
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.ItemDetail> listPreviousPermitYearDetail = null;
    private java.lang.String currentYear;
    private java.lang.String previousYear;
    
    public ItemsHeldAvailableFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.FragmentItemsHeldAvailableBinding getBinding() {
        return null;
    }
    
    @java.lang.Override
    public void onResume() {
    }
    
    @java.lang.Override
    public void onStart() {
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void clearList() {
    }
    
    private final void checkLiveNetworkConnection() {
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    private final void setupItemHeld(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it) {
    }
    
    private final void buttonHandler() {
    }
    
    @kotlin.OptIn(markerClass = {androidx.compose.ui.text.android.InternalPlatformTextApi.class})
    private final void setSpannableString() {
    }
    
    private final void showPopupMenu(android.content.Context context, android.view.View view, java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeld> itemsHeld) {
    }
    
    @java.lang.Override
    public void onPause() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/primary/ItemsHeldAvailableFragment$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/primary/ItemsHeldAvailableFragment;", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.ItemsHeldAvailableFragment newInstance() {
            return null;
        }
    }
}