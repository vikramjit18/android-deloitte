package com.fwp.deloittedctcustomerapp.data.db;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/db/Params;", "", "()V", "DBNAME", "", "DBVERSION", "", "DOWNLOADED_TAGS_TABLE_NAME", "VALIREQUESTTABLENAME", "app_release"})
public final class Params {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.data.db.Params INSTANCE = null;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String DBNAME = "dct_db";
    public static final int DBVERSION = 1;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String VALIREQUESTTABLENAME = "validate_request";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String DOWNLOADED_TAGS_TABLE_NAME = "downloaded_tags";
    
    private Params() {
        super();
    }
}