package com.fwp.deloittedctcustomerapp.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0010\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010$\u001a\u00020%2\u0006\u0010\u0010\u001a\u00020&J\u000e\u0010\'\u001a\u00020%2\u0006\u0010(\u001a\u00020)J\u000e\u0010*\u001a\u00020%2\u0006\u0010\u0014\u001a\u00020&J\u000e\u0010+\u001a\u00020%2\u0006\u0010\u0016\u001a\u00020&J\u000e\u0010,\u001a\u00020%2\u0006\u0010\u001a\u001a\u00020&J\u000e\u0010-\u001a\u00020%2\u0006\u0010.\u001a\u00020&J\u000e\u0010/\u001a\u00020%2\u0006\u0010\u001c\u001a\u00020&J\u0014\u00100\u001a\u00020%2\f\u00101\u001a\b\u0012\u0004\u0012\u00020\u00050\fJ\u000e\u00102\u001a\u00020%2\u0006\u0010 \u001a\u00020\u000eJ\u000e\u00103\u001a\u00020%2\u0006\u00104\u001a\u00020&R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\f0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00050\u00118F\u00a2\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00050\u00118F\u00a2\u0006\u0006\u001a\u0004\b\u0015\u0010\u0013R\u0017\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00050\u00118F\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0013R\u0017\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00050\u00118F\u00a2\u0006\u0006\u001a\u0004\b\u0019\u0010\u0013R\u0017\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00050\u00118F\u00a2\u0006\u0006\u001a\u0004\b\u001b\u0010\u0013R\u0017\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00050\u00118F\u00a2\u0006\u0006\u001a\u0004\b\u001d\u0010\u0013R\u001d\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\f0\u00118F\u00a2\u0006\u0006\u001a\u0004\b\u001f\u0010\u0013R\u0017\u0010 \u001a\b\u0012\u0004\u0012\u00020\u000e0\u00118F\u00a2\u0006\u0006\u001a\u0004\b!\u0010\u0013R\u0017\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00050\u00118F\u00a2\u0006\u0006\u001a\u0004\b#\u0010\u0013\u00a8\u00065"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/viewmodel/Communicator;", "Landroidx/lifecycle/ViewModel;", "()V", "_email", "Landroidx/lifecycle/MutableLiveData;", "", "_fn", "_ln", "_message", "_mn", "_password", "_sponsorDetails", "", "_userCreds", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/SignupRequest;", "_username", "email", "Landroidx/lifecycle/LiveData;", "getEmail", "()Landroidx/lifecycle/LiveData;", "fn", "getFn", "ln", "getLn", "message", "getMessage", "mn", "getMn", "password", "getPassword", "sponsorDetails", "getSponsorDetails", "userCreds", "getUserCreds", "username", "getUsername", "setEmailCommunicator", "", "", "setEtagModel", "etagDetail", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/EtagDetail;", "setFNCommunicator", "setLNCommunicator", "setMNCommunicator", "setMsgCommunicator", "msg", "setPassCommunicator", "setSponsorList", "list", "setUserCredModel", "setUserNameCommunicator", "user", "app_release"})
public final class Communicator extends androidx.lifecycle.ViewModel {
    private final androidx.lifecycle.MutableLiveData<java.lang.Object> _message = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Object>> _sponsorDetails = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.requests.SignupRequest> _userCreds = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Object> _username = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Object> _email = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Object> _password = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Object> _fn = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Object> _mn = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Object> _ln = null;
    
    public Communicator() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.lang.Object> getMessage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.util.List<java.lang.Object>> getSponsorDetails() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.requests.SignupRequest> getUserCreds() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.lang.Object> getUsername() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.lang.Object> getEmail() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.lang.Object> getPassword() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.lang.Object> getFn() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.lang.Object> getMn() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.lang.Object> getLn() {
        return null;
    }
    
    public final void setMsgCommunicator(@org.jetbrains.annotations.NotNull
    java.lang.String msg) {
    }
    
    public final void setUserNameCommunicator(@org.jetbrains.annotations.NotNull
    java.lang.String user) {
    }
    
    public final void setEmailCommunicator(@org.jetbrains.annotations.NotNull
    java.lang.String email) {
    }
    
    public final void setPassCommunicator(@org.jetbrains.annotations.NotNull
    java.lang.String password) {
    }
    
    public final void setFNCommunicator(@org.jetbrains.annotations.NotNull
    java.lang.String fn) {
    }
    
    public final void setMNCommunicator(@org.jetbrains.annotations.NotNull
    java.lang.String mn) {
    }
    
    public final void setLNCommunicator(@org.jetbrains.annotations.NotNull
    java.lang.String ln) {
    }
    
    public final void setSponsorList(@org.jetbrains.annotations.NotNull
    java.util.List<? extends java.lang.Object> list) {
    }
    
    public final void setUserCredModel(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.SignupRequest userCreds) {
    }
    
    public final void setEtagModel(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.responses.EtagDetail etagDetail) {
    }
}