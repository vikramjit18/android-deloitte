package com.fwp.deloittedctcustomerapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \n2\u00020\u0001:\u0002\n\u000bB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/DownloadTask;", "", "context", "Landroid/content/Context;", "downloadUrl", "", "(Landroid/content/Context;Ljava/lang/String;)V", "downloadFileName", "progressBar", "Landroid/widget/ProgressBar;", "Companion", "DownloadingTask", "app_release"})
public final class DownloadTask {
    private final android.content.Context context = null;
    private java.lang.String downloadUrl = "";
    private java.lang.String downloadFileName = "";
    private android.widget.ProgressBar progressBar;
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.utils.DownloadTask.Companion Companion = null;
    private static final java.lang.String TAG = "Download Task";
    
    public DownloadTask(@org.jetbrains.annotations.NotNull
    android.content.Context context, @org.jetbrains.annotations.NotNull
    java.lang.String downloadUrl) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0082\u0004\u0018\u00002\u001a\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\'\u0010\r\u001a\u0004\u0018\u00010\u00022\u0016\u0010\u000e\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u00020\u000f\"\u0004\u0018\u00010\u0002H\u0014\u00a2\u0006\u0002\u0010\u0010J\u0012\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0002H\u0014R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR\u001c\u0010\n\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\u0007\"\u0004\b\f\u0010\t\u00a8\u0006\u0014"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/DownloadTask$DownloadingTask;", "Landroid/os/AsyncTask;", "Ljava/lang/Void;", "(Lcom/fwp/deloittedctcustomerapp/utils/DownloadTask;)V", "apkStorage", "Ljava/io/File;", "getApkStorage", "()Ljava/io/File;", "setApkStorage", "(Ljava/io/File;)V", "outputFile", "getOutputFile", "setOutputFile", "doInBackground", "p0", "", "([Ljava/lang/Void;)Ljava/lang/Void;", "onPostExecute", "", "result", "app_release"})
    final class DownloadingTask extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        @org.jetbrains.annotations.Nullable
        private java.io.File apkStorage;
        @org.jetbrains.annotations.Nullable
        private java.io.File outputFile;
        
        public DownloadingTask() {
            super();
        }
        
        @org.jetbrains.annotations.Nullable
        public final java.io.File getApkStorage() {
            return null;
        }
        
        public final void setApkStorage(@org.jetbrains.annotations.Nullable
        java.io.File p0) {
        }
        
        @org.jetbrains.annotations.Nullable
        public final java.io.File getOutputFile() {
            return null;
        }
        
        public final void setOutputFile(@org.jetbrains.annotations.Nullable
        java.io.File p0) {
        }
        
        @java.lang.Override
        protected void onPostExecute(@org.jetbrains.annotations.Nullable
        java.lang.Void result) {
        }
        
        @org.jetbrains.annotations.Nullable
        @java.lang.Override
        protected java.lang.Void doInBackground(@org.jetbrains.annotations.NotNull
        java.lang.Void... p0) {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/DownloadTask$Companion;", "", "()V", "TAG", "", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}