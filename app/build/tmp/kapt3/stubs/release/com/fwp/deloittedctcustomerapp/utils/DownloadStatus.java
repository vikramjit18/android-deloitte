package com.fwp.deloittedctcustomerapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/DownloadStatus;", "", "()V", "NO_DOWNLOAD", "", "OTHER_DEVICE", "SAME_DEVICE", "app_release"})
public final class DownloadStatus {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.utils.DownloadStatus INSTANCE = null;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String OTHER_DEVICE = "O";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String SAME_DEVICE = "Y";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String NO_DOWNLOAD = "N";
    
    private DownloadStatus() {
        super();
    }
}