package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0007\u0018\u0000 C2\u00020\u0001:\u0001CB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010&\u001a\u00020\'H\u0002J\b\u0010(\u001a\u00020\'H\u0002J\b\u0010)\u001a\u00020\'H\u0002J\b\u0010*\u001a\u00020\'H\u0002J\b\u0010+\u001a\u00020,H\u0002J\u0012\u0010-\u001a\u00020\u000b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u000bH\u0002J\b\u0010.\u001a\u00020\'H\u0002J$\u0010/\u001a\u0002002\u0006\u00101\u001a\u0002022\b\u00103\u001a\u0004\u0018\u0001042\b\u00105\u001a\u0004\u0018\u000106H\u0016J\b\u00107\u001a\u00020\'H\u0002J\u001a\u00108\u001a\u00020\'2\u0006\u00109\u001a\u0002002\b\u00105\u001a\u0004\u0018\u000106H\u0017J\u0018\u0010:\u001a\u00020\'2\u0006\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u00020>H\u0002J\b\u0010?\u001a\u00020\'H\u0003J\u0010\u0010@\u001a\u00020\'2\u0006\u0010;\u001a\u00020<H\u0002J\b\u0010A\u001a\u00020\u000bH\u0002J\b\u0010B\u001a\u00020\'H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0015R\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001f\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010 \u001a\u00020!8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b\"\u0010#\u00a8\u0006D"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/ReviewHarvestFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/ReviewHarvestFragmentBinding;", "accountOwner", "", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/ReviewHarvestFragmentBinding;", "confNo", "", "description", "eTagYearIndex", "fusedLocationProviderClient", "Lcom/google/android/gms/location/FusedLocationProviderClient;", "gson", "Lcom/google/gson/Gson;", "harvestDate", "latitude", "", "Ljava/lang/Double;", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "locationCallback", "Lcom/google/android/gms/location/LocationCallback;", "longitude", "opportunity", "owner", "position", "speciceCode", "spiId", "validateTagActivityViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/activityViewModel/ValidateTagActivityViewModel;", "getValidateTagActivityViewModel", "()Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/activityViewModel/ValidateTagActivityViewModel;", "validateTagActivityViewModel$delegate", "Lkotlin/Lazy;", "buttonHandler", "", "buttonProperty", "fetchLocation", "init", "isLocationEnabled", "", "lastFourChar", "navigteToSuccessPage", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onGPS", "onViewCreated", "view", "ownerMatching", "it", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagResponse;", "item", "", "requestLocation", "setupUI", "sixRandomNo", "viewModelObser", "Companion", "app_release"})
@dagger.hilt.android.AndroidEntryPoint
public final class ReviewHarvestFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.ReviewHarvestFragment.Companion Companion = null;
    private com.fwp.deloittedctcustomerapp.databinding.ReviewHarvestFragmentBinding _binding;
    private final kotlin.Lazy validateTagActivityViewModel$delegate = null;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private com.google.android.gms.location.FusedLocationProviderClient fusedLocationProviderClient;
    private java.lang.String harvestDate;
    private java.lang.String confNo;
    private java.lang.String spiId;
    private java.lang.String speciceCode;
    private java.lang.Object eTagYearIndex;
    private java.lang.Object accountOwner;
    private java.lang.Object position;
    private java.lang.Object owner;
    private java.lang.Object opportunity;
    private java.lang.Object description;
    private java.lang.Double longitude;
    private java.lang.Double latitude;
    private final com.google.gson.Gson gson = null;
    private final com.google.android.gms.location.LocationCallback locationCallback = null;
    
    public ReviewHarvestFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.ReviewHarvestFragmentBinding getBinding() {
        return null;
    }
    
    private final com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel getValidateTagActivityViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setupUI(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse it) {
    }
    
    private final void ownerMatching(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse it, int item) {
    }
    
    private final java.lang.String lastFourChar(java.lang.String owner) {
        return null;
    }
    
    private final java.lang.String sixRandomNo() {
        return null;
    }
    
    private final void onGPS() {
    }
    
    private final boolean isLocationEnabled() {
        return false;
    }
    
    private final void fetchLocation() {
    }
    
    @android.annotation.SuppressLint(value = {"MissingPermission"})
    private final void requestLocation() {
    }
    
    private final void init() {
    }
    
    private final void navigteToSuccessPage() {
    }
    
    private final void buttonHandler() {
    }
    
    private final void viewModelObser() {
    }
    
    private final void buttonProperty() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/ReviewHarvestFragment$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/ReviewHarvestFragment;", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.ReviewHarvestFragment newInstance() {
            return null;
        }
    }
}