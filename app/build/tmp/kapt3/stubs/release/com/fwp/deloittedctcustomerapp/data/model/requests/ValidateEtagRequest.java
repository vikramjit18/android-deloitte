package com.fwp.deloittedctcustomerapp.data.model.requests;

import java.lang.System;

@androidx.room.Entity(tableName = "validate_request")
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0006\n\u0002\b\u001f\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0087\b\u0018\u00002\u00020\u0001By\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0001\u0010\t\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0001\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\u000b\u0012\n\b\u0001\u0010\r\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0001\u0010\u000e\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u001e\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010!\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u0010\u0010%\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003\u00a2\u0006\u0002\u0010\u0018J\u0010\u0010&\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003\u00a2\u0006\u0002\u0010\u0018J\u000b\u0010\'\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u0082\u0001\u0010(\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u00062\n\b\u0003\u0010\t\u001a\u0004\u0018\u00010\u00062\n\b\u0003\u0010\n\u001a\u0004\u0018\u00010\u000b2\n\b\u0003\u0010\f\u001a\u0004\u0018\u00010\u000b2\n\b\u0003\u0010\r\u001a\u0004\u0018\u00010\u00062\n\b\u0003\u0010\u000e\u001a\u0004\u0018\u00010\u0006H\u00c6\u0001\u00a2\u0006\u0002\u0010)J\u0013\u0010*\u001a\u00020+2\b\u0010,\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010-\u001a\u00020\u0003H\u00d6\u0001J\t\u0010.\u001a\u00020\u0006H\u00d6\u0001R\u0018\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0018\u0010\u0007\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011R\u0018\u0010\b\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0011R\u0018\u0010\t\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0011R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u001a\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006X\u0087\u0004\u00a2\u0006\n\n\u0002\u0010\u0019\u001a\u0004\b\u0017\u0010\u0018R\u001a\u0010\f\u001a\u0004\u0018\u00010\u000b8\u0006X\u0087\u0004\u00a2\u0006\n\n\u0002\u0010\u0019\u001a\u0004\b\u001a\u0010\u0018R\u0018\u0010\r\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0011R\u0018\u0010\u000e\u001a\u0004\u0018\u00010\u00068\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0011R\u0016\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0016\u00a8\u0006/"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/requests/ValidateEtagRequest;", "", "id", "", "sync", "deviceId", "", "etagUploadDate", "harvestConfNum", "harvestedDate", "lat", "", "longt", "speciesCd", "spiId", "(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/String;)V", "getDeviceId", "()Ljava/lang/String;", "getEtagUploadDate", "getHarvestConfNum", "getHarvestedDate", "getId", "()I", "getLat", "()Ljava/lang/Double;", "Ljava/lang/Double;", "getLongt", "getSpeciesCd", "getSpiId", "getSync", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/String;)Lcom/fwp/deloittedctcustomerapp/data/model/requests/ValidateEtagRequest;", "equals", "", "other", "hashCode", "toString", "app_release"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ValidateEtagRequest {
    @androidx.room.ColumnInfo(name = "id")
    @androidx.room.PrimaryKey(autoGenerate = true)
    private final int id = 0;
    @androidx.room.ColumnInfo(name = "sync")
    private final int sync = 0;
    @org.jetbrains.annotations.Nullable
    @androidx.room.ColumnInfo(name = "device_id")
    private final java.lang.String deviceId = null;
    @org.jetbrains.annotations.Nullable
    @androidx.room.ColumnInfo(name = "etag_upload_date")
    private final java.lang.String etagUploadDate = null;
    @org.jetbrains.annotations.Nullable
    @androidx.room.ColumnInfo(name = "harvestConfNum")
    private final java.lang.String harvestConfNum = null;
    @org.jetbrains.annotations.Nullable
    @androidx.room.ColumnInfo(name = "harvested_date")
    private final java.lang.String harvestedDate = null;
    @org.jetbrains.annotations.Nullable
    @androidx.room.ColumnInfo(name = "lat")
    private final java.lang.Double lat = null;
    @org.jetbrains.annotations.Nullable
    @androidx.room.ColumnInfo(name = "longt")
    private final java.lang.Double longt = null;
    @org.jetbrains.annotations.Nullable
    @androidx.room.ColumnInfo(name = "speciesCd")
    private final java.lang.String speciesCd = null;
    @org.jetbrains.annotations.Nullable
    @androidx.room.ColumnInfo(name = "spi_id")
    private final java.lang.String spiId = null;
    
    @org.jetbrains.annotations.NotNull
    public final com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest copy(int id, int sync, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "device_id")
    java.lang.String deviceId, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "etag_upload_date")
    java.lang.String etagUploadDate, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "harvestConfNum")
    java.lang.String harvestConfNum, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "harvested_date")
    java.lang.String harvestedDate, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "lat")
    java.lang.Double lat, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "longt")
    java.lang.Double longt, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "speciesCd")
    java.lang.String speciesCd, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "spi_id")
    java.lang.String spiId) {
        return null;
    }
    
    @java.lang.Override
    public boolean equals(@org.jetbrains.annotations.Nullable
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.lang.String toString() {
        return null;
    }
    
    public ValidateEtagRequest(int id, int sync, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "device_id")
    java.lang.String deviceId, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "etag_upload_date")
    java.lang.String etagUploadDate, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "harvestConfNum")
    java.lang.String harvestConfNum, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "harvested_date")
    java.lang.String harvestedDate, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "lat")
    java.lang.Double lat, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "longt")
    java.lang.Double longt, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "speciesCd")
    java.lang.String speciesCd, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "spi_id")
    java.lang.String spiId) {
        super();
    }
    
    public final int component1() {
        return 0;
    }
    
    public final int getId() {
        return 0;
    }
    
    public final int component2() {
        return 0;
    }
    
    public final int getSync() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getDeviceId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getEtagUploadDate() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getHarvestConfNum() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getHarvestedDate() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Double component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Double getLat() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Double component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Double getLongt() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getSpeciesCd() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component10() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getSpiId() {
        return null;
    }
}