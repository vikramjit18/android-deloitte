package com.fwp.deloittedctcustomerapp.data.model.responses.etag;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b3\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B\u00e9\u0001\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\u0010\b\u0001\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b\u0012\n\b\u0001\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u0012\u0010\b\u0001\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\b\u0012\n\b\u0001\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u0012\u0010\b\u0001\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\b\u0012\u0010\b\u0001\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\b\u0012\n\b\u0001\u0010\u0015\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0016J\u000b\u0010*\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010-\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0011\u0010.\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\bH\u00c6\u0003J\u000b\u0010/\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0011\u00100\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\bH\u00c6\u0003J\u0011\u00101\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\bH\u00c6\u0003J\u000b\u00102\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u00103\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u00104\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u00105\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0011\u00106\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bH\u00c6\u0003J\u000b\u00107\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u00108\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u00109\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010:\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u00ed\u0001\u0010;\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\u0010\b\u0003\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b2\n\b\u0003\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\f\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\r\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u000e\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0010\u001a\u0004\u0018\u00010\u00032\u0010\b\u0003\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\b2\n\b\u0003\u0010\u0012\u001a\u0004\u0018\u00010\u00032\u0010\b\u0003\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\b2\u0010\b\u0003\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\b2\n\b\u0003\u0010\u0015\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010<\u001a\u00020=2\b\u0010>\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010?\u001a\u00020@H\u00d6\u0001J\t\u0010A\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0018R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0018R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0018R\u0019\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0018R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0018R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u0018R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0018R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0018R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\u0018R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0018R\u0019\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\b\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u001dR\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0018R\u0019\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010\u001dR\u0019\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\b\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001dR\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010\u0018\u00a8\u0006B"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagsubDetails;", "", "alsNumber", "", "backgroundImageUrl", "description", "downloads", "eTagMandatoryDetails", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/ETagMandatoryDetail;", "etagConfirmationNumber", "harvestTimeStamp", "imageUrlIcon", "issuedDate", "licenseName", "oppurtunity", "owner", "regulationsText", "regulationsUrl", "sponsorDetails", "status", "subtitle", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V", "getAlsNumber", "()Ljava/lang/String;", "getBackgroundImageUrl", "getDescription", "getDownloads", "getETagMandatoryDetails", "()Ljava/util/List;", "getEtagConfirmationNumber", "getHarvestTimeStamp", "getImageUrlIcon", "getIssuedDate", "getLicenseName", "getOppurtunity", "getOwner", "getRegulationsText", "getRegulationsUrl", "getSponsorDetails", "getStatus", "getSubtitle", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class EtagsubDetails {
    @org.jetbrains.annotations.Nullable
    private final java.lang.String alsNumber = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String backgroundImageUrl = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String description = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String downloads = null;
    @org.jetbrains.annotations.Nullable
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.ETagMandatoryDetail> eTagMandatoryDetails = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String etagConfirmationNumber = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String harvestTimeStamp = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String imageUrlIcon = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String issuedDate = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String licenseName = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String oppurtunity = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String owner = null;
    @org.jetbrains.annotations.Nullable
    private final java.util.List<java.lang.String> regulationsText = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String regulationsUrl = null;
    @org.jetbrains.annotations.Nullable
    private final java.util.List<java.lang.Object> sponsorDetails = null;
    @org.jetbrains.annotations.Nullable
    private final java.util.List<java.lang.String> status = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String subtitle = null;
    
    @org.jetbrains.annotations.NotNull
    public final com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagsubDetails copy(@org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "alsNumber")
    java.lang.String alsNumber, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "backgroundImageUrl")
    java.lang.String backgroundImageUrl, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "description")
    java.lang.String description, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "downloads")
    java.lang.String downloads, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "eTagMandatoryDetails")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.ETagMandatoryDetail> eTagMandatoryDetails, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "etagConfirmationNumber")
    java.lang.String etagConfirmationNumber, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "harvestTimeStamp")
    java.lang.String harvestTimeStamp, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "imageUrlIcon")
    java.lang.String imageUrlIcon, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "issuedDate")
    java.lang.String issuedDate, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "licenseName")
    java.lang.String licenseName, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "oppurtunity")
    java.lang.String oppurtunity, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "owner")
    java.lang.String owner, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "regulationsText")
    java.util.List<java.lang.String> regulationsText, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "regulationsUrl")
    java.lang.String regulationsUrl, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "sponsorDetails")
    java.util.List<? extends java.lang.Object> sponsorDetails, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "status")
    java.util.List<java.lang.String> status, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "subtitle")
    java.lang.String subtitle) {
        return null;
    }
    
    @java.lang.Override
    public boolean equals(@org.jetbrains.annotations.Nullable
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.lang.String toString() {
        return null;
    }
    
    public EtagsubDetails(@org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "alsNumber")
    java.lang.String alsNumber, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "backgroundImageUrl")
    java.lang.String backgroundImageUrl, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "description")
    java.lang.String description, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "downloads")
    java.lang.String downloads, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "eTagMandatoryDetails")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.ETagMandatoryDetail> eTagMandatoryDetails, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "etagConfirmationNumber")
    java.lang.String etagConfirmationNumber, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "harvestTimeStamp")
    java.lang.String harvestTimeStamp, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "imageUrlIcon")
    java.lang.String imageUrlIcon, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "issuedDate")
    java.lang.String issuedDate, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "licenseName")
    java.lang.String licenseName, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "oppurtunity")
    java.lang.String oppurtunity, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "owner")
    java.lang.String owner, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "regulationsText")
    java.util.List<java.lang.String> regulationsText, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "regulationsUrl")
    java.lang.String regulationsUrl, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "sponsorDetails")
    java.util.List<? extends java.lang.Object> sponsorDetails, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "status")
    java.util.List<java.lang.String> status, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "subtitle")
    java.lang.String subtitle) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getAlsNumber() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getBackgroundImageUrl() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getDescription() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getDownloads() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.ETagMandatoryDetail> component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.ETagMandatoryDetail> getETagMandatoryDetails() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getEtagConfirmationNumber() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getHarvestTimeStamp() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getImageUrlIcon() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getIssuedDate() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component10() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getLicenseName() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component11() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getOppurtunity() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component12() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getOwner() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<java.lang.String> component13() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<java.lang.String> getRegulationsText() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component14() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getRegulationsUrl() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<java.lang.Object> component15() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<java.lang.Object> getSponsorDetails() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<java.lang.String> component16() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<java.lang.String> getStatus() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component17() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getSubtitle() {
        return null;
    }
}