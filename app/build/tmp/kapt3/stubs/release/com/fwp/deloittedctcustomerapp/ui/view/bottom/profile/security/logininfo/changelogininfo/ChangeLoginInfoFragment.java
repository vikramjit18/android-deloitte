package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo;

import java.lang.System;

/**
 * FWP
 * Hilt entry point
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0007\u0018\u0000 32\u00020\u0001:\u00013B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0016H\u0002J\b\u0010\u0018\u001a\u00020\u0016H\u0002J\b\u0010\u0019\u001a\u00020\u0016H\u0002J\b\u0010\u001a\u001a\u00020\u0016H\u0002J\b\u0010\u001b\u001a\u00020\u0016H\u0002J\u0010\u0010\u001c\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\b\u0010\u001f\u001a\u00020\u0016H\u0002J\"\u0010 \u001a\u0004\u0018\u00010!2\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020#H\u0016J$\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\b\u0010+\u001a\u0004\u0018\u00010,2\b\u0010-\u001a\u0004\u0018\u00010.H\u0016J\u001a\u0010/\u001a\u00020\u00162\u0006\u00100\u001a\u00020(2\b\u0010-\u001a\u0004\u0018\u00010.H\u0016J\b\u00101\u001a\u00020\u0016H\u0002J\b\u00102\u001a\u00020\u0016H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u001b\u0010\b\u001a\u00020\t8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00064"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/security/logininfo/changelogininfo/ChangeLoginInfoFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/ChangeLoginInfoFragmentBinding;", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/ChangeLoginInfoFragmentBinding;", "changeLoginInfoViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/security/logininfo/changelogininfo/ChangeLoginInfoViewModel;", "getChangeLoginInfoViewModel", "()Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/security/logininfo/changelogininfo/ChangeLoginInfoViewModel;", "changeLoginInfoViewModel$delegate", "Lkotlin/Lazy;", "listOffLoad", "", "", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "mTextWatcher", "Landroid/text/TextWatcher;", "buttonActivation", "", "buttonHandler", "checkLiveNetworkConnection", "disablePrimaryBtn", "enablePrimaryBtn", "init", "offLoadApi", "offloadRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/DownloadOffloadRequest;", "offloadObsr", "onCreateAnimation", "Landroid/view/animation/Animation;", "transit", "", "enter", "", "nextAnim", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "updateEmailBtnHandler", "viewModelObser", "Companion", "app_release"})
@dagger.hilt.android.AndroidEntryPoint
public final class ChangeLoginInfoFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo.ChangeLoginInfoFragment.Companion Companion = null;
    private com.fwp.deloittedctcustomerapp.databinding.ChangeLoginInfoFragmentBinding _binding;
    private final kotlin.Lazy changeLoginInfoViewModel$delegate = null;
    private final java.util.List<java.lang.String> listOffLoad = null;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    
    /**
     * create a textWatcher member
     */
    private final android.text.TextWatcher mTextWatcher = null;
    
    public ChangeLoginInfoFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.ChangeLoginInfoFragmentBinding getBinding() {
        return null;
    }
    
    private final com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo.ChangeLoginInfoViewModel getChangeLoginInfoViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    @java.lang.Override
    public android.view.animation.Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void checkLiveNetworkConnection() {
    }
    
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void viewModelObser() {
    }
    
    private final void offLoadApi(com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest offloadRequest) {
    }
    
    private final void init() {
    }
    
    /**
     * All click
     * listener function
     */
    private final void buttonHandler() {
    }
    
    /**
     * function for
     * activate primary button
     */
    private final void buttonActivation() {
    }
    
    private final void updateEmailBtnHandler() {
    }
    
    /**
     * function for the
     * disable primary btn
     */
    private final void disablePrimaryBtn() {
    }
    
    /**
     * function for the
     * enabling primary btn
     */
    private final void enablePrimaryBtn() {
    }
    
    private final void offloadObsr() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/security/logininfo/changelogininfo/ChangeLoginInfoFragment$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/security/logininfo/changelogininfo/ChangeLoginInfoFragment;", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo.ChangeLoginInfoFragment newInstance() {
            return null;
        }
    }
}