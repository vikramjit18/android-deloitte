package com.fwp.deloittedctcustomerapp.data.hilt;

import java.lang.System;

@dagger.hilt.InstallIn(value = {dagger.hilt.components.SingletonComponent.class})
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007J\u0012\u0010\u000b\u001a\u00020\n2\b\b\u0001\u0010\f\u001a\u00020\rH\u0007J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0007J\b\u0010\u0012\u001a\u00020\u0011H\u0007J\b\u0010\u0013\u001a\u00020\u0014H\u0007J\b\u0010\u0015\u001a\u00020\u0016H\u0007J\u0018\u0010\u0017\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u0016H\u0007\u00a8\u0006\u0019"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/hilt/NetworkModule;", "", "()V", "provideApiServices", "Lcom/fwp/deloittedctcustomerapp/data/api/ApiHelper;", "retrofit", "Lretrofit2/Retrofit;", "provideAppDao", "Lcom/fwp/deloittedctcustomerapp/data/db/AppDao;", "appDatabase", "Lcom/fwp/deloittedctcustomerapp/data/db/AppDatabase;", "provideAppDatabase", "context", "Landroid/content/Context;", "provideCallFactory", "Lokhttp3/Call$Factory;", "httpLoggingInterceptor", "Lokhttp3/logging/HttpLoggingInterceptor;", "provideHttpLoginInterceptor", "provideMoshi", "Lcom/squareup/moshi/Moshi;", "provideMoshiConvertorFactory", "Lretrofit2/converter/moshi/MoshiConverterFactory;", "provideRetrofitApi", "moshiConverterFactory", "app_release"})
@dagger.Module
public final class NetworkModule {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.data.hilt.NetworkModule INSTANCE = null;
    
    private NetworkModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    @javax.inject.Singleton
    @dagger.Provides
    public final com.fwp.deloittedctcustomerapp.data.db.AppDatabase provideAppDatabase(@org.jetbrains.annotations.NotNull
    @dagger.hilt.android.qualifiers.ApplicationContext
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @javax.inject.Singleton
    @dagger.Provides
    public final com.fwp.deloittedctcustomerapp.data.db.AppDao provideAppDao(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.db.AppDatabase appDatabase) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @javax.inject.Singleton
    @dagger.Provides
    public final okhttp3.logging.HttpLoggingInterceptor provideHttpLoginInterceptor() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @javax.inject.Singleton
    @dagger.Provides
    public final okhttp3.Call.Factory provideCallFactory(@org.jetbrains.annotations.NotNull
    okhttp3.logging.HttpLoggingInterceptor httpLoggingInterceptor) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @javax.inject.Singleton
    @dagger.Provides
    public final com.squareup.moshi.Moshi provideMoshi() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @javax.inject.Singleton
    @dagger.Provides
    public final retrofit2.converter.moshi.MoshiConverterFactory provideMoshiConvertorFactory() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @javax.inject.Singleton
    @dagger.Provides
    public final retrofit2.Retrofit provideRetrofitApi(@org.jetbrains.annotations.NotNull
    okhttp3.Call.Factory httpLoggingInterceptor, @org.jetbrains.annotations.NotNull
    retrofit2.converter.moshi.MoshiConverterFactory moshiConverterFactory) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @javax.inject.Singleton
    @dagger.Provides
    public final com.fwp.deloittedctcustomerapp.data.api.ApiHelper provideApiServices(@org.jetbrains.annotations.NotNull
    retrofit2.Retrofit retrofit) {
        return null;
    }
}