package com.fwp.deloittedctcustomerapp.ui.view.bottom.common;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000e\u001a\u00020\u000fH\u0002J\b\u0010\u0010\u001a\u00020\u000fH\u0002J$\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u000bH\u0016J\b\u0010\u0018\u001a\u00020\u000fH\u0016R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/common/AlsNotLinked;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/AlsNotLinkedFragmentBinding;", "alsLinkedState", "", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/AlsNotLinkedFragmentBinding;", "extras", "Landroid/os/Bundle;", "viewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/common/AlsNotLinkedViewModel;", "buttonHandler", "", "init", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "onDestroyView", "Companion", "app_release"})
@dagger.hilt.android.AndroidEntryPoint
public final class AlsNotLinked extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.common.AlsNotLinked.Companion Companion = null;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.common.AlsNotLinkedViewModel viewModel;
    private com.fwp.deloittedctcustomerapp.databinding.AlsNotLinkedFragmentBinding _binding;
    private android.os.Bundle extras;
    private java.lang.String alsLinkedState = "";
    
    public AlsNotLinked() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.AlsNotLinkedFragmentBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void init() {
    }
    
    private final void buttonHandler() {
    }
    
    @java.lang.Override
    public void onDestroyView() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/common/AlsNotLinked$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/common/AlsNotLinked;", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.common.AlsNotLinked newInstance() {
            return null;
        }
    }
}