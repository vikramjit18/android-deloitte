package com.fwp.deloittedctcustomerapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\t\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tJ\u0016\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\rJ\u0016\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u0006J\u001e\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\u000bJ\u0006\u0010\u0014\u001a\u00020\u000bJ\u0006\u0010\u0015\u001a\u00020\u000bJ\u0006\u0010\u0016\u001a\u00020\u0017J\u0006\u0010\u0018\u001a\u00020\u0017J\u0016\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dJ\u000e\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\u001c\u001a\u00020\u001dJ\u000e\u0010 \u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u001dJ\u001e\u0010\"\u001a\u00020\u00042\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u00062\u0006\u0010&\u001a\u00020\u0006J\u0016\u0010\'\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010(\u001a\u00020\u0017J\u001e\u0010)\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010*\u001a\u00020\u00172\u0006\u0010+\u001a\u00020,J\u000e\u0010-\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J&\u0010.\u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u001d2\u0006\u0010/\u001a\u00020\u000b2\u0006\u00100\u001a\u00020\u000b2\u0006\u00101\u001a\u00020\u000bJ6\u00102\u001a\u00020\u00042\u0006\u00103\u001a\u00020\u00062\u0006\u0010!\u001a\u00020\u001d2\u0006\u0010/\u001a\u00020\u000b2\u0006\u00100\u001a\u00020\u000b2\u0006\u00104\u001a\u00020\u000b2\u0006\u00105\u001a\u00020\u000bJ6\u00106\u001a\u00020\u00042\u0006\u00103\u001a\u00020\u00062\u0006\u0010!\u001a\u00020\u001d2\u0006\u0010/\u001a\u00020\u000b2\u0006\u00100\u001a\u00020\u000b2\u0006\u00101\u001a\u00020\u000b2\u0006\u00107\u001a\u00020\u000bJ>\u00108\u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u001d2\u0006\u0010/\u001a\u00020\u000b2\u0006\u00100\u001a\u00020\u000b2\u0006\u00109\u001a\u00020\u000b2\u0006\u0010:\u001a\u00020\u000b2\u0006\u0010;\u001a\u00020\u001b2\u0006\u0010<\u001a\u00020\u0017J\u000e\u0010=\u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u001dJ\u000e\u0010>\u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u001dJ\u000e\u0010?\u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u001dJ\u0016\u0010@\u001a\u00020A2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0005\u001a\u00020\u0006J&\u0010B\u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u001d2\u0006\u0010/\u001a\u00020\u000b2\u0006\u00100\u001a\u00020\u000b2\u0006\u00101\u001a\u00020\u000bJ&\u0010C\u001a\u00020\u00042\u0006\u0010D\u001a\u00020\u001b2\u0006\u0010E\u001a\u00020\u000b2\u0006\u0010F\u001a\u00020\u00172\u0006\u0010G\u001a\u00020\u0017J-\u0010H\u001a\u00020I2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010J\u001a\u00020\u000b2\b\u0010K\u001a\u0004\u0018\u00010\u00172\u0006\u0010L\u001a\u00020\u001f\u00a2\u0006\u0002\u0010MJ\u0010\u0010N\u001a\u0004\u0018\u00010\r2\u0006\u0010O\u001a\u00020\u000bJ\u0016\u0010P\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010E\u001a\u00020\u000bJ\u0016\u0010Q\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006R"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/UtilsMethods;", "", "()V", "activityBack", "", "activity", "Landroid/app/Activity;", "backStack", "navController", "Landroidx/navigation/NavController;", "dateDifference", "", "startDate", "Ljava/util/Date;", "endDate", "dialerOpener", "phoneNo", "downloadFile", "url", "setTitle", "getCurrentDateMMMddYY", "getCurrentDateTime", "getCurrentYear", "", "getPreviousYear", "hideKeyBoard", "view", "Landroid/view/View;", "context", "Landroid/content/Context;", "isInternetConnected", "", "loader", "mContext", "navigateFragmentToActivity", "fragmentActivity", "Landroidx/fragment/app/FragmentActivity;", "activityRequired", "toActivity", "navigateToFragment", "navIdInt", "navigateToFragmentWithValues", "destinationId", "parameters", "Landroid/os/Bundle;", "openEmailApp", "showAccountExpireAlert", "tvPrimary", "tvSecondary", "primaryBtn", "showDialerAlert", "mActivity", "rBtn", "lBtn", "showDialerAlsAlert", "secBtn", "showFragmentNavigationAlert", "leftBtn", "rightBtn", "mView", "viewId", "showNoNetwokDialog", "showNoNetwokDownloadDialog", "showNoNetwokLogoutDialog", "showSearchAlsDialog", "Landroidx/appcompat/app/AlertDialog;", "showSimpleAlert", "snackBarCustom", "contextView", "message", "textColor", "bgColor", "stringSpannable", "Landroid/text/SpannableStringBuilder;", "string", "drawable", "isUnderLine", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Integer;Z)Landroid/text/SpannableStringBuilder;", "stringToDateConversion", "s", "toastMaker", "websiteOpener", "app_release"})
public final class UtilsMethods {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.utils.UtilsMethods INSTANCE = null;
    
    private UtilsMethods() {
        super();
    }
    
    public final void activityBack(@org.jetbrains.annotations.NotNull
    android.app.Activity activity) {
    }
    
    public final void snackBarCustom(@org.jetbrains.annotations.NotNull
    android.view.View contextView, @org.jetbrains.annotations.NotNull
    java.lang.String message, int textColor, int bgColor) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final android.text.SpannableStringBuilder stringSpannable(@org.jetbrains.annotations.NotNull
    android.content.Context context, @org.jetbrains.annotations.NotNull
    java.lang.String string, @org.jetbrains.annotations.Nullable
    java.lang.Integer drawable, boolean isUnderLine) {
        return null;
    }
    
    public final void backStack(@org.jetbrains.annotations.NotNull
    androidx.navigation.NavController navController) {
    }
    
    public final void navigateToFragment(@org.jetbrains.annotations.NotNull
    android.view.View view, int navIdInt) {
    }
    
    public final void navigateToFragmentWithValues(@org.jetbrains.annotations.NotNull
    android.view.View view, int destinationId, @org.jetbrains.annotations.NotNull
    android.os.Bundle parameters) {
    }
    
    public final void websiteOpener(@org.jetbrains.annotations.NotNull
    java.lang.String url, @org.jetbrains.annotations.NotNull
    android.app.Activity activity) {
    }
    
    public final void dialerOpener(@org.jetbrains.annotations.NotNull
    java.lang.String phoneNo, @org.jetbrains.annotations.NotNull
    android.app.Activity activity) {
    }
    
    public final void navigateFragmentToActivity(@org.jetbrains.annotations.NotNull
    androidx.fragment.app.FragmentActivity fragmentActivity, @org.jetbrains.annotations.NotNull
    android.app.Activity activityRequired, @org.jetbrains.annotations.NotNull
    android.app.Activity toActivity) {
    }
    
    public final void openEmailApp(@org.jetbrains.annotations.NotNull
    android.app.Activity activity) {
    }
    
    public final void downloadFile(@org.jetbrains.annotations.NotNull
    android.app.Activity activity, @org.jetbrains.annotations.NotNull
    java.lang.String url, @org.jetbrains.annotations.NotNull
    java.lang.String setTitle) {
    }
    
    public final void toastMaker(@org.jetbrains.annotations.NotNull
    android.content.Context context, @org.jetbrains.annotations.NotNull
    java.lang.String message) {
    }
    
    public final void loader(@org.jetbrains.annotations.NotNull
    android.content.Context mContext) {
    }
    
    public final void showSimpleAlert(@org.jetbrains.annotations.NotNull
    android.content.Context mContext, @org.jetbrains.annotations.NotNull
    java.lang.String tvPrimary, @org.jetbrains.annotations.NotNull
    java.lang.String tvSecondary, @org.jetbrains.annotations.NotNull
    java.lang.String primaryBtn) {
    }
    
    public final void showAccountExpireAlert(@org.jetbrains.annotations.NotNull
    android.content.Context mContext, @org.jetbrains.annotations.NotNull
    java.lang.String tvPrimary, @org.jetbrains.annotations.NotNull
    java.lang.String tvSecondary, @org.jetbrains.annotations.NotNull
    java.lang.String primaryBtn) {
    }
    
    public final void showNoNetwokDownloadDialog(@org.jetbrains.annotations.NotNull
    android.content.Context mContext) {
    }
    
    public final void showNoNetwokDialog(@org.jetbrains.annotations.NotNull
    android.content.Context mContext) {
    }
    
    public final void showNoNetwokLogoutDialog(@org.jetbrains.annotations.NotNull
    android.content.Context mContext) {
    }
    
    public final void showDialerAlert(@org.jetbrains.annotations.NotNull
    android.app.Activity mActivity, @org.jetbrains.annotations.NotNull
    android.content.Context mContext, @org.jetbrains.annotations.NotNull
    java.lang.String tvPrimary, @org.jetbrains.annotations.NotNull
    java.lang.String tvSecondary, @org.jetbrains.annotations.NotNull
    java.lang.String rBtn, @org.jetbrains.annotations.NotNull
    java.lang.String lBtn) {
    }
    
    public final void showDialerAlsAlert(@org.jetbrains.annotations.NotNull
    android.app.Activity mActivity, @org.jetbrains.annotations.NotNull
    android.content.Context mContext, @org.jetbrains.annotations.NotNull
    java.lang.String tvPrimary, @org.jetbrains.annotations.NotNull
    java.lang.String tvSecondary, @org.jetbrains.annotations.NotNull
    java.lang.String primaryBtn, @org.jetbrains.annotations.NotNull
    java.lang.String secBtn) {
    }
    
    public final void showFragmentNavigationAlert(@org.jetbrains.annotations.NotNull
    android.content.Context mContext, @org.jetbrains.annotations.NotNull
    java.lang.String tvPrimary, @org.jetbrains.annotations.NotNull
    java.lang.String tvSecondary, @org.jetbrains.annotations.NotNull
    java.lang.String leftBtn, @org.jetbrains.annotations.NotNull
    java.lang.String rightBtn, @org.jetbrains.annotations.NotNull
    android.view.View mView, int viewId) {
    }
    
    public final void hideKeyBoard(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.NotNull
    android.content.Context context) {
    }
    
    public final boolean isInternetConnected(@org.jetbrains.annotations.NotNull
    android.content.Context context) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.appcompat.app.AlertDialog showSearchAlsDialog(@org.jetbrains.annotations.NotNull
    android.content.Context context, @org.jetbrains.annotations.NotNull
    android.app.Activity activity) {
        return null;
    }
    
    public final int getPreviousYear() {
        return 0;
    }
    
    public final int getCurrentYear() {
        return 0;
    }
    
    /**
     * Monatna time standerd GMT-7
     * daylight saving GMT-6
     */
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getCurrentDateTime() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String dateDifference(@org.jetbrains.annotations.NotNull
    java.util.Date startDate, @org.jetbrains.annotations.NotNull
    java.util.Date endDate) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getCurrentDateMMMddYY() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.Date stringToDateConversion(@org.jetbrains.annotations.NotNull
    java.lang.String s) {
        return null;
    }
}