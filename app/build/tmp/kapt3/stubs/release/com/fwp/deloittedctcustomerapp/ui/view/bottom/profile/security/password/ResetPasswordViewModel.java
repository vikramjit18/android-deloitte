package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u001b\u001a\u00020\u001c2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\t0\u001eH\u0002J\u0016\u0010\u001f\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020\u000e2\u0006\u0010!\u001a\u00020\"J\u0006\u0010#\u001a\u00020\u001cJ\u0006\u0010$\u001a\u00020\u001cJ\u0012\u0010%\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\'0&0\u0010J\u0010\u0010(\u001a\u00020\u001c2\u0006\u0010)\u001a\u00020*H\u0007R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\t0\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u0018\u0010\u0012R\u001d\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\u00108F\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u0012\u00a8\u0006+"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/security/password/ResetPasswordViewModel;", "Landroidx/lifecycle/ViewModel;", "mainRepo", "Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;", "appDao", "Lcom/fwp/deloittedctcustomerapp/data/db/AppDao;", "(Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;Lcom/fwp/deloittedctcustomerapp/data/db/AppDao;)V", "_changePasswordResponse", "Landroidx/lifecycle/MutableLiveData;", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/changePasswordResponse/ChangePasswordResponse;", "_offloadResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/downloadOffload/DownloadOffloadResponse;", "_toast", "Lcom/fwp/deloittedctcustomerapp/utils/Event;", "", "changePasswordResponse", "Landroidx/lifecycle/LiveData;", "getChangePasswordResponse", "()Landroidx/lifecycle/LiveData;", "coroutineExceptionHandler", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "getCoroutineExceptionHandler", "()Lkotlinx/coroutines/CoroutineExceptionHandler;", "offloadResponse", "getOffloadResponse", "toast", "getToast", "changePassRes", "", "response", "Lretrofit2/Response;", "changePassword", "jwtToken", "changePasswordRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ChangePasswordRequest;", "cleardb", "deleteDownloadedTagSid", "getDownloadedTagSid", "", "Lcom/fwp/deloittedctcustomerapp/data/model/DownloadedTags;", "offLoadRequest", "downloadOffloadRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/DownloadOffloadRequest;", "app_release"})
public final class ResetPasswordViewModel extends androidx.lifecycle.ViewModel {
    private final com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo = null;
    private final com.fwp.deloittedctcustomerapp.data.db.AppDao appDao = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ChangePasswordResponse> _changePasswordResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toast = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse> _offloadResponse = null;
    @org.jetbrains.annotations.NotNull
    private final kotlinx.coroutines.CoroutineExceptionHandler coroutineExceptionHandler = null;
    
    @javax.inject.Inject
    public ResetPasswordViewModel(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo, @org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.db.AppDao appDao) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ChangePasswordResponse> getChangePasswordResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToast() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse> getOffloadResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final kotlinx.coroutines.CoroutineExceptionHandler getCoroutineExceptionHandler() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.util.List<com.fwp.deloittedctcustomerapp.data.model.DownloadedTags>> getDownloadedTagSid() {
        return null;
    }
    
    public final void deleteDownloadedTagSid() {
    }
    
    public final void cleardb() {
    }
    
    public final void changePassword(@org.jetbrains.annotations.NotNull
    java.lang.String jwtToken, @org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.ChangePasswordRequest changePasswordRequest) {
    }
    
    private final void changePassRes(retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ChangePasswordResponse> response) {
    }
    
    @kotlinx.coroutines.DelicateCoroutinesApi
    public final void offLoadRequest(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest downloadOffloadRequest) {
    }
}