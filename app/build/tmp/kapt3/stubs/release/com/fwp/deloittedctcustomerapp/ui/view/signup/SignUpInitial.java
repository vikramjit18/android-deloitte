package com.fwp.deloittedctcustomerapp.ui.view.signup;

import java.lang.System;

/**
 * FWP
 * Hilt entry point
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0017\u001a\u00020\u0018H\u0002J\b\u0010\u0019\u001a\u00020\u0018H\u0002J\b\u0010\u001a\u001a\u00020\u0018H\u0002J\b\u0010\u001b\u001a\u00020\nH\u0002J$\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\b\u0010 \u001a\u0004\u0018\u00010!2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\b\u0010$\u001a\u00020\u0018H\u0016J\u001a\u0010%\u001a\u00020\u00182\u0006\u0010&\u001a\u00020\u001d2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\b\u0010\'\u001a\u00020\u0018H\u0002J \u0010(\u001a\u00020\u00182\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020,H\u0002J0\u0010.\u001a\u00020\u00182\u0006\u0010/\u001a\u0002002\u0006\u00101\u001a\u0002022\u0006\u00103\u001a\u0002022\u0006\u00104\u001a\u0002022\u0006\u00105\u001a\u000202H\u0002J\b\u00106\u001a\u00020\u0018H\u0002J\b\u00107\u001a\u00020\u0018H\u0002J\b\u00108\u001a\u00020\u0018H\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000f\u001a\u00020\u00108BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00069"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/signup/SignUpInitial;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/SignUpInitialsFragmentBinding;", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/SignUpInitialsFragmentBinding;", "setBinding", "(Lcom/fwp/deloittedctcustomerapp/databinding/SignUpInitialsFragmentBinding;)V", "firstTime", "", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "mTextWatcher", "Landroid/text/TextWatcher;", "signupViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/signup/SignUpViewModel;", "getSignupViewModel", "()Lcom/fwp/deloittedctcustomerapp/ui/view/signup/SignUpViewModel;", "signupViewModel$delegate", "Lkotlin/Lazy;", "viewModelCommunicator", "Lcom/fwp/deloittedctcustomerapp/ui/viewmodel/Communicator;", "apiHitUserCheck", "", "buttonActivation", "buttonHandler", "isUserNameValidate", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onViewCreated", "view", "passValidator", "passwordValidator", "password", "Lcom/google/android/material/textfield/TextInputEditText;", "validateImg", "", "unValidateImg", "showActivityNavigationAlert", "mContext", "Landroid/content/Context;", "tvPrimary", "", "tvSecondary", "leftBtn", "rightBtn", "userNameValidator", "validator", "viewModelObserver", "app_release"})
@dagger.hilt.android.AndroidEntryPoint
public final class SignUpInitial extends androidx.fragment.app.Fragment {
    
    /**
     * Global variables
     */
    public com.fwp.deloittedctcustomerapp.databinding.SignUpInitialsFragmentBinding binding;
    private final kotlin.Lazy signupViewModel$delegate = null;
    private com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator viewModelCommunicator;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private boolean firstTime = true;
    
    /**
     * create a
     * textWatcher member
     */
    private final android.text.TextWatcher mTextWatcher = null;
    
    public SignUpInitial() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final com.fwp.deloittedctcustomerapp.databinding.SignUpInitialsFragmentBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.databinding.SignUpInitialsFragmentBinding p0) {
    }
    
    private final com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpViewModel getSignupViewModel() {
        return null;
    }
    
    @java.lang.Override
    public void onResume() {
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    /**
     * function for
     * validation username and password
     */
    private final void validator() {
    }
    
    /**
     * Function for
     * validation username
     */
    private final boolean isUserNameValidate() {
        return false;
    }
    
    /**
     * Initiate
     * button activation function
     */
    private final void buttonActivation() {
    }
    
    private final void apiHitUserCheck() {
    }
    
    private final void viewModelObserver() {
    }
    
    private final void showActivityNavigationAlert(android.content.Context mContext, java.lang.String tvPrimary, java.lang.String tvSecondary, java.lang.String leftBtn, java.lang.String rightBtn) {
    }
    
    /**
     * Button handler function
     */
    private final void buttonHandler() {
    }
    
    /**
     * function for
     * validating password
     */
    private final void passValidator() {
    }
    
    private final void passwordValidator(com.google.android.material.textfield.TextInputEditText password, int validateImg, int unValidateImg) {
    }
    
    /**
     * function for
     * validating username
     */
    private final void userNameValidator() {
    }
}