package com.fwp.deloittedctcustomerapp.data.model.responses.etag;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001Be\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\fJ\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003Ji\u0010\u001e\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\t\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u000b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\"\u001a\u00020#H\u00d6\u0001J\t\u0010$\u001a\u00020\u0003H\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000eR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000eR\u0013\u0010\t\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u000eR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000eR\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u000e\u00a8\u0006%"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagLicenseDetails;", "", "districtCode", "", "districtSpeciesCode", "downloadStatus", "etagsubDetails", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagsubDetails;", "imageUrl", "isEtagActive", "spiId", "validatedStatus", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagsubDetails;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getDistrictCode", "()Ljava/lang/String;", "getDistrictSpeciesCode", "getDownloadStatus", "getEtagsubDetails", "()Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagsubDetails;", "getImageUrl", "getSpiId", "getValidatedStatus", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class EtagLicenseDetails {
    @org.jetbrains.annotations.Nullable
    private final java.lang.String districtCode = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String districtSpeciesCode = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String downloadStatus = null;
    @org.jetbrains.annotations.Nullable
    private final com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagsubDetails etagsubDetails = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String imageUrl = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String isEtagActive = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String spiId = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String validatedStatus = null;
    
    @org.jetbrains.annotations.NotNull
    public final com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenseDetails copy(@org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "districtCode")
    java.lang.String districtCode, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "districtSpeciesCode")
    java.lang.String districtSpeciesCode, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "downloadStatus")
    java.lang.String downloadStatus, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "etagsubDetails")
    com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagsubDetails etagsubDetails, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "imageUrl")
    java.lang.String imageUrl, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "isEtagActive")
    java.lang.String isEtagActive, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "spiId")
    java.lang.String spiId, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "validatedStatus")
    java.lang.String validatedStatus) {
        return null;
    }
    
    @java.lang.Override
    public boolean equals(@org.jetbrains.annotations.Nullable
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.lang.String toString() {
        return null;
    }
    
    public EtagLicenseDetails(@org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "districtCode")
    java.lang.String districtCode, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "districtSpeciesCode")
    java.lang.String districtSpeciesCode, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "downloadStatus")
    java.lang.String downloadStatus, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "etagsubDetails")
    com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagsubDetails etagsubDetails, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "imageUrl")
    java.lang.String imageUrl, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "isEtagActive")
    java.lang.String isEtagActive, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "spiId")
    java.lang.String spiId, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "validatedStatus")
    java.lang.String validatedStatus) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getDistrictCode() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getDistrictSpeciesCode() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getDownloadStatus() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagsubDetails component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagsubDetails getEtagsubDetails() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getImageUrl() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String isEtagActive() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getSpiId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getValidatedStatus() {
        return null;
    }
}