package com.fwp.deloittedctcustomerapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\t\u001a\u00020\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/DateInputMethod;", "", "input", "Landroid/widget/EditText;", "(Landroid/widget/EditText;)V", "getInput", "()Landroid/widget/EditText;", "mDateEntryWatcher", "Landroid/text/TextWatcher;", "listen", "", "app_release"})
public final class DateInputMethod {
    @org.jetbrains.annotations.NotNull
    private final android.widget.EditText input = null;
    private final android.text.TextWatcher mDateEntryWatcher = null;
    
    public DateInputMethod(@org.jetbrains.annotations.NotNull
    android.widget.EditText input) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final android.widget.EditText getInput() {
        return null;
    }
    
    public final void listen() {
    }
}