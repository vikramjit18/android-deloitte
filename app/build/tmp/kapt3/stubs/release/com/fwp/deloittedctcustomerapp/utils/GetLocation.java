package com.fwp.deloittedctcustomerapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u0004H\u0002J\u0010\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u0004H\u0002J\b\u0010\u000f\u001a\u00020\rH\u0003R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/GetLocation;", "Landroidx/lifecycle/LiveData;", "Lcom/google/android/gms/location/LocationResult;", "activity", "Landroid/app/Activity;", "(Landroid/app/Activity;)V", "fusedLocationProviderClient", "Lcom/google/android/gms/location/FusedLocationProviderClient;", "locationCallback", "Lcom/google/android/gms/location/LocationCallback;", "locationManager", "Landroid/location/LocationManager;", "fetchLocation", "", "onGPS", "requestLocation", "app_release"})
public final class GetLocation extends androidx.lifecycle.LiveData<com.google.android.gms.location.LocationResult> {
    private final android.app.Activity activity = null;
    private com.google.android.gms.location.FusedLocationProviderClient fusedLocationProviderClient;
    private final android.location.LocationManager locationManager = null;
    private final com.google.android.gms.location.LocationCallback locationCallback = null;
    
    public GetLocation(@org.jetbrains.annotations.NotNull
    android.app.Activity activity) {
        super(null);
    }
    
    private final void onGPS(android.app.Activity activity) {
    }
    
    private final void fetchLocation(android.app.Activity activity) {
    }
    
    @android.annotation.SuppressLint(value = {"MissingPermission"})
    private final void requestLocation() {
    }
}