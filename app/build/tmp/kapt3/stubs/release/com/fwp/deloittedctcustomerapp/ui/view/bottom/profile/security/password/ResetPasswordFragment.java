package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password;

import java.lang.System;

/**
 * FWP
 * Hilt entry point
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u0000 92\u00020\u0001:\u00019B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0016H\u0002J\b\u0010\u0018\u001a\u00020\u0016H\u0002J\b\u0010\u0019\u001a\u00020\u0016H\u0002J\b\u0010\u001a\u001a\u00020\u0016H\u0002J\u0010\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\b\u0010\u001e\u001a\u00020\u0016H\u0002J\"\u0010\u001f\u001a\u0004\u0018\u00010 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\"H\u0016J$\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)2\b\u0010*\u001a\u0004\u0018\u00010+2\b\u0010,\u001a\u0004\u0018\u00010-H\u0016J\b\u0010.\u001a\u00020\u0016H\u0016J\u001a\u0010/\u001a\u00020\u00162\u0006\u00100\u001a\u00020\'2\b\u0010,\u001a\u0004\u0018\u00010-H\u0016J\b\u00101\u001a\u00020\u0016H\u0002J\b\u00102\u001a\u00020\u0016H\u0002J \u00103\u001a\u00020\u00162\u0006\u00104\u001a\u0002052\u0006\u00106\u001a\u00020\"2\u0006\u00107\u001a\u00020\"H\u0002J\b\u00108\u001a\u00020\u0016H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000f\u001a\u00020\u00108BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006:"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/security/password/ResetPasswordFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/ResetPasswordFragmentBinding;", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/ResetPasswordFragmentBinding;", "listOffLoad", "", "", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "mTextWatcher", "Landroid/text/TextWatcher;", "resetPasswordViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/security/password/ResetPasswordViewModel;", "getResetPasswordViewModel", "()Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/security/password/ResetPasswordViewModel;", "resetPasswordViewModel$delegate", "Lkotlin/Lazy;", "buttonActivation", "", "buttonHandler", "checkLiveNetworkConnection", "disablePrimaryBtn", "enablePrimaryBtn", "offloadApi", "offloadRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/DownloadOffloadRequest;", "offloadObsr", "onCreateAnimation", "Landroid/view/animation/Animation;", "transit", "", "enter", "", "nextAnim", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onViewCreated", "view", "passValidator", "passwordAlert", "passwordValidator", "password", "Lcom/google/android/material/textfield/TextInputEditText;", "validateImg", "unValidateImg", "saveNewPasswordBtnHandler", "Companion", "app_release"})
@dagger.hilt.android.AndroidEntryPoint
public final class ResetPasswordFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password.ResetPasswordFragment.Companion Companion = null;
    
    /**
     * Global variables
     */
    private com.fwp.deloittedctcustomerapp.databinding.ResetPasswordFragmentBinding _binding;
    private final kotlin.Lazy resetPasswordViewModel$delegate = null;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private final java.util.List<java.lang.String> listOffLoad = null;
    
    /**
     * create a textWatcher member
     */
    private final android.text.TextWatcher mTextWatcher = null;
    
    public ResetPasswordFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.ResetPasswordFragmentBinding getBinding() {
        return null;
    }
    
    private final com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password.ResetPasswordViewModel getResetPasswordViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    @java.lang.Override
    public android.view.animation.Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    /**
     * Checking live
     * internet connection
     * function in run time
     */
    private final void checkLiveNetworkConnection() {
    }
    
    @java.lang.Override
    public void onResume() {
    }
    
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void offloadApi(com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest offloadRequest) {
    }
    
    private final void offloadObsr() {
    }
    
    /**
     * All click
     * listener function
     */
    private final void buttonHandler() {
    }
    
    /**
     * Validating password
     */
    private final void passValidator() {
    }
    
    private final void passwordValidator(com.google.android.material.textfield.TextInputEditText password, int validateImg, int unValidateImg) {
    }
    
    /**
     * function for
     * activate primary button
     */
    private final void buttonActivation() {
    }
    
    private final void passwordAlert() {
    }
    
    /**
     * btn click listener
     * after the btn activation
     */
    private final void saveNewPasswordBtnHandler() {
    }
    
    /**
     * function for the
     * disable primary btn
     */
    private final void disablePrimaryBtn() {
    }
    
    /**
     * function for the
     * enabling primary btn
     */
    private final void enablePrimaryBtn() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/security/password/ResetPasswordFragment$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/security/password/ResetPasswordFragment;", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password.ResetPasswordFragment newInstance() {
            return null;
        }
    }
}