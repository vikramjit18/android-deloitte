package com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0011\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B/\u0012\u0010\b\u0001\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\bJ\u0011\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J3\u0010\u0016\u001a\u00020\u00002\u0010\b\u0003\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u0006H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0006H\u00d6\u0001R\"\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u000e\"\u0004\b\u0012\u0010\u0010\u00a8\u0006\u001d"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/changePasswordResponse/ChangePasswordResponse;", "", "resObject", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/changePasswordResponse/ResObject;", "responseCode", "", "responseMessage", "(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V", "getResObject", "()Ljava/util/List;", "setResObject", "(Ljava/util/List;)V", "getResponseCode", "()Ljava/lang/String;", "setResponseCode", "(Ljava/lang/String;)V", "getResponseMessage", "setResponseMessage", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ChangePasswordResponse {
    @org.jetbrains.annotations.Nullable
    private java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ResObject> resObject;
    @org.jetbrains.annotations.Nullable
    private java.lang.String responseCode;
    @org.jetbrains.annotations.Nullable
    private java.lang.String responseMessage;
    
    @org.jetbrains.annotations.NotNull
    public final com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ChangePasswordResponse copy(@org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "resObject")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ResObject> resObject, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "responseCode")
    java.lang.String responseCode, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "responseMessage")
    java.lang.String responseMessage) {
        return null;
    }
    
    @java.lang.Override
    public boolean equals(@org.jetbrains.annotations.Nullable
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.lang.String toString() {
        return null;
    }
    
    public ChangePasswordResponse(@org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "resObject")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ResObject> resObject, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "responseCode")
    java.lang.String responseCode, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "responseMessage")
    java.lang.String responseMessage) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ResObject> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ResObject> getResObject() {
        return null;
    }
    
    public final void setResObject(@org.jetbrains.annotations.Nullable
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.changePasswordResponse.ResObject> p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getResponseCode() {
        return null;
    }
    
    public final void setResponseCode(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getResponseMessage() {
        return null;
    }
    
    public final void setResponseMessage(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
}