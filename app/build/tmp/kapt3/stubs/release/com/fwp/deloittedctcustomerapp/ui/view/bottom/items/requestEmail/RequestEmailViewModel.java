package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\u0015J\u0016\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\n2\u0006\u0010\u0017\u001a\u00020\u0018J\u0016\u0010\u0019\u001a\u00020\u00122\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00070\u001bH\u0002R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00070\f8F\u00a2\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u001d\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\f8F\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u000e\u00a8\u0006\u001c"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/requestEmail/RequestEmailViewModel;", "Landroidx/lifecycle/ViewModel;", "mainRepo", "Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;", "(Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;)V", "_requestCopyEmailResponse", "Landroidx/lifecycle/MutableLiveData;", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/requestEmail/RequestEmailResponse;", "_toast", "Lcom/fwp/deloittedctcustomerapp/utils/Event;", "", "requestCopyEmailResponse", "Landroidx/lifecycle/LiveData;", "getRequestCopyEmailResponse", "()Landroidx/lifecycle/LiveData;", "toast", "getToast", "requestEmailPrimary", "", "jwtToken", "requestCopyPrimaryRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/RequestCopyPrimaryRequest;", "requestEmailSecondary", "requestCopySecondaryRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/RequestCopySecondaryRequest;", "requestResponse", "response", "Lretrofit2/Response;", "app_release"})
public final class RequestEmailViewModel extends androidx.lifecycle.ViewModel {
    private final com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse> _requestCopyEmailResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toast = null;
    
    @javax.inject.Inject
    public RequestEmailViewModel(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse> getRequestCopyEmailResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToast() {
        return null;
    }
    
    public final void requestEmailPrimary(@org.jetbrains.annotations.NotNull
    java.lang.String jwtToken, @org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopyPrimaryRequest requestCopyPrimaryRequest) {
    }
    
    public final void requestEmailSecondary(@org.jetbrains.annotations.NotNull
    java.lang.String jwtToken, @org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopySecondaryRequest requestCopySecondaryRequest) {
    }
    
    private final void requestResponse(retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse> response) {
    }
}