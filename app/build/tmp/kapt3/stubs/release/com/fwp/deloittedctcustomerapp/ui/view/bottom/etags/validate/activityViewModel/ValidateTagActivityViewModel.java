package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010(\u001a\u00020)J\u000e\u0010*\u001a\u00020)2\u0006\u0010+\u001a\u00020,J\u0016\u0010-\u001a\u00020)2\f\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00130/H\u0002J\u000e\u00100\u001a\u00020)2\u0006\u00101\u001a\u000202J\u000e\u00103\u001a\u00020)2\u0006\u00104\u001a\u000205J\u0012\u00106\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u000205070\u0019J\u000e\u00108\u001a\u00020)2\u0006\u00109\u001a\u00020:J\u000e\u0010;\u001a\u00020)2\u0006\u00104\u001a\u000205J\u000e\u0010<\u001a\u00020)2\u0006\u0010+\u001a\u00020,J\u000e\u0010=\u001a\u00020)2\u0006\u0010>\u001a\u00020\u000fR\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0017\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\t0\u00198F\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u001bR\u0017\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00198F\u00a2\u0006\u0006\u001a\u0004\b\u001d\u0010\u001bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\t0\u00198F\u00a2\u0006\u0006\u001a\u0004\b\u001f\u0010\u001bR\u001d\u0010 \u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e0\u00198F\u00a2\u0006\u0006\u001a\u0004\b!\u0010\u001bR\u001d\u0010\"\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e0\u00198F\u00a2\u0006\u0006\u001a\u0004\b#\u0010\u001bR\u001d\u0010$\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u000e0\u00198F\u00a2\u0006\u0006\u001a\u0004\b%\u0010\u001bR\u0017\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00130\u00198F\u00a2\u0006\u0006\u001a\u0004\b\'\u0010\u001b\u00a8\u0006?"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/activityViewModel/ValidateTagActivityViewModel;", "Landroidx/lifecycle/ViewModel;", "mainRepo", "Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;", "appDao", "Lcom/fwp/deloittedctcustomerapp/data/db/AppDao;", "(Lcom/fwp/deloittedctcustomerapp/data/repo/MainRepo;Lcom/fwp/deloittedctcustomerapp/data/db/AppDao;)V", "_downloadResponse", "Landroidx/lifecycle/MutableLiveData;", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/downloadOffload/DownloadOffloadResponse;", "_etagResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagResponse;", "_offloadResponse", "_toast", "Lcom/fwp/deloittedctcustomerapp/utils/Event;", "", "_toastDownloadOffload", "_toastGetEtag", "_validateEtagResponse", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/validateEtag/ValidateEtagResponse;", "coroutineExceptionHandler", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "getCoroutineExceptionHandler", "()Lkotlinx/coroutines/CoroutineExceptionHandler;", "downloadResponse", "Landroidx/lifecycle/LiveData;", "getDownloadResponse", "()Landroidx/lifecycle/LiveData;", "etagResponse", "getEtagResponse", "offloadResponse", "getOffloadResponse", "toast", "getToast", "toastDownloadOffload", "getToastDownloadOffload", "toastGetEtag", "getToastGetEtag", "validateEtagResponse", "getValidateEtagResponse", "deleteValiData", "", "downloadRequest", "downloadOffloadRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/DownloadOffloadRequest;", "etagData", "response", "Lretrofit2/Response;", "getEtag", "etagRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/EtagRequest;", "getEtagValidated", "validateEtagRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ValidateEtagRequest;", "getValidateTagData", "", "insertDownloadTag", "downloadedTags", "Lcom/fwp/deloittedctcustomerapp/data/model/DownloadedTags;", "insertValidateTagData", "offLoadRequest", "removeDetailsDownloadTagsById", "onlineSpiId", "app_release"})
public final class ValidateTagActivityViewModel extends androidx.lifecycle.ViewModel {
    private final com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo = null;
    private final com.fwp.deloittedctcustomerapp.data.db.AppDao appDao = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.validateEtag.ValidateEtagResponse> _validateEtagResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toast = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse> _offloadResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse> _downloadResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse> _etagResponse = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastGetEtag = null;
    private final androidx.lifecycle.MutableLiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> _toastDownloadOffload = null;
    @org.jetbrains.annotations.NotNull
    private final kotlinx.coroutines.CoroutineExceptionHandler coroutineExceptionHandler = null;
    
    @javax.inject.Inject
    public ValidateTagActivityViewModel(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.repo.MainRepo mainRepo, @org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.db.AppDao appDao) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.validateEtag.ValidateEtagResponse> getValidateEtagResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToast() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse> getOffloadResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse> getDownloadResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse> getEtagResponse() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastGetEtag() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<com.fwp.deloittedctcustomerapp.utils.Event<java.lang.String>> getToastDownloadOffload() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final kotlinx.coroutines.CoroutineExceptionHandler getCoroutineExceptionHandler() {
        return null;
    }
    
    public final void getEtagValidated(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest validateEtagRequest) {
    }
    
    private final void etagData(retrofit2.Response<com.fwp.deloittedctcustomerapp.data.model.responses.validateEtag.ValidateEtagResponse> response) {
    }
    
    public final void insertValidateTagData(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest validateEtagRequest) {
    }
    
    public final void deleteValiData() {
    }
    
    public final void insertDownloadTag(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.DownloadedTags downloadedTags) {
    }
    
    public final void removeDetailsDownloadTagsById(@org.jetbrains.annotations.NotNull
    java.lang.String onlineSpiId) {
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.util.List<com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest>> getValidateTagData() {
        return null;
    }
    
    public final void getEtag(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.EtagRequest etagRequest) {
    }
    
    public final void offLoadRequest(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest downloadOffloadRequest) {
    }
    
    public final void downloadRequest(@org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.data.model.requests.DownloadOffloadRequest downloadOffloadRequest) {
    }
}