package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail;

import java.lang.System;

/**
 * FWP
 * Hilt entry point
 */
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u0000 -2\u00020\u0001:\u0001-B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0018\u001a\u00020\u0019H\u0002J\b\u0010\u001a\u001a\u00020\u0019H\u0002J\b\u0010\u001b\u001a\u00020\u0019H\u0002J\b\u0010\u001c\u001a\u00020\u0019H\u0002J$\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\"2\b\u0010#\u001a\u0004\u0018\u00010\fH\u0016J\b\u0010$\u001a\u00020\u0019H\u0016J\u001a\u0010%\u001a\u00020\u00192\u0006\u0010&\u001a\u00020\u001e2\b\u0010#\u001a\u0004\u0018\u00010\fH\u0016J\b\u0010\'\u001a\u00020\u0019H\u0002J\u0010\u0010(\u001a\u00020\u00192\u0006\u0010)\u001a\u00020*H\u0002J\b\u0010+\u001a\u00020\u0019H\u0002J\b\u0010,\u001a\u00020\u0019H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\tR\u0010\u0010\n\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0012\u001a\u00020\u00138BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0014\u0010\u0015\u00a8\u0006."}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/requestEmail/RequestEmailFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/FragmentRequestEmailBinding;", "alsNo", "", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/FragmentRequestEmailBinding;", "dob", "extras", "Landroid/os/Bundle;", "itemAccount", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "mTextWatcher", "Landroid/text/TextWatcher;", "requestEmailViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/requestEmail/RequestEmailViewModel;", "getRequestEmailViewModel", "()Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/requestEmail/RequestEmailViewModel;", "requestEmailViewModel$delegate", "Lkotlin/Lazy;", "buttonEnable", "", "buttonHandler", "disableButton", "enableButton", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "onDestroyView", "onViewCreated", "view", "requestApiHit", "showDialerAlert", "mContext", "Landroid/content/Context;", "validatorEmails", "viewModelObser", "Companion", "app_release"})
@dagger.hilt.android.AndroidEntryPoint
public final class RequestEmailFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailFragment.Companion Companion = null;
    private com.fwp.deloittedctcustomerapp.databinding.FragmentRequestEmailBinding _binding;
    private final kotlin.Lazy requestEmailViewModel$delegate = null;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private java.lang.Object itemAccount;
    private java.lang.Object alsNo;
    private java.lang.Object dob;
    private android.os.Bundle extras;
    
    /**
     * create
     * a textWatcher member
     */
    private final android.text.TextWatcher mTextWatcher = null;
    
    public RequestEmailFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.FragmentRequestEmailBinding getBinding() {
        return null;
    }
    
    private final com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailViewModel getRequestEmailViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void validatorEmails() {
    }
    
    private final void disableButton() {
    }
    
    private final void enableButton() {
    }
    
    private final void buttonHandler() {
    }
    
    private final void showDialerAlert(android.content.Context mContext) {
    }
    
    private final void requestApiHit() {
    }
    
    private final void viewModelObser() {
    }
    
    private final void buttonEnable() {
    }
    
    /**
     * Destroy
     * binding on app close
     */
    @java.lang.Override
    public void onDestroyView() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/requestEmail/RequestEmailFragment$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/requestEmail/RequestEmailFragment;", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailFragment newInstance() {
            return null;
        }
    }
}