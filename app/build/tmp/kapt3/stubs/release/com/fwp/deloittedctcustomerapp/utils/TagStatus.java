package com.fwp.deloittedctcustomerapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000f\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/utils/TagStatus;", "", "()V", "CANCEL", "", "DONATED", "EXPIRED", "ISSUE", "ISSUED", "OFF_LINE_ISSUE", "OFF_LINE_ISSUE_PENDING", "OFF_LINE_ISSUE_PREREQUISITE", "REFUNDED", "REFUND_PENDING", "RETURN", "RETURN_PENDING", "VALIDATED", "VOID", "VOID_PENDING", "app_release"})
public final class TagStatus {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.utils.TagStatus INSTANCE = null;
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ISSUED = "ISSUED";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String VALIDATED = "VALIDATED";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String EXPIRED = "EXPIRED";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String ISSUE = "ISSUE";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String CANCEL = "CANCEL";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String OFF_LINE_ISSUE_PENDING = "OFF-LINE ISSUE/PENDING";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String OFF_LINE_ISSUE_PREREQUISITE = "OFF-LINE ISSUE/PREREQUISITE";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String OFF_LINE_ISSUE = "OFF-LINE ISSUE";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String REFUND_PENDING = "REFUND/PENDING";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String REFUNDED = "REFUNDED";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String RETURN_PENDING = "RETURN/PENDING";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String RETURN = "RETURN";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String VOID_PENDING = "VOID/PENDING";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String VOID = "VOID";
    @org.jetbrains.annotations.NotNull
    public static final java.lang.String DONATED = "DONATED";
    
    private TagStatus() {
        super();
    }
}