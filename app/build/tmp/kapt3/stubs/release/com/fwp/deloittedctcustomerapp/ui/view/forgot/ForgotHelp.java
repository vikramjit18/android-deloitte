package com.fwp.deloittedctcustomerapp.ui.view.forgot;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u00020\bH\u0002J\b\u0010\t\u001a\u00020\bH\u0002J$\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016J\b\u0010\u0012\u001a\u00020\bH\u0002J\b\u0010\u0013\u001a\u00020\bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/forgot/ForgotHelp;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/ForgotHelpFragmentBinding;", "viewModelCommunicator", "Lcom/fwp/deloittedctcustomerapp/ui/viewmodel/Communicator;", "buttonHandler", "", "buttonProperty", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "setEmail", "showAlert", "app_release"})
public final class ForgotHelp extends androidx.fragment.app.Fragment {
    private com.fwp.deloittedctcustomerapp.databinding.ForgotHelpFragmentBinding binding;
    private com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator viewModelCommunicator;
    
    public ForgotHelp() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void buttonHandler() {
    }
    
    @kotlin.OptIn(markerClass = {androidx.compose.ui.text.android.InternalPlatformTextApi.class})
    private final void showAlert() {
    }
    
    @kotlin.OptIn(markerClass = {androidx.compose.ui.text.android.InternalPlatformTextApi.class})
    private final void setEmail() {
    }
    
    private final void buttonProperty() {
    }
}