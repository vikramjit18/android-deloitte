package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u00ae\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010!\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 ^2\u00020\u0001:\u0001^B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010#\u001a\u00020$2\u000e\u0010%\u001a\n\u0012\u0004\u0012\u00020&\u0018\u00010\u000fH\u0002J\b\u0010\'\u001a\u00020$H\u0002J\u0010\u0010(\u001a\u00020$2\u0006\u0010)\u001a\u00020*H\u0002J\b\u0010+\u001a\u00020\u0010H\u0002J\b\u0010,\u001a\u00020$H\u0002J\b\u0010-\u001a\u00020$H\u0002J\b\u0010.\u001a\u00020$H\u0002J\u0010\u0010/\u001a\u00020$2\u0006\u00100\u001a\u00020\u0010H\u0002J\u0010\u00101\u001a\u00020$2\u0006\u00102\u001a\u00020\u0010H\u0002J\u0010\u00103\u001a\u00020$2\u0006\u00104\u001a\u000205H\u0002J\b\u00106\u001a\u00020$H\u0002J$\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:2\b\u0010;\u001a\u0004\u0018\u00010<2\b\u0010=\u001a\u0004\u0018\u00010\u0017H\u0016J\u001a\u0010>\u001a\u00020$2\u0006\u0010?\u001a\u0002082\b\u0010=\u001a\u0004\u0018\u00010\u0017H\u0016J\u0010\u0010@\u001a\u00020$2\u0006\u0010A\u001a\u00020BH\u0002J\u0010\u0010C\u001a\u00020$2\u0006\u00104\u001a\u000205H\u0002J\u0010\u0010D\u001a\u00020$2\u0006\u00104\u001a\u000205H\u0002J\u0010\u0010E\u001a\u00020$2\u0006\u00104\u001a\u000205H\u0002J\u0010\u0010F\u001a\u00020$2\u0006\u00104\u001a\u000205H\u0002J\u0018\u0010G\u001a\u00020$2\u0006\u0010H\u001a\u00020I2\u0006\u0010)\u001a\u00020*H\u0002J\u0010\u0010J\u001a\u00020$2\u0006\u0010K\u001a\u00020LH\u0002J\u0010\u0010M\u001a\u00020$2\u0006\u0010H\u001a\u00020IH\u0002J\u0010\u0010N\u001a\u00020$2\u0006\u0010)\u001a\u00020*H\u0002J\u0018\u0010O\u001a\u00020$2\u0006\u0010P\u001a\u00020Q2\u0006\u0010?\u001a\u000208H\u0002J0\u0010R\u001a\u00020$2\u0006\u0010S\u001a\u00020Q2\u0006\u0010T\u001a\u00020\u00102\u0006\u0010U\u001a\u00020\u00102\u0006\u0010V\u001a\u00020\u00102\u0006\u0010W\u001a\u00020\u0010H\u0002J\u0016\u0010X\u001a\u00020$2\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fH\u0002J\u0016\u0010Y\u001a\u00020$2\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fH\u0002J\u0010\u0010Z\u001a\u00020$2\u0006\u00104\u001a\u000205H\u0002J\u0010\u0010[\u001a\u00020$2\u0006\u0010\\\u001a\u00020]H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00100\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00100\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00100\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006_"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/ValidateTagFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/ValidateTagFragmentBinding;", "accountOwner", "", "animalName", "animalregion", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/ValidateTagFragmentBinding;", "communicator", "Lcom/fwp/deloittedctcustomerapp/ui/viewmodel/Communicator;", "dateAndTime", "", "", "downloadStatus", "", "eTagYearIndex", "etagValiRequest", "Lcom/fwp/deloittedctcustomerapp/data/model/requests/ValidateEtagRequest;", "extras", "Landroid/os/Bundle;", "gson", "Lcom/google/gson/Gson;", "issueDateAndTime", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "offlineSpiidList", "position", "spiIDLIst", "tagStatus", "validateTagActivityViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/activityViewModel/ValidateTagActivityViewModel;", "apiResponseMandatory", "", "eTagMandatoryDetails", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/ETagMandatoryDetail;", "buttonHandler", "buttonProperties", "etagList", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagLicenses;", "dateTimeFormatter", "disableValidateBtn", "downloadObsr", "enableValidateBtn", "initImage", "imagename", "initTagicon", "iconName", "offlineValidationHarvestInfo", "etagLicenseDetails", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagLicenseDetails;", "offloadObsr", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "onViewCreated", "view", "ownerDataFinder", "item", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagAccounts;", "settingClipDownloadData", "settingDownload", "settingDownloadOnEtagActive", "settingHarvestDetails", "settingIssueDate", "etagsubDetails", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagsubDetails;", "settingOfflineHarvestDate", "i", "", "settingSponserDetails", "settingTagStatus", "showPopupMenu", "context", "Landroid/content/Context;", "showRemoveTagStateDialog", "mContext", "tvPrimary", "tvSecondary", "cancelText", "secText", "showTagStateDialog", "startDownloadProcess", "validateInOffline", "validateUISetup", "it", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagResponse;", "Companion", "app_release"})
public final class ValidateTagFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.ValidateTagFragment.Companion Companion = null;
    private java.util.List<java.lang.String> issueDateAndTime;
    private java.util.List<java.lang.String> dateAndTime;
    private java.lang.Object eTagYearIndex;
    private java.lang.Object accountOwner;
    private java.lang.Object position;
    private java.lang.Object animalName;
    private java.lang.Object animalregion;
    private android.os.Bundle extras;
    private com.fwp.deloittedctcustomerapp.databinding.ValidateTagFragmentBinding _binding;
    private com.fwp.deloittedctcustomerapp.ui.viewmodel.Communicator communicator;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private final java.util.List<java.lang.String> spiIDLIst = null;
    private final java.util.List<java.lang.String> offlineSpiidList = null;
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest> etagValiRequest = null;
    private final java.util.List<java.lang.String> tagStatus = null;
    private final java.util.List<java.lang.String> downloadStatus = null;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel validateTagActivityViewModel;
    private final com.google.gson.Gson gson = null;
    
    public ValidateTagFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.ValidateTagFragmentBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initImage(java.lang.String imagename) {
    }
    
    private final void initTagicon(java.lang.String iconName) {
    }
    
    private final void buttonHandler() {
    }
    
    private final void apiResponseMandatory(java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.ETagMandatoryDetail> eTagMandatoryDetails) {
    }
    
    private final void showPopupMenu(android.content.Context context, android.view.View view) {
    }
    
    private final void showRemoveTagStateDialog(android.content.Context mContext, java.lang.String tvPrimary, java.lang.String tvSecondary, java.lang.String cancelText, java.lang.String secText) {
    }
    
    private final void offloadObsr() {
    }
    
    private final void validateUISetup(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse it) {
    }
    
    private final void ownerDataFinder(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagAccounts item) {
    }
    
    private final void settingIssueDate(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagsubDetails etagsubDetails, com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses etagList) {
    }
    
    private final void settingClipDownloadData(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenseDetails etagLicenseDetails) {
    }
    
    private final void settingDownload(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenseDetails etagLicenseDetails) {
    }
    
    private final void settingSponserDetails(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagsubDetails etagsubDetails) {
    }
    
    private final void settingHarvestDetails(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenseDetails etagLicenseDetails) {
    }
    
    private final void offlineValidationHarvestInfo(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenseDetails etagLicenseDetails) {
    }
    
    private final void settingOfflineHarvestDate(int i) {
    }
    
    private final void settingTagStatus(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses etagList) {
    }
    
    private final void settingDownloadOnEtagActive(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenseDetails etagLicenseDetails) {
    }
    
    private final java.lang.String dateTimeFormatter() {
        return null;
    }
    
    private final void validateInOffline(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenseDetails etagLicenseDetails) {
    }
    
    private final void showTagStateDialog(java.util.List<java.lang.String> spiIDLIst) {
    }
    
    private final void startDownloadProcess(java.util.List<java.lang.String> spiIDLIst) {
    }
    
    private final void buttonProperties(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses etagList) {
    }
    
    private final void enableValidateBtn() {
    }
    
    private final void disableValidateBtn() {
    }
    
    private final void downloadObsr() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/ValidateTagFragment$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/ValidateTagFragment;", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.ValidateTagFragment newInstance() {
            return null;
        }
    }
}