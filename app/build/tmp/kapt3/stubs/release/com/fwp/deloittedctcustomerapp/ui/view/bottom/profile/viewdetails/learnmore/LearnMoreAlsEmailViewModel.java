package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.learnmore;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00078\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t\u00a8\u0006\n"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/profile/viewdetails/learnmore/LearnMoreAlsEmailViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_content1Text", "Landroidx/lifecycle/MutableLiveData;", "", "content1Text", "Landroidx/lifecycle/LiveData;", "getContent1Text", "()Landroidx/lifecycle/LiveData;", "app_release"})
public final class LearnMoreAlsEmailViewModel extends androidx.lifecycle.ViewModel {
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.P)
    private final androidx.lifecycle.MutableLiveData<java.lang.String> _content1Text = null;
    @org.jetbrains.annotations.NotNull
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.P)
    private final androidx.lifecycle.LiveData<java.lang.String> content1Text = null;
    
    public LearnMoreAlsEmailViewModel() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.lifecycle.LiveData<java.lang.String> getContent1Text() {
        return null;
    }
}