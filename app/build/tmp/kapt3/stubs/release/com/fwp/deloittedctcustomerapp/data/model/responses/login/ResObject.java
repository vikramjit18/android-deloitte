package com.fwp.deloittedctcustomerapp.data.model.responses.login;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001e\b\u0087\b\u0018\u00002\u00020\u0001BY\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\n\b\u0001\u0010\t\u001a\u0004\u0018\u00010\n\u0012\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJ\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u0010\u0010\u001e\u001a\u0004\u0018\u00010\bH\u00c6\u0003\u00a2\u0006\u0002\u0010\u0015J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\nH\u00c6\u0003J\u0010\u0010 \u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJ\u000b\u0010!\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003Jb\u0010\"\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0003\u0010\t\u001a\u0004\u0018\u00010\n2\n\b\u0003\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\f\u001a\u0004\u0018\u00010\u0005H\u00c6\u0001\u00a2\u0006\u0002\u0010#J\u0013\u0010$\u001a\u00020\u00032\b\u0010%\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010&\u001a\u00020\bH\u00d6\u0001J\t\u0010\'\u001a\u00020\u0005H\u00d6\u0001R\u0015\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u000e\u0010\u000fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012R\u0015\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\n\n\u0002\u0010\u0016\u001a\u0004\b\u0014\u0010\u0015R\u0013\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0015\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u0019\u0010\u000fR\u0013\u0010\f\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0012\u00a8\u0006("}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/login/ResObject;", "", "alsLinked", "", "fwpEmail", "", "jwt", "logoutPeriod", "", "name", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/login/Name;", "touAccepted", "userName", "(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/fwp/deloittedctcustomerapp/data/model/responses/login/Name;Ljava/lang/Boolean;Ljava/lang/String;)V", "getAlsLinked", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "getFwpEmail", "()Ljava/lang/String;", "getJwt", "getLogoutPeriod", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getName", "()Lcom/fwp/deloittedctcustomerapp/data/model/responses/login/Name;", "getTouAccepted", "getUserName", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/fwp/deloittedctcustomerapp/data/model/responses/login/Name;Ljava/lang/Boolean;Ljava/lang/String;)Lcom/fwp/deloittedctcustomerapp/data/model/responses/login/ResObject;", "equals", "other", "hashCode", "toString", "app_release"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ResObject {
    @org.jetbrains.annotations.Nullable
    private final java.lang.Boolean alsLinked = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String fwpEmail = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String jwt = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.Integer logoutPeriod = null;
    @org.jetbrains.annotations.Nullable
    private final com.fwp.deloittedctcustomerapp.data.model.responses.login.Name name = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.Boolean touAccepted = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String userName = null;
    
    @org.jetbrains.annotations.NotNull
    public final com.fwp.deloittedctcustomerapp.data.model.responses.login.ResObject copy(@org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "alsLinked")
    java.lang.Boolean alsLinked, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "fwpEmail")
    java.lang.String fwpEmail, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "jwt")
    java.lang.String jwt, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "logoutPeriod")
    java.lang.Integer logoutPeriod, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "name")
    com.fwp.deloittedctcustomerapp.data.model.responses.login.Name name, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "touAccepted")
    java.lang.Boolean touAccepted, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "userName")
    java.lang.String userName) {
        return null;
    }
    
    @java.lang.Override
    public boolean equals(@org.jetbrains.annotations.Nullable
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.lang.String toString() {
        return null;
    }
    
    public ResObject(@org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "alsLinked")
    java.lang.Boolean alsLinked, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "fwpEmail")
    java.lang.String fwpEmail, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "jwt")
    java.lang.String jwt, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "logoutPeriod")
    java.lang.Integer logoutPeriod, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "name")
    com.fwp.deloittedctcustomerapp.data.model.responses.login.Name name, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "touAccepted")
    java.lang.Boolean touAccepted, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "userName")
    java.lang.String userName) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Boolean component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Boolean getAlsLinked() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getFwpEmail() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getJwt() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Integer component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Integer getLogoutPeriod() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final com.fwp.deloittedctcustomerapp.data.model.responses.login.Name component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final com.fwp.deloittedctcustomerapp.data.model.responses.login.Name getName() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Boolean component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Boolean getTouAccepted() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getUserName() {
        return null;
    }
}