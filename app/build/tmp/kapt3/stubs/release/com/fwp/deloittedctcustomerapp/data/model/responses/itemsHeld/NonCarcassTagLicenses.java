package com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u001c\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001BS\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0010\b\u0001\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006\u0012\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\n\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u000bJ\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0011\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006H\u00c6\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010!\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003JW\u0010\"\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u0010\b\u0003\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00062\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\t\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\n\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010&\u001a\u00020\'H\u00d6\u0001J\t\u0010(\u001a\u00020\u0003H\u00d6\u0001R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\r\"\u0004\b\u0011\u0010\u000fR\"\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001c\u0010\b\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\r\"\u0004\b\u0017\u0010\u000fR\u001c\u0010\t\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\r\"\u0004\b\u0019\u0010\u000fR\u001c\u0010\n\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\r\"\u0004\b\u001b\u0010\u000f\u00a8\u0006)"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/NonCarcassTagLicenses;", "", "alsNo", "", "licenseYear", "nonCarcassTagLicensesList", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/NonCarcassTagLicensesX;", "owner", "session", "usageDates", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAlsNo", "()Ljava/lang/String;", "setAlsNo", "(Ljava/lang/String;)V", "getLicenseYear", "setLicenseYear", "getNonCarcassTagLicensesList", "()Ljava/util/List;", "setNonCarcassTagLicensesList", "(Ljava/util/List;)V", "getOwner", "setOwner", "getSession", "setSession", "getUsageDates", "setUsageDates", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class NonCarcassTagLicenses {
    @org.jetbrains.annotations.Nullable
    private java.lang.String alsNo;
    @org.jetbrains.annotations.Nullable
    private java.lang.String licenseYear;
    @org.jetbrains.annotations.Nullable
    private java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicensesX> nonCarcassTagLicensesList;
    @org.jetbrains.annotations.Nullable
    private java.lang.String owner;
    @org.jetbrains.annotations.Nullable
    private java.lang.String session;
    @org.jetbrains.annotations.Nullable
    private java.lang.String usageDates;
    
    @org.jetbrains.annotations.NotNull
    public final com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicenses copy(@org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "alsNo")
    java.lang.String alsNo, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "licenseYear")
    java.lang.String licenseYear, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "nonCarcassTagLicensesList")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicensesX> nonCarcassTagLicensesList, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "owner")
    java.lang.String owner, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "session")
    java.lang.String session, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "usageDates")
    java.lang.String usageDates) {
        return null;
    }
    
    @java.lang.Override
    public boolean equals(@org.jetbrains.annotations.Nullable
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.lang.String toString() {
        return null;
    }
    
    public NonCarcassTagLicenses(@org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "alsNo")
    java.lang.String alsNo, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "licenseYear")
    java.lang.String licenseYear, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "nonCarcassTagLicensesList")
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicensesX> nonCarcassTagLicensesList, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "owner")
    java.lang.String owner, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "session")
    java.lang.String session, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "usageDates")
    java.lang.String usageDates) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getAlsNo() {
        return null;
    }
    
    public final void setAlsNo(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getLicenseYear() {
        return null;
    }
    
    public final void setLicenseYear(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicensesX> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicensesX> getNonCarcassTagLicensesList() {
        return null;
    }
    
    public final void setNonCarcassTagLicensesList(@org.jetbrains.annotations.Nullable
    java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.NonCarcassTagLicensesX> p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getOwner() {
        return null;
    }
    
    public final void setOwner(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getSession() {
        return null;
    }
    
    public final void setSession(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getUsageDates() {
        return null;
    }
    
    public final void setUsageDates(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
}