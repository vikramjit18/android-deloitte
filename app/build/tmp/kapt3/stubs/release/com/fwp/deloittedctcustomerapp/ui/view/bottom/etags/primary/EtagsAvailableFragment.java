package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\b\b\u0007\u0018\u0000 Z2\u00020\u0001:\u0001ZB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010!\u001a\u00020\"H\u0002J\b\u0010#\u001a\u00020\"H\u0002J\b\u0010$\u001a\u00020\"H\u0002J\u0010\u0010%\u001a\u00020\"2\u0006\u0010&\u001a\u00020\u0007H\u0002J\u001e\u0010\'\u001a\u00020\"2\u0006\u0010(\u001a\u00020)2\f\u0010*\u001a\b\u0012\u0004\u0012\u00020,0+H\u0002J\u0010\u0010-\u001a\u00020\"2\u0006\u0010.\u001a\u00020\u0007H\u0002J\u0012\u0010/\u001a\u00020\"2\b\u00100\u001a\u0004\u0018\u000101H\u0016J$\u00102\u001a\u0002032\u0006\u00104\u001a\u0002052\b\u00106\u001a\u0004\u0018\u0001072\b\u00100\u001a\u0004\u0018\u000101H\u0016J\b\u00108\u001a\u00020\"H\u0016J\b\u00109\u001a\u00020\"H\u0016J\u001a\u0010:\u001a\u00020\"2\u0006\u0010;\u001a\u0002032\b\u00100\u001a\u0004\u0018\u000101H\u0016J$\u0010<\u001a\u00020\"2\f\u0010=\u001a\b\u0012\u0004\u0012\u00020\u00130+2\f\u0010>\u001a\b\u0012\u0004\u0012\u00020\u00130+H\u0002J\b\u0010?\u001a\u00020\"H\u0002J\u0010\u0010@\u001a\u00020\"2\u0006\u0010A\u001a\u00020BH\u0003J\u0010\u0010C\u001a\u00020\"2\u0006\u0010D\u001a\u00020EH\u0002J>\u0010F\u001a\u00020\"2\u0006\u0010G\u001a\u00020H2\u0006\u0010I\u001a\u00020\u00072\u0006\u0010J\u001a\u00020\u00072\u0006\u0010K\u001a\u00020\u00072\u0006\u0010L\u001a\u00020\u00072\f\u0010M\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0002J\u0018\u0010N\u001a\u00020\"2\u0006\u0010O\u001a\u00020H2\u0006\u0010;\u001a\u000203H\u0002J\u0018\u0010P\u001a\u00020\"2\u0006\u0010Q\u001a\u00020R2\u0006\u0010D\u001a\u00020SH\u0002J\u0010\u0010T\u001a\u00020\"2\u0006\u0010D\u001a\u00020SH\u0002J0\u0010U\u001a\u00020\"2\u0006\u0010G\u001a\u00020H2\u0006\u0010I\u001a\u00020\u00072\u0006\u0010J\u001a\u00020\u00072\u0006\u0010K\u001a\u00020\u00072\u0006\u0010L\u001a\u00020\u0007H\u0002J\u0018\u0010V\u001a\u00020\"2\u0006\u0010Q\u001a\u00020R2\u0006\u0010D\u001a\u00020EH\u0002J\u0016\u0010W\u001a\u00020\"2\u0006\u0010Q\u001a\u00020R2\u0006\u0010D\u001a\u00020EJ\u0018\u0010X\u001a\u00020\"2\u0006\u0010Q\u001a\u00020R2\u0006\u0010D\u001a\u00020SH\u0002J\b\u0010Y\u001a\u00020\"H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006["}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/EtagsAvailableFragment;", "Landroidx/fragment/app/Fragment;", "()V", "_binding", "Lcom/fwp/deloittedctcustomerapp/databinding/EtagsAvailableFragmentBinding;", "allCurrentSpiIdList", "", "", "allDownloadStatus", "allDownloadStatusForOffLoad", "allPreviousSpiIdList", "binding", "getBinding", "()Lcom/fwp/deloittedctcustomerapp/databinding/EtagsAvailableFragmentBinding;", "currentDownloadStatus", "downLoadAllSpiIDLIst", "gson", "Lcom/google/gson/Gson;", "listCurrentYearDetail", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/Etag;", "listPreviousYearDetail", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "offLoadAllSpiIDLIst", "offLoadPreSpiIDLIst", "offlineSpiid", "previousDownloadStatus", "validateTagActivityViewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/validate/activityViewModel/ValidateTagActivityViewModel;", "viewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "watchDbToDelete", "", "buttonHandler", "", "checkLiveNetworkConnection", "clearList", "downloadObsr", "spiIDLIst", "loopEtagLicenseList", "userEtags", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/Etag;", "etagLicensesList", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagLicenses;", "offloadObsr", "idString", "onActivityCreated", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onPause", "onResume", "onViewCreated", "view", "recyclerContent", "previousList", "currentList", "setSpannableString", "setupUI", "it", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/etag/EtagResponse;", "showCurrentProgressBtn", "holder", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/adapter/EtagCurrentYearAdapter$MyCurrentYearViewHolder;", "showDownloadTagStateDialog", "mContext", "Landroid/content/Context;", "tvPrimary", "tvSecondary", "cancelText", "secText", "downLoadSpiIDLIst", "showPopupMenu", "context", "showPreTagStateDialog", "position", "", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/adapter/EtagPreviousYearAdapter$MyPreviousYearViewHolder;", "showPreviousProgressBtn", "showRemoveTagStateDialog", "showTagStateDialog", "startCurrentDownloadProcess", "startPreDownloadProcess", "viewModelObsr", "Companion", "app_release"})
@dagger.hilt.android.AndroidEntryPoint
public final class EtagsAvailableFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.EtagsAvailableFragment.Companion Companion = null;
    private boolean watchDbToDelete = false;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel viewModel;
    private com.fwp.deloittedctcustomerapp.databinding.EtagsAvailableFragmentBinding _binding;
    private final java.util.List<java.lang.String> downLoadAllSpiIDLIst = null;
    private final java.util.List<java.lang.String> offLoadAllSpiIDLIst = null;
    private final java.util.List<java.lang.String> offLoadPreSpiIDLIst = null;
    private final java.util.List<java.lang.String> allCurrentSpiIdList = null;
    private final java.util.List<java.lang.String> allPreviousSpiIdList = null;
    private final java.util.List<java.lang.String> currentDownloadStatus = null;
    private final java.util.List<java.lang.String> previousDownloadStatus = null;
    private final java.util.List<java.lang.String> allDownloadStatus = null;
    private final java.util.List<java.lang.String> allDownloadStatusForOffLoad = null;
    private final java.util.List<java.lang.String> offlineSpiid = null;
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.Etag> listCurrentYearDetail = null;
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.Etag> listPreviousYearDetail = null;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel validateTagActivityViewModel;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private final com.google.gson.Gson gson = null;
    
    public EtagsAvailableFragment() {
        super();
    }
    
    private final com.fwp.deloittedctcustomerapp.databinding.EtagsAvailableFragmentBinding getBinding() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void checkLiveNetworkConnection() {
    }
    
    @java.lang.Override
    public void onResume() {
    }
    
    @java.lang.Override
    public void onActivityCreated(@org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    private final void viewModelObsr() {
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    private final void setupUI(com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse it) {
    }
    
    private final void loopEtagLicenseList(com.fwp.deloittedctcustomerapp.data.model.responses.etag.Etag userEtags, java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses> etagLicensesList) {
    }
    
    private final void clearList() {
    }
    
    private final void recyclerContent(java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.Etag> previousList, java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.Etag> currentList) {
    }
    
    public final void startCurrentDownloadProcess(int position, @org.jetbrains.annotations.NotNull
    com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagCurrentYearAdapter.MyCurrentYearViewHolder holder) {
    }
    
    private final void startPreDownloadProcess(int position, com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.MyPreviousYearViewHolder holder) {
    }
    
    private final void showTagStateDialog(int position, com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagCurrentYearAdapter.MyCurrentYearViewHolder holder) {
    }
    
    private final void showPreTagStateDialog(int position, com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.MyPreviousYearViewHolder holder) {
    }
    
    private final void showCurrentProgressBtn(com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagCurrentYearAdapter.MyCurrentYearViewHolder holder) {
    }
    
    private final void showPreviousProgressBtn(com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.adapter.EtagPreviousYearAdapter.MyPreviousYearViewHolder holder) {
    }
    
    @kotlin.OptIn(markerClass = {androidx.compose.ui.text.android.InternalPlatformTextApi.class})
    private final void setSpannableString() {
    }
    
    private final void buttonHandler() {
    }
    
    private final void downloadObsr(java.lang.String spiIDLIst) {
    }
    
    private final void showPopupMenu(android.content.Context context, android.view.View view) {
    }
    
    private final void offloadObsr(java.lang.String idString) {
    }
    
    private final void showRemoveTagStateDialog(android.content.Context mContext, java.lang.String tvPrimary, java.lang.String tvSecondary, java.lang.String cancelText, java.lang.String secText) {
    }
    
    private final void showDownloadTagStateDialog(android.content.Context mContext, java.lang.String tvPrimary, java.lang.String tvSecondary, java.lang.String cancelText, java.lang.String secText, java.util.List<java.lang.String> downLoadSpiIDLIst) {
    }
    
    @java.lang.Override
    public void onPause() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/EtagsAvailableFragment$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/etags/primary/EtagsAvailableFragment;", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.EtagsAvailableFragment newInstance() {
            return null;
        }
    }
}