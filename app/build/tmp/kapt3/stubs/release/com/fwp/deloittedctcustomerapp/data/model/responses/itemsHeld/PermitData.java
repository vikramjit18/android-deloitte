package com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b%\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B}\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\u0010\b\u0001\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\t\u0012\u0010\b\u0001\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\t\u0012\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\rJ\u000b\u0010$\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\'\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010(\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0011\u0010)\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\tH\u00c6\u0003J\u0011\u0010*\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\tH\u00c6\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010,\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0081\u0001\u0010-\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00032\u0010\b\u0003\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\t2\u0010\b\u0003\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\t2\n\b\u0003\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010.\u001a\u00020/2\b\u00100\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00101\u001a\u000202H\u00d6\u0001J\t\u00103\u001a\u00020\u0003H\u00d6\u0001R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000f\"\u0004\b\u0013\u0010\u0011R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u000f\"\u0004\b\u0015\u0010\u0011R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u000f\"\u0004\b\u0017\u0010\u0011R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u000f\"\u0004\b\u0019\u0010\u0011R\"\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\"\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001b\"\u0004\b\u001f\u0010\u001dR\u001c\u0010\u000b\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u000f\"\u0004\b!\u0010\u0011R\u001c\u0010\f\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u000f\"\u0004\b#\u0010\u0011\u00a8\u00064"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/PermitData;", "", "alsNo", "", "designatedArea", "issuedDate", "owner", "permit", "regulationsSection1", "", "regulationsSection2", "speciesDescription", "speciesName", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V", "getAlsNo", "()Ljava/lang/String;", "setAlsNo", "(Ljava/lang/String;)V", "getDesignatedArea", "setDesignatedArea", "getIssuedDate", "setIssuedDate", "getOwner", "setOwner", "getPermit", "setPermit", "getRegulationsSection1", "()Ljava/util/List;", "setRegulationsSection1", "(Ljava/util/List;)V", "getRegulationsSection2", "setRegulationsSection2", "getSpeciesDescription", "setSpeciesDescription", "getSpeciesName", "setSpeciesName", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class PermitData {
    @org.jetbrains.annotations.Nullable
    private java.lang.String alsNo;
    @org.jetbrains.annotations.Nullable
    private java.lang.String designatedArea;
    @org.jetbrains.annotations.Nullable
    private java.lang.String issuedDate;
    @org.jetbrains.annotations.Nullable
    private java.lang.String owner;
    @org.jetbrains.annotations.Nullable
    private java.lang.String permit;
    @org.jetbrains.annotations.Nullable
    private java.util.List<java.lang.String> regulationsSection1;
    @org.jetbrains.annotations.Nullable
    private java.util.List<java.lang.String> regulationsSection2;
    @org.jetbrains.annotations.Nullable
    private java.lang.String speciesDescription;
    @org.jetbrains.annotations.Nullable
    private java.lang.String speciesName;
    
    @org.jetbrains.annotations.NotNull
    public final com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.PermitData copy(@org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "alsNo")
    java.lang.String alsNo, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "designatedArea")
    java.lang.String designatedArea, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "issuedDate")
    java.lang.String issuedDate, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "owner")
    java.lang.String owner, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "permit")
    java.lang.String permit, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "regulationsSection1")
    java.util.List<java.lang.String> regulationsSection1, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "regulationsSection2")
    java.util.List<java.lang.String> regulationsSection2, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "speciesDescription")
    java.lang.String speciesDescription, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "speciesName")
    java.lang.String speciesName) {
        return null;
    }
    
    @java.lang.Override
    public boolean equals(@org.jetbrains.annotations.Nullable
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.lang.String toString() {
        return null;
    }
    
    public PermitData(@org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "alsNo")
    java.lang.String alsNo, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "designatedArea")
    java.lang.String designatedArea, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "issuedDate")
    java.lang.String issuedDate, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "owner")
    java.lang.String owner, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "permit")
    java.lang.String permit, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "regulationsSection1")
    java.util.List<java.lang.String> regulationsSection1, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "regulationsSection2")
    java.util.List<java.lang.String> regulationsSection2, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "speciesDescription")
    java.lang.String speciesDescription, @org.jetbrains.annotations.Nullable
    @com.squareup.moshi.Json(name = "speciesName")
    java.lang.String speciesName) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getAlsNo() {
        return null;
    }
    
    public final void setAlsNo(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getDesignatedArea() {
        return null;
    }
    
    public final void setDesignatedArea(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getIssuedDate() {
        return null;
    }
    
    public final void setIssuedDate(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getOwner() {
        return null;
    }
    
    public final void setOwner(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getPermit() {
        return null;
    }
    
    public final void setPermit(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<java.lang.String> component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<java.lang.String> getRegulationsSection1() {
        return null;
    }
    
    public final void setRegulationsSection1(@org.jetbrains.annotations.Nullable
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<java.lang.String> component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.util.List<java.lang.String> getRegulationsSection2() {
        return null;
    }
    
    public final void setRegulationsSection2(@org.jetbrains.annotations.Nullable
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getSpeciesDescription() {
        return null;
    }
    
    public final void setSpeciesDescription(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getSpeciesName() {
        return null;
    }
    
    public final void setSpeciesName(@org.jetbrains.annotations.Nullable
    java.lang.String p0) {
    }
}