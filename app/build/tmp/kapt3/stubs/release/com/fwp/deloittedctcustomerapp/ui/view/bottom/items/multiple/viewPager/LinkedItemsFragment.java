package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.multiple.viewPager;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0007\u0018\u0000 D2\u00020\u0001:\u0001DB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0016\u001a\u00020\u0017H\u0002J\u0018\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0006H\u0002J&\u0010\u001c\u001a\u00020\u00172\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001e2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0006H\u0002J\u0018\u0010 \u001a\u00020\u00172\u0006\u0010!\u001a\u00020\"2\u0006\u0010\u001b\u001a\u00020\u0006H\u0002J\u0012\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010\u0006H\u0002J$\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)2\b\u0010*\u001a\u0004\u0018\u00010+2\b\u0010,\u001a\u0004\u0018\u00010-H\u0016J\b\u0010.\u001a\u00020\u0017H\u0016J\u001a\u0010/\u001a\u00020\u00172\u0006\u00100\u001a\u00020\'2\b\u0010,\u001a\u0004\u0018\u00010-H\u0016J\b\u00101\u001a\u00020\u0017H\u0002J&\u00102\u001a\u00020\u00172\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001e2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0006H\u0002J\u0018\u00103\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0006H\u0002J\u0018\u00104\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0006H\u0002J$\u00105\u001a\u00020\u00172\f\u00106\u001a\b\u0012\u0004\u0012\u00020\r0\u001e2\f\u00107\u001a\b\u0012\u0004\u0012\u00020\r0\u001eH\u0002J\u0018\u00108\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0006H\u0002J\b\u00109\u001a\u00020\u0017H\u0002J\u001a\u0010:\u001a\u00020\u00172\b\u0010;\u001a\u0004\u0018\u00010\"2\u0006\u0010\u001b\u001a\u00020\u0006H\u0002J0\u0010<\u001a\u00020\u00172\u0006\u0010=\u001a\u00020>2\u0006\u00100\u001a\u00020\'2\u0006\u0010?\u001a\u00020\u00062\u0006\u0010@\u001a\u00020\u00062\u0006\u0010A\u001a\u00020\u0006H\u0002J\u0010\u0010B\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0010\u0010C\u001a\u00020\u00172\u0006\u0010\u001b\u001a\u00020\u0006H\u0003R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006E"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/multiple/viewPager/LinkedItemsFragment;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/fwp/deloittedctcustomerapp/databinding/LinkedItemsFragmentBinding;", "currentYear", "", "gson", "Lcom/google/gson/Gson;", "lastSpinnerIndex", "", "listCurrentYearDetail", "", "Lcom/fwp/deloittedctcustomerapp/data/model/ItemDetail;", "listPreviousYearDetail", "loadingDialog", "Lcom/fwp/deloittedctcustomerapp/utils/LoadingDialog;", "ownerItemList", "previousYear", "selectedName", "viewModel", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/viewmodel/LandingScreenActivityViewModel;", "buttonHandler", "", "currentPermit", "responseList", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/Account;", "name", "currentPermitList", "permit", "", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/PermitData;", "findAccount", "it", "Lcom/fwp/deloittedctcustomerapp/data/model/responses/itemsHeld/ItemsHeldResponse;", "getOwner", "", "owner", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onPause", "onViewCreated", "view", "ownerSpinner", "perviousPermitList", "previousLicenceYear", "previousPermit", "recyclerContent", "previousList", "currentList", "setOwnerItemDetailsCurrent", "setSpannableString", "setUpUI", "response", "showPopupMenu", "context", "Landroid/content/Context;", "als", "dob", "year", "shownHidePreviousYearState", "viewModelObsr", "Companion", "app_release"})
@dagger.hilt.android.AndroidEntryPoint
public final class LinkedItemsFragment extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull
    public static final com.fwp.deloittedctcustomerapp.ui.view.bottom.items.multiple.viewPager.LinkedItemsFragment.Companion Companion = null;
    private com.fwp.deloittedctcustomerapp.databinding.LinkedItemsFragmentBinding binding;
    private com.fwp.deloittedctcustomerapp.utils.LoadingDialog loadingDialog;
    private com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel viewModel;
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.ItemDetail> listCurrentYearDetail = null;
    private final java.util.List<com.fwp.deloittedctcustomerapp.data.model.ItemDetail> listPreviousYearDetail = null;
    private final java.util.List<java.lang.String> ownerItemList = null;
    private java.lang.String currentYear = "";
    private java.lang.String previousYear = "";
    private java.lang.String selectedName = "";
    private int lastSpinnerIndex = 0;
    private final com.google.gson.Gson gson = null;
    
    public LinkedItemsFragment() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override
    public void onViewCreated(@org.jetbrains.annotations.NotNull
    android.view.View view, @org.jetbrains.annotations.Nullable
    android.os.Bundle savedInstanceState) {
    }
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    private final void viewModelObsr(java.lang.String name) {
    }
    
    private final void setUpUI(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse response, java.lang.String name) {
    }
    
    private final void findAccount(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.ItemsHeldResponse it, java.lang.String name) {
    }
    
    private final void setOwnerItemDetailsCurrent(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account responseList, java.lang.String name) {
    }
    
    private final void previousPermit(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account responseList, java.lang.String name) {
    }
    
    private final void perviousPermitList(java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.PermitData> permit, com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account responseList, java.lang.String name) {
    }
    
    private final void shownHidePreviousYearState(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account responseList) {
    }
    
    private final void previousLicenceYear(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account responseList, java.lang.String name) {
    }
    
    private final void currentPermit(com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account responseList, java.lang.String name) {
    }
    
    private final void currentPermitList(java.util.List<com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.PermitData> permit, com.fwp.deloittedctcustomerapp.data.model.responses.itemsHeld.Account responseList, java.lang.String name) {
    }
    
    private final java.lang.Object getOwner(java.lang.String owner) {
        return null;
    }
    
    @kotlin.OptIn(markerClass = {androidx.compose.ui.text.android.InternalPlatformTextApi.class})
    private final void setSpannableString() {
    }
    
    private final void ownerSpinner() {
    }
    
    private final void recyclerContent(java.util.List<com.fwp.deloittedctcustomerapp.data.model.ItemDetail> previousList, java.util.List<com.fwp.deloittedctcustomerapp.data.model.ItemDetail> currentList) {
    }
    
    private final void buttonHandler() {
    }
    
    private final void showPopupMenu(android.content.Context context, android.view.View view, java.lang.String als, java.lang.String dob, java.lang.String year) {
    }
    
    @java.lang.Override
    public void onPause() {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/multiple/viewPager/LinkedItemsFragment$Companion;", "", "()V", "newInstance", "Lcom/fwp/deloittedctcustomerapp/ui/view/bottom/items/multiple/viewPager/LinkedItemsFragment;", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final com.fwp.deloittedctcustomerapp.ui.view.bottom.items.multiple.viewPager.LinkedItemsFragment newInstance() {
            return null;
        }
    }
}