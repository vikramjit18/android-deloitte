-if class com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse
-if class com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponse
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.requestEmail.RequestEmailResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
