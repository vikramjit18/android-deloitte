-if class com.fwp.deloittedctcustomerapp.data.model.responses.profile.ResObject
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.profile.ResObject
-if class com.fwp.deloittedctcustomerapp.data.model.responses.profile.ResObject
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.profile.ResObjectJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
