-if class com.fwp.deloittedctcustomerapp.data.model.responses.etag.Etag
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.etag.Etag
-if class com.fwp.deloittedctcustomerapp.data.model.responses.etag.Etag
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
