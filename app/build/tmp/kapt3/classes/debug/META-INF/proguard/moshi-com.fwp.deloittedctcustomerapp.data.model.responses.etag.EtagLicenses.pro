-if class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses
-if class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicenses
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagLicensesJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
