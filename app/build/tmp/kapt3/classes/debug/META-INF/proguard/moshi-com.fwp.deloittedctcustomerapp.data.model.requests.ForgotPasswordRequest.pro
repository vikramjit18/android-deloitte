-if class com.fwp.deloittedctcustomerapp.data.model.requests.ForgotPasswordRequest
-keepnames class com.fwp.deloittedctcustomerapp.data.model.requests.ForgotPasswordRequest
-if class com.fwp.deloittedctcustomerapp.data.model.requests.ForgotPasswordRequest
-keep class com.fwp.deloittedctcustomerapp.data.model.requests.ForgotPasswordRequestJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
