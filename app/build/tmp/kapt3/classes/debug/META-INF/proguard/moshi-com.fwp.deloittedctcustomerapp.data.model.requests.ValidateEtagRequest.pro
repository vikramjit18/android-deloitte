-if class com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest
-keepnames class com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest
-if class com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest
-keep class com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequestJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest
-keepclassmembers class com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest {
    public synthetic <init>(int,int,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.Double,java.lang.Double,java.lang.String,java.lang.String,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
