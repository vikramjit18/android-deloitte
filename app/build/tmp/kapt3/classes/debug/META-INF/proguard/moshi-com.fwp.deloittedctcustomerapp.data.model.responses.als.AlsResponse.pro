-if class com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse
-if class com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponse
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.als.AlsResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
