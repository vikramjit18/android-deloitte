-if class com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse
-if class com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponse
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.zipCode.ZipcodeResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
