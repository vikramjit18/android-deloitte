-if class com.fwp.deloittedctcustomerapp.data.model.responses.masterData.GeoInfo
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.masterData.GeoInfo
-if class com.fwp.deloittedctcustomerapp.data.model.responses.masterData.GeoInfo
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.masterData.GeoInfoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
