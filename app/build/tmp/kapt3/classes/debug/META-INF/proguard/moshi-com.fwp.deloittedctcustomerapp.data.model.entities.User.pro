-if class com.fwp.deloittedctcustomerapp.data.model.entities.User
-keepnames class com.fwp.deloittedctcustomerapp.data.model.entities.User
-if class com.fwp.deloittedctcustomerapp.data.model.entities.User
-keep class com.fwp.deloittedctcustomerapp.data.model.entities.UserJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
