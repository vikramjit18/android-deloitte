-if class com.fwp.deloittedctcustomerapp.data.model.requests.ChangeFwpEmailRequest
-keepnames class com.fwp.deloittedctcustomerapp.data.model.requests.ChangeFwpEmailRequest
-if class com.fwp.deloittedctcustomerapp.data.model.requests.ChangeFwpEmailRequest
-keep class com.fwp.deloittedctcustomerapp.data.model.requests.ChangeFwpEmailRequestJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
