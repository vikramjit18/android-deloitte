-if class com.fwp.deloittedctcustomerapp.data.model.responses.als.ResObject
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.als.ResObject
-if class com.fwp.deloittedctcustomerapp.data.model.responses.als.ResObject
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.als.ResObjectJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
