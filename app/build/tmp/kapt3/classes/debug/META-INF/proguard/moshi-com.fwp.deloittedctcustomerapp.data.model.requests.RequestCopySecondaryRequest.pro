-if class com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopySecondaryRequest
-keepnames class com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopySecondaryRequest
-if class com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopySecondaryRequest
-keep class com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopySecondaryRequestJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
