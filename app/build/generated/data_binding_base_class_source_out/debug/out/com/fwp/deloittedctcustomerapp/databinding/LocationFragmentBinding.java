// Generated by view binder compiler. Do not edit!
package com.fwp.deloittedctcustomerapp.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.fwp.deloittedctcustomerapp.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class LocationFragmentBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final LayoutBackButtonBinding back;

  @NonNull
  public final TextView gotoDeviceSetting;

  @NonNull
  public final ScrollView scrollView4;

  @NonNull
  public final TextView textLocation;

  @NonNull
  public final TextView youCanAdjust;

  private LocationFragmentBinding(@NonNull ConstraintLayout rootView,
      @NonNull LayoutBackButtonBinding back, @NonNull TextView gotoDeviceSetting,
      @NonNull ScrollView scrollView4, @NonNull TextView textLocation,
      @NonNull TextView youCanAdjust) {
    this.rootView = rootView;
    this.back = back;
    this.gotoDeviceSetting = gotoDeviceSetting;
    this.scrollView4 = scrollView4;
    this.textLocation = textLocation;
    this.youCanAdjust = youCanAdjust;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static LocationFragmentBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static LocationFragmentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.location_fragment, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static LocationFragmentBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.back;
      View back = ViewBindings.findChildViewById(rootView, id);
      if (back == null) {
        break missingId;
      }
      LayoutBackButtonBinding binding_back = LayoutBackButtonBinding.bind(back);

      id = R.id.gotoDeviceSetting;
      TextView gotoDeviceSetting = ViewBindings.findChildViewById(rootView, id);
      if (gotoDeviceSetting == null) {
        break missingId;
      }

      id = R.id.scrollView4;
      ScrollView scrollView4 = ViewBindings.findChildViewById(rootView, id);
      if (scrollView4 == null) {
        break missingId;
      }

      id = R.id.textLocation;
      TextView textLocation = ViewBindings.findChildViewById(rootView, id);
      if (textLocation == null) {
        break missingId;
      }

      id = R.id.youCanAdjust;
      TextView youCanAdjust = ViewBindings.findChildViewById(rootView, id);
      if (youCanAdjust == null) {
        break missingId;
      }

      return new LocationFragmentBinding((ConstraintLayout) rootView, binding_back,
          gotoDeviceSetting, scrollView4, textLocation, youCanAdjust);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
