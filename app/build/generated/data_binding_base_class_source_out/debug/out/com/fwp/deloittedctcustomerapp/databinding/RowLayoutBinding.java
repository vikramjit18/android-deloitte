// Generated by view binder compiler. Do not edit!
package com.fwp.deloittedctcustomerapp.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.fwp.deloittedctcustomerapp.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class RowLayoutBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final ImageView itemEndImage;

  @NonNull
  public final ImageView itemImage;

  @NonNull
  public final LinearLayout itemLayout;

  @NonNull
  public final TextView itemText;

  private RowLayoutBinding(@NonNull ConstraintLayout rootView, @NonNull ImageView itemEndImage,
      @NonNull ImageView itemImage, @NonNull LinearLayout itemLayout, @NonNull TextView itemText) {
    this.rootView = rootView;
    this.itemEndImage = itemEndImage;
    this.itemImage = itemImage;
    this.itemLayout = itemLayout;
    this.itemText = itemText;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static RowLayoutBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static RowLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.row_layout, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static RowLayoutBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.item_end_image;
      ImageView itemEndImage = ViewBindings.findChildViewById(rootView, id);
      if (itemEndImage == null) {
        break missingId;
      }

      id = R.id.item_image;
      ImageView itemImage = ViewBindings.findChildViewById(rootView, id);
      if (itemImage == null) {
        break missingId;
      }

      id = R.id.item_layout;
      LinearLayout itemLayout = ViewBindings.findChildViewById(rootView, id);
      if (itemLayout == null) {
        break missingId;
      }

      id = R.id.item_text;
      TextView itemText = ViewBindings.findChildViewById(rootView, id);
      if (itemText == null) {
        break missingId;
      }

      return new RowLayoutBinding((ConstraintLayout) rootView, itemEndImage, itemImage, itemLayout,
          itemText);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
