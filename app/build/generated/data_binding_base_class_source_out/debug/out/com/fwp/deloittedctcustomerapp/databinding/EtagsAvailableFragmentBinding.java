// Generated by view binder compiler. Do not edit!
package com.fwp.deloittedctcustomerapp.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.fwp.deloittedctcustomerapp.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class EtagsAvailableFragmentBinding implements ViewBinding {
  @NonNull
  private final FrameLayout rootView;

  @NonNull
  public final BuyApplyCardLayoutBinding buyAndApply;

  @NonNull
  public final ConstraintLayout clToolTip;

  @NonNull
  public final RecyclerView currentYearRecyclerView;

  @NonNull
  public final LinearLayout currentYearRecyclerViewLayout;

  @NonNull
  public final CustomPopupBinding customPopup1;

  @NonNull
  public final ImageView downArrow;

  @NonNull
  public final ImageView downloadAll;

  @NonNull
  public final ProgressLayoutBinding loader;

  @NonNull
  public final CardView mainEtagCard;

  @NonNull
  public final ImageView more;

  @NonNull
  public final NestedScrollView nestedScrollView7;

  @NonNull
  public final NoEtagLayoutBinding noEtagAvailable;

  @NonNull
  public final NoEtagYearBinding noEtagPrevYear;

  @NonNull
  public final TextView previousYearHeading;

  @NonNull
  public final RecyclerView previousYearRecyclerView;

  @NonNull
  public final LinearLayout previousYearRecyclerViewLayout;

  @NonNull
  public final RelativeLayout rLEtagMain;

  @NonNull
  public final TextView yearHeader;

  private EtagsAvailableFragmentBinding(@NonNull FrameLayout rootView,
      @NonNull BuyApplyCardLayoutBinding buyAndApply, @NonNull ConstraintLayout clToolTip,
      @NonNull RecyclerView currentYearRecyclerView,
      @NonNull LinearLayout currentYearRecyclerViewLayout, @NonNull CustomPopupBinding customPopup1,
      @NonNull ImageView downArrow, @NonNull ImageView downloadAll,
      @NonNull ProgressLayoutBinding loader, @NonNull CardView mainEtagCard,
      @NonNull ImageView more, @NonNull NestedScrollView nestedScrollView7,
      @NonNull NoEtagLayoutBinding noEtagAvailable, @NonNull NoEtagYearBinding noEtagPrevYear,
      @NonNull TextView previousYearHeading, @NonNull RecyclerView previousYearRecyclerView,
      @NonNull LinearLayout previousYearRecyclerViewLayout, @NonNull RelativeLayout rLEtagMain,
      @NonNull TextView yearHeader) {
    this.rootView = rootView;
    this.buyAndApply = buyAndApply;
    this.clToolTip = clToolTip;
    this.currentYearRecyclerView = currentYearRecyclerView;
    this.currentYearRecyclerViewLayout = currentYearRecyclerViewLayout;
    this.customPopup1 = customPopup1;
    this.downArrow = downArrow;
    this.downloadAll = downloadAll;
    this.loader = loader;
    this.mainEtagCard = mainEtagCard;
    this.more = more;
    this.nestedScrollView7 = nestedScrollView7;
    this.noEtagAvailable = noEtagAvailable;
    this.noEtagPrevYear = noEtagPrevYear;
    this.previousYearHeading = previousYearHeading;
    this.previousYearRecyclerView = previousYearRecyclerView;
    this.previousYearRecyclerViewLayout = previousYearRecyclerViewLayout;
    this.rLEtagMain = rLEtagMain;
    this.yearHeader = yearHeader;
  }

  @Override
  @NonNull
  public FrameLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static EtagsAvailableFragmentBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static EtagsAvailableFragmentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.etags_available_fragment, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static EtagsAvailableFragmentBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.buy_and_apply;
      View buyAndApply = ViewBindings.findChildViewById(rootView, id);
      if (buyAndApply == null) {
        break missingId;
      }
      BuyApplyCardLayoutBinding binding_buyAndApply = BuyApplyCardLayoutBinding.bind(buyAndApply);

      id = R.id.cl_tool_tip;
      ConstraintLayout clToolTip = ViewBindings.findChildViewById(rootView, id);
      if (clToolTip == null) {
        break missingId;
      }

      id = R.id.current_year_recycler_view;
      RecyclerView currentYearRecyclerView = ViewBindings.findChildViewById(rootView, id);
      if (currentYearRecyclerView == null) {
        break missingId;
      }

      id = R.id.current_year_recycler_view_layout;
      LinearLayout currentYearRecyclerViewLayout = ViewBindings.findChildViewById(rootView, id);
      if (currentYearRecyclerViewLayout == null) {
        break missingId;
      }

      id = R.id.custom_popup1;
      View customPopup1 = ViewBindings.findChildViewById(rootView, id);
      if (customPopup1 == null) {
        break missingId;
      }
      CustomPopupBinding binding_customPopup1 = CustomPopupBinding.bind(customPopup1);

      id = R.id.down_arrow;
      ImageView downArrow = ViewBindings.findChildViewById(rootView, id);
      if (downArrow == null) {
        break missingId;
      }

      id = R.id.downloadAll;
      ImageView downloadAll = ViewBindings.findChildViewById(rootView, id);
      if (downloadAll == null) {
        break missingId;
      }

      id = R.id.loader;
      View loader = ViewBindings.findChildViewById(rootView, id);
      if (loader == null) {
        break missingId;
      }
      ProgressLayoutBinding binding_loader = ProgressLayoutBinding.bind(loader);

      id = R.id.main_etag_card;
      CardView mainEtagCard = ViewBindings.findChildViewById(rootView, id);
      if (mainEtagCard == null) {
        break missingId;
      }

      id = R.id.more;
      ImageView more = ViewBindings.findChildViewById(rootView, id);
      if (more == null) {
        break missingId;
      }

      id = R.id.nestedScrollView7;
      NestedScrollView nestedScrollView7 = ViewBindings.findChildViewById(rootView, id);
      if (nestedScrollView7 == null) {
        break missingId;
      }

      id = R.id.no_etag_available;
      View noEtagAvailable = ViewBindings.findChildViewById(rootView, id);
      if (noEtagAvailable == null) {
        break missingId;
      }
      NoEtagLayoutBinding binding_noEtagAvailable = NoEtagLayoutBinding.bind(noEtagAvailable);

      id = R.id.no_etag_prev_year;
      View noEtagPrevYear = ViewBindings.findChildViewById(rootView, id);
      if (noEtagPrevYear == null) {
        break missingId;
      }
      NoEtagYearBinding binding_noEtagPrevYear = NoEtagYearBinding.bind(noEtagPrevYear);

      id = R.id.previous_year_heading;
      TextView previousYearHeading = ViewBindings.findChildViewById(rootView, id);
      if (previousYearHeading == null) {
        break missingId;
      }

      id = R.id.previous_year_recycler_view;
      RecyclerView previousYearRecyclerView = ViewBindings.findChildViewById(rootView, id);
      if (previousYearRecyclerView == null) {
        break missingId;
      }

      id = R.id.previous_year_recycler_view_layout;
      LinearLayout previousYearRecyclerViewLayout = ViewBindings.findChildViewById(rootView, id);
      if (previousYearRecyclerViewLayout == null) {
        break missingId;
      }

      id = R.id.r_l_etag_main;
      RelativeLayout rLEtagMain = ViewBindings.findChildViewById(rootView, id);
      if (rLEtagMain == null) {
        break missingId;
      }

      id = R.id.yearHeader;
      TextView yearHeader = ViewBindings.findChildViewById(rootView, id);
      if (yearHeader == null) {
        break missingId;
      }

      return new EtagsAvailableFragmentBinding((FrameLayout) rootView, binding_buyAndApply,
          clToolTip, currentYearRecyclerView, currentYearRecyclerViewLayout, binding_customPopup1,
          downArrow, downloadAll, binding_loader, mainEtagCard, more, nestedScrollView7,
          binding_noEtagAvailable, binding_noEtagPrevYear, previousYearHeading,
          previousYearRecyclerView, previousYearRecyclerViewLayout, rLEtagMain, yearHeader);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
