// Generated by view binder compiler. Do not edit!
package com.fwp.deloittedctcustomerapp.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.fwp.deloittedctcustomerapp.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ResetPasswordFragmentBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final LayoutBackButtonBinding back;

  @NonNull
  public final ImageView checkboxLeastCharacter;

  @NonNull
  public final ImageView checkboxNumberCharacter;

  @NonNull
  public final ConstraintLayout clMain;

  @NonNull
  public final ConstraintLayout constraintLayout12;

  @NonNull
  public final ConstraintLayout constraintLayout3;

  @NonNull
  public final ConstraintLayout currentPassLayout;

  @NonNull
  public final Button forgotPass;

  @NonNull
  public final HorizontalLineBinding horizontalLine2;

  @NonNull
  public final TextView includeNo;

  @NonNull
  public final NestedScrollView nestedScrollView4;

  @NonNull
  public final LayoutOflineNotifyBinding network;

  @NonNull
  public final ConstraintLayout newConfirmPassLayout;

  @NonNull
  public final TextView newConfirmPassText;

  @NonNull
  public final ConstraintLayout newPassLayout;

  @NonNull
  public final TextView newPassText;

  @NonNull
  public final TextInputEditText passCurrent;

  @NonNull
  public final TextView passCurrentText;

  @NonNull
  public final TextInputEditText passNew;

  @NonNull
  public final TextInputEditText passNewConfirm;

  @NonNull
  public final TextView passTitle;

  @NonNull
  public final TextInputLayout passwordCurrentTextLayout;

  @NonNull
  public final TextInputLayout passwordNewConfirmTextLayout;

  @NonNull
  public final TextInputLayout passwordNewTextLayout;

  @NonNull
  public final ConstraintLayout passwordRequirement;

  @NonNull
  public final TextView reqText;

  @NonNull
  public final LayoutButtonBinding saveNewPass;

  @NonNull
  public final TextView textView7;

  private ResetPasswordFragmentBinding(@NonNull ConstraintLayout rootView,
      @NonNull LayoutBackButtonBinding back, @NonNull ImageView checkboxLeastCharacter,
      @NonNull ImageView checkboxNumberCharacter, @NonNull ConstraintLayout clMain,
      @NonNull ConstraintLayout constraintLayout12, @NonNull ConstraintLayout constraintLayout3,
      @NonNull ConstraintLayout currentPassLayout, @NonNull Button forgotPass,
      @NonNull HorizontalLineBinding horizontalLine2, @NonNull TextView includeNo,
      @NonNull NestedScrollView nestedScrollView4, @NonNull LayoutOflineNotifyBinding network,
      @NonNull ConstraintLayout newConfirmPassLayout, @NonNull TextView newConfirmPassText,
      @NonNull ConstraintLayout newPassLayout, @NonNull TextView newPassText,
      @NonNull TextInputEditText passCurrent, @NonNull TextView passCurrentText,
      @NonNull TextInputEditText passNew, @NonNull TextInputEditText passNewConfirm,
      @NonNull TextView passTitle, @NonNull TextInputLayout passwordCurrentTextLayout,
      @NonNull TextInputLayout passwordNewConfirmTextLayout,
      @NonNull TextInputLayout passwordNewTextLayout, @NonNull ConstraintLayout passwordRequirement,
      @NonNull TextView reqText, @NonNull LayoutButtonBinding saveNewPass,
      @NonNull TextView textView7) {
    this.rootView = rootView;
    this.back = back;
    this.checkboxLeastCharacter = checkboxLeastCharacter;
    this.checkboxNumberCharacter = checkboxNumberCharacter;
    this.clMain = clMain;
    this.constraintLayout12 = constraintLayout12;
    this.constraintLayout3 = constraintLayout3;
    this.currentPassLayout = currentPassLayout;
    this.forgotPass = forgotPass;
    this.horizontalLine2 = horizontalLine2;
    this.includeNo = includeNo;
    this.nestedScrollView4 = nestedScrollView4;
    this.network = network;
    this.newConfirmPassLayout = newConfirmPassLayout;
    this.newConfirmPassText = newConfirmPassText;
    this.newPassLayout = newPassLayout;
    this.newPassText = newPassText;
    this.passCurrent = passCurrent;
    this.passCurrentText = passCurrentText;
    this.passNew = passNew;
    this.passNewConfirm = passNewConfirm;
    this.passTitle = passTitle;
    this.passwordCurrentTextLayout = passwordCurrentTextLayout;
    this.passwordNewConfirmTextLayout = passwordNewConfirmTextLayout;
    this.passwordNewTextLayout = passwordNewTextLayout;
    this.passwordRequirement = passwordRequirement;
    this.reqText = reqText;
    this.saveNewPass = saveNewPass;
    this.textView7 = textView7;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ResetPasswordFragmentBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ResetPasswordFragmentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.reset_password_fragment, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ResetPasswordFragmentBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.back;
      View back = ViewBindings.findChildViewById(rootView, id);
      if (back == null) {
        break missingId;
      }
      LayoutBackButtonBinding binding_back = LayoutBackButtonBinding.bind(back);

      id = R.id.checkbox_least_character;
      ImageView checkboxLeastCharacter = ViewBindings.findChildViewById(rootView, id);
      if (checkboxLeastCharacter == null) {
        break missingId;
      }

      id = R.id.checkbox_number_character;
      ImageView checkboxNumberCharacter = ViewBindings.findChildViewById(rootView, id);
      if (checkboxNumberCharacter == null) {
        break missingId;
      }

      ConstraintLayout clMain = (ConstraintLayout) rootView;

      id = R.id.constraintLayout12;
      ConstraintLayout constraintLayout12 = ViewBindings.findChildViewById(rootView, id);
      if (constraintLayout12 == null) {
        break missingId;
      }

      id = R.id.constraintLayout3;
      ConstraintLayout constraintLayout3 = ViewBindings.findChildViewById(rootView, id);
      if (constraintLayout3 == null) {
        break missingId;
      }

      id = R.id.currentPassLayout;
      ConstraintLayout currentPassLayout = ViewBindings.findChildViewById(rootView, id);
      if (currentPassLayout == null) {
        break missingId;
      }

      id = R.id.forgotPass;
      Button forgotPass = ViewBindings.findChildViewById(rootView, id);
      if (forgotPass == null) {
        break missingId;
      }

      id = R.id.horizontal_line2;
      View horizontalLine2 = ViewBindings.findChildViewById(rootView, id);
      if (horizontalLine2 == null) {
        break missingId;
      }
      HorizontalLineBinding binding_horizontalLine2 = HorizontalLineBinding.bind(horizontalLine2);

      id = R.id.includeNo;
      TextView includeNo = ViewBindings.findChildViewById(rootView, id);
      if (includeNo == null) {
        break missingId;
      }

      id = R.id.nestedScrollView4;
      NestedScrollView nestedScrollView4 = ViewBindings.findChildViewById(rootView, id);
      if (nestedScrollView4 == null) {
        break missingId;
      }

      id = R.id.network;
      View network = ViewBindings.findChildViewById(rootView, id);
      if (network == null) {
        break missingId;
      }
      LayoutOflineNotifyBinding binding_network = LayoutOflineNotifyBinding.bind(network);

      id = R.id.newConfirmPassLayout;
      ConstraintLayout newConfirmPassLayout = ViewBindings.findChildViewById(rootView, id);
      if (newConfirmPassLayout == null) {
        break missingId;
      }

      id = R.id.newConfirmPassText;
      TextView newConfirmPassText = ViewBindings.findChildViewById(rootView, id);
      if (newConfirmPassText == null) {
        break missingId;
      }

      id = R.id.newPassLayout;
      ConstraintLayout newPassLayout = ViewBindings.findChildViewById(rootView, id);
      if (newPassLayout == null) {
        break missingId;
      }

      id = R.id.newPassText;
      TextView newPassText = ViewBindings.findChildViewById(rootView, id);
      if (newPassText == null) {
        break missingId;
      }

      id = R.id.passCurrent;
      TextInputEditText passCurrent = ViewBindings.findChildViewById(rootView, id);
      if (passCurrent == null) {
        break missingId;
      }

      id = R.id.passCurrentText;
      TextView passCurrentText = ViewBindings.findChildViewById(rootView, id);
      if (passCurrentText == null) {
        break missingId;
      }

      id = R.id.passNew;
      TextInputEditText passNew = ViewBindings.findChildViewById(rootView, id);
      if (passNew == null) {
        break missingId;
      }

      id = R.id.passNewConfirm;
      TextInputEditText passNewConfirm = ViewBindings.findChildViewById(rootView, id);
      if (passNewConfirm == null) {
        break missingId;
      }

      id = R.id.passTitle;
      TextView passTitle = ViewBindings.findChildViewById(rootView, id);
      if (passTitle == null) {
        break missingId;
      }

      id = R.id.passwordCurrentTextLayout;
      TextInputLayout passwordCurrentTextLayout = ViewBindings.findChildViewById(rootView, id);
      if (passwordCurrentTextLayout == null) {
        break missingId;
      }

      id = R.id.passwordNewConfirmTextLayout;
      TextInputLayout passwordNewConfirmTextLayout = ViewBindings.findChildViewById(rootView, id);
      if (passwordNewConfirmTextLayout == null) {
        break missingId;
      }

      id = R.id.passwordNewTextLayout;
      TextInputLayout passwordNewTextLayout = ViewBindings.findChildViewById(rootView, id);
      if (passwordNewTextLayout == null) {
        break missingId;
      }

      id = R.id.passwordRequirement;
      ConstraintLayout passwordRequirement = ViewBindings.findChildViewById(rootView, id);
      if (passwordRequirement == null) {
        break missingId;
      }

      id = R.id.reqText;
      TextView reqText = ViewBindings.findChildViewById(rootView, id);
      if (reqText == null) {
        break missingId;
      }

      id = R.id.saveNewPass;
      View saveNewPass = ViewBindings.findChildViewById(rootView, id);
      if (saveNewPass == null) {
        break missingId;
      }
      LayoutButtonBinding binding_saveNewPass = LayoutButtonBinding.bind(saveNewPass);

      id = R.id.textView7;
      TextView textView7 = ViewBindings.findChildViewById(rootView, id);
      if (textView7 == null) {
        break missingId;
      }

      return new ResetPasswordFragmentBinding((ConstraintLayout) rootView, binding_back,
          checkboxLeastCharacter, checkboxNumberCharacter, clMain, constraintLayout12,
          constraintLayout3, currentPassLayout, forgotPass, binding_horizontalLine2, includeNo,
          nestedScrollView4, binding_network, newConfirmPassLayout, newConfirmPassText,
          newPassLayout, newPassText, passCurrent, passCurrentText, passNew, passNewConfirm,
          passTitle, passwordCurrentTextLayout, passwordNewConfirmTextLayout, passwordNewTextLayout,
          passwordRequirement, reqText, binding_saveNewPass, textView7);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
