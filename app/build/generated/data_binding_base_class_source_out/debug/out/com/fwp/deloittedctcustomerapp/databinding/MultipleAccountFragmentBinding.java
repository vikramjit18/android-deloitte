// Generated by view binder compiler. Do not edit!
package com.fwp.deloittedctcustomerapp.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.fwp.deloittedctcustomerapp.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class MultipleAccountFragmentBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final LayoutBackButtonBinding back;

  @NonNull
  public final TextView closeMy;

  @NonNull
  public final HorizontalLineBinding horizontalLine1;

  @NonNull
  public final TextView ifYouLooking;

  @NonNull
  public final TextView linked;

  @NonNull
  public final TextView multiALsNo;

  @NonNull
  public final TextView multiName;

  @NonNull
  public final ImageView multiProPic;

  @NonNull
  public final ConstraintLayout multiProfileCard;

  @NonNull
  public final TextView myFwpGoto;

  @NonNull
  public final TextView resident;

  @NonNull
  public final ScrollView scrollView4;

  @NonNull
  public final ConstraintLayout unlinkContainer;

  private MultipleAccountFragmentBinding(@NonNull ConstraintLayout rootView,
      @NonNull LayoutBackButtonBinding back, @NonNull TextView closeMy,
      @NonNull HorizontalLineBinding horizontalLine1, @NonNull TextView ifYouLooking,
      @NonNull TextView linked, @NonNull TextView multiALsNo, @NonNull TextView multiName,
      @NonNull ImageView multiProPic, @NonNull ConstraintLayout multiProfileCard,
      @NonNull TextView myFwpGoto, @NonNull TextView resident, @NonNull ScrollView scrollView4,
      @NonNull ConstraintLayout unlinkContainer) {
    this.rootView = rootView;
    this.back = back;
    this.closeMy = closeMy;
    this.horizontalLine1 = horizontalLine1;
    this.ifYouLooking = ifYouLooking;
    this.linked = linked;
    this.multiALsNo = multiALsNo;
    this.multiName = multiName;
    this.multiProPic = multiProPic;
    this.multiProfileCard = multiProfileCard;
    this.myFwpGoto = myFwpGoto;
    this.resident = resident;
    this.scrollView4 = scrollView4;
    this.unlinkContainer = unlinkContainer;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static MultipleAccountFragmentBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static MultipleAccountFragmentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.multiple_account_fragment, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static MultipleAccountFragmentBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.back;
      View back = ViewBindings.findChildViewById(rootView, id);
      if (back == null) {
        break missingId;
      }
      LayoutBackButtonBinding binding_back = LayoutBackButtonBinding.bind(back);

      id = R.id.closeMy;
      TextView closeMy = ViewBindings.findChildViewById(rootView, id);
      if (closeMy == null) {
        break missingId;
      }

      id = R.id.horizontal_line1;
      View horizontalLine1 = ViewBindings.findChildViewById(rootView, id);
      if (horizontalLine1 == null) {
        break missingId;
      }
      HorizontalLineBinding binding_horizontalLine1 = HorizontalLineBinding.bind(horizontalLine1);

      id = R.id.ifYouLooking;
      TextView ifYouLooking = ViewBindings.findChildViewById(rootView, id);
      if (ifYouLooking == null) {
        break missingId;
      }

      id = R.id.linked;
      TextView linked = ViewBindings.findChildViewById(rootView, id);
      if (linked == null) {
        break missingId;
      }

      id = R.id.multiALsNo;
      TextView multiALsNo = ViewBindings.findChildViewById(rootView, id);
      if (multiALsNo == null) {
        break missingId;
      }

      id = R.id.multiName;
      TextView multiName = ViewBindings.findChildViewById(rootView, id);
      if (multiName == null) {
        break missingId;
      }

      id = R.id.multiProPic;
      ImageView multiProPic = ViewBindings.findChildViewById(rootView, id);
      if (multiProPic == null) {
        break missingId;
      }

      id = R.id.multiProfileCard;
      ConstraintLayout multiProfileCard = ViewBindings.findChildViewById(rootView, id);
      if (multiProfileCard == null) {
        break missingId;
      }

      id = R.id.myFwpGoto;
      TextView myFwpGoto = ViewBindings.findChildViewById(rootView, id);
      if (myFwpGoto == null) {
        break missingId;
      }

      id = R.id.resident;
      TextView resident = ViewBindings.findChildViewById(rootView, id);
      if (resident == null) {
        break missingId;
      }

      id = R.id.scrollView4;
      ScrollView scrollView4 = ViewBindings.findChildViewById(rootView, id);
      if (scrollView4 == null) {
        break missingId;
      }

      id = R.id.unlinkContainer;
      ConstraintLayout unlinkContainer = ViewBindings.findChildViewById(rootView, id);
      if (unlinkContainer == null) {
        break missingId;
      }

      return new MultipleAccountFragmentBinding((ConstraintLayout) rootView, binding_back, closeMy,
          binding_horizontalLine1, ifYouLooking, linked, multiALsNo, multiName, multiProPic,
          multiProfileCard, myFwpGoto, resident, scrollView4, unlinkContainer);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
