// Generated by view binder compiler. Do not edit!
package com.fwp.deloittedctcustomerapp.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.fwp.deloittedctcustomerapp.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ItemHeldChildRowBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final LinearLayout itemContainer;

  @NonNull
  public final ImageView ivListIcon;

  @NonNull
  public final TextView tvTagLicence;

  @NonNull
  public final TextView year;

  private ItemHeldChildRowBinding(@NonNull ConstraintLayout rootView,
      @NonNull LinearLayout itemContainer, @NonNull ImageView ivListIcon,
      @NonNull TextView tvTagLicence, @NonNull TextView year) {
    this.rootView = rootView;
    this.itemContainer = itemContainer;
    this.ivListIcon = ivListIcon;
    this.tvTagLicence = tvTagLicence;
    this.year = year;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ItemHeldChildRowBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ItemHeldChildRowBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.item_held_child_row, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ItemHeldChildRowBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.itemContainer;
      LinearLayout itemContainer = ViewBindings.findChildViewById(rootView, id);
      if (itemContainer == null) {
        break missingId;
      }

      id = R.id.iv_list_icon;
      ImageView ivListIcon = ViewBindings.findChildViewById(rootView, id);
      if (ivListIcon == null) {
        break missingId;
      }

      id = R.id.tv_tag_licence;
      TextView tvTagLicence = ViewBindings.findChildViewById(rootView, id);
      if (tvTagLicence == null) {
        break missingId;
      }

      id = R.id.year;
      TextView year = ViewBindings.findChildViewById(rootView, id);
      if (year == null) {
        break missingId;
      }

      return new ItemHeldChildRowBinding((ConstraintLayout) rootView, itemContainer, ivListIcon,
          tvTagLicence, year);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
