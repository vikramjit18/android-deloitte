package dagger.hilt.internal.processedrootsentinel.codegen;

import dagger.hilt.internal.processedrootsentinel.ProcessedRootSentinel;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@ProcessedRootSentinel(
    roots = "com.fwp.deloittedctcustomerapp.data.hilt.BaseApplication"
)
public class _com_fwp_deloittedctcustomerapp_data_hilt_BaseApplication {
}
