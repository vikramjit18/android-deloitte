// Code generated by moshi-kotlin-codegen. Do not edit.
@file:Suppress("DEPRECATION", "unused", "ClassName", "REDUNDANT_PROJECTION",
    "RedundantExplicitType", "LocalVariableName", "RedundantVisibilityModifier",
    "PLATFORM_CLASS_MAPPED_TO_KOTLIN", "IMPLICIT_NOTHING_TYPE_ARGUMENT_IN_RETURN_POSITION")

package com.fwp.deloittedctcustomerapp.`data`.model.requests

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.`internal`.Util
import java.lang.NullPointerException
import kotlin.String
import kotlin.Suppress
import kotlin.Unit
import kotlin.collections.emptySet
import kotlin.text.buildString

public class LoginRequestJsonAdapter(
  moshi: Moshi
) : JsonAdapter<LoginRequest>() {
  private val options: JsonReader.Options = JsonReader.Options.of("password", "username")

  private val stringAdapter: JsonAdapter<String> = moshi.adapter(String::class.java, emptySet(),
      "password")

  public override fun toString(): String = buildString(34) {
      append("GeneratedJsonAdapter(").append("LoginRequest").append(')') }

  public override fun fromJson(reader: JsonReader): LoginRequest {
    var password: String? = null
    var username: String? = null
    reader.beginObject()
    while (reader.hasNext()) {
      when (reader.selectName(options)) {
        0 -> password = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("password",
            "password", reader)
        1 -> username = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("username",
            "username", reader)
        -1 -> {
          // Unknown name, skip it.
          reader.skipName()
          reader.skipValue()
        }
      }
    }
    reader.endObject()
    return LoginRequest(
        password = password ?: throw Util.missingProperty("password", "password", reader),
        username = username ?: throw Util.missingProperty("username", "username", reader)
    )
  }

  public override fun toJson(writer: JsonWriter, value_: LoginRequest?): Unit {
    if (value_ == null) {
      throw NullPointerException("value_ was null! Wrap in .nullSafe() to write nullable values.")
    }
    writer.beginObject()
    writer.name("password")
    stringAdapter.toJson(writer, value_.password)
    writer.name("username")
    stringAdapter.toJson(writer, value_.username)
    writer.endObject()
  }
}
