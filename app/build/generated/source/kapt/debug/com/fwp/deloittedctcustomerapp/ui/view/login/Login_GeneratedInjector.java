package com.fwp.deloittedctcustomerapp.ui.view.login;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = Login.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface Login_GeneratedInjector {
  void injectLogin(Login login);
}
