package com.fwp.deloittedctcustomerapp.ui.view.als.lookup;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = LearnMoreLookUpFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface LearnMoreLookUpFragment_GeneratedInjector {
  void injectLearnMoreLookUpFragment(LearnMoreLookUpFragment learnMoreLookUpFragment);
}
