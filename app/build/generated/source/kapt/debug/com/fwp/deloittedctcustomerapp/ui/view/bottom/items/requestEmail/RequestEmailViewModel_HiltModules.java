package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityRetainedComponent;
import dagger.hilt.android.components.ViewModelComponent;
import dagger.hilt.android.internal.lifecycle.HiltViewModelMap;
import dagger.hilt.codegen.OriginatingElement;
import dagger.multibindings.IntoMap;
import dagger.multibindings.IntoSet;
import dagger.multibindings.StringKey;
import java.lang.String;

@OriginatingElement(
    topLevelClass = RequestEmailViewModel.class
)
public final class RequestEmailViewModel_HiltModules {
  private RequestEmailViewModel_HiltModules() {
  }

  @Module
  @InstallIn(ViewModelComponent.class)
  public abstract static class BindsModule {
    private BindsModule() {
    }

    @Binds
    @IntoMap
    @StringKey("com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailViewModel")
    @HiltViewModelMap
    public abstract ViewModel binds(RequestEmailViewModel vm);
  }

  @Module
  @InstallIn(ActivityRetainedComponent.class)
  public static final class KeyModule {
    private KeyModule() {
    }

    @Provides
    @IntoSet
    @HiltViewModelMap.KeySet
    public static String provide() {
      return "com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailViewModel";
    }
  }
}
