package com.fwp.deloittedctcustomerapp.databinding;
import com.fwp.deloittedctcustomerapp.R;
import com.fwp.deloittedctcustomerapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LinkedItemsFragmentBindingImpl extends LinkedItemsFragmentBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(19);
        sIncludes.setIncludes(1, 
            new String[] {"layout_no_account_selected"},
            new int[] {9},
            new int[] {com.fwp.deloittedctcustomerapp.R.layout.layout_no_account_selected});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.no_item_available, 4);
        sViewsWithIds.put(R.id.license_current_year, 5);
        sViewsWithIds.put(R.id.buy_and_apply, 6);
        sViewsWithIds.put(R.id.no_item_prev_year, 7);
        sViewsWithIds.put(R.id.license_prev_year, 8);
        sViewsWithIds.put(R.id.nestedScrollView7, 10);
        sViewsWithIds.put(R.id.dropdown1, 11);
        sViewsWithIds.put(R.id.spinner1, 12);
        sViewsWithIds.put(R.id.rl_main, 13);
        sViewsWithIds.put(R.id.yearHeader, 14);
        sViewsWithIds.put(R.id.more, 15);
        sViewsWithIds.put(R.id.current_year_recycler_view, 16);
        sViewsWithIds.put(R.id.previous_year_heading, 17);
        sViewsWithIds.put(R.id.previous_year_recycler_view, 18);
    }
    // views
    @NonNull
    private final android.widget.FrameLayout mboundView0;
    @NonNull
    private final android.widget.RelativeLayout mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LinkedItemsFragmentBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 19, sIncludes, sViewsWithIds));
    }
    private LinkedItemsFragmentBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (bindings[6] != null) ? com.fwp.deloittedctcustomerapp.databinding.BuyApplyCardLayoutBinding.bind((android.view.View) bindings[6]) : null
            , (androidx.recyclerview.widget.RecyclerView) bindings[16]
            , (android.widget.LinearLayout) bindings[2]
            , (android.widget.LinearLayout) bindings[11]
            , (com.fwp.deloittedctcustomerapp.databinding.LayoutNoAccountSelectedBinding) bindings[9]
            , (bindings[5] != null) ? com.fwp.deloittedctcustomerapp.databinding.ItemHeldChildRowBinding.bind((android.view.View) bindings[5]) : null
            , (bindings[8] != null) ? com.fwp.deloittedctcustomerapp.databinding.ItemHeldChildRowBinding.bind((android.view.View) bindings[8]) : null
            , (android.widget.ImageView) bindings[15]
            , (androidx.core.widget.NestedScrollView) bindings[10]
            , (bindings[4] != null) ? com.fwp.deloittedctcustomerapp.databinding.NoEtagLayoutBinding.bind((android.view.View) bindings[4]) : null
            , (bindings[7] != null) ? com.fwp.deloittedctcustomerapp.databinding.NoEtagYearBinding.bind((android.view.View) bindings[7]) : null
            , (android.widget.TextView) bindings[17]
            , (androidx.recyclerview.widget.RecyclerView) bindings[18]
            , (android.widget.LinearLayout) bindings[3]
            , (android.widget.RelativeLayout) bindings[13]
            , (android.widget.Spinner) bindings[12]
            , (android.widget.TextView) bindings[14]
            );
        this.currentYearRecyclerViewLayout.setTag(null);
        setContainedBinding(this.includeNoAccountView);
        this.mboundView0 = (android.widget.FrameLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.RelativeLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.previousYearRecyclerViewLayout.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        includeNoAccountView.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (includeNoAccountView.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        includeNoAccountView.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeIncludeNoAccountView((com.fwp.deloittedctcustomerapp.databinding.LayoutNoAccountSelectedBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeIncludeNoAccountView(com.fwp.deloittedctcustomerapp.databinding.LayoutNoAccountSelectedBinding IncludeNoAccountView, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
        executeBindingsOn(includeNoAccountView);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): includeNoAccountView
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}