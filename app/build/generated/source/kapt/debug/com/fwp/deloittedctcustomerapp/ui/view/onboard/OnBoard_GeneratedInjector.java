package com.fwp.deloittedctcustomerapp.ui.view.onboard;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = OnBoard.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface OnBoard_GeneratedInjector {
  void injectOnBoard(OnBoard onBoard);
}
