package com.fwp.deloittedctcustomerapp.data.hilt;

import androidx.hilt.lifecycle.ViewModelFactoryModules;
import com.fwp.deloittedctcustomerapp.ui.view.MainActivity_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.als.AlsLinkingActivity_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.als.confirmAls.ConfirmAlsFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.als.linking.LinkingFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.als.lookup.LearnMoreLookUpFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.als.lookup.LookUpAlsFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.als.viewModel.AlsViewModel_HiltModules;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.LandingScreenActivity_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.common.AlsNotLinked_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.EtagsFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.MultiUserEtagAvailableFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.EtagsAvailableFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.ReviewHarvestFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.ValidateTagsActivity_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel_HiltModules;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.sponsors.ViewAllSponsorsFrag_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView.TagLicenceActivity_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView.TagPermitActivity_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.multiple.viewPager.LinkedItemsFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.ItemsHeldAvailableFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailActivity_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailViewModel_HiltModules;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.ProfileFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.ProfileViewModel_HiltModules;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo.ChangeLoginInfoFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo.ChangeLoginInfoViewModel_HiltModules;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password.ResetPasswordFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password.ResetPasswordViewModel_HiltModules;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.PrimaryAccountFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.PrimaryAccountViewModel_HiltModules;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.editprimary.EditPrimaryFragment_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel_HiltModules;
import com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotActivity_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotInitial_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotViewModel_HiltModules;
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginActivity_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginAgreement_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginViewModel_HiltModules;
import com.fwp.deloittedctcustomerapp.ui.view.login.Login_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.onboard.OnBoard_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.resetPass.ForgotReset_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.resetPass.ResetPassActivity_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpActivate_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpActivity_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpAgreement_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpDetails_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpInitial_GeneratedInjector;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpViewModel_HiltModules;
import dagger.Binds;
import dagger.Component;
import dagger.Module;
import dagger.Subcomponent;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.android.components.ActivityRetainedComponent;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.android.components.ServiceComponent;
import dagger.hilt.android.components.ViewComponent;
import dagger.hilt.android.components.ViewModelComponent;
import dagger.hilt.android.components.ViewWithFragmentComponent;
import dagger.hilt.android.internal.builders.ActivityComponentBuilder;
import dagger.hilt.android.internal.builders.ActivityRetainedComponentBuilder;
import dagger.hilt.android.internal.builders.FragmentComponentBuilder;
import dagger.hilt.android.internal.builders.ServiceComponentBuilder;
import dagger.hilt.android.internal.builders.ViewComponentBuilder;
import dagger.hilt.android.internal.builders.ViewModelComponentBuilder;
import dagger.hilt.android.internal.builders.ViewWithFragmentComponentBuilder;
import dagger.hilt.android.internal.lifecycle.DefaultViewModelFactories;
import dagger.hilt.android.internal.lifecycle.HiltViewModelFactory;
import dagger.hilt.android.internal.lifecycle.HiltWrapper_DefaultViewModelFactories_ActivityModule;
import dagger.hilt.android.internal.lifecycle.HiltWrapper_HiltViewModelFactory_ActivityCreatorEntryPoint;
import dagger.hilt.android.internal.lifecycle.HiltWrapper_HiltViewModelFactory_ViewModelModule;
import dagger.hilt.android.internal.managers.ActivityComponentManager;
import dagger.hilt.android.internal.managers.FragmentComponentManager;
import dagger.hilt.android.internal.managers.HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedComponentBuilderEntryPoint;
import dagger.hilt.android.internal.managers.HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedLifecycleEntryPoint;
import dagger.hilt.android.internal.managers.HiltWrapper_ActivityRetainedComponentManager_LifecycleModule;
import dagger.hilt.android.internal.managers.ServiceComponentManager;
import dagger.hilt.android.internal.managers.ViewComponentManager;
import dagger.hilt.android.internal.modules.ApplicationContextModule;
import dagger.hilt.android.internal.modules.HiltWrapper_ActivityModule;
import dagger.hilt.android.scopes.ActivityRetainedScoped;
import dagger.hilt.android.scopes.ActivityScoped;
import dagger.hilt.android.scopes.FragmentScoped;
import dagger.hilt.android.scopes.ServiceScoped;
import dagger.hilt.android.scopes.ViewModelScoped;
import dagger.hilt.android.scopes.ViewScoped;
import dagger.hilt.components.SingletonComponent;
import dagger.hilt.internal.GeneratedComponent;
import dagger.hilt.migration.DisableInstallInCheck;
import javax.inject.Singleton;

public final class BaseApplication_HiltComponents {
  private BaseApplication_HiltComponents() {
  }

  @Module(
      subcomponents = ServiceC.class
  )
  @DisableInstallInCheck
  abstract interface ServiceCBuilderModule {
    @Binds
    ServiceComponentBuilder bind(ServiceC.Builder builder);
  }

  @Module(
      subcomponents = ActivityRetainedC.class
  )
  @DisableInstallInCheck
  abstract interface ActivityRetainedCBuilderModule {
    @Binds
    ActivityRetainedComponentBuilder bind(ActivityRetainedC.Builder builder);
  }

  @Module(
      subcomponents = ActivityC.class
  )
  @DisableInstallInCheck
  abstract interface ActivityCBuilderModule {
    @Binds
    ActivityComponentBuilder bind(ActivityC.Builder builder);
  }

  @Module(
      subcomponents = ViewModelC.class
  )
  @DisableInstallInCheck
  abstract interface ViewModelCBuilderModule {
    @Binds
    ViewModelComponentBuilder bind(ViewModelC.Builder builder);
  }

  @Module(
      subcomponents = ViewC.class
  )
  @DisableInstallInCheck
  abstract interface ViewCBuilderModule {
    @Binds
    ViewComponentBuilder bind(ViewC.Builder builder);
  }

  @Module(
      subcomponents = FragmentC.class
  )
  @DisableInstallInCheck
  abstract interface FragmentCBuilderModule {
    @Binds
    FragmentComponentBuilder bind(FragmentC.Builder builder);
  }

  @Module(
      subcomponents = ViewWithFragmentC.class
  )
  @DisableInstallInCheck
  abstract interface ViewWithFragmentCBuilderModule {
    @Binds
    ViewWithFragmentComponentBuilder bind(ViewWithFragmentC.Builder builder);
  }

  @Component(
      modules = {
          ApplicationContextModule.class,
          ActivityRetainedCBuilderModule.class,
          ServiceCBuilderModule.class,
          NetworkModule.class
      }
  )
  @Singleton
  public abstract static class SingletonC implements BaseApplication_GeneratedInjector,
      HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedComponentBuilderEntryPoint,
      ServiceComponentManager.ServiceComponentBuilderEntryPoint,
      SingletonComponent,
      GeneratedComponent {
  }

  @Subcomponent
  @ServiceScoped
  public abstract static class ServiceC implements ServiceComponent,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ServiceComponentBuilder {
    }
  }

  @Subcomponent(
      modules = {
          AlsViewModel_HiltModules.KeyModule.class,
          ActivityCBuilderModule.class,
          ViewModelCBuilderModule.class,
          ChangeLoginInfoViewModel_HiltModules.KeyModule.class,
          ForgotViewModel_HiltModules.KeyModule.class,
          HiltWrapper_ActivityRetainedComponentManager_LifecycleModule.class,
          LandingScreenActivityViewModel_HiltModules.KeyModule.class,
          LoginViewModel_HiltModules.KeyModule.class,
          PrimaryAccountViewModel_HiltModules.KeyModule.class,
          ProfileViewModel_HiltModules.KeyModule.class,
          RequestEmailViewModel_HiltModules.KeyModule.class,
          ResetPasswordViewModel_HiltModules.KeyModule.class,
          SignUpViewModel_HiltModules.KeyModule.class,
          ValidateTagActivityViewModel_HiltModules.KeyModule.class
      }
  )
  @ActivityRetainedScoped
  public abstract static class ActivityRetainedC implements ActivityRetainedComponent,
      ActivityComponentManager.ActivityComponentBuilderEntryPoint,
      HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedLifecycleEntryPoint,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ActivityRetainedComponentBuilder {
    }
  }

  @Subcomponent(
      modules = {
          FragmentCBuilderModule.class,
          ViewCBuilderModule.class,
          HiltWrapper_ActivityModule.class,
          HiltWrapper_DefaultViewModelFactories_ActivityModule.class,
          ViewModelFactoryModules.ActivityModule.class
      }
  )
  @ActivityScoped
  public abstract static class ActivityC implements MainActivity_GeneratedInjector,
      AlsLinkingActivity_GeneratedInjector,
      LandingScreenActivity_GeneratedInjector,
      ValidateTagsActivity_GeneratedInjector,
      TagLicenceActivity_GeneratedInjector,
      TagPermitActivity_GeneratedInjector,
      RequestEmailActivity_GeneratedInjector,
      ForgotActivity_GeneratedInjector,
      LoginActivity_GeneratedInjector,
      ResetPassActivity_GeneratedInjector,
      SignUpActivity_GeneratedInjector,
      ActivityComponent,
      DefaultViewModelFactories.ActivityEntryPoint,
      HiltWrapper_HiltViewModelFactory_ActivityCreatorEntryPoint,
      FragmentComponentManager.FragmentComponentBuilderEntryPoint,
      ViewComponentManager.ViewComponentBuilderEntryPoint,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ActivityComponentBuilder {
    }
  }

  @Subcomponent(
      modules = {
          AlsViewModel_HiltModules.BindsModule.class,
          ChangeLoginInfoViewModel_HiltModules.BindsModule.class,
          ForgotViewModel_HiltModules.BindsModule.class,
          HiltWrapper_HiltViewModelFactory_ViewModelModule.class,
          LandingScreenActivityViewModel_HiltModules.BindsModule.class,
          LoginViewModel_HiltModules.BindsModule.class,
          PrimaryAccountViewModel_HiltModules.BindsModule.class,
          ProfileViewModel_HiltModules.BindsModule.class,
          RequestEmailViewModel_HiltModules.BindsModule.class,
          ResetPasswordViewModel_HiltModules.BindsModule.class,
          SignUpViewModel_HiltModules.BindsModule.class,
          ValidateTagActivityViewModel_HiltModules.BindsModule.class
      }
  )
  @ViewModelScoped
  public abstract static class ViewModelC implements ViewModelComponent,
      HiltViewModelFactory.ViewModelFactoriesEntryPoint,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ViewModelComponentBuilder {
    }
  }

  @Subcomponent
  @ViewScoped
  public abstract static class ViewC implements ViewComponent,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ViewComponentBuilder {
    }
  }

  @Subcomponent(
      modules = {
          ViewWithFragmentCBuilderModule.class,
          ViewModelFactoryModules.FragmentModule.class
      }
  )
  @FragmentScoped
  public abstract static class FragmentC implements ConfirmAlsFragment_GeneratedInjector,
      LinkingFragment_GeneratedInjector,
      LearnMoreLookUpFragment_GeneratedInjector,
      LookUpAlsFragment_GeneratedInjector,
      AlsNotLinked_GeneratedInjector,
      EtagsFragment_GeneratedInjector,
      MultiUserEtagAvailableFragment_GeneratedInjector,
      EtagsAvailableFragment_GeneratedInjector,
      ReviewHarvestFragment_GeneratedInjector,
      ViewAllSponsorsFrag_GeneratedInjector,
      LinkedItemsFragment_GeneratedInjector,
      ItemsHeldAvailableFragment_GeneratedInjector,
      RequestEmailFragment_GeneratedInjector,
      ProfileFragment_GeneratedInjector,
      ChangeLoginInfoFragment_GeneratedInjector,
      ResetPasswordFragment_GeneratedInjector,
      PrimaryAccountFragment_GeneratedInjector,
      EditPrimaryFragment_GeneratedInjector,
      ForgotInitial_GeneratedInjector,
      LoginAgreement_GeneratedInjector,
      Login_GeneratedInjector,
      OnBoard_GeneratedInjector,
      ForgotReset_GeneratedInjector,
      SignUpActivate_GeneratedInjector,
      SignUpAgreement_GeneratedInjector,
      SignUpDetails_GeneratedInjector,
      SignUpInitial_GeneratedInjector,
      FragmentComponent,
      DefaultViewModelFactories.FragmentEntryPoint,
      ViewComponentManager.ViewWithFragmentComponentBuilderEntryPoint,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends FragmentComponentBuilder {
    }
  }

  @Subcomponent
  @ViewScoped
  public abstract static class ViewWithFragmentC implements ViewWithFragmentComponent,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ViewWithFragmentComponentBuilder {
    }
  }
}
