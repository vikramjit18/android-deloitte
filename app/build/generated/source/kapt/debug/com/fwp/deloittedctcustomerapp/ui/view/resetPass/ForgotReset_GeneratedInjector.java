package com.fwp.deloittedctcustomerapp.ui.view.resetPass;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ForgotReset.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ForgotReset_GeneratedInjector {
  void injectForgotReset(ForgotReset forgotReset);
}
