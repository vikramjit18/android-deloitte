// Code generated by moshi-kotlin-codegen. Do not edit.
@file:Suppress("DEPRECATION", "unused", "ClassName", "REDUNDANT_PROJECTION",
    "RedundantExplicitType", "LocalVariableName", "RedundantVisibilityModifier",
    "PLATFORM_CLASS_MAPPED_TO_KOTLIN", "IMPLICIT_NOTHING_TYPE_ARGUMENT_IN_RETURN_POSITION")

package com.fwp.deloittedctcustomerapp.`data`.model.responses.itemsHeld

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.lang.NullPointerException
import kotlin.String
import kotlin.Suppress
import kotlin.Unit
import kotlin.collections.List
import kotlin.collections.emptySet
import kotlin.text.buildString

public class NonCarcassTagLicensesJsonAdapter(
  moshi: Moshi
) : JsonAdapter<NonCarcassTagLicenses>() {
  private val options: JsonReader.Options = JsonReader.Options.of("alsNo", "licenseYear",
      "nonCarcassTagLicensesList", "owner", "session", "usageDates")

  private val nullableStringAdapter: JsonAdapter<String?> = moshi.adapter(String::class.java,
      emptySet(), "alsNo")

  private val nullableListOfNonCarcassTagLicensesXAdapter:
      JsonAdapter<List<NonCarcassTagLicensesX>?> =
      moshi.adapter(Types.newParameterizedType(List::class.java,
      NonCarcassTagLicensesX::class.java), emptySet(), "nonCarcassTagLicensesList")

  public override fun toString(): String = buildString(43) {
      append("GeneratedJsonAdapter(").append("NonCarcassTagLicenses").append(')') }

  public override fun fromJson(reader: JsonReader): NonCarcassTagLicenses {
    var alsNo: String? = null
    var licenseYear: String? = null
    var nonCarcassTagLicensesList: List<NonCarcassTagLicensesX>? = null
    var owner: String? = null
    var session: String? = null
    var usageDates: String? = null
    reader.beginObject()
    while (reader.hasNext()) {
      when (reader.selectName(options)) {
        0 -> alsNo = nullableStringAdapter.fromJson(reader)
        1 -> licenseYear = nullableStringAdapter.fromJson(reader)
        2 -> nonCarcassTagLicensesList =
            nullableListOfNonCarcassTagLicensesXAdapter.fromJson(reader)
        3 -> owner = nullableStringAdapter.fromJson(reader)
        4 -> session = nullableStringAdapter.fromJson(reader)
        5 -> usageDates = nullableStringAdapter.fromJson(reader)
        -1 -> {
          // Unknown name, skip it.
          reader.skipName()
          reader.skipValue()
        }
      }
    }
    reader.endObject()
    return NonCarcassTagLicenses(
        alsNo = alsNo,
        licenseYear = licenseYear,
        nonCarcassTagLicensesList = nonCarcassTagLicensesList,
        owner = owner,
        session = session,
        usageDates = usageDates
    )
  }

  public override fun toJson(writer: JsonWriter, value_: NonCarcassTagLicenses?): Unit {
    if (value_ == null) {
      throw NullPointerException("value_ was null! Wrap in .nullSafe() to write nullable values.")
    }
    writer.beginObject()
    writer.name("alsNo")
    nullableStringAdapter.toJson(writer, value_.alsNo)
    writer.name("licenseYear")
    nullableStringAdapter.toJson(writer, value_.licenseYear)
    writer.name("nonCarcassTagLicensesList")
    nullableListOfNonCarcassTagLicensesXAdapter.toJson(writer, value_.nonCarcassTagLicensesList)
    writer.name("owner")
    nullableStringAdapter.toJson(writer, value_.owner)
    writer.name("session")
    nullableStringAdapter.toJson(writer, value_.session)
    writer.name("usageDates")
    nullableStringAdapter.toJson(writer, value_.usageDates)
    writer.endObject()
  }
}
