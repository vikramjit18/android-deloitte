package com.fwp.deloittedctcustomerapp.ui.view.login;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = LoginAgreement.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface LoginAgreement_GeneratedInjector {
  void injectLoginAgreement(LoginAgreement loginAgreement);
}
