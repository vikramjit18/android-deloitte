package com.fwp.deloittedctcustomerapp.ui.view.als.confirmAls;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ConfirmAlsFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ConfirmAlsFragment_GeneratedInjector {
  void injectConfirmAlsFragment(ConfirmAlsFragment confirmAlsFragment);
}
