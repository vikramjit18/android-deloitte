package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ItemsHeldAvailableFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ItemsHeldAvailableFragment_GeneratedInjector {
  void injectItemsHeldAvailableFragment(ItemsHeldAvailableFragment itemsHeldAvailableFragment);
}
