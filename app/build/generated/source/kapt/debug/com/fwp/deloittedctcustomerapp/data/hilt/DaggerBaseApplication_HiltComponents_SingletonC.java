// Generated by Dagger (https://dagger.dev).
package com.fwp.deloittedctcustomerapp.data.hilt;

import android.app.Activity;
import android.app.Service;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;
import com.fwp.deloittedctcustomerapp.data.api.ApiHelper;
import com.fwp.deloittedctcustomerapp.data.db.AppDao;
import com.fwp.deloittedctcustomerapp.data.db.AppDatabase;
import com.fwp.deloittedctcustomerapp.data.repo.MainRepo;
import com.fwp.deloittedctcustomerapp.ui.view.MainActivity;
import com.fwp.deloittedctcustomerapp.ui.view.als.AlsLinkingActivity;
import com.fwp.deloittedctcustomerapp.ui.view.als.confirmAls.ConfirmAlsFragment;
import com.fwp.deloittedctcustomerapp.ui.view.als.linking.LinkingFragment;
import com.fwp.deloittedctcustomerapp.ui.view.als.lookup.LearnMoreLookUpFragment;
import com.fwp.deloittedctcustomerapp.ui.view.als.lookup.LookUpAlsFragment;
import com.fwp.deloittedctcustomerapp.ui.view.als.viewModel.AlsViewModel;
import com.fwp.deloittedctcustomerapp.ui.view.als.viewModel.AlsViewModel_HiltModules_KeyModule_ProvideFactory;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.LandingScreenActivity;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.common.AlsNotLinked;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.EtagsFragment;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple.MultiUserEtagAvailableFragment;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary.EtagsAvailableFragment;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.ReviewHarvestFragment;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.ValidateTagsActivity;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel_HiltModules_KeyModule_ProvideFactory;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.sponsors.ViewAllSponsorsFrag;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView.TagLicenceActivity;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView.TagPermitActivity;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.multiple.viewPager.LinkedItemsFragment;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.primary.ItemsHeldAvailableFragment;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailActivity;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailFragment;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailViewModel;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailViewModel_HiltModules_KeyModule_ProvideFactory;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.ProfileFragment;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.ProfileViewModel;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.ProfileViewModel_HiltModules_KeyModule_ProvideFactory;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo.ChangeLoginInfoFragment;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo.ChangeLoginInfoViewModel;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo.ChangeLoginInfoViewModel_HiltModules_KeyModule_ProvideFactory;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password.ResetPasswordFragment;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password.ResetPasswordViewModel;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password.ResetPasswordViewModel_HiltModules_KeyModule_ProvideFactory;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.PrimaryAccountFragment;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.PrimaryAccountViewModel;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.PrimaryAccountViewModel_HiltModules_KeyModule_ProvideFactory;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.editprimary.EditPrimaryFragment;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel;
import com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel_HiltModules_KeyModule_ProvideFactory;
import com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotActivity;
import com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotInitial;
import com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotViewModel;
import com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotViewModel_HiltModules_KeyModule_ProvideFactory;
import com.fwp.deloittedctcustomerapp.ui.view.login.Login;
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginActivity;
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginAgreement;
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginViewModel;
import com.fwp.deloittedctcustomerapp.ui.view.login.LoginViewModel_HiltModules_KeyModule_ProvideFactory;
import com.fwp.deloittedctcustomerapp.ui.view.onboard.OnBoard;
import com.fwp.deloittedctcustomerapp.ui.view.resetPass.ForgotReset;
import com.fwp.deloittedctcustomerapp.ui.view.resetPass.ResetPassActivity;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpActivate;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpActivity;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpAgreement;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpDetails;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpInitial;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpViewModel;
import com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpViewModel_HiltModules_KeyModule_ProvideFactory;
import dagger.hilt.android.ActivityRetainedLifecycle;
import dagger.hilt.android.internal.builders.ActivityComponentBuilder;
import dagger.hilt.android.internal.builders.ActivityRetainedComponentBuilder;
import dagger.hilt.android.internal.builders.FragmentComponentBuilder;
import dagger.hilt.android.internal.builders.ServiceComponentBuilder;
import dagger.hilt.android.internal.builders.ViewComponentBuilder;
import dagger.hilt.android.internal.builders.ViewModelComponentBuilder;
import dagger.hilt.android.internal.builders.ViewWithFragmentComponentBuilder;
import dagger.hilt.android.internal.lifecycle.DefaultViewModelFactories;
import dagger.hilt.android.internal.lifecycle.DefaultViewModelFactories_InternalFactoryFactory_Factory;
import dagger.hilt.android.internal.managers.ActivityRetainedComponentManager_Lifecycle_Factory;
import dagger.hilt.android.internal.modules.ApplicationContextModule;
import dagger.hilt.android.internal.modules.ApplicationContextModule_ProvideApplicationFactory;
import dagger.hilt.android.internal.modules.ApplicationContextModule_ProvideContextFactory;
import dagger.internal.DaggerGenerated;
import dagger.internal.DoubleCheck;
import dagger.internal.MapBuilder;
import dagger.internal.Preconditions;
import dagger.internal.SetBuilder;
import java.util.Map;
import java.util.Set;
import javax.inject.Provider;
import okhttp3.Call;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DaggerBaseApplication_HiltComponents_SingletonC extends BaseApplication_HiltComponents.SingletonC {
  private final ApplicationContextModule applicationContextModule;

  private final DaggerBaseApplication_HiltComponents_SingletonC singletonC = this;

  private Provider<HttpLoggingInterceptor> provideHttpLoginInterceptorProvider;

  private Provider<Call.Factory> provideCallFactoryProvider;

  private Provider<MoshiConverterFactory> provideMoshiConvertorFactoryProvider;

  private Provider<Retrofit> provideRetrofitApiProvider;

  private Provider<ApiHelper> provideApiServicesProvider;

  private Provider<AppDatabase> provideAppDatabaseProvider;

  private Provider<AppDao> provideAppDaoProvider;

  private Provider<MainRepo> mainRepoProvider;

  private DaggerBaseApplication_HiltComponents_SingletonC(
      ApplicationContextModule applicationContextModuleParam) {
    this.applicationContextModule = applicationContextModuleParam;
    initialize(applicationContextModuleParam);

  }

  public static Builder builder() {
    return new Builder();
  }

  private Call.Factory callFactory() {
    return NetworkModule_ProvideCallFactoryFactory.provideCallFactory(provideHttpLoginInterceptorProvider.get());
  }

  private Retrofit retrofit() {
    return NetworkModule_ProvideRetrofitApiFactory.provideRetrofitApi(provideCallFactoryProvider.get(), provideMoshiConvertorFactoryProvider.get());
  }

  private ApiHelper apiHelper() {
    return NetworkModule_ProvideApiServicesFactory.provideApiServices(provideRetrofitApiProvider.get());
  }

  private AppDatabase appDatabase() {
    return NetworkModule_ProvideAppDatabaseFactory.provideAppDatabase(ApplicationContextModule_ProvideContextFactory.provideContext(applicationContextModule));
  }

  private AppDao appDao() {
    return NetworkModule_ProvideAppDaoFactory.provideAppDao(provideAppDatabaseProvider.get());
  }

  private MainRepo mainRepo() {
    return new MainRepo(provideApiServicesProvider.get(), provideAppDaoProvider.get());
  }

  @SuppressWarnings("unchecked")
  private void initialize(final ApplicationContextModule applicationContextModuleParam) {
    this.provideHttpLoginInterceptorProvider = DoubleCheck.provider(new SwitchingProvider<HttpLoggingInterceptor>(singletonC, 4));
    this.provideCallFactoryProvider = DoubleCheck.provider(new SwitchingProvider<Call.Factory>(singletonC, 3));
    this.provideMoshiConvertorFactoryProvider = DoubleCheck.provider(new SwitchingProvider<MoshiConverterFactory>(singletonC, 5));
    this.provideRetrofitApiProvider = DoubleCheck.provider(new SwitchingProvider<Retrofit>(singletonC, 2));
    this.provideApiServicesProvider = DoubleCheck.provider(new SwitchingProvider<ApiHelper>(singletonC, 1));
    this.provideAppDatabaseProvider = DoubleCheck.provider(new SwitchingProvider<AppDatabase>(singletonC, 7));
    this.provideAppDaoProvider = DoubleCheck.provider(new SwitchingProvider<AppDao>(singletonC, 6));
    this.mainRepoProvider = DoubleCheck.provider(new SwitchingProvider<MainRepo>(singletonC, 0));
  }

  @Override
  public void injectBaseApplication(BaseApplication baseApplication) {
  }

  @Override
  public ActivityRetainedComponentBuilder retainedComponentBuilder() {
    return new ActivityRetainedCBuilder(singletonC);
  }

  @Override
  public ServiceComponentBuilder serviceComponentBuilder() {
    return new ServiceCBuilder(singletonC);
  }

  public static final class Builder {
    private ApplicationContextModule applicationContextModule;

    private Builder() {
    }

    public Builder applicationContextModule(ApplicationContextModule applicationContextModule) {
      this.applicationContextModule = Preconditions.checkNotNull(applicationContextModule);
      return this;
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This method is a no-op. For more, see https://dagger.dev/unused-modules.
     */
    @Deprecated
    public Builder networkModule(NetworkModule networkModule) {
      Preconditions.checkNotNull(networkModule);
      return this;
    }

    public BaseApplication_HiltComponents.SingletonC build() {
      Preconditions.checkBuilderRequirement(applicationContextModule, ApplicationContextModule.class);
      return new DaggerBaseApplication_HiltComponents_SingletonC(applicationContextModule);
    }
  }

  private static final class ActivityRetainedCBuilder implements BaseApplication_HiltComponents.ActivityRetainedC.Builder {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private ActivityRetainedCBuilder(DaggerBaseApplication_HiltComponents_SingletonC singletonC) {
      this.singletonC = singletonC;
    }

    @Override
    public BaseApplication_HiltComponents.ActivityRetainedC build() {
      return new ActivityRetainedCImpl(singletonC);
    }
  }

  private static final class ActivityCBuilder implements BaseApplication_HiltComponents.ActivityC.Builder {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private Activity activity;

    private ActivityCBuilder(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
        ActivityRetainedCImpl activityRetainedCImpl) {
      this.singletonC = singletonC;
      this.activityRetainedCImpl = activityRetainedCImpl;
    }

    @Override
    public ActivityCBuilder activity(Activity activity) {
      this.activity = Preconditions.checkNotNull(activity);
      return this;
    }

    @Override
    public BaseApplication_HiltComponents.ActivityC build() {
      Preconditions.checkBuilderRequirement(activity, Activity.class);
      return new ActivityCImpl(singletonC, activityRetainedCImpl, activity);
    }
  }

  private static final class FragmentCBuilder implements BaseApplication_HiltComponents.FragmentC.Builder {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl;

    private Fragment fragment;

    private FragmentCBuilder(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
        ActivityRetainedCImpl activityRetainedCImpl, ActivityCImpl activityCImpl) {
      this.singletonC = singletonC;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.activityCImpl = activityCImpl;
    }

    @Override
    public FragmentCBuilder fragment(Fragment fragment) {
      this.fragment = Preconditions.checkNotNull(fragment);
      return this;
    }

    @Override
    public BaseApplication_HiltComponents.FragmentC build() {
      Preconditions.checkBuilderRequirement(fragment, Fragment.class);
      return new FragmentCImpl(singletonC, activityRetainedCImpl, activityCImpl, fragment);
    }
  }

  private static final class ViewWithFragmentCBuilder implements BaseApplication_HiltComponents.ViewWithFragmentC.Builder {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl;

    private final FragmentCImpl fragmentCImpl;

    private View view;

    private ViewWithFragmentCBuilder(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
        ActivityRetainedCImpl activityRetainedCImpl, ActivityCImpl activityCImpl,
        FragmentCImpl fragmentCImpl) {
      this.singletonC = singletonC;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.activityCImpl = activityCImpl;
      this.fragmentCImpl = fragmentCImpl;
    }

    @Override
    public ViewWithFragmentCBuilder view(View view) {
      this.view = Preconditions.checkNotNull(view);
      return this;
    }

    @Override
    public BaseApplication_HiltComponents.ViewWithFragmentC build() {
      Preconditions.checkBuilderRequirement(view, View.class);
      return new ViewWithFragmentCImpl(singletonC, activityRetainedCImpl, activityCImpl, fragmentCImpl, view);
    }
  }

  private static final class ViewCBuilder implements BaseApplication_HiltComponents.ViewC.Builder {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl;

    private View view;

    private ViewCBuilder(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
        ActivityRetainedCImpl activityRetainedCImpl, ActivityCImpl activityCImpl) {
      this.singletonC = singletonC;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.activityCImpl = activityCImpl;
    }

    @Override
    public ViewCBuilder view(View view) {
      this.view = Preconditions.checkNotNull(view);
      return this;
    }

    @Override
    public BaseApplication_HiltComponents.ViewC build() {
      Preconditions.checkBuilderRequirement(view, View.class);
      return new ViewCImpl(singletonC, activityRetainedCImpl, activityCImpl, view);
    }
  }

  private static final class ViewModelCBuilder implements BaseApplication_HiltComponents.ViewModelC.Builder {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private SavedStateHandle savedStateHandle;

    private ViewModelCBuilder(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
        ActivityRetainedCImpl activityRetainedCImpl) {
      this.singletonC = singletonC;
      this.activityRetainedCImpl = activityRetainedCImpl;
    }

    @Override
    public ViewModelCBuilder savedStateHandle(SavedStateHandle handle) {
      this.savedStateHandle = Preconditions.checkNotNull(handle);
      return this;
    }

    @Override
    public BaseApplication_HiltComponents.ViewModelC build() {
      Preconditions.checkBuilderRequirement(savedStateHandle, SavedStateHandle.class);
      return new ViewModelCImpl(singletonC, activityRetainedCImpl, savedStateHandle);
    }
  }

  private static final class ServiceCBuilder implements BaseApplication_HiltComponents.ServiceC.Builder {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private Service service;

    private ServiceCBuilder(DaggerBaseApplication_HiltComponents_SingletonC singletonC) {
      this.singletonC = singletonC;
    }

    @Override
    public ServiceCBuilder service(Service service) {
      this.service = Preconditions.checkNotNull(service);
      return this;
    }

    @Override
    public BaseApplication_HiltComponents.ServiceC build() {
      Preconditions.checkBuilderRequirement(service, Service.class);
      return new ServiceCImpl(singletonC, service);
    }
  }

  private static final class ViewWithFragmentCImpl extends BaseApplication_HiltComponents.ViewWithFragmentC {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl;

    private final FragmentCImpl fragmentCImpl;

    private final ViewWithFragmentCImpl viewWithFragmentCImpl = this;

    private ViewWithFragmentCImpl(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
        ActivityRetainedCImpl activityRetainedCImpl, ActivityCImpl activityCImpl,
        FragmentCImpl fragmentCImpl, View viewParam) {
      this.singletonC = singletonC;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.activityCImpl = activityCImpl;
      this.fragmentCImpl = fragmentCImpl;


    }
  }

  private static final class FragmentCImpl extends BaseApplication_HiltComponents.FragmentC {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl;

    private final FragmentCImpl fragmentCImpl = this;

    private FragmentCImpl(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
        ActivityRetainedCImpl activityRetainedCImpl, ActivityCImpl activityCImpl,
        Fragment fragmentParam) {
      this.singletonC = singletonC;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.activityCImpl = activityCImpl;


    }

    @Override
    public void injectConfirmAlsFragment(ConfirmAlsFragment confirmAlsFragment) {
    }

    @Override
    public void injectLinkingFragment(LinkingFragment linkingFragment) {
    }

    @Override
    public void injectLearnMoreLookUpFragment(LearnMoreLookUpFragment learnMoreLookUpFragment) {
    }

    @Override
    public void injectLookUpAlsFragment(LookUpAlsFragment lookUpAlsFragment) {
    }

    @Override
    public void injectAlsNotLinked(AlsNotLinked alsNotLinked) {
    }

    @Override
    public void injectEtagsFragment(EtagsFragment etagsFragment) {
    }

    @Override
    public void injectMultiUserEtagAvailableFragment(
        MultiUserEtagAvailableFragment multiUserEtagAvailableFragment) {
    }

    @Override
    public void injectEtagsAvailableFragment(EtagsAvailableFragment etagsAvailableFragment) {
    }

    @Override
    public void injectReviewHarvestFragment(ReviewHarvestFragment reviewHarvestFragment) {
    }

    @Override
    public void injectViewAllSponsorsFrag(ViewAllSponsorsFrag viewAllSponsorsFrag) {
    }

    @Override
    public void injectLinkedItemsFragment(LinkedItemsFragment linkedItemsFragment) {
    }

    @Override
    public void injectItemsHeldAvailableFragment(
        ItemsHeldAvailableFragment itemsHeldAvailableFragment) {
    }

    @Override
    public void injectRequestEmailFragment(RequestEmailFragment requestEmailFragment) {
    }

    @Override
    public void injectProfileFragment(ProfileFragment profileFragment) {
    }

    @Override
    public void injectChangeLoginInfoFragment(ChangeLoginInfoFragment changeLoginInfoFragment) {
    }

    @Override
    public void injectResetPasswordFragment(ResetPasswordFragment resetPasswordFragment) {
    }

    @Override
    public void injectPrimaryAccountFragment(PrimaryAccountFragment primaryAccountFragment) {
    }

    @Override
    public void injectEditPrimaryFragment(EditPrimaryFragment editPrimaryFragment) {
    }

    @Override
    public void injectForgotInitial(ForgotInitial forgotInitial) {
    }

    @Override
    public void injectLoginAgreement(LoginAgreement loginAgreement) {
    }

    @Override
    public void injectLogin(Login login) {
    }

    @Override
    public void injectOnBoard(OnBoard onBoard) {
    }

    @Override
    public void injectForgotReset(ForgotReset forgotReset) {
    }

    @Override
    public void injectSignUpActivate(SignUpActivate signUpActivate) {
    }

    @Override
    public void injectSignUpAgreement(SignUpAgreement signUpAgreement) {
    }

    @Override
    public void injectSignUpDetails(SignUpDetails signUpDetails) {
    }

    @Override
    public void injectSignUpInitial(SignUpInitial signUpInitial) {
    }

    @Override
    public DefaultViewModelFactories.InternalFactoryFactory getHiltInternalFactoryFactory() {
      return activityCImpl.getHiltInternalFactoryFactory();
    }

    @Override
    public ViewWithFragmentComponentBuilder viewWithFragmentComponentBuilder() {
      return new ViewWithFragmentCBuilder(singletonC, activityRetainedCImpl, activityCImpl, fragmentCImpl);
    }
  }

  private static final class ViewCImpl extends BaseApplication_HiltComponents.ViewC {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl;

    private final ViewCImpl viewCImpl = this;

    private ViewCImpl(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
        ActivityRetainedCImpl activityRetainedCImpl, ActivityCImpl activityCImpl, View viewParam) {
      this.singletonC = singletonC;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.activityCImpl = activityCImpl;


    }
  }

  private static final class ActivityCImpl extends BaseApplication_HiltComponents.ActivityC {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl = this;

    private ActivityCImpl(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
        ActivityRetainedCImpl activityRetainedCImpl, Activity activityParam) {
      this.singletonC = singletonC;
      this.activityRetainedCImpl = activityRetainedCImpl;


    }

    @Override
    public void injectMainActivity(MainActivity mainActivity) {
    }

    @Override
    public void injectAlsLinkingActivity(AlsLinkingActivity alsLinkingActivity) {
    }

    @Override
    public void injectLandingScreenActivity(LandingScreenActivity landingScreenActivity) {
    }

    @Override
    public void injectValidateTagsActivity(ValidateTagsActivity validateTagsActivity) {
    }

    @Override
    public void injectTagLicenceActivity(TagLicenceActivity tagLicenceActivity) {
    }

    @Override
    public void injectTagPermitActivity(TagPermitActivity tagPermitActivity) {
    }

    @Override
    public void injectRequestEmailActivity(RequestEmailActivity requestEmailActivity) {
    }

    @Override
    public void injectForgotActivity(ForgotActivity forgotActivity) {
    }

    @Override
    public void injectLoginActivity(LoginActivity loginActivity) {
    }

    @Override
    public void injectResetPassActivity(ResetPassActivity resetPassActivity) {
    }

    @Override
    public void injectSignUpActivity(SignUpActivity signUpActivity) {
    }

    @Override
    public DefaultViewModelFactories.InternalFactoryFactory getHiltInternalFactoryFactory() {
      return DefaultViewModelFactories_InternalFactoryFactory_Factory.newInstance(ApplicationContextModule_ProvideApplicationFactory.provideApplication(singletonC.applicationContextModule), getViewModelKeys(), new ViewModelCBuilder(singletonC, activityRetainedCImpl));
    }

    @Override
    public Set<String> getViewModelKeys() {
      return SetBuilder.<String>newSetBuilder(11).add(AlsViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(ChangeLoginInfoViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(ForgotViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(LandingScreenActivityViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(LoginViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(PrimaryAccountViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(ProfileViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(RequestEmailViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(ResetPasswordViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(SignUpViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(ValidateTagActivityViewModel_HiltModules_KeyModule_ProvideFactory.provide()).build();
    }

    @Override
    public ViewModelComponentBuilder getViewModelComponentBuilder() {
      return new ViewModelCBuilder(singletonC, activityRetainedCImpl);
    }

    @Override
    public FragmentComponentBuilder fragmentComponentBuilder() {
      return new FragmentCBuilder(singletonC, activityRetainedCImpl, activityCImpl);
    }

    @Override
    public ViewComponentBuilder viewComponentBuilder() {
      return new ViewCBuilder(singletonC, activityRetainedCImpl, activityCImpl);
    }
  }

  private static final class ViewModelCImpl extends BaseApplication_HiltComponents.ViewModelC {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ViewModelCImpl viewModelCImpl = this;

    private Provider<AlsViewModel> alsViewModelProvider;

    private Provider<ChangeLoginInfoViewModel> changeLoginInfoViewModelProvider;

    private Provider<ForgotViewModel> forgotViewModelProvider;

    private Provider<LandingScreenActivityViewModel> landingScreenActivityViewModelProvider;

    private Provider<LoginViewModel> loginViewModelProvider;

    private Provider<PrimaryAccountViewModel> primaryAccountViewModelProvider;

    private Provider<ProfileViewModel> profileViewModelProvider;

    private Provider<RequestEmailViewModel> requestEmailViewModelProvider;

    private Provider<ResetPasswordViewModel> resetPasswordViewModelProvider;

    private Provider<SignUpViewModel> signUpViewModelProvider;

    private Provider<ValidateTagActivityViewModel> validateTagActivityViewModelProvider;

    private ViewModelCImpl(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
        ActivityRetainedCImpl activityRetainedCImpl, SavedStateHandle savedStateHandleParam) {
      this.singletonC = singletonC;
      this.activityRetainedCImpl = activityRetainedCImpl;

      initialize(savedStateHandleParam);

    }

    private AlsViewModel alsViewModel() {
      return new AlsViewModel(singletonC.mainRepoProvider.get());
    }

    private ChangeLoginInfoViewModel changeLoginInfoViewModel() {
      return new ChangeLoginInfoViewModel(singletonC.mainRepoProvider.get(), singletonC.provideAppDaoProvider.get());
    }

    private ForgotViewModel forgotViewModel() {
      return new ForgotViewModel(singletonC.mainRepoProvider.get());
    }

    private LandingScreenActivityViewModel landingScreenActivityViewModel() {
      return new LandingScreenActivityViewModel(singletonC.mainRepoProvider.get(), singletonC.provideAppDaoProvider.get());
    }

    private LoginViewModel loginViewModel() {
      return new LoginViewModel(singletonC.mainRepoProvider.get());
    }

    private PrimaryAccountViewModel primaryAccountViewModel() {
      return new PrimaryAccountViewModel(singletonC.mainRepoProvider.get());
    }

    private ProfileViewModel profileViewModel() {
      return new ProfileViewModel(singletonC.mainRepoProvider.get());
    }

    private RequestEmailViewModel requestEmailViewModel() {
      return new RequestEmailViewModel(singletonC.mainRepoProvider.get());
    }

    private ResetPasswordViewModel resetPasswordViewModel() {
      return new ResetPasswordViewModel(singletonC.mainRepoProvider.get(), singletonC.provideAppDaoProvider.get());
    }

    private SignUpViewModel signUpViewModel() {
      return new SignUpViewModel(singletonC.mainRepoProvider.get());
    }

    private ValidateTagActivityViewModel validateTagActivityViewModel() {
      return new ValidateTagActivityViewModel(singletonC.mainRepoProvider.get(), singletonC.provideAppDaoProvider.get());
    }

    @SuppressWarnings("unchecked")
    private void initialize(final SavedStateHandle savedStateHandleParam) {
      this.alsViewModelProvider = new SwitchingProvider<>(singletonC, activityRetainedCImpl, viewModelCImpl, 0);
      this.changeLoginInfoViewModelProvider = new SwitchingProvider<>(singletonC, activityRetainedCImpl, viewModelCImpl, 1);
      this.forgotViewModelProvider = new SwitchingProvider<>(singletonC, activityRetainedCImpl, viewModelCImpl, 2);
      this.landingScreenActivityViewModelProvider = new SwitchingProvider<>(singletonC, activityRetainedCImpl, viewModelCImpl, 3);
      this.loginViewModelProvider = new SwitchingProvider<>(singletonC, activityRetainedCImpl, viewModelCImpl, 4);
      this.primaryAccountViewModelProvider = new SwitchingProvider<>(singletonC, activityRetainedCImpl, viewModelCImpl, 5);
      this.profileViewModelProvider = new SwitchingProvider<>(singletonC, activityRetainedCImpl, viewModelCImpl, 6);
      this.requestEmailViewModelProvider = new SwitchingProvider<>(singletonC, activityRetainedCImpl, viewModelCImpl, 7);
      this.resetPasswordViewModelProvider = new SwitchingProvider<>(singletonC, activityRetainedCImpl, viewModelCImpl, 8);
      this.signUpViewModelProvider = new SwitchingProvider<>(singletonC, activityRetainedCImpl, viewModelCImpl, 9);
      this.validateTagActivityViewModelProvider = new SwitchingProvider<>(singletonC, activityRetainedCImpl, viewModelCImpl, 10);
    }

    @Override
    public Map<String, Provider<ViewModel>> getHiltViewModelMap() {
      return MapBuilder.<String, Provider<ViewModel>>newMapBuilder(11).put("com.fwp.deloittedctcustomerapp.ui.view.als.viewModel.AlsViewModel", (Provider) alsViewModelProvider).put("com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo.ChangeLoginInfoViewModel", (Provider) changeLoginInfoViewModelProvider).put("com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotViewModel", (Provider) forgotViewModelProvider).put("com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel", (Provider) landingScreenActivityViewModelProvider).put("com.fwp.deloittedctcustomerapp.ui.view.login.LoginViewModel", (Provider) loginViewModelProvider).put("com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.PrimaryAccountViewModel", (Provider) primaryAccountViewModelProvider).put("com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.ProfileViewModel", (Provider) profileViewModelProvider).put("com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailViewModel", (Provider) requestEmailViewModelProvider).put("com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password.ResetPasswordViewModel", (Provider) resetPasswordViewModelProvider).put("com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpViewModel", (Provider) signUpViewModelProvider).put("com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel", (Provider) validateTagActivityViewModelProvider).build();
    }

    private static final class SwitchingProvider<T> implements Provider<T> {
      private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

      private final ActivityRetainedCImpl activityRetainedCImpl;

      private final ViewModelCImpl viewModelCImpl;

      private final int id;

      SwitchingProvider(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
          ActivityRetainedCImpl activityRetainedCImpl, ViewModelCImpl viewModelCImpl, int id) {
        this.singletonC = singletonC;
        this.activityRetainedCImpl = activityRetainedCImpl;
        this.viewModelCImpl = viewModelCImpl;
        this.id = id;
      }

      @SuppressWarnings("unchecked")
      @Override
      public T get() {
        switch (id) {
          case 0: // com.fwp.deloittedctcustomerapp.ui.view.als.viewModel.AlsViewModel 
          return (T) viewModelCImpl.alsViewModel();

          case 1: // com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo.ChangeLoginInfoViewModel 
          return (T) viewModelCImpl.changeLoginInfoViewModel();

          case 2: // com.fwp.deloittedctcustomerapp.ui.view.forgot.ForgotViewModel 
          return (T) viewModelCImpl.forgotViewModel();

          case 3: // com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel 
          return (T) viewModelCImpl.landingScreenActivityViewModel();

          case 4: // com.fwp.deloittedctcustomerapp.ui.view.login.LoginViewModel 
          return (T) viewModelCImpl.loginViewModel();

          case 5: // com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.PrimaryAccountViewModel 
          return (T) viewModelCImpl.primaryAccountViewModel();

          case 6: // com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.ProfileViewModel 
          return (T) viewModelCImpl.profileViewModel();

          case 7: // com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail.RequestEmailViewModel 
          return (T) viewModelCImpl.requestEmailViewModel();

          case 8: // com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password.ResetPasswordViewModel 
          return (T) viewModelCImpl.resetPasswordViewModel();

          case 9: // com.fwp.deloittedctcustomerapp.ui.view.signup.SignUpViewModel 
          return (T) viewModelCImpl.signUpViewModel();

          case 10: // com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.activityViewModel.ValidateTagActivityViewModel 
          return (T) viewModelCImpl.validateTagActivityViewModel();

          default: throw new AssertionError(id);
        }
      }
    }
  }

  private static final class ActivityRetainedCImpl extends BaseApplication_HiltComponents.ActivityRetainedC {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final ActivityRetainedCImpl activityRetainedCImpl = this;

    @SuppressWarnings("rawtypes")
    private Provider lifecycleProvider;

    private ActivityRetainedCImpl(DaggerBaseApplication_HiltComponents_SingletonC singletonC) {
      this.singletonC = singletonC;

      initialize();

    }

    @SuppressWarnings("unchecked")
    private void initialize() {
      this.lifecycleProvider = DoubleCheck.provider(new SwitchingProvider<Object>(singletonC, activityRetainedCImpl, 0));
    }

    @Override
    public ActivityComponentBuilder activityComponentBuilder() {
      return new ActivityCBuilder(singletonC, activityRetainedCImpl);
    }

    @Override
    public ActivityRetainedLifecycle getActivityRetainedLifecycle() {
      return (ActivityRetainedLifecycle) lifecycleProvider.get();
    }

    private static final class SwitchingProvider<T> implements Provider<T> {
      private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

      private final ActivityRetainedCImpl activityRetainedCImpl;

      private final int id;

      SwitchingProvider(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
          ActivityRetainedCImpl activityRetainedCImpl, int id) {
        this.singletonC = singletonC;
        this.activityRetainedCImpl = activityRetainedCImpl;
        this.id = id;
      }

      @SuppressWarnings("unchecked")
      @Override
      public T get() {
        switch (id) {
          case 0: // dagger.hilt.android.internal.managers.ActivityRetainedComponentManager.Lifecycle 
          return (T) ActivityRetainedComponentManager_Lifecycle_Factory.newInstance();

          default: throw new AssertionError(id);
        }
      }
    }
  }

  private static final class ServiceCImpl extends BaseApplication_HiltComponents.ServiceC {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final ServiceCImpl serviceCImpl = this;

    private ServiceCImpl(DaggerBaseApplication_HiltComponents_SingletonC singletonC,
        Service serviceParam) {
      this.singletonC = singletonC;


    }
  }

  private static final class SwitchingProvider<T> implements Provider<T> {
    private final DaggerBaseApplication_HiltComponents_SingletonC singletonC;

    private final int id;

    SwitchingProvider(DaggerBaseApplication_HiltComponents_SingletonC singletonC, int id) {
      this.singletonC = singletonC;
      this.id = id;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T get() {
      switch (id) {
        case 0: // com.fwp.deloittedctcustomerapp.data.repo.MainRepo 
        return (T) singletonC.mainRepo();

        case 1: // com.fwp.deloittedctcustomerapp.data.api.ApiHelper 
        return (T) singletonC.apiHelper();

        case 2: // retrofit2.Retrofit 
        return (T) singletonC.retrofit();

        case 3: // okhttp3.Call.Factory 
        return (T) singletonC.callFactory();

        case 4: // okhttp3.logging.HttpLoggingInterceptor 
        return (T) NetworkModule_ProvideHttpLoginInterceptorFactory.provideHttpLoginInterceptor();

        case 5: // retrofit2.converter.moshi.MoshiConverterFactory 
        return (T) NetworkModule_ProvideMoshiConvertorFactoryFactory.provideMoshiConvertorFactory();

        case 6: // com.fwp.deloittedctcustomerapp.data.db.AppDao 
        return (T) singletonC.appDao();

        case 7: // com.fwp.deloittedctcustomerapp.data.db.AppDatabase 
        return (T) singletonC.appDatabase();

        default: throw new AssertionError(id);
      }
    }
  }
}
