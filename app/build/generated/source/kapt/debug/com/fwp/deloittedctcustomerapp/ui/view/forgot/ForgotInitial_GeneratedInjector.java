package com.fwp.deloittedctcustomerapp.ui.view.forgot;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ForgotInitial.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ForgotInitial_GeneratedInjector {
  void injectForgotInitial(ForgotInitial forgotInitial);
}
