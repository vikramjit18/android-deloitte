package com.fwp.deloittedctcustomerapp.databinding;
import com.fwp.deloittedctcustomerapp.R;
import com.fwp.deloittedctcustomerapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityTagLicenceBindingImpl extends ActivityTagLicenceBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.fadedDivider1, 2);
        sViewsWithIds.put(R.id.fadedDivider2, 3);
        sViewsWithIds.put(R.id.licenseContainer, 4);
        sViewsWithIds.put(R.id.ll_head, 5);
        sViewsWithIds.put(R.id.headImg, 6);
        sViewsWithIds.put(R.id.head, 7);
        sViewsWithIds.put(R.id.licence_info, 8);
        sViewsWithIds.put(R.id.dividerYellow1, 9);
        sViewsWithIds.put(R.id.ownerInfoLayout, 10);
        sViewsWithIds.put(R.id.tv_owner_name, 11);
        sViewsWithIds.put(R.id.alsInfoLayout, 12);
        sViewsWithIds.put(R.id.tv_als_number, 13);
        sViewsWithIds.put(R.id.residentInfoLayout, 14);
        sViewsWithIds.put(R.id.tv_license_yr, 15);
        sViewsWithIds.put(R.id.constraintLayout7, 16);
        sViewsWithIds.put(R.id.textView18, 17);
        sViewsWithIds.put(R.id.dividerYellow2, 18);
        sViewsWithIds.put(R.id.recycler_view, 19);
        sViewsWithIds.put(R.id.ll_listing, 20);
        sViewsWithIds.put(R.id.tv_last_session_dt, 21);
        sViewsWithIds.put(R.id.ll_dates, 22);
        sViewsWithIds.put(R.id.tv_usage_dt, 23);
        sViewsWithIds.put(R.id.ll_session, 24);
        sViewsWithIds.put(R.id.close_lay, 25);
        sViewsWithIds.put(R.id.close, 26);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityTagLicenceBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 27, sIncludes, sViewsWithIds));
    }
    private ActivityTagLicenceBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[12]
            , (android.widget.ImageView) bindings[26]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[25]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[1]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[16]
            , (android.widget.ImageView) bindings[9]
            , (android.widget.ImageView) bindings[18]
            , (bindings[2] != null) ? com.fwp.deloittedctcustomerapp.databinding.FadedDividerBinding.bind((android.view.View) bindings[2]) : null
            , (bindings[3] != null) ? com.fwp.deloittedctcustomerapp.databinding.FadedDividerBinding.bind((android.view.View) bindings[3]) : null
            , (android.widget.TextView) bindings[7]
            , (android.widget.ImageView) bindings[6]
            , (android.widget.TextView) bindings[8]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[4]
            , (androidx.appcompat.widget.LinearLayoutCompat) bindings[22]
            , (android.widget.LinearLayout) bindings[5]
            , (androidx.appcompat.widget.LinearLayoutCompat) bindings[20]
            , (androidx.appcompat.widget.LinearLayoutCompat) bindings[24]
            , (android.widget.LinearLayout) bindings[10]
            , (androidx.recyclerview.widget.RecyclerView) bindings[19]
            , (android.widget.LinearLayout) bindings[14]
            , (android.widget.FrameLayout) bindings[0]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[21]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[23]
            );
        this.constraintLayout6.setTag(null);
        this.tagLicenceLayout.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}