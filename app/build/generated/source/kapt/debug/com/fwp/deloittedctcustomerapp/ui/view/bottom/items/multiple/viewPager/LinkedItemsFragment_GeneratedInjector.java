package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.multiple.viewPager;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = LinkedItemsFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface LinkedItemsFragment_GeneratedInjector {
  void injectLinkedItemsFragment(LinkedItemsFragment linkedItemsFragment);
}
