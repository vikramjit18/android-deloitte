package com.fwp.deloittedctcustomerapp.databinding;
import com.fwp.deloittedctcustomerapp.R;
import com.fwp.deloittedctcustomerapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LinkedEtagsFragmentBindingImpl extends LinkedEtagsFragmentBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(17);
        sIncludes.setIncludes(1, 
            new String[] {"layout_no_account_selected"},
            new int[] {7},
            new int[] {com.fwp.deloittedctcustomerapp.R.layout.layout_no_account_selected});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.no_etag_available, 4);
        sViewsWithIds.put(R.id.buy_and_apply, 5);
        sViewsWithIds.put(R.id.no_etag_prev_year, 6);
        sViewsWithIds.put(R.id.dropdown1, 8);
        sViewsWithIds.put(R.id.spinner1, 9);
        sViewsWithIds.put(R.id.rl_info_container, 10);
        sViewsWithIds.put(R.id.yearHeader, 11);
        sViewsWithIds.put(R.id.downloadAll, 12);
        sViewsWithIds.put(R.id.more, 13);
        sViewsWithIds.put(R.id.current_year_recycler_view, 14);
        sViewsWithIds.put(R.id.previous_year_heading, 15);
        sViewsWithIds.put(R.id.previous_year_recycler_view, 16);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LinkedEtagsFragmentBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 17, sIncludes, sViewsWithIds));
    }
    private LinkedEtagsFragmentBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (bindings[5] != null) ? com.fwp.deloittedctcustomerapp.databinding.BuyApplyCardLayoutBinding.bind((android.view.View) bindings[5]) : null
            , (androidx.recyclerview.widget.RecyclerView) bindings[14]
            , (android.widget.LinearLayout) bindings[2]
            , (android.widget.ImageView) bindings[12]
            , (android.widget.LinearLayout) bindings[8]
            , (android.widget.FrameLayout) bindings[0]
            , (com.fwp.deloittedctcustomerapp.databinding.LayoutNoAccountSelectedBinding) bindings[7]
            , (android.widget.ImageView) bindings[13]
            , (bindings[4] != null) ? com.fwp.deloittedctcustomerapp.databinding.NoEtagLayoutBinding.bind((android.view.View) bindings[4]) : null
            , (bindings[6] != null) ? com.fwp.deloittedctcustomerapp.databinding.NoEtagYearBinding.bind((android.view.View) bindings[6]) : null
            , (android.widget.TextView) bindings[15]
            , (androidx.recyclerview.widget.RecyclerView) bindings[16]
            , (android.widget.LinearLayout) bindings[3]
            , (android.widget.RelativeLayout) bindings[10]
            , (android.widget.Spinner) bindings[9]
            , (android.widget.TextView) bindings[11]
            );
        this.currentYearRecyclerViewLayout.setTag(null);
        this.flLinkedItems.setTag(null);
        setContainedBinding(this.includeNoAccountView);
        this.mboundView1 = (android.widget.RelativeLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.previousYearRecyclerViewLayout.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        includeNoAccountView.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (includeNoAccountView.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        includeNoAccountView.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeIncludeNoAccountView((com.fwp.deloittedctcustomerapp.databinding.LayoutNoAccountSelectedBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeIncludeNoAccountView(com.fwp.deloittedctcustomerapp.databinding.LayoutNoAccountSelectedBinding IncludeNoAccountView, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
        executeBindingsOn(includeNoAccountView);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): includeNoAccountView
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}