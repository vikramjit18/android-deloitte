package com.fwp.deloittedctcustomerapp.databinding;
import com.fwp.deloittedctcustomerapp.R;
import com.fwp.deloittedctcustomerapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LinkingFragmentBindingImpl extends LinkingFragmentBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.iv_close, 3);
        sViewsWithIds.put(R.id.et_small, 4);
        sViewsWithIds.put(R.id.et_zipcode, 5);
        sViewsWithIds.put(R.id.linkAls, 6);
        sViewsWithIds.put(R.id.lookUpAls, 7);
        sViewsWithIds.put(R.id.tv_next_screen, 8);
        sViewsWithIds.put(R.id.primary_text, 9);
        sViewsWithIds.put(R.id.textView2, 10);
        sViewsWithIds.put(R.id.learnMore, 11);
        sViewsWithIds.put(R.id.textView3, 12);
        sViewsWithIds.put(R.id.et_number, 13);
        sViewsWithIds.put(R.id.textView6, 14);
        sViewsWithIds.put(R.id.textView7, 15);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LinkingFragmentBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 16, sIncludes, sViewsWithIds));
    }
    private LinkingFragmentBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.EditText) bindings[13]
            , (bindings[4] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutEdittextBinding.bind((android.view.View) bindings[4]) : null
            , (bindings[5] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutEdittextBinding.bind((android.view.View) bindings[5]) : null
            , (bindings[3] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutBackButtonBinding.bind((android.view.View) bindings[3]) : null
            , (android.widget.TextView) bindings[11]
            , (android.widget.LinearLayout) bindings[2]
            , (bindings[6] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutButtonBinding.bind((android.view.View) bindings[6]) : null
            , (bindings[7] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutButtonBinding.bind((android.view.View) bindings[7]) : null
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[8]
            );
        this.linearLayout2.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[1];
        this.mboundView1.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}