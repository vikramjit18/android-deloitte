package com.fwp.deloittedctcustomerapp.databinding;
import com.fwp.deloittedctcustomerapp.R;
import com.fwp.deloittedctcustomerapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityTagPermitBindingImpl extends ActivityTagPermitBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.fadedDivider1, 4);
        sViewsWithIds.put(R.id.fadedDivider2, 5);
        sViewsWithIds.put(R.id.fadedDivider5, 6);
        sViewsWithIds.put(R.id.fadedDividers, 7);
        sViewsWithIds.put(R.id.nestedScrollView3, 8);
        sViewsWithIds.put(R.id.licenseContainer, 9);
        sViewsWithIds.put(R.id.ll_head, 10);
        sViewsWithIds.put(R.id.headImg, 11);
        sViewsWithIds.put(R.id.head, 12);
        sViewsWithIds.put(R.id.permit_info, 13);
        sViewsWithIds.put(R.id.dividerYellow1, 14);
        sViewsWithIds.put(R.id.ownerInfoLayout, 15);
        sViewsWithIds.put(R.id.tv_owner_name, 16);
        sViewsWithIds.put(R.id.alsInfoLayout, 17);
        sViewsWithIds.put(R.id.tv_als_number, 18);
        sViewsWithIds.put(R.id.issuedInfoLayout, 19);
        sViewsWithIds.put(R.id.tv_issued_dt, 20);
        sViewsWithIds.put(R.id.opportunityHead, 21);
        sViewsWithIds.put(R.id.dividerYellowOp, 22);
        sViewsWithIds.put(R.id.opportunityInfoLayout, 23);
        sViewsWithIds.put(R.id.tv_opportunity, 24);
        sViewsWithIds.put(R.id.desInfoLayout, 25);
        sViewsWithIds.put(R.id.tv_description, 26);
        sViewsWithIds.put(R.id.textView18, 27);
        sViewsWithIds.put(R.id.dividerYellow2, 28);
        sViewsWithIds.put(R.id.regulation_1, 29);
        sViewsWithIds.put(R.id.textView20, 30);
        sViewsWithIds.put(R.id.regulation_link, 31);
        sViewsWithIds.put(R.id.tv22, 32);
        sViewsWithIds.put(R.id.tv24, 33);
        sViewsWithIds.put(R.id.close_lay, 34);
        sViewsWithIds.put(R.id.close, 35);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityTagPermitBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 36, sIncludes, sViewsWithIds));
    }
    private ActivityTagPermitBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[17]
            , (android.widget.ImageView) bindings[35]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[34]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[1]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[3]
            , (android.widget.LinearLayout) bindings[25]
            , (android.widget.ImageView) bindings[14]
            , (android.widget.ImageView) bindings[28]
            , (android.widget.ImageView) bindings[22]
            , (bindings[4] != null) ? com.fwp.deloittedctcustomerapp.databinding.FadedDividerBinding.bind((android.view.View) bindings[4]) : null
            , (bindings[5] != null) ? com.fwp.deloittedctcustomerapp.databinding.FadedDividerBinding.bind((android.view.View) bindings[5]) : null
            , (bindings[6] != null) ? com.fwp.deloittedctcustomerapp.databinding.FadedDividerBinding.bind((android.view.View) bindings[6]) : null
            , (bindings[7] != null) ? com.fwp.deloittedctcustomerapp.databinding.FadedDividerBinding.bind((android.view.View) bindings[7]) : null
            , (android.widget.TextView) bindings[12]
            , (android.widget.ImageView) bindings[11]
            , (android.widget.LinearLayout) bindings[19]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[9]
            , (android.widget.LinearLayout) bindings[10]
            , (androidx.core.widget.NestedScrollView) bindings[8]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[2]
            , (android.widget.TextView) bindings[21]
            , (android.widget.LinearLayout) bindings[23]
            , (android.widget.LinearLayout) bindings[15]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[29]
            , (android.widget.TextView) bindings[31]
            , (android.widget.FrameLayout) bindings[0]
            , (android.widget.TextView) bindings[27]
            , (android.widget.TextView) bindings[30]
            , (android.widget.TextView) bindings[32]
            , (android.widget.TextView) bindings[33]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[26]
            , (android.widget.TextView) bindings[20]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TextView) bindings[16]
            );
        this.constraintLayout6.setTag(null);
        this.constraintLayout7.setTag(null);
        this.opportunityContainer.setTag(null);
        this.tagPermitLayout.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}