package com.fwp.deloittedctcustomerapp.ui.view.bottom.common;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = AlsNotLinked.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface AlsNotLinked_GeneratedInjector {
  void injectAlsNotLinked(AlsNotLinked alsNotLinked);
}
