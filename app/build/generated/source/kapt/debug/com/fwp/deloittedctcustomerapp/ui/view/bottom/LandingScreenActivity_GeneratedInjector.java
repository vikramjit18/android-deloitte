package com.fwp.deloittedctcustomerapp.ui.view.bottom;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = LandingScreenActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
public interface LandingScreenActivity_GeneratedInjector {
  void injectLandingScreenActivity(LandingScreenActivity landingScreenActivity);
}
