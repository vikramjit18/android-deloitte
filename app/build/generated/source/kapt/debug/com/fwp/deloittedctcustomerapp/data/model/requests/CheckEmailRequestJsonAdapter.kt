// Code generated by moshi-kotlin-codegen. Do not edit.
@file:Suppress("DEPRECATION", "unused", "ClassName", "REDUNDANT_PROJECTION",
    "RedundantExplicitType", "LocalVariableName", "RedundantVisibilityModifier",
    "PLATFORM_CLASS_MAPPED_TO_KOTLIN", "IMPLICIT_NOTHING_TYPE_ARGUMENT_IN_RETURN_POSITION")

package com.fwp.deloittedctcustomerapp.`data`.model.requests

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import java.lang.NullPointerException
import kotlin.String
import kotlin.Suppress
import kotlin.Unit
import kotlin.collections.emptySet
import kotlin.text.buildString

public class CheckEmailRequestJsonAdapter(
  moshi: Moshi
) : JsonAdapter<CheckEmailRequest>() {
  private val options: JsonReader.Options = JsonReader.Options.of("emailids")

  private val nullableStringAdapter: JsonAdapter<String?> = moshi.adapter(String::class.java,
      emptySet(), "emailids")

  public override fun toString(): String = buildString(39) {
      append("GeneratedJsonAdapter(").append("CheckEmailRequest").append(')') }

  public override fun fromJson(reader: JsonReader): CheckEmailRequest {
    var emailids: String? = null
    reader.beginObject()
    while (reader.hasNext()) {
      when (reader.selectName(options)) {
        0 -> emailids = nullableStringAdapter.fromJson(reader)
        -1 -> {
          // Unknown name, skip it.
          reader.skipName()
          reader.skipValue()
        }
      }
    }
    reader.endObject()
    return CheckEmailRequest(
        emailids = emailids
    )
  }

  public override fun toJson(writer: JsonWriter, value_: CheckEmailRequest?): Unit {
    if (value_ == null) {
      throw NullPointerException("value_ was null! Wrap in .nullSafe() to write nullable values.")
    }
    writer.beginObject()
    writer.name("emailids")
    nullableStringAdapter.toJson(writer, value_.emailids)
    writer.endObject()
  }
}
