// Generated by Dagger (https://dagger.dev).
package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary;

import com.fwp.deloittedctcustomerapp.data.repo.MainRepo;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class PrimaryAccountViewModel_Factory implements Factory<PrimaryAccountViewModel> {
  private final Provider<MainRepo> mainRepoProvider;

  public PrimaryAccountViewModel_Factory(Provider<MainRepo> mainRepoProvider) {
    this.mainRepoProvider = mainRepoProvider;
  }

  @Override
  public PrimaryAccountViewModel get() {
    return newInstance(mainRepoProvider.get());
  }

  public static PrimaryAccountViewModel_Factory create(Provider<MainRepo> mainRepoProvider) {
    return new PrimaryAccountViewModel_Factory(mainRepoProvider);
  }

  public static PrimaryAccountViewModel newInstance(MainRepo mainRepo) {
    return new PrimaryAccountViewModel(mainRepo);
  }
}
