package com.fwp.deloittedctcustomerapp.ui.view.forgot;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ForgotActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
public interface ForgotActivity_GeneratedInjector {
  void injectForgotActivity(ForgotActivity forgotActivity);
}
