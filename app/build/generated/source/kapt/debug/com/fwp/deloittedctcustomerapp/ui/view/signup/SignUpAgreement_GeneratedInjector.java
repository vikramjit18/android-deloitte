package com.fwp.deloittedctcustomerapp.ui.view.signup;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = SignUpAgreement.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface SignUpAgreement_GeneratedInjector {
  void injectSignUpAgreement(SignUpAgreement signUpAgreement);
}
