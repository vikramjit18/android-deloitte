// Code generated by moshi-kotlin-codegen. Do not edit.
@file:Suppress("DEPRECATION", "unused", "ClassName", "REDUNDANT_PROJECTION",
    "RedundantExplicitType", "LocalVariableName", "RedundantVisibilityModifier",
    "PLATFORM_CLASS_MAPPED_TO_KOTLIN", "IMPLICIT_NOTHING_TYPE_ARGUMENT_IN_RETURN_POSITION")

package com.fwp.deloittedctcustomerapp.`data`.model.requests

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.`internal`.Util
import java.lang.NullPointerException
import kotlin.String
import kotlin.Suppress
import kotlin.Unit
import kotlin.collections.emptySet
import kotlin.text.buildString

public class ResetPasswordRequestJsonAdapter(
  moshi: Moshi
) : JsonAdapter<ResetPasswordRequest>() {
  private val options: JsonReader.Options = JsonReader.Options.of("emailid", "password", "uid")

  private val stringAdapter: JsonAdapter<String> = moshi.adapter(String::class.java, emptySet(),
      "emailid")

  public override fun toString(): String = buildString(42) {
      append("GeneratedJsonAdapter(").append("ResetPasswordRequest").append(')') }

  public override fun fromJson(reader: JsonReader): ResetPasswordRequest {
    var emailid: String? = null
    var password: String? = null
    var uid: String? = null
    reader.beginObject()
    while (reader.hasNext()) {
      when (reader.selectName(options)) {
        0 -> emailid = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("emailid",
            "emailid", reader)
        1 -> password = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("password",
            "password", reader)
        2 -> uid = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("uid", "uid", reader)
        -1 -> {
          // Unknown name, skip it.
          reader.skipName()
          reader.skipValue()
        }
      }
    }
    reader.endObject()
    return ResetPasswordRequest(
        emailid = emailid ?: throw Util.missingProperty("emailid", "emailid", reader),
        password = password ?: throw Util.missingProperty("password", "password", reader),
        uid = uid ?: throw Util.missingProperty("uid", "uid", reader)
    )
  }

  public override fun toJson(writer: JsonWriter, value_: ResetPasswordRequest?): Unit {
    if (value_ == null) {
      throw NullPointerException("value_ was null! Wrap in .nullSafe() to write nullable values.")
    }
    writer.beginObject()
    writer.name("emailid")
    stringAdapter.toJson(writer, value_.emailid)
    writer.name("password")
    stringAdapter.toJson(writer, value_.password)
    writer.name("uid")
    stringAdapter.toJson(writer, value_.uid)
    writer.endObject()
  }
}
