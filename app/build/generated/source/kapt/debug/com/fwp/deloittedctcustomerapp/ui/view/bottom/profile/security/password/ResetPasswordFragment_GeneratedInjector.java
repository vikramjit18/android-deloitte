package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.password;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ResetPasswordFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ResetPasswordFragment_GeneratedInjector {
  void injectResetPasswordFragment(ResetPasswordFragment resetPasswordFragment);
}
