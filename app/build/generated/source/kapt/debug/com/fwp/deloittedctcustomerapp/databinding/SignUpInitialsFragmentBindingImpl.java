package com.fwp.deloittedctcustomerapp.databinding;
import com.fwp.deloittedctcustomerapp.R;
import com.fwp.deloittedctcustomerapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class SignUpInitialsFragmentBindingImpl extends SignUpInitialsFragmentBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.next, 3);
        sViewsWithIds.put(R.id.back, 4);
        sViewsWithIds.put(R.id.linearProgressIndicator2, 5);
        sViewsWithIds.put(R.id.agreement_title, 6);
        sViewsWithIds.put(R.id.textView5, 7);
        sViewsWithIds.put(R.id.textView6, 8);
        sViewsWithIds.put(R.id.usernameLayout, 9);
        sViewsWithIds.put(R.id.user, 10);
        sViewsWithIds.put(R.id.textView, 11);
        sViewsWithIds.put(R.id.passText, 12);
        sViewsWithIds.put(R.id.passwordLayout, 13);
        sViewsWithIds.put(R.id.pass, 14);
        sViewsWithIds.put(R.id.reqText, 15);
        sViewsWithIds.put(R.id.checkbox_least_character, 16);
        sViewsWithIds.put(R.id.textView7, 17);
        sViewsWithIds.put(R.id.checkbox_number_character, 18);
        sViewsWithIds.put(R.id.includeNo, 19);
    }
    // views
    @Nullable
    private final com.fwp.deloittedctcustomerapp.databinding.LayoutLoginBgBinding mboundView0;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView01;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public SignUpInitialsFragmentBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 20, sIncludes, sViewsWithIds));
    }
    private SignUpInitialsFragmentBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[6]
            , (android.widget.ImageView) bindings[4]
            , (android.widget.ImageView) bindings[16]
            , (android.widget.ImageView) bindings[18]
            , (android.widget.TextView) bindings[19]
            , (com.google.android.material.progressindicator.LinearProgressIndicator) bindings[5]
            , (bindings[3] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutButtonBinding.bind((android.view.View) bindings[3]) : null
            , (com.google.android.material.textfield.TextInputEditText) bindings[14]
            , (android.widget.TextView) bindings[12]
            , (com.google.android.material.textfield.TextInputLayout) bindings[13]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[17]
            , (com.google.android.material.textfield.TextInputEditText) bindings[10]
            , (com.google.android.material.textfield.TextInputLayout) bindings[9]
            );
        this.mboundView0 = (bindings[2] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutLoginBgBinding.bind((android.view.View) bindings[2]) : null;
        this.mboundView01 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView01.setTag(null);
        this.mboundView1 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[1];
        this.mboundView1.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}