package com.fwp.deloittedctcustomerapp.data.db;

import androidx.annotation.NonNull;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.migration.AutoMigrationSpec;
import androidx.room.migration.Migration;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class AppDatabase_Impl extends AppDatabase {
  private volatile AppDao _appDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `validate_request` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `sync` INTEGER NOT NULL, `device_id` TEXT, `etag_upload_date` TEXT, `harvestConfNum` TEXT, `harvested_date` TEXT, `lat` REAL, `longt` REAL, `speciesCd` TEXT, `spi_id` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `downloaded_tags` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `downloadedSpiId` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '9020460aa5578ef1315f6842fdb94202')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `validate_request`");
        _db.execSQL("DROP TABLE IF EXISTS `downloaded_tags`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsValidateRequest = new HashMap<String, TableInfo.Column>(10);
        _columnsValidateRequest.put("id", new TableInfo.Column("id", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsValidateRequest.put("sync", new TableInfo.Column("sync", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsValidateRequest.put("device_id", new TableInfo.Column("device_id", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsValidateRequest.put("etag_upload_date", new TableInfo.Column("etag_upload_date", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsValidateRequest.put("harvestConfNum", new TableInfo.Column("harvestConfNum", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsValidateRequest.put("harvested_date", new TableInfo.Column("harvested_date", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsValidateRequest.put("lat", new TableInfo.Column("lat", "REAL", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsValidateRequest.put("longt", new TableInfo.Column("longt", "REAL", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsValidateRequest.put("speciesCd", new TableInfo.Column("speciesCd", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsValidateRequest.put("spi_id", new TableInfo.Column("spi_id", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysValidateRequest = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesValidateRequest = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoValidateRequest = new TableInfo("validate_request", _columnsValidateRequest, _foreignKeysValidateRequest, _indicesValidateRequest);
        final TableInfo _existingValidateRequest = TableInfo.read(_db, "validate_request");
        if (! _infoValidateRequest.equals(_existingValidateRequest)) {
          return new RoomOpenHelper.ValidationResult(false, "validate_request(com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest).\n"
                  + " Expected:\n" + _infoValidateRequest + "\n"
                  + " Found:\n" + _existingValidateRequest);
        }
        final HashMap<String, TableInfo.Column> _columnsDownloadedTags = new HashMap<String, TableInfo.Column>(2);
        _columnsDownloadedTags.put("id", new TableInfo.Column("id", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsDownloadedTags.put("downloadedSpiId", new TableInfo.Column("downloadedSpiId", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysDownloadedTags = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesDownloadedTags = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoDownloadedTags = new TableInfo("downloaded_tags", _columnsDownloadedTags, _foreignKeysDownloadedTags, _indicesDownloadedTags);
        final TableInfo _existingDownloadedTags = TableInfo.read(_db, "downloaded_tags");
        if (! _infoDownloadedTags.equals(_existingDownloadedTags)) {
          return new RoomOpenHelper.ValidationResult(false, "downloaded_tags(com.fwp.deloittedctcustomerapp.data.model.DownloadedTags).\n"
                  + " Expected:\n" + _infoDownloadedTags + "\n"
                  + " Found:\n" + _existingDownloadedTags);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "9020460aa5578ef1315f6842fdb94202", "7cf70e7cd53a448097d579c8ed973485");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "validate_request","downloaded_tags");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `validate_request`");
      _db.execSQL("DELETE FROM `downloaded_tags`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  protected Map<Class<?>, List<Class<?>>> getRequiredTypeConverters() {
    final HashMap<Class<?>, List<Class<?>>> _typeConvertersMap = new HashMap<Class<?>, List<Class<?>>>();
    _typeConvertersMap.put(AppDao.class, AppDao_Impl.getRequiredConverters());
    return _typeConvertersMap;
  }

  @Override
  public Set<Class<? extends AutoMigrationSpec>> getRequiredAutoMigrationSpecs() {
    final HashSet<Class<? extends AutoMigrationSpec>> _autoMigrationSpecsSet = new HashSet<Class<? extends AutoMigrationSpec>>();
    return _autoMigrationSpecsSet;
  }

  @Override
  public List<Migration> getAutoMigrations(
      @NonNull Map<Class<? extends AutoMigrationSpec>, AutoMigrationSpec> autoMigrationSpecsMap) {
    return Arrays.asList();
  }

  @Override
  public AppDao getAppDao() {
    if (_appDao != null) {
      return _appDao;
    } else {
      synchronized(this) {
        if(_appDao == null) {
          _appDao = new AppDao_Impl(this);
        }
        return _appDao;
      }
    }
  }
}
