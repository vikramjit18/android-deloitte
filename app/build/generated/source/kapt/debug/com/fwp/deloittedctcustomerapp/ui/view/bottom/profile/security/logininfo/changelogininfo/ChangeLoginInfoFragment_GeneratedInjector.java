package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.security.logininfo.changelogininfo;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ChangeLoginInfoFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ChangeLoginInfoFragment_GeneratedInjector {
  void injectChangeLoginInfoFragment(ChangeLoginInfoFragment changeLoginInfoFragment);
}
