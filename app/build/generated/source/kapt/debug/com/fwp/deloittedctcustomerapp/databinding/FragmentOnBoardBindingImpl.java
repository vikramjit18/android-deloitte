package com.fwp.deloittedctcustomerapp.databinding;
import com.fwp.deloittedctcustomerapp.R;
import com.fwp.deloittedctcustomerapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentOnBoardBindingImpl extends FragmentOnBoardBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.horizontal_line, 3);
        sViewsWithIds.put(R.id.horizontal_line4, 4);
        sViewsWithIds.put(R.id.login, 5);
        sViewsWithIds.put(R.id.signup, 6);
        sViewsWithIds.put(R.id.scrollView2, 7);
        sViewsWithIds.put(R.id.dctTagline, 8);
        sViewsWithIds.put(R.id.taglineDesc, 9);
        sViewsWithIds.put(R.id.onboardTitile1, 10);
        sViewsWithIds.put(R.id.onboardTxt1, 11);
        sViewsWithIds.put(R.id.onboardTitile2, 12);
        sViewsWithIds.put(R.id.onboardTxt2, 13);
        sViewsWithIds.put(R.id.onboardTitile3, 14);
        sViewsWithIds.put(R.id.onboardTxt3, 15);
        sViewsWithIds.put(R.id.onboardTitile4, 16);
        sViewsWithIds.put(R.id.onboardTxt4, 17);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentOnBoardBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 18, sIncludes, sViewsWithIds));
    }
    private FragmentOnBoardBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.LinearLayoutCompat) bindings[2]
            , (android.widget.TextView) bindings[8]
            , (bindings[3] != null) ? com.fwp.deloittedctcustomerapp.databinding.HorizontalLineBinding.bind((android.view.View) bindings[3]) : null
            , (bindings[4] != null) ? com.fwp.deloittedctcustomerapp.databinding.HorizontalLineBinding.bind((android.view.View) bindings[4]) : null
            , (bindings[5] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutButtonBinding.bind((android.view.View) bindings[5]) : null
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[16]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[17]
            , (androidx.core.widget.NestedScrollView) bindings[7]
            , (bindings[6] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutButtonBinding.bind((android.view.View) bindings[6]) : null
            , (android.widget.TextView) bindings[9]
            );
        this.constraintLayout3.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}