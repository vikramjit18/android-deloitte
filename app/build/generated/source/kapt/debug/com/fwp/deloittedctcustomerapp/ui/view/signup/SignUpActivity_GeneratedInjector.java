package com.fwp.deloittedctcustomerapp.ui.view.signup;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = SignUpActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
public interface SignUpActivity_GeneratedInjector {
  void injectSignUpActivity(SignUpActivity signUpActivity);
}
