package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = RequestEmailFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface RequestEmailFragment_GeneratedInjector {
  void injectRequestEmailFragment(RequestEmailFragment requestEmailFragment);
}
