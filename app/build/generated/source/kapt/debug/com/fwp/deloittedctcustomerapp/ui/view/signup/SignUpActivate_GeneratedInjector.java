package com.fwp.deloittedctcustomerapp.ui.view.signup;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = SignUpActivate.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface SignUpActivate_GeneratedInjector {
  void injectSignUpActivate(SignUpActivate signUpActivate);
}
