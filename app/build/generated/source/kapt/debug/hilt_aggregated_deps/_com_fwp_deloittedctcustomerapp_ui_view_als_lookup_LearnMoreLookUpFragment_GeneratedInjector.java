package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.FragmentComponent",
    entryPoints = "com.fwp.deloittedctcustomerapp.ui.view.als.lookup.LearnMoreLookUpFragment_GeneratedInjector"
)
public class _com_fwp_deloittedctcustomerapp_ui_view_als_lookup_LearnMoreLookUpFragment_GeneratedInjector {
}
