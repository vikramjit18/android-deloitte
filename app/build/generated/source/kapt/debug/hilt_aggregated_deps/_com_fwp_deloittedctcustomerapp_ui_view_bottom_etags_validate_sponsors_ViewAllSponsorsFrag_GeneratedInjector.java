package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.FragmentComponent",
    entryPoints = "com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.sponsors.ViewAllSponsorsFrag_GeneratedInjector"
)
public class _com_fwp_deloittedctcustomerapp_ui_view_bottom_etags_validate_sponsors_ViewAllSponsorsFrag_GeneratedInjector {
}
