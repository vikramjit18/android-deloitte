package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.components.SingletonComponent",
    modules = "com.fwp.deloittedctcustomerapp.data.hilt.NetworkModule"
)
public class _com_fwp_deloittedctcustomerapp_data_hilt_NetworkModule {
}
