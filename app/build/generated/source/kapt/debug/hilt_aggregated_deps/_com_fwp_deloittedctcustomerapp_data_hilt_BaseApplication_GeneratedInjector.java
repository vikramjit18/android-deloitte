package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.components.SingletonComponent",
    entryPoints = "com.fwp.deloittedctcustomerapp.data.hilt.BaseApplication_GeneratedInjector"
)
public class _com_fwp_deloittedctcustomerapp_data_hilt_BaseApplication_GeneratedInjector {
}
