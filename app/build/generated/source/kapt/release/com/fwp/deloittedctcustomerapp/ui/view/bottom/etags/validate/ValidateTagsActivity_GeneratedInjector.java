package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ValidateTagsActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
public interface ValidateTagsActivity_GeneratedInjector {
  void injectValidateTagsActivity(ValidateTagsActivity validateTagsActivity);
}
