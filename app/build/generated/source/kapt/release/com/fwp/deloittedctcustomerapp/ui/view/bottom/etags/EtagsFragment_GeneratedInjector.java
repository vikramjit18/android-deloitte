package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = EtagsFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface EtagsFragment_GeneratedInjector {
  void injectEtagsFragment(EtagsFragment etagsFragment);
}
