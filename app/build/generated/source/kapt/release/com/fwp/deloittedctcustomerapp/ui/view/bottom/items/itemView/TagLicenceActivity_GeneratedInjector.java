package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.itemView;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = TagLicenceActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
public interface TagLicenceActivity_GeneratedInjector {
  void injectTagLicenceActivity(TagLicenceActivity tagLicenceActivity);
}
