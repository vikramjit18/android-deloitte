package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = PrimaryAccountFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface PrimaryAccountFragment_GeneratedInjector {
  void injectPrimaryAccountFragment(PrimaryAccountFragment primaryAccountFragment);
}
