// Generated by Dagger (https://dagger.dev).
package com.fwp.deloittedctcustomerapp.data.hilt;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import retrofit2.converter.moshi.MoshiConverterFactory;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NetworkModule_ProvideMoshiConvertorFactoryFactory implements Factory<MoshiConverterFactory> {
  @Override
  public MoshiConverterFactory get() {
    return provideMoshiConvertorFactory();
  }

  public static NetworkModule_ProvideMoshiConvertorFactoryFactory create() {
    return InstanceHolder.INSTANCE;
  }

  public static MoshiConverterFactory provideMoshiConvertorFactory() {
    return Preconditions.checkNotNullFromProvides(NetworkModule.INSTANCE.provideMoshiConvertorFactory());
  }

  private static final class InstanceHolder {
    private static final NetworkModule_ProvideMoshiConvertorFactoryFactory INSTANCE = new NetworkModule_ProvideMoshiConvertorFactoryFactory();
  }
}
