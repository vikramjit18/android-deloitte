package com.fwp.deloittedctcustomerapp.databinding;
import com.fwp.deloittedctcustomerapp.R;
import com.fwp.deloittedctcustomerapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class SignUpDetailsFragmentBindingImpl extends SignUpDetailsFragmentBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.et_firstName, 3);
        sViewsWithIds.put(R.id.et_middle_name, 4);
        sViewsWithIds.put(R.id.et_last_name, 5);
        sViewsWithIds.put(R.id.next, 6);
        sViewsWithIds.put(R.id.back, 7);
        sViewsWithIds.put(R.id.linearProgressIndicator2, 8);
        sViewsWithIds.put(R.id.nestedScrollView5, 9);
        sViewsWithIds.put(R.id.agreement_title, 10);
        sViewsWithIds.put(R.id.textView5, 11);
        sViewsWithIds.put(R.id.tv_first_name, 12);
        sViewsWithIds.put(R.id.tv_middle_name, 13);
        sViewsWithIds.put(R.id.tv_last_name, 14);
        sViewsWithIds.put(R.id.tv_email, 15);
        sViewsWithIds.put(R.id.emailLayout, 16);
        sViewsWithIds.put(R.id.email, 17);
        sViewsWithIds.put(R.id.conEmail, 18);
        sViewsWithIds.put(R.id.aonEmailLayout, 19);
        sViewsWithIds.put(R.id.confirmEmail, 20);
    }
    // views
    @Nullable
    private final com.fwp.deloittedctcustomerapp.databinding.LayoutLoginBgBinding mboundView0;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView01;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public SignUpDetailsFragmentBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private SignUpDetailsFragmentBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[10]
            , (com.google.android.material.textfield.TextInputLayout) bindings[19]
            , (android.widget.ImageView) bindings[7]
            , (android.widget.TextView) bindings[18]
            , (com.google.android.material.textfield.TextInputEditText) bindings[20]
            , (com.google.android.material.textfield.TextInputEditText) bindings[17]
            , (com.google.android.material.textfield.TextInputLayout) bindings[16]
            , (bindings[3] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutEdittextBinding.bind((android.view.View) bindings[3]) : null
            , (bindings[5] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutEdittextBinding.bind((android.view.View) bindings[5]) : null
            , (bindings[4] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutEdittextBinding.bind((android.view.View) bindings[4]) : null
            , (com.google.android.material.progressindicator.LinearProgressIndicator) bindings[8]
            , (androidx.core.widget.NestedScrollView) bindings[9]
            , (bindings[6] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutButtonBinding.bind((android.view.View) bindings[6]) : null
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[13]
            );
        this.mboundView0 = (bindings[2] != null) ? com.fwp.deloittedctcustomerapp.databinding.LayoutLoginBgBinding.bind((android.view.View) bindings[2]) : null;
        this.mboundView01 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView01.setTag(null);
        this.mboundView1 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[1];
        this.mboundView1.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}