package com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.editprimary;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = EditPrimaryFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface EditPrimaryFragment_GeneratedInjector {
  void injectEditPrimaryFragment(EditPrimaryFragment editPrimaryFragment);
}
