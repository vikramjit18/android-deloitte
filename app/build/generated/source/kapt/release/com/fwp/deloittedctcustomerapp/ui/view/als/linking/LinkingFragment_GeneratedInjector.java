package com.fwp.deloittedctcustomerapp.ui.view.als.linking;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = LinkingFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface LinkingFragment_GeneratedInjector {
  void injectLinkingFragment(LinkingFragment linkingFragment);
}
