package com.fwp.deloittedctcustomerapp.ui.view.resetPass;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ResetPassActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
public interface ResetPassActivity_GeneratedInjector {
  void injectResetPassActivity(ResetPassActivity resetPassActivity);
}
