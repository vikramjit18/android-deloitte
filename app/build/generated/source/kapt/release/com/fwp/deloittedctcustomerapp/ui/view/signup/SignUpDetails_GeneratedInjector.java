package com.fwp.deloittedctcustomerapp.ui.view.signup;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = SignUpDetails.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface SignUpDetails_GeneratedInjector {
  void injectSignUpDetails(SignUpDetails signUpDetails);
}
