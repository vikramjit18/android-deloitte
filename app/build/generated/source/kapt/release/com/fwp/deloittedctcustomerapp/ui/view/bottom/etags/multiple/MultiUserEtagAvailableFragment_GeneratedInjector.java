package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.multiple;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = MultiUserEtagAvailableFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface MultiUserEtagAvailableFragment_GeneratedInjector {
  void injectMultiUserEtagAvailableFragment(
      MultiUserEtagAvailableFragment multiUserEtagAvailableFragment);
}
