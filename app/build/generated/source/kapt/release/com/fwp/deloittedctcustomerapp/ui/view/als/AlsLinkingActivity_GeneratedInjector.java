package com.fwp.deloittedctcustomerapp.ui.view.als;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = AlsLinkingActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
public interface AlsLinkingActivity_GeneratedInjector {
  void injectAlsLinkingActivity(AlsLinkingActivity alsLinkingActivity);
}
