// Code generated by moshi-kotlin-codegen. Do not edit.
@file:Suppress("DEPRECATION", "unused", "ClassName", "REDUNDANT_PROJECTION",
    "RedundantExplicitType", "LocalVariableName", "RedundantVisibilityModifier",
    "PLATFORM_CLASS_MAPPED_TO_KOTLIN", "IMPLICIT_NOTHING_TYPE_ARGUMENT_IN_RETURN_POSITION")

package com.fwp.deloittedctcustomerapp.`data`.model.requests

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.`internal`.Util
import java.lang.NullPointerException
import kotlin.String
import kotlin.Suppress
import kotlin.Unit
import kotlin.collections.emptySet
import kotlin.text.buildString

public class UserProfileRequestJsonAdapter(
  moshi: Moshi
) : JsonAdapter<UserProfileRequest>() {
  private val options: JsonReader.Options = JsonReader.Options.of("jwt")

  private val stringAdapter: JsonAdapter<String> = moshi.adapter(String::class.java, emptySet(),
      "jwt")

  public override fun toString(): String = buildString(40) {
      append("GeneratedJsonAdapter(").append("UserProfileRequest").append(')') }

  public override fun fromJson(reader: JsonReader): UserProfileRequest {
    var jwt: String? = null
    reader.beginObject()
    while (reader.hasNext()) {
      when (reader.selectName(options)) {
        0 -> jwt = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("jwt", "jwt", reader)
        -1 -> {
          // Unknown name, skip it.
          reader.skipName()
          reader.skipValue()
        }
      }
    }
    reader.endObject()
    return UserProfileRequest(
        jwt = jwt ?: throw Util.missingProperty("jwt", "jwt", reader)
    )
  }

  public override fun toJson(writer: JsonWriter, value_: UserProfileRequest?): Unit {
    if (value_ == null) {
      throw NullPointerException("value_ was null! Wrap in .nullSafe() to write nullable values.")
    }
    writer.beginObject()
    writer.name("jwt")
    stringAdapter.toJson(writer, value_.jwt)
    writer.endObject()
  }
}
