package com.fwp.deloittedctcustomerapp;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.fwp.deloittedctcustomerapp.databinding.ActivityTagLicenceBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.ActivityTagPermitBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.ConfirmAlsFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.EtagsAvailableChildRowBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.ForgotHelpFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.ForgotInitialFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.ForgotResetFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.ForgotSuccessFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.FragmentLoginAgreementBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.FragmentLoginBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.FragmentOnBoardBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.FragmentRequestEmailBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.FragmentSignUpAgreementBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.FragmentStoryListBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.FyiTermsBottomSheetBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.LayoutNoAccountSelectedBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.LearnMoreLookUpFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.LinkedEtagsFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.LinkedItemsFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.LinkingFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.LookUpAlsFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.MultiUserEtagAvailableFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.MultiUserItemsFragmnetFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.SignUpActivateFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.SignUpDetailsFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.SignUpInitialsFragmentBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.SponsorRowBindingImpl;
import com.fwp.deloittedctcustomerapp.databinding.ViewAllSponsorsFragmentBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYTAGLICENCE = 1;

  private static final int LAYOUT_ACTIVITYTAGPERMIT = 2;

  private static final int LAYOUT_CONFIRMALSFRAGMENT = 3;

  private static final int LAYOUT_ETAGSAVAILABLECHILDROW = 4;

  private static final int LAYOUT_FORGOTHELPFRAGMENT = 5;

  private static final int LAYOUT_FORGOTINITIALFRAGMENT = 6;

  private static final int LAYOUT_FORGOTRESETFRAGMENT = 7;

  private static final int LAYOUT_FORGOTSUCCESSFRAGMENT = 8;

  private static final int LAYOUT_FRAGMENTLOGIN = 9;

  private static final int LAYOUT_FRAGMENTLOGINAGREEMENT = 10;

  private static final int LAYOUT_FRAGMENTONBOARD = 11;

  private static final int LAYOUT_FRAGMENTREQUESTEMAIL = 12;

  private static final int LAYOUT_FRAGMENTSIGNUPAGREEMENT = 13;

  private static final int LAYOUT_FRAGMENTSTORYLIST = 14;

  private static final int LAYOUT_FYITERMSBOTTOMSHEET = 15;

  private static final int LAYOUT_LAYOUTNOACCOUNTSELECTED = 16;

  private static final int LAYOUT_LEARNMORELOOKUPFRAGMENT = 17;

  private static final int LAYOUT_LINKEDETAGSFRAGMENT = 18;

  private static final int LAYOUT_LINKEDITEMSFRAGMENT = 19;

  private static final int LAYOUT_LINKINGFRAGMENT = 20;

  private static final int LAYOUT_LOOKUPALSFRAGMENT = 21;

  private static final int LAYOUT_MULTIUSERETAGAVAILABLEFRAGMENT = 22;

  private static final int LAYOUT_MULTIUSERITEMSFRAGMNETFRAGMENT = 23;

  private static final int LAYOUT_SIGNUPACTIVATEFRAGMENT = 24;

  private static final int LAYOUT_SIGNUPDETAILSFRAGMENT = 25;

  private static final int LAYOUT_SIGNUPINITIALSFRAGMENT = 26;

  private static final int LAYOUT_SPONSORROW = 27;

  private static final int LAYOUT_VIEWALLSPONSORSFRAGMENT = 28;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(28);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.activity_tag_licence, LAYOUT_ACTIVITYTAGLICENCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.activity_tag_permit, LAYOUT_ACTIVITYTAGPERMIT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.confirm_als_fragment, LAYOUT_CONFIRMALSFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.etags_available_child_row, LAYOUT_ETAGSAVAILABLECHILDROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.forgot_help_fragment, LAYOUT_FORGOTHELPFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.forgot_initial_fragment, LAYOUT_FORGOTINITIALFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.forgot_reset_fragment, LAYOUT_FORGOTRESETFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.forgot_success_fragment, LAYOUT_FORGOTSUCCESSFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.fragment_login, LAYOUT_FRAGMENTLOGIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.fragment_login_agreement, LAYOUT_FRAGMENTLOGINAGREEMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.fragment_on_board, LAYOUT_FRAGMENTONBOARD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.fragment_request_email, LAYOUT_FRAGMENTREQUESTEMAIL);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.fragment_sign_up_agreement, LAYOUT_FRAGMENTSIGNUPAGREEMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.fragment_story_list, LAYOUT_FRAGMENTSTORYLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.fyi_terms_bottom_sheet, LAYOUT_FYITERMSBOTTOMSHEET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.layout_no_account_selected, LAYOUT_LAYOUTNOACCOUNTSELECTED);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.learn_more_look_up_fragment, LAYOUT_LEARNMORELOOKUPFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.linked_etags_fragment, LAYOUT_LINKEDETAGSFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.linked_items_fragment, LAYOUT_LINKEDITEMSFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.linking_fragment, LAYOUT_LINKINGFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.look_up_als_fragment, LAYOUT_LOOKUPALSFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.multi_user_etag_available_fragment, LAYOUT_MULTIUSERETAGAVAILABLEFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.multi_user_items_fragmnet_fragment, LAYOUT_MULTIUSERITEMSFRAGMNETFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.sign_up_activate_fragment, LAYOUT_SIGNUPACTIVATEFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.sign_up_details_fragment, LAYOUT_SIGNUPDETAILSFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.sign_up_initials_fragment, LAYOUT_SIGNUPINITIALSFRAGMENT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.sponsor_row, LAYOUT_SPONSORROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.fwp.deloittedctcustomerapp.R.layout.view_all_sponsors_fragment, LAYOUT_VIEWALLSPONSORSFRAGMENT);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYTAGLICENCE: {
          if ("layout/activity_tag_licence_0".equals(tag)) {
            return new ActivityTagLicenceBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_tag_licence is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYTAGPERMIT: {
          if ("layout/activity_tag_permit_0".equals(tag)) {
            return new ActivityTagPermitBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_tag_permit is invalid. Received: " + tag);
        }
        case  LAYOUT_CONFIRMALSFRAGMENT: {
          if ("layout/confirm_als_fragment_0".equals(tag)) {
            return new ConfirmAlsFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for confirm_als_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_ETAGSAVAILABLECHILDROW: {
          if ("layout/etags_available_child_row_0".equals(tag)) {
            return new EtagsAvailableChildRowBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for etags_available_child_row is invalid. Received: " + tag);
        }
        case  LAYOUT_FORGOTHELPFRAGMENT: {
          if ("layout/forgot_help_fragment_0".equals(tag)) {
            return new ForgotHelpFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for forgot_help_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_FORGOTINITIALFRAGMENT: {
          if ("layout/forgot_initial_fragment_0".equals(tag)) {
            return new ForgotInitialFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for forgot_initial_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_FORGOTRESETFRAGMENT: {
          if ("layout/forgot_reset_fragment_0".equals(tag)) {
            return new ForgotResetFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for forgot_reset_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_FORGOTSUCCESSFRAGMENT: {
          if ("layout/forgot_success_fragment_0".equals(tag)) {
            return new ForgotSuccessFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for forgot_success_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTLOGIN: {
          if ("layout/fragment_login_0".equals(tag)) {
            return new FragmentLoginBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_login is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTLOGINAGREEMENT: {
          if ("layout/fragment_login_agreement_0".equals(tag)) {
            return new FragmentLoginAgreementBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_login_agreement is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTONBOARD: {
          if ("layout/fragment_on_board_0".equals(tag)) {
            return new FragmentOnBoardBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_on_board is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTREQUESTEMAIL: {
          if ("layout/fragment_request_email_0".equals(tag)) {
            return new FragmentRequestEmailBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_request_email is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSIGNUPAGREEMENT: {
          if ("layout/fragment_sign_up_agreement_0".equals(tag)) {
            return new FragmentSignUpAgreementBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_sign_up_agreement is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSTORYLIST: {
          if ("layout/fragment_story_list_0".equals(tag)) {
            return new FragmentStoryListBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_story_list is invalid. Received: " + tag);
        }
        case  LAYOUT_FYITERMSBOTTOMSHEET: {
          if ("layout/fyi_terms_bottom_sheet_0".equals(tag)) {
            return new FyiTermsBottomSheetBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fyi_terms_bottom_sheet is invalid. Received: " + tag);
        }
        case  LAYOUT_LAYOUTNOACCOUNTSELECTED: {
          if ("layout/layout_no_account_selected_0".equals(tag)) {
            return new LayoutNoAccountSelectedBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for layout_no_account_selected is invalid. Received: " + tag);
        }
        case  LAYOUT_LEARNMORELOOKUPFRAGMENT: {
          if ("layout/learn_more_look_up_fragment_0".equals(tag)) {
            return new LearnMoreLookUpFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for learn_more_look_up_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_LINKEDETAGSFRAGMENT: {
          if ("layout/linked_etags_fragment_0".equals(tag)) {
            return new LinkedEtagsFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for linked_etags_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_LINKEDITEMSFRAGMENT: {
          if ("layout/linked_items_fragment_0".equals(tag)) {
            return new LinkedItemsFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for linked_items_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_LINKINGFRAGMENT: {
          if ("layout/linking_fragment_0".equals(tag)) {
            return new LinkingFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for linking_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_LOOKUPALSFRAGMENT: {
          if ("layout/look_up_als_fragment_0".equals(tag)) {
            return new LookUpAlsFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for look_up_als_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_MULTIUSERETAGAVAILABLEFRAGMENT: {
          if ("layout/multi_user_etag_available_fragment_0".equals(tag)) {
            return new MultiUserEtagAvailableFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for multi_user_etag_available_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_MULTIUSERITEMSFRAGMNETFRAGMENT: {
          if ("layout/multi_user_items_fragmnet_fragment_0".equals(tag)) {
            return new MultiUserItemsFragmnetFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for multi_user_items_fragmnet_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_SIGNUPACTIVATEFRAGMENT: {
          if ("layout/sign_up_activate_fragment_0".equals(tag)) {
            return new SignUpActivateFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for sign_up_activate_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_SIGNUPDETAILSFRAGMENT: {
          if ("layout/sign_up_details_fragment_0".equals(tag)) {
            return new SignUpDetailsFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for sign_up_details_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_SIGNUPINITIALSFRAGMENT: {
          if ("layout/sign_up_initials_fragment_0".equals(tag)) {
            return new SignUpInitialsFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for sign_up_initials_fragment is invalid. Received: " + tag);
        }
        case  LAYOUT_SPONSORROW: {
          if ("layout/sponsor_row_0".equals(tag)) {
            return new SponsorRowBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for sponsor_row is invalid. Received: " + tag);
        }
        case  LAYOUT_VIEWALLSPONSORSFRAGMENT: {
          if ("layout/view_all_sponsors_fragment_0".equals(tag)) {
            return new ViewAllSponsorsFragmentBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for view_all_sponsors_fragment is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(2);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "viewModel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(28);

    static {
      sKeys.put("layout/activity_tag_licence_0", com.fwp.deloittedctcustomerapp.R.layout.activity_tag_licence);
      sKeys.put("layout/activity_tag_permit_0", com.fwp.deloittedctcustomerapp.R.layout.activity_tag_permit);
      sKeys.put("layout/confirm_als_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.confirm_als_fragment);
      sKeys.put("layout/etags_available_child_row_0", com.fwp.deloittedctcustomerapp.R.layout.etags_available_child_row);
      sKeys.put("layout/forgot_help_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.forgot_help_fragment);
      sKeys.put("layout/forgot_initial_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.forgot_initial_fragment);
      sKeys.put("layout/forgot_reset_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.forgot_reset_fragment);
      sKeys.put("layout/forgot_success_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.forgot_success_fragment);
      sKeys.put("layout/fragment_login_0", com.fwp.deloittedctcustomerapp.R.layout.fragment_login);
      sKeys.put("layout/fragment_login_agreement_0", com.fwp.deloittedctcustomerapp.R.layout.fragment_login_agreement);
      sKeys.put("layout/fragment_on_board_0", com.fwp.deloittedctcustomerapp.R.layout.fragment_on_board);
      sKeys.put("layout/fragment_request_email_0", com.fwp.deloittedctcustomerapp.R.layout.fragment_request_email);
      sKeys.put("layout/fragment_sign_up_agreement_0", com.fwp.deloittedctcustomerapp.R.layout.fragment_sign_up_agreement);
      sKeys.put("layout/fragment_story_list_0", com.fwp.deloittedctcustomerapp.R.layout.fragment_story_list);
      sKeys.put("layout/fyi_terms_bottom_sheet_0", com.fwp.deloittedctcustomerapp.R.layout.fyi_terms_bottom_sheet);
      sKeys.put("layout/layout_no_account_selected_0", com.fwp.deloittedctcustomerapp.R.layout.layout_no_account_selected);
      sKeys.put("layout/learn_more_look_up_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.learn_more_look_up_fragment);
      sKeys.put("layout/linked_etags_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.linked_etags_fragment);
      sKeys.put("layout/linked_items_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.linked_items_fragment);
      sKeys.put("layout/linking_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.linking_fragment);
      sKeys.put("layout/look_up_als_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.look_up_als_fragment);
      sKeys.put("layout/multi_user_etag_available_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.multi_user_etag_available_fragment);
      sKeys.put("layout/multi_user_items_fragmnet_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.multi_user_items_fragmnet_fragment);
      sKeys.put("layout/sign_up_activate_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.sign_up_activate_fragment);
      sKeys.put("layout/sign_up_details_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.sign_up_details_fragment);
      sKeys.put("layout/sign_up_initials_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.sign_up_initials_fragment);
      sKeys.put("layout/sponsor_row_0", com.fwp.deloittedctcustomerapp.R.layout.sponsor_row);
      sKeys.put("layout/view_all_sponsors_fragment_0", com.fwp.deloittedctcustomerapp.R.layout.view_all_sponsors_fragment);
    }
  }
}
