package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ReviewHarvestFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ReviewHarvestFragment_GeneratedInjector {
  void injectReviewHarvestFragment(ReviewHarvestFragment reviewHarvestFragment);
}
