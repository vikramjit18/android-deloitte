package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.primary;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = EtagsAvailableFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface EtagsAvailableFragment_GeneratedInjector {
  void injectEtagsAvailableFragment(EtagsAvailableFragment etagsAvailableFragment);
}
