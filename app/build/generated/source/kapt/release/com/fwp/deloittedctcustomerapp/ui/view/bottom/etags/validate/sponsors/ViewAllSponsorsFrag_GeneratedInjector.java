package com.fwp.deloittedctcustomerapp.ui.view.bottom.etags.validate.sponsors;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = ViewAllSponsorsFrag.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface ViewAllSponsorsFrag_GeneratedInjector {
  void injectViewAllSponsorsFrag(ViewAllSponsorsFrag viewAllSponsorsFrag);
}
