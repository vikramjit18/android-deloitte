package com.fwp.deloittedctcustomerapp.data.db;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.CoroutinesRoom;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.fwp.deloittedctcustomerapp.data.model.DownloadedTags;
import com.fwp.deloittedctcustomerapp.data.model.requests.ValidateEtagRequest;
import java.lang.Class;
import java.lang.Double;
import java.lang.Exception;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.Unit;
import kotlin.coroutines.Continuation;

@SuppressWarnings({"unchecked", "deprecation"})
public final class AppDao_Impl implements AppDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<ValidateEtagRequest> __insertionAdapterOfValidateEtagRequest;

  private final EntityInsertionAdapter<DownloadedTags> __insertionAdapterOfDownloadedTags;

  private final SharedSQLiteStatement __preparedStmtOfDeleteValidateRequest;

  private final SharedSQLiteStatement __preparedStmtOfDeleteDownloadedTagsTable;

  private final SharedSQLiteStatement __preparedStmtOfDeleteDownloadedTagsById;

  public AppDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfValidateEtagRequest = new EntityInsertionAdapter<ValidateEtagRequest>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `validate_request` (`id`,`sync`,`device_id`,`etag_upload_date`,`harvestConfNum`,`harvested_date`,`lat`,`longt`,`speciesCd`,`spi_id`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ValidateEtagRequest value) {
        stmt.bindLong(1, value.getId());
        stmt.bindLong(2, value.getSync());
        if (value.getDeviceId() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getDeviceId());
        }
        if (value.getEtagUploadDate() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getEtagUploadDate());
        }
        if (value.getHarvestConfNum() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getHarvestConfNum());
        }
        if (value.getHarvestedDate() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getHarvestedDate());
        }
        if (value.getLat() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindDouble(7, value.getLat());
        }
        if (value.getLongt() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindDouble(8, value.getLongt());
        }
        if (value.getSpeciesCd() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getSpeciesCd());
        }
        if (value.getSpiId() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getSpiId());
        }
      }
    };
    this.__insertionAdapterOfDownloadedTags = new EntityInsertionAdapter<DownloadedTags>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `downloaded_tags` (`id`,`downloadedSpiId`) VALUES (nullif(?, 0),?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, DownloadedTags value) {
        stmt.bindLong(1, value.getId());
        if (value.getDownloadedSpiId() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getDownloadedSpiId());
        }
      }
    };
    this.__preparedStmtOfDeleteValidateRequest = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM validate_request";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteDownloadedTagsTable = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM downloaded_tags";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteDownloadedTagsById = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM downloaded_tags where downloadedSpiId = ?";
        return _query;
      }
    };
  }

  @Override
  public Object insertValidateRequest(final ValidateEtagRequest validateEtagRequest,
      final Continuation<? super Unit> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        __db.beginTransaction();
        try {
          __insertionAdapterOfValidateEtagRequest.insert(validateEtagRequest);
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
        }
      }
    }, continuation);
  }

  @Override
  public Object insertDownloadTags(final DownloadedTags downloadedTags,
      final Continuation<? super Unit> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        __db.beginTransaction();
        try {
          __insertionAdapterOfDownloadedTags.insert(downloadedTags);
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
        }
      }
    }, continuation);
  }

  @Override
  public Object deleteValidateRequest(final Continuation<? super Unit> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteValidateRequest.acquire();
        __db.beginTransaction();
        try {
          _stmt.executeUpdateDelete();
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
          __preparedStmtOfDeleteValidateRequest.release(_stmt);
        }
      }
    }, continuation);
  }

  @Override
  public void deleteDownloadedTagsTable() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteDownloadedTagsTable.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteDownloadedTagsTable.release(_stmt);
    }
  }

  @Override
  public Object deleteDownloadedTagsById(final String onlineSpiId,
      final Continuation<? super Unit> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteDownloadedTagsById.acquire();
        int _argIndex = 1;
        if (onlineSpiId == null) {
          _stmt.bindNull(_argIndex);
        } else {
          _stmt.bindString(_argIndex, onlineSpiId);
        }
        __db.beginTransaction();
        try {
          _stmt.executeUpdateDelete();
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
          __preparedStmtOfDeleteDownloadedTagsById.release(_stmt);
        }
      }
    }, continuation);
  }

  @Override
  public LiveData<List<ValidateEtagRequest>> getValidateRequest() {
    final String _sql = "SELECT * FROM validate_request";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"validate_request"}, false, new Callable<List<ValidateEtagRequest>>() {
      @Override
      public List<ValidateEtagRequest> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfSync = CursorUtil.getColumnIndexOrThrow(_cursor, "sync");
          final int _cursorIndexOfDeviceId = CursorUtil.getColumnIndexOrThrow(_cursor, "device_id");
          final int _cursorIndexOfEtagUploadDate = CursorUtil.getColumnIndexOrThrow(_cursor, "etag_upload_date");
          final int _cursorIndexOfHarvestConfNum = CursorUtil.getColumnIndexOrThrow(_cursor, "harvestConfNum");
          final int _cursorIndexOfHarvestedDate = CursorUtil.getColumnIndexOrThrow(_cursor, "harvested_date");
          final int _cursorIndexOfLat = CursorUtil.getColumnIndexOrThrow(_cursor, "lat");
          final int _cursorIndexOfLongt = CursorUtil.getColumnIndexOrThrow(_cursor, "longt");
          final int _cursorIndexOfSpeciesCd = CursorUtil.getColumnIndexOrThrow(_cursor, "speciesCd");
          final int _cursorIndexOfSpiId = CursorUtil.getColumnIndexOrThrow(_cursor, "spi_id");
          final List<ValidateEtagRequest> _result = new ArrayList<ValidateEtagRequest>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final ValidateEtagRequest _item;
            final int _tmpId;
            _tmpId = _cursor.getInt(_cursorIndexOfId);
            final int _tmpSync;
            _tmpSync = _cursor.getInt(_cursorIndexOfSync);
            final String _tmpDeviceId;
            if (_cursor.isNull(_cursorIndexOfDeviceId)) {
              _tmpDeviceId = null;
            } else {
              _tmpDeviceId = _cursor.getString(_cursorIndexOfDeviceId);
            }
            final String _tmpEtagUploadDate;
            if (_cursor.isNull(_cursorIndexOfEtagUploadDate)) {
              _tmpEtagUploadDate = null;
            } else {
              _tmpEtagUploadDate = _cursor.getString(_cursorIndexOfEtagUploadDate);
            }
            final String _tmpHarvestConfNum;
            if (_cursor.isNull(_cursorIndexOfHarvestConfNum)) {
              _tmpHarvestConfNum = null;
            } else {
              _tmpHarvestConfNum = _cursor.getString(_cursorIndexOfHarvestConfNum);
            }
            final String _tmpHarvestedDate;
            if (_cursor.isNull(_cursorIndexOfHarvestedDate)) {
              _tmpHarvestedDate = null;
            } else {
              _tmpHarvestedDate = _cursor.getString(_cursorIndexOfHarvestedDate);
            }
            final Double _tmpLat;
            if (_cursor.isNull(_cursorIndexOfLat)) {
              _tmpLat = null;
            } else {
              _tmpLat = _cursor.getDouble(_cursorIndexOfLat);
            }
            final Double _tmpLongt;
            if (_cursor.isNull(_cursorIndexOfLongt)) {
              _tmpLongt = null;
            } else {
              _tmpLongt = _cursor.getDouble(_cursorIndexOfLongt);
            }
            final String _tmpSpeciesCd;
            if (_cursor.isNull(_cursorIndexOfSpeciesCd)) {
              _tmpSpeciesCd = null;
            } else {
              _tmpSpeciesCd = _cursor.getString(_cursorIndexOfSpeciesCd);
            }
            final String _tmpSpiId;
            if (_cursor.isNull(_cursorIndexOfSpiId)) {
              _tmpSpiId = null;
            } else {
              _tmpSpiId = _cursor.getString(_cursorIndexOfSpiId);
            }
            _item = new ValidateEtagRequest(_tmpId,_tmpSync,_tmpDeviceId,_tmpEtagUploadDate,_tmpHarvestConfNum,_tmpHarvestedDate,_tmpLat,_tmpLongt,_tmpSpeciesCd,_tmpSpiId);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<List<ValidateEtagRequest>> getUnSyncedValidateRequest() {
    final String _sql = "SELECT * FROM validate_request where sync=0";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"validate_request"}, false, new Callable<List<ValidateEtagRequest>>() {
      @Override
      public List<ValidateEtagRequest> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfSync = CursorUtil.getColumnIndexOrThrow(_cursor, "sync");
          final int _cursorIndexOfDeviceId = CursorUtil.getColumnIndexOrThrow(_cursor, "device_id");
          final int _cursorIndexOfEtagUploadDate = CursorUtil.getColumnIndexOrThrow(_cursor, "etag_upload_date");
          final int _cursorIndexOfHarvestConfNum = CursorUtil.getColumnIndexOrThrow(_cursor, "harvestConfNum");
          final int _cursorIndexOfHarvestedDate = CursorUtil.getColumnIndexOrThrow(_cursor, "harvested_date");
          final int _cursorIndexOfLat = CursorUtil.getColumnIndexOrThrow(_cursor, "lat");
          final int _cursorIndexOfLongt = CursorUtil.getColumnIndexOrThrow(_cursor, "longt");
          final int _cursorIndexOfSpeciesCd = CursorUtil.getColumnIndexOrThrow(_cursor, "speciesCd");
          final int _cursorIndexOfSpiId = CursorUtil.getColumnIndexOrThrow(_cursor, "spi_id");
          final List<ValidateEtagRequest> _result = new ArrayList<ValidateEtagRequest>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final ValidateEtagRequest _item;
            final int _tmpId;
            _tmpId = _cursor.getInt(_cursorIndexOfId);
            final int _tmpSync;
            _tmpSync = _cursor.getInt(_cursorIndexOfSync);
            final String _tmpDeviceId;
            if (_cursor.isNull(_cursorIndexOfDeviceId)) {
              _tmpDeviceId = null;
            } else {
              _tmpDeviceId = _cursor.getString(_cursorIndexOfDeviceId);
            }
            final String _tmpEtagUploadDate;
            if (_cursor.isNull(_cursorIndexOfEtagUploadDate)) {
              _tmpEtagUploadDate = null;
            } else {
              _tmpEtagUploadDate = _cursor.getString(_cursorIndexOfEtagUploadDate);
            }
            final String _tmpHarvestConfNum;
            if (_cursor.isNull(_cursorIndexOfHarvestConfNum)) {
              _tmpHarvestConfNum = null;
            } else {
              _tmpHarvestConfNum = _cursor.getString(_cursorIndexOfHarvestConfNum);
            }
            final String _tmpHarvestedDate;
            if (_cursor.isNull(_cursorIndexOfHarvestedDate)) {
              _tmpHarvestedDate = null;
            } else {
              _tmpHarvestedDate = _cursor.getString(_cursorIndexOfHarvestedDate);
            }
            final Double _tmpLat;
            if (_cursor.isNull(_cursorIndexOfLat)) {
              _tmpLat = null;
            } else {
              _tmpLat = _cursor.getDouble(_cursorIndexOfLat);
            }
            final Double _tmpLongt;
            if (_cursor.isNull(_cursorIndexOfLongt)) {
              _tmpLongt = null;
            } else {
              _tmpLongt = _cursor.getDouble(_cursorIndexOfLongt);
            }
            final String _tmpSpeciesCd;
            if (_cursor.isNull(_cursorIndexOfSpeciesCd)) {
              _tmpSpeciesCd = null;
            } else {
              _tmpSpeciesCd = _cursor.getString(_cursorIndexOfSpeciesCd);
            }
            final String _tmpSpiId;
            if (_cursor.isNull(_cursorIndexOfSpiId)) {
              _tmpSpiId = null;
            } else {
              _tmpSpiId = _cursor.getString(_cursorIndexOfSpiId);
            }
            _item = new ValidateEtagRequest(_tmpId,_tmpSync,_tmpDeviceId,_tmpEtagUploadDate,_tmpHarvestConfNum,_tmpHarvestedDate,_tmpLat,_tmpLongt,_tmpSpeciesCd,_tmpSpiId);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<List<DownloadedTags>> getDownloadedTags() {
    final String _sql = "SELECT * FROM downloaded_tags";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"downloaded_tags"}, false, new Callable<List<DownloadedTags>>() {
      @Override
      public List<DownloadedTags> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfDownloadedSpiId = CursorUtil.getColumnIndexOrThrow(_cursor, "downloadedSpiId");
          final List<DownloadedTags> _result = new ArrayList<DownloadedTags>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final DownloadedTags _item;
            final int _tmpId;
            _tmpId = _cursor.getInt(_cursorIndexOfId);
            final String _tmpDownloadedSpiId;
            if (_cursor.isNull(_cursorIndexOfDownloadedSpiId)) {
              _tmpDownloadedSpiId = null;
            } else {
              _tmpDownloadedSpiId = _cursor.getString(_cursorIndexOfDownloadedSpiId);
            }
            _item = new DownloadedTags(_tmpId,_tmpDownloadedSpiId);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
