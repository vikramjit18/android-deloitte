package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = RequestEmailActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
public interface RequestEmailActivity_GeneratedInjector {
  void injectRequestEmailActivity(RequestEmailActivity requestEmailActivity);
}
