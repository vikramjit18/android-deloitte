// Generated by Dagger (https://dagger.dev).
package com.fwp.deloittedctcustomerapp.ui.view.bottom.items.requestEmail;

import com.fwp.deloittedctcustomerapp.data.repo.MainRepo;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class RequestEmailViewModel_Factory implements Factory<RequestEmailViewModel> {
  private final Provider<MainRepo> mainRepoProvider;

  public RequestEmailViewModel_Factory(Provider<MainRepo> mainRepoProvider) {
    this.mainRepoProvider = mainRepoProvider;
  }

  @Override
  public RequestEmailViewModel get() {
    return newInstance(mainRepoProvider.get());
  }

  public static RequestEmailViewModel_Factory create(Provider<MainRepo> mainRepoProvider) {
    return new RequestEmailViewModel_Factory(mainRepoProvider);
  }

  public static RequestEmailViewModel newInstance(MainRepo mainRepo) {
    return new RequestEmailViewModel(mainRepo);
  }
}
