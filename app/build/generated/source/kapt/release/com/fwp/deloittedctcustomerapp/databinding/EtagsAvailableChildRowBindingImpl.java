package com.fwp.deloittedctcustomerapp.databinding;
import com.fwp.deloittedctcustomerapp.R;
import com.fwp.deloittedctcustomerapp.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class EtagsAvailableChildRowBindingImpl extends EtagsAvailableChildRowBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.etag_issue, 2);
        sViewsWithIds.put(R.id.etagValidated, 3);
        sViewsWithIds.put(R.id.etag_void, 4);
        sViewsWithIds.put(R.id.etag_refunded, 5);
        sViewsWithIds.put(R.id.etag_expire, 6);
        sViewsWithIds.put(R.id.relativeLayout, 7);
        sViewsWithIds.put(R.id.tagImage, 8);
        sViewsWithIds.put(R.id.tagDetailsContainer, 9);
        sViewsWithIds.put(R.id.year_animal_type, 10);
        sViewsWithIds.put(R.id.region, 11);
        sViewsWithIds.put(R.id.downloadButtonContainer, 12);
        sViewsWithIds.put(R.id.validateButton, 13);
        sViewsWithIds.put(R.id.iconProgressButton, 14);
        sViewsWithIds.put(R.id.iconDownloadButton, 15);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public EtagsAvailableChildRowBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 16, sIncludes, sViewsWithIds));
    }
    private EtagsAvailableChildRowBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.LinearLayout) bindings[12]
            , (bindings[6] != null) ? com.fwp.deloittedctcustomerapp.databinding.ChipEtagExpireBinding.bind((android.view.View) bindings[6]) : null
            , (bindings[2] != null) ? com.fwp.deloittedctcustomerapp.databinding.ChipEtagIssueBinding.bind((android.view.View) bindings[2]) : null
            , (bindings[5] != null) ? com.fwp.deloittedctcustomerapp.databinding.ChipEtagRefundBinding.bind((android.view.View) bindings[5]) : null
            , (android.widget.FrameLayout) bindings[1]
            , (bindings[3] != null) ? com.fwp.deloittedctcustomerapp.databinding.ChipEtagValidatedBinding.bind((android.view.View) bindings[3]) : null
            , (bindings[4] != null) ? com.fwp.deloittedctcustomerapp.databinding.ChipEtagVoidBinding.bind((android.view.View) bindings[4]) : null
            , (com.google.android.material.button.MaterialButton) bindings[15]
            , (androidx.cardview.widget.CardView) bindings[14]
            , (android.widget.TextView) bindings[11]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[7]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[9]
            , (android.widget.ImageView) bindings[8]
            , (android.widget.Button) bindings[13]
            , (android.widget.TextView) bindings[10]
            );
        this.etagStateContainer.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel ViewModel) {
        this.mViewModel = ViewModel;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}