package dagger.hilt.internal.aggregatedroot.codegen;

import dagger.hilt.android.HiltAndroidApp;
import dagger.hilt.internal.aggregatedroot.AggregatedRoot;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedRoot(
    root = "com.fwp.deloittedctcustomerapp.data.hilt.BaseApplication",
    originatingRoot = "com.fwp.deloittedctcustomerapp.data.hilt.BaseApplication",
    rootAnnotation = HiltAndroidApp.class
)
public class _com_fwp_deloittedctcustomerapp_data_hilt_BaseApplication {
}
