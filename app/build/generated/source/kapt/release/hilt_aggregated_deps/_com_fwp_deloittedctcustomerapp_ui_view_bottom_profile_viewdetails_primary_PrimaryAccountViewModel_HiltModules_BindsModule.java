package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ViewModelComponent",
    modules = "com.fwp.deloittedctcustomerapp.ui.view.bottom.profile.viewdetails.primary.PrimaryAccountViewModel_HiltModules.BindsModule"
)
public class _com_fwp_deloittedctcustomerapp_ui_view_bottom_profile_viewdetails_primary_PrimaryAccountViewModel_HiltModules_BindsModule {
}
