package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ActivityRetainedComponent",
    modules = "com.fwp.deloittedctcustomerapp.ui.view.bottom.viewmodel.LandingScreenActivityViewModel_HiltModules.KeyModule"
)
public class _com_fwp_deloittedctcustomerapp_ui_view_bottom_viewmodel_LandingScreenActivityViewModel_HiltModules_KeyModule {
}
