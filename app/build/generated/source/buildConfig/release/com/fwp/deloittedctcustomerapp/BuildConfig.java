/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.fwp.deloittedctcustomerapp;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.fwp.deloittedctcustomerapp";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 37;
  public static final String VERSION_NAME = "T.100.0.10";
}
