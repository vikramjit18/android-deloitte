-if class com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopyPrimaryRequest
-keepnames class com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopyPrimaryRequest
-if class com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopyPrimaryRequest
-keep class com.fwp.deloittedctcustomerapp.data.model.requests.RequestCopyPrimaryRequestJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
