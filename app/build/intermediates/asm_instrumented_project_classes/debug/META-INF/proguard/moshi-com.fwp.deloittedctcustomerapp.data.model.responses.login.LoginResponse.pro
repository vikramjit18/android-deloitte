-if class com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse
-if class com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponse
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.login.LoginResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
