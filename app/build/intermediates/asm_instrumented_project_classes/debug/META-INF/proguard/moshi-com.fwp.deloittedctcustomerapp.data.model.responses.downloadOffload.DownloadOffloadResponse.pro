-if class com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse
-if class com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponse
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.downloadOffload.DownloadOffloadResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
