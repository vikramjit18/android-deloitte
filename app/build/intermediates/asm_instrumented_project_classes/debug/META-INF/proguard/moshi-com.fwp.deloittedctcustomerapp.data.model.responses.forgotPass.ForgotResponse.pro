-if class com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse
-if class com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponse
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.forgotPass.ForgotResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
