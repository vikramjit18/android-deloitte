-if class com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
-keepnames class com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
-if class com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
-keep class com.fwp.deloittedctcustomerapp.data.model.DownloadedTagsJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.fwp.deloittedctcustomerapp.data.model.DownloadedTags
-keepclassmembers class com.fwp.deloittedctcustomerapp.data.model.DownloadedTags {
    public synthetic <init>(int,java.lang.String,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
