-if class com.fwp.deloittedctcustomerapp.data.model.responses.etag.ResObject
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.etag.ResObject
-if class com.fwp.deloittedctcustomerapp.data.model.responses.etag.ResObject
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.etag.ResObjectJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
