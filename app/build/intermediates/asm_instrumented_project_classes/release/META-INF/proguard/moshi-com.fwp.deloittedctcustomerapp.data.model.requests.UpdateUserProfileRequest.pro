-if class com.fwp.deloittedctcustomerapp.data.model.requests.UpdateUserProfileRequest
-keepnames class com.fwp.deloittedctcustomerapp.data.model.requests.UpdateUserProfileRequest
-if class com.fwp.deloittedctcustomerapp.data.model.requests.UpdateUserProfileRequest
-keep class com.fwp.deloittedctcustomerapp.data.model.requests.UpdateUserProfileRequestJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
