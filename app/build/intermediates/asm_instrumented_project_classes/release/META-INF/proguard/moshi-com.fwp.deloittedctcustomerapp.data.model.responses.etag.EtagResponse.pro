-if class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse
-if class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponse
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
