-if class com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
-if class com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponse
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.profile.UserProfileResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
