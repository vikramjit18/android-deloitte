-if class com.fwp.deloittedctcustomerapp.data.model.requests.ResetPasswordRequest
-keepnames class com.fwp.deloittedctcustomerapp.data.model.requests.ResetPasswordRequest
-if class com.fwp.deloittedctcustomerapp.data.model.requests.ResetPasswordRequest
-keep class com.fwp.deloittedctcustomerapp.data.model.requests.ResetPasswordRequestJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
