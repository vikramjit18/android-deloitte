-if class com.fwp.deloittedctcustomerapp.data.model.entities.UserCreds
-keepnames class com.fwp.deloittedctcustomerapp.data.model.entities.UserCreds
-if class com.fwp.deloittedctcustomerapp.data.model.entities.UserCreds
-keep class com.fwp.deloittedctcustomerapp.data.model.entities.UserCredsJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.fwp.deloittedctcustomerapp.data.model.entities.UserCreds
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.fwp.deloittedctcustomerapp.data.model.entities.UserCreds
-keepclassmembers class com.fwp.deloittedctcustomerapp.data.model.entities.UserCreds {
    public synthetic <init>(java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
