-if class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagAccounts
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagAccounts
-if class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagAccounts
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.etag.EtagAccountsJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
