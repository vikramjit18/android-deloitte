-if class com.fwp.deloittedctcustomerapp.data.model.responses.validateEtag.ValidateEtagResponse
-keepnames class com.fwp.deloittedctcustomerapp.data.model.responses.validateEtag.ValidateEtagResponse
-if class com.fwp.deloittedctcustomerapp.data.model.responses.validateEtag.ValidateEtagResponse
-keep class com.fwp.deloittedctcustomerapp.data.model.responses.validateEtag.ValidateEtagResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
